﻿$('input[type=file]').change(function () {  //選取類型為file且值發生改變的
    var file = this.files[0]; //定義file=發生改的file
    name = file.name; //name=檔案名稱
    size = file.size; //size=檔案大小
    type = file.type; //type=檔案型態

    if (file.size > 1024 * 1024 * 10) { //假如檔案大小超過300KB (300000/1024)

        alert("檔案上限10MB!!"); //顯示警告!!
        $(this).val("");  //將檔案欄設為空白
    }
    else if (file.type != "image/png" && file.type != "image/jpg" && !file.type != "image/gif" && file.type != "image/jpeg"
        && file.type != "application/pdf" && file.type != ".xlsx" && file.type != ".xls" && file.type != ".csv" && file.type != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && file.type != "video/mp4" && file.type != ".docx" && file.type != ".doc" && file.type != "application/vnd.openxmlformats-officedocument.wordprocessingml.document") { //假如檔案格式不等於 png 、jpg、gif、jpeg、mp4
        alert("檔案格式不符合: png, jpg, pdf, gif, xls, docx or mp4"); //顯示警告
        $(this).val(""); //將檔案欄設為空
    }
});