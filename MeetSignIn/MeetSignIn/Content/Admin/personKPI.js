﻿$(function () {
    //setDateTime($("#state_checkdate"), 0);
    var personId = $('#PersonId').val();
    var deptId = $('#DeptId').val();
    //加入KPI
    $('.join_kpi').on('click', function () {
        var parent = $(this).parent().parent().find('.join_parent').text();
        var title = $(this).parent().parent().find('.join_title').text();
        if (confirm('確定要加入【' + parent + '】的【' + title + '】項目嗎?')) {
            $.ajax({
                url: "/PersonKPIAdmin/JoinKPI",
                type: 'POST',
                anysc: false,
                dataType: "text",
                data: {
                    kpiid: $(this).attr('id'),
                    personId: personId,
                    deptId: deptId
                },
                success: function (response) {//response : PersonKPI 資料
                    if (response == 'success') {
                        toastr.success('資料已更新');
                        location.reload();
                        //    Reload('/Admin/PersonKPIAdmin/Edit?PersonId=' + $('#PersonId').val());
                    }
                    else {
                        alert('加入失敗');
                    }
                }
            });
        }      
    });

    //編輯目標狀態PersonKPI
    //var personkpi_ribbon = $('#personkpi_ribbon');
    $('.edit_person_kpi').on('click', function () {
        var id = $(this).attr('id');
        SetStateData(id);
    });

    function SetStateData(id) {       
        GetState(id);
        //GetApproval(id);
        $('#personkpi_form_id').val(id);
        $('#personkpi_form_kpiid').val($('#personkpi_id_' + id).find('#personkpi_fairlykpiid').val());
        $('#personkpi_form_kpititle').val($('#personkpi_id_' + id).find('.personkpi_fairlykpititle').text());
        $('#personkpi_form_title').val($('#personkpi_id_' + id).find('.personkpi_title').text());
        $('#personkpi_form_description').val($('#personkpi_id_' + id).find('.personkpi_description').val());

        //personkpi_ribbon.removeClass();
        //personkpi_ribbon.addClass('ribbon');
        //personkpi_ribbon.addClass('text-xl');
        //switch (parseInt($('#personkpi_id_' + id + ' #personkpi_approval').attr('value'))) {
        //    case 0:
        //        personkpi_ribbon.addClass('bg-secondary');
        //        personkpi_ribbon.text('草稿');
        //        break;
        //    case 1:
        //        personkpi_ribbon.addClass('bg-warning');
        //        personkpi_ribbon.text('審核中');
        //        break;
        //    case 2:
        //        personkpi_ribbon.addClass('bg-success');
        //        personkpi_ribbon.text('審核通過');
        //        break;
        //    case 3:
        //        personkpi_ribbon.addClass('bg-danger');
        //        personkpi_ribbon.text('審核駁回');
        //        break;
        //}
    }

    SetAddState();

    //送審
    $('#submit_state_trial').on('click', function () {
        if (confirm('確定要提交審查嗎?')) {
            $('#state_approval').val(1);
            SaveStateData();
        }
    });

    //儲存進度State(同一副指標)
    $('.save_states').on('click', function () {
        SaveStatesData($(this).attr('id').split('_')[1]);
    });

    //儲存進度State
    function SaveStatesData(kpiid) {
        var saveList = [];
        $('#personkpi_id_' + kpiid).find('.state_id').each(function () {
            //$(this).children('.state_description');
            saveList.push({
                ID: $(this).val(),
                PersonKPIID: kpiid,
                PersonId: $('#PersonId').val(),
                DeptId: $('#DeptId').val(),
                Title: $(this).parent().find('.state_title').val(),
                Description: $(this).parent().find('.state_description').val(),
                TargetNum: $(this).parent().find('.state_num').val(),
                TargetUnit: $(this).parent().find('.state_unit').val(),
                Percentage: $(this).parent().find('.state_percentage').val(),
                Quarter: $(this).parent().find('.state_quarter').val(),
                //approvalState: AAA,
                IsFinished: $(this).parent().find('.state_isFinished').is(':checked'),
                CheckDate: $(this).parent().find('.state_checkDate').val(),
            });
        });

        var stateDatas = JSON.stringify({ 'datas': saveList });
     
        console.log(stateDatas);
        $.ajax({
            url: "/PersonKPIAdmin/SaveStates",
            type: 'POST',
            anysc: false,
            dataType: "text",
            data: {
                stateDatas: stateDatas
            },
            success: function (response) {
                toastr.success('資料已更新');
                if (response == 'success') {
                    location.reload();
                }
            }
        });
    }

    $('#save_state').on('click', function () {
        SaveStateData();
    });

    function SaveStateData() {
        var saveData = {
            id: $('#state_id').val() == '' ? 0 : $('#state_id').val(),
            personkpiid: parseInt($('#state_personkpi_id').val()),
            personid: personId,
            deptid: deptId,
            title: $('#state_title').val(),
            description: $('#state_description').val(),
            num: parseInt($('#state_num').val()),
            unit: $('#state_unit').val(),
            approvalState: $('#state_approval').val(),
            sort: parseInt($('#state_sort').val()),
            isfinished: $('#state_isfinished').is(':checked'),
            checkdate: $('#state_checkdate').val()
        };
        //console.log(saveData);
        $.ajax({
            url: "/PersonKPIAdmin/SaveState",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: saveData,
            success: function (response) {
                toastr.success('資料已更新');
                $('#close_popup').click();
                location.reload();
            //    Reload('/Admin/PersonKPIAdmin/Edit?PersonId=' + $('#PersonId').val());
            }
        });
    }

    //作廢
    $('#save_invalid').on('click', function () {
        $.ajax({
            url: "/PersonKPIAdmin/InvalidInfo",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: {
                id: $('')
            },
            success: function (response) {
                //console.log(response);
                location.reload();
            }
        });
    });


    //送審
    $('#submit_trial').on('click', function () {
        if (confirm('確定要提交審查嗎?')) {
            var id = $(this).attr('name').split('_')[2];
            $('#edit_personkpi_approval').val(1);
            SetStateData(id);
            SaveData();
        }
    });

    //編輯頁送審
    $('#submit_personkpi_trial').on('click', function () {
        if (confirm('確定要提交審查嗎?')) {
            $('#IsAudit').val(true);
            $('#Edit_Form').submit();
        }
    });

    //儲存目標PersonKPI
    $('#save_personkpi').on('click', function () {
        $('#edit_personkpi_approval').val(0);
        SaveData();
    });

    function SaveData() {
        var saveData = {
            ID: $('#personkpi_form_id').val() == '' ? 0 : $('#personkpi_form_id').val(),
            KPIID: parseInt($('#personkpi_form_kpiid').val()),
            KPITitle: $('#personkpi_form_title').val(),
            KPIDescription: $('#personkpi_form_description').val(),
            PersonId: $('#PersonId').val(),
            DeptId: $('#DeptId').val(),
            //State: $("input[name='personkpi_state']:checked").val(),
            State: 1,
            //ApprovalState: $("input[name='personkpi_approval']:checked").val(),
            ApprovalState: 0,
            IsAudit: $('#edit_personkpi_approval').val() == 1
        };
        //console.log(saveData);
        $.ajax({
            url: "/PersonKPIAdmin/SavePersonKPI",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: saveData,
            success: function (response) {
                toastr.success('資料已更新');
                $('#close_popup').click();
                location.reload();
            //    Reload('/Admin/PersonKPIAdmin/Edit?PersonId=' + personId);
            },
            error: function () {
                toastr.error('更新失敗');
            }
        });
    }

    var state_ribbon = $('#state_ribbon');

    //$('.edit_state').on('click', function () {
    //    var idArr = $(this).attr('id').split('_');
    //    var personkpiId = idArr[0];
    //    var stateId = idArr[2];
    //    $.ajax({
    //        url: "/PersonKPIAdmin/GetStateInfo",
    //        type: 'POST',
    //        anysc: false,
    //        dataType: "json",
    //        data: {
    //            personId: $('#PersonID').val(),
    //            deptId: $('#DeptId').val(),
    //            stateId: stateId
    //        },
    //        success: function (response) {
    //            //console.log(response);
    //            $('#state_id').val(response.ID);
    //            $('#state_personkpi_id').val(response.PersonKPIID);
    //            $('#state_personkpi_personid').val(response.PersonID);
    //            $('#state_personkpi_deptid').val(response.DeptID);
    //            $('#state_title').val(response.Title);
    //            $('#state_description').val(response.Description);
    //            $('#state_num').val(response.TargetNum);
    //            $('#state_unit').val(response.TargetUnit);
    //            $('#state_sort').val(response.Sort);
    //            if (response.IsFinished) {
    //                $('#state_isfinished').prop('checked', true);
    //            }
    //            else {
    //                $('#state_isfinished').prop('checked', false);
    //            }

    //            $('#state_approval').val(response.ApprovalState);
    //            state_ribbon.removeClass();
    //            state_ribbon.addClass('ribbon');
    //            state_ribbon.addClass('text-xl');
    //            switch (response.ApprovalState) {
    //                case 0:
    //                    state_ribbon.addClass('bg-secondary');
    //                    state_ribbon.text('草稿');
    //                    break;
    //                case 1:
    //                    state_ribbon.addClass('bg-warning');
    //                    state_ribbon.text('審核中');
    //                    break;
    //                case 2:
    //                    state_ribbon.addClass('bg-success');
    //                    state_ribbon.text('審核通過');
    //                    break;
    //                case 3:
    //                    state_ribbon.addClass('bg-danger');
    //                    state_ribbon.text('審核駁回');
    //                    break;
    //            }
    //            if (response.ApprovalState == 1) {
    //                $('#save_state').attr('style', 'display:none;');
    //                $('#submit_state_trial').attr('style', 'display:none;');
    //            }
    //            else {
    //                $('#save_state').removeAttr('style', 'display:none;');
    //                $('#submit_state_trial').removeAttr('style', 'display:none;');
    //            }

    //            var checkDate = new Date(parseInt(response.CheckDate.substr(6)));
    //            var day = ("0" + checkDate.getDate()).slice(-2);
    //            var month = ("0" + (checkDate.getMonth() + 1)).slice(-2);
    //            var today = checkDate.getFullYear() + "-" + (month) + "-" + (day);
    //            $('#state_checkdate').val(today);
    //            //$('#state_checkdate').val(ComposeDate(response.CheckDate));
    //        }
    //    });
    //});
 
    //取得Person KPI 進度

    function GetState(personkpiId) {
        var state = $('#personkpi_id_' + personkpiId + ' #personkpi_state').attr('value');
        $.ajax({
            url: "/PersonKPIAdmin/GetState",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: {
                personkpiId: personkpiId,
            },
            success: function (response) {
                //console.log(response);
                var stateListHtml = '';
                for (var i = 0; i < response.length; i++) {
                    var checkedStr = '';
                    var stateInfo = response[i];
                    if (state != 0 && stateInfo.ID == state) {
                        checkedStr = 'checked';
                    }
                    stateListHtml += `<div class="pretty p-icon p-round personkpi_state_radio">
                                                        <input type="radio" name="personkpi_state" value="${stateInfo.ID}" ${checkedStr}>
                                                        <div class="state p-info-o">
                                                            <i class="icon fa fa-check"></i>
                                                            <label>${stateInfo.Title}</label>
                                                        </div>
                                                    </div>`;
                }
                var addStateHtml = `<div> 
                                                        <button type="button" class="btn btn-sm btn-outline-success add_state" id="state_${personkpi_id}" data-bs-toggle="modal" data-bs-target="#stateModal" style="margin-top: 10px;"><i class="fa fa-plus"></i></button>
                                                    </div>`;
                $('#personkpi_state_area').html(stateListHtml);
                //$('.add_state_area').html(addStateHtml);
                SetAddState();
            }
        });
    }

    //取得Person KPI 審核狀態
    //function GetApproval(personkpiId) {
    //    var approval = $('#personkpi_id_' + personkpiId + ' #personkpi_approval').attr('value');
    //    $('.personkpi_approval_radio').each(function () {
    //        if ($(this).find('input').val() == approval) {
    //            $(this).find('input').prop('checked', true);
    //        }
    //    });
    //}

    function SetAddState() {
        //新增進度State
        $('.add_state').on('click', function () {
            var personkpiId = $(this).attr('id').split('_')[1];
            $('#stateModalLabel').text($('#personkpi_id_' + personkpiId + ' #personkpi_title').text());
            $('#state_personkpi_id').val(personkpiId);
            $('#state_num').val(0);
            $('#state_sort').val(0);

            state_ribbon.removeClass();
            state_ribbon.addClass('ribbon');
            state_ribbon.addClass('text-xl');
            state_ribbon.addClass('bg-secondary');
            state_ribbon.text('草稿');

            var plusMonths = 1;
            var now = new Date();
            var day = ("0" + now.getDate()).slice(-2);
            var month = ("0" + (now.getMonth() + 1 + plusMonths)).slice(-2);
            var today = now.getFullYear() + "-" + (month) + "-" + (day);
            $('#state_checkdate').val(today);
        });
    }

    $('.revoke_person_kpi').on('click', function () {
        if (!confirm('確定要撤銷嗎?')) {
            return false;
        }
    });

    $('.edit_monthkpi').on('click', function () {
        var idArr = $(this).attr('id').split('_');
        var kpiId = idArr[0];
        var personkpiId = idArr[1];
        var month = idArr[2];
        $.ajax({
            url: "/PersonKPIAdmin/GetMonthKPIInfo",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: {
                personId: $('#PersonID').val(),
                deptId: $('#DeptId').val(),
                personkpiId: personkpiId,
                month: month
            },
            success: function (response) {
                //console.log(response);
                $('#month_id').val(response.ID);
                $('#month_kpiid').val(response.KPIID);
                $('#month_personid').val(response.PetsonId);
                $('#month_deptid').val(response.DeptId);
                $('#month_todo').val(response.ToDo);
                $('#month_expect').val(response.Expect);
                $('#month_complete').val(response.Complete);
                if (response.IsFinished) {
                    $('#month_isfinished').prop('checked', true);
                }
                else {
                    $('#month_isfinished').prop('checked', false);
                }

                var deadline = new Date(parseInt(response.CheckDate.substr(6)));
                var day = ("0" + deadline.getDate()).slice(-2);
                var month = ("0" + (deadline.getMonth() + 1)).slice(-2);
                var today = deadline.getFullYear() + "-" + (month) + "-" + (day);
                $('#month_deadline').val(today);
            }
        });
    });

    //取得 MonthKPI 
    function GetMonthKPI(personkpiId) {
        var month = $('#personkpi_id_' + personkpiId + ' #personkpi_month').attr('value');
        $.ajax({
            url: "/PersonKPIAdmin/GetMonthKPI",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: {
                personkpiId: personkpiId,
            },
            success: function (response) {
                //console.log(response);
                var monthListHtml = '';
                for (var i = 0; i < response.length; i++) {
                    var checkedStr = '';
                    var monthInfo = response[i];
                    if (month != 0 && monthInfo.ID == month) {
                        checkedStr = 'checked';
                    }
                    monthListHtml += `<div class="pretty p-icon p-round personkpi_month_radio">
                                                        <input type="radio" name="personkpi_month" value="${monthInfo.ID}" ${checkedStr}>
                                                        <div class="month p-info-o">
                                                            <i class="icon fa fa-check"></i>
                                                            <label>${monthInfo.Title}</label>
                                                        </div>
                                                    </div>`;
                }
                $('#personkpi_month_area').html(monthListHtml);
                SetAddState();
            }
        });
    }
})
