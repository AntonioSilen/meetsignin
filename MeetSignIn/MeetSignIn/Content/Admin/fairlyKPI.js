﻿$(function () {
    //setDateTime($("#CheckDate"), 0);

    var $selectedDeptType = $('#Dept');
    var $selectedDeptKPIType = $('#dept_dept');
    var remove;

    //Dept
    $selectedDeptType.on('itemAdded', function (event) {
        if (remove) {
            $selectedSigner.tagsinput('remove', remove);
            remove = '';
        }
    });
    $('.js-signer-Dept').on('click', function () {
        //var id = $(this).val();
        var val = $(this).text();
        $selectedDeptType.tagsinput('add', val);
    });

    $('.add_dept_kpi').on('click', function () {
        var obj = new Object();
        obj.CheckDateStr = $('#CheckDate').val();
        obj.Type = $('#Type').val();
        obj.Level = 2;
        obj.IsOnline = false;
        obj.ApprovalState = 0;
        SetPopupValue(obj);
    });

    //var kpi_ribbon = $('#kpi_ribbon');
    $('.edit_dept_kpi').on('click', function () {
        var id = $(this).attr('id');
        $.ajax({
            url: "/DeptKPIAdmin/GetInfo",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: {
                id: id
            },
            success: function (response) {
                SetPopupValue(response);
            }
        });
    });

    //送審
    $('#submit_trial').on('click', function () {
        if (confirm('確定要提交審查嗎?')) {
            $('#dept_approval').val(1);
            SaveData();
        }     
    });

    //儲存
    $('#save_dept_kpi').on('click', function () {
        $('#dept_approval').val(0);
        SaveData();
    });

    function SaveData() {
        var saveData = {
            id: $('#dept_id').val() == '' ? 0 : $('#dept_id').val(),
            title: $('#dept_title').val(),
            type: $('#Type').val(),
            level: 2,
            dept: $('#dept_dept').val(),
            parentkpiId: $('#ID').val(),
            checkdate: $('#dept_checkdate').val(),
            description: $('#dept_description').val(),
            isonline: $('#dept_isonline').is(':checked'),
            //approvalState: $("input[name='dept_approval']:checked").val(),
            approvalState: $('#dept_approval').val(),
        };
        $.ajax({
            url: "/DeptKPIAdmin/SaveInfo",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: saveData,
            success: function (response) {
                location.reload();
                //var statusStr = response.IsOnline ? '<span class="badge badge-success" style="padding: 8px;">啟用</span>' : '<span class="badge badge-danger" style="padding: 8px;">停用</span>';
                //var approvalStr = '';
                //switch (response.ApprovalState) {
                //    case 0:
                //        approvalStr = '<span class="badge badge-secondary" style="padding: 8px;">草稿</span>';
                //        break;
                //    case 1:
                //        approvalStr = '<span class="badge badge-warning" style="padding: 8px;">審核中</span>';
                //        break;
                //    case 2:
                //        approvalStr = '<span class="badge badge-success" style="padding: 8px;">審核通過</span>';
                //        break;
                //    case 3:
                //        approvalStr = '<span class="badge badge-danger" style="padding: 8px;">審核駁回</span>';
                //        break;
                //}
                //var deptStr = '';
                //$('#close_popup').click();
                //if ($('#dept_kpi_' + response.ID).length <= 0) {//新增
                //    $('#depe_kpi_tbody').append(`<tr id="dept_kpi_${response.ID}">
                //                                            <td id="td_title" >${response.Title}</td>
                //                                            <td id="td_type" >${response.TypeStr}</td>
                //                                            <td id="td_level" >${response.LevelStr}</td>
                //                                            <td id="td_dept" >${deptStr}</td>
                //                                            <td id="td_status" >${statusStr}</td>
                //                                            <td id="td_approval" >${approvalStr}</td>
                //                                            <th id="td_checkdate" >${response.CheckDateStr}</th>
                //                                            <td>
                //                                                <a href="#" class="btn btn-primary edit_dept_kpi" id="${response.ID}" data-bs-toggle="modal" data-bs-target="#exampleModal">
                //                                                    <i class="fa fa-edit"></i>
                //                                                </a>
                //                                            </td>
                //                                        </tr>`);
                //}
                //else {//更新
                //    $('#dept_kpi_' + response.ID + ' #td_title').text(response.Title);
                //    $('#dept_kpi_' + response.ID + ' #td_type').text(response.TypeStr);
                //    $('#dept_kpi_' + response.ID + ' #td_level').text(response.LevelStr);
                //    $('#dept_kpi_' + response.ID + ' #td_status').html(statusStr);
                //    $('#dept_kpi_' + response.ID + ' #td_approval').html(approvalStr);
                //    $('#dept_kpi_' + response.ID + ' #td_checkdate').text(response.CheckDateStr);
                //}
                toastr.success('資料已更新');
            },
            error: function () {
                toastr.error('更新失敗');
            }
        });
    }

    function SetTagsInput(dept) {
        //DeptKPI
        $selectedDeptKPIType.on('itemAdded', function (event) {
            if (remove) {
                $selectedSigner.tagsinput('remove', remove);
                remove = '';
            }
        });
        $('.js-signer-Dept_KPI').on('click', function () {
            var id = $(this).val();
            var val = $(this).text();
            $selectedDeptKPIType.tagsinput('add', val);
        });
        if (dept != null && dept != '') {
            var dataArr = dept.split(',');
            for (var i = 0; i < dataArr.length; i++) {
                $selectedDeptKPIType.tagsinput('add', $.trim(dataArr[i]));
            }
        }
        else {
            $('#dept_dept').tagsinput('removeAll');
        }
    }

    function SetPopupValue(data) {
        $('#dept_id').val(data.ID);
        $('#dept_title').val(data.Title);
        $('#dept_type').val(data.Type);
        $('#dept_level').val(data.Level);
        $('#dept_dept').val(data.Dept);
        $('#dept_checkdate').val(data.CheckDateStr);
        $('#dept_description').val(data.Description);
        if (data.IsOnline) {
            $('#dept_isonline').prop('checked', true);
        }
        $('.dept_kpi_radio').each(function () {
            if ($(this).find('input').val() == data.ApprovalState) {
                $(this).find('input').prop('checked', true);
            }
        });
        $("#dept_checkdate").datepicker({ dateFormat: "yy-mm-dd" });
        SetTagsInput(data.Dept);

        if (data.ApprovalState == 1) {
            $('#save_dept_kpi').attr('style', 'display:none;');
        }
        else {
            $('#save_dept_kpi').attr('style', 'display:initial;');
        }

    //    $('#dept_approval_lbl').removeClass();
    //    switch (data.ApprovalState) {
    //        case 0:
    //            $('#dept_approval_lbl').text('草稿');             
    //            $('#dept_approval_lbl').addClass('label label-default bg-gray');
    //            break;
    //        case 1:
    //            $('#dept_approval_lbl').text('審核中');
    //            $('#dept_approval_lbl').addClass('label label-warning');
    //            break;
    //        case 2:
    //            $('#dept_approval_lbl').text('審核通過');
    //            $('#dept_approval_lbl').addClass('label label-success bg-olive');
    //            break;
    //        case 3:
    //            $('#dept_approval_lbl').text('審核駁回');
    //            $('#dept_approval_lbl').addClass('label label-danger');
    //            break;
    //    }
    }
})