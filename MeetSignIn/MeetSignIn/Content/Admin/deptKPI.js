﻿$(function () {
    var deptId = $('#DeptId').val();

    //作廢
    $('#save_invalid').on('click', function () {
        $.ajax({
            url: "/PersonKPIAdmin/InvalidInfo",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: {
                id: $('')
            },
            success: function (response) {
                //console.log(response);
                location.reload();
            }
        });
    });


    //送審
    $('#submit_trial').on('click', function () {
        if (confirm('確定要提交審查嗎?')) {
            $('#edit_personkpi_approval').val(1);
            SaveData();
        }
    });

    //儲存目標PersonKPI
    $('#save_personkpi').on('click', function () {
        $('#edit_personkpi_approval').val(0);
        SaveData();
    });

    function SaveData() {
        var saveData = {
            ID: $('#personkpi_form_id').val() == '' ? 0 : $('#personkpi_form_id').val(),
            KPIID: parseInt($('#personkpi_form_kpititle').val()),
            KPITitle: $('#personkpi_form_title').val(),
            KPIDescription: $('#personkpi_form_description').val(),
            PersonId: $('#PersonId').val(),
            DeptId: $('#DeptId').val(),
            State: $("input[name='personkpi_state']:checked").val(),
            //ApprovalState: $("input[name='personkpi_approval']:checked").val(),
            ApprovalState: $('#edit_personkpi_approval').val(),
        };
        //console.log(saveData);
        $.ajax({
            url: "/PersonKPIAdmin/SavePersonKPI",
            type: 'POST',
            anysc: false,
            dataType: "json",
            data: saveData,
            success: function (response) {
                toastr.success('資料已更新');
                $('#close_popup').click();
                location.reload();
            //    Reload('/Admin/PersonKPIAdmin/Edit?PersonId=' + personId);
            },
            error: function () {
                toastr.error('更新失敗');
            }
        });
    }

    $('.revoke_person_kpi').on('click', function () {
        if (!confirm('確定要撤銷嗎?')) {
            return false;
        }
    });
})
