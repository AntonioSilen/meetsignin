﻿"use strict";

var selectedArr = [];
var controllerStr = $('#controller_str').val() || 'MeetingAdmin';

//廠區切換
function ChangeFactory() {
    var factory = $('#FactoryID').val();
    var roomId = $('#RoomID').val();
    $('#RoomID').children().remove();
    $.ajax({
        url: "/Admin/" + controllerStr + "/GetRooms",
        type: 'POST',
        dataType: "json",
        data: {
            factory: factory
        },
        success: function (data) {
            console.log(data);
            $.map(data, function (room) {
                var selected = room.ID == roomId ? 'selected' : '';
                $('#RoomID').append('<option value="' + room.ID + '" ' + selected + '>' + room.Place + ' (' + room.RoomNumber + ')</option>');
            });
        }
    });
}
//廠區切換
function ChangeDepartment(iscadre) {
    var department = $('#Department').val();
    $('#check_list').children().remove();
    $.ajax({
        url: "/Admin/" + controllerStr + "/GetEmployee",
        type: 'POST',
        dataType: "json",
        data: {
            department: department,
            cadre: iscadre
        },
        success: function (data) {
            console.log(data);
            var checkall = '1';
            $.map(data, function (employee) {
                var name = employee.Name + ' ' + (employee.EngName || "");
                var checked = selectedArr.indexOf(employee.JobID) >= 0 ? 'checked' : '';
                if (checked == '') {
                    checkall = '0';
                }
                $('#check_list').append('<p><input type="checkbox" id="' + employee.ID + '_' + employee.JobID + '_' + employee.DepartmentStr + '_' + name + '" name="ckb" class="notify_ckb" ' + checked + ' /> ' + name + '</p>');
            });
            if (checkall == '1') {
                $('#check_all').prop("checked", true);
            }
            else {
                $('#check_all').prop("checked", false);
            }
            CheckedNotify();
        }
    });
}
//檢查會議室使用狀態
function CheckRoomStatus() {
    var room = $('#RoomID').val();
    var start = $('#StartDate').val();
    var end = $('#EndDate').val();
    $.ajax({
        url: "/Admin/" + controllerStr + "/CheckRoomStatus",
        type: 'POST',
        dataType: "text",
        async: false,
        data: {
            room: room,
            start: start,
            end: end,
            excmid: $('#ID').val()
        },
        success: function (data) {
            return data;
        }
    });
}

function CheckedNotify() {
    $('.notify_ckb').on('change', function () {
        SwitchNotifyStatus($(this));
    });   
}

function SwitchNotifyStatus(target) {
    var thisId = target.attr('id');
    var info = thisId.split('_');
    if (target.is(":checked")) {//勾選
        if (selectedArr.indexOf(info[1]) < 0) {
            $('#notify_tb').append('<tr class="tb_item" id="' + info[1] + '">' +
                '<td>' + info[1] + '</td>' +
                '<td><label class="text text-success">' + info[3] + '</label></td>' +
                '<td><label class="label label-primary">' + info[2] + '</label></td>' +
                '</tr>');
            selectedArr.push(info[1]);
        }
    }
    else {//取消勾選
        var index = selectedArr.indexOf(info[1]);
        if (index >= 0) {
            $('#' + info[1]).remove();
            selectedArr.splice(index, 1);
        }
    }
}