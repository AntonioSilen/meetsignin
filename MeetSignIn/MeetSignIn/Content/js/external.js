﻿$(function () {
    $('.course_date').each(function () {
        $(this).datepicker({ dateFormat: "yy-mm-dd" });
    });
    $('.close_course').each(function () {
        var id = $(this).attr('id').split('_')[1];
        $(this).on('click', function () {
            if (confirm('確定移除此筆資料嗎?')) {
                //var id = $(this).attr('id').split('_')[1];
                $('#course_' + id).remove();
            }
        });
    });

    $('.copy_last_course').on('click', function () {
        var fromId = parseInt($(this).attr('id').split('_')[1]);
        var toId = fromId + 1;
        var toElement = $('#course_' + toId);
        toElement.find('.course_organizer').val($('#course_' + fromId).find('.course_organizer').val());
        toElement.find('.course_lecturer').val($('#course_' + fromId).find('.course_lecturer').val());
        toElement.find('.course_courseMaterial').val($('#course_' + fromId).find('.course_courseMaterial').val());
        toElement.find('.course_address').val($('#course_' + fromId).find('.course_address').val());
        toElement.find('.course_startDate').val($('#course_' + fromId).find('.course_startDate').val());
        toElement.find('.course_endDate').val($('#course_' + fromId).find('.course_endDate').val());
        toElement.find('.course_registery').val($('#course_' + fromId).find('.course_registery').val());
        toElement.find('.course_travel').val($('#course_' + fromId).find('.course_travel').val());
        toElement.find('.course_others').val($('#course_' + fromId).find('.course_others').val());
        toElement.find('.course_total').val($('#course_' + fromId).find('.course_total').val());
    });

    //$('#add_course').on('click', function () {
    //    var courseStr = '';
    //    var item_num = $('.course_item').length + 1;
    //    courseStr += `<div class="course_item" id="course_${item_num}">
    //                        <div class="form-group">
    //                            <div class="col-sm-12">
    //                                <hr align="center" width="100%" class="hr_dashed">
    //                                <h3 class="h3_course">
    //                                    外訓課程${item_num}
    //                                    <button type="button" class="close close_course" style="opacity:0.7;" id="close_${item_num}">
    //                                        <span aria-hidden="true" style="color:#c12b2b;">×</span>
    //                                    </button>
    //                                </h3>
    //                            </div>
    //                        </div>
    //                        <div class="form-group" style="margin-left:5px;">
    //                            <div class="col-sm-4">
    //                                <label>主辦單位</label>
    //                                <input class="form-control lmt-h course_organizer lock_disabled" maxlength="50" />
    //                            </div>
    //                            <div class="col-sm-2">
    //                                <label>講師姓名</label>
    //                               <input class="form-control lmt-h course_lecturer lock_disabled" maxlength="50" />
    //                            </div>
    //                            <div class="col-sm-3">
    //                                <label>教材</label>
    //                                <textarea class="form-control lmt-h course_courseMaterial lock_disabled" maxlength="200" style="resize: none;"></textarea>
    //                            </div>
    //                            <div class="col-sm-3">
    //                                <label>地點</label>
    //                                <textarea class="form-control lmt-h course_address lock_disabled" maxlength="200" style="resize: none;"></textarea>
    //                            </div>
    //                        </div>
    //                        <div class="form-group" style="margin-left:5px;">
    //                            <div class="col-sm-6">
    //                                <label>開始上課日期</label>
    //                                <input class="form-control course_startDate lock_disabled"  id=course_startDate_${item_num} value="${$('#course_startDate_1').val()}" required />
    //                            </div>
    //                            <div class="col-sm-6">
    //                                <label>結束上課日期</label>
    //                                <input class="form-control course_endDate lock_disabled"  id=course_endDate_${item_num} value="${$('#course_endDate_1').val()}" required />
    //                            </div>
    //                        </div>
    //                        <div class="form-group" style="margin-left:5px;">
    //                            <div class="col-sm-2">
    //                                <label>報名費</label>
    //                                <input class="form-control lmt-h course_registery lock_disabled" type="number" />
    //                            </div>
    //                            <div class="col-sm-2">
    //                                <label>差旅</label>
    //                                <input class="form-control lmt-h course_travel lock_disabled" type="number" />
    //                            </div>
    //                            <div class="col-sm-2">
    //                                <label>其他費用</label>
    //                                <input class="form-control lmt-h course_others lock_disabled" type="number" />
    //                            </div>
    //                            <div class="col-sm-2">
    //                                <label>合計</label>
    //                                <input class="form-control lmt-h course_total lock_disabled" type="number" />
    //                            </div>
    //                        </div>
    //                    </div>`;

    //    $('#course_list').append(courseStr);
    //    $("#course_date_" + item_num).datepicker({ dateFormat: "yy-mm-dd" });
    //    //setDateTime($("#course_date_" + item_num), 0);

    //    $('#close_' + item_num).on('click', function () {
    //        if (confirm('確定移除此筆資料嗎?')) {
    //            //var id = $(this).attr('id').split('_')[1];
    //            $('#course_' + item_num).remove();
    //        }
    //    });
    //});

    $('#SaveBtn').on('click', function () {       
        CheckData();
    });

    $('#SkipBtn').on('click', function () {
        $('#IsSkip').val(true);
        $('#EditForm').submit();
    });

    if ($('#IsAudit').val() == 'True') {
        $('.lock_disabled').each(function () {
            $(this).attr('disabled', true);
        });
    }

    $('.fee').on('change', function () {
        var courseId = $(this).attr('id').split('_')[1];
        var course_registery = parseInt($('#registery_' + courseId).val()) || 0;
        var course_travel = parseInt($('#travel_' + courseId).val()) || 0;
        var course_others = parseInt($('#others_' + courseId).val()) || 0;
        $('#total_' + courseId).val(course_registery + course_travel + course_others);
    });

    $('#SendCreateAudit').on('click', function () {
        //if (!$('#OfficerAudit').is(':checked')) {
        //    alert('承辦人須完成簽核，方可將資料送審 !');
        //}
        //else {
            if (confirm('確定將資料送審嗎?')) {
                $('#IsAudit').val(true);
                /* $('#EditForm').submit();*/
                CheckData();
            }
        //}
    });

    function CheckData() {
        var isvalidate = true;
        var jsonObj = [];
        if ($('#Inspection').val() == '') {
            alert('須至少選擇一項評估驗收方式 !');
            isvalidate = false;
        }
        else if ($('#Reason').val() == '') {
            alert('請填寫遴選課程原因 !');
            isvalidate = false;
        }
        else {
            $('.course_item').each(function () {
                if ($(this).find('.course_organizer').val() == '' && isvalidate) {
                    alert('課程【主辦單位】不可空白，若無資料請填"無資料"');
                    isvalidate = false;
                }
                else if ($(this).find('.course_lecturer').val() == '' && isvalidate) {
                    alert('課程【講師姓名】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                else if ($(this).find('.course_courseMaterial').val() == '' && isvalidate) {
                    alert('課程【教材】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                else if ($(this).find('.course_address').val() == '' && isvalidate) {
                    alert('課程【地點】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                else if ($(this).find('.course_startDate').val() == '' && isvalidate) {
                    alert('課程【開始上課日期】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                else if ($(this).find('.course_endDate').val() == '' && isvalidate) {
                    alert('課程【結束上課日期】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                else if ($(this).find('.course_registery').val() == '' && isvalidate) {
                    alert('課程【報名費】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                else if ($(this).find('.course_total').val() == '' && isvalidate) {
                    alert('課程【合計】不可空白，若無資料請填"無資料');
                    isvalidate = false;
                }
                var item = {}
                item["ID"] = $(this).find('.course_id').val();
                item["TrainingID"] = $(this).find('.course_trainingId').val();
                item["ExternalID"] = $(this).find('.course_externalId').val();
                item["Organizer"] = $(this).find('.course_organizer').val();
                item["Lecturer"] = $(this).find('.course_lecturer').val();
                item["CourseMaterial"] = $(this).find('.course_courseMaterial').val();
                item["Address"] = $(this).find('.course_address').val();
                item["StartDate"] = $(this).find('.course_startDate').val();
                item["EndDate"] = $(this).find('.course_endDate').val();
                item["Registery"] = $(this).find('.course_registery').val();
                item["Travel"] = $(this).find('.course_travel').val();
                item["Others"] = $(this).find('.course_others').val();
                item["Total"] = $(this).find('.course_total').val();
                item["IsOnline"] = $(this).find('.course_isonline').val();

                jsonObj.push(item);
            });
            $('#JsonCourse').val(JSON.stringify(jsonObj));

            if (isvalidate) {
                $('#EditForm').submit();
            }
            //else {
            //    alert('課程資料有缺漏，請於填寫後再送出資料 !');
            //}
        }
    }
})