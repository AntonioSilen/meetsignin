﻿$(function () {
    $('.course_date').each(function () {
        $(this).datepicker({ dateFormat: "yy-mm-dd" });
    });
    $('.close_course').each(function () {
        var id = $(this).attr('id').split('_')[1];
        $(this).on('click', function () {
            if (confirm('確定移除此筆資料嗎?')) {
                //var id = $(this).attr('id').split('_')[1];
                $('#course_' + id).remove();
            }
        });
    });

    CopyCourse();

    function CopyCourse() {
        $('.copy_last_course').on('click', function () {
            var fromId = parseInt($(this).attr('id').split('_')[1]) - 1;
            var toId = fromId + 1;
            var toElement = $('#course_' + toId);
            toElement.find('.course_outline').val($('#course_' + fromId).find('.course_outline').val());
            toElement.find('.course_date').val($('#course_' + fromId).find('.course_date').val());
            toElement.find('.course_time').val($('#course_' + fromId).find('.course_time').val());
            toElement.find('.course_hours').val($('#course_' + fromId).find('.course_hours').val());
            toElement.find('.course_progress').val($('#course_' + fromId).find('.course_progress').val());
            toElement.find('.course_lecturer').val($('#course_' + fromId).find('.course_lecturer').val());
            toElement.find('.course_method').val($('#course_' + fromId).find('.course_method').val());
        });
    }

    $('#add_course').on('click', function () {
        var courseStr = '';
        var item_num = $('.course_item').length + 1;
        courseStr += `<div class="course_item" id="course_${item_num}">                                       
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <hr align="center" width="100%" class="hr_dashed">
                                            <h3 class="h3_course">
                                                內訓課程${item_num}
                                                <button type="button" class="close close_course" style="opacity:0.7;" id="close_${item_num}">
                                                    <span aria-hidden="true" style="color:#c12b2b;">×</span>
                                                </button>
                                            </h3>
                                            <input type="button" id="copy_${item_num}" class="btn btn-default copy_last_course" value="複製上一個課程"/>
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left:5px;">
                                        <div class="col-sm-4">
                                            <label>大綱</label>
                                            <textarea class="form-control lmt-h course_outline lock_disabled" maxlength="200" style="resize:none; " required></textarea>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>日期</label>
                                            <input class="form-control lmt-h course_date lock_disabled" id=course_date_${item_num} value="${$('#course_date_1').val()}" required />
                                        </div>
                                        <div class="col-sm-4">
                                            <label>授課時間</label>
                                            <textarea class="form-control lmt-h course_time lock_disabled" maxlength="200" style="resize: none;" required></textarea>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>時數</label>
                                            <input class="form-control lmt-h course_hours lock_disabled" type="number" step="0.1" required />
                                        </div>
                                    </div>
                                    <div class="form-group" style="margin-left:5px;">
                                        <div class="col-sm-5">
                                            <label>課程進度/內容</label>
                                            <textarea class="form-control lmt-h course_progress lock_disabled" maxlength="500" style="resize: none;" required></textarea>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>授課講師</label>
                                            <input class="form-control lmt-h course_lecturer lock_disabled" maxlength="50" required />
                                        </div>
                                        <div class="col-sm-5">
                                            <label>訓練方式</label>
                                            <input class="form-control lmt-h course_method lock_disabled" maxlength="500" required />
                                        </div>
                                        <div class="col-sm-5">
                                            <label>設備</label><br />
                                            <div class="pretty p-default p-curve">
                                                <input type="checkbox" class="course_devicecheck"/>
                                                <div class="state p-success">
                                                    <label>已檢查</label>
                                                    <strong style="color: red;"> ∗ 需確認並勾選此項以進行後續步驟</strong>
                                                </div>
                                            </div>
                                            <textarea class="form-control lmt-h course_device lock_disabled" maxlength="500" style="resize: none;"></textarea>
                                        </div>
                                    </div>
                                </div>`;

        $('#course_list').append(courseStr);
        $("#course_date_" + item_num).datepicker({ dateFormat: "yy-mm-dd" });
        //setDateTime($("#course_date_" + item_num), 0);

        $('#close_' + item_num).on('click', function () {
            if (confirm('確定移除此筆資料嗎?')) {
                //var id = $(this).attr('id').split('_')[1];
                $('#course_' + item_num).remove();
            }
        });
        CopyCourse();
    });

    $('#SaveBtn').on('click', function () {
        CheckData();
    });

    $('#SkipBtn').on('click', function () {
        $('#IsSkip').val(true);
        $('#EditForm').submit();
    });

    if ($('#IsAudit').val() == 'True') {
        $('.lock_disabled').each(function () {
            $(this).attr('disabled', true);
        });
    }

    $('#SendCreateAudit').on('click', function () {
        //if (!$('#OfficerAudit').is(':checked')) {
        //    alert('承辦人須完成簽核，方可將資料送審 !');
        //}
        if (!$('#Response').prop("checked") && $('#Response').val() == 'False'
            && !$('#Learning').prop("checked") && $('#Learning').val() == 'False'
            && !$('#Behavior').prop("checked") && $('#Behavior').val() == 'False'
            && !$('#Outcome').prop("checked") && $('#Outcome').val() == 'False') {
            alert('須至少選擇一項評估驗收方式 !');
        }
        else {
            if (confirm('確定將資料送審嗎?')) {
                $('#IsAudit').val(true);
                CheckData();
                //$('#EditForm').submit();
            }
        }
    });

    var alertStr = '';
    var isvalidate = true;
    var isscroll = false;
    function CheckData() {
        var jsonObj = [];
        isvalidate = true;
        isscroll = false;
        alertStr = '';
        CheckCourseInfo('訓練需求調查', $('#Investigation').val(), 'Investigation');
        CheckCourseInfo('師資學歷', $('#Education').val(), 'Education');
        CheckCourseInfo('師資經歷', $('#Experience').val(), 'Experience', alertStr);
        CheckCourseInfo('訓練目標', $('#Objective').val(), 'Objective');
        CheckCourseInfo('授課地點', $('#Address').val(), 'Address');
        var isCourseValidate = true;
        $('.course_item').each(function () {
            var course1 = 'course_1';
            CheckCourseInfo('大綱', $(this).find('.course_outline').val(), course1, true);
            CheckCourseInfo('日期', $(this).find('.course_date').val(), course1, true);
            CheckCourseInfo('授課時間', $(this).find('.course_time').val(), course1, true);
            CheckCourseInfo('時數', $(this).find('.course_hours').val(), course1, true);
            CheckCourseInfo('課程進度/內容', $(this).find('.course_progress').val(), course1, true);
            CheckCourseInfo('授課講師', $(this).find('.course_lecturer').val(), course1, true);
            CheckCourseInfo('訓練方式', $(this).find('.course_method').val(), course1, true);
            var item = {}
            item["ID"] = $(this).find('.course_id').val();
            item["TrainingID"] = $(this).find('.course_trainingId').val();
            item["InternalID"] = $(this).find('.course_internalId').val();
            item["Outline"] = $(this).find('.course_outline').val();
            item["Date"] = $(this).find('.course_date').val();
            item["Time"] = $(this).find('.course_time').val();
            item["Hours"] = $(this).find('.course_hours').val();
            item["Progress"] = $(this).find('.course_progress').val();
            item["Lecturer"] = $(this).find('.course_lecturer').val();
            item["TrainingMethod"] = $(this).find('.course_method').val();
            item["DeviceCheck"] = $(this).find('.course_devicecheck').is(':checked');
            item["Device"] = $(this).find('.course_device').val();
            item["IsOnline"] = $(this).find('.course_isonline').val();

            jsonObj.push(item);
        });
        $('#JsonCourse').val(JSON.stringify(jsonObj));

        if (isvalidate) {
            $('#EditForm').submit();
        }
        else {
            if (!isCourseValidate) {
                alertStr = '須至少填寫一項課程，多餘課程請點擊右側【×】移除 ! \r\n' + alertStr;
            }
            alert(alertStr);
        }
    }

    function CheckCourseInfo(field, value, scrollId, isCourse = false) {
        if (value == '') {
            if (isCourse) {
                alertStr += '課程【' + field + '】不可空白，若無資料請填"無" ! \r\n';
                isCourseValidate = false
            }
            else {
                alertStr += '【' + field + '】不可空白 ! \r\n';
            }
          
            ScrollTo($("#" + scrollId), isscroll);
            isvalidate = false;
        }
    }

    function ScrollTo(e, isscroll) {
        if (!isscroll) {
            isscroll = true;
            $([document.documentElement, document.body]).animate({
                scrollTop: e.offset().top
            }, 500);
        }
    }
})