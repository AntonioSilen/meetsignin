﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.AuditLevel;
using MeetSignIn.Areas.Admin.ViewModels.Check;
using MeetSignIn.Areas.Admin.ViewModels.ClassActionPlan;
using MeetSignIn.Areas.Admin.ViewModels.ClassOpinion;
using MeetSignIn.Areas.Admin.ViewModels.ClassReport;
using MeetSignIn.Areas.Admin.ViewModels.Competency;
using MeetSignIn.Areas.Admin.ViewModels.DeptKPI;
using MeetSignIn.Areas.Admin.ViewModels.Draw;
using MeetSignIn.Areas.Admin.ViewModels.DrawAward;
using MeetSignIn.Areas.Admin.ViewModels.DrawAwardRelation;
using MeetSignIn.Areas.Admin.ViewModels.DrawPerson;
using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Areas.Admin.ViewModels.ExternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.FairlyPerson;
using MeetSignIn.Areas.Admin.ViewModels.InternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.KPI;
using MeetSignIn.Areas.Admin.ViewModels.Manager;
using MeetSignIn.Areas.Admin.ViewModels.Meeting;
using MeetSignIn.Areas.Admin.ViewModels.OptionGroup;
using MeetSignIn.Areas.Admin.ViewModels.Person;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPI;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPIState;
using MeetSignIn.Areas.Admin.ViewModels.PFAppraise;
using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Areas.Admin.ViewModels.PFSeason;
using MeetSignIn.Areas.Admin.ViewModels.PFWork;
using MeetSignIn.Areas.Admin.ViewModels.PFWorks;
using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Areas.Admin.ViewModels.QNRResult;
using MeetSignIn.Areas.Admin.ViewModels.ReviewReport;
using MeetSignIn.Areas.Admin.ViewModels.Room;
using MeetSignIn.Areas.Admin.ViewModels.SignIn;
using MeetSignIn.Areas.Admin.ViewModels.Theme;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Areas.Admin.ViewModels.Vision;
using MeetSignIn.Models;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.ViewModels.PF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                #region 後台            
                cfg.CreateMap<AuditLevel, AuditLevelView>();
                cfg.CreateMap<AuditLevelView, AuditLevel>();

                cfg.CreateMap<MainAdmin, ManagerView>();
                cfg.CreateMap<ManagerView, MainAdmin>();

                cfg.CreateMap<Meeting, MeetingView>();
                cfg.CreateMap<MeetingView, Meeting>();
                cfg.CreateMap<MeetingJoinSingInAndEmployeeAndDepartment, MeetingView>();
                cfg.CreateMap<MeetingView, MeetingJoinSingInAndEmployeeAndDepartment>();

                cfg.CreateMap<SignIn, ViewModels.SignInView>();
                cfg.CreateMap<ViewModels.SignInView, SignIn>();

                cfg.CreateMap<Room, RoomView>();
                cfg.CreateMap<RoomView, Room>();

                cfg.CreateMap<Employee, EmployeeView>();
                cfg.CreateMap<EmployeeView, Employee>();

                cfg.CreateMap<Department, DeptItem>();
                cfg.CreateMap<DeptItem, Department>();

                cfg.CreateMap<GroupJoinPersonAndDepartment, EmployeeView>();
                cfg.CreateMap<EmployeeView, GroupJoinPersonAndDepartment>();

                cfg.CreateMap<GroupJoinPersonAndDepartment, FairlyPersonView>();
                cfg.CreateMap<FairlyPersonView, GroupJoinPersonAndDepartment>();

                cfg.CreateMap<ClosingFile, FilesView>();
                cfg.CreateMap<FilesView, ClosingFile>();

                cfg.CreateMap<Training, TrainingView>();
                cfg.CreateMap<TrainingView, Training>();
                cfg.CreateMap<Training, SeminarTrainingView>();
                cfg.CreateMap<SeminarTrainingView, Training>();

                cfg.CreateMap<InternalPlan, InternalPlanView>();
                cfg.CreateMap<InternalPlanView, InternalPlan>();

                cfg.CreateMap<InternalCourse, InternalCourseView>();
                cfg.CreateMap<InternalCourseView, InternalCourse>();

                cfg.CreateMap<InternalAcceptance, InternalAcceptanceView>();
                cfg.CreateMap<InternalAcceptanceView, InternalAcceptance>();

                cfg.CreateMap<ExternalPlan, ExternalPlanView>();
                cfg.CreateMap<ExternalPlanView, ExternalPlan>();

                cfg.CreateMap<ExternalCourse, ExternalCourseView>();
                cfg.CreateMap<ExternalCourseView, ExternalCourse>();

                cfg.CreateMap<ExternalCourse, ExternalTrainingModel>();
                cfg.CreateMap<ExternalTrainingModel, ExternalCourse>();

                cfg.CreateMap<ExternalAcceptance, ExternalAcceptanceView>();
                cfg.CreateMap<ExternalAcceptanceView, ExternalAcceptance>();

                cfg.CreateMap<ClassActionPlan, ClassActionPlanView>();
                cfg.CreateMap<ClassActionPlanView, ClassActionPlan>();

                cfg.CreateMap<ClassOpinion, ClassOpinionView>();
                cfg.CreateMap<ClassOpinionView, ClassOpinion>();

                cfg.CreateMap<ClassReport, ClassReportView>();
                cfg.CreateMap<ClassReportView, ExternalCourse>();

                //cfg.CreateMap<PlanHolder, PlanHolderView>();
                //cfg.CreateMap<PlanHolderView, PlanHolder>();

                //cfg.CreateMap<FunctionDoc, FunctionDocView>();
                //cfg.CreateMap<FunctionDocView, FunctionDoc>();

                cfg.CreateMap<ReviewReport, ReviewReportView>();
                cfg.CreateMap<ReviewReportView, ReviewReport>();

                cfg.CreateMap<Draw, DrawView>();
                cfg.CreateMap<DrawView, Draw>();

                cfg.CreateMap<DrawAward, DrawAwardView>();
                cfg.CreateMap<DrawAwardView, DrawAward>();

                cfg.CreateMap<DrawAwardRelation, DrawAwardRelationView>();
                cfg.CreateMap<DrawAwardRelationView, DrawAwardRelation>();

                cfg.CreateMap<DrawPerson, DrawPersonView>();
                cfg.CreateMap<DrawPersonView, DrawPerson>();

                cfg.CreateMap<Competency, CompetencyView>();
                cfg.CreateMap<CompetencyView, Competency>();

                cfg.CreateMap<Theme, ThemeView>();
                cfg.CreateMap<ThemeView, Theme>();

                cfg.CreateMap<Person, PersonView>();
                cfg.CreateMap<PersonView, Person>();

                cfg.CreateMap<GroupJoinPersonAndDepartment, PersonInfo>();
                cfg.CreateMap<PersonInfo, GroupJoinPersonAndDepartment>();

                cfg.CreateMap<comGroupPerson, Areas.Admin.ViewModels.Temperature.TemperatureNotInView>();
                cfg.CreateMap<Areas.Admin.ViewModels.Temperature.TemperatureNotInView, comGroupPerson>();

                cfg.CreateMap<Temperature, MeetSignIn.Areas.Admin.ViewModels.Temperature.TemperatureView>();
                cfg.CreateMap<MeetSignIn.Areas.Admin.ViewModels.Temperature.TemperatureView, Temperature>();

                cfg.CreateMap<TrainingPerson, MeetSignIn.Areas.Admin.ViewModels.TrainingPerson.TrainingPersonView>();
                cfg.CreateMap<MeetSignIn.Areas.Admin.ViewModels.TrainingPerson.TrainingPersonView, TrainingPerson>();

                cfg.CreateMap<TrainingAbnormal, MeetSignIn.Areas.Admin.ViewModels.TrainingAbnormal.TrainingAbnormalView>();
                cfg.CreateMap<MeetSignIn.Areas.Admin.ViewModels.TrainingAbnormal.TrainingAbnormalView, TrainingAbnormal>();

                cfg.CreateMap<KPI, KPIView>();
                cfg.CreateMap<KPIView, KPI>();

                cfg.CreateMap<KPI, DeptKPIView>();
                cfg.CreateMap<DeptKPIView, KPI>();

                cfg.CreateMap<PersonKPI, PersonKPIView>();
                cfg.CreateMap<PersonKPIView, PersonKPI>();

                cfg.CreateMap<PersonKPIState, PersonKPIStateView>();
                cfg.CreateMap<PersonKPIStateView, PersonKPIState>();

                cfg.CreateMap<PersonKPI, CheckChildModel>();
                cfg.CreateMap<CheckChildModel, PersonKPI>();

                cfg.CreateMap<PersonKPIState, CheckChildModel>();
                cfg.CreateMap<CheckChildModel, PersonKPIState>();

                cfg.CreateMap<Check, CheckView>();
                cfg.CreateMap<CheckView, Check>();

                cfg.CreateMap<PFAppraise, PFAppraiseView>();
                cfg.CreateMap<PFAppraiseView, PFAppraise>();

                cfg.CreateMap<PFAppraiseWorks, PFAppraiseView>();
                cfg.CreateMap<PFAppraiseView, PFAppraiseWorks>();

                cfg.CreateMap<PFAppraise, FormView>();
                cfg.CreateMap<FormView, PFAppraise>();

                cfg.CreateMap<PFItmsModel, FormView>();
                cfg.CreateMap<FormView, PFItmsModel>();

                cfg.CreateMap<PFCategory, PFCategoryView>();
                cfg.CreateMap<PFCategoryView, PFCategory>();

                cfg.CreateMap<PFCategory, PFCategoryDeptView>();
                cfg.CreateMap<PFCategoryDeptView, PFCategory>();

                cfg.CreateMap<PFCategory, PFCategoryClsView>();
                cfg.CreateMap<PFCategoryClsView, PFCategory>();

                cfg.CreateMap<PFCategory, PFCategoryCateView>();
                cfg.CreateMap<PFCategoryCateView, PFCategory>();

                cfg.CreateMap<PFCategory, PFCategoryItmView>();
                cfg.CreateMap<PFCategoryItmView, PFCategory>();

                cfg.CreateMap<PFCategory, PFCategoryLvlView>();
                cfg.CreateMap<PFCategoryLvlView, PFCategory>();              

                cfg.CreateMap<PFWork, WorksView>();
                cfg.CreateMap<WorksView, PFWork>();       

                cfg.CreateMap<PFWorks, PFWorksView>();
                cfg.CreateMap<PFWorksView, PFWorks>();

                cfg.CreateMap<PFWork, PFWorkView>();
                cfg.CreateMap<PFWorkView, PFWork>();

                cfg.CreateMap<PFWorksRelation, PFWorksView>();
                cfg.CreateMap<PFWorksView, PFWorksRelation>();

                cfg.CreateMap<PFWork, PFWorksIndexView>();
                cfg.CreateMap<PFWorksIndexView, PFWork>();

                cfg.CreateMap<PFSeason, PFSeasonView>();
                cfg.CreateMap<PFSeasonView, PFSeason>();

                cfg.CreateMap<GroupJoinPersonAndDepartment, PersonView>();
                cfg.CreateMap<PersonView, GroupJoinPersonAndDepartment>();

                cfg.CreateMap<Vision, VisionView>();
                cfg.CreateMap<VisionView, Vision>();

                cfg.CreateMap<OptionGroup, OptionGroupView>();
                cfg.CreateMap<OptionGroupView, OptionGroup>();

                cfg.CreateMap<OptionItem, OptionItemView>();
                cfg.CreateMap<OptionItemView, OptionItem>();

                cfg.CreateMap<QNR, QNRView>();
                cfg.CreateMap<QNRView, QNR>();

                cfg.CreateMap<QNRQuestion, QNRQuestionView>();
                cfg.CreateMap<QNRQuestionView, QNRQuestion>();

                cfg.CreateMap<QNRQuestion, QuizQuestionView>();
                cfg.CreateMap<QuizQuestionView, QNRQuestion>();

                cfg.CreateMap<QNRResult, QNRResultView>();
                cfg.CreateMap<QNRResultView, QNRResult>();

                cfg.CreateMap<QNRResult, QNRPersonResultView>();
                cfg.CreateMap<QNRPersonResultView, QNRResult>();
                #endregion

                #region 前台
                cfg.CreateMap<Meeting, MeetSignIn.ViewModels.Meeting.MeetingView>();
                cfg.CreateMap<MeetSignIn.ViewModels.Meeting.MeetingView, Meeting>();

                cfg.CreateMap<Employee, MeetSignIn.ViewModels.Employee.EmployeeView>();
                cfg.CreateMap<MeetSignIn.ViewModels.Employee.EmployeeView, Employee>();

                cfg.CreateMap<GroupJoinPersonAndDepartment, MeetSignIn.ViewModels.Employee.EmployeeView>();
                cfg.CreateMap<MeetSignIn.ViewModels.Employee.EmployeeView, GroupJoinPersonAndDepartment>();

                cfg.CreateMap<Temperature, MeetSignIn.Areas.Health.ViewModels.TemperatureView>();
                cfg.CreateMap<MeetSignIn.Areas.Health.ViewModels.TemperatureView, Temperature>();
                #endregion
            });
        }
    }
}