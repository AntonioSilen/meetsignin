﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFAppraiseRelationRepository : GenericRepository<PFAppraiseRelation>
    {
        public PFAppraiseRelationRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFAppraiseRelation> Query(int appraiseId = 0, int appraiseWorksId = 0, int itmId = 0, int lvlId = 0, int workId = 0)
        {
            var query = GetAll();
            if (appraiseId != 0)
            {
                query = query.Where(q => q.AppraiseID == appraiseId);
            }
            if (appraiseWorksId != 0)
            {
                query = query.Where(q => q.AppraiseWorksID == appraiseWorksId);
            }
            if (itmId != 0)
            {
                query = query.Where(q => q.ItmID == itmId);
            }
            if (lvlId != 0)
            {
                query = query.Where(q => q.LvlID == lvlId);
            }
            if (workId != 0)
            {
                query = query.Where(q => q.WorksID == workId);
            }

            return query;
        }

        public PFAppraiseRelation GetFirstData(int appraiseId = 0, int itmId = 0, int lvlId = 0, int workId = 0)
        {
            var query = GetAll().Where(q => q.AppraiseID == appraiseId);
            if (itmId != 0)
            {
                query = query.Where(q => q.ItmID == itmId);
            }
            if (lvlId != 0)
            {
                query = query.Where(q => q.LvlID == lvlId);
            }
            if (workId != 0)
            {
                query = query.Where(q => q.WorksID == workId);
            }

            return query.FirstOrDefault();
        }

        public PFAppraiseRelation FindBy(int id)
        {
            return db.PFAppraiseRelation.Find(id);
        }
    }
}