﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class OptionItemRepository : GenericRepository<OptionItem>
    {
        public OptionItemRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<OptionItem> Query(bool? status = null, int groupId = 0, string title = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (groupId != 0)
            {
                query = query.Where(q => q.OptionGroupID == groupId);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            return query;
        }

        public OptionItem FindBy(int id)
        {
            return db.OptionItem.Find(id);
        }
    }
}