﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class TrainingImageRepository : GenericRepository<TrainingImage>
    {
        public TrainingImageRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<TrainingImage> Query(int tid = 0)
        {
            var query = GetAll();
            if (tid != 0)
            {
                query = query.Where(q => q.TrainingID == tid);
            }

            return query;
        }

        public TrainingImage FindBy(int id)
        {
            return db.TrainingImage.Find(id);
        }

        public void DeleteFile(int id)
        {
            TrainingImage data = FindBy(id);

            db.SaveChanges();
        }
        public string DeletePic(int id)
        {
            TrainingImage data = FindBy(id);
            string fileName = data.Pics;
            db.TrainingImage.Remove(data);

            db.SaveChanges();
            return fileName;
        }
    }
}