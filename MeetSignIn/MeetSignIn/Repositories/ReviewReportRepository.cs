﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class ReviewReportRepository : GenericRepository<ReviewReport>
    {
        public ReviewReportRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ReviewReport> Query(string title = "", int year = 0, int month = 0)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            if (year != 0)
            {
                query = query.Where(q => q.Year == year);
            }
            if (year != 0)
            {
                query = query.Where(q => q.Month == month);
            }
            return query;
        }

        public ReviewReport FindBy(int id)
        {
            return db.ReviewReport.Find(id);
        }

        public string DeleteReport(int id)
        {
            ReviewReport data = FindBy(id);
            string fileName = data.FileName;
            data.FileName = "";

            db.SaveChanges();
            return fileName;
        }
    }
}