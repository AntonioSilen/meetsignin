﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFCategoryRepository : GenericRepository<PFCategory>
    {
        public PFCategoryRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFItmsModel> GetItmsQuery(string DeptId, int ClsID, int CateID = 0, int type = 0)
        {
            var query = from itm in db.PFCategory
                        join cate in db.PFCategory.Where(q => q.ParentID == ClsID && q.IsOnline)
                        on itm.ParentID equals cate.ID                       
                        //where p.ServiceStatus != "40"
                        select new PFItmsModel()
                        {
                            ID = itm.ID,
                            Title = itm.Title,
                            Sort = itm.Sort,
                            Type = itm.Type,
                            DeptId = itm.DeptId,
                            ClassID = cate.ParentID,
                            CateID = cate.ID,
                            CateTitle = cate.Title,
                            CateSort = cate.Sort,                           
                        };
            var list = query.ToList();
            if (CateID != 0)
            {
                query = query.Where(q => q.CateID == CateID);
            }
            list = query.ToList();
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            list = query.ToList();

            return query.OrderBy(q => q.CateSort).ThenBy(q => q.Sort).AsQueryable();
        }

        public IQueryable<PFCategory> Query(bool? isOnline = null, string deptId = "", int parentID = 0, string levelType = "", bool? isRequired = null, int type = 0)
        {
            var query = GetAll();
            if (isOnline.HasValue)
            {
                query = query.Where(q => q.IsOnline == isOnline);
            }
            if (isRequired.HasValue)
            {
                query = query.Where(q => q.IsRequired == isRequired);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (!string.IsNullOrEmpty(levelType))
            {
                query = query.Where(q => q.LevelType == levelType);
            }
            if (parentID != 0)
            {
                query = query.Where(q => q.ParentID == parentID);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            return query;
        }

        public PFCategory FindBy(int id)
        {
            return db.PFCategory.Find(id);
        }
    }

    public class PFItmsModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Sort { get; set; }
        public int Type { get; set; }
        public string DeptId { get; set; }
        public int ClassID { get; set; }
        public int CateID { get; set; }
        public string CateTitle { get; set; }
        public int CateSort { get; set; }
    }
}