﻿using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class TrainingRepository : GenericRepository<Training>
    {
        public TrainingRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());

        public IQueryable<Training> ParticipateQuery(int trainingId = 0, string title = "", string personId = "")
        {
            //var personTrainingIdList = db.Training.Where(q => q.GroupType == 2).Select(q => q.ID).ToList();
            var query = GetAll().Where(q => q.IsOnline && q.GroupType == (int)GroupType.Person);
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }
            int signedStatus = (int)SignStatus.Signed;
            var IdList = trainingPersonRepository.Query(null, "", trainingId, personId, "").Where(q => q.SignStatus == signedStatus).Select(q => q.TraningID).ToList();
            var parentIdList = GetAll().Where(q => IdList.Contains(q.ID) && q.SeriesParentID != 0).Select(q => q.SeriesParentID).ToList();
            query = query.Where(q => IdList.Contains(q.ID) || parentIdList.Contains(q.ID));

            return query;
        }

        public IQueryable<Training> Query(string searchValue, DateTime start, DateTime end, 
            string DeptID = "", string DepartmentStr = ""
            , int factory = 0, int room = 0, int exceptMid = 0, int? parentId = null, 
            bool? IsClose = null, int category = 0, bool modelState = false, int theme = 0)
        {
            var query = GetAll().Where(q => q.IsOnline);
            if (modelState)
            {
                query = query.Where(q => q.IsModel == true);
            }
            if (parentId.HasValue)
            {
                int parentIdInt = Convert.ToInt32(parentId);
                query = query.Where(q => q.SeriesParentID == parentIdInt);
            }
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(q => q.Title.Contains(searchValue) || q.Organiser.Contains(searchValue));
            }
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }
            if (room != 0)
            {
                query = query.Where(q => q.RoomID == room);
            }
            if (start > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.StartDate >= start);
            }
            if (end > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.EndDate <= end);
            }
            if (!string.IsNullOrEmpty(DeptID) && DeptID != "0001")
            {
                query = query.Where(q => q.DeptID == DeptID);
            }
            if (!string.IsNullOrEmpty(DepartmentStr))
            {
                var trainingIdList = trainingPersonRepository.Query(true, "", 0, "", DepartmentStr).Select(q => q.TraningID).Distinct().ToList();
                if (trainingIdList.Count() > 0)
                {
                    query = query.Where(q => trainingIdList.Contains(q.ID));
                }
            }
            if (exceptMid != 0)
            {
                query = query.Where(q => q.ID != exceptMid);
            }
            if (IsClose.HasValue)
            {
                query = query.Where(q => q.IsClose == IsClose && q.SeriesParentID == 0);
            }
            if (category != 0)
            {
                query = query.Where(q => q.Category == category);
            }
            if (theme != 0)
            {
                query = query.Where(q => q.Theme == theme);
            }

            return query;
        }

        public Training FindBy(int id)
        {
            return db.Training.Find(id);
        }

        public List<NumAndPercentageModel> GetNumAndPercentage(DateTime start, DateTime end, string deptId = "", string department = "", string type = "", bool modelState = false)
        {
            List<NumAndPercentageModel> data = new List<NumAndPercentageModel>();
            if (type != "Fee")
            {
                var query = GetAll().Where(q => q.IsOnline);
                if (modelState)
                {
                    query = query.Where(q => q.IsModel == true);
                }
                if (!string.IsNullOrEmpty(deptId))
                {
                    query = query.Where(q => q.DeptID == deptId);
                }
                if (!string.IsNullOrEmpty(department))
                {
                    //var trainingIdList = trainingPersonRepository.GetSignInInfoList(0, "", department).Select(q => q.TrainingID).Distinct().ToList();
                    var trainingIdList = trainingPersonRepository.Query(true, "", 0, "", department).Select(q => q.TraningID).Distinct().ToList();
                    if (trainingIdList.Count() > 0)
                    {
                        query = query.Where(q => trainingIdList.Contains(q.ID));
                    }
                }
                if (start >= Convert.ToDateTime("2000/01/01") && end >= Convert.ToDateTime("2000/01/01"))
                {
                    query = query.Where(q => q.StartDate >= start && q.EndDate <= end);
                }
                
                switch (type)
                {
                    case "Category":
                        return query.GroupBy(d => d.Category)
                              .SelectMany(grp => grp
                              .Select(row => new NumAndPercentageModel()
                              {
                                  Title = row.Category.ToString(),
                                  Num = grp.Count(),
                                  CateCount = grp.Count(),
                                  AllCount = query.Count()
                              })
                          ).Distinct().ToList();
                    case "Theme":
                        return query.GroupBy(d => d.Theme)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.Theme.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                    //case "Fee":
                    //    return query.GroupBy(d => d.Fee)
                    //       .SelectMany(grp => grp
                    //       .Select(row => new NumAndPercentageModel()
                    //       {
                    //           Title = row.Fee.ToString(),
                    //           Num = grp.Count(),
                    //           CateCount = grp.Count(),
                    //           AllCount = query.Count()
                    //       })
                    //   ).Distinct().ToList();
                    case "Hours":
                        return query.GroupBy(d => d.Hours)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.Hours.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                    //case "Exacutive":
                    //    return query.GroupBy(d => d.Theme)
                    //        .SelectMany(grp => grp
                    //        .Select(row => new NumAndPercentageModel()
                    //        {
                    //            Title = row.Theme.ToString(),
                    //            Num = grp.Count(),
                    //            CateCount = grp.Count(),
                    //            AllCount = query.Count()
                    //        })
                    //    ).Distinct().ToList();
                }
            }
            else
            {
                //取得費用統計資料
                var query = GetAll().Where(q => q.IsOnline);
                if (!string.IsNullOrEmpty(deptId))
                {
                    query = query.Where(q => q.DeptID == deptId);
                }
                if (!string.IsNullOrEmpty(department))
                {
                    var trainingIdList = trainingPersonRepository.Query(true, "", 0, "", department).Select(q => q.TraningID).Distinct().ToList();
                    if (trainingIdList.Count() > 0)
                    {
                        query = query.Where(q => trainingIdList.Contains(q.ID));
                    }
                }
                if (start >= Convert.ToDateTime("2000/01/01") && end >= Convert.ToDateTime("2000/01/01"))
                {
                    query = query.Where(q => q.StartDate >= start && q.EndDate <= end);
                }
                return query.GroupBy(d => d.Fee)
                   .SelectMany(grp => grp
                   .Select(row => new NumAndPercentageModel()
                   {
                       Title = row.Fee.ToString(),
                       Num = grp.Count(),
                       CateCount = grp.Count(),
                       AllCount = query.Count()
                   })
               ).Distinct().ToList();
                                
                //var externalList = db.ExternalPlan.Where(q => q.IsAudit).ToList();
                //foreach (var item in externalList)
                //{
                //    data.Add(new NumAndPercentageModel()
                //    {
                //        Title = row.Hours.ToString(),
                //        Num = grp.Count(),
                //        CateCount = grp.Count(),
                //        AllCount = query.Count()
                //    });
                //}
                //var internalList = db.InternalPlan.Where(q => q.IsAudit).ToList();
                //foreach (var item in internalList)
                //{

                //}
                //var seminarList = db.SeminarPlan.Where(q => q.IsAudit).ToList();
                //foreach (var item in seminarList)
                //{

                //}
            }
            return data;
        }

        /// <summary>
        /// 出席率資料
        /// </summary>
        /// <param name="deptId"></param>
        /// <param name="department"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public IQueryable<TrainingExacutiveModel> GetExacutiveList(string deptId, string department, DateTime startDate, DateTime endDate)
        {
            var query = from t in db.Training                           
                            where t.IsOnline == true
                            && t.StartDate >= startDate && t.StartDate <= endDate

                            select new TrainingExacutiveModel()
                            {//???
                                ID = t.ID,
                                Title = t.Title,
                                StartDate = t.StartDate,
                                EndDate = t.EndDate,
                                TotalNum = db.TrainingPerson.Count(),
                                SignedNum = db.TrainingPerson.Where(q => q.TraningID == t.ID && q.SignStatus == 1).Count(),
                                //LeaveNum = t,
                                //UnSignNum = t,
                                //OpinionFilledNum = t,
                                //AccptanceFilledNum = t,
                                //QuizCompleteNum = t,
                            };
            //if (!string.IsNullOrEmpty(deptId))
            //{
            //    query = query.Where(q => q.DeptID == deptId);
            //}
            if (!string.IsNullOrEmpty(department))
            {
                var trainingIdList = trainingPersonRepository.Query(true, "", 0, "", department).Select(q => q.TraningID).Distinct().ToList();
                if (trainingIdList.Count() > 0)
                {
                    query = query.Where(q => trainingIdList.Contains(q.ID));
                }
            }
            if (startDate >= Convert.ToDateTime("2000/01/01") && endDate >= Convert.ToDateTime("2000/01/01"))
            {
                query = query.Where(q => q.StartDate >= startDate && q.EndDate <= endDate);
            }

            return query;
        }

        public List<NumAndPercentageModel> GetPDDRONumAndPercentage(int id, string type = "")
        {
            var query = db.ClassOpinion.Where(q => q.TrainingID == id);
            List<NumAndPercentageModel> data = new List<NumAndPercentageModel>();
            switch (type)
            {
                case "Compatibility":
                    return query.GroupBy(d => d.Compatibility)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.Compatibility.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                case "Architecture":
                    return query.GroupBy(d => d.Architecture)
                              .SelectMany(grp => grp
                              .Select(row => new NumAndPercentageModel()
                              {
                                  Title = row.Architecture.ToString(),
                                  Num = grp.Count(),
                                  CateCount = grp.Count(),
                                  AllCount = query.Count()
                              })
                          ).Distinct().ToList();
                case "Schedule":
                    return query.GroupBy(d => d.Schedule)
                             .SelectMany(grp => grp
                             .Select(row => new NumAndPercentageModel()
                             {
                                 Title = row.Schedule.ToString(),
                                 Num = grp.Count(),
                                 CateCount = grp.Count(),
                                 AllCount = query.Count()
                             })
                         ).Distinct().ToList();
                case "CourseMaterials":
                    return query.GroupBy(d => d.CourseMaterials)
                           .SelectMany(grp => grp
                           .Select(row => new NumAndPercentageModel()
                           {
                               Title = row.CourseMaterials.ToString(),
                               Num = grp.Count(),
                               CateCount = grp.Count(),
                               AllCount = query.Count()
                           })
                       ).Distinct().ToList();
                case "Device":
                    return query.GroupBy(d => d.Device)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.Device.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                case "Professional":
                    return query.GroupBy(d => d.Professional)
                             .SelectMany(grp => grp
                             .Select(row => new NumAndPercentageModel()
                             {
                                 Title = row.Professional.ToString(),
                                 Num = grp.Count(),
                                 CateCount = grp.Count(),
                                 AllCount = query.Count()
                             })
                         ).Distinct().ToList();
                case "Expression":
                    return query.GroupBy(d => d.Expression)
                             .SelectMany(grp => grp
                             .Select(row => new NumAndPercentageModel()
                             {
                                 Title = row.Expression.ToString(),
                                 Num = grp.Count(),
                                 CateCount = grp.Count(),
                                 AllCount = query.Count()
                             })
                         ).Distinct().ToList();
                case "TimeControl":
                    return query.GroupBy(d => d.TimeControl)
                             .SelectMany(grp => grp
                             .Select(row => new NumAndPercentageModel()
                             {
                                 Title = row.TimeControl.ToString(),
                                 Num = grp.Count(),
                                 CateCount = grp.Count(),
                                 AllCount = query.Count()
                             })
                         ).Distinct().ToList();
                case "TeachingMethod":
                    return query.GroupBy(d => d.TeachingMethod)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.TeachingMethod.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                case "Recommended":
                    return query.GroupBy(d => d.Recommended)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.Recommended.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                case "ActualUsage":
                    return query.GroupBy(d => d.ActualUsage)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.ActualUsage.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                case "CurrentUsage":
                    return query.GroupBy(d => d.CurrentUsage)
                            .SelectMany(grp => grp
                            .Select(row => new NumAndPercentageModel()
                            {
                                Title = row.CurrentUsage.ToString(),
                                Num = grp.Count(),
                                CateCount = grp.Count(),
                                AllCount = query.Count()
                            })
                        ).Distinct().ToList();
                case "FutureUsage":
                    return query.GroupBy(d => d.FutureUsage)
                             .SelectMany(grp => grp
                             .Select(row => new NumAndPercentageModel()
                             {
                                 Title = row.FutureUsage.ToString(),
                                 Num = grp.Count(),
                                 CateCount = grp.Count(),
                                 AllCount = query.Count()
                             })
                         ).Distinct().ToList();
                case "Difficulty":
                    return query.GroupBy(d => d.Difficulty)
                           .SelectMany(grp => grp
                           .Select(row => new NumAndPercentageModel()
                           {
                               Title = row.Difficulty.ToString(),
                               Num = grp.Count(),
                               CateCount = grp.Count(),
                               AllCount = query.Count()
                           })
                       ).Distinct().ToList();
            }


            return data;
        }

        public TrainingFilterModel GetTrainingByNum(string roomNum, int tid = 0)
        {
            try
            {
                DateTime now = DateTime.Now;
                DateTime last = Convert.ToDateTime(now.ToShortDateString() + " 23:59:59");
                var roomInfo = db.Room.Where(q => q.RoomNumber == roomNum).FirstOrDefault();
                var trainingList = GetTrainingFilterList().Where(q => q.RoomID == roomInfo.ID && q.SignDate <= now && q.LastSignDate >= now).OrderBy(q => q.StartDate).ThenBy(q => q.ID);
                if (tid != 0)
                {
                    return trainingList.Where(q => q.ID == tid).FirstOrDefault();
                }

                return trainingList.FirstOrDefault();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string DeletePic(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainPic;
            data.MainPic = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteFile(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainFile;
            data.MainFile = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteVideo(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainVideo;
            data.MainVideo = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteReport(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainVideo;
            data.MainReport = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteAcceptance(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainVideo;
            data.MainAcceptance = "";

            db.SaveChanges();
            return fileName;
        }

        public List<TrainingFilterModel> GetTrainingFilterList()
        {
            var query = from t in db.Training
                        where t.IsSignable == true
                        && t.IsOnline == true
                        select new TrainingFilterModel()
                        {
                            ID = t.ID,
                            RoomID = t.RoomID,
                            SignDate = t.SignDate,
                            StartDate = t.StartDate,
                            LateMinutes = t.LateMinutes,
                            LastSignDate = t.StartDate,
                            EndDate = t.EndDate,
                        };
            var resultList = query.ToList();
            foreach (var item in resultList)
            {
                item.LastSignDate = item.StartDate.AddMinutes(item.LateMinutes);
            }

            return resultList;
        }

        public class TrainingFilterModel
        {
            public int ID { get; set; }
            public int RoomID { get; set; }
            public DateTime SignDate { get; set; }
            public DateTime StartDate { get; set; }
            public int LateMinutes { get; set; }
            public DateTime LastSignDate { get; set; }
            public DateTime EndDate { get; set; }
        }

        //public class NumAndPercentageModel
        //{
        //    public string Title { get; set; }
        //    public double DoubleTitle
        //    {
        //        get
        //        {
        //            try
        //            {
        //                return Convert.ToDouble(this.Title);
        //            }
        //            catch (Exception)
        //            {
        //                return 0;
        //            }
        //        }
        //    }
        //    public int Num { get; set; }

        //    public int AllCount { get; set; }
        //    public int CateCount { get; set; }

        //    public string Percentage
        //    {
        //        get
        //        {
        //            return (100.0 * this.CateCount / this.AllCount).ToString("0.00");
        //        }
        //    }
        //}
    }
}