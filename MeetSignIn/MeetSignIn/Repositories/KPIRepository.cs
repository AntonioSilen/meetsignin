﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class KPIRepository : GenericRepository<KPI>
    {
        public KPIRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<KPI> Query(bool? status, string title, int type = 0, int level = 0, int parentKPI = 0, string deptId = "", bool? isApproved = null, int? approvelState = null, List<string> deptList = null)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (isApproved.HasValue)
            {
                query = query.Where(q => q.ApprovalState == (int)ApprovalState.Approved);
            }
            if (approvelState.HasValue)
            {
                query = query.Where(q => q.ApprovalState == (int)ApprovalState.Approved);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (level != 0)
            {
                query = query.Where(q => q.Level == level);
            }
            if (parentKPI != 0)
            {
                query = query.Where(q => q.ParentKPIID == parentKPI);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                //判斷部門
                var kpiIdList = db.KPIRelation.Where(q => q.DeptId == deptId).Select(q => q.KPIID).ToList();
                query = query.Where(q => kpiIdList.Contains(q.ID));
            }
            if (deptList != null && deptList.Count() > 0)
            {
                //判斷部門
                var kpiIdList = db.KPIRelation.Where(q => deptList.Contains(q.DeptId)).Select(q => q.KPIID).ToList();
                query = query.Where(q => kpiIdList.Contains(q.ID));
            }


            return query;
        }

        public KPI GetByTitle(string title)
        {
            return GetAll().Where(q => q.Title == title).FirstOrDefault();
        }
    }
}