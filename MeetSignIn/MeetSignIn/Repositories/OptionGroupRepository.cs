﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class OptionGroupRepository : GenericRepository<OptionGroup>
    {
        public OptionGroupRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<OptionGroup> Query(string title = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            return query;
        }

        public OptionGroup FindBy(int id)
        {
            return db.OptionGroup.Find(id);
        }
    }
}