﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFWorksRelationRepository : GenericRepository<PFWorksRelation>
    {
        public PFWorksRelationRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFWorksRelation> Query(bool? status = null, int classId = 0, int workId = 0, int itemId = 0, string deptId = "", string title = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            if (workId != 0)
            {
                query = query.Where(q => q.WorkID == workId);
            }
            if (itemId != 0)
            {
                query = query.Where(q => q.ItemID == itemId);
            }
            return query;
        }

        public PFWorksRelation FindBy(int id)
        {
            return db.PFWorksRelation.Find(id);
        }
    }
}