﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class AuditLevelRepository : GenericRepository<AuditLevel>
    {
        public AuditLevelRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<AuditLevel> Query(bool? status = null, string principal = "", string president = "", string vice = "", string manager = "", string classsupervisor = "", string teamleader = "", string hr = "", string deptId = "")
        {
            var query = GetAll();
            //if (!string.IsNullOrEmpty(principal))
            //{
            //    query = query.Where(q => q.PrincipalReview == principal);
            //}
            if (!string.IsNullOrEmpty(president))
            {
                query = query.Where(q => q.PresidentReview == president);
            }
            if (!string.IsNullOrEmpty(vice))
            {
                query = query.Where(q => q.ViceReview == vice);
            }
            if (!string.IsNullOrEmpty(principal))
            {
                query = query.Where(q => q.PrincipalReview == principal);
            }

            if (!string.IsNullOrEmpty(manager))
            {
                query = query.Where(q => q.ManagerReview == manager);
            }

            if (!string.IsNullOrEmpty(classsupervisor))
            {
                query = query.Where(q => q.CSReview.Contains(classsupervisor));
            }
            if (!string.IsNullOrEmpty(teamleader))
            {
                query = query.Where(q => q.TLReview.Contains(teamleader));
            }

            if (!string.IsNullOrEmpty(hr))
            {
                query = query.Where(q => q.HRReview == hr);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId.Contains(deptId) || q.DeptName.Contains(deptId));
            }
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            return query;
        }

        public AuditLevel FindBy(int id)
        {
            return db.AuditLevel.Find(id);
        }

        public AuditLevel FindByDept(string deptId)
        {
            return db.AuditLevel.Where(q => q.DeptId == deptId).FirstOrDefault();
        }
    }
}