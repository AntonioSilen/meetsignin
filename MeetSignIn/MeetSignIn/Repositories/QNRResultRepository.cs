﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class QNRResultRepository : GenericRepository<QNRResult>
    {
        public QNRResultRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<QNRResult> Query(int qnrId = 0, int qnrQuestionId = 0, string personId = "", string deptId = "", int answerType = 0, bool getLastVersion = false)
        {
            var query = GetAll();
            if (qnrId != 0)
            {
                query = query.Where(q => q.QNRID == qnrId);
            }
            if (qnrQuestionId != 0)
            {
                query = query.Where(q => q.QNRQuestionID == qnrQuestionId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId == personId);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (answerType != 0)
            {
                query = query.Where(q => q.AnswerType == answerType);
            }

            if (getLastVersion)
            {
                var firstQuery = query.FirstOrDefault();
                int existVersion = query.Where(q => q.QNRQuestionID == firstQuery.QNRQuestionID).Count();
                query = query.Where(q => q.Version == existVersion);
            }

            return query;
        }

        public QNRResult FindBy(int id)
        {
            return db.QNRResult.Find(id);
        }
    }
}