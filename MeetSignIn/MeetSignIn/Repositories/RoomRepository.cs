﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class RoomRepository : GenericRepository<Room>
    {
        public RoomRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Room> Query(int factory = 0, string roomNumber = "", bool? status = null)
        {
            var query = GetAll();
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }
            if (!string.IsNullOrEmpty(roomNumber))
            {
                query = query.Where(q => q.RoomNumber.Contains(roomNumber));
            }
            if (status.HasValue)
            {
                query = query.Where(q => q.Status == status);
            }
            return query;
        }

        public Room FindBy(int id)
        {
            return db.Room.Find(id);
        }

        public string GetRoomString(int id)
        {
            var query = db.Room.Find(id);
            var factory = EnumHelper.GetDescription((Factory)query.FactoryID);
            return "(" + query.RoomNumber + ")" + factory + " " + query.Place;
        }
    }
}