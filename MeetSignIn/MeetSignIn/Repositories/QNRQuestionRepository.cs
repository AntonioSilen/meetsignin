﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class QNRQuestionRepository : GenericRepository<QNRQuestion>
    {
        public QNRQuestionRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<QNRQuestion> Query(bool? isRequired = null, int qnrId = 0, string question = "", int answerType = 0)
        {
            var query = GetAll();
            if (isRequired.HasValue)
            {
                query = query.Where(q => q.IsRequired == isRequired);
            }
            if (!string.IsNullOrEmpty(question))
            {
                query = query.Where(q => q.Question == question);
            }
            if (qnrId != 0)
            {
                query = query.Where(q => q.QNRID == qnrId);
            }
            if (answerType != 0)
            {
                query = query.Where(q => q.AnswerType == answerType);
            }
            return query;
        }

        public QNRQuestion FindBy(int id)
        {
            return db.QNRQuestion.Find(id);
        }

        public QNRQuestion FindLinkedQuestion(int qId, int id)
        {
            string triggerId = id.ToString();
            return db.QNRQuestion.Where(q => q.TriggerAnswer == triggerId && q.LinkQNRQID == qId).FirstOrDefault();
        }
    }
}