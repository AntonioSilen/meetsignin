﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PlanHolderRepository : GenericRepository<PlanHolder>
    {
        public PlanHolderRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<PlanHolder> Query(int tid = 0)
        {
            var query = GetAll();
            if (tid != 0)
            {
                query = query.Where(q => q.TrainingID == tid);
            }

            return query;
        }

        public PlanHolder FindBy(int id)
        {
            return db.PlanHolder.Find(id);
        }

        public string DeleteFile(int id)
        {
            PlanHolder data = FindBy(id);
            string fileName = data.FileName;
            db.PlanHolder.Remove(data);

            db.SaveChanges();
            return fileName;
        }
    }
}