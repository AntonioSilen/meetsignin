﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFSeasonRepository : GenericRepository<PFSeason>
    {
        public PFSeasonRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFSeason> Query(bool? status = null, int classId = 0, string deptId = "", string title = "", bool? actived = null)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            if (actived.HasValue)
            {
                DateTime now = DateTime.Now;
                if (actived == true)
                {
                    query = query.Where(q => q.StartDate <= now && now < q.EndDate);
                }
                else
                {
                    query = query.Where(q => q.StartDate > now || now >= q.EndDate);
                }
            }

            return query;
        }

        public PFSeason FindBy(int id)
        {
            return db.PFSeason.Find(id);
        }
    }
}