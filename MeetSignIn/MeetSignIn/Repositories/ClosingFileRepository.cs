﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class ClosingFileRepository : GenericRepository<ClosingFile>
    {
        public ClosingFileRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<ClosingFile> Query(int tid = 0, string indexItem = "", string fileNameType = "")
        {
            var query = GetAll();
            if (tid != 0)
            {
                query = query.Where(q => q.TrainingID == tid);
            }
            if (!string.IsNullOrEmpty(indexItem))
            {
                query = query.Where(q => q.IndexItem == indexItem);
            }
            if (!string.IsNullOrEmpty(fileNameType))
            {
                query = query.Where(q => q.FileNameType == fileNameType);
            }

            return query;
        }

        public ClosingFile FindFile(int tid, string indexItem, string fileNameType)
        {
            return GetAll().Where(q => q.TrainingID == tid && q.IndexItem == indexItem && q.FileNameType == fileNameType).FirstOrDefault();
        }

        public ClosingFile FindBy(int id)
        {
            return db.ClosingFile.Find(id);
        }

        public void DeleteFile(int id)
        {
            ClosingFile data = FindBy(id);

            db.SaveChanges();
        }
    }
}