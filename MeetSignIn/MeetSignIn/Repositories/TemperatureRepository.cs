﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class TemperatureRepository : GenericRepository<Temperature>
    {
        public TemperatureRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Temperature> Query(string personId, bool? isAbnormal, DateTime? date)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            if (isAbnormal.HasValue)
            {
                query = query.Where(q => q.IsAbnormal == isAbnormal);
            }
            if (date.HasValue)
            {
                var aaa = query.ToList();
                string dateStr = Convert.ToDateTime(date).ToString("yyyy-MM-dd");
                var start = Convert.ToDateTime(dateStr + " 00:00:00");
                var end = Convert.ToDateTime(dateStr + " 23:59:59");
                query = query.Where(q => q.CreateDate >= start && q.CreateDate <= end);
                aaa = query.ToList();
            }
            return query;
        }

        public Temperature FindBy(int id)
        {
            return db.Temperature.Find(id);
        }
    }
}