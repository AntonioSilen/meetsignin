﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class KPIRelationRepository : GenericRepository<KPIRelation>
    {
        public KPIRelationRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<KPIRelation> Query(int kpiId = 0, string deptId = "", string deptName = "", string personId = "", string personName = "")
        {
            var query = GetAll();
            if (kpiId != 0)
            {
                query = query.Where(q => q.KPIID == kpiId);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(p => p.DeptId == deptId);
            }
            if (!string.IsNullOrEmpty(deptName))
            {
                query = query.Where(p => p.DeptName == deptName);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(p => p.PersonId == personId);
            }
            if (!string.IsNullOrEmpty(personName))
            {
                query = query.Where(p => p.PersonName == personName);
            }

            return query;
        }       
    }
}