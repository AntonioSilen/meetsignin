﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class TrainingPersonRepository : GenericRepository<TrainingPerson>
    {
        public TrainingPersonRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<TrainPersonJoinPersonAndDepartment> GetSignInInfoList(int tid = 0, string jid = "", string DeptID = "")
        {
            try
            {
                var query = GetAll();
                if (tid != 0)
                {
                    query = query.Where(p => p.TraningID == tid);
                }
                if (!string.IsNullOrEmpty(jid))
                {
                    //【JobID】等於T8資料庫的【PersonId】
                    query = query.Where(q => q.JobID == jid);
                }
                var notifyViewList = Mapper.Map<List<Areas.Admin.ViewModels.TrainingPerson.TrainingPersonView>>(query).AsQueryable();
                var resultList = new List<TrainPersonJoinPersonAndDepartment>();
                if (!string.IsNullOrEmpty(DeptID))
                {
                    notifyViewList = notifyViewList.Where(q => q.DeptID == DeptID);
                }
                foreach (var item in notifyViewList)
                {
                    var joinQuery = from p in t8db.comPerson
                            join g in t8db.comGroupPerson
                            on p.PersonId equals g.PersonId
                            join d in t8db.comDepartment
                            on p.DeptId equals d.DeptId
                            where g.PersonId == item.JobID
                            select new TrainPersonJoinPersonAndDepartment()
                            {
                                ID = item.ID,
                                TrainingID = item.TraningID,
                                DeptID = d.DeptId,
                                DeptName = d.DeptName,
                                //ParentDeptID = d.ParentDeptId,
                                PersonId = g.PersonId,
                                SignStatus = item.SignStatus,
                                SignDate = item.SignTime,
                                IsPassed = item.IsPassed,
                                Name = g.PersonName,
                                EngName = g.EngName,
                                Email = g.EMail
                                //InductionDate = p.InductionDate,
                                //Birthday = g.Birthday,
                                //IsSign = item.IsSign,
                            };
                    resultList.Add(joinQuery.FirstOrDefault());
                }
             
                return resultList.AsQueryable();
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IQueryable<TrainingStatisticsJoinModel> GetExacutiveRateList(string deptId, string department, DateTime startDate, DateTime endDate)
        {
            try
            {
                /*
                select t.ID, t.Title, t.Lecturer, t.StartDate, (tp.Name + tp.JobID) as Person, 
                case 
                when tp.SignStatus  = 0 then '未到'
                when tp.SignStatus  = 1 then '已簽到'
                when tp.SignStatus  = 2 then '請假'
                end,
                (select top(1)(Compatibility + Architecture + Schedule + 
                CourseMaterials + Device + Professional + 
                Expression + TimeControl + TeachingMethod + 
                Recommended + ActualUsage + CurrentUsage + 
                FutureUsage - 13) from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID),
                (select top(1)(Compatibility + Architecture + Schedule + CourseMaterials + Device - 5) 
                from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID),
                (select top(1)(Professional + Expression + TimeControl + TeachingMethod + Recommended - 5) 
                from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID),
                (select top(1)(ActualUsage + CurrentUsage + FutureUsage - 3) 
                from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID),
                case 
	                when(select top(1) Difficulty from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID) = 1 then '有點難'
	                when(select top(1) Difficulty from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID) = 2 then '適中'
	                when(select top(1) Difficulty from ClassOpinion where TrainingID = t.ID and PersonID = tp.JobID) = 3 then '有點簡單'
                else null
                end
                from TrainingPerson as tp
                join Training as t
                on tp.TraningID = t.ID
                where t.IsOnline = 1 and t.StartDate >= '2022/01/01' and t.EndDate < '2025/01/01'
                and (t.SeriesParentID = 0 or (t.SeriesParentID in (select ID from Training where IsOnline = 1)
                and (t.TrainingCourseID in (select ID from InternalCourse) or t.TrainingCourseID in (select ID from ExternalCourse)))
                )
                order by t.ID
                */
                var joinQuery = from tp in db.TrainingPerson
                    join t in db.Training
                    on tp.TraningID equals t.ID
                    where t.IsOnline == true 
                    && t.StartDate >= startDate && t.StartDate <= endDate

                    select new TrainingStatisticsJoinModel()
                    {
                        ID = t.ID,
                        Title = t.Title,
                        Lecturer = t.Lecturer,
                        StartDate = t.StartDate,
                        EndDate = t.EndDate,
                        DeptID = t.DeptID,
                        Person = tp.Name + tp.JobID,
                        PersonId = tp.JobID,
                        SignStatus = tp.SignStatus,
                    };
                if (!string.IsNullOrEmpty(deptId))
                {
                    joinQuery = joinQuery.Where(q => q.DeptID == deptId);
                }
                if (!string.IsNullOrEmpty(department))
                {
                    //var trainingIdList = trainingPersonRepository.GetSignInInfoList(0, "", department).Select(q => q.TrainingID).Distinct().ToList();
                    var trainingIdList = Query(true, "", 0, "", department).Select(q => q.TraningID).Distinct().ToList();
                    if (trainingIdList.Count() > 0)
                    {
                        joinQuery = joinQuery.Where(q => trainingIdList.Contains(q.ID));
                    }
                }
                if (startDate >= Convert.ToDateTime("2000/01/01") && endDate >= Convert.ToDateTime("2000/01/01"))
                {
                    joinQuery = joinQuery.Where(q => q.StartDate >= startDate && q.EndDate <= endDate);
                }
                var tempList = joinQuery.ToList();
                foreach (var item in tempList)
                {
                    var opinionData = db.ClassOpinion.Where(q => q.TrainingID == item.ID && q.PersonID == item.PersonId).FirstOrDefault();
                    if (opinionData != null)
                    {
                        item.CoursePoints = opinionData.Compatibility + opinionData.Architecture + opinionData.Schedule + opinionData.CourseMaterials + opinionData.Device - 5;
                        item.LecturerPoints = opinionData.Professional + opinionData.Expression + opinionData.TimeControl + opinionData.TeachingMethod + opinionData.Recommended - 5;
                        item.RewordPoints = opinionData.ActualUsage + opinionData.CurrentUsage + opinionData.FutureUsage - 3;

                        item.OpinionPoints = item.CoursePoints + item.LecturerPoints + item.RewordPoints;

                        switch (opinionData.Difficulty)
                        {
                            case 1:
                                item.Difficulty = "有點簡單";
                                break;
                            case 2:
                                item.Difficulty = "適中";
                                break;
                            case 3:
                                item.Difficulty = "有點難";
                                break;
                            default:
                                item.Difficulty = "適中";
                                break;
                        }
                    }                
                }

                return tempList.AsQueryable();
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IQueryable<TrainingPerson> Query(bool? status, string name = "", int tid = 0, string jid = "", string DeptID = "", int signStatus = 0)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsSign == status);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name.Contains(name));
            }
            if (tid != 0)
            {
                query = query.Where(q => q.TraningID == tid);
            }
            if (signStatus != 0)
            {
                query = query.Where(q => q.SignStatus != signStatus);
            }
            if (!string.IsNullOrEmpty(jid))
            {
                query = query.Where(q => q.JobID == jid);
                var ck = query.ToList();
            }
            if (!string.IsNullOrEmpty(DeptID))
            {
                var t8PersonIdList = t8db.comPerson.Where(q => q.DeptId == DeptID).Select(q => q.PersonId).ToList();
                query = query.Where(q => t8PersonIdList.Contains(q.JobID));
            }
            return query;
        }

        public TrainingPerson FindBySignCode(string signCode)
        {
            return db.TrainingPerson.Where(q => q.SignCode == signCode).FirstOrDefault();
        }

        public TrainingPerson FindBy(int id)
        {
            return db.TrainingPerson.Find(id);
        }

        public TrainingPerson FindByJobID(string jid)
        {
            return db.TrainingPerson.Where(q => q.JobID == jid).FirstOrDefault();
        }

        public List<NumAndPercentageModel> GetNumAndPercentage(int trainingId, string type)
        {
            List<NumAndPercentageModel> result = new List<NumAndPercentageModel>();
            var query = GetAll().Where(q => q.TraningID == trainingId);
          
            List<NumAndPercentageModel> data = new List<NumAndPercentageModel>();
            switch (type)
            {
                case "Sign":
                    result = query.GroupBy(d => d.SignStatus)
                          .SelectMany(grp => grp
                          .Select(row => new NumAndPercentageModel()
                          {
                              Title = row.SignStatus.ToString(),
                              Num = grp.Count(),
                              CateCount = grp.Count(),
                              AllCount = query.Count()
                          })
                      ).Distinct().ToList();
                    foreach (var item in result)
                    {
                        item.Title = StatusConverter.Convert(Convert.ToInt32(item.Title));
                    }
                    break;
                case "Opinion":
                    var classOpinionQuery = db.ClassOpinion.Where(q => q.TrainingID == trainingId);
                    var personIdList = classOpinionQuery.Select(q => q.PersonID).ToList();
                    var finished = query.Where(q => personIdList.Contains(q.JobID));
                    if (finished.Count() > 0)
                    {
                        result.Add(new NumAndPercentageModel()
                        {
                            Title = "已填寫",
                            Num = finished.Count(),
                            CateCount = finished.Count(),
                            AllCount = query.Count()
                        });
                    }

                    int leaveStatus = (int)SignStatus.Leave;
                    var leavePersonIdList = query.Where(q => q.SignStatus == leaveStatus).Select(q => q.JobID).ToList();
                    var unfilledList = query.Where(q => !personIdList.Contains(q.JobID));
                    var unfinished = unfilledList.Where(q => !leavePersonIdList.Contains(q.JobID));
                    if (unfinished.Count() > 0)
                    {
                        result.Add(new NumAndPercentageModel()
                        {
                            Title = "未填寫",
                            Num = unfinished.Count(),
                            CateCount = unfinished.Count(),
                            AllCount = query.Count()
                        });
                    }
                   
                    var skipped = unfilledList.Where(q => leavePersonIdList.Contains(q.JobID));
                    if (skipped.Count() > 0)
                    {
                        result.Add(new NumAndPercentageModel()
                        {
                            Title = "請假",
                            Num = skipped.Count(),
                            CateCount = skipped.Count(),
                            AllCount = query.Count()
                        });
                    }
                   
                    break;
                case "Learning":
                    var learningQuery = db.InternalAcceptance.Where(q => q.TrainingID == trainingId).ToList();
                    var passedIdList = learningQuery.Where(q => q.IsPassed == true).Select(q => q.PersonID).ToList();
                    var notPassedList = learningQuery.Where(q => !passedIdList.Contains(q.PersonID));
                        result.Add(new NumAndPercentageModel()
                        {
                            Title = "通過",
                            Num = passedIdList.Count(),
                            CateCount = passedIdList.Count(),
                            AllCount = learningQuery.Count()
                        });
                        result.Add(new NumAndPercentageModel()
                        {
                            Title = "未通過",
                            Num = notPassedList.Count(),
                            CateCount = notPassedList.Count(),
                            AllCount = learningQuery.Count()
                        });

                    break;
            }
            return result.OrderBy(q => q.Title).ToList();
        }

        public class StatusConverter
        {
            public static string Convert(int status)
            {
                switch (status)
                {
                    case 0:
                        return "未簽到";
                    case 1:
                        return "已簽到";
                    case 2:
                        return "請假";
                    // 其他情況
                    default:
                        return "未簽到";
                }
            }
        }

        //public string GetSignStatusStr(int signStatus)
        //{
        //    switch (signStatus)
        //    {
        //        case 0 :
        //            return "未簽到";
        //        case 1 :
        //            return "已簽到";
        //        case 2 :
        //            return "請假";
        //        default:
        //            return "未簽到";
        //    }
        //}

        public bool CleanOldList(int tid)
        {
            try
            {
                var queryList = GetAll().Where(q => q.TraningID == tid).ToList();
                if (queryList.Count() > 0)
                {
                    foreach (var data in queryList)
                    {
                        db.TrainingPerson.Remove(data);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}