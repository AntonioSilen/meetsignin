﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class QuestionDBRepository : GenericRepository<QuestionDB>
    {
        public QuestionDBRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<QuestionDB> Query(bool? isRequired = null, string question = "", int answerType = 0)
        {
            var query = GetAll();
            if (isRequired.HasValue)
            {
                query = query.Where(q => q.IsRequired == isRequired);
            }
            if (!string.IsNullOrEmpty(question))
            {
                query = query.Where(q => q.Question == question);
            }
            if (answerType != 0)
            {
                query = query.Where(q => q.AnswerType == answerType);
            }
            return query;
        }

        public QuestionDB FindBy(int id)
        {
            return db.QuestionDB.Find(id);
        }
    }
}