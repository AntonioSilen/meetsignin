﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.T8Repositories
{
    public class comDepartmentRepository : GenericRepository<comDepartment>
    {
        public comDepartmentRepository(T8ERPEntities context) : base(context) { }
        private T8ERPEntities t8db = new T8ERPEntities();
        //private InspectionEntities db = new InspectionEntities();
        public IQueryable<comDepartment> Query(List<int> lvList, string parentDeptId = "")
        {
            var query = GetAll();
            if (lvList.Count() > 0)
            {               
                query = query.Where(q => lvList.Contains(q.Lv));                
            }
            if (!string.IsNullOrEmpty(parentDeptId))
            {
                query = query.Where(q => q.ParentDeptId == parentDeptId);
            }

            return query;
        }

        public comDepartment FindByDeptName(string deptName)
        {
            return t8db.comDepartment.Where(q => q.DeptName == deptName).FirstOrDefault();
        }
        public comDepartment FindByDeptId(string deptId)
        {
            return t8db.comDepartment.Where(q => q.DeptId == deptId).FirstOrDefault();
        }
    }
}