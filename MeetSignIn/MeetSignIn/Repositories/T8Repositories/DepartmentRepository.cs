﻿using MeetSignIn.Models;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.T8Repositories
{
    public class DepartmentRepository : GenericRepository<comDepartment>
    {
        public DepartmentRepository(T8ERPEntities context) : base(context) { }
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<ExistDepartment> GetDepartmentQuery()
        {
            var query = from d in t8db.comDepartment
                        join p in t8db.comPerson
                        on d.DeptId equals p.DeptId
                        join g in t8db.comGroupPerson
                        on p.PersonId equals g.PersonId
                        where p.ServiceStatus != "40"
                        select new ExistDepartment()
                        {
                            DeptID = d.DeptId,
                            ParentDeptID = d.ParentDeptId,
                            Title = d.DeptName
                        };

            return query.Distinct();
        }

        public IQueryable<comDepartment> Query(string deptName = "", string deptId = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(deptName))
            {
                query = query.Where(q => q.DeptName == deptName);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }

            return query;
        }

        public comPerson FindBy(int id)
        {
            return t8db.comPerson.Find(id);
        }

        public comDepartment FindByDeptId(string deptId)
        {
            return Query().Where(q => q.DeptId == deptId).FirstOrDefault();
        }
    }
}