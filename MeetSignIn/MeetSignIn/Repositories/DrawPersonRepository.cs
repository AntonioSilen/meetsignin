﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DrawPersonRepository : GenericRepository<DrawPerson>
    {
        public DrawPersonRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<DrawPerson> Query(int did = 0, string personId = "", string name = "", int drawStatus = 0)
        {
            var query = GetAll();           
            if (did != 0)
            {
                query = query.Where(q => q.DrawID == did);
            }
            if (drawStatus != 0)
            {
                query = query.Where(q => q.DrawStatus == drawStatus);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.PersonName.Contains(name));
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId == personId);
                var ck = query.ToList();
            }

            return query;
        }

        public DrawPerson FindBy(int id)
        {
            return db.DrawPerson.Find(id);
        }

    }
}