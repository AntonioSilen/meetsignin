﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFWorkRepository : GenericRepository<PFWork>
    {
        public PFWorkRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFWork> Query(bool? status = null, int classId = 0, string deptId = "", string title = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            return query;
        }

        public IQueryable<PFWorksTitleModel> GetTitles(int classId = 0, string deptId = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }

            var result = query.Select(x => new PFWorksTitleModel
            {
                ClassID = x.ClassID,
                DeptId = x.DeptId,
                Title = x.Title
            });
            return result;
        }

        public PFWork FindBy(int id)
        {
            return db.PFWork.Find(id);
        }      
    }
}