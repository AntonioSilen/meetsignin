﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DrawRepository : GenericRepository<Draw>
    {
        public DrawRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Draw> Query(string title = "", int type = 0)
        {
            var query = GetAll();

            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            return query;
        }

        public Draw FindBy(int id)
        {
            return db.Draw.Find(id);
        }
    }
}