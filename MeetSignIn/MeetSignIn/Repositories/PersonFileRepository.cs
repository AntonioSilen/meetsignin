﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PersonFileRepository : GenericRepository<PersonFile>
    {
        public PersonFileRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PersonFile> Query(int pid = 0)
        {
            var query = GetAll();
            if (pid != 0)
            {
                query = query.Where(q => q.PersonID == pid);
            }

            return query;
        }

        public PersonFile FindBy(int id)
        {
            return db.PersonFile.Find(id);
        }
    }
}