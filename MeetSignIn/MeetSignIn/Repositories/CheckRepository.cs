﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class CheckRepository : GenericRepository<Check>
    {
        public CheckRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Check> Query(int type = 0, int targetId = 0, int state = 0)
        {
            var query = GetAll();
            if (type != 0)
            {
                query = query.Where(q => q.TargetType == type);
            }
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId);
            }
            if (state != 0)
            {
                query = query.Where(q => q.ApprovalState == (int)ApprovalState.Approving);
            }

            return query;
        }
    }
}