﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DrawAwardSortRepository : GenericRepository<DrawAwardSort>
    {
        public DrawAwardSortRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<DrawAwardSort> Query(int did = 0, int sort = 0)
        {
            var query = GetAll();           

            if (did != 0)
            {
                query = query.Where(q => q.DrawID == did);
            }
            if (sort != 0)
            {
                query = query.Where(q => q.Sort == sort);
            }

            return query;
        }

        public DrawAwardSort FindBy(int id)
        {
            return db.DrawAwardSort.Find(id);
        }

    }
}