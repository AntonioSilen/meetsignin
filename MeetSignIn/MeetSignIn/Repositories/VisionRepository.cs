﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class VisionRepository : GenericRepository<Vision>
    {
        public VisionRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Vision> Query(bool? status = null, int year = 0, string brief = "")
        {
            var query = GetAll();
            if (year != 0)
            {
                query = query.Where(q => q.Year == year);
            }
            if (!string.IsNullOrEmpty(brief))
            {
                query = query.Where(q => q.Brief.Contains(brief));
            }
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            return query;
        }

        public Vision FindBy(int id)
        {
            return db.Vision.Find(id);
        }
    }
}