﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DrawAwardRepository : GenericRepository<DrawAward>
    {
        public DrawAwardRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<DrawAward> Query(int did = 0, string title = "", bool? isRepeatable = null)
        {
            var query = GetAll();           
            if (did != 0)
            {
                query = query.Where(q => q.DrawID == did);
            }
            if (isRepeatable.HasValue)
            {
                query = query.Where(q => q.IsRepeatable == isRepeatable);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title.Contains(title));
            }         

            return query;
        }

        public DrawAward FindBy(int id)
        {
            return db.DrawAward.Find(id);
        }

    }
}