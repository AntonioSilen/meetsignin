﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class CheckImageRepository : GenericRepository<CheckImage>
    {
        public CheckImageRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<CheckImage> Query(int cid = 0)
        {
            var query = GetAll();
            if (cid != 0)
            {
                query = query.Where(q => q.CheckID == cid);
            }

            return query;
        }

        public CheckImage FindBy(int id)
        {
            return db.CheckImage.Find(id);
        }

        public void DeleteFile(int id)
        {
            CheckImage data = FindBy(id);

            db.SaveChanges();
        }
    }
}