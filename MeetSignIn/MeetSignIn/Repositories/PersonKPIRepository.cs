﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PersonKPIRepository : GenericRepository<PersonKPI>
    {
        public PersonKPIRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PersonKPI> Query(int? approvalState, bool? Islatest, int kpiid = 0, string personId = "", string deptId = "", int state = 0, bool? isInvalid = null)
        {
            var query = GetAll();
            if (approvalState.HasValue)
            {
                query = query.Where(p => p.ApprovalState == approvalState);
            }
            if (Islatest.HasValue)
            {
                query = query.Where(p => p.IsLatest == Islatest);
            }
            if (isInvalid.HasValue)
            {
                query = query.Where(p => p.IsInvalid == isInvalid);
            }
            if (kpiid != 0)
            {
                query = query.Where(p => p.KPIID == kpiid);
            }
            if (state != 0)
            {
                query = query.Where(p => p.State == state);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(p => p.PersonId == personId);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(p => p.DeptId == deptId);
            }

            return query;
        }

        public PersonKPI GetByPersonIdAndKPIID(int deptKpiId, string personId)
        {
            return GetAll().Where(q => q.KPIID == deptKpiId && q.PersonId == personId).FirstOrDefault();
        }
    }
}