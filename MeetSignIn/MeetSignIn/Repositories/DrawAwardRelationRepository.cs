﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DrawAwardRelationRepository : GenericRepository<DrawAwardRelation>
    {
        public DrawAwardRelationRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<DrawAwardRelation> Query(int did = 0, int aid = 0, string personId = "", bool? isRevoke = null)
        {
            var query = GetAll();           
            if (did != 0)
            {
                query = query.Where(q => q.DrawID == did);
            }   
            if (aid != 0)
            {
                query = query.Where(q => q.AwardID == aid);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId == personId);
            }         
            if (isRevoke.HasValue)
            {
                query = query.Where(q => q.IsRevoke == isRevoke);
            }

            return query;
        }

        public DrawAwardRelation FindBy(int id)
        {
            return db.DrawAwardRelation.Find(id);
        }

    }
}