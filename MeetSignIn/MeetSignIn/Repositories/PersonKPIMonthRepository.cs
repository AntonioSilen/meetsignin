﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PersonKPIMonthRepository : GenericRepository<PersonKPIMonth>
    {
        public PersonKPIMonthRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PersonKPIMonth> Query(bool? isFinished = null, int personKpiId = 0, string personId = "", int month = 0, int type = 0)
        {
            var query = GetAll();
            if (isFinished.HasValue)
            {
                query = query.Where(p => p.IsFinished == isFinished);
            }
            if (personKpiId != 0)
            {
                query = query.Where(q => q.PersonKPIID == personKpiId); ;
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId); ;
            }
            if (month != 0)
            {
                query = query.Where(q => q.Month == month); ;
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type); ;
            }

            return query;
        }

        public PersonKPIMonth FindByMonth(string personId, int personkpiId, int month)
        {
            return db.PersonKPIMonth.Where(q => q.PersonID == personId && q.PersonKPIID == personkpiId && q.Month == month && q.Type == (int)InfoState.Initial).FirstOrDefault();
        }
    }
}