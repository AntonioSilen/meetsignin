﻿using MeetSignIn.Models;
using MeetSignIn.Models.Join;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class MeetingRepository : GenericRepository<Meeting>
    {
        public MeetingRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public MeetingJoinRoom FindByMeetingID(int mid)
        {

            var notifyList = db.Meeting.Where(p => p.ID == mid);            

            var query = from m in notifyList
                        join r in db.Room
                        on m.RoomID equals r.ID                       
                        select new MeetingJoinRoom()
                        {
                            MeetingID = m.ID,
                            Title = m.Title,
                            Organiser = m.Organiser,
                            FactoryID = r.FactoryID,
                            RoomID = r.ID,
                            StartDate = m.StartDate,
                            EndDate = m.EndDate,
                            IsSignable = m.IsSignable,
                            LateMinutes = m.LateMinutes,
                            RoomNumber = r.RoomNumber,
                            Place = r.Place,
                        };
            return query.FirstOrDefault();
        }

        public IQueryable<MeetingJoinSingInAndEmployeeAndDepartment> GetMeetingDetailList(string searchValue, DateTime start, DateTime end
            , int department = 0, int factory = 0, int room = 0)
        {

            var query = from m in db.Meeting
                        join s in db.SignIn
                        on m.ID equals s.MeetingID     
                        join e in db.Employee
                        on s.JobID equals e.JobID
                        join d in db.Department
                        on e.DepartmentID equals d.ID                       
                        select new MeetingJoinSingInAndEmployeeAndDepartment()
                        {
                            ID = m.ID,
                            Title = m.Title,
                            Organiser = m.Organiser,
                            FactoryID = m.FactoryID,
                            RoomID = m.RoomID,
                            StartDate = m.StartDate,
                            EndDate = m.EndDate,
                            IsSignable = m.IsSignable,
                            LateMinutes = m.LateMinutes,
                            SignCode = s.SignCode,
                            JobID = e.JobID,
                            DepartmentID = d.ID,
                            DepartmentTitle = d.Title,
                        };

            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(q => q.Title.Contains(searchValue) || q.Organiser.Contains(searchValue));
            }
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }
            if (room != 0)
            {
                query = query.Where(q => q.RoomID == room);
            }
            if (start > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.StartDate >= start);
            }
            if (end > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.EndDate <= end);
            }

                
            return query;
        }

        public IQueryable<Meeting> Query(string searchValue, DateTime start, DateTime end
            , int department = 0, int factory = 0, int room = 0, int exceptMid = 0)
        {
            var query = GetAll();          
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(q => q.Title.Contains(searchValue) || q.Organiser.Contains(searchValue));
            }
            //if (department != 0)
            //{
            //    var signInList = db.SignIn.Where(q => q.d)
            //    query = query.Where(q => q.Department == department);
            //}
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }
            if (room != 0)
            {
                query = query.Where(q => q.RoomID == room);
            }
            if (start > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.StartDate >= start);
            }
            if (end > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.EndDate <= end);
            }
            if (exceptMid != 0)
            {
                query = query.Where(q => q.ID != exceptMid);
            }

            return query;
        }

        public Meeting FindBy(int id)
        {
            return db.Meeting.Find(id);
        }

        public Meeting GetMeetByNum(string roomNum)
        {
            try
            {
                DateTime now = DateTime.Now;
                DateTime last = Convert.ToDateTime(now.ToShortDateString() + " 23:59:59");
                var roomInfo = db.Room.Where(q => q.RoomNumber == roomNum).FirstOrDefault();
                //var result = GetAll().Where(q => q.RoomID == roomInfo.ID && q.EndDate > now && q.EndDate <= last).OrderBy(q => q.StartDate).FirstOrDefault();
                var result = GetMeetingFilterList().Where(q => q.RoomID == roomInfo.ID && q.SignDate <= now && q.LastSignDate >= now).OrderBy(q => q.StartDate).FirstOrDefault();

                return GetById(result.ID);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string DeleteFile(int id, int type)
        {
            Meeting data = FindBy(id);
            var delete = string.Empty;
            switch (type)
            {
                case 1:
                    delete = data.FileOne;
                    data.FileOne = string.Empty;
                    break;
                case 2:
                    delete = data.FileTwo;
                    data.FileTwo = string.Empty;
                    break;
                default:
                    break;
            }
          
            db.SaveChanges();
            return delete;
        }


        public void TransformCreater(int id, int creater)
        {
            var meetingList = GetAll().Where(q => q.Creater == creater);
            foreach (var item in meetingList)
            {
                item.Creater = 1;
            }
            db.SaveChanges();
        }

        public List<MeetingFilterModel> GetMeetingFilterList()
        {
            var query = from m in db.Meeting
                        where m.IsSignable == true
                        select new MeetingFilterModel()
                        {
                            ID = m.ID,
                            RoomID = m.RoomID,
                            SignDate = m.SignDate,
                            StartDate = m.StartDate,
                            LateMinutes = m.LateMinutes,
                            LastSignDate = m.StartDate,
                            EndDate = m.EndDate,
                        };
            var resultList = query.ToList();
            foreach (var item in resultList)
            {
                item.LastSignDate = item.StartDate.AddMinutes(item.LateMinutes);
            }

            return resultList;
        }

        public class MeetingFilterModel
        {
            public int ID { get; set; }
            public int RoomID { get; set; }
            public DateTime SignDate { get; set; }
            public DateTime StartDate { get; set; }
            public int LateMinutes { get; set; }
            public DateTime LastSignDate { get; set; }
            public DateTime EndDate { get; set; }
        }
    }
}