﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class FunctionDocRepository : GenericRepository<FunctionDoc>
    {
        public FunctionDocRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<FunctionDoc> Query(string personId = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId == personId);
            }

            return query;
        }

        public FunctionDoc FindBy(int id)
        {
            return db.FunctionDoc.Find(id);
        }

        public string DeleteFile(int id)
        {
            FunctionDoc data = FindBy(id);

            db.SaveChanges();

            return data.FileName;
        }
    }
}