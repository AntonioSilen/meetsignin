﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class CompetencyRepository : GenericRepository<Competency>
    {
        public CompetencyRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Competency> Query(bool? status = null, string deptName = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(deptName))
            {
                query = query.Where(q => q.Depts.Contains(deptName));
            }
            return query;
        }

        public Competency FindBy(int id)
        {
            return db.Competency.Find(id);
        }
    }
}