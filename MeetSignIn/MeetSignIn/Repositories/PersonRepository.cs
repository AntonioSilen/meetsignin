﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PersonRepository : GenericRepository<Person>
    {
        public PersonRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Person> Query(string jid)
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(jid))
            {
                query = query.Where(q => q.JobID == jid);
            }

            return query;
        }

        public Person FindBy(int id)
        {
            return db.Person.Find(id);
        }

        public Person GetByJobID(string jobId)
        {
            var query = db.Person.Where(q => q.JobID == jobId).FirstOrDefault();
            if (query == null)
            {
                db.Person.Add(new Person
                {
                    JobID = jobId,
                    Competency = "",
                    UpdateDate = DateTime.Now,
                });
                db.SaveChanges();
            }
            query = db.Person.Where(q => q.JobID == jobId).FirstOrDefault();
            return query;
        }
    }
}