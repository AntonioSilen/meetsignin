﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Models.Chart;
using MeetSignIn.Models.Join;
using MeetSignIn.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class TrainingAbnormalRepository : GenericRepository<TrainingAbnormal>
    {
        public TrainingAbnormalRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<TrainingAbnormal> Query(DateTime start, DateTime end, int tid = 0, bool? isAudit = null)
        {
            var query = GetAll();
            if (tid != 0)
            {
                query = query.Where(q => q.TrainingID == tid);
            }
            if (start > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.CreateDate >= start);
            }
            if (end > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.CreateDate <= end);
            }
            if (isAudit.HasValue)
            {
                query = query.Where(q => q.IsAudit == isAudit);
            }
            return query;
        }

        public TrainingAbnormal FindBy(int id)
        {
            return db.TrainingAbnormal.Find(id);
        }
    }
}