﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.TrainingForm
{
    public class ExternalCourseRepository : GenericRepository<ExternalCourse>
    {
        public ExternalCourseRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ExternalCourse> Query(int traingingId = 0, int externalId = 0, bool? isOnline = null)
        {
            var query = GetAll().Where(q => q.IsOnline);
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }         
            if (externalId != 0)
            {
                query = query.Where(q => q.ExternalID == externalId);
            }
            if (isOnline.HasValue)
            {
                query = query.Where(q => q.IsOnline == isOnline);
            }

            return query;
        }

        public ExternalCourse FindBy(int id)
        {
            return db.ExternalCourse.Find(id);
        }
    }
}