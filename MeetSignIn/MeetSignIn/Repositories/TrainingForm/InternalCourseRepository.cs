﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.TrainingForm
{
    public class InternalCourseRepository : GenericRepository<InternalCourse>
    {
        public InternalCourseRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<InternalCourse> Query(int traingingId = 0, int internalId = 0, bool? isOnline = null)
        {
            var query = GetAll().Where(q => q.IsOnline);
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            var tp = query.ToList();
            if (internalId != 0)
            {
                query = query.Where(q => q.InternalID == internalId);
            }
            tp = query.ToList();
            if (isOnline.HasValue)
            {
                query = query.Where(q => q.IsOnline == isOnline);
            }
            tp = query.ToList();

            return query;
        }

        public InternalCourse FindBy(int id)
        {
            return db.InternalCourse.Find(id);
        }
    }
}