﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.TrainingForm
{
    public class ExternalAcceptanceRepository : GenericRepository<ExternalAcceptance>
    {
        public ExternalAcceptanceRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ExternalAcceptance> Query(int traingingId, string personId = "", string name = "")
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name == name);
            }

            return query;
        }

        public ExternalAcceptance FindBy(int id)
        {
            return db.ExternalAcceptance.Find(id);
        }

        public string DeleteReportFile(int id)
        {
            ExternalAcceptance data = FindBy(id);
            string fileName = data.ReportFile;
            data.ReportFile = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteCertificateFile(int id)
        {
            ExternalAcceptance data = FindBy(id);
            string fileName = data.CertificateFile;
            data.CertificateFile = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteActionPlanFile(int id)
        {
            ExternalAcceptance data = FindBy(id);
            string fileName = data.ActionPlanFile;
            data.ActionPlanFile = "";

            db.SaveChanges();
            return fileName;
        }

        public ExternalAcceptance FindByTrainingIdAndPersonId(int tid, string personId)
        {
            return db.ExternalAcceptance.Where(q => q.TrainingID == tid && q.PersonID == personId).FirstOrDefault();
        }
    }
}