﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.TrainingForm
{
    public class SeminarPlanRepository : GenericRepository<SeminarPlan>
    {
        public SeminarPlanRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<SeminarPlan> Query(int traingingId, bool? isAudit = null)
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            if (isAudit.HasValue)
            {
                query = query.Where(q => q.IsAudit == isAudit);
            }

            return query;
        }

        public SeminarPlan FindBy(int id)
        {
            return db.SeminarPlan.Find(id);
        }
    }
}