﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.TrainingForm
{
    public class ExternalPlanRepository : GenericRepository<ExternalPlan>
    {
        public ExternalPlanRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ExternalPlan> Query(int traingingId, bool? isAudit = null)
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            if (isAudit.HasValue)
            {
                query = query.Where(q => q.IsAudit == isAudit);
            }

            return query;
        }

        public ExternalPlan FindBy(int id)
        {
            return db.ExternalPlan.Find(id);
        }
    }
}