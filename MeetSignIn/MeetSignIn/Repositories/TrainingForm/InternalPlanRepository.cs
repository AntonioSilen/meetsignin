﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories.TrainingForm
{
    public class InternalPlanRepository : GenericRepository<InternalPlan>
    {
        public InternalPlanRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<InternalPlan> Query(int traingingId, bool? isAudit = null)
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            if (isAudit.HasValue)
            {
                query = query.Where(q => q.IsAudit == isAudit);
            }

            return query;
        }

        public InternalPlan FindBy(int id)
        {
            return db.InternalPlan.Find(id);
        }

        public InternalPlan FindValidPlan(int traingingId)
        {
            return GetAll().Where(q => q.TrainingID == traingingId && q.IsAudit).OrderByDescending(q => q.ID).FirstOrDefault();
        }
    }
}