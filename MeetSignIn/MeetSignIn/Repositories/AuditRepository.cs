﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class AuditRepository : GenericRepository<Audit>
    {
        public AuditRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Audit> Query(bool? isInvalid = null, int targetId = 0, int type = 0, int? state = null, string trainingTitle = "", string personId = "", int trainingId = 0, int officer = 0, int? pfState = null, bool auditIndexPage = false, bool modelState = false)
        {
            var query = GetAll().Where(q => q.Type != (int)AuditType.ClassOpinion);
            if (isInvalid.HasValue)
            {
                query = query.Where(q => q.IsInvalid == isInvalid);
            }
            var queryList = query.ToList();
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (state.HasValue || pfState.HasValue)
            {//審核狀態
                //if (officer != 0)
                //{//兼承辦人
                //    if ((state.HasValue && !pfState.HasValue))
                //    {
                //        query = query.Where(q => q.Officer == officer || (q.Type != (int)AuditType.PF && q.State == state));
                //    }
                //    else if ((!state.HasValue && pfState.HasValue))
                //    {
                //        query = query.Where(q => q.Officer == officer || (q.Type == (int)AuditType.PF && q.State == pfState));
                //    }
                //    else
                //    {
                //        query = query.Where(q => q.Officer == officer || (q.Type != (int)AuditType.PF && q.State == state) || (q.Type == (int)AuditType.PF && q.State == pfState));
                //    }
                //}
                //else
                //{
                //    if ((state.HasValue && !pfState.HasValue))
                //    {
                //        query = query.Where(q => q.Type != (int)AuditType.PF && q.State == state);
                //    }
                //    else if ((!state.HasValue && pfState.HasValue))
                //    {
                //        query = query.Where(q => q.Type == (int)AuditType.PF && q.State == pfState);
                //    }
                //    else
                //    {
                //        query = query.Where(q => (q.Type != (int)AuditType.PF && q.State == state) || (q.Type == (int)AuditType.PF && q.State == pfState));
                //    }
                //}
                if (state.HasValue && !pfState.HasValue)
                {//訓練審核狀態
                 //if (state == (int)AuditState.PresidentAudit)
                 //{
                 //    var adminInfo = AdminInfoHelper.GetAdminInfo();
                 //    var otherState = 0;
                 //    switch (adminInfo.Type)
                 //    {
                 //        case (int)AdminType.Manager:
                 //            otherState = (int)AuditState.OfficerAudit;
                 //            query = query.Where(q => q.Type != (int)AuditType.PF && (q.State == state || q.State == otherState));
                 //            break;
                 //        case (int)AdminType.Vice:
                 //            otherState = (int)AuditState.ManagerAudit;
                 //            query = query.Where(q => q.Type != (int)AuditType.PF && (q.State == state || q.State == otherState));
                 //            break;
                 //        //case (int)AdminType.Principal:
                 //        //    stateInt = (int)MeetSignIn.Models.AuditState.PresidentAudit;
                 //        //    break;
                 //        case (int)AdminType.President:
                 //            otherState = (int)AuditState.ViceAudit;
                 //            query = query.Where(q => q.Type != (int)AuditType.PF && (q.State == state || q.State == otherState));
                 //            break;
                 //    }

                    //}
                    //else
                    //{
                    //    query = query.Where(q => q.Type != (int)AuditType.PF && q.State == state);
                    //}                  
                    query = query.Where(q => q.Type != (int)AuditType.PF && q.State == state);
                    queryList = query.ToList();
                }
                else if (!state.HasValue && pfState.HasValue)
                {//績效審核狀態
                    query = query.Where(q => q.Type == (int)AuditType.PF && q.State == pfState);
                }
                else
                {//訓練&績效審核狀態
                    query = query.Where(q => (q.Type != (int)AuditType.PF && q.State == state) || (q.Type == (int)AuditType.PF && q.State == pfState));
                }
            }
            //if (pfState.HasValue)
            //{//績效審核狀態
            //    if (officer != 0)
            //    {//兼承辦人
            //        query = query.Where(q => q.Officer == officer || q.State == pfState);                
            //    }
            //    else
            //    {
            //        query = query.Where(q => q.State == pfState);
            //    }
            //}
            else
            {//未審
                if (officer != 0)
                {//兼承辦人
                    query = query.Where(q => q.Officer == officer);
                }
            }
            if (!string.IsNullOrEmpty(trainingTitle))
            {
                var trainingIdList = db.Training.Where(q => q.Title.Contains(trainingTitle)).Select(q => q.ID).ToList();
                query = query.Where(q => trainingIdList.Contains(q.TrainingID));
            }
            if (trainingId != 0)
            {
                var trainingIdList = db.Training.Where(q => q.ID == trainingId).Select(q => q.ID).ToList();
                query = query.Where(q => trainingIdList.Contains(q.TrainingID));
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            if (auditIndexPage)
            {
                var tempList = query.ToList();
                foreach (var item in query)
                {
                    var trainingData = db.Training.Where(q => q.ID == item.TrainingID).FirstOrDefault();
                    if (item.Type == (int)AuditType.CloseExternalTraining || item.Type == (int)AuditType.CloseInternalTraining)
                    {
                        if (trainingData.IsApproved == false || trainingData.IsApproved.HasValue == false)
                        {
                            tempList.Remove(item);
                            continue;
                        }
                    }
                    if (modelState && trainingData.IsModel != true)
                    {
                        tempList.Remove(item);
                    }
                }
                return tempList.AsQueryable();
            }
    
            return query;
        }

        public Audit FindBy(int id)
        {
            return db.Audit.Find(id);
        }

        public Audit FindByTarget(int targetId = 0, int type = 0, int trainingId = 0, string personId = "")
        {
            var query = GetAll();
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (trainingId != 0)
            {
                query = query.Where(q => q.TrainingID == trainingId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            return query.FirstOrDefault();
        }
    }
}