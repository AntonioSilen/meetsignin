﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFWorksRepository : GenericRepository<PFWorks>
    {
        public PFWorksRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFWorks> Query(bool? status = null, int classId = 0, int itemId = 0, string deptId = "", string title = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            if (itemId != 0)
            {
                query = query.Where(q => q.ItemID == itemId);
            }
            return query;
        }

        public IQueryable<PFWorksTitleModel> GetTitles (int classId = 0, int itemId = 0, string deptId = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            if (itemId != 0)
            {
                query = query.Where(q => q.ItemID == itemId);
            }

            var result = query.Select(x => new PFWorksTitleModel
            {
                ClassID = x.ClassID,
                DeptId = x.DeptId,
                Title = x.Title
            });
            return result;
        }

        public PFWorks FindBy(int id)
        {
            return db.PFWorks.Find(id);
        }

        public PFWorks FindByTitle(string title, int classId = 0, int itemId = 0, int lvlId = 0)
        {
            var query = db.PFWorks.Where(q => q.Title == title);
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            if (itemId != 0)
            {
                query = query.Where(q => q.ItemID == itemId);
            }
            if (lvlId != 0)
            {
                query = query.Where(q => q.LvlID == lvlId);
            }
            return query.FirstOrDefault();
        }
    }

    public class PFWorksTitleModel
    {
        public string Title { get; set; }
        public string DeptId { get; set; }
        public int ClassID { get; set; }
    }
}