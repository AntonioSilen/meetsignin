﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DeptKPIPercentageRepository : GenericRepository<DeptKPIPercentage>
    {
        public DeptKPIPercentageRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<DeptKPIPercentage> Query(bool? Islatest, int deptkpiid = 0, int personkpiid = 0, string personId = "", string deptId = "", int state = 0, bool? isInvalid = null)
        {
            var query = GetAll();
            if (Islatest.HasValue)
            {
                query = query.Where(p => p.IsLatest == Islatest);
            }
            if (isInvalid.HasValue)
            {
                query = query.Where(p => p.IsInvalid == isInvalid);
            }
            if (deptkpiid != 0)
            {
                query = query.Where(p => p.DeptKPIID == deptkpiid);
            }
            if (personkpiid != 0)
            {
                query = query.Where(p => p.PersonKPIID == personkpiid);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(p => p.PersonId == personId);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(p => p.DeptId == deptId);
            }

            return query;
        }

        public DeptKPIPercentage GetDivisionInfo(int kpiId, int personKpiId, string personId)
        {
            return GetAll().Where(q => q.DeptKPIID == kpiId && q.PersonKPIID == personKpiId && q.PersonId == personId && q.IsLatest && !q.IsInvalid).FirstOrDefault();
        }
    }
}