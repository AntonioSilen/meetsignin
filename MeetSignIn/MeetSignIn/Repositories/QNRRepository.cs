﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class QNRRepository : GenericRepository<QNR>
    {
        public QNRRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<QNR> Query(bool? status = null, string title = "", string description = "", int year = 0, int month = 0)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            if (!string.IsNullOrEmpty(title))
            {
                query = query.Where(q => q.Title == title);
            }
            if (!string.IsNullOrEmpty(description))
            {
                query = query.Where(q => q.Title == description);
            }
            if (year != 0)
            {
                query = query.Where(q => q.Year == year);
            }
            if (month != 0)
            {
                query = query.Where(q => q.Month == month);
            }
            return query;
        }

        public QNR FindBy(int id)
        {
            return db.QNR.Find(id);
        }

        public QNR FindByTrainingId(int trainingId)
        {
            var query = db.QNR.Where(q => q.TrainingID == trainingId).FirstOrDefault();
            return query;
        }
    }
}