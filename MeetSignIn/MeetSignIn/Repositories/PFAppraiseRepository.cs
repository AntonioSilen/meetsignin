﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFAppraiseRepository : GenericRepository<PFAppraise>
    {
        public PFAppraiseRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFAppraise> Query(string personId = ""/*, int classId = 0*/, int seasonId = 0, string deptId = "", string seasonTitle = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId == personId);
            }
            //if (classId != 0)
            //{
            //    query = query.Where(q => q.ClassID == classId);
            //}
            if (seasonId != 0)
            {
                query = query.Where(q => q.SeasonID == seasonId);
            }
            if (!string.IsNullOrEmpty(seasonTitle))
            {
                query = query.Where(q => q.SeasonTitle.Contains(seasonTitle));
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }

            return query;
        }

        public PFAppraise GetFirstData(string personId = ""/*, int classId = 0*/, int seasonId = 0, string deptId = "")
        {
            var query = Query(personId/*, classId*/, seasonId, deptId);            

            return query.FirstOrDefault();
        }

        public PFAppraise FindBy(int id)
        {
            return db.PFAppraise.Find(id);
        }
    }
}