﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PFAppraiseWorksRepository : GenericRepository<PFAppraiseWorks>
    {
        public PFAppraiseWorksRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PFAppraiseWorks> Query(int appraiseId = 0, int workId = 0, string personId = "", int classId = 0, string deptId = "")
        {
            var query = GetAll();
            if (appraiseId != 0)
            {
                query = query.Where(q => q.AppraiseID == appraiseId);
            }
            if (workId != 0)
            {
                query = query.Where(q => q.WorksID == workId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId == personId);
            }
            if (classId != 0)
            {
                query = query.Where(q => q.ClassID == classId);
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId);
            }

            return query;
        }

        public IQueryable<PFAppraiseWorks> AuditQuery(int tlAudit = 0, int csAudit = 0, int managerAudit = 0)
        {
            var query = GetAll();
            if (tlAudit != 0)
            {
                query = query.Where(q => q.TLAudit == tlAudit);
            }
            if (csAudit != 0)
            {
                query = query.Where(q => q.CSAudit == csAudit);
            }
            if (managerAudit != 0)
            {
                query = query.Where(q => q.ManagerAudit == managerAudit);
            }

            return query;
        }


        public PFAppraiseWorks GetFirstData(int appraiseId = 0, int workId = 0, string personId = "", int classId = 0, string deptId = "")
        {
            var query = Query(appraiseId, workId, personId, classId, deptId);          

            return query.FirstOrDefault();
        }

        public PFAppraiseWorks FindBy(int id)
        {
            return db.PFAppraiseWorks.Find(id);
        }
    }
}