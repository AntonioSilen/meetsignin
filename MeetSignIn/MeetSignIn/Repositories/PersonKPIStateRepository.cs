﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class PersonKPIStateRepository : GenericRepository<PersonKPIState>
    {
        public PersonKPIStateRepository(MeetingSignInDBEntities context) : base(context) { }
        private readonly MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<PersonKPIState> Query(bool? isFinished = null, int personKpiId = 0, bool? isApproved = null)
        {
            var query = GetAll();
            if (isFinished.HasValue)
            {
                query = query.Where(p => p.IsFinished == isFinished);
            }
            if (isApproved.HasValue)
            {
                query = query.Where(p => p.ApprovalState == (int)MeetSignIn.Models.ApprovalState.Approved);
            }
            if (personKpiId != 0)
            {
                query = query.Where(q => q.PersonKPIID == personKpiId); ;
            }

            return query;
        }
       
    }
}