﻿using iMedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace iMedia.Repositories
{
    public class ArticleRepository : GenericRepository<Article>
    {
        private iMediaDBEntity db = new iMediaDBEntity();
        public ArticleRepository(iMediaDBEntity context) : base(context) { }

        public IQueryable<Article> Query(bool? status, bool? main, bool? focus)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.IsOnline == status);
            }
            if (main.HasValue)
            {
                query = query.Where(p => p.IsMain == main);
            }
            if (focus.HasValue)
            {
                query = query.Where(p => p.IsFocus == focus);
            }
            return query;
        }

        public bool UpdateViews(int id)
        {
            try
            {
                var query = GetById(id);
                query.Views = query.Views +1;
                db.Entry(query).State = EntityState.Modified;
                db.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
    public class ArticleDetailRepository : GenericRepository<ArticleDetail>
    {
        public ArticleDetailRepository(iMediaDBEntity context) : base(context) { }

        public IQueryable<ArticleDetail> Query(int aid)
        {
            var query = GetAll();
            if (aid != 0)
            {
                query = query.Where(p => p.ArticleId == aid);
            }
            return query;
        }
    }
}