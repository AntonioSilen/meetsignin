﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Repositories
{
    public class DepartmentRepository : GenericRepository<Department>
    {
        public DepartmentRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Department> Query(bool? status = null)
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.Status == status);
            }
       
            return query;
        }

        public Department FindBy(int id)
        {
            return db.Department.Find(id);
        }

        public Department FindByDeptID(string deptId)
        {
            return db.Department.Where(q => q.DeptID == deptId).First();
        }
    }
}