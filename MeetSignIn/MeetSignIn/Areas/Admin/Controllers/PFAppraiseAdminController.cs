﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.PFAppraise;
using MeetSignIn.Areas.Admin.ViewModels.PFWork;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class PFAppraiseAdminController : BaseAdminController
    {
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
        private PFAppraiseRepository pFAppraiseRepository = new PFAppraiseRepository(new MeetingSignInDBEntities());
        private PFAppraiseWorksRepository pFAppraiseWorksRepository = new PFAppraiseWorksRepository(new MeetingSignInDBEntities());
        private PFAppraiseRelationRepository pFAppraiseRelationRepository = new PFAppraiseRelationRepository(new MeetingSignInDBEntities());
        private PFWorksRelationRepository pFWorksRelationRepository = new PFWorksRelationRepository(new MeetingSignInDBEntities());
        private PFWorkRepository pFWorkRepository = new PFWorkRepository(new MeetingSignInDBEntities());
        private PFSeasonRepository pFSeasonRepository = new PFSeasonRepository(new MeetingSignInDBEntities());
        private comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
        private comPersonRepository comPersonRepository = new comPersonRepository(new T8ERPEntities());

        public static PFHelper pFHelper = new PFHelper();

        public int PF = (int)AuditType.PF;

        public string CLS = EnumHelper.GetDescription(LevelType.CLS);
        public string CATE = EnumHelper.GetDescription(LevelType.CATE);
        public string ITM = EnumHelper.GetDescription(LevelType.ITM);
        public string LVL = EnumHelper.GetDescription(LevelType.LVL);
        public string WORK = EnumHelper.GetDescription(LevelType.WORK);

        /// <summary>
        /// PFAppraise列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Index(PFAppraiseIndexView model)
        {
            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;

            var query = pFAppraiseRepository.Query(model.PersonId/*, model.ClassID*/, model.SeasonID, model.DeptId, model.SeasonTitle);
            var pageResult = query.ToPageResult<PFAppraise>(model);
            model.PageResult = Mapper.Map<PageResult<PFAppraiseView>>(pageResult);

            foreach (var item in model.PageResult.Data)
            {
                var personInfo = comPersonRepository.FindByPersonID(item.PersonId);
                if (personInfo != null)
                {
                    item.PersonName = personInfo.PersonName;
                    item.PersonDeptName = personInfo.DeptName;
                }

                item.ClassTitleList = new List<string>();
                var classIdList = pFAppraiseWorksRepository.Query(item.ID).Select(q => q.ClassID).Distinct();
                foreach (var classId in classIdList)
                {
                    var classInfo = pFCategoryRepository.GetById(classId);

                    if (classInfo != null)
                    {
                        item.ClassTitleList.Add(classInfo.Title);
                    }                    
                }

                #region 計算分數
                //int bp = 0;
                //var appraiseRelationList = pFAppraiseRelationRepository.Query(item.ID).ToList();
                //foreach (var ar in appraiseRelationList)
                //{
                //    bp += ar.Points;
                //}
                //int wp = 0;
                //var workRelationList = pFWorksRelationRepository.Query(true, item.ClassID, item.WorksID, 0, model.DeptId).ToList();
                //foreach (var wr in workRelationList)
                //{
                //    wp += wr.Points;
                //}
                //item.Points = bp * wp;
                #endregion

                #region 審核狀態
                var auditInfo = auditRepository.FindByTarget(item.ID, PF, 0, item.PersonId);
                item.AuditState = auditInfo == null ? 0 : auditInfo.State;
                item.AuditStatus = EnumHelper.GetDescription((PFAuditState)item.AuditState);
                item.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                #endregion
            }

            return View(model);
        }

        /// <summary>
        /// PFAppraise檢視頁，上方顯示基本資訊(需另取得職責權重)
        /// 可切換作業(頁籤)，顯示全部表單
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id = 0, bool FromAudit = false)
        {
            var model = new PFAppraiseView();
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            //model.Points = 0;

            model = Mapper.Map<PFAppraiseView>(pFAppraiseRepository.GetById(id));
            var personInfo = comPersonRepository.FindByPersonID(model.PersonId);
            model.PersonName = personInfo.PersonName;

            var seasonInfo = pFHelper.GetActivatePFSeason(model.DeptId);
            if (seasonInfo == null)
            {
                return RedirectToAction("Index");
            }
            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;

            var appraiseData = pFAppraiseRepository.GetFirstData(model.PersonId/*, model.ClassID*/, seasonInfo.ID);
            if (appraiseData == null)
            {
                return RedirectToAction("Index");
            }
            model.AppraiseID = appraiseData.ID;

            try
            {
                #region 作業清單
                var classIdList = pFAppraiseWorksRepository.Query(model.AppraiseID).Select(q => q.ClassID).ToList();
                var workIdList = pFAppraiseWorksRepository.Query(model.AppraiseID).Select(q => q.WorksID).ToList();
                model.WorkList = Mapper.Map<List<PFWorkView>>(pFWorkRepository.Query(true).Where(q => workIdList.Contains(q.ID)));
                foreach (var work in model.WorkList)
                {
                    #region 計算權重
                    work.Points = 0;
                    var workRelationList = pFWorksRelationRepository.Query(null, work.ClassID, work.ID, 0, model.DeptId).ToList();
                    foreach (var wr in workRelationList)
                    {
                        work.Points += wr.Points;
                    }
                    #endregion

                    #region 表單狀態
                    var appraiseWorkData = pFAppraiseWorksRepository.GetFirstData(appraiseData.ID, work.ID, model.PersonId, work.ClassID, model.DeptId);
                    if (appraiseWorkData != null)
                    {
                        work.AppraiseWorksID = appraiseWorkData.ID;
                        work.IsFinished = appraiseWorkData.IsFinished;
                        work.IsAudit = appraiseWorkData.IsAudit;
                        work.TLAudit = appraiseWorkData.TLAudit;
                        work.CSAudit = appraiseWorkData.CSAudit;
                        work.ManagerAudit = appraiseWorkData.ManagerAudit;
                    }
                    #endregion

                    work.CateList = Mapper.Map<List<ViewModels.PFCategory.PFCategoryCateView>>(pFCategoryRepository.Query(true, model.DeptId, work.ClassID, CATE));
                    foreach (var cate in work.CateList)
                    {
                        cate.ItmList = Mapper.Map<List<ViewModels.PFCategory.PFCategoryItmView>>(pFCategoryRepository.Query(true, model.DeptId, cate.ID, ITM, null, 2));
                        foreach (var item in cate.ItmList)
                        {
                            if (appraiseWorkData != null)
                            {
                                var relationInfo = pFAppraiseRelationRepository.Query(appraiseData.ID, appraiseWorkData.ID, item.ID, 0, work.ID).FirstOrDefault();
                                if (relationInfo != null)
                                {
                                    var lvlInfo = pFCategoryRepository.GetById(relationInfo.LvlID);
                                    item.LvlID = lvlInfo.ID;
                                    item.LvlTitle = lvlInfo.Title;
                                    item.LvlPoints = lvlInfo.Points;
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {

            }

            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();

            var auditLevelData = auditLevelRepository.FindByDept(model.DeptId);
            model.AuditLevelInfo = auditLevelData;
            //班組長
            if (model.TLAudit != AuditProcessing)
            {
                personInfo = t8personRepository.FindByPersonId(appraiseData.TLSignature.Replace(".jpg", ""));
                if (personInfo == null)
                {
                    model.TLDeptId = "";
                    model.TLDeptName = "";
                    model.TLName = "";
                }
                else
                {
                    model.TLDeptId = personInfo.DeptId;
                    model.TLDeptName = personInfo.DepartmentStr;
                    model.TLName = personInfo.Name;
                }
                //model.TLDeptId = personInfo == null ? "" : personInfo.DeptId;
                //model.TLDeptName = personInfo == null ? "" : personInfo.DepartmentStr;
                //model.TLName = personInfo == null ? "" : personInfo.Name;
            }
            //課級主管
            if (model.CSAudit != AuditProcessing)
            {
                personInfo = t8personRepository.FindByPersonId(appraiseData.CSSignature.Replace(".jpg", ""));
                if (personInfo == null)
                {
                    model.CSDeptId = "";
                    model.CSDeptName = "";
                    model.CSName = "";
                }
                else
                {
                    model.CSDeptId = personInfo.DeptId;
                    model.CSDeptName = personInfo.DepartmentStr;
                    model.CSName = personInfo.Name;
                }
                //model.CSDeptId = personInfo == null ? "" : personInfo.DeptId;
                //model.CSDeptName = personInfo == null ? "" : personInfo.DepartmentStr;
                //model.CSName = personInfo == null ? "" : personInfo.Name;
            }            
            //主管
            if (model.ManagerAudit != AuditProcessing)
            {
                personInfo = t8personRepository.FindByPersonId(appraiseData.ManagerSignature.Replace(".jpg", ""));
                if (personInfo == null)
                {
                    model.ManagerDeptId = "";
                    model.ManagerDeptName = "";
                    model.ManagerName = "";
                }
                else
                {
                    model.ManagerDeptId = personInfo.DeptId;
                    model.ManagerDeptName = personInfo.DepartmentStr;
                    model.ManagerName = personInfo.Name;
                }
                //model.ManagerDeptId = personInfo == null ? "" : personInfo.DeptId;
                //model.ManagerDeptName = personInfo == null ? "" : personInfo.DepartmentStr;
                //model.ManagerName = personInfo == null ? "" : personInfo.Name;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PFAppraiseView model)
        {

            PFAppraise data = Mapper.Map<PFAppraise>(model);
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            int adminId = adminInfo.ID;

            #region 送審
            if (model.IsAudit)
            {
                if (model.AppraiseID == 0)
                {
                    return RedirectToAction("Index");
                }
                var appraiseData = pFAppraiseRepository.GetById(model.AppraiseID);
                if (appraiseData == null)
                {
                    return View(model);
                }
                appraiseData.TLAudit = AuditProcessing;
                appraiseData.CSAudit = AuditProcessing;
                appraiseData.ManagerAudit = AuditProcessing;
                var workList = model.WorkList;
                foreach (var work in workList)
                {
                    var appraiseWork = pFAppraiseWorksRepository.GetById(work.AppraiseWorksID);
                    if (appraiseData == null)
                    {
                        continue;
                    }
                    appraiseWork.TLAudit = work.TLAudit;
                    appraiseWork.CSAudit = work.CSAudit;
                    appraiseWork.ManagerAudit = work.ManagerAudit;

                    if (appraiseWork.TLAudit != AuditProcessing && string.IsNullOrEmpty(appraiseWork.TLSignature))
                    {
                        appraiseWork.TLSignature = adminInfo.Account + ".jpg";
                        appraiseWork.TLAuditDate = DateTime.Now;
                    }
                    if (appraiseWork.CSAudit != AuditProcessing && string.IsNullOrEmpty(appraiseWork.CSSignature))
                    {
                        appraiseWork.CSSignature = adminInfo.Account + ".jpg";
                        appraiseWork.CSAuditDate = DateTime.Now;
                    }
                    if (appraiseWork.ManagerAudit != AuditProcessing && string.IsNullOrEmpty(appraiseWork.ManagerSignature))
                    {
                        appraiseWork.ManagerSignature = adminInfo.Account + ".jpg";
                        appraiseWork.ManagerAuditDate = DateTime.Now;
                    }

                    pFAppraiseWorksRepository.Update(appraiseWork);
                }

                var auditInfo = auditRepository.Query(null, model.ID, PF).FirstOrDefault();

                #region 更新評比主資料審核狀態
                if (workList.Where(q => q.TLAudit == AuditApproval).Count() == workList.Count())
                {
                    appraiseData.TLAudit = AuditApproval;
                }
                else if (workList.Where(q => q.TLAudit == AuditReject).Count() > 0)
                {
                    appraiseData.TLAudit = AuditReject;
                    auditInfo.IsInvalid = true;
                }
                if (workList.Where(q => q.CSAudit == AuditApproval).Count() == workList.Count())
                {
                    appraiseData.CSAudit = AuditApproval;
                }
                else if (workList.Where(q => q.CSAudit == AuditReject).Count() > 0)
                {
                    appraiseData.CSAudit = AuditReject;
                    auditInfo.IsInvalid = true;
                }
                if (workList.Where(q => q.ManagerAudit == AuditApproval).Count() == workList.Count())
                {
                    appraiseData.ManagerAudit = AuditApproval;
                }
                else if (workList.Where(q => q.ManagerAudit == AuditReject).Count() > 0)
                {
                    appraiseData.ManagerAudit = AuditReject;
                    auditInfo.IsInvalid = true;
                }

                if (appraiseData.TLAudit != AuditProcessing && string.IsNullOrEmpty(appraiseData.TLSignature))
                {
                    appraiseData.TLSignature = adminInfo.Account + ".jpg";
                    appraiseData.TLAuditDate = DateTime.Now;
                }
                if (appraiseData.CSAudit != AuditProcessing && string.IsNullOrEmpty(appraiseData.CSSignature))
                {
                    appraiseData.CSSignature = adminInfo.Account + ".jpg";
                    appraiseData.CSAuditDate = DateTime.Now;
                }
                if (appraiseData.ManagerAudit != AuditProcessing && string.IsNullOrEmpty(appraiseData.ManagerSignature))
                {
                    appraiseData.ManagerSignature = adminInfo.Account + ".jpg";
                    appraiseData.ManagerAuditDate = DateTime.Now;
                }
                pFAppraiseRepository.Update(appraiseData);
                #endregion



                #region 簽核狀態
                int state = HandlePFState(model.DeptId, appraiseData.TLAudit, appraiseData.CSAudit, appraiseData.ManagerAudit);
                #endregion

                if (auditInfo == null)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    auditInfo.State = state;
                    auditRepository.Update(auditInfo);
                }

                //寄信通知
                //string reciver = GetAuditRevicer(state, model.PersonId, (int)AuditType.ExternalAcceptance, model.PersonID);
                //if (!string.IsNullOrEmpty(reciver))
                //{
                //    //SendAuditNotify(model.TrainingID, model.ID, (int)AuditType.ClassOpinion, reciver);
                //}
            }
            #endregion

            return RedirectToAction("Edit", new { id = model.ID, FromAudit = model.FromAudit });
        }

        //public JsonResult GetWorks(int workId, string title, int itemId, int classId, string deptId)
        //{
        //    var result = Mapper.Map<PFAppraiseView>(pFWorksRelationRepository.Query(null, classId, workId, itemId, deptId, title).FirstOrDefault());
        //    if (result == null)
        //    {
        //        result = new PFAppraiseView();
        //    }
        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        public string SetReturn(int appraiseId)
        {
            try
            {
                var appraiseData = pFAppraiseRepository.GetById(appraiseId);
                if (appraiseData != null)
                {
                    appraiseData.TLAudit = AuditProcessing;
                    appraiseData.TLAuditDate = null;
                    appraiseData.TLSignature = null;
                    appraiseData.CSAudit = AuditProcessing;
                    appraiseData.CSAuditDate = null;
                    appraiseData.CSSignature = null;
                    appraiseData.ManagerAudit = AuditProcessing;
                    appraiseData.ManagerAuditDate = null;
                    appraiseData.ManagerSignature = null;
                    pFAppraiseRepository.Update(appraiseData);
                }

                var appraiseWorksData = pFAppraiseWorksRepository.Query(appraiseId);
                foreach (var item in appraiseWorksData.ToList())
                {
                    item.IsAudit = false;
                    item.IsFinished = false;
                    item.TLAudit = AuditProcessing;
                    item.TLAuditDate = null;
                    item.TLSignature = null;
                    item.CSAudit = AuditProcessing;
                    item.CSAuditDate = null;
                    item.CSSignature = null;
                    item.ManagerAudit = AuditProcessing;
                    item.ManagerAuditDate = null;
                    item.ManagerSignature = null;
                    pFAppraiseWorksRepository.Update(item);
                }

                var auditData = auditRepository.FindByTarget(appraiseId, PF);
                if (auditData != null)
                {
                    auditData.State = (int)PFAuditState.Processing;
                    auditData.IsInvalid = false;
                }
                auditRepository.Update(auditData);

                return "success";
            }
            catch (Exception e)
            {
                return "failed";
            }
        }

        #region 匯出Excel
        /// <summary>
        /// 匯出Excel清單 (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportListExcel(int id)
        {
            //取出要匯出Excel的資料
            var appraiseData = pFAppraiseRepository.GetById(id);

            var personInfo = comPersonRepository.FindByPersonID(appraiseData.PersonId);
            string personStr = personInfo == null ? "" : personInfo.PersonName;

            //填寫過的班別
            List<int> classIdList = pFAppraiseWorksRepository.Query(appraiseData.ID).Select(q => q.ClassID).Distinct().ToList();
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                                                                            //建立Excel
                ExcelPackage ep = new ExcelPackage();
               
                foreach (var classId in classIdList)
                {
                    var classInfo = pFCategoryRepository.GetById(classId);
                    //建立Sheet，後方為定義Sheet的名稱
                    ExcelWorksheet sheet = ep.Workbook.Worksheets.Add(classInfo.Title);
                    sheet.Cells.Style.Font.Size = 10;
                   
                    var appraiseWorkList = pFAppraiseWorksRepository.Query(appraiseData.ID, 0, "", classId).ToList();

                    #region 合併儲存格
                    for (int i = 1; i <= 4; i++)
                    {
                        ExcelRange rangeCol = sheet.Cells[("{Letter}1:{Letter}4").Replace("{Letter}", Convert.ToChar(i + 64).ToString())];
                        rangeCol.Merge = true;
                        // 設定水平置中。
                        rangeCol.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        // 設定垂直置中。
                        rangeCol.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    }

                    int worksCount = appraiseWorkList.Count() > 0 ? appraiseWorkList.Count() : 1;
                    ExcelRange rangeRow = sheet.Cells[("E1:{Letter}1").Replace("{Letter}", Convert.ToChar((5 + worksCount - 1) + 64).ToString())];
                    rangeRow.Merge = true;
                    // 設定水平置中。
                    rangeRow.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    // 設定垂直置中。
                    rangeRow.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    #endregion

                    int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                    //第1列是標題列 
                    //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                    //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                    //取得做法可參考最後的參考連結。

                    int row = 1;    //列:橫的
                    sheet.Cells[row, col++].Value = "分類";
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = "項目";
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = "層級";
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = "點數";
                    sheet.Column(col).AutoFit();
                    sheet.Cells[row, col++].Value = classInfo.Title + "(" + personStr + ")";
                    sheet.Column(col).AutoFit();
                    row++;

                    #region 評比項目
                    int cateRow = 5;
                    int cateCol = 1;
                    int itemRow = 5;
                    int itemCol = 2;
                    int lvlRow = 5;
                    int lvlCol = 3;
                    List<ItemPosition> positionArr = new List<ItemPosition>();

                    var cateList = Mapper.Map<List<ViewModels.PFCategory.PFCategoryCateView>>(pFCategoryRepository.Query(true, appraiseData.DeptId, classInfo.ID, CATE).OrderBy(q => q.Sort));
                    foreach (var cate in cateList)
                    {//分類     
                        sheet.Cells[cateRow, cateCol].Value = cate.Title;
                        sheet.Column(cateCol).Width = 15;// 設置自動換行
                        sheet.Column(cateCol).Style.WrapText = true; // 設置自動換行
                        sheet.Column(cateCol).Style.Font.Color.SetColor(System.Drawing.Color.Red); // 設置文字顏色

                        var itemList = Mapper.Map<List<ViewModels.PFCategory.PFCategoryItmView>>(pFCategoryRepository.Query(true, appraiseData.DeptId, cate.ID, ITM, null, 2).OrderBy(q => q.Sort));
                        
                        int lvlCount = 0;
                        foreach (var item in itemList)
                        {//項目
                            var lvlList = Mapper.Map<List<ViewModels.PFCategory.PFCategoryLvlView>>(pFCategoryRepository.Query(true, appraiseData.DeptId, item.ID, LVL).OrderBy(q => q.Sort));
                            foreach (var lvl in lvlList)
                            {//層級
                                sheet.Cells[lvlRow, lvlCol].Value = lvl.Title;
                                sheet.Column(lvlCol).Width = 40;
                                sheet.Column(lvlCol).Style.WrapText = true; // 設置自動換行
                               
                                sheet.Cells[lvlRow, lvlCol+1].Value = lvl.Points;
                                sheet.Column(lvlCol+1).Style.WrapText = true; // 設置自動換行
                                sheet.Column(lvlCol+1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;// 設置自動換行
                                sheet.Column(lvlCol+1).AutoFit();
                                lvlRow++;                               
                            }

                            positionArr.Add(new ItemPosition()
                            {
                                ItemId = item.ID,
                                Position = itemRow,
                            });

                            //計算合併高度
                            ExcelRange itemRange = sheet.Cells[($"B{itemRow}:B{ itemRow + lvlList.Count() - 1}")];
                            itemRange.Merge = true;
                            // 設定垂直置中。
                            itemRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                            //合併評比結果欄位
                            for (int i = 0; i < worksCount; i++)
                            {
                                ExcelRange appraiseRelationRange = sheet.Cells[($"{Convert.ToChar(i + 64 + 5).ToString()}{itemRow}:{Convert.ToChar(i + 64 + 5).ToString()}{ itemRow + lvlList.Count() - 1}")];
                                appraiseRelationRange.Merge = true;
                                //// 設定水平置中。
                                //appraiseRelationRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //// 設定垂直置中。
                                //appraiseRelationRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                sheet.Column(i + 5).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            }

                            sheet.Cells[itemRow, itemCol].Value = item.Title;
                            sheet.Column(itemCol).Width = 20;// 設置自動換行
                            sheet.Column(itemCol).Style.WrapText = true; // 設置自動換行
                            
                            itemRow = itemRow + lvlList.Count();
                            cateRow = cateRow + lvlList.Count();
                            lvlCount += lvlList.Count();
                        }
                        //計算合併高度
                        ExcelRange cateRange = sheet.Cells[($"A{cateRow - lvlCount}:A{ cateRow - 1}")];
                        cateRange.Merge = true;
                        // 設定垂直置中。
                        cateRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    }
                    #endregion

                    #region 班別&作業
                    col = 5;//從E(5)列開始
                    int crCol = 5;
                    foreach (var work in appraiseWorkList)
                    {
                        //建立作業資料                    
                        //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                        row = 2;
                        var relationList = pFAppraiseRelationRepository.Query(work.AppraiseID, 0, 0, 0, work.ID);

                        var pfWork = pFWorkRepository.GetById(work.WorksID);
                        sheet.Cells[row, col].Value = pfWork.Points;
                        sheet.Column(col).Style.WrapText = true; // 設置自動換行
                        sheet.Column(col).AutoFit();
                        row++;
                        sheet.Cells[row, col].Value = work.WorkTitle;
                        sheet.Column(col).Style.WrapText = true; // 設置自動換行
                        sheet.Column(col).AutoFit();
                        row++;

                        int formPoint = 0;
                        var classRelationList = pFAppraiseRelationRepository.Query(appraiseData.ID, 0, 0, 0, work.WorksID);
                       
                        foreach (var cr in classRelationList)
                        {
                            formPoint += cr.Points;

                            #region 評比結果   
                            var positionInfo = positionArr.Where(q => q.ItemId == cr.ItmID).FirstOrDefault();
                            var index = positionInfo == null ? 0 : positionInfo.Position;//取得層級位置
                            sheet.Cells[index, crCol].Value = cr.Points;
                            #endregion
                        }
                        crCol++;

                        sheet.Cells[row, col].Value = formPoint * pfWork.Points;
                        sheet.Column(col).AutoFit();
                        col++;
                    }
                    #endregion
                    sheet.Column(crCol).AutoFit();
                    crCol++;
                }

                #region 匯出檔案
                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", appraiseData.SeasonTitle + "-" + personStr + "-員工技能評比表.xlsx");
                #endregion
            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion
    }

    public class ItemPosition
    {
        public int ItemId { get; set; }
        public int Position { get; set; }
    }
}