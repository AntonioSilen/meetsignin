﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.SignIn;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class SignInAdminController : BaseAdminController
    {
        private SignInRepository signInRepository = new SignInRepository(new MeetingSignInDBEntities());
        private MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());

        public ActionResult Index(SignInIndexView model)
        {
            if (model.MeetingID == 0)
            {
                return RedirectToAction("Index", "MeetingAdmin");
            }
            model.MeetingTitle = meetingRepository.FindBy(model.MeetingID).Title;
            var query = signInRepository.Query(model.IsSign, model.Name, model.MeetingID, model.JobID);
            var pageResult = query.ToPageResult<SignIn>(model);
            model.PageResult = Mapper.Map<PageResult<SignInView>>(pageResult);

            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    signInRepository.Delete(id);
        //    ShowMessage(true, "刪除成功");
        //    return RedirectToAction("Index", new { MeetingID  = id});
        //}

        public ActionResult SignIn(int id)
        {
            var data = signInRepository.FindBy(id);
            data.IsSign = true;
            data.SignStatus = (int)SignStatus.Signed;
            data.SignTime = DateTime.Now;
            signInRepository.Update(data);
            return RedirectToAction("Index", new { MeetingID = data.MeetingID});
        }
        public ActionResult Leave(int id)
        {
            var data = signInRepository.FindBy(id);
            data.IsSign = true;
            data.SignStatus = (int)SignStatus.Leave;
            //data.SignTime = DateTime.Now;
            signInRepository.Update(data);
            return RedirectToAction("Index", new { MeetingID = data.MeetingID});
        }

        public ActionResult ExportExcel(int mid)
        {
            try
            {
                string meetingTitle = meetingRepository.FindBy(mid).Title;
                var query = signInRepository.GetSignInInfoList(mid).OrderBy(q => q.DepartmentID).ToList();
                var dtHelper = new DataTableHelper<SignInView>();
                DataTable dt = dtHelper.ListToDataTable(query);
                string fileName = meetingTitle.Replace("/", "-") + "_名單.xlsx";
                string path = Server.MapPath("/FileUploads/SignInExcel/") + fileName;
                ExcelHelper.TableToExcel(dt, path, 2);
                return File(path, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return null;
            }
        }
    }
}