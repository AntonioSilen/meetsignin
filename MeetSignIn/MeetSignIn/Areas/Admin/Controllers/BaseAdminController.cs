﻿using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels;
using MeetSignIn.Models;
using MeetSignIn.Models.Approval;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{  
    /// <summary>
    /// 引用此controller可以取得當前語系，靜態文字會自動轉換
    /// </summary>
    [CultureFilter]
    public class BaseAdminController : Controller
    {
        public Language GetLanguage()
        {
            string lang = HttpContext.Request.RequestContext.RouteData.Values["lang"].ToString();
            return LanguageHelper.GetLanguage(lang);
        }

        public string officialHost = "220.228.58.43:18151";
        public string emptySignature = "0001.jpg";
        public string adminSignature = "0000.jpg";

        public string[] seniorList = { "1012", "0663", "1364", "1279", "0025" };
        public string[] juniorList = { "1077", "1853", "0163", "0367" };

        public int Draft = (int)ApprovalState.Draft;
        public int Approving = (int)ApprovalState.Approving;
        public int Approved = (int)ApprovalState.Approved;
        public int Reject = (int)ApprovalState.Reject;

        public int AuditProcessing = (int)FormAudit.Processing;
        public int AuditApproval = (int)FormAudit.Approval;
        public int AuditReject = (int)FormAudit.Reject;

        public AdminType Principal = AdminType.Principal;

        public DateTime DefaultDate = Convert.ToDateTime("1999/12/31");

        public readonly string FileUploads = System.Web.Configuration.WebConfigurationManager.AppSettings["FileUploads"];

        public ApprovalStateMachine _stateMachine;

        public string ControllerName
        {
            get
            {
                return this.ControllerContext.RouteData.Values["controller"].ToString();
            }
        }

        /// <summary>
        /// 取得圖片儲存路徑，取得方式為controller名稱，然後將Admin替換成Photo        
        /// </summary>
        public string PhotoFolder
        {
            get
            {
                string controllerName = ControllerName;
                string fileFolder = controllerName.Replace("Admin", "Photo");
                string fileUploads = Server.MapPath(FileUploads);
                string savePath = Path.Combine(fileUploads, fileFolder);
                return savePath;
            }
        }

        public bool modelState
        {
            get
            {
                try
                {
                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Content/Admin/ModelState.txt");
                    string text = System.IO.File.ReadAllText(path);
                    return string.IsNullOrEmpty(text) || text == "0" ? false : true;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 顯示訊息
        /// 成功時為綠色訊息
        /// 失敗時為紅色訊息
        /// </summary>        
        /// <param name="success">
        /// 是否成功 成功:true 失敗:false
        /// </param>
        /// <param name="message">
        /// 要顯示的訊息，若帶入空字串則預設成[success==true]為"成功"，[success==false]為"失敗"
        /// </param>
        public void ShowMessage(bool success, string message)
        {
            string tempDataKey = success ? "Result" : "Error";

            if (string.IsNullOrEmpty(message))
                message = success ? "Success" : "Failed";

            this.TempData[tempDataKey] = message;
        }

        /// <summary>
        /// 找不到時的頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult NotFound()
        {
            return View("~/Areas/Admin/Views/Shared/NotFound.cshtml");
        }

        /// <summary>
        /// 錯誤頁面
        /// </summary>
        /// <param name="errorMessage">
        /// 錯誤訊息
        /// </param>
        /// <returns></returns>
        public ActionResult Error(string errorMessage = "")
        {
            TempData["ErrorMessage"] = errorMessage;
            return View("~/Areas/Admin/Views/Shared/Error.cshtml");
        }

        /// <summary>
        /// CKEditor上傳圖片
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Upload(HttpPostedFileBase upload)
        {
            string newFileName = ImageHelper.SaveFile(upload, PhotoFolder);

            string photoFolder = ControllerName.Replace("Admin", "Photo");
            string savePath = string.Format("{0}/{1}/",
                FileUploads.Replace("~", ""),
                photoFolder
                );

            return Json(new { uploaded = 1, fileName = newFileName, url = savePath + newFileName });
        }

        public string GetKPIDept(int id)
        {
            KPIRelationRepository kPIRelationRepository = new KPIRelationRepository(new MeetingSignInDBEntities());
            var relationList = kPIRelationRepository.Query(id).Select(q => q.DeptName).ToList();
            string deptStr = string.Join(", ", relationList.ToArray());
            return deptStr;
        }

        public string GetAuditState(AuditLevel auditLevel)
        {
            string defaultPersonId = "0000";
            var levelStr = "";        
            if (auditLevel.ManagerReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.Manager).ToString() + ",";
            }
            if (auditLevel.ViceReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.Vice).ToString() + ",";
            }
            if (auditLevel.PresidentReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.President).ToString() + ",";
            }
            if (auditLevel.HRReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.HR).ToString() + ",";
            }
            if (auditLevel.PrincipalReview != defaultPersonId)
            {
                levelStr += ((int)Principal).ToString();
            }

            return levelStr;
        }

        public string GetPFAuditState(AuditLevel auditLevel)
        {
            string defaultPersonId = "0000";
            var levelStr = "";
            if (auditLevel.TLReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.TeamLeader).ToString() + ",";
            }
            if (auditLevel.CSReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.ClassSupervisor).ToString() + ",";
            }
            if (auditLevel.ManagerReview != defaultPersonId)
            {
                levelStr += ((int)AdminType.Manager).ToString() + ",";
            }         

            return levelStr;
        }

        public DateTime? SetAuditDate(int audit, DateTime? date)
        {
            if (audit > (int)FormAudit.Processing && (date == null || Convert.ToDateTime(date) < Convert.ToDateTime("2000/01/01")))
            {
                return DateTime.Now;
            }
            return (Convert.ToDateTime(date) < Convert.ToDateTime("2000/01/01") || Convert.ToDateTime(date) == DefaultDate) ? null : date;
        }

        public Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public string GetAuditRevicer(int state, int officer, int type = 0, string personId = "")
        {
            AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());            

            var superior = "";
            var superiorInfo = new Models.MainAdmin();
            switch (state)
            {
                case (int)AuditState.Processing://通知承辦人
                    superiorInfo = adminRepository.GetById(officer);
                    superior = superiorInfo == null ? "" : superiorInfo.Account;
                    break;
                case (int)AuditState.OfficerAudit://通知部門主管
                    if (!string.IsNullOrEmpty(personId))
                    {
                        //填表人部門
                        var personInfo = t8personRepository.FindByPersonId(personId);
                        var personDept = personInfo == null ? "" : personInfo.DeptID;

                        var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "", personDept).Select(q => q.JobID).ToList();
                        superiorInfo = adminRepository.Query(true, (int)AdminType.Manager).Where(q => q.DeptId == personDept).FirstOrDefault();
                        superior = superiorInfo == null ? "" : superiorInfo.Account;
                    }
                    break;
                case (int)AuditState.ManagerAudit://通知副總經理
                    superiorInfo = adminRepository.Query(true, (int)AdminType.Vice).FirstOrDefault();
                    superior = superiorInfo == null ? "" : superiorInfo.Account;
                    break;
                case (int)AuditState.ViceAudit://通知校長
                    //superiorInfo = adminRepository.Query(true, (int)Principal).FirstOrDefault();
                    superiorInfo = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                    superior = superiorInfo == null ? "" : superiorInfo.Account;
                    break;
                case (int)AuditState.PrincipalAudit://通知總經理
                    superiorInfo = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                    superior = superiorInfo == null ? "" : superiorInfo.Account;
                    break;
                case (int)AuditState.PresidentAudit://通知填寫人員
                    if (type < (int)AuditType.ClassActionPlan)
                    {//計畫表
                        superiorInfo = adminRepository.GetById(officer);
                        superior = superiorInfo == null ? "" : superiorInfo.Account;
                    }
                    else
                    {//課後表
                        superior = personId;
                    }
                    break;
            }
            return superior;
        }

        /// <summary>
        /// 提供允許執行動作清單
        /// </summary>
        /// <returns>允許執行動作清單</returns>
        public List<ApprovalAction> AvailableActions()
        {
            return _stateMachine.AvailableActions();
        }

        public int HandleState(string createrDeptId, int OfficerAudit, int ManagerAudit, int ViceAudit, int PresidentAudit, int PrincipalAudit, bool isTraining = true)
        {
            int AuditProcessing = (int)FormAudit.Processing;
            int AuditApproval = (int)FormAudit.Approval;
            int AuditReject = (int)FormAudit.Reject;

            int RejectState = (int)AuditState.Reject;
            int CurrentState = (int)AuditState.OfficerAudit;

            var auditFlow = AdminType.Manager;

            AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());

            var finalAudit = auditLevelRepository.FindByDept(createrDeptId);
            var levelArr = GetAuditState(finalAudit).Split(',');
            bool isManager = levelArr.Contains(((int)AdminType.Manager).ToString());
            bool isVice = levelArr.Contains(((int)AdminType.Vice).ToString());
            bool isPres = levelArr.Contains(((int)AdminType.President).ToString());
            bool isPrin = levelArr.Contains(((int)Principal).ToString());
            if (OfficerAudit == AuditApproval)
            {//承辦人核准
                if (isManager)
                {//需主管審核
                    if (auditFlow != AdminType.Manager)
                    {
                        return CurrentState;
                    }
                    if (auditFlow == AdminType.Manager)
                    {//傳簽到主管
                        if (ManagerAudit <= AuditProcessing)
                        {//待審
                            return CurrentState;
                        }
                        if (ManagerAudit == AuditApproval)
                        {//核准
                            CurrentState = (int)AuditState.ManagerAudit;
                            auditFlow = AdminType.Vice;
                        }
                        else if (ManagerAudit == AuditReject)
                        {//駁回
                            CurrentState = RejectState;
                        }
                    }
                }
                else
                {//傳簽
                    auditFlow = AdminType.Vice;
                    CurrentState = (int)AuditState.ManagerAudit;
                }

                if (CurrentState != RejectState)
                {
                    if (isVice)
                    {//需副總審核
                        if (auditFlow != AdminType.Vice)
                        {
                            return CurrentState;
                        }
                        if (auditFlow == AdminType.Vice)
                        {//傳簽到副總
                            if (ViceAudit <= AuditProcessing)
                            {//待審
                                return CurrentState;
                            }
                            if (ViceAudit == AuditApproval)
                            {//核准
                                CurrentState = (int)AuditState.ViceAudit;
                                auditFlow = AdminType.President;
                            }
                            else if (ViceAudit == AuditReject)
                            {//駁回
                                CurrentState = RejectState;
                            }
                        }
                    }
                    else
                    {//傳簽
                        auditFlow = AdminType.President;
                        CurrentState = (int)AuditState.ViceAudit;
                    }
                }
                if (CurrentState != RejectState)
                {
                    if (isPres)
                    {//需總經理審核
                        if (auditFlow != AdminType.President)
                        {
                            return CurrentState;
                        }
                        if (auditFlow == AdminType.President)
                        {//傳簽到總經理
                            if (PresidentAudit <= AuditProcessing)
                            {//待審
                                return CurrentState;
                            }
                            if (PresidentAudit == AuditApproval)
                            {//核准
                                CurrentState = (int)AuditState.PresidentAudit;
                                auditFlow = Principal;
                            }
                            else if (PresidentAudit == AuditReject)
                            {//駁回
                                CurrentState = RejectState;
                            }
                        }
                    }
                    else
                    {//傳簽
                        auditFlow = Principal;
                        CurrentState = (int)AuditState.PresidentAudit;
                    }
                }
                if (CurrentState != RejectState)
                {
                    if (!isTraining)
                    {
                        if (auditFlow == Principal)
                        {
                            CurrentState = (int)AuditState.Approval;
                        }
                        return CurrentState;
                    }
                    if (isPrin)
                    {//需校長審核
                        if (auditFlow != Principal)
                        {
                            return CurrentState;
                        }
                        if (auditFlow == Principal)
                        {//傳簽到校長
                            if (PrincipalAudit <= AuditProcessing)
                            {//待審
                                return CurrentState;
                            }
                            if (PrincipalAudit == AuditApproval)
                            {//核准
                                CurrentState = (int)AuditState.Approval;
                            }
                            else if (PrincipalAudit == AuditReject)
                            {//駁回
                                CurrentState = RejectState;
                            }
                        }
                    }
                    else
                    {
                        CurrentState = (int)AuditState.Approval;
                    }
                }
            }

            return CurrentState;
        }

        public int HandlePFState(string createrDeptId, int TLAudit, int CSAudit, int ManagerAudit)
        {
            int AuditProcessing = (int)FormAudit.Processing;
            int AuditApproval = (int)FormAudit.Approval;
            int AuditReject = (int)FormAudit.Reject;

            int RejectState = (int)PFAuditState.Reject;
            int CurrentState = (int)PFAuditState.Processing;

            var auditFlow = AdminType.TeamLeader;

            AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());

            var finalAudit = auditLevelRepository.FindByDept(createrDeptId);
            var levelArr = GetPFAuditState(finalAudit).Split(',');
            bool isTL = levelArr.Contains(((int)AdminType.TeamLeader).ToString());
            bool isCS = levelArr.Contains(((int)AdminType.ClassSupervisor).ToString());
            bool isManager = levelArr.Contains(((int)AdminType.Manager).ToString());


            if (CurrentState != RejectState)
            {
                if (isTL)
                {//需班組長審核
                    if (auditFlow != AdminType.TeamLeader)
                    {
                        return CurrentState;
                    }
                    if (auditFlow == AdminType.TeamLeader)
                    {//傳簽到課級主管
                        if (TLAudit <= AuditProcessing)
                        {//待審
                            return CurrentState;
                        }
                        if (TLAudit == AuditApproval)
                        {//核准
                            CurrentState = (int)PFAuditState.TLAudit;
                            auditFlow = AdminType.ClassSupervisor;
                        }
                        else if (CSAudit == AuditReject)
                        {//駁回
                            CurrentState = RejectState;
                        }
                    }
                }
                else
                {//傳簽
                    auditFlow = AdminType.ClassSupervisor;
                    CurrentState = (int)PFAuditState.TLAudit;
                }
            }

            if (CurrentState != RejectState)
            {
                if (isCS)
                {//需課級主管審核
                    if (auditFlow != AdminType.ClassSupervisor)
                    {
                        return CurrentState;
                    }
                    if (auditFlow == AdminType.ClassSupervisor)
                    {//傳簽到主管
                        if (CSAudit <= AuditProcessing)
                        {//待審
                            return CurrentState;
                        }
                        if (CSAudit == AuditApproval)
                        {//核准
                            CurrentState = (int)PFAuditState.CSAudit;
                            auditFlow = AdminType.Manager;
                        }
                        else if (CSAudit == AuditReject)
                        {//駁回
                            CurrentState = RejectState;
                        }
                    }
                }
            }
            else
            {//傳簽
                auditFlow = AdminType.Manager;
                CurrentState = (int)PFAuditState.CSAudit;
            }

            if (CurrentState != RejectState)
            {
                if (isManager)
                {//需主管審核
                    if (auditFlow != AdminType.President)
                    {
                        return CurrentState;
                    }
                    if (auditFlow == AdminType.President)
                    {//傳簽到主管
                        if (ManagerAudit <= AuditProcessing)
                        {//待審
                            return CurrentState;
                        }
                        if (ManagerAudit == AuditApproval)
                        {//核准
                            CurrentState = (int)AuditState.Approval;
                        }
                        else if (ManagerAudit == AuditReject)
                        {//駁回
                            CurrentState = RejectState;
                        }
                    }
                }
                else
                {
                    CurrentState = (int)AuditState.Approval;
                }
            }            

            return CurrentState;
        }

        public JsonResult GetPersonByDeptId(string DeptId)
        {
            var query = t8personRepository.GetPersonQuery("", "", DeptId).ToList();
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(query);
        }

        /// <summary>
        /// 設定簽章
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <param name="auditLevelData"></param>
        public void SetSignature<T>(T model, AuditLevel auditLevelData) where T : AuditView
        {
            Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

            int AuditProcessing = (int)FormAudit.Processing;

            //主管
            if (model.ManagerAudit > AuditProcessing)
            {
                model.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.ManagerReview);
                if (personInfo != null)
                {
                    model.ManagerDeptId = personInfo.DeptId;
                    model.ManagerDeptName = personInfo.DepartmentStr;
                    model.ManagerName = personInfo.Name;
                }
            }
            //副總
            if (model.ViceAudit > AuditProcessing)
            {
                model.ViceSignature = auditLevelData.ViceReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.ViceReview);
                if (personInfo != null)
                {
                    model.ViceDeptId = personInfo.DeptId;
                    model.ViceDeptName = personInfo.DepartmentStr;
                    model.ViceName = personInfo.Name;
                }
            }
            //總經理
            if (model.PresidentAudit > AuditProcessing)
            {
                model.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.PresidentReview);
                if (personInfo != null)
                {
                    model.PresidentDeptId = personInfo.DeptId;
                    model.PresidentDeptName = personInfo.DepartmentStr;
                    model.PresidentName = personInfo.Name;
                }
            }
            //校長
            if (model.PrincipalAudit > AuditProcessing)
            {
                model.PrincipalSignature = auditLevelData.PrincipalReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.PrincipalReview);
                if (personInfo != null)
                {
                    model.PrincipalDeptId = personInfo.DeptId;
                    model.PrincipalName = personInfo.DepartmentStr;
                    model.PrincipalName = personInfo.Name;
                }
            }
        }
    }
}