﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.DeptKPI;
using MeetSignIn.Areas.Admin.ViewModels.KPI;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPI;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class DeptKPIAdminController : BaseAdminController
    {
        // GET: Admin/DeptKPIAdmin
        private KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
        private KPIRelationRepository kPIRelationRepository = new KPIRelationRepository(new MeetingSignInDBEntities());
        private DeptKPIPercentageRepository deptKPIPercentageRepository = new DeptKPIPercentageRepository(new MeetingSignInDBEntities());
        private PersonKPIRepository personKPIRepository = new PersonKPIRepository(new MeetingSignInDBEntities());
        private PersonKPIStateRepository personKPIStateRepository = new PersonKPIStateRepository(new MeetingSignInDBEntities());
        private PersonKPIMonthRepository personKPIMonthRepository = new PersonKPIMonthRepository(new MeetingSignInDBEntities());
        private CheckRepository checkRepository = new CheckRepository(new MeetingSignInDBEntities());
        private comGroupPersonRepository comGroupPersonRepository = new comGroupPersonRepository(new T8ERPEntities());
        private comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());

        public ActionResult Index(KPIIndexView model)
        {
            List<string> deptList = new List<string>();
            if (!string.IsNullOrEmpty(model.Dept))
            {
                var deptArr = model.Dept.Split(',');
                foreach (var item in deptArr.ToList())
                {
                    string dept = item.Trim();
                    deptList.Add(comDepartmentRepository.FindByDeptName(dept).DeptId);
                }
            }

            var parentKpiInfo = kPIRepository.GetByTitle(model.ParentKPITitle);
            int parentKpiId = parentKpiInfo == null ? 0 : parentKpiInfo.ID;
            var query = kPIRepository.Query(model.IsOnline, model.Title, model.Type, 2, parentKpiId, "", null, model.ApprovalState, deptList);
            var pageResult = query.ToPageResult<KPI>(model);
            model.PageResult = Mapper.Map<PageResult<KPIView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                if (item.ParentKPIID != 0)
                {
                    item.ParentKPITitle = kPIRepository.GetById(item.ParentKPIID).Title;
                }
                item.Dept = GetKPIDept(item.ID);
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new DeptKPIView();
            model.CheckDate = DateTime.Now.AddMonths(6);
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            var query = kPIRepository.GetById(id);
            model = Mapper.Map<DeptKPIView>(query);
            model.PersonKPIList = Mapper.Map<List<PersonKPIView>>(personKPIRepository.Query(null, true, id));
            foreach (var item in model.PersonKPIList)
            {
                var personInfo = t8personRepository.FindByPersonId(item.PersonId);
                item.Name = personInfo == null ? "" : personInfo.PersonName;
                item.DeptName = personInfo == null ? "" : personInfo.DeptName;

                item.DivisionInfo = deptKPIPercentageRepository.GetDivisionInfo(item.KPIID, item.ID, item.PersonId);
            }
           
            var parentKPIInfo = kPIRepository.GetById(model.ParentKPIID);
            model.ParentKPITitle = parentKPIInfo == null ? "" : parentKPIInfo.Title;         

            #region 部門關聯
            var relationList = kPIRelationRepository.Query(model.ID).Select(q => q.DeptName);
            model.Dept = String.Join(", ", relationList.ToArray());
            #endregion

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(KPIView model)
        {
            if (ModelState.IsValid)
            {
                KPI data = Mapper.Map<KPI>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = kPIRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    kPIRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }
                #region 部門關聯
                if (!string.IsNullOrEmpty(model.Dept))
                {
                    var oldData = kPIRelationRepository.Query(model.ID);
                    foreach (var item in oldData.ToList())
                    {
                        kPIRelationRepository.Delete(item.ID);
                    }
                    var deptArr = model.Dept.Split(',');
                    foreach (var item in deptArr)
                    {
                        var dept = comDepartmentRepository.FindByDeptName(item.Trim());
                        kPIRelationRepository.Insert(new KPIRelation()
                        {
                            KPIID = model.ID,
                            KPITitle = model.Title,
                            DeptId = dept.DeptId,
                            DeptName = dept.DeptName,
                        });
                    }
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            kPIRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 取得部門指標
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetInfo(int id)
        {
            var result = Mapper.Map<KPIView>(kPIRepository.GetById(id));
            result.Dept = GetKPIDept(id);
            result.CheckDateStr = result.CheckDate.ToString("yyyy/MM/dd HH:mm");
            //result.TypeStr = result.TypeOptions.Where(q => q.Value == result.Type.ToString()).FirstOrDefault().Text;
            //result.LevelStr = result.LevelOptions.Where(q => q.Value == result.Type.ToString()).FirstOrDefault().Text;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 儲存部門指標
        /// </summary>
        /// <param name="id"></param>
        /// <param name="title"></param>
        /// <param name="type"></param>
        /// <param name="level"></param>
        /// <param name="dept"></param>
        /// <param name="parentkpiId"></param>
        /// <param name="checkdate"></param>
        /// <param name="description"></param>
        /// <param name="isonline"></param>
        /// <param name="approvalState"></param>
        /// <returns></returns>
        public JsonResult SaveInfo(int id, string title, int type, int level, string dept, int parentkpiId, DateTime checkdate, string description, bool isonline, int approvalState)
        {
            approvalState = (int)ApprovalState.Approved;
            try
            {
                var query = new KPI();
               
                if (id == 0)
                {
                    query = new KPI()
                    {
                        ID = id,
                        Title = title,
                        Type = type,
                        Level = level,
                        ParentKPIID = parentkpiId,
                        CheckDate = checkdate,
                        Description = description,
                        IsOnline = isonline,
                        ApprovalState = approvalState,
                    };
                    id = kPIRepository.Insert(query);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    query = kPIRepository.GetById(id);
                    query.ID = id;
                    query.Title = title;
                    query.Type = type;
                    query.Level = level;
                    query.ParentKPIID = parentkpiId;
                    query.CheckDate = checkdate;
                    query.Description = description;
                    query.IsOnline = isonline;
                    query.ApprovalState = approvalState;
                    kPIRepository.Update(query);
                    ShowMessage(true, "修改成功");
                }
                #region 部門關聯
                if (!string.IsNullOrEmpty(dept))
                {
                    var oldData = kPIRelationRepository.Query(id);
                    foreach (var item in oldData.ToList())
                    {
                        kPIRelationRepository.Delete(item.ID);
                    }
                    var deptArr = dept.Split(',');
                    foreach (var item in deptArr)
                    {
                        var deptInfo = comDepartmentRepository.FindByDeptName(item.Trim());
                        kPIRelationRepository.Insert(new KPIRelation()
                        {
                            KPIID = id,
                            KPITitle = title,
                            DeptId = deptInfo.DeptId,
                            DeptName = deptInfo.DeptName,
                        });
                    }
                }
                #endregion

                #region 送審
                //if (approvalState == (int)ApprovalState.Approving)
                //{
                //    var newCheck = new Check()
                //    {
                //        TargetID = id,
                //        TargetType = (int)TargetType.KPI,
                //        ApprovalState = (int)ApprovalState.Approving,
                //        CreateDate = DateTime.Now,
                //        Creater = AdminInfoHelper.GetAdminInfo().ID,
                //    };
                //    checkRepository.Insert(newCheck);
                //}           
                #endregion

                query = kPIRepository.GetById(id);
                var result = Mapper.Map<KPIView>(query);
                result.CheckDateStr = result.CheckDate.ToString("yyyy/MM/dd HH:mm");
                result.TypeStr = result.TypeOptions.Where(q => q.Value == result.Type.ToString()).FirstOrDefault().Text;
                result.LevelStr = result.LevelOptions.Where(q => q.Value == result.Level.ToString()).FirstOrDefault().Text;

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public string Save_Participant(int deptKpiId, List<string> PersonIdList)
        {
            try
            {
                var kpiInfo = kPIRepository.GetById(deptKpiId);
                if (PersonIdList.Count() > 0 && kpiInfo != null)
                {
                    var joinedList = personKPIRepository.Query(null, true, deptKpiId, "", "", 0, false).Select(q => q.PersonId).ToList();
                    var notjoinList = joinedList.Where(q => !PersonIdList.Contains(q)).ToList();
                    foreach (var item in PersonIdList)
                    {                        
                        var data = personKPIRepository.GetByPersonIdAndKPIID(deptKpiId, item);                
                        var personInfo = t8personRepository.FindByPersonId(item);
                        if (data == null && personInfo != null)
                        {//新增個人目標
                            data = new PersonKPI();
                            data.KPIID = deptKpiId;
                            data.KPITitle = kpiInfo.Title;
                            data.KPIDescription = kpiInfo.Description;
                            data.PersonId = item;
                            data.DeptId = personInfo.DeptID;
                            data.State = 1;
                            data.ApprovalState = 0;
                            data.IsLatest = true;
                            data.IsInvalid = false;
                            data.CreateDate = DateTime.Now;
                            personKPIRepository.Insert(data);

                            #region 建立空白進度 四個季度
                            for (int i = 1; i <= 4; i++)
                            {
                                personKPIStateRepository.Insert(new PersonKPIState()
                                {
                                    PersonKPIID = data.ID,
                                    PersonID = data.PersonId,
                                    DeptID = data.DeptId,
                                    Sort = 0,
                                    Title = "Q" + i.ToString(),
                                    IsAssessed = false,
                                    IsLatest = true,
                                    IsFinished = false,
                                    CreateDate = data.CreateDate
                                });
                            }
                            #endregion

                            #region 建立空白進度 12個月分
                            DateTime Now = DateTime.Now;
                            for (int i = 1; i <= 12; i++)
                            {
                                personKPIMonthRepository.Insert(new PersonKPIMonth()
                                {
                                    PersonKPIID = data.ID,
                                    PersonID = data.PersonId,
                                    DeptID = data.DeptId,
                                    Month = i,
                                    ToDo = "",
                                    Expect = "",
                                    Complete = "",
                                    Percentage = 0,
                                    Type = (int)InfoState.Initial,

                                    IsFinished = false,
                                    Deadline = new DateTime(Now.Year, i, 1).AddMonths(1).AddDays(-1),
                                    CreateDate = data.CreateDate
                                });
                            }
                            #endregion
                        }

                    }

                    foreach (var item in notjoinList)
                    {
                        var data = personKPIRepository.GetByPersonIdAndKPIID(deptKpiId, item);
                        data.IsLatest = false;
                        data.IsInvalid = true;
                        personKPIRepository.Update(data);
                    }
                }

                return "success";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SavePercentage(int deptKpiId, string jsonData)
        {
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    //停用舊分工資料
                    var oldDatList = deptKPIPercentageRepository.Query(null, deptKpiId).ToList();
                    foreach (var item in oldDatList)
                    {
                        item.IsInvalid = true;
                        item.IsLatest = false;
                    }

                    //新增分工資料
                    var datas = JsonConvert.DeserializeObject<KPIPercentageDataModel>(jsonData);
                    var dataList = datas.datas;
                    foreach (var item in dataList)
                    {
                        var newPercentage = new DeptKPIPercentage() {
                            DeptKPIID = deptKpiId,
                            PersonKPIID = Convert.ToInt32(item.PersonKPIID),
                            Title = "",
                            QuarterTitle = "",
                            Division = item.Division,
                            Rank = item.Rank,
                            Percentage = item.Percentage,
                            PersonId = item.PersonId,
                            DeptId = item.DeptId,
                            IsInvalid = false,
                            IsLatest = true,
                            CreateDate = item.CreateDate,
                        };
                        deptKPIPercentageRepository.Insert(newPercentage);
                    }

                }
                return "success";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }

    public class KPIPercentageDataModel
    {
        public List<DeptKPIPercentage> datas { get; set; }
    }
}