﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.ClassOpinion;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Areas.Admin.ViewModels.TrainingPerson;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class TrainingPersonAdminController : BaseAdminController
    {
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());
        private InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
        //private ClassActionPlanRepository classActionPlanRepository = new ClassActionPlanRepository(new MeetingSignInDBEntities());
        private ClassOpinionRepository classOpinionRepository = new ClassOpinionRepository(new MeetingSignInDBEntities());
        //private ClassReportRepository classReportRepository = new ClassReportRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());

        public string YaHeiUILight = "Microsoft YaHei UI Light";

        public ActionResult Index(TrainingPersonIndexView model)
        {
            if (model.TraningID == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            var trainingInfo = trainingRepository.FindBy(model.TraningID);
            if (trainingInfo != null)
            {
                model.TraningTitle = trainingInfo.Title;
                model.Category = trainingInfo.Category;
                model.Creater = trainingInfo.Creater;
                model.GroupType = trainingInfo.GroupType;
                if (trainingInfo.Category != (int)Category.External)
                {
                    var trainingPlan = internalPlanRepository.FindValidPlan(model.TraningID);
                    if (trainingPlan != null)
                    {
                        model.HasLearning = trainingPlan.Learning;
                    }

                }
            }

            var query = trainingPersonRepository.Query(model.IsSign, model.Name, model.TraningID, model.JobID);
            model.TrainingPersonList = Mapper.Map<List<TrainingPersonView>>(query);
            model.TrainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(model.TraningID));
            foreach (var item in model.TrainingPersonList)
            {
                var acceptanceData = internalAcceptanceRepository.Query(item.TraningID, item.JobID).FirstOrDefault();
                item.Score = acceptanceData == null || acceptanceData.Score == null ? 0 : (int)acceptanceData.Score;
                item.Suggestion = acceptanceData == null ? "" : acceptanceData.Suggestion;

                if (model.Category == (int)Category.External)
                {
                    //外訓簽核狀態
                    var EAAudit = auditRepository.Query(null, 0, (int)AuditType.ExternalAcceptance, null, "", item.JobID, item.TraningID).FirstOrDefault();
                    if (EAAudit != null)
                    {
                        if (EAAudit.IsInvalid)
                        {
                            item.EAStatus = "駁回";
                        }
                        else
                        {
                            item.EAStatus = EnumHelper.GetDescription((AuditState)EAAudit.State);
                        }
                    }
                    else
                    {
                        item.EAStatus = externalAcceptanceRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                    }

                    //是否需外訓驗收
                    var externalPlan = externalPlanRepository.Query(item.TraningID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (externalPlan != null)
                    {
                        //"6"為課後意見
                        item.HasAcceptance = !string.IsNullOrEmpty(externalPlan.Inspection.Replace(",", "").Replace("6", "")) && model.TrainingInfo.GroupType == (int)GroupType.Person;
                        item.HasOpinion = externalPlan.Inspection.IndexOf("6") >= 0;
                    }
                }
                else if (model.Category == (int)Category.Internal)
                {
                    //內訓簽核狀態
                    var IAAudit = auditRepository.Query(null, 0, (int)AuditType.InternalAcceptance, null, "", item.JobID, item.TraningID).FirstOrDefault();
                    if (IAAudit != null)
                    {
                        if (IAAudit.IsInvalid)
                        {
                            item.IAStatus = "駁回";
                        }
                        else
                        {
                            item.IAStatus = EnumHelper.GetDescription((AuditState)IAAudit.State);
                        }
                    }
                    else
                    {
                        item.IAStatus = internalAcceptanceRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                    }

                    //是否需內訓驗收
                    var internalPlan = internalPlanRepository.Query(item.TraningID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (trainingInfo.SeriesParentID != 0)
                    {
                        internalPlan = internalPlanRepository.Query(trainingInfo.SeriesParentID).OrderByDescending(q => q.ID).FirstOrDefault();
                    }
                    if (internalPlan != null)
                    {
                        item.HasAcceptance = (/*internalPlan.Learning ||*/ internalPlan.Behavior || internalPlan.Outcome) && model.TrainingInfo.GroupType == (int)GroupType.Person;
                        item.HasOpinion = internalPlan.Response;
                    }
                }
                else if (model.Category == (int)Category.Seminar)
                {
                    item.HasOpinion = true;
                }

                //課後意見填寫狀態
                var opinionAudit = auditRepository.Query(null, 0, (int)AuditType.ClassOpinion, null, "", item.JobID, item.TraningID).FirstOrDefault();
                if (opinionAudit != null)
                {
                    if (opinionAudit.IsInvalid)
                    {
                        item.OpinionStatus = "駁回";
                    }
                    else
                    {
                        item.OpinionStatus = EnumHelper.GetDescription((AuditState)opinionAudit.State);
                    }
                }
                else
                {
                    item.OpinionStatus = classOpinionRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                }
                model.TrainingPersonList = model.TrainingPersonList.OrderBy(q => q.SignStatus).ThenBy(q => q.OpinionStatus).ToList();

                #region 停用
                //var actionPlanAudit = auditRepository.Query(null, 0, (int)AuditType.ClassActionPlan, null, "", item.JobID, item.TraningID).FirstOrDefault();
                //if (actionPlanAudit != null)
                //{
                //    if (actionPlanAudit.IsInvalid)
                //    {
                //        item.ActionPlanStatus = "駁回";
                //    }
                //    else
                //    {
                //        item.ActionPlanStatus = EnumHelper.GetDescription((AuditState)actionPlanAudit.State);
                //    }
                //}
                //else
                //{
                //    item.ActionPlanStatus = classActionPlanRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                //}

                //var reportAudit = auditRepository.Query(null, 0, (int)AuditType.ClassReport, null, "", item.JobID, item.TraningID).FirstOrDefault();
                //if (reportAudit != null)
                //{
                //    if (reportAudit.IsInvalid)
                //    {
                //        item.ReportStatus = "駁回";
                //    }
                //    else
                //    {
                //        item.ReportStatus = EnumHelper.GetDescription((AuditState)reportAudit.State);
                //    }
                //}
                //else
                //{
                //    item.ReportStatus = classReportRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                //}
                #endregion

                #region 判斷系列課程簽到狀態
                DateTime now = DateTime.Now;
                List<int> seriesTrainingIdList = trainingRepository.Query("", now.AddYears(-5), now.AddYears(5), "", "", 0, 0, 0, item.TraningID).Select(q => q.ID).ToList();
                //if (trainingPersonRepository.Query(true, "", 0, item.JobID).Count() > 0)
                //{
                //    item.SignStatus = 1;
                //}
                #endregion
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var personInfo = trainingPersonRepository.GetById(id);
            trainingPersonRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", "PersonAdmin", new { jid = personInfo.JobID });
        }

        public ActionResult SignIn(int id)
        {
            var data = trainingPersonRepository.FindBy(id);
            var trainingInfo = trainingRepository.GetById(data.TraningID);
            data.IsSign = true;
            data.SignStatus = (int)SignStatus.Signed;
            data.SignTime = trainingInfo == null ? DateTime.Now : trainingInfo.StartDate;
            trainingPersonRepository.Update(data);
            return RedirectToAction("Index", new { TraningID = data.TraningID });
        }
        public ActionResult Leave(int id)
        {
            var data = trainingPersonRepository.FindBy(id);
            data.IsSign = true;
            data.SignStatus = (int)SignStatus.Leave;
            //data.SignTime = DateTime.Now;
            trainingPersonRepository.Update(data);
            return RedirectToAction("Index", new { TraningID = data.TraningID });
        }

        //public ActionResult ExportExcel(int tid)
        //{
        //    string path = "";
        //    string fileName = "";
        //    try
        //    {
        //        string meetingTitle = trainingRepository.GetById(tid).Title;
        //        var query = trainingPersonRepository.GetSignInInfoList(tid).OrderBy(q => q.DeptID).ToList();
        //        var dtHelper = new DataTableHelper<TrainPersonJoinPersonAndDepartment>();
        //        DataTable dt = dtHelper.ListToDataTable(query);
        //        fileName = meetingTitle.Replace("/", "-").Trim() + "_名單.xlsx";
        //        path = Server.MapPath("/FileUploads/TrainingPersonExcel/") + fileName;
        //        ExcelHelper.TableToExcel(dt, path);
        //        return File(path, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
        //    }
        //    catch (Exception e)
        //    {
        //        Response.Write(e.Message);
        //        string msg = e.Message + " | " + path + " | " + fileName;
        //        ShowMessage(false, msg);
        //        return Json(msg);
        //    }
        //}

        public string ChangePassed(int id, bool passed)
        {
            try
            {
                var data = trainingPersonRepository.GetById(id);
                data.IsPassed = passed;
                trainingPersonRepository.Update(data);

                var internalAcceptance = internalAcceptanceRepository.Query(data.TraningID, data.JobID).FirstOrDefault();
                if (internalAcceptance != null)
                {
                    internalAcceptance.IsPassed = passed;
                    internalAcceptanceRepository.Update(internalAcceptance);
                    ShowMessage(true, "已更新資料");
                    return passed ? "通過" : "未通過";
                }
                else
                {
                    internalAcceptanceRepository.Insert(new InternalAcceptance()
                    {
                        TrainingID = data.TraningID,
                        PersonID = data.JobID,
                        Name = t8personRepository.FindByPersonId(data.JobID).Name,
                        IsPassed = passed
                    });
                    ShowMessage(true, "已更新資料");
                    return passed ? "通過" : "未通過";
                }
            }
            catch (Exception e)
            {
                ShowMessage(false, "更新失敗" + e.Message);
                return "failed : " + e.Message;
            }
            return "failed";
        }
        public string SaveScore(int trainingId, string scoreData)
        {
            try
            {
                List<PersonScore> scoreList = JsonConvert.DeserializeObject<List<PersonScore>>(scoreData);
                //JObject json = JObject.Parse(scoreData);
                if (scoreList != null && scoreList.Count() > 0)
                {
                    foreach (var item in scoreList)
                    {
                        var internalAcceptance = internalAcceptanceRepository.Query(trainingId, item.PersonId).FirstOrDefault();
                        if (internalAcceptance != null)
                        {
                            internalAcceptance.Score = item.Score;
                            internalAcceptance.Suggestion = item.Suggestion;
                            internalAcceptanceRepository.Update(internalAcceptance);
                            ShowMessage(true, "已更新資料");
                        }
                        else
                        {
                            internalAcceptanceRepository.Insert(new InternalAcceptance()
                            {
                                TrainingID = trainingId,
                                PersonID = item.PersonId,
                                Name = t8personRepository.FindByPersonId(item.PersonId).Name,
                                Score = item.Score,
                                Suggestion = item.Suggestion
                            });
                        }
                    }
                }
                //var data = trainingPersonRepository.GetById(id);
                //data.IsPassed = passed;
                //trainingPersonRepository.Update(data);
                //ShowMessage(true, "已更新資料");
                //return passed ? "通過" : "未通過";
            }
            catch (Exception e)
            {
                ShowMessage(false, "更新失敗" + e.Message);
                return "failed : " + e.Message;
            }
            return "";
        }

        #region 匯出Excel
        /// <summary>
        /// 匯出Excel (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportExcel(int id)
        {
            //取出要匯出Excel的資料
            var trainingInfo = trainingRepository.GetById(id);
            string trainingTitle = trainingInfo == null ? "" : trainingInfo.Title;

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                #region 簽到資料
                var query = trainingPersonRepository.GetSignInInfoList(id).OrderBy(q => q.DeptID).ToList();

                //建立Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("簽到名單");
                sheet.Cells.Style.Font.Size = 10;
                var fillColor = System.Drawing.Color.Salmon;
                int row = 1;    //列:橫的
                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //第1列是標題列 
                //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                //取得做法可參考最後的參考連結。

                sheet.Cells[row, col].Value = "編號";
                sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGoldenrodYellow); // 設置背景顏色
                col++;
                sheet.Cells[row, col].Value = trainingInfo.ID;
                sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray); // 設置背景顏色
                row++;
                col = 1;

                sheet.Cells[row, col].Value = "標題";
                sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGoldenrodYellow); // 設置背景顏色
                col++;
                sheet.Cells[row, col].Value = trainingInfo.Title;
                sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray); // 設置背景顏色

                row = 3;
                col = 1;
                sheet.Cells[row, col++].Value = "部門";
                sheet.Cells[row, col++].Value = "人員";
                sheet.Cells[row, col++].Value = "是否簽到";
                sheet.Cells[row, col++].Value = "簽到時間";

                if (trainingInfo.Cert != 2 && trainingInfo.Cert != 0)
                {
                    sheet.Cells[row, col++].Value = "是否通過";
                    sheet.Column(col).AutoFit();
                }

                sheet.Cells[row, col++].Value = "信箱";
                sheet.Column(col).AutoFit();

                sheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Row(row).Style.Fill.BackgroundColor.SetColor(fillColor); // 設置背景顏色
                for (int c = 1; c <= 5; c++)
                {
                    sheet.Column(c).Style.Font.Name = YaHeiUILight;
                }

                row++;
                foreach (var tp in query.OrderBy(q => q.SignStatus).ToList())
                {
                    col = 1;
                    #region 抽籤名單                
                    List<ItemPosition> positionArr = new List<ItemPosition>();

                    fillColor = System.Drawing.Color.LightBlue;

                    //var personInfo = t8personRepository.FindByJobID(tp.PersonId);
                    string personName = tp.Name;
                    string deptName = tp.DeptName;

                    sheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Row(row).Style.Fill.BackgroundColor.SetColor(fillColor); // 設置背景顏色

                    //sheet.Cells[row, col++].Value = trainingInfo.ID;
                    //sheet.Column(col).Style.WrapText = true; // 設置自動換行
                    //sheet.Column(col).AutoFit();
                    ////sheet.Column(col).Style.Font.Color.SetColor(System.Drawing.Color.Red); // 設置文字顏色

                    //sheet.Cells[row, col++].Value = trainingInfo.Title;
                    //sheet.Column(col).Style.WrapText = true; // 設置自動換行
                    //sheet.Column(col).AutoFit();

                    sheet.Cells[row, col++].Value = deptName;
                    sheet.Column(col).AutoFit();

                    sheet.Cells[row, col++].Value = personName;
                    sheet.Column(col).AutoFit();

                    sheet.Cells[row, col++].Value = tp.SignStatus == (int)SignStatus.Signed ? "已簽到" : "未簽到";
                    sheet.Column(col).AutoFit();

                    sheet.Cells[row, col++].Value = tp.SignStatus == (int)SignStatus.Signed ? tp.SignDate.ToString("yyyy-MM-dd HH:mm") : "無";
                    sheet.Column(col).AutoFit();

                    if (trainingInfo.Cert != 2 && trainingInfo.Cert != 0)
                    {
                        sheet.Cells[row, col++].Value = tp.IsPassed;
                        sheet.Column(col).AutoFit();
                    }

                    sheet.Cells[row, col++].Value = tp.Email;
                    sheet.Column(col).AutoFit();

                    row++;
                    #endregion
                }
                #endregion

                #region 回饋資料
                ExcelWorksheet opinionSheet = ep.Workbook.Worksheets.Add("意見表資料");
                sheet.Cells.Style.Font.Size = 10;
                fillColor = System.Drawing.Color.Salmon;
                row = 1;    //列:橫的
                col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                SetColumnStyle(col, opinionSheet);

                opinionSheet.Cells[row, col++].Value = "人員";
                SetColumnStyle(col, opinionSheet);

                opinionSheet.Cells[row, col++].Value = "內容與主題契合度";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "課程架構";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "課程時間安排";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "教材設備";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "教室整體設施";
                SetColumnStyle(col, opinionSheet);

                opinionSheet.Cells[row, col++].Value = "對主題之專業程度";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "表達技巧";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "課程時間掌握能力";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "教學方式";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "您是否會像其他同仁推薦該講師之課程";
                SetColumnStyle(col, opinionSheet);

                opinionSheet.Cells[row, col++].Value = "具實際應用價值";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "應用於目前工作上的程度";
                SetColumnStyle(col, opinionSheet);
                opinionSheet.Cells[row, col++].Value = "應用於未來工作上的程度";
                SetColumnStyle(col, opinionSheet);

                opinionSheet.Cells[row, col++].Value = "對您而言，本課程內容的難易程度";
                SetColumnStyle(col, opinionSheet);
                row++;

                var opinionList = Mapper.Map<List<ClassOpinionView>>(classOpinionRepository.Query(trainingInfo.ID));
                if (opinionList.Count() > 0)
                {
                    foreach (var item in opinionList)
                    {
                        var personInfo = t8personRepository.FindByPersonId(item.PersonID);
                        if (personInfo != null)
                        {
                            col = 1;
                            opinionSheet.Column(col).AutoFit();
                            opinionSheet.Cells[row, col++].Value = item.PersonID + " - " + personInfo.Name;
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Compatibility);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Architecture);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Schedule);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.CourseMaterials);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Device);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Professional);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Expression);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.TimeControl);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.TeachingMethod);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.Recommended);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.ActualUsage);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.CurrentUsage);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.FutureUsage);
                            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((DifficultyRadio)item.Difficulty);
                            row++;
                        }
                    }
                }
                #endregion

                #region 匯出檔案
                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", trainingTitle + ".xlsx");
                #endregion
            }
            catch (Exception e)
            {

                throw;
            }

            //string path = "";
            //string fileName = "";
            ////string meetingTitle = trainingRepository.GetById(id).Title;
            //var query = trainingPersonRepository.GetSignInInfoList(id).OrderBy(q => q.DeptID).ToList();
            //var dtHelper = new DataTableHelper<TrainPersonJoinPersonAndDepartment>();
            //DataTable dt = dtHelper.ListToDataTable(query);
            //fileName = trainingTitle.Replace("/", "-").Trim() + "_名單.xlsx";
            //path = Server.MapPath("/FileUploads/TrainingPersonExcel/") + fileName;
            //ExcelHelper.TableToExcel(dt, path);
            //return File(path, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

        }

        private void SetColumnStyle(int col, ExcelWorksheet sheet)
        {
            sheet.Column(col).Style.Font.Name = YaHeiUILight;
            sheet.Column(col).AutoFit();
        }
        #endregion

    }

    public class PersonScore
    {
        public string PersonId { get; set; }
        public int Score { get; set; }
        public string Suggestion { get; set; }
    }
}