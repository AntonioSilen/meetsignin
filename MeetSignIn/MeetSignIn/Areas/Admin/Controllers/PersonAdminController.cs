﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Person;
using MeetSignIn.Areas.Admin.ViewModels.PersonFile;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Areas.Admin.ViewModels.TrainingPerson;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class PersonAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());
        private InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private PersonRepository personRepository = new PersonRepository(new MeetingSignInDBEntities());
        private PersonFileRepository personFileRepository = new PersonFileRepository(new MeetingSignInDBEntities());
        public ActionResult Index(PersonIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var query = personRepository.Query(model.JobID);
            if (!string.IsNullOrEmpty(model.DeptID))
            {
                var personQuery = t8personRepository.GetPersonQuery().Where(q => q.DepartmentStr.Contains(model.DeptID));
                if (personQuery.Count() > 0)
                {
                    var deptIdList = personQuery.Select(q => q.JobID).ToList();
                    query = query.Where(q => deptIdList.Contains(q.JobID));
                }
            }

            var managerPersonInfo = t8personRepository.FindByPersonId(adminRepository.GetById(adminInfo.ID).Account);
            if (managerPersonInfo != null)
            {
                var personDept = managerPersonInfo == null ? "" : managerPersonInfo.DeptID;
                if (adminInfo.Type == (int)AdminType.Manager)
                {//主管審核
                    if (!string.IsNullOrEmpty(personDept))
                    {//判斷部門
                        var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "", personDept).Select(q => q.JobID).ToList();
                        var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account)).Select(q => q.ID).ToList();
                        query = query.Where(q => sameDeptPersonIdList.Contains(q.JobID));
                    }
                }
            }
           
            var pageResult = query.ToPageResult<Person>(model);
            model.PageResult = Mapper.Map<PageResult<PersonView>>(pageResult);

            foreach (var item in model.PageResult.Data)
            {
                try
                {
                    var personInfo = t8personRepository.FindByPersonId(item.JobID);
                    item.PersonInfo = Mapper.Map<PersonInfo>(personInfo);
                    item.PersonInfo.Department = personInfo.DepartmentStr;

                    //var trainingPerson = trainingPersonRepository.Query(true, "", 0, model.JobID);
                    //int trainingCount = 0;
                    //foreach (var tp in trainingPerson)
                    //{
                    //    var trainingInfo = trainingRepository.GetById(tp.TraningID);
                    //    if (trainingInfo != null && trainingInfo.IsOnline)
                    //    {
                    //        trainingCount ++ ;
                    //    }
                    //}
                    //item.TrainingCount = trainingCount;
                }
                catch (Exception e)
                {
                    //personRepository.Delete(item.ID);
                    var newResult = model.PageResult.Data.ToList();
                    newResult.Remove(item);
                    model.PageResult.Data = newResult.AsEnumerable();
                }
            }

            return View(model);
        }

        public ActionResult Edit(string jid = "")
        {
            var model = new PersonView();
            if (string.IsNullOrEmpty(jid))
            {
                ShowMessage(false, "找不到工號 ! ");
                return RedirectToAction("Index", "ManagerAdmin'");
            }
            model.JobID = jid;
            var personInfo = t8personRepository.FindComPersonByPersonId(model.JobID);
            //if (personInfo == null)
            //{
            //    ShowMessage(false, "該人員已離職 ! ");
            //    //return RedirectToAction("Index", "ManagerAdmin");
            //}
            var query = personRepository.GetByJobID(jid);
            model = Mapper.Map<PersonView>(query);

            model.PersonInfo = Mapper.Map<PersonInfo>(personInfo);
            model.PersonInfo.Department = personInfo.DepartmentStr;
            string inductionDate = model.PersonInfo.InductionDate.ToString();
            model.PersonInfo.InductionDateStr = inductionDate.Substring(0, 4) + "/" + inductionDate.Substring(4, 2) + "/" + inductionDate.Substring(6, 2);

            model.TrainingList = GetTrainingPersonList(model.JobID);

            model.PersonFileList = Mapper.Map<List<PersonFileView>>(personFileRepository.Query(model.ID)); 

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PersonView model)
        {
            if (ModelState.IsValid)
            {
                Person data = Mapper.Map<Person>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = personRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    personRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 檔案處理
                string PhotoFolder = Path.Combine(Server.MapPath("~/FileUploads"), "PersonFilePhoto");
                if (model.PersonFiles != null)
                {
                    foreach (HttpPostedFileBase pic in model.PersonFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(pic);
                        if (hasFile)
                        {
                            PersonFile fileData = new PersonFile();
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            fileData.PersonID = model.ID;
                            fileData.MainFile = ImageHelper.SaveFile(pic, PhotoFolder);
                            fileData.FileName = pic.FileName;
                            personFileRepository.Insert(fileData);
                        }
                    }
                }
                #endregion

                return RedirectToAction("Edit", new { jid = model.JobID });
            }

            return View(model);
        }

        public List<TrainingPersonListModel> GetTrainingPersonList(string jobId)
        {
            List<TrainingPersonListModel> result = new List<TrainingPersonListModel>();
           var trainingPerson = trainingPersonRepository.Query(true, "", 0, jobId);
            foreach (var item in trainingPerson)
            {
                TrainingPersonListModel data = new TrainingPersonListModel();
                data.TrainingPersonInfo = Mapper.Map<TrainingPersonView>(item);
                data.TrainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(item.TraningID));
                if (data.TrainingInfo == null || !data.TrainingInfo.IsOnline || (!data.TrainingInfo.IsModel && modelState))
                {
                    continue;
                }
                var auditInfo = auditRepository.Query(false, 0, 0, null, "", "", item.TraningID).Where(q => q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal).FirstOrDefault();
                if (auditInfo == null)
                {
                    data.AuditState = 0;
                }
                else
                {
                    data.AuditState = auditInfo.State;
                }
                data.AuditStatus = EnumHelper.GetDescription((AuditState)data.AuditState);

                #region 驗收狀態
                if(data.TrainingInfo.Category == (int)Category.External)
                {
                    var EAAudit = auditRepository.Query(null, 0, (int)AuditType.ExternalAcceptance, null, "", item.JobID, item.TraningID).FirstOrDefault();
                    if (EAAudit != null)
                    {
                        if (EAAudit.IsInvalid)
                        {
                            data.EAStatus = "駁回";
                        }
                        else
                        {
                            data.EAStatus = EnumHelper.GetDescription((AuditState)EAAudit.State);
                        }
                    }
                    else
                    {
                        data.EAStatus = externalAcceptanceRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                    }
                }
                else
                {
                    var IAAudit = auditRepository.Query(null, 0, (int)AuditType.InternalAcceptance, null, "", item.JobID, item.TraningID).FirstOrDefault();
                    if (IAAudit != null)
                    {
                        if (IAAudit.IsInvalid)
                        {
                            data.IAStatus = "駁回";
                        }
                        else
                        {
                            data.IAStatus = EnumHelper.GetDescription((AuditState)IAAudit.State);
                        }
                    }
                    else
                    {
                        data.IAStatus = internalAcceptanceRepository.Query(item.TraningID, item.JobID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                    }
                }
                #endregion

                result.Add(data);            
            }
            return result;
        }

        public ActionResult Delete(int id)
        {
            personRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public string ChangePassed(int id, bool passed)
        {
            try
            {
                var data = trainingPersonRepository.GetById(id);
                data.IsPassed = passed;
                trainingPersonRepository.Update(data);
                ShowMessage(true, "已更新資料");
                return passed ? "通過" : "未通過";
            }
            catch (Exception e)
            {
                ShowMessage(false, "更新失敗" + e.Message);
                return "failed : " + e.Message;
            }
        }

        public JsonResult GetFairlyPerson()
        {
            var result = t8personRepository.GetPersonQuery();
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}