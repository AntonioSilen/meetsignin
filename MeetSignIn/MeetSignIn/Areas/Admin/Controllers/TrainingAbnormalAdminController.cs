﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.TrainingAbnormal;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class TrainingAbnormalAdminController : BaseAdminController
    {
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingAbnormalRepository trainingAbnormalRepository = new TrainingAbnormalRepository(new MeetingSignInDBEntities());
        private ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());
        private ExternalCourseRepository externalCourseRepository = new ExternalCourseRepository(new MeetingSignInDBEntities());
        private InternalCourseRepository internalCourseRepository = new InternalCourseRepository(new MeetingSignInDBEntities());

        public ActionResult Index(TrainingAbnormalIndexView model)
        {
            var trainingInfo = trainingRepository.GetById(model.TrainingID);
            if (trainingInfo == null)
            {
                ShowMessage(false, "找不到訓練資料");
                return RedirectToAction("Index", "TrainingAdmin");
            }
            model.TrainingID = trainingInfo.ID;
            model.Title = trainingInfo.Title;
            model.Category = trainingInfo.Category;

            var query = trainingAbnormalRepository.Query(model.StartDate, model.EndDate, model.TrainingID);
            var pageResult = query.ToPageResult<TrainingAbnormal>(model);
            model.PageResult = Mapper.Map<PageResult<TrainingAbnormalView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0, int tid = 0)
        {
            var trainingInfo = trainingRepository.GetById(tid);
            if (trainingInfo == null)
            {
                ShowMessage(false, "找不到訓練資料");
                return RedirectToAction("Index", "TrainingAdmin");
            }
            var model = new TrainingAbnormalView();
            model.TrainingID = trainingInfo.ID;
            model.Title = trainingInfo.Title;
            model.Category = trainingInfo.Category;
            if (id != 0)
            {
                var query = trainingAbnormalRepository.FindBy(id);
                model = Mapper.Map<TrainingAbnormalView>(query);
            }
            var officerPersonInfo = t8personRepository.FindByPersonId(adminRepository.GetById(model.TrainingInfo.Creater).Account);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                //if (adminRepository.FindByPersonId(officerPersonInfo.JobID).Type == (int)AdminType.Principal)
                if (adminRepository.FindByPersonId(officerPersonInfo.JobID).IsPrincipal)
                {
                    model.ManagerAudit = AuditProcessing;
                    auditLevelData.ViceReview = officerPersonInfo.JobID;
                    model.PresidentAudit = AuditProcessing;
                }
                model.AuditLevelInfo = auditLevelData;          
                SetSignature(model, auditLevelData);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(TrainingAbnormalView model)
        {
            if (ModelState.IsValid)
            {
                TrainingAbnormal data = Mapper.Map<TrainingAbnormal>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                var category = model.TrainingInfo.Category;
                if (model.IsAudit)
                {
                    data.OfficerAudit = (int)FormAudit.Approval;
                }
                #region 日期處理
                data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                data.PrincipalAuditDate = SetAuditDate(data.PrincipalAudit, data.PrincipalAuditDate);
                data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                #endregion

                #region 簽章
                //承辦人
                var officerInfo = adminRepository.GetById(model.TrainingInfo.Creater);
                var officerPersonInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                if (data.OfficerAudit > 1)
                {
                    int intCK;
                    if (!int.TryParse(officerInfo.Account, out intCK))
                    {
                        data.OfficerSignature = adminSignature;
                    }
                    else
                    {
                        data.OfficerSignature = officerInfo.Account + ".jpg";
                    }
                }
                if (officerPersonInfo != null)
                {
                    var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                    //主管
                    if (data.ManagerAudit > 1 && string.IsNullOrEmpty(data.ManagerSignature))
                    {
                        data.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                    }
                    //副總
                    if (data.ViceAudit > 1 && string.IsNullOrEmpty(data.ViceSignature))
                    {
                        data.ViceSignature = auditLevelData.ViceReview + ".jpg";
                    }
                    //總經理
                    if (data.PresidentAudit > 1 && string.IsNullOrEmpty(data.PresidentSignature))
                    {
                        data.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                    }
                    //校長
                    if (data.PrincipalAudit > 1 && string.IsNullOrEmpty(data.PrincipalSignature))
                    {
                        data.PrincipalSignature = auditLevelData.PrincipalReview + ".jpg";
                    }
                }
                else
                {
                    data.ManagerSignature = adminSignature;
                    data.ViceSignature = adminSignature;
                    //校長
                    if (data.PrincipalAudit > 1)
                    {
                        //var principal = adminRepository.Query(true, (int)AdminType.Principal).FirstOrDefault();
                        var principal = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                        data.PrincipalSignature = principal == null ? emptySignature : principal.Account + ".jpg";
                    }
                    //總經理
                    if (data.PresidentAudit > 1)
                    {
                        var president = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                        data.PresidentSignature = president == null ? emptySignature : president.Account + ".jpg";
                    }
                }

                #endregion

                #region 簽核人員
                var trainingInfo = trainingRepository.GetById(data.TrainingID);
                if (adminInfo.Type < (int)AdminType.ClassSupervisor)
                {
                    if (trainingInfo.Creater == adminInfo.ID)
                    {
                        switch (adminInfo.Type)
                        {
                            case (int)AdminType.Sysadmin:
                                data.ManagerAudit = data.OfficerAudit;
                                data.ManagerAuditDate = data.OfficerAuditDate;
                                data.ManagerRemark = data.OfficerRemark;
                                data.ManagerSignature = data.OfficerSignature;
                                break;
                            case (int)AdminType.Manager:
                                data.ManagerAudit = data.OfficerAudit;
                                data.ManagerAuditDate = data.OfficerAuditDate;
                                data.ManagerRemark = data.OfficerRemark;
                                data.ManagerSignature = data.OfficerSignature;
                                break;
                            case (int)AdminType.Vice:
                                data.ViceAudit = data.OfficerAudit;
                                data.ViceAuditDate = data.OfficerAuditDate;
                                data.ViceRemark = data.OfficerRemark;
                                data.ViceSignature = data.OfficerSignature;
                                break;
                            //case (int)AdminType.Principal:
                            //    data.PrincipalAudit = data.OfficerAudit;
                            //    data.PrincipalAuditDate = data.OfficerAuditDate;
                            //    data.PrincipalRemark = data.OfficerRemark;
                            //    data.PrincipalSignature = data.OfficerSignature;
                            //    break;
                            case (int)AdminType.President:
                                data.PresidentAudit = data.OfficerAudit;
                                data.PresidentAuditDate = data.OfficerAuditDate;
                                data.PresidentRemark = data.OfficerRemark;
                                data.PresidentSignature = data.OfficerSignature;
                                break;
                        }
                    }
                }
                #endregion

                var trainingQuery = trainingRepository.GetById(model.TrainingID);
                trainingQuery.IsAbnormal = true;
                trainingRepository.Update(trainingQuery);

                if (trainingQuery.Category == (int)Category.External)
                {
                    var ePlan = externalPlanRepository.Query(trainingQuery.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                    var newData = Mapper.Map<ViewModels.Training.ExternalPlanView>(ePlan);
                    var oldAudit = auditRepository.Query(null, ePlan.ID, (int)AuditType.External, null, "", "", trainingQuery.ID).FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);
                    }
                    SetAuditHelper.SetDefault(newData);

                    newData.ID = externalPlanRepository.Insert(Mapper.Map<ExternalPlan>(newData));

                    var existList = externalCourseRepository.Query(model.TrainingID, ePlan.ID).ToList();
                    foreach (var item in existList)
                    {
                        item.ExternalID = newData.ID;
                        externalCourseRepository.Update(item);
                    }
                }
                else
                {
                    var iPlan = internalPlanRepository.Query(trainingQuery.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                    var newData = Mapper.Map<ViewModels.Training.InternalPlanView>(iPlan);
                    var oldAudit = auditRepository.Query(null, iPlan.ID, (int)AuditType.Internal, null, "", "", trainingQuery.ID).FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);
                    }
                    SetAuditHelper.SetDefault(newData);

                    newData.ID = internalPlanRepository.Insert(Mapper.Map<InternalPlan>(newData));
                    var existList = internalCourseRepository.Query(model.TrainingID, iPlan.ID).ToList();
                    foreach (var item in existList)
                    {
                        item.InternalID = newData.ID;
                        internalCourseRepository.Update(item);
                    }
                }

                if (model.ID == 0)
                {
                    model.ID = trainingAbnormalRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    trainingAbnormalRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 送審
                if (data.OfficerAudit == (int)FormAudit.Approval && data.IsAudit)
                {
                    if (category == (int)AuditType.External)
                    {
                        var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.TrainingAbnormal).FirstOrDefault();

                        #region 簽核狀態
                        int state = HandleState(model.TrainingInfo.CreaterDeptId, data.OfficerAudit, data.ManagerAudit, data.ViceAudit, data.PresidentAudit, data.PrincipalAudit);
                        #endregion
                        
                        if (auditInfo == null)
                        {
                            var auditId = auditRepository.Insert(new Audit()
                            {
                                TrainingID = data.TrainingID,
                                PersonID = officerInfo.Account,
                                TargetID = data.ID,
                                Type = (int)AuditType.TrainingAbnormal,
                                State = state,
                                IsInvalid = false,
                                Officer = model.TrainingInfo.Creater,
                                CreateDate = DateTime.Now
                            });
                        }
                        else
                        {
                            if (data.OfficerAudit == AuditReject || data.ManagerAudit == AuditReject || data.PrincipalAudit == AuditReject || data.ViceAudit == AuditReject || data.PresidentAudit == AuditReject)
                            {
                                auditInfo.IsInvalid = true;

                                var personInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                                //駁回通知承辦人
                                string mailBody = "";
                                mailBody = $"<h3>您所送審的【{trainingInfo.Title}】外訓異動單申請已駁回，請前往查看</h3>";

                                string checkUrl = $"{Request.Url.Scheme}://220.228.58.43:18151/TrainingAbnormalAdmin/Edit?trainingId={trainingInfo.ID}&FromAudit=false";

                                mailBody += $"<p>前往查看 : <a href=\"{checkUrl}\">{checkUrl}</a></p>";

                                //MailHelper.POP3Mail("i1o2i2o3i1@gmail.com", "【" + model.TrainingInfo.Title + "】已被駁回", mailBody);
                                MailHelper.POP3Mail(personInfo.Email, "【" + model.TrainingInfo.Title + "】的異動資料已被駁回", mailBody);
                            }
                            auditInfo.State = state;
                            auditRepository.Update(auditInfo);
                        }
                    }
                    else
                    {
                        var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.TrainingAbnormal).FirstOrDefault();

                        #region 簽核狀態
                        int state = HandleState(model.TrainingInfo.CreaterDeptId, data.OfficerAudit, data.ManagerAudit, data.ViceAudit, data.PresidentAudit, data.PrincipalAudit);
                        #endregion

                        if (auditInfo == null)
                        {
                            var auditId = auditRepository.Insert(new Audit()
                            {
                                TrainingID = data.TrainingID,
                                PersonID = officerInfo.Account,
                                TargetID = data.ID,
                                Type = (int)AuditType.TrainingAbnormal,
                                State = state,
                                IsInvalid = false,
                                Officer = model.TrainingInfo.Creater,
                                CreateDate = DateTime.Now
                            });
                        }
                        else
                        {
                            if (data.OfficerAudit == AuditReject || data.ManagerAudit == AuditReject || data.PrincipalAudit == AuditReject || data.ViceAudit == AuditReject || data.PresidentAudit == AuditReject)
                            {
                                auditInfo.IsInvalid = true;

                                var personInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                                //駁回通知承辦人
                                string mailBody = "";
                                mailBody = $"<h3>您所送審的【{trainingInfo.Title}】內訓異動單已駁回，請前往查看</h3>";

                                string checkUrl = $"{Request.Url.Scheme}://220.228.58.43:18151/InternalAcceptanceAdmin/Edit?trainingId={trainingInfo.ID}&FromAudit=false";

                                mailBody += $"<p>前往查看 : <a href=\"{checkUrl}\">{checkUrl}</a></p>";

                                //MailHelper.POP3Mail("i1o2i2o3i1@gmail.com", "【" + model.TrainingInfo.Title + "】已被駁回", mailBody);
                                MailHelper.POP3Mail(personInfo.Email, "【" + model.TrainingInfo.Title + "】的異動資料已被駁回", mailBody);
                            }
                            auditInfo.State = state;
                            auditRepository.Update(auditInfo);
                        }
                    }
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID, tid = model.TrainingID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            trainingAbnormalRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}