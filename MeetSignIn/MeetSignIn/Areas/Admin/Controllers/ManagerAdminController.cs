﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Manager;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ManagerAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());
        private FunctionDocRepository functionDocRepository = new FunctionDocRepository(new MeetingSignInDBEntities());
        
        // GET: Admin/ManagerAdmin
        public ActionResult Index(ManagerIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (adminInfo.Type != (int)AdminType.Sysadmin && adminInfo.DeptId != "B40")
            {
                return RedirectToAction("Edit", new { id = adminInfo.ID });
            }
            var thisAcc = adminInfo.Account;
            var admintype = adminRepository.GetAll().Where(a => a.Account == thisAcc).FirstOrDefault().Type;
            var query = adminRepository.Query(model.Status, model.Type, model.Account);
            //if (admintype != 1)
            //{
            //    query = query.Where(a => a.Type == 2);
            //}
            var pageResult = query.ToPageResult<Models.MainAdmin>(model);
            model.PageResult = Mapper.Map<PageResult<ManagerView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                var adminData = adminRepository.GetById(item.ID);
                var personInfo = t8personRepository.FindByPersonId(item.Account);
                if (personInfo != null)
                {
                    if (string.IsNullOrEmpty(item.DeptId) || string.IsNullOrEmpty(item.Department))
                    {                        
                        adminData.DeptId = personInfo.DeptID;
                        adminData.Department = personInfo.DepartmentStr;
                        adminRepository.Update(adminData);
                    }
                    item.DeptId = personInfo.DeptID;
                    item.Department = personInfo.DepartmentStr;
                    item.Name = personInfo.Name;
                }
                else
                {
                    adminData.DeptId = "0001";
                    adminData.Department = "菲力工業股份有限公司";
                    adminRepository.Update(adminData);
                    item.DeptId = "0001";
                    item.Department = "菲力工業股份有限公司";
                }
            }
            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (adminInfo.Type > (int)AdminType.Sysadmin && adminInfo.ID != id)
            {
                return RedirectToAction("Edit", new { id = adminInfo.ID});
            }
            ManagerView model;
            if (id == 0)
            {
                model = new ManagerView();
                model.Type = 2;
            }
            else
            {
                var query = adminRepository.GetById(id);
                model = Mapper.Map<ManagerView>(query);

                var personInfo = t8personRepository.FindByPersonId(model.Account);
                if (personInfo != null)
                {
                    model.Department = personInfo.DeptID;
                    model.Name = personInfo.Name;
                }

                model.DocList = functionDocRepository.Query(model.Account).ToList();
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ManagerView model)
        {
            if (string.IsNullOrEmpty(model.Department) && model.DeptId == "0001")
            {
                model.Department = "菲力工業股份有限公司";
            }
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (adminInfo.Type != (int)AdminType.Sysadmin && adminInfo.Account != model.Account)
            {
                ShowMessage(false, "無權限執行此動作 !");
                return View(model);
            }
            if (ModelState.IsValid)
            {
                Models.MainAdmin admin = Mapper.Map<Models.MainAdmin>(model);
                admin.UpdateDate = DateTime.Now;
                admin.Updater = 1;
                int accountCount = adminRepository.Query(true, 0, model.Account).Count();
                if (accountCount > 0 && AdminInfoHelper.GetAdminInfo().Account != model.Account && model.ID == 0)
                {
                    ShowMessage(false, "該帳號已被使用，請重新輸入");
                    return View(model);
                }

                if (model.DocFiles != null)
                {
                    foreach (HttpPostedFileBase file in model.DocFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(file);
                        if (hasFile)
                        {
                            FunctionDoc docData = new FunctionDoc();
                            docData.PersonId = model.Account;
                            docData.FileName = ImageHelper.SaveFile(file, PhotoFolder, docData.PersonId + "-" + file.FileName.Split('.')[0]);
                            docData.Type = 1;
                            functionDocRepository.Insert(docData);
                        }
                    }
                }

                if (model.ID == 0)
                {
                    admin.CreateDate = DateTime.Now;
                    admin.Creater = 1;
                    model.ID = adminRepository.Insert(admin);
                }
                else
                {
                    adminRepository.Update(admin);
                }
                ShowMessage(true, "");
                return RedirectToAction(nameof(ManagerAdminController.Edit), new { id = model.ID });
            }

            ShowMessage(false, "輸入資料有誤");
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            Models.MainAdmin admin = adminRepository.GetById(id);
            adminRepository.Delete(admin.ID);
            meetingRepository.TransformCreater(id, admin.Creater);
            ShowMessage(true, "Data deleted.");
            return RedirectToAction(nameof(ManagerAdminController.Index));
        }

        public ActionResult DeleteFile(int id)
        {
            var data = functionDocRepository.GetById(id);
            var personInfo = adminRepository.FindByPersonId(data.PersonId);
            functionDocRepository.Delete(id);
            ImageHelper.DeleteFile(PhotoFolder, data.FileName);
            ShowMessage(true, "刪除成功");
            if (data.Type == 1)
            {
                return RedirectToAction("Edit", new { id = personInfo.ID });
            }
            else if (data.Type == 2)
            {
                return RedirectToAction("External", "TrainingAdmin", new { trainingId = Convert.ToInt32(data.PersonId.Replace("LT", "")) });
            }
            else
            {
                return RedirectToAction("Internal", "TrainingAdmin", new { trainingId = Convert.ToInt32(data.PersonId.Replace("LT", "")) });
            }
        }
    }
}