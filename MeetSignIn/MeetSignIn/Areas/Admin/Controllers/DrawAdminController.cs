﻿using AutoMapper;
using ExcelDataReader;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Draw;
using MeetSignIn.Areas.Admin.ViewModels.DrawAward;
using MeetSignIn.Areas.Admin.ViewModels.DrawAwardRelation;
using MeetSignIn.Areas.Admin.ViewModels.DrawPerson;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class DrawAdminController : BaseAdminController
    {
        private DrawRepository drawRepository = new DrawRepository(new MeetingSignInDBEntities());
        private DrawAwardRepository drawAwardRepository = new DrawAwardRepository(new MeetingSignInDBEntities());
        private DrawAwardSortRepository drawAwardSortRepository = new DrawAwardSortRepository(new MeetingSignInDBEntities());
        private DrawAwardRelationRepository drawAwardRelationRepository = new DrawAwardRelationRepository(new MeetingSignInDBEntities());
        private DrawPersonRepository drawPersonRepository = new DrawPersonRepository(new MeetingSignInDBEntities());
        public ActionResult Index(DrawIndexView model)
        {
            var query = drawRepository.Query(model.Title, model.Type);
            var pageResult = query.ToPageResult<Draw>(model);
            model.PageResult = Mapper.Map<PageResult<DrawView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new DrawView();
            if (id != 0)
            {
                var query = drawRepository.FindBy(id);
                model = Mapper.Map<DrawView>(query);
                model.DrawPersonList = Mapper.Map<List<DrawPersonView>>(drawPersonRepository.Query(id));
                model.DrawAwardList = Mapper.Map<List<DrawAwardView>>(drawAwardRepository.Query(id));
                foreach (var item in model.DrawAwardList)
                {
                    var awardRelationList = drawAwardRelationRepository.Query(item.DrawID, item.ID).Select(q => q.PersonId).ToList();
                    item.WinList = Mapper.Map<List<DrawAwardRelationView>>(drawAwardRelationRepository.Query(item.DrawID, item.ID).Where(q => awardRelationList.Contains(q.PersonId)));
                    foreach (var win in item.WinList)
                    {
                        var personInfo = t8personRepository.FindByPersonId(win.PersonId);
                        win.PersonName = personInfo == null ? "" : personInfo.PersonName;
                    }
                    item.IsDraw = item.WinList.Count() > 0;
                }
                model.DrawSortList = drawAwardSortRepository.Query(id).ToList();
            }
            else
            {
                model.DrawPersonList = new List<DrawPersonView>();
                model.DrawAwardList = new List<DrawAwardView>();
                model.DrawSortList = new List<DrawAwardSort>();
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(DrawView model)
        {
            if (ModelState.IsValid)
            {
                Draw data = Mapper.Map<Draw>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = drawRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    drawRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 讀取Excel名單
                var file = model.ParticipantFile;
                if (file != null && file.ContentLength > 0)
                {
                    List<string> personIdList = new List<string>();
                    // 讀取上傳的 Excel 檔案
                    try
                    {
                        using (var stream = file.InputStream)
                        {
                            // 使用 ExcelDataReader 套件讀取 Excel 檔案
                            using (var reader = ExcelReaderFactory.CreateReader(stream))
                            {
                                // 迴圈讀取每一列資料
                                while (reader.Read())
                                {
                                    // 讀取每個欄位的值
                                    var dataVal = reader.GetValue(0);
                                    if (dataVal != null)
                                    {
                                        var value = dataVal.ToString();
                                        if (!string.IsNullOrEmpty(value))
                                        {
                                            personIdList.Add(value.PadLeft(4, '0'));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {

                    }
                    if (personIdList.Count() > 0)
                    {
                        var drawPersonQuery = drawPersonRepository.Query(model.ID).Select(q => q.PersonId).ToList();
                        foreach (var personId in personIdList.Where(q => !drawPersonQuery.Contains(q)))
                        {
                            var personInfo = t8personRepository.FindByPersonId(personId);
                            if (personInfo != null)
                            {
                                drawPersonRepository.Insert(new DrawPerson() {
                                    DrawID = model.ID,
                                    PersonId = personId,
                                    PersonName = personInfo.PersonName,
                                    DrawStatus = (int)DrawStatus.Zero,
                                    IsOnline = true,
                                });
                            }
                            else if (personId == "9025")
                            {
                                drawPersonRepository.Insert(new DrawPerson()
                                {
                                    DrawID = model.ID,
                                    PersonId = personId,
                                    PersonName = "阿蘭",
                                    DrawStatus = (int)DrawStatus.Zero,
                                    IsOnline = true,
                                });
                            }
                            else if (personId == "9026")
                            {
                                drawPersonRepository.Insert(new DrawPerson()
                                {
                                    DrawID = model.ID,
                                    PersonId = personId,
                                    PersonName = "莊漢昭",
                                    DrawStatus = (int)DrawStatus.Zero,
                                    IsOnline = true,
                                });
                            }
                        }
                        
                    }
                }
                //// 建立 ExcelPackage 對象
                //var package = new ExcelPackage(new FileInfo(model.ParticipantFile.FileName));

                //// 取得第一個工作表
                //var worksheet = package.Workbook.Worksheets[0];

                //// 取得工作表中的資料列數量
                //var rowCount = worksheet.Dimension.Rows;

                //// 迴圈讀取每一列資料
                //for (int row = 1; row <= rowCount; row++)
                //{
                //    // 讀取每一列中的資料
                //    var cellValue1 = worksheet.Cells[row, 1].Value;
                //    var cellValue2 = worksheet.Cells[row, 2].Value;

                //    // 將讀取到的資料進行處理
                //    // ...
                //}
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            drawRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult Remove(int id, bool isDisable = false)
        {
            if (isDisable)
            {
                var data = drawPersonRepository.GetById(id);
                data.IsOnline = false;
                drawPersonRepository.Update(data);
                return RedirectToAction("Index");
            }
            drawPersonRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult RevokeAward(int id)
        {
            var data = drawAwardRepository.GetById(id);
            data.IsOnline = false;
            drawAwardRepository.Update(data);            
            
            ShowMessage(true, "已撤銷");
            return RedirectToAction("Edit", new { id = data.DrawID});
        }

        #region ajax
        public JsonResult GetAward(int id)
        {
            var result = new DrawAwardView();
            try
            {
                result = Mapper.Map<DrawAwardView>(drawAwardRepository.GetById(id));
                if (result != null)
                {
                    var awardRelationList = drawAwardRelationRepository.Query(result.DrawID, id).Select(q => q.PersonId).ToList();
                    result.WinList = Mapper.Map<List<DrawAwardRelationView>>(drawAwardRelationRepository.Query(result.DrawID, id).Where(q => awardRelationList.Contains(q.PersonId)));
                    foreach (var win in result.WinList)
                    {
                        var personInfo = t8personRepository.FindByPersonId(win.PersonId);
                        win.PersonName = personInfo == null ? "" : personInfo.PersonName;
                        win.DeptName = personInfo == null ? "" : personInfo.DeptName;

                        var awardSortData = drawAwardSortRepository.Query(result.DrawID, win.Sort).FirstOrDefault();
                        win.AwardDate = awardSortData == null ? "" : awardSortData.Title;
                    }
                }
                result.IsDraw = result.WinList.Count() > 0;
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }            

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetWin(int id)
        {
            var result = new DrawAwardRelationView();
            try
            {
                result = Mapper.Map<DrawAwardRelationView>(drawAwardRelationRepository.GetById(id));
                var personInfo = t8personRepository.FindByPersonId(result.PersonId);
                result.PersonName = personInfo == null ? "" : personInfo.PersonName;
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }            

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSort(int id)
        {
            var result = new DrawAwardSort();
            try
            {
                result = drawAwardSortRepository.GetById(id);
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }            

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public string SaveAward(int id, int drawId, string title, int rate, int number, bool isrepeatable)
        {
            try
            {
                var data = new DrawAward();
                if (id != 0)
                {
                    data = drawAwardRepository.GetById(id);
                }
                data.DrawID = drawId;
                data.Title = title;
                data.Rate = rate;
                data.Number = number;
                data.IsRepeatable = isrepeatable;
                if (id != 0)
                {
                    drawAwardRepository.Update(data);
                }
                else
                {
                    data.IsOnline = true;
                    id = drawAwardRepository.Insert(data);
                }
                return "success";
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
                return "failed";
            }
        }
        public string SaveSort(int id, int drawId, int srot, string title, string annotation)
        {
            try
            {
                var data = new DrawAwardSort();
                if (id != 0)
                {
                    data = drawAwardSortRepository.GetById(id);
                }
                data.DrawID = drawId;
                data.Sort = srot;
                data.Title = title;
                data.Annotation = annotation;
                if (id != 0)
                {
                    drawAwardSortRepository.Update(data);
                }
                else
                {
                    id = drawAwardSortRepository.Insert(data);
                }
                return "success";
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
                return "failed";
            }
        }
        public string SaveWin(int id, int drawId, string title, int rate, int number, bool isrepeatable)
        {
            try
            {
                var data = new DrawAward();
                if (id != 0)
                {
                    data = drawAwardRepository.GetById(id);
                }
                data.DrawID = drawId;
                data.Title = title;
                data.Rate = rate;
                data.Number = number;
                data.IsRepeatable = isrepeatable;
                if (id != 0)
                {
                    drawAwardRepository.Update(data);
                }
                else
                {
                    data.IsOnline = true;
                    id = drawAwardRepository.Insert(data);
                }
                return "success";
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
                return "failed";
            }
        }

        /// <summary>
        /// 抽籤
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string DrawLots(int id)
        {
            try
            {
                //獎項
                var award = Mapper.Map<DrawAwardView>(drawAwardRepository.GetById(id));
                if (award != null)
                {
                    //抽籤名單
                    var personList = drawPersonRepository.Query(award.DrawID);
                    if (!award.IsRepeatable)
                    {
                        var awardRelationList = drawAwardRelationRepository.Query(award.DrawID).Select(q => q.PersonId).ToList();
                        personList = personList.Where(q => !awardRelationList.Contains(q.PersonId));
                    }

                    if (personList.Count() == 0)
                    {//無人可抽
                        return "draw out";
                    }

                    List<string> personIdList = personList.Select(q => q.PersonId).ToList();
                    Random rand = new Random();
                    for (int i = 0; i < award.Number; i++)
                    {
                        int index = rand.Next(personIdList.Count);

                        var winPersonId = personIdList[index];
                        drawAwardRelationRepository.Insert(new DrawAwardRelation() { 
                            DrawID = award.DrawID,
                            AwardID = award.ID,
                            PersonId = winPersonId,
                            Sort = i,
                            IsRevoke = false,
                            CreateDate = DateTime.Now,
                        });

                        var drawPerson = drawPersonRepository.Query(award.DrawID, winPersonId).FirstOrDefault();
                        if (drawPerson != null)
                        {
                            if (drawPerson.DrawStatus == (int)MeetSignIn.Models.DrawStatus.Zero)
                            {
                                drawPerson.DrawStatus = (int)MeetSignIn.Models.DrawStatus.One;
                            }
                            else
                            {
                                drawPerson.DrawStatus = (int)MeetSignIn.Models.DrawStatus.MoreThanOne;
                            }
                            drawPersonRepository.Update(drawPerson);
                        }
                        personIdList.RemoveAt(index);
                    }

                    return "success";
                }
                
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
                return "failed";
            }
            return "not found";
        }
        #endregion

        #region 匯出Excel
        /// <summary>
        /// 匯出Excel (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportExcel(int id)
        {
            //取出要匯出Excel的資料
            var drawAwardQuery = drawAwardRepository.Query(id);
            var drawData = drawRepository.GetById(drawAwardQuery.FirstOrDefault().DrawID);
            var sortQuery = drawAwardSortRepository.Query(id);

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                //建立Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add(drawData.Title);
                sheet.Cells.Style.Font.Size = 10;
                var fillColor = System.Drawing.Color.Salmon;
                int row = 1;    //列:橫的
                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //第1列是標題列 
                //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                //取得做法可參考最後的參考連結。

                sheet.Cells[row, col++].Value = "工號";
                sheet.Column(col).AutoFit();
                sheet.Cells[row, col++].Value = "姓名";
                sheet.Column(col).AutoFit();
                sheet.Cells[row, col++].Value = "部門";
                sheet.Column(col).AutoFit();
                sheet.Cells[row, col++].Value = "日期";
                sheet.Column(col).AutoFit();
                sheet.Cells[row, col++].Value = "註解";
                sheet.Column(col).AutoFit();

                sheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                sheet.Row(row).Style.Fill.BackgroundColor.SetColor(fillColor); // 設置背景顏色
                for (int c = 1; c <= 5; c++)
                {
                    sheet.Column(c).Style.Font.Name = "Microsoft YaHei UI Light";
                }
                
                row++;

                int i = 0;
                foreach (var drawAward in drawAwardQuery.OrderBy(q => q.ID).ToList())
                {
                    col = 1;    
                    #region 抽籤名單                
                    List<ItemPosition> positionArr = new List<ItemPosition>();

                    var winList = Mapper.Map<List<DrawAwardRelationView>>(drawAwardRelationRepository.Query(drawData.ID, drawAward.ID));
                    foreach (var ar in winList)
                    {//分類
                        col = 1;

                        fillColor = System.Drawing.Color.LightBlue;
                        if (i % 2 == 0)
                        {
                            fillColor = System.Drawing.Color.Khaki;
                        }

                        var personInfo = t8personRepository.FindByPersonId(ar.PersonId);
                        string personName = personInfo == null ? "" : personInfo.PersonName;
                        if (ar.PersonId == "9025")
                        {
                            personName = "阿蘭";
                        }
                        else if (ar.PersonId == "9026")
                        {
                            personName = "莊漢昭";
                        }
                        string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;

                        var sortData = sortQuery.Where(q => q.Sort == ar.Sort).FirstOrDefault();
                        var awardDate = sortData == null ? "" : sortData.Title;

                        sheet.Cells[row, col].Value = ar.PersonId;
                        sheet.Column(col).Style.WrapText = true; // 設置自動換行
                        sheet.Column(col).AutoFit();
                        //sheet.Column(col).Style.Font.Color.SetColor(System.Drawing.Color.Red); // 設置文字顏色
                        sheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Row(row).Style.Fill.BackgroundColor.SetColor(fillColor); // 設置背景顏色
                        

                        sheet.Cells[row, col + 1].Value = personName;
                        //sheet.Column(col + 1).Style.WrapText = true; // 設置自動換行
                        sheet.Column(col + 1).AutoFit();


                        sheet.Cells[row, col + 2].Value = deptName;
                        sheet.Column(col + 2).AutoFit();

                        sheet.Cells[row, col + 3].Value = drawAward.Title;
                        sheet.Column(col + 3).AutoFit();

                        sheet.Cells[row, col + 4].Value = awardDate;
                        sheet.Column(col + 4).AutoFit();

                        row++;
                    }
                    #endregion
                    i++;
                }

                #region 匯出檔案
                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", drawData.Title + ".xlsx");
                #endregion
            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion

    }
}