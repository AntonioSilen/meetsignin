﻿using MeetSignIn.Areas.Admin.ViewModels.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    public class UserAdminController : Controller
    {
        // GET: Admin/UserAdmin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit()
        {
            // prepare default view model for testing
            var viewModel = new UserIndexView();
            viewModel.User = new UserView() { Name = "Chirs Chen", Phone = "0800-000-000", UserId = "Chris" };
            viewModel.UserTasks = new List<UserTask>()
            {
                new UserTask() {UserId = "Chris", TaskName="do something 1", CompletedDate = DateTime.Now.AddHours(6)},
                new UserTask() {UserId = "Tony", TaskName="do something 2", CompletedDate = DateTime.Now.AddDays(1)},
                new UserTask() {UserId = "Fedtrick", TaskName="do something 3", CompletedDate = DateTime.Now.AddMonths(2)},
                new UserTask() {UserId = "Katherine", TaskName="do something 4", CompletedDate = DateTime.Now.AddYears(1)},
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(UserIndexView viewModel)
        {
            // save ...
            return RedirectToAction("Edit");
        }
    }
}