﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.InternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.TrainingPerson;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class InternalAcceptanceAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        //private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());
        private InternalCourseRepository internalCourseRepository = new InternalCourseRepository(new MeetingSignInDBEntities());
        private InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
        //private Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());

        // GET: Admin/InternalAcceptanceAdmin
        public ActionResult Index(TrainingPersonIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
        

            return View(model);
        }

        public ActionResult Edit(int id = 0, int trainingId = 0, string personId = "", bool isNew = false, bool FromAudit = false, bool isGroup = false)
        {
            if (trainingId == 0 || string.IsNullOrEmpty(personId))
            {
                return RedirectToAction("Index", "HomeAdmin");
            }
            var model = new InternalAcceptanceView();
            var query = internalAcceptanceRepository.Query(trainingId, personId).OrderByDescending(q => q.ID).FirstOrDefault();
            if (query != null)
            {
                model = Mapper.Map<InternalAcceptanceView>(query);
                if (isNew && model.IsAudit)
                {
                    var newData = Mapper.Map<InternalAcceptanceView>(query);
                    var oldAudit = auditRepository.Query(false, query.ID, (int)AuditType.InternalAcceptance, null, "", "", query.TrainingID).FirstOrDefault();
                    if (oldAudit != null)
                    {
                        //停用審核資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        //複製計畫表
                        newData.ID = 0;
                        SetAuditHelper.SetDefault(newData);
                        newData.AuditInfo = model.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.InternalAcceptance, query.TrainingID, query.PersonID);
                        internalAcceptanceRepository.Insert(Mapper.Map<InternalAcceptance>(newData));

                        return RedirectToAction("Edit", new { trainingId = model.TrainingID, personId = model.PersonID });
                    }
                }
            }
            else
            {
                model = new InternalAcceptanceView()
                {
                    PersonID = personId,
                    TrainingID = trainingId,
                    Name = t8personRepository.FindByPersonId(personId).Name
                };
            }
            model.IsGroup = isGroup;
            var trainingPlan = internalPlanRepository.Query(trainingId, null).FirstOrDefault();
            if (trainingPlan != null)
            {
                model.HasOpinion = trainingPlan.Response;
                model.HasLearning = trainingPlan.Learning;
                model.HasActionPlan = trainingPlan.Behavior;
                model.HasPerformance = trainingPlan.Outcome;
            }
            var courseQuery = internalCourseRepository.Query(trainingId, trainingPlan.ID, true);
            model.Address = "";
            model.Lecturer = "";
            foreach (var item in courseQuery)
            {
                if (!model.Lecturer.Contains(item.Lecturer))
                {
                    model.Lecturer += $@"{item.Lecturer} " + Environment.NewLine;
                }
            }

            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            //var officerPersonInfo = t8personRepository.FindByJobID(adminRepository.GetById(model.TrainingInfo.Creater).Account);//調整
            var officerPersonInfo = t8personRepository.FindByPersonId(adminRepository.Query(true, 0, model.PersonID).FirstOrDefault().Account);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                //if (adminRepository.FindByPersonId(officerPersonInfo.JobID).Type == (int)AdminType.Principal)
                if (adminRepository.FindByPersonId(officerPersonInfo.JobID).IsPrincipal)
                {
                    model.ManagerAudit = AuditProcessing;
                    auditLevelData.ViceReview = officerPersonInfo.JobID;
                    model.PresidentAudit = AuditProcessing;
                }
                model.AuditLevelInfo = auditLevelData;
                SetSignature(model, auditLevelData);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(InternalAcceptanceView model)
        {
            if (ModelState.IsValid)
            {
                InternalAcceptance data = Mapper.Map<InternalAcceptance>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                if (model.IsAudit)
                {
                    data.OfficerAudit = (int)FormAudit.Approval;
                }
                #region 日期處理
                data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                data.PrincipalAuditDate = SetAuditDate(data.PrincipalAudit, data.PrincipalAuditDate);
                data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                #endregion

                #region 簽章
                //承辦人
                //var officerInfo = adminRepository.GetById(model.TrainingInfo.Creater);//調整
                var officerInfo = adminRepository.Query(true, 0, model.PersonID).FirstOrDefault();
                var officerPersonInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                if (data.OfficerAudit > AuditProcessing)
                {
                    int intCK;
                    if (!int.TryParse(officerInfo.Account, out intCK))
                    {
                        data.OfficerSignature = adminSignature;
                    }
                    else
                    {
                        data.OfficerSignature = officerInfo.Account + ".jpg";
                    }
                }
                if (officerPersonInfo != null)
                {
                    var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                    //主管
                    if (data.ManagerAudit > 1 && string.IsNullOrEmpty(data.ManagerSignature))
                    {
                        data.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                    }
                    //副總
                    if (data.ViceAudit > 1 && string.IsNullOrEmpty(data.ViceSignature))
                    {
                        data.ViceSignature = auditLevelData.ViceReview + ".jpg";
                    }
                    //總經理
                    if (data.PresidentAudit > 1 && string.IsNullOrEmpty(data.PresidentSignature))
                    {
                        data.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                    }
                    //校長
                    if (data.PrincipalAudit > 1 && string.IsNullOrEmpty(data.PrincipalSignature))
                    {
                        data.PrincipalSignature = auditLevelData.PrincipalReview + ".jpg";
                    }
                }
                else
                {
                    data.ManagerSignature = adminSignature;
                    data.ViceSignature = adminSignature;
                    //校長
                    if (data.PrincipalAudit > AuditProcessing)
                    {
                        //var principal = adminRepository.Query(true, (int)AdminType.Principal).FirstOrDefault();
                        var principal = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                        data.PrincipalSignature = principal == null ? emptySignature : principal.Account + ".jpg";
                    }
                    //總經理
                    if (data.PresidentAudit > AuditProcessing)
                    {
                        var president = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                        data.PresidentSignature = president == null ? emptySignature : president.Account + ".jpg";
                    }
                }

                #endregion

                #region 檔案處理
                bool hasActionPlanFile = ImageHelper.CheckFileExists(model.ActionPlanPost);
                if (hasActionPlanFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.ActionPlanFile);
                    data.ActionPlanFile = ImageHelper.SaveFile(model.ActionPlanPost, PhotoFolder);
                }
                #endregion

                #region 簽核人員
                var trainingInfo = trainingRepository.GetById(data.TrainingID);
                if (adminInfo.Type < (int)AdminType.ClassSupervisor && adminInfo.Type > (int)AdminType.Sysadmin)
                {
                    if (trainingInfo.Creater == adminInfo.ID)
                    {
                        switch (adminInfo.Type)
                        {
                            case (int)AdminType.Manager:
                                data.ManagerAudit = data.OfficerAudit;
                                data.ManagerAuditDate = data.OfficerAuditDate;
                                data.ManagerRemark = data.OfficerRemark;
                                data.ManagerSignature = data.OfficerSignature;
                                break;
                            case (int)AdminType.Vice:
                                data.ViceAudit = data.OfficerAudit;
                                data.ViceAuditDate = data.OfficerAuditDate;
                                data.ViceRemark = data.OfficerRemark;
                                data.ViceSignature = data.OfficerSignature;
                                break;
                            //case (int)AdminType.Principal:
                            //    data.PrincipalAudit = data.OfficerAudit;
                            //    data.PrincipalAuditDate = data.OfficerAuditDate;
                            //    data.PrincipalRemark = data.OfficerRemark;
                            //    data.PrincipalSignature = data.OfficerSignature;
                            //    break;
                            case (int)AdminType.President:
                                data.PresidentAudit = data.OfficerAudit;
                                data.PresidentAuditDate = data.OfficerAuditDate;
                                data.PresidentRemark = data.OfficerRemark;
                                data.PresidentSignature = data.OfficerSignature;
                                break;
                        }
                    }
                }
                #endregion

                if (model.ID == 0)
                {
                    model.ID = internalAcceptanceRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    if (model.PrincipalAudit == Approved)
                    {
                        trainingInfo.IsApproved = true;
                        trainingRepository.Update(trainingInfo);
                    }
                    internalAcceptanceRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 送審
                if (model.IsAudit)
                {
                    var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.InternalAcceptance).FirstOrDefault();
                    #region 簽核狀態
                    int state = HandleState(model.TrainingInfo.CreaterDeptId, data.OfficerAudit, data.ManagerAudit, data.ViceAudit, data.PresidentAudit, data.PrincipalAudit);
                    #endregion

                    if (auditInfo == null)
                    {
                        var auditId = auditRepository.Insert(new Audit()
                        {
                            TrainingID = data.TrainingID,
                            PersonID = data.PersonID,
                            TargetID = data.ID,
                            Type = (int)AuditType.InternalAcceptance,
                            State = state,
                            IsInvalid = false,
                            Officer = model.TrainingInfo.Creater,
                            CreateDate = DateTime.Now
                        });
                    }
                    else
                    {
                        if (data.OfficerAudit == (int)FormAudit.Reject || data.ManagerAudit == (int)FormAudit.Reject || data.PrincipalAudit == (int)FormAudit.Reject || data.ViceAudit == (int)FormAudit.Reject || data.PresidentAudit == (int)FormAudit.Reject)
                        {
                            auditInfo.IsInvalid = true;

                            var personInfo = t8personRepository.FindByPersonId(data.PersonID);
                            //駁回通知承辦人
                            string mailBody = "";
                            mailBody = $"<h3>您所送審的【{trainingInfo.Title}】內訓驗收資料已駁回，請前往查看</h3>";

                            string signUrl = $"{Request.Url.Scheme}://220.228.58.43:18151/InternalAcceptanceAdmin/Edit?trainingId={trainingInfo.ID}&personId={data.PersonID}";

                            mailBody += $"<p>前往察看 : <a href=\"{signUrl}\">{signUrl}</a></p>";

                            //MailHelper.POP3Mail("i1o2i2o3i1@gmail.com", "【" + model.TrainingInfo.Title + "】已被駁回", mailBody);
                            MailHelper.POP3Mail(personInfo.Email, "【" + model.TrainingInfo.Title + "】的驗收資料已被駁回", mailBody);
                        }
                        auditInfo.State = state;
                        auditRepository.Update(auditInfo);
                    }
                    //string reciver = GetAuditRevicer(state, model.TrainingInfo.Creater, (int)AuditType.InternalAcceptance, model.PersonID);
                    //if (!string.IsNullOrEmpty(reciver))
                    //{
                    //    SendAuditNotify(model.TrainingID, model.ID, (int)AuditType.ClassOpinion, reciver);
                    //}
                }
                #endregion

                return RedirectToAction("Edit", new { trainingId = model.TrainingID, personId = model.PersonID, FromAudit = model.FromAudit });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            internalAcceptanceRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
        public ActionResult DeleteActionPlanFile(int id)
        {
            var query = internalAcceptanceRepository.GetById(id);
            string file = internalAcceptanceRepository.DeleteActionPlanFile(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { trainingId = query.TrainingID, personId = query.PersonID });
        }

        #region 審核階段通知

        #endregion    
    }
}