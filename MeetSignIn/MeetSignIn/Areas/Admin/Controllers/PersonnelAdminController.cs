﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class PersonnelAdminController : BaseAdminController
    {
        //private EmployeeRepository employeeRepository = new EmployeeRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public ActionResult Index(EmployeeIndexView model)
        {
            var query = personRepository.GetPersonQuery(model.Name, model.JobID, model.DeptID);
            var pageResult = query.ToPageResult<GroupJoinPersonAndDepartment>(model);
            model.PageResult = Mapper.Map<PageResult<EmployeeView>>(pageResult);

            return View(model);
        }

        //public ActionResult Edit(int id = 0)
        //{
        //    var model = new EmployeeView();
        //    if (id != 0)
        //    {
        //        var query = personRepository.FindBy(id);
        //        model = Mapper.Map<EmployeeView>(query);
        //    }
        //    else
        //    {

        //    }

        //    return View(model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //[ValidateInput(false)]
        //public ActionResult Edit(EmployeeView model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        GroupJoinPersonAndDepartment data = Mapper.Map<GroupJoinPersonAndDepartment>(model);
        //        int adminId = AdminInfoHelper.GetAdminInfo().ID;

        //        if (model.ID == 0)
        //        {
        //            model.ID = personRepository.Insert(data);
        //            ShowMessage(true, "新增成功");
        //        }
        //        else
        //        {
        //            personRepository.Update(data);
        //            ShowMessage(true, "修改成功");
        //        }

        //        return RedirectToAction("Edit", new { id = model.ID });
        //    }

        //    return View(model);
        //}

        //public ActionResult Delete(int id)
        //{
        //    personRepository.Delete(id);
        //    ShowMessage(true, "刪除成功");
        //    return RedirectToAction("Index");
        //}
    }
}