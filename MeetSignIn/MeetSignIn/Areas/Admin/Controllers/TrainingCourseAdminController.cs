﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    public class TrainingCourseAdminController : BaseAdminController
    {
        private ExternalCourseRepository externalCourseRepository = new ExternalCourseRepository(new MeetingSignInDBEntities());
        private InternalCourseRepository internalCourseRepository = new InternalCourseRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private TrainingImageRepository trainingImageRepository = new TrainingImageRepository(new MeetingSignInDBEntities());
        private RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        private ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());

        public ActionResult ECEdit(int tid = 0)
        {
            var model = new ExternalCourseIndexView();
            if (tid == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            model.TrainingID = tid;
            model.TrainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(tid));
            var planQuery = externalPlanRepository.Query(tid).OrderByDescending(q => q.ID).FirstOrDefault();
            if (model.TrainingInfo == null || planQuery == null)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            model.ExternalID = planQuery.ID;
            model.CourseList = Mapper.Map<List<ExternalCourseView>>(externalCourseRepository.Query(tid, planQuery.ID));
            for (int i = 0; i < model.CourseList.Count(); i++)
            {
                var item = model.CourseList[i];
                var trainingData = trainingRepository.GetAll().Where(q => q.TrainingCourseID == item.ID).FirstOrDefault();
                if (i == 0 && trainingData == null)
                {
                    trainingData = trainingRepository.GetById(tid);
                    trainingData.TrainingCourseID = model.CourseList[i].ID;
                    trainingRepository.Update(trainingData);
                }
                if (trainingData != null)
                {
                    string roomNumber = trainingData.RoomID == 0 ? "" : roomRepository.FindBy(trainingData.RoomID).RoomNumber;
                    //item.SingInUrl = $"{Request.Url.Scheme}://{officialHost}/Sign/TrainingCheck?rn={roomNumber}&tid={trainingData.ID}";
                    item.OpinionUrl = $"{Request.Url.Scheme}://{officialHost}/Admin/ClassOpinionAdmin/ToOpinion?trainingId={trainingData.ID}";
                    item.SeriesTrainingID = trainingData.ID;
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ECEdit(ExternalCourseIndexView model)
        {
            try
            {
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                var trainingInfo = trainingRepository.GetById(model.TrainingID);
                if (adminId != trainingInfo.Creater && adminInfo.Type > (int)AdminType.Manager)
                {
                    ShowMessage(false, "儲存失敗，無修改權限");
                    return RedirectToAction("ICEdit", new { tid = model.TrainingID });
                }

                #region 關聯課程
                if (!string.IsNullOrEmpty(model.JsonCourse))
                {
                    var jsonCourse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ExternalCourse>>(model.JsonCourse);
                    var existList = externalCourseRepository.Query(model.TrainingID, model.ExternalID).ToList();
                    var newIdList = jsonCourse.Select(q => q.ID).ToList();
                    if (model.TrainingID != 0 && model.ExternalID != 0)
                    {
                        //var oldCourseQuery = externalCourseRepository.Query(model.TrainingID, model.ID);

                        foreach (var item in jsonCourse)
                        {
                            if (item.ID == 0)
                            {
                                externalCourseRepository.Insert(new ExternalCourse()
                                {
                                    ExternalID = model.ExternalID,
                                    TrainingID = model.TrainingID,
                                    Organizer = item.Organizer,
                                    Lecturer = item.Lecturer,
                                    CourseMaterial = item.CourseMaterial,
                                    Address = item.Address,
                                    StartDate = item.StartDate,
                                    EndDate = item.EndDate,
                                    Registery = item.Registery,
                                    Travel = item.Travel,
                                    Others = item.Others,
                                    Total = item.Total,
                                    DeviceCheck = item.DeviceCheck,
                                    Device = item.Device,
                                    IsOnline = true,
                                });
                            }
                            else
                            {
                                var courseInfo = Mapper.Map<ExternalCourse>(item);
                                externalCourseRepository.Update(courseInfo);
                            }
                        }

                        //停用課程
                        var offLineList = existList.Where(q => !newIdList.Contains(q.ID)).ToList();
                        foreach (var item in offLineList)
                        {
                            var disableData = externalCourseRepository.GetById(item.ID);
                            disableData.IsOnline = false;
                            externalCourseRepository.Update(disableData);
                            //externalCourseRepository.Delete(item.ID);
                        }
                    }
                }
                #endregion
                ShowMessage(true, "");
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }

            return RedirectToAction("ICEdit", new { tid = model.TrainingID });
        }

        public ActionResult ICEdit(int tid = 0)
        {
            var model = new InternalCourseIndexView();
            if (tid == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            model.TrainingID = tid;
            model.TrainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(tid));
            var planQuery = internalPlanRepository.Query(tid).OrderByDescending(q => q.ID).FirstOrDefault();
            if (model.TrainingInfo == null || planQuery == null)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            model.InternalID = planQuery.ID;
            model.CourseList = Mapper.Map<List<InternalCourseView>>(internalCourseRepository.Query(tid, planQuery.ID).OrderBy(q => q.ID));
            model.CourseListJson = JsonSerializer.Serialize(new CourseJsonObj() { 
                CourseList = model.CourseList
            });
            for (int i = 0; i < model.CourseList.Count(); i++)
            {
                var item = model.CourseList[i];
                item.Registed = false;               
                var trainingData = trainingRepository.GetAll().Where(q => q.TrainingCourseID == item.ID).FirstOrDefault();
                if (i == 0 && trainingData == null)
                {
                    trainingData = trainingRepository.GetById(tid);
                    trainingData.TrainingCourseID = model.CourseList[i].ID;
                    trainingRepository.Update(trainingData);
                }
                if (trainingData != null)
                {
                    string roomNumber = trainingData.RoomID == 0 ? "" : roomRepository.FindBy(trainingData.RoomID).RoomNumber;
                    item.SingInUrl = $"{Request.Url.Scheme}://{officialHost}/Sign/TrainingCheck?rn={roomNumber}&tid={trainingData.ID}";
                    item.OpinionUrl = $"{Request.Url.Scheme}://{officialHost}/Admin/ClassOpinionAdmin/ToOpinion?trainingId={trainingData.ID}";
                    item.SeriesTrainingID = trainingData.ID;
                    //會議室登記狀態
                    TrainingAdminController trainingAdminController = new TrainingAdminController();
                    var result = trainingAdminController.CheckRoomStatus(trainingData.RoomID, trainingData.StartDate, trainingData.EndDate, 0);
                    item.Registed = result != "可選會議室";
                }               
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ICEdit(InternalCourseIndexView model)
        {
            try
            {
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                var trainingInfo = trainingRepository.GetById(model.TrainingID);
                if (adminId != trainingInfo.Creater && adminInfo.Type > (int)AdminType.Manager)
                {
                    ShowMessage(false, "儲存失敗，無修改權限");
                    return RedirectToAction("ICEdit", new { tid = model.TrainingID });
                }

                #region 關聯課程
                ///課程建立對應簽到表
                ///須注意儲存取得當前課程清單
                if (!string.IsNullOrEmpty(model.JsonCourse))
                {
                    var jsonCourse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<InternalCourse>>(model.JsonCourse);
                    //舊課程
                    var existList = internalCourseRepository.Query(model.TrainingID, model.InternalID).ToList();
                    //新課程ID
                    var newIdList = jsonCourse.Select(q => q.ID).ToList();
                    if (model.TrainingID != 0 && model.InternalID != 0)
                    {
                        //var oldCourseQuery = internalCourseRepository.Query(model.TrainingID, model.ID);

                        //更新、添加課程
                        foreach (var item in jsonCourse)
                        {
                            if (item.ID == 0)
                            {
                                internalCourseRepository.Insert(new InternalCourse()
                                {
                                    InternalID = model.InternalID,
                                    TrainingID = model.TrainingID,
                                    Outline = item.Outline,
                                    Date = item.Date,
                                    Time = item.Time,
                                    Hours = item.Hours,
                                    Progress = item.Progress,
                                    Lecturer = item.Lecturer,
                                    DeviceCheck = item.DeviceCheck,
                                    Device = item.Device,
                                    TrainingMethod = item.TrainingMethod,
                                    IsOnline = true,
                                });
                            }
                            else
                            {
                                var courseInfo = Mapper.Map<InternalCourse>(item);
                                internalCourseRepository.Update(courseInfo);
                            }
                        }

                        //停用課程
                        var offLineList = existList.Where(q => !newIdList.Contains(q.ID)).ToList();
                        foreach (var item in offLineList)
                        {
                            var disableData = internalCourseRepository.GetById(item.ID);
                            disableData.IsOnline = false;
                            internalCourseRepository.Update(disableData);
                            //var trainingQuery = trainingRepository.
                            //internalCourseRepository.Delete(item.ID);
                        }
                    }
                }
                #endregion
                ShowMessage(true, "儲存成功");
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }

            return RedirectToAction("ICEdit", new { tid = model.TrainingID });
        }

        [HttpPost]
        public ActionResult SeriesFiles(InternalCourseIndexView model)
        {
            if (model.ID == 0 && model.SeriesTrainingID == 0)
            {
                return RedirectToAction("ICEdit", new { tid = model.ID });
            }
            var uploadData = model.UploadFilesData;
            //Training data = Mapper.Map<Training>(uploadData);
            var data = trainingRepository.GetById(model.SeriesTrainingID);
            string trainingFolder = PhotoFolder.Replace("TrainingCoursePhoto", "TrainingPhoto");
            try
            {
                if (uploadData.PicsFiles != null)
                {//上傳多圖
                    foreach (HttpPostedFileBase pic in uploadData.PicsFiles)
                    {
                        bool hasPic = ImageHelper.CheckFileExists(pic);
                        if (hasPic)
                        {
                            TrainingImage fileData = new TrainingImage();
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            //fileData.TrainingID = model.ID;
                            fileData.TrainingID = model.SeriesTrainingID;
                            string fileName = pic.FileName;
                            var fileNameArr = fileName.Split('.');
                            string saveFileName = "";
                            for (int i = 0; i < fileNameArr.Count() - 1; i++)
                            {
                                saveFileName += fileNameArr[i];
                            }
                            fileData.Pics = ImageHelper.SaveFile(pic, trainingFolder, model.SeriesTrainingID + "_" + saveFileName);
                            trainingImageRepository.Insert(fileData);
                        }
                    }
                }
                bool hasFile = ImageHelper.CheckFileExists(uploadData.TraningFile);
                bool hasVideo = ImageHelper.CheckFileExists(uploadData.TraningVideo);
                bool hasReport = ImageHelper.CheckFileExists(uploadData.TraningReport);
                if (hasFile)
                {//檔案
                    ImageHelper.DeleteFile(PhotoFolder, uploadData.MainFile);
                    string fileName = uploadData.TraningFile.FileName;
                    var fileNameArr = fileName.Split('.');
                    string saveFileName = "";
                    for (int i = 0; i < fileNameArr.Count() - 1; i++)
                    {
                        saveFileName += fileNameArr[i];
                    }
                    data.MainFile = ImageHelper.SaveFile(uploadData.TraningFile, trainingFolder, model.SeriesTrainingID + "_" + saveFileName);
                }
                if (hasVideo)
                {//影片
                    ImageHelper.DeleteFile(PhotoFolder, uploadData.MainVideo);
                    string fileName = uploadData.TraningVideo.FileName;
                    var fileNameArr = fileName.Split('.');
                    string saveFileName = "";
                    for (int i = 0; i < fileNameArr.Count() - 1; i++)
                    {
                        saveFileName += fileNameArr[i];
                    }
                    data.MainVideo = ImageHelper.SaveFile(uploadData.TraningVideo, trainingFolder, model.SeriesTrainingID + "_" + saveFileName);
                }
                if (hasReport)
                {//報告
                    ImageHelper.DeleteFile(PhotoFolder, uploadData.MainReport);
                    string fileName = uploadData.TraningReport.FileName;
                    var fileNameArr = fileName.Split('.');
                    string saveFileName = "";
                    for (int i = 0; i < fileNameArr.Count() - 1; i++)
                    {
                        saveFileName += fileNameArr[i];
                    }
                    data.MainReport = ImageHelper.SaveFile(uploadData.TraningReport, trainingFolder, model.SeriesTrainingID + "_" + saveFileName);
                }
                trainingRepository.Update(data);
                ShowMessage(true, "儲存成功");
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }
            string returnPage = "ICEdit";
            if (data.Category == (int)Category.External)
            {
                returnPage = "ECEdit";
            }
            if (data.SeriesParentID != 0)
            {
                return RedirectToAction(returnPage, new { tid = data.SeriesParentID });
            }
            return RedirectToAction(returnPage, new { tid = data.ID });
        }

        public string ActiveICourse(int id, int index)
        {
            try
            {
                //需重新考慮系列課程更新會全部重建問題
                //新建附屬訓練課程需綁定系列課程編號
                var courseQuery = internalCourseRepository.GetById(id);
                var trainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(courseQuery.TrainingID));
                //TrainingAdminController trainingAdminController = new TrainingAdminController();
                //var result = trainingAdminController.CheckRoomStatus(trainingInfo.RoomID);

                var trainingData = trainingRepository.GetAll().Where(q => q.TrainingCourseID == courseQuery.ID).FirstOrDefault();
                if (courseQuery != null && trainingData == null)
                {
                    trainingInfo.ID = 0;
                    var seriesTraining = Mapper.Map<Training>(trainingInfo);
                    seriesTraining.SeriesParentID = courseQuery.TrainingID;
                    seriesTraining.TrainingCourseID = courseQuery.ID;
                    seriesTraining.RoomID = trainingInfo.RoomID;
                    seriesTraining.FactoryID = trainingInfo.FactoryID;
                    seriesTraining.DeptID = trainingInfo.DeptID;
                    var courseDate = courseQuery.Date.ToString("yyyy-MM-dd");
                    seriesTraining.SignDate = Convert.ToDateTime(courseDate + trainingInfo.SignDate.ToString(" HH:mm:ss"));
                    seriesTraining.StartDate = Convert.ToDateTime(courseDate + trainingInfo.StartDate.ToString(" HH:mm:ss"));
                    seriesTraining.EndDate = Convert.ToDateTime(courseDate + trainingInfo.EndDate.ToString(" HH:mm:ss"));
                    seriesTraining.MainPic = null;
                    seriesTraining.MainVideo = null;
                    seriesTraining.MainFile = null;
                    seriesTraining.MainReport = null;
                    seriesTraining.DeviceCheck = courseQuery.DeviceCheck;
                    seriesTraining.Device = courseQuery.Device;
                    int seriesId = trainingRepository.Insert(seriesTraining);

                    var trainingPersonList = trainingPersonRepository.Query(null, "", courseQuery.TrainingID).ToList();
                    foreach (var tp in trainingPersonList)
                    {
                        var seriesTrainingPerson = new TrainingPerson();
                        seriesTrainingPerson = tp;
                        seriesTrainingPerson.TraningID = seriesId;
                        seriesTrainingPerson.JobID = tp.JobID;
                        seriesTrainingPerson.Name = tp.Name;
                        seriesTrainingPerson.IsSign = false;
                        seriesTrainingPerson.SignStatus = (int)SignStatus.UnSigned;
                        seriesTrainingPerson.IsSent = false;
                        seriesTrainingPerson.IsPassed = false;
                        seriesTrainingPerson.SignTime = null;
                        trainingPersonRepository.Insert(seriesTrainingPerson);
                    }
                    return "success";
                }
                return "exist";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string ActiveECourse(int id, int index)
        {
            try
            {
                var courseQuery = externalCourseRepository.GetById(id);
                if (courseQuery != null)
                {
                    var trainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(courseQuery.TrainingID));
                    trainingInfo.ID = 0;
                    var seriesTraining = Mapper.Map<Training>(trainingInfo);
                    seriesTraining.SeriesParentID = trainingInfo.ID;
                    seriesTraining.TrainingCourseID = courseQuery.ID;
                    seriesTraining.DeviceCheck = courseQuery.DeviceCheck;
                    seriesTraining.Device = courseQuery.Device;
                    int seriesId = trainingRepository.Insert(seriesTraining);

                    var trainingPersonList = trainingPersonRepository.Query(true, "", courseQuery.TrainingID).ToList();
                    foreach (var tp in trainingPersonList)
                    {
                        var seriesTrainingPerson = new TrainingPerson();
                        seriesTrainingPerson = tp;
                        seriesTrainingPerson.ID = 0;
                        seriesTrainingPerson.TraningID = seriesId;
                        trainingPersonRepository.Insert(seriesTrainingPerson);
                    }
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public ActionResult DeleteEC(int id)
        {
            var courseInfo = externalCourseRepository.GetById(id);
            externalCourseRepository.Delete(id);
            ShowMessage(true, "課程已刪除");
            return RedirectToAction("ECEdit", "TrainingCourseAdmin", new { tid = courseInfo.TrainingID });
        }
        public ActionResult DeleteIC(int id)
        {
            var courseInfo = internalCourseRepository.GetById(id);
            internalCourseRepository.Delete(id);
            ShowMessage(true, "課程已刪除");
            return RedirectToAction("ICEdit", "TrainingCourseAdmin", new { tid = courseInfo.TrainingID });
        }

        public JsonResult GetSeriesFiles(int trainingId)
        {
            try
            {
                var query = trainingRepository.GetById(trainingId);
                var data = Mapper.Map<TrainingView>(query);
                data.PicList = trainingImageRepository.Query(trainingId).ToList();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public class CourseJsonObj
        {
            public List<InternalCourseView> CourseList { get; set; }
        }
    }
}