﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.OptionGroup;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class OptionGroupAdminController : BaseAdminController
    {
        private OptionGroupRepository optionGroupRepository = new OptionGroupRepository(new MeetingSignInDBEntities());
        private OptionItemRepository optionItemRepository = new OptionItemRepository(new MeetingSignInDBEntities());

        public ActionResult Index(OptionGroupIndexView model)
        {
            var query = optionGroupRepository.Query(model.Title);
            var pageResult = query.ToPageResult<OptionGroup>(model);
            model.PageResult = Mapper.Map<PageResult<OptionGroupView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new OptionGroupView();
            if (id != 0)
            {
                var query = optionGroupRepository.FindBy(id);
                model = Mapper.Map<OptionGroupView>(query);
                model.ItemList = Mapper.Map<List<OptionItemView>>(optionItemRepository.Query(null, id));
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(OptionGroupView model)
        {
            if (ModelState.IsValid)
            {
                OptionGroup data = Mapper.Map<OptionGroup>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = optionGroupRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    optionGroupRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            optionGroupRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public string SaveOptionItem(int id, int optiongroupId, string title, int sort, bool isonline)
        {
            try
            {
                if (id != 0)
                {
                    var query = optionItemRepository.GetById(id);
                    query.Title = title;
                    query.Sort = sort;
                    query.IsOnline = isonline;
                    optionItemRepository.Update(query);
                }
                else
                {
                    optionItemRepository.Insert(new OptionItem()
                    {
                        OptionGroupID = optiongroupId,
                        Title = title,
                        Sort = sort,
                        IsOnline = isonline
                    });
                }

                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

        public JsonResult GetOptionGroup()
        {
            var query = optionGroupRepository.Query();

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOptionItem(int itemId)
        {
            var query = optionItemRepository.GetById(itemId);

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public string DeleteOptionItem(int Id)
        {
            try
            {
                optionItemRepository.Delete(Id);
                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }
    }
}