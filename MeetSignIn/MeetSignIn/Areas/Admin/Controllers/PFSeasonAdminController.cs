﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.PFSeason;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class PFSeasonAdminController : BaseAdminController
    {
        private PFSeasonRepository pFSeasonRepository = new PFSeasonRepository(new MeetingSignInDBEntities());
        private comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());

        public ActionResult Index(PFSeasonIndexView model)
        {
            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;
            var query = pFSeasonRepository.Query(model.IsOnline, model.ClassID, model.DeptId, model.Title);
            var pageResult = query.ToPageResult<PFSeason>(model);
            model.PageResult = Mapper.Map<PageResult<PFSeasonView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new PFSeasonView();
            
            if (id != 0)
            {
                var query = pFSeasonRepository.FindBy(id);
                model = Mapper.Map<PFSeasonView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PFSeasonView model)
        {
            if (ModelState.IsValid)
            {
                PFSeason data = Mapper.Map<PFSeason>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = pFSeasonRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    pFSeasonRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID, DeptId = model.DeptId });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            pFSeasonRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}