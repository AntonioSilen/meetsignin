﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.ClassOpinion;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class ClassOpinionAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private ClassOpinionRepository classOpinionRepository = new ClassOpinionRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());

        // GET: Admin/ClassOpinionAdmin
        public ActionResult Index(ClassOpinionIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (model.TrainingID == 0)
            {
                if (adminInfo.Type >= (int)AdminType.ClassSupervisor)
                {
                    return RedirectToAction("Participate", "TrainingAdmin", new { PersonID = adminInfo.Account });
                }
                else
                {
                    return RedirectToAction("Index", "TrainingAdmin");
                }
            }
            var query = classOpinionRepository.Query(model.TrainingID, model.PersonID, model.Name);
            var pageResult = query.ToPageResult<ClassOpinion>(model);
            model.PageResult = Mapper.Map<PageResult<ClassOpinionView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0, int trainingId = 0, string personId = "", bool isNew = false, bool FromAudit = false, string lang = "")
        {
            if (trainingId == 0 || string.IsNullOrEmpty(personId))
            {
                return RedirectToAction("Index", "HomeAdmin");
            }
            var model = new ClassOpinionView();
            var query = classOpinionRepository.Query(trainingId, personId).OrderByDescending(q => q.ID).FirstOrDefault();
            if (query != null)
            {//舊資料
                model = Mapper.Map<ClassOpinionView>(query);
                if (isNew && model.IsAudit)
                {//取消送審/重新編輯
                    var newData = Mapper.Map<ClassOpinionView>(query);
                    var oldAudit = auditRepository.Query(null, query.ID, (int)AuditType.ClassOpinion, null, "", "", query.TrainingID).FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        //重置審核資料
                        newData.ID = 0;
                        newData.IsAudit = false;
                        newData.OfficerAudit = 1;
                        newData.OfficerRemark = "";
                        newData.OfficerSignature = "";
                        newData.ManagerAudit = 1;
                        newData.ManagerRemark = "";
                        newData.ManagerSignature = "";
                        newData.ViceAudit = 1;
                        newData.ViceRemark = "";
                        newData.ViceSignature = "";
                        newData.PrincipalAudit = 1;
                        newData.PrincipalRemark = "";
                        newData.PrincipalSignature = "";
                        newData.PresidentAudit = 1;
                        newData.PresidentRemark = "";
                        newData.PresidentSignature = "";
                        newData.AuditInfo = model.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.ClassOpinion, query.TrainingID, query.PersonID);
                        return View(newData);
                    }
                }
            }
            else
            {//新建資料
                model = new ClassOpinionView()
                {
                    PersonID = personId,
                    TrainingID = trainingId,
                    Name = t8personRepository.FindByPersonId(personId).Name
                };
            }
            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var personInfo = t8personRepository.FindByPersonId(adminRepository.GetById(adminInfo.ID).Account);
            if (personInfo != null)
            {
                model.OfficerDeptId = personInfo.DeptID;
                model.OfficerDeptName = personInfo.DepartmentStr;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ClassOpinionView model)
        {
            model.Expect = string.IsNullOrEmpty(model.Expect) ? "無" : model.Expect;
            model.Advice = string.IsNullOrEmpty(model.Advice) ? "無" : model.Advice;
            if (ModelState.IsValid)
            {
                ClassOpinion data = Mapper.Map<ClassOpinion>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                #region 日期處理
                data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                data.PrincipalAuditDate = SetAuditDate(data.PrincipalAudit, data.PrincipalAuditDate);
                data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                #endregion

                #region 簽章
                ////承辦人
                //var officerInfo = adminRepository.GetById(model.TrainingInfo.Creater);
                //if (data.OfficerAudit > 1)
                //{
                //    data.OfficerSignature = officerInfo == null ? emptySignature : officerInfo.Account + ".jpg";
                //    int intCK;
                //    if (!int.TryParse(officerInfo.Account, out intCK))
                //    {
                //        data.OfficerSignature = adminSignature;
                //    }
                //}
                ////主管(Fairly.jpg無部門帳號的菲力簽章)
                //if (data.ManagerAudit > 1)
                //{
                //    var personInfo = t8personRepository.FindByJobID(officerInfo.Account);
                //    var personDept = personInfo == null ? "0001" : personInfo.DeptID;
                //    var tempSameDept = t8personRepository.GetPersonQuery("", "", personDept).ToList();
                //    var deptPersonIdList = tempSameDept.Select(q => q.PersonId).ToList();
                //    var managerInfo = adminRepository.Query(true, (int)AdminType.Manager).Where(q => deptPersonIdList.Contains(q.Account)).FirstOrDefault();
                //    data.ManagerSignature = managerInfo == null ? emptySignature : managerInfo.Account + ".jpg";
                //}
                ////副總
                //if (data.ViceAudit > 1)
                //{
                //    var vice = adminRepository.Query(true, (int)AdminType.Vice).FirstOrDefault();
                //    data.ViceSignature = vice == null ? emptySignature : vice.Account + ".jpg";
                //}
                ////校長
                //if (data.PrincipalAudit > 1)
                //{
                //    var principal = adminRepository.Query(true, (int)AdminType.Principal).FirstOrDefault();
                //    data.PrincipalSignature = principal == null ? emptySignature : principal.Account + ".jpg";
                //}
                ////總經理
                //if (data.PresidentAudit > 1)
                //{
                //    var president = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                //    data.PresidentSignature = president == null ? emptySignature : president.Account + ".jpg";
                //}
                #endregion

                #region 檔案處理               
                //bool hasFile = ImageHelper.CheckFileExists(model.ManagerPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.ManagerSignature);
                //    data.ManagerSignature = ImageHelper.SaveFile(model.ManagerPost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.VicePost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.ViceSignature);
                //    data.ViceSignature = ImageHelper.SaveFile(model.VicePost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.OfficerPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.OfficerSignature);
                //    data.OfficerSignature = ImageHelper.SaveFile(model.OfficerPost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.PrincipalPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.PrincipalSignature);
                //    data.PrincipalSignature = ImageHelper.SaveFile(model.PrincipalPost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.PresidentPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.PresidentSignature);
                //    data.PresidentSignature = ImageHelper.SaveFile(model.PresidentPost, PhotoFolder);
                //}
                #endregion

                #region 簽核人員
                //if (adminInfo.Type < (int)AdminType.Employee && adminInfo.Type > (int)AdminType.Sysadmin)
                //{
                //    var trainingInfo = trainingRepository.GetById(data.TrainingID);
                //    if (trainingInfo.Creater == adminInfo.ID)
                //    {
                //        switch (adminInfo.Type)
                //        {
                //            case (int)AdminType.Manager:
                //                data.ManagerAudit = data.OfficerAudit;
                //                data.ManagerAuditDate = data.OfficerAuditDate;
                //                data.ManagerRemark = data.OfficerRemark;
                //                data.ManagerSignature = data.OfficerSignature;
                //                break;
                //            case (int)AdminType.Vice:
                //                data.ViceAudit = data.OfficerAudit;
                //                data.ViceAuditDate = data.OfficerAuditDate;
                //                data.ViceRemark = data.OfficerRemark;
                //                data.ViceSignature = data.OfficerSignature;
                //                break;
                //            case (int)AdminType.Principal:
                //                data.PrincipalAudit = data.OfficerAudit;
                //                data.PrincipalAuditDate = data.OfficerAuditDate;
                //                data.PrincipalRemark = data.OfficerRemark;
                //                data.PrincipalSignature = data.OfficerSignature;
                //                break;
                //            case (int)AdminType.President:
                //                data.PresidentAudit = data.OfficerAudit;
                //                data.PresidentAuditDate = data.OfficerAuditDate;
                //                data.PresidentRemark = data.OfficerRemark;
                //                data.PresidentSignature = data.OfficerSignature;
                //                break;
                //        }
                //    }
                //}
                #endregion

                SetOpinion(data);

                if (model.ID == 0)
                {
                    model.ID = classOpinionRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    classOpinionRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 送審               
                //if (model.IsAudit)
                //{
                //    var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.ClassOpinion).FirstOrDefault();
                //    var state = (int)AuditState.Processing;
                //    if (data.OfficerAudit == (int)FormAudit.Approval)
                //    {
                //        state = (int)AuditState.OfficerAudit;
                //    }
                //    if (data.ManagerAudit == (int)FormAudit.Approval)
                //    {
                //        state = (int)AuditState.ManagerAudit;
                //    }
                //    if (data.ViceAudit == (int)FormAudit.Approval)
                //    {
                //        state = (int)AuditState.ViceAudit;
                //    }
                //    if (data.PresidentAudit == (int)FormAudit.Approval)
                //    {
                //        state = (int)AuditState.PresidentAudit;
                //    }
                //    if (data.PrincipalAudit == (int)FormAudit.Approval)
                //    {
                //        state = (int)AuditState.PrincipalAudit;
                //    }
                //    if (data.OfficerAudit == (int)FormAudit.Reject || data.ManagerAudit == (int)FormAudit.Reject || data.PrincipalAudit == (int)FormAudit.Reject || data.ViceAudit == (int)FormAudit.Reject || data.PresidentAudit == (int)FormAudit.Reject)
                //    {
                //        auditInfo.IsInvalid = true;
                //    }
                //    if (auditInfo == null)
                //    {
                //        var auditId = auditRepository.Insert(new Audit()
                //        {
                //            TrainingID = data.TrainingID,
                //            PersonID = data.PersonID,
                //            TargetID = data.ID,
                //            Type = (int)AuditType.ClassOpinion,
                //            State = state,
                //            IsInvalid = false,
                //            Officer = model.TrainingInfo.Creater,
                //            CreateDate = DateTime.Now
                //        });
                //    }
                //    else
                //    {
                //        auditInfo.State = state;
                //        auditRepository.Update(auditInfo);
                //    }

                //    //寄送通知
                //    string reciver = GetAuditRevicer(state, model.TrainingInfo.Creater, (int)AuditType.ClassOpinion, model.PersonID);
                //    if (!string.IsNullOrEmpty(reciver))
                //    {
                //        //SendAuditNotify(model.TrainingID, model.ID, (int)AuditType.ClassOpinion, reciver);
                //    }
                //}
                #endregion

                return RedirectToAction("Edit", new { trainingId = model.TrainingID, personId = model.PersonID, FromAudit = model.FromAudit });
            }
            ShowMessage(false, "儲存失敗");
            return View(model);
        }

        private static void SetOpinion(ClassOpinion data)
        {
            int normalOpinion = (int)AdmitRadio.L4;
            if (data.Compatibility == 0)
            {
                data.Compatibility = normalOpinion;
            }
            if (data.Architecture == 0)
            {
                data.Architecture = normalOpinion;
            }
            if (data.Schedule == 0)
            {
                data.Schedule = normalOpinion;
            }
            if (data.CourseMaterials == 0)
            {
                data.CourseMaterials = normalOpinion;
            }
            if (data.Device == 0)
            {
                data.Device = normalOpinion;
            }
            if (data.Professional == 0)
            {
                data.Professional = normalOpinion;
            }
            if (data.Expression == 0)
            {
                data.Expression = normalOpinion;
            }
            if (data.TimeControl == 0)
            {
                data.TimeControl = normalOpinion;
            }
            if (data.TeachingMethod == 0)
            {
                data.TeachingMethod = normalOpinion;
            }
            if (data.Recommended == 0)
            {
                data.Recommended = normalOpinion;
            }
            if (data.ActualUsage == 0)
            {
                data.ActualUsage = normalOpinion;
            }
            if (data.CurrentUsage == 0)
            {
                data.CurrentUsage = normalOpinion;
            }
            if (data.FutureUsage == 0)
            {
                data.FutureUsage = normalOpinion;
            }
            if (data.Difficulty == 0)
            {
                data.Difficulty = 2;
            }
        }

        public ActionResult Delete(int id)
        {
            classOpinionRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult ToOpinion(int trainingId = 0)
        {
            if (trainingId == 0)
            {
                ShowMessage(false, "缺少訓練編號。");
                return RedirectToAction("Index", "TrainingAdmin");
            }

            var lang = Request.QueryString["lang"];

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var trainingInfo = trainingRepository.GetById(trainingId);
            bool inCourse = true;
            //if (trainingInfo.SeriesParentID != 0)
            //{
            //    inCourse = trainingPersonRepository.Query(null, "", trainingInfo.SeriesParentID, adminInfo.Account).Count() > 0;
            //}
            //else
            //{
                inCourse = trainingPersonRepository.Query(null, "", trainingId, adminInfo.Account).Count() > 0;
            //}
            if (inCourse)
            {
                return RedirectToAction("Edit", "ClassOpinionAdmin", new { trainingId = trainingId, personId = adminInfo.Account, lang = lang });
            }
            else
            {
                ShowMessage(false, "您不在此次訓練名單中。");
                return RedirectToAction("Index", "TrainingAdmin");
            }
            //ViewBag.TrainingID = trainingId;
            //return View();
        }

        [HttpPost]
        public ActionResult ToOpinion(int trainingId = 0, string personId = "")
        {
            if (trainingId == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }

            if (!string.IsNullOrEmpty(personId))
            {
                if (trainingPersonRepository.Query(null, "", trainingId, personId).Count() > 0)
                {
                    return RedirectToAction("Edit", "ClassOpinionAdmin", new { trainingId = trainingId, personId = personId });
                }
                else
                {
                    ShowMessage(false, "您不在此次訓練名單中。");
                }
            }

            ViewBag.TrainingID = trainingId;
            return View();
        }

        #region 審核階段通知
        public ActionResult SendAuditNotify(int tid, int targetId, int type, string personId)
        {
            try
            {
                var trainingInfo = Mapper.Map<ViewModels.Training.TrainingView>(trainingRepository.FindBy(tid));
                if (trainingInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無教育訓練資料");
                }
                //審核紀錄
                var auditInfo = auditRepository.FindByTarget(targetId, type, tid);
                //收信人
                var reciverList = t8personRepository.GetPersonQuery("", personId);
                foreach (var item in reciverList)
                {
                    //寄送Email
                    //string from = "a842695137@gmail.com";
                    //string to = "seelen12@fairlybike.com";
                    string to = item.Email;
                    string subject = "";
                    string mailBody = "";
                    mailBody = $"<h3>【{trainingInfo.Title}】 {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{trainingInfo.RoomStr}</h3>" +
                                                $"<p>發起人 : {trainingInfo.Organiser}</p>" +
                                                $"<p>被通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                    string signUrl = $"{Request.Url.Scheme}://220.228.58.43:18151/Sign/TrainingCheck?rn={trainingInfo.RoomNum}&tid={trainingInfo.ID}";
                    if (!string.IsNullOrEmpty(trainingInfo.Lecturer))
                    {
                        mailBody += $"<p>講師 : {trainingInfo.Lecturer}";
                    }
                    mailBody += $"<p>預定開始時間 : {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                        $"<p>預定結束時間 : {trainingInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";
                    if (trainingInfo.FactoryID == 9)
                    {
                        mailBody += $"<p>簽到連結 : <a href=\"{signUrl}\">{signUrl}</a></p>";
                        if (!string.IsNullOrEmpty(trainingInfo.Url))
                        {
                            var urlArr = Regex.Split(trainingInfo.Url, "\r\n");
                            for (int i = 0; i < urlArr.Count(); i++)
                            {
                                mailBody += $"<p>會議連結 第{i}場 : <a href=\"{urlArr[i]}\">{urlArr[i]}</a></p>";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.Note))
                    {
                        mailBody += $"<p>    注意事項 : </p>";
                        var urlArr = Regex.Split(trainingInfo.Note, "\r\n");
                        for (int i = 0; i < urlArr.Count(); i++)
                        {
                            mailBody += $"<p>    {urlArr[i].Replace("  ", "  ")}</p>";
                        }
                    }

                    subject = $"教育訓練通知 - 系統信件";

                    MailHelper.POP3Mail(to, subject, mailBody);
                }

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "TrainingAdmin", new { id = tid });
        }
        #endregion
    }
}