﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Areas.Admin.ViewModels.PFSeason;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class PFCategoryAdminController : BaseAdminController
    {
        private PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
        public string CLS = EnumHelper.GetDescription(LevelType.CLS);
        public string CATE = EnumHelper.GetDescription(LevelType.CATE);
        public string ITM = EnumHelper.GetDescription(LevelType.ITM);
        public string LVL = EnumHelper.GetDescription(LevelType.LVL);
        public string WORK = EnumHelper.GetDescription(LevelType.WORK);

        public ActionResult Index(PFCategoryIndexView model)
        {
            var query = pFCategoryRepository.Query();
            var pageResult = query.ToPageResult<PFCategory>(model);
            model.PageResult = Mapper.Map<PageResult<PFCategoryView>>(pageResult);

            return View(model);
        }

        public ActionResult DeptPFEdit(string DeptId, int id = 0)
        {
            var model = new PFCategoryDeptView();
            if (string.IsNullOrEmpty(DeptId))
            {
                return RedirectToAction("Index");
            }
            model.DeptId = DeptId.Split('_')[0];
            model.DeptName = DeptId.Split('_')[1];
            PFHelper pFHelper = new PFHelper();
            model.SeasonInfo = Mapper.Map<PFSeasonView>(pFHelper.GetActivatePFSeason(model.DeptId));
            if (id != 0)
            {
                var query = pFCategoryRepository.FindBy(id);
                model = Mapper.Map<PFCategoryDeptView>(query);
            }
            
            model.ClassList = Mapper.Map<List<PFCategoryView>>(pFCategoryRepository.Query(null, model.DeptId, 0, CLS));

            return View(model);
        }

        public ActionResult Edit(string DeptId, int id = 0)
        {
            var model = new PFCategoryView();
            if (string.IsNullOrEmpty(DeptId))
            {
                return RedirectToAction("Index");
            }
            if (id != 0)
            {
                var query = pFCategoryRepository.FindBy(id);
                model = Mapper.Map<PFCategoryView>(query);
            }
            model.DeptId = DeptId.Split('_')[0];
            model.DeptName = DeptId.Split('_')[1];
            model.ClassList = Mapper.Map<List<PFCategoryView>>(pFCategoryRepository.Query(null, model.DeptId, 0, CLS));

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PFCategoryView model)
        {
            if (ModelState.IsValid)
            {
                PFCategory data = Mapper.Map<PFCategory>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = pFCategoryRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    pFCategoryRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        //public ActionResult Delete(int id)
        //{
        //    pFCategoryRepository.Delete(id);
        //    ShowMessage(true, "刪除成功");
        //    return RedirectToAction("Index");
        //}

        public JsonResult GetCls(int id)
        {
            var query = pFCategoryRepository.GetById(id);
            if (id != 0 && query != null)
            {
                var cateList = pFCategoryRepository.Query(null, query.DeptId, query.ID, CATE);
                if (cateList.Count() < 1)
                {
                    var cates = EnumHelper.GetValues<PF_Category>();
                    foreach (var item in cates)
                    {
                        pFCategoryRepository.Insert(new PFCategory() { 
                            Title = EnumHelper.GetDescription(item),
                            DeptId = query.DeptId,
                            ParentID = query.ID,
                            LevelType = CATE,
                            Points = 0,
                            Sort = (int)item,
                            IsRequired = true,
                            IsOnline = true,
                        });
                    }
                }
                var result = Mapper.Map<PFCategoryClsView>(query);
                result.CateList = Mapper.Map<List<PFCategoryCateView>>(pFCategoryRepository.Query(null, query.DeptId, query.ID, CATE).OrderBy(q => q.Type).ThenBy(q => q.Sort));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(new PFCategoryClsView(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCate(int id)
        {
            var query = pFCategoryRepository.GetById(id);
            if (id != 0 && query != null)
            {
                var result = Mapper.Map<PFCategoryCateView>(query);
                result.ItmList = Mapper.Map<List<PFCategoryItmView>>(pFCategoryRepository.Query(null, query.DeptId, query.ID, ITM).OrderBy(q => q.Type).ThenBy(q => q.Sort));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(new PFCategoryCateView(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetItm(int id)
        {
            var query = pFCategoryRepository.GetById(id);
            if (id != 0 && query != null)
            {
                var lvlList = pFCategoryRepository.Query(null, query.DeptId, query.ID, LVL);
                if (lvlList.Count() < 1)
                {
                    pFCategoryRepository.Insert(new PFCategory()
                    {
                        Title = "無",
                        DeptId = query.DeptId,
                        ParentID = query.ID,
                        LevelType = LVL,
                        Points = 0,
                        Sort = 0,
                        Type = query.Type,
                        IsRequired = false,
                        IsOnline = true,
                    });
                }

                var result = Mapper.Map<PFCategoryItmView>(query);
                result.LvlList = Mapper.Map<List<PFCategoryLvlView>>(pFCategoryRepository.Query(null, query.DeptId, query.ID, LVL).OrderBy(q => q.Type).ThenBy(q => q.Sort));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(new PFCategoryItmView(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLvl(int id)
        {
            var query = pFCategoryRepository.GetById(id);
            if (id != 0 && query != null)
            {            
                var result = Mapper.Map<PFCategoryLvlView>(query);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return Json(new PFCategoryLvlView(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveCls(int id, string title, string thaititle, string deptId, string leveltype, int parentID, int points, int sort, bool isrequired, bool isonline)
        {
            try
            {
                id = SaveCategoryData(id, title, thaititle, deptId, CLS, parentID, points, sort, 0, isrequired, isonline);
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }
            var result = Mapper.Map<PFCategoryClsView>(pFCategoryRepository.GetById(id));
            result.CateList = Mapper.Map<List<PFCategoryCateView>>(pFCategoryRepository.Query(null, result.DeptId, result.ID, CATE));

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveCate(int id, string title, string thaititle, string deptId, string leveltype, int parentID, int points, int sort, bool isrequired, bool isonline)
        {
            try
            {
                id = SaveCategoryData(id, title, thaititle, deptId, CATE, parentID, points, sort, 0, isrequired, isonline);
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }
            var result = Mapper.Map<PFCategoryCateView>(pFCategoryRepository.GetById(id));
            result.ItmList = Mapper.Map<List<PFCategoryItmView>>(pFCategoryRepository.Query(null, result.DeptId, result.ID, ITM));

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveItm(int id, string title, string thaititle, string deptId, string leveltype, int parentID, int points, int sort, int type, bool isrequired, bool isonline)
        {
            try
            {
                id = SaveCategoryData(id, title, thaititle, deptId, ITM, parentID, points, sort, type, isrequired, isonline);
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }
            var result = Mapper.Map<PFCategoryItmView>(pFCategoryRepository.GetById(id));
            result.LvlList = Mapper.Map<List<PFCategoryLvlView>>(pFCategoryRepository.Query(null, result.DeptId, result.ID, LVL));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveLvl(int id, string title, string thaititle, string deptId, string leveltype, int parentID, int points, int sort, int type, bool isrequired, bool isonline)
        {
            try
            {
                id = SaveCategoryData(id, title, thaititle, deptId, LVL, parentID, points, sort, type, isrequired, isonline);
            }
            catch (Exception e)
            {
                ShowMessage(false, e.Message);
            }
            var result = Mapper.Map<PFCategoryLvlView>(pFCategoryRepository.GetById(id));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private int SaveCategoryData(int id, string title, string thaititle, string deptId, string levelType, int parentID, int points, int sort, int type, bool isrequired, bool isonline)
        {
            var data = new PFCategory();
            if (id != 0)
            {
                data = pFCategoryRepository.GetById(id);
            }
            data.Title = title;
            data.ThaiTitle = thaititle;
            data.DeptId = deptId;
            data.LevelType = levelType;
            data.ParentID = parentID;
            data.Points = points;
            data.Sort = sort;
            data.Type = type;
            data.IsRequired = isrequired;
            data.IsOnline = isonline;
            if (id != 0)
            {
                pFCategoryRepository.Update(data);
            }
            else
            {
                id = pFCategoryRepository.Insert(data);
            }

            return id;
        }
    }
}