﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Audit;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class AuditAdminController : BaseAdminController
    {
        // GET: Admin/AuditAdmin
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
        private PersonKPIRepository personKPIRepository = new PersonKPIRepository(new MeetingSignInDBEntities());
        private PFAppraiseRepository pFAppraiseRepository = new PFAppraiseRepository(new MeetingSignInDBEntities());
        private PFAppraiseWorksRepository pFAppraiseWorksRepository = new PFAppraiseWorksRepository(new MeetingSignInDBEntities());

        // GET: Admin/AuditAdmin
        public ActionResult Index(AuditIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            switch ((AdminType)adminInfo.Type)
            {
                case AdminType.Employee:
                    return RedirectToAction("Participate", "TrainingAdmin", new { PersonID  = adminInfo.Account});
                //case AdminType.ClassSupervisor:
                //    return RedirectToAction("Participate", "TrainingAdmin", new { PersonID  = adminInfo.Account});
                //case AdminType.TeamLeader:
                //    return RedirectToAction("Participate", "TrainingAdmin", new { PersonID  = adminInfo.Account});              
            }
            
            int officerId = model.IsOfficer ? adminInfo.ID : 0;
            
            var query = auditRepository.Query(model.IsInvalid, model.TargetID, model.Type, model.State, model.TrainingTitle, model.PersonID, model.TrainingID, officerId, model.PFState, true, modelState);
            if (!model.IsOfficer)
            {
                query = query.Where(q => q.Officer != adminInfo.ID);
            }

            #region 負責(審核)部門
            List<string> auditDepts = new List<string>();
            if (adminInfo.Type == (int)AdminType.Manager)
            {//主管審核
                auditDepts = auditLevelRepository.Query(true, "", "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.Type == (int)AdminType.Vice)
            {//副總審核
                if (adminInfo.IsPrincipal)
                {
                    if (model.State == 2)
                    {
                        auditDepts = auditLevelRepository.Query(true, "", "", adminInfo.Account, "").Select(q => q.DeptId).ToList();
                    }
                    else if (model.State == 5)
                    {
                        auditDepts = auditLevelRepository.Query(true, adminInfo.Account, "", "", "").Select(q => q.DeptId).ToList();
                    }
                    //auditDepts = auditLevelRepository.Query(true, adminInfo.Account, "", "", "").Select(q => q.DeptId).ToList();
                }
                else
                {
                    auditDepts = auditLevelRepository.Query(true, "", "", adminInfo.Account, "").Select(q => q.DeptId).ToList();
                }                
            }
            else if (adminInfo.Type == (int)AdminType.President)
            {//總經理審核
                auditDepts = auditLevelRepository.Query(true, "", adminInfo.Account, "", "").Select(q => q.DeptId).ToList();
            }
            //else if (adminInfo.Type == (int)AdminType.Principal)
            //else if (adminInfo.IsPrincipal)
            //{//校長審核
            //    auditDepts = auditLevelRepository.Query(true, adminInfo.Account, "", "", "").Select(q => q.DeptId).ToList();
            //}
            else if (adminInfo.Type == (int)AdminType.ClassSupervisor)
            {//課級主管
                auditDepts = auditLevelRepository.Query(true, "", "", "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.Type == (int)AdminType.TeamLeader)
            {//班組長
                auditDepts = auditLevelRepository.Query(true, "", "", "", "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.Type == (int)AdminType.HR)
            {//人資
                auditDepts = auditLevelRepository.Query(true, "", "", "", "", "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
            }
            #endregion

            //取得負責審核部門人員工號
            var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "", "", false, true, true).Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
            var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account)).Select(q => q.ID).ToList();
            //if (adminInfo.Type == (int)AdminType.President || adminInfo.Type == (int)AdminType.Principal)
            if (adminInfo.Type == (int)AdminType.President || adminInfo.IsPrincipal)
            {
                query = query.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminIdList.Contains(q.Officer) || q.Officer == 1);
            }
            else
            {
                query = query.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminIdList.Contains(q.Officer));
            }

            var pageResult = query.ToPageResult<Audit>(model);
            model.PageResult = Mapper.Map<PageResult<AuditItemView>>(pageResult);
            var tempList = model.PageResult.Data.ToList();
            foreach (var item in tempList.ToList())
            {
                item.TrainingTitle = "";
                if (item.Type == (int)AuditType.PersonKPI)
                {
                    var personKpiInfo = personKPIRepository.GetById(item.TargetID);
                    if (personKpiInfo == null)
                    {
                        item.TrainingTitle = "";
                    }
                    else
                    {
                        var kpiInfo = kPIRepository.GetById(personKpiInfo.KPIID);
                        if (kpiInfo == null)
                        {
                            item.TrainingTitle = "";
                        }
                        else
                        {
                            var fairlyKpiInfo = kPIRepository.GetById(kpiInfo.ParentKPIID);
                            item.TrainingTitle = $"{fairlyKpiInfo.Title} >> {kpiInfo.Title}";
                        }
                    }
                  
                }
                else if (item.Type == (int)AuditType.PF)
                {
                    var appraiseData = pFAppraiseRepository.GetById(item.TargetID);
                    if (appraiseData != null)
                    {
                        item.TrainingTitle = appraiseData.SeasonTitle;
                    }
                    var appraiseWorksData = pFAppraiseWorksRepository.GetFirstData(appraiseData.ID);
                    if (appraiseWorksData != null)
                    {
                        item.TrainingTitle += " - " + appraiseWorksData.ClassTitle;
                    }
                }
                else
                {
                    Training trainInfo = trainingRepository.GetById(item.TrainingID);
                    try
                    {
                        if (trainInfo == null)
                        {
                            auditRepository.Delete(item.ID);
                            continue;
                        }
                        item.TrainingID = trainInfo.ID;
                        var officer = adminRepository.GetById(item.Officer);
                        string officerStr = officer == null || string.IsNullOrEmpty(officer.Name) ? officer.Account : officer.Name;
                        item.TrainingTitle = " 【承辦人 : " + officerStr + "】" + trainInfo.Title;
                        //item.TrainingTitle += trainInfo.Title;                    
                    }
                    catch (Exception ex)
                    {
                        item.TrainingTitle = "找不到標題";
                    }

                }

                if (item.Type > (int)AuditType.Internal)
                {
                    var personInfo = t8personRepository.FindByPersonId(item.PersonID);
                    if (personInfo != null)
                    {
                        item.PersonName = "【" + personInfo.DepartmentStr + "】";
                        item.PersonName += personInfo.Name;
                    }
                }
                else
                {
                    var officerAdminInfo = adminRepository.GetById(item.Officer);
                    var officerPersonInfo = t8personRepository.FindByPersonId(officerAdminInfo.Account);
                    item.OfficerName = officerPersonInfo == null ? officerAdminInfo.Name : officerPersonInfo.Name;
                }

                switch ((AuditType)item.Type)
                {
                    case AuditType.External:
                        ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());
                        var epData = externalPlanRepository.GetById(item.TargetID);
                        if (epData == null || epData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.Internal:
                        InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());
                        var ipData = internalPlanRepository.GetById(item.TargetID);
                        if (ipData == null || ipData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.ExternalAcceptance:
                        ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());
                        var eaData = externalAcceptanceRepository.GetById(item.TargetID);
                        if (eaData == null || eaData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.InternalAcceptance:
                        InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
                        var iaData = internalAcceptanceRepository.GetById(item.TargetID);
                        if (iaData == null || iaData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.Seminar:
                        SeminarPlanRepository seminarPlanRepository = new SeminarPlanRepository(new MeetingSignInDBEntities());
                        var spData = seminarPlanRepository.GetById(item.TargetID);
                        if (spData == null || spData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.PersonKPI:
                        PersonKPIRepository personKPIRepository = new PersonKPIRepository(new MeetingSignInDBEntities());
                        var pkData = personKPIRepository.GetById(item.TargetID);
                        if (pkData == null || pkData.PresidentAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.PF:
                        PFAppraiseRepository pFAppraiseRepository = new PFAppraiseRepository(new MeetingSignInDBEntities());
                        var paData = pFAppraiseRepository.GetById(item.TargetID);
                        if (paData == null || paData.ManagerAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                }
            }
            model.PageResult.Data = tempList.AsEnumerable();

            return View(model);
        }
        
        public ActionResult Add(int id, int type)
        {
            if (id == 0 || type == 0)
            {
                return RedirectToAction("Index", "AuditIndex");
            }
            //新增待審資料
            var data = new Audit() {
                TargetID = id,
                Type = type,
                State = (int)AuditState.OfficerAudit,
                IsInvalid = false,
                Officer = 1,
                CreateDate= DateTime.Now
            };
            auditRepository.Insert(data);

            return RedirectToAction("Index", "AuditIndex");
        }
    }
}