﻿using AutoMapper;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Areas.Admin.ViewModels.Meeting;
using MeetSignIn.Areas.Admin.ViewModels.Room;
using MeetSignIn.Models;
using MeetSignIn.Models.Calendar;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class MeetingAdminController : BaseAdminController
    {
        private MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        //private EmployeeRepository employeeRepository = new EmployeeRepository(new MeetingSignInDBEntities());
        private SignInRepository signInRepository = new SignInRepository(new MeetingSignInDBEntities());
        private Repositories.DepartmentRepository departmentRepository = new Repositories.DepartmentRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public ActionResult Index(MeetingIndexView model)
        {
            var query = meetingRepository.Query(model.Title, model.StartDate, model.EndDate, model.Department, model.FactoryID, model.RoomID);
            var pageResult = query.ToPageResult<Meeting>(model);
            model.PageResult = Mapper.Map<PageResult<MeetingView>>(pageResult);
            var dataList = new List<MeetingView>(model.PageResult.Data);
            foreach (var item in dataList.ToArray())
            {
                var temp = signInRepository.GetSignInInfoList(item.ID).ToList();
                var signInInfo = temp.Select(q => q.DepartmentTitle).Distinct().ToList();
                item.DepartmentListStr =  signInInfo.Aggregate((a, b) => a + ", " + b);
                if (model.Department != 0)
                {
                    var departmentTitle = departmentRepository.FindBy(model.Department).Title;
                    if (!item.DepartmentListStr.Contains(departmentTitle))
                    {
                        dataList.Remove(item);
                    }
                }
            }
            model.PageResult.Data = dataList.AsEnumerable();

            return View(model);
        }

        public ActionResult Copy(int id)
        {
            try
            {
                var model = new MeetingView();
                var query = meetingRepository.FindBy(id);
                model = Mapper.Map<MeetingView>(query);
                if (model.StartDate < Convert.ToDateTime("2010/01/01") || model.EndDate < Convert.ToDateTime("2010/01/01"))
                {
                    SetDefaultDate(model);
                }

                #region 建立新會議
                string Add14Str = DateTime.Now.AddDays(14).ToString("yyyy/MM/dd");
                var nModel = Mapper.Map<Meeting>(model);
                nModel.ID = 0;
                nModel.Title += "---複製";
                nModel.FileOne = "";
                nModel.FileTwo = "";
                nModel.Record = "";
                nModel.SignDate = Convert.ToDateTime(Add14Str + nModel.SignDate.ToString(" HH:mm"));
                nModel.StartDate = Convert.ToDateTime(Add14Str + nModel.StartDate.ToString(" HH:mm"));
                nModel.EndDate = Convert.ToDateTime(Add14Str + nModel.EndDate.ToString(" HH:mm"));
                nModel.IsSignable = false;
                nModel.CreateDate = DateTime.Now;

                int nId = meetingRepository.Insert(nModel);
                #endregion

                var notifyList = signInRepository.Query(null, "", id).Select(q => q.JobID).ToList();
                var employeeInfoList = personRepository.GetPersonQuery().Where(q => notifyList.Contains(q.JobID)).ToList();
                //model.Notify = Mapper.Map<List<EmployeeView>>(employeeInfoList);

                #region 新增會議名單
                //新增名單                   
                foreach (var item in employeeInfoList)
                {
                    string engName = string.IsNullOrEmpty(item.EngName) ? "" : " " + item.EngName;
                    var result = signInRepository.Insert(new SignIn()
                    {
                        SignCode = Convert.ToString(Guid.NewGuid()),
                        MeetingID = nId,
                        JobID = item.JobID,
                        Name = item.Name + engName,
                        IsSign = false,
                        SignStatus = (int)SignStatus.UnSigned,
                        IsSent = false,
                    });
                }
                #endregion

                return RedirectToAction("Edit", new { id = nId });
            }
            catch (Exception ex)
            {

                throw;
            }
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new MeetingView();
            if (id != 0)
            {
                var query = meetingRepository.FindBy(id);
                model = Mapper.Map<MeetingView>(query);
                if (model.StartDate < Convert.ToDateTime("2010/01/01") || model.EndDate < Convert.ToDateTime("2010/01/01"))
                {
                    SetDefaultDate(model);
                }

                var notifyList = signInRepository.Query(null, "", id).Select(q => q.JobID).ToList();
                var employeeInfo = personRepository.GetPersonQuery().Where(q => notifyList.Contains(q.JobID)).ToList();
                model.Notify = Mapper.Map<List<EmployeeView>>(employeeInfo);
                //if (model.Notify.Count() > 0)
                //{
                //    foreach (var item in model.Notify)
                //    {
                //        item.DepartmentStr = departmentRepository.FindBy(item.DepartmentID).Title;
                //    }
                //}               
            }
            else
            {
                SetDefaultDate(model);
                model.IsSignable = false;
            }
            //model.Department = 0;

            return View(model);
        }

        private static void SetDefaultDate(MeetingView model)
        {
            model.SignDate = DateTime.Now.AddHours(1.5);
            model.StartDate = DateTime.Now.AddHours(2);
            model.EndDate = DateTime.Now.AddHours(3);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(MeetingView model, HttpPostedFileBase fileone, HttpPostedFileBase filetwo)
        {
            if (ModelState.IsValid)
            {
                Meeting data = Mapper.Map<Meeting>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;
                data.Updater = adminId;
                bool isHR = adminInfo.DeptId == "B40";

                #region 檔案處理
                bool hasFile = ImageHelper.CheckFileExists(fileone);
                bool hasFile2 = ImageHelper.CheckFileExists(filetwo);
                if (hasFile)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                    data.FileOne = ImageHelper.SaveFile(fileone, PhotoFolder);
                }
                if (hasFile2)
                {
                    ImageHelper.DeleteFile(PhotoFolder, model.FileTwo);
                    data.FileTwo = ImageHelper.SaveFile(filetwo, PhotoFolder);
                }
                #endregion

                if (model.ID == 0)
                {
                    data.Creater = adminId;
                    data.MeetingNumber = data.StartDate.ToString("yyyyMmddHHmm");
                    model.ID = meetingRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    if (adminId != meetingRepository.FindBy(model.ID).Creater && adminInfo.Type != (int)AdminType.Sysadmin && !isHR)
                    {
                        ShowMessage(false, "儲存失敗，無修改權限");
                        return View(model);
                    }
                    meetingRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                System.IO.Directory.CreateDirectory(Server.MapPath("/FileUploads/Qrcode") + "/" + model.ID);

                #region 新增會議名單
                //判斷會議存在
                if (model.ID != 0)
                {
                    //新名單
                    var employeeInfoList = personRepository.GetSelected(model.selectedNotifyList);
                    var newList = employeeInfoList.OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                    var newJoinedId = newList.Count() > 0 ? newList.Aggregate((a, b) => a + ", " + b) : "";
                    //原名單
                    var oldList = signInRepository.Query(null, "", model.ID).OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                    var oldJoinedId = oldList.Count() > 0 ? oldList.Aggregate((a, b) => a + ", " + b) : "";
                    bool isDiff = (oldJoinedId != newJoinedId && newList.Count() > 0);
                    if (isDiff)
                    {
                        try
                        {
                            //刪除原名單
                            var deletList = signInRepository.Query(null, "", model.ID).Where(q => !newList.Contains(q.JobID)).Select(q => q.ID);
                            foreach (var item in deletList)
                            {
                                signInRepository = new SignInRepository(new MeetingSignInDBEntities());
                                signInRepository.Delete(item);
                            }

                            //新增名單                   
                            var failedList = new List<string>();
                            foreach (var item in employeeInfoList.Where(q => !oldList.Contains(q.JobID)))
                            {
                                string engName = string.IsNullOrEmpty(item.EngName) ? "" : " " + item.EngName;
                                var result = signInRepository.Insert(new SignIn()
                                {
                                    SignCode = Convert.ToString(Guid.NewGuid()),
                                    MeetingID = model.ID,
                                    JobID = item.JobID,
                                    Name = item.Name + engName,
                                    IsSign = false,
                                    SignStatus = (int)SignStatus.UnSigned,
                                    IsSent = false,
                                });
                                if (result == 0)
                                {
                                    failedList.Add(item.JobID);
                                }
                            }
                        }
                        catch (System.Exception e)
                        {

                            throw;
                        }
                    }                  
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            DeleteImage(id, 1);
            DeleteImage(id, 2);

            meetingRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult DeleteImage(int id, int type)
        {
            string file = meetingRepository.DeleteFile(id, type);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult SendNotify(int mid)
        {
            #region Gmail 發信
            //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port
            //System.Net.Mail.SmtpClient MySmtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);

            //設定你的帳號密碼
            //MySmtp.Credentials = new System.Net.NetworkCredential("a842695137@gmail.com", "judnmgpwvfdvmsyk");

            //Gmial 的 smtp 使用 SSL
            //MySmtp.EnableSsl = true;
            #endregion

            try
            {
                var meetingInfo = Mapper.Map<MeetingView>(meetingRepository.FindBy(mid));
                if (meetingInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無會議資料");
                }
                var roomInfo = roomRepository.FindBy(meetingInfo.RoomID);
                var notifyList = signInRepository.Query(null, "", mid);
                var notifyJidList = notifyList.Select(q => q.JobID).ToList();
                var reciverList = Mapper.Map<List<EmployeeView>>(personRepository.GetPersonQuery().Where(q => notifyJidList.Contains(q.JobID)).ToList());
                string errMsg = "";
                foreach (var item in reciverList)
                {
                    try
                    {
                        var thisSignIn = notifyList.Where(q => q.JobID == item.JobID).FirstOrDefault();

                        //生成Qrcode圖片
                        string host = $"{Request.Url.Scheme}://{Request.Url.Authority}";
                        string fileName = $"/{meetingInfo.ID}_{item.JobID}.png";
                        //string qrcodeUrl = $"{host}/Sign/MailSignIn?mid={meetingInfo.ID}&jid={item.JobID}&rn={roomInfo.RoomNumber}";
                        string qrcodeUrl = thisSignIn.SignCode;
                        string savePath = Server.MapPath("/FileUploads/Qrcode") + $"/{meetingInfo.ID}/{fileName}";
                        QRCodeHelper.GeneratorQrCodeImage(qrcodeUrl, savePath);

                        //寄送Email
                        //string from = "a842695137@gmail.com";
                        //string to = "seelen12@fairlybike.com";
                        string to = item.Email;
                        string subject = "";
                        string mailBody = "";
                        mailBody = $"<h3>【{meetingInfo.Title}】 {meetingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{meetingInfo.RoomStr}</h3>" +
                                                    $"<p>發起人 : {meetingInfo.Organiser}</p>" +
                                                    $"<p>被通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                        string signUrl = $"{Request.Url.Scheme}://220.228.58.43:18151/Sign/Check?rn={meetingInfo.RoomNum}&mid={meetingInfo.ID}";
                        mailBody += $"<p>預定開始時間 : {meetingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                                    $"<p>預定結束時間 : {meetingInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";
                        if (meetingInfo.FactoryID == 9)
                        {
                            mailBody += $"<p>簽到連結 : <a href=\"{signUrl}\">{signUrl}</a></p>";
                            if (!string.IsNullOrEmpty(meetingInfo.Url))
                            {
                                var urlArr = Regex.Split(meetingInfo.Url, "\r\n");
                                for (int i = 0; i < urlArr.Count(); i++)
                                {
                                    mailBody += $"<p>會議連結 第{i}場 : <a href=\"{urlArr[i]}\">{urlArr[i]}</a></p>";
                                }
                            }                         
                        }
                        if (!string.IsNullOrEmpty(meetingInfo.NotifyMsg))
                        {
                            mailBody += $"<p>{meetingInfo.NotifyMsg.Replace("\r\n", "<br>")}</p>";
                        }
                        if (!string.IsNullOrEmpty(meetingInfo.Note))
                        {
                            mailBody += $"<p>    注意事項 : </p>";
                            var urlArr = Regex.Split(meetingInfo.Note, "\r\n");
                            for (int i = 0; i < urlArr.Count(); i++)
                            {
                                mailBody += $"<p>    {urlArr[i].Replace("  ", "  ")}</p>";
                            }
                        }
                        //mailBody += $"<br><p>掃碼簽到</p>" +
                        //                            $"<br><span>* 需待會議〔開放簽到〕後始可簽到</span>" +
                        //                            $"<br><img src=\"{host}/FileUploads/Qrcode/{meetingInfo.ID}/{fileName}\" alt=\"QR Code\" />";
                        //mailBody += $"<br><a class=\"btn\" href=\"{qrcodeUrl}\">點擊簽到</a>";
                        subject = $"會議通知 - 系統信件";

                        var calendar = new Ical.Net.Calendar();

                        DateTime today = DateTime.Now;
                        calendar.Events.Add(new Ical.Net.CalendarComponents.CalendarEvent
                        {
                            Class = "PUBLIC",
                            Summary = subject,
                            Created = new CalDateTime(today),
                            //Description = $"【{meetingInfo.Title}】會議通知",
                            Description = $"【{meetingInfo.Title}】會議通知",
                            Start = new CalDateTime(meetingInfo.StartDate),
                            End = new CalDateTime(meetingInfo.EndDate),
                            Sequence = 0,
                            Uid = Guid.NewGuid().ToString(),
                            Location = meetingInfo.RoomStr,
                        });

                        var serializer = new CalendarSerializer(new SerializationContext());
                        var serializedCalendar = serializer.SerializeToString(calendar);
                        var bytesCalendar = Encoding.UTF8.GetBytes(serializedCalendar);
                        MemoryStream ms = new MemoryStream(bytesCalendar);

                        MailHelper.POP3CalendarMail(to, subject, mailBody, ms);
                        //if (System.IO.File.Exists(savePath))
                        //{
                        //    System.IO.File.Delete(savePath);
                        //}

                        //更新寄送狀態                    
                        thisSignIn.IsSent = true;
                        signInRepository.Update(thisSignIn);
                    }
                    catch (Exception e)
                    {
                        errMsg += $" {e.Message} | ";
                    }
                }

                ShowMessage(true, "通知發送成功 " + errMsg);
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "MeetingAdmin", new { id = mid });
        }

        /// <summary>
        /// 取得廠區會議室資料
        /// </summary>
        /// <param name="factory"></param>
        /// <returns></returns>
        public JsonResult GetRooms(int factory)
        {
            var query = roomRepository.Query(factory, "", true);        
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(query.ToList());
        }

        /// <summary>
        /// 取得課別員工資料
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        //public JsonResult GetEmployee(string deptId, int cadre)
        public JsonResult GetEmployee(int department, int cadre)
        {
            var deptId = departmentRepository.FindBy(department).DeptID;
            var query = personRepository.GetPersonQuery("", "", deptId, cadre == 1);        
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(query.ToList());
        }
        public string CheckRoomStatus(int room, DateTime start, DateTime end, int excmid)
        {
            string usedStr = "該會議室於此時段已被預約囉，請重新選擇時間或更換會議室，謝謝。";
            if (trainingRepository.Query("", start, end, "", "", 0, room, excmid, 0).Where(q => q.FactoryID != 9).Count() > 0)
            {
                return usedStr;
            }
            if (meetingRepository.Query("", start, end, 0, 0, room, excmid).Where(q => q.FactoryID != 9).Count() > 0)
            {
                return usedStr;
            }
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return "可選會議室";
        }

        /// <summary>
        /// CKEditor上傳圖片
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        //[HttpPost]
        //public JsonResult Upload(HttpPostedFileBase upload)
        //{
        //    string newFileName = ImageHelper.SaveFile(upload, PhotoFolder);

        //    string photoFolder = "MeetingCK";
        //    string savePath = string.Format("{0}/{1}/",
        //        FileUploads.Replace("~", ""),
        //        photoFolder
        //        );

        //    return Json(new { uploaded = 1, fileName = newFileName, url = savePath + newFileName });
        //}

    }
}