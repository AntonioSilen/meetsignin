﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.OptionGroup;
using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Areas.Admin.ViewModels.QNRResult;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class QNRAdminController : BaseAdminController
    {
        private QNRRepository qNRRepository = new QNRRepository(new MeetingSignInDBEntities());
        private QNRQuestionRepository qNRQuestionRepository = new QNRQuestionRepository(new MeetingSignInDBEntities());
        private QNRResultRepository qNRResultRepository = new QNRResultRepository(new MeetingSignInDBEntities());
        private OptionItemRepository optionItemRepository = new OptionItemRepository(new MeetingSignInDBEntities());

        public string YaHeiUILight = "Microsoft YaHei UI Light";

        public ActionResult Index(QNRIndexView model)
        {
            var query = qNRRepository.Query(model.IsOnline, model.Title, model.Description, model.Year, model.Months);
            var pageResult = query.ToPageResult<QNR>(model);
            model.PageResult = Mapper.Map<PageResult<QNRView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new QNRView();
            if (id != 0)
            {
                var query = qNRRepository.FindBy(id);
                model = Mapper.Map<QNRView>(query);
                model.QuestionList = Mapper.Map<List<QNRQuestionView>>(qNRQuestionRepository.Query(null, id));

                model.PersonResultList = new List<QNRPersonResultView>();
                var personIdList = qNRResultRepository.Query(id).Select(q => q.PersonId).Distinct().ToList();
                foreach (var personId in personIdList)
                {
                    var firstResult = qNRResultRepository.Query(id, 0, personId).FirstOrDefault();
                    if (firstResult != null)
                    {
                        var personInfo = t8personRepository.FindByPersonId(personId);
                        model.PersonResultList.Add(new QNRPersonResultView()
                        {
                            QNRID = id,
                            PersonId = personId,
                            PersonName = personInfo.Name,
                            DeptId = personInfo.DeptId,
                            DeptName = personInfo.DeptName,
                            CreateDate = firstResult.CreateDate,
                        });
                    }                 
                }
                
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(QNRView model)
        {
            if (ModelState.IsValid)
            {
                QNR data = Mapper.Map<QNR>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();

                if (model.ID == 0)
                {
                    data.CreatePersonId = adminInfo.Account;
                    model.ID = qNRRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    qNRRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult CopyQNR(int id)
        {
            try
            {
                var model = new QNRView();
                var query = qNRRepository.FindBy(id);
                model = Mapper.Map<QNRView>(query);

                #region 建立新問卷
                var data = Mapper.Map<QNR>(model);
                data.ID = 0;
                data.Title += "---複製";
                data.Description = query.Description;
                data.Depts = query.Depts;
                data.Year = query.Year;
                data.Month = query.Month;
                data.IsOnline = false;
                data.CreateDate = DateTime.Now;

                int nId = qNRRepository.Insert(data);
                #endregion

                var qnestionList = qNRQuestionRepository.Query(null, query.ID).ToList();

                #region 複製問題
                //新增名單                   
                foreach (var item in qnestionList)
                {
                    var result = qNRQuestionRepository.Insert(new QNRQuestion()
                    {
                        QNRID = nId,
                        Question = item.Question,
                        AnswerType = item.AnswerType,
                        OptionGroup = item.OptionGroup,
                        IsRequired = item.IsRequired,
                        Sort = item.Sort,
                    });
                }
                #endregion

                return RedirectToAction("Edit", new { id = nId });
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult Delete(int id)
        {   
            qNRRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public string SaveQuestion(int id, int qnrId, string question, int answertype, int sort, int optiongroup, bool isrequired)
        {
            try
            {
                if (id != 0)
                {
                    var query = qNRQuestionRepository.GetById(id);
                    query.QNRID = qnrId;
                    query.Question = question;
                    query.AnswerType = answertype;
                    query.Sort = sort;
                    query.OptionGroup = optiongroup;
                    query.IsRequired = isrequired;
                    qNRQuestionRepository.Update(query);
                }
                else
                {
                    qNRQuestionRepository.Insert(new QNRQuestion()
                    {
                        QNRID = qnrId,
                        Question = question,
                        AnswerType = answertype,
                        Sort = sort,
                        OptionGroup = optiongroup,
                        IsRequired = isrequired
                    });
                }

                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

        public JsonResult GetQuestion(int questionId)
        {
            var query = qNRQuestionRepository.GetById(questionId);

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQNRResult(string personId, int qnrid)
        {
            var queryList = Mapper.Map<List<QNRResultView>>(qNRResultRepository.Query(qnrid, 0, personId, "", 0, true)).ToList();
            foreach (var item in queryList)
            {
                var questionInfo = qNRQuestionRepository.GetById(item.QNRQuestionID);
                if (questionInfo != null)
                {
                    item.Question = questionInfo.Question;
                }
                else
                {
                    queryList.Remove(item);
                    continue;
                }

                if (item.AnswerType == 4)
                {
                    var optionItemInfo = optionItemRepository.GetById(Convert.ToInt32(item.Answer));
                    if (optionItemInfo != null)
                    {
                        item.Answer = optionItemInfo.Title;
                    }                    
                }
            }

            return Json(queryList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOptions(int groupId, int qId)
        {
            var query = Mapper.Map<List<OptionItemView>>(optionItemRepository.Query(true, groupId));
            foreach (var item in query)
            {                
                var linkqInfo = qNRQuestionRepository.FindBy(qId);
                if (linkqInfo == null)
                {
                    return Json(new List<OptionItemView>(), JsonRequestBehavior.AllowGet);
                }
                item.LinkQNRQID = linkqInfo.ID;
                item.LinkQNRQTitle = linkqInfo.Question;
                var qInfo = qNRQuestionRepository.FindLinkedQuestion(qId, item.ID);
                item.QNRQID = 0;
                item.QNRQTitle = "";
                if (qInfo != null)
                {
                    item.QNRQID = qInfo.ID;
                    item.QNRQTitle = qInfo.Question;
                }
            }
          
            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public string DeleteQuestion(int Id)
        {
            try
            {
                qNRQuestionRepository.Delete(Id);
                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

        #region 匯出Excel
        /// <summary>
        /// 匯出Excel (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportExcel(int id)
        {
            //取出要匯出Excel的資料
            var qnrInfo = qNRRepository.GetById(id);
            string qnrTitle = qnrInfo == null ? "" : qnrInfo.Title;

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                var fillColor = System.Drawing.Color.Salmon;
                int row = 1;    //列:橫的
                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                #region 回饋資料
                ExcelWorksheet opinionSheet = ep.Workbook.Worksheets.Add("意見表資料");
                fillColor = System.Drawing.Color.Salmon;
                row = 1;    //列:橫的
                col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                var qnrQList = qNRQuestionRepository.Query(null, id).OrderBy(q => q.Sort).ToList();
                row++;
                foreach (var qnrQ in qnrQList)
                {
                    opinionSheet.Cells[row++, col].Value = qnrQ.Question;
                    SetColumnStyle(col, opinionSheet);
                }

                var qnrQID = qnrQList.Select(q => q.ID).ToList();
                var qnrResultPersonIdList = qNRResultRepository.Query(id).Select(q => q.PersonId).Distinct().OrderBy(q => q).ToList();
                col = 2;
                foreach (var personId in qnrResultPersonIdList)
                {
                    row = 1;
                    var qnrResultList = qNRResultRepository.Query(id, 0, personId, "", 0, true);
                    var personInfo = t8personRepository.FindByPersonId(personId);
                    opinionSheet.Cells[row++, col].Value = personInfo.Name;
                    foreach (var result in qnrResultList)
                    {
                        opinionSheet.Column(col).AutoFit();
                        switch (result.AnswerType)
                        {
                            case 1://文字
                                opinionSheet.Cells[row++, col].Value = result.Answer;
                                break;
                            case 2://多行文字
                                opinionSheet.Cells[row++, col].Value = result.Answer;
                                break;
                            case 3://日期
                                opinionSheet.Cells[row++, col].Value = result.Answer;
                                break;
                            case 4://單選
                                var questionOptionGroup = qNRQuestionRepository.GetById(result.QNRQuestionID);
                                var optionItem = optionItemRepository.Query(true, questionOptionGroup.OptionGroup);
                                int resultAnswer = Convert.ToInt32(result.Answer);
                                string answer = optionItem == null ? "" : optionItem.Where(q => q.ID == resultAnswer).FirstOrDefault().Title;
                                opinionSheet.Cells[row++, col].Value = answer;
                                break;
                            case 5://複選                          
                                opinionSheet.Cells[row++, col].Value = result.Answer;
                                break;
                        }
                    }
                    col++;
                }
                       
                //if (qNRQList.Count() > 0)
                //{
                //    foreach (var item in qNRQList)
                //    {
                //        var personInfo = t8personRepository.FindByPersonId(item.PersonID);
                //        if (personInfo != null)
                //        {
                //            col = 1;
                //            opinionSheet.Column(col).AutoFit();
                //            opinionSheet.Cells[row, col++].Value = item.PersonID + " - " + personInfo.Name;
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Compatibility);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Architecture);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Schedule);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.CourseMaterials);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Device);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Professional);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.Expression);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((SatisfyRadio)item.TimeControl);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.TeachingMethod);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.Recommended);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.ActualUsage);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.CurrentUsage);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((AdmitRadio)item.FutureUsage);
                //            opinionSheet.Cells[row, col++].Value = EnumHelper.GetDescription((DifficultyRadio)item.Difficulty);
                //            row++;
                //        }
                //    }
                //}
                #endregion

                #region 匯出檔案
                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", qnrTitle + ".xlsx");
                #endregion
            }
            catch (Exception e)
            {

                throw;
            }

            //string path = "";
            //string fileName = "";
            ////string meetingTitle = trainingRepository.GetById(id).Title;
            //var query = trainingPersonRepository.GetSignInInfoList(id).OrderBy(q => q.DeptID).ToList();
            //var dtHelper = new DataTableHelper<TrainPersonJoinPersonAndDepartment>();
            //DataTable dt = dtHelper.ListToDataTable(query);
            //fileName = trainingTitle.Replace("/", "-").Trim() + "_名單.xlsx";
            //path = Server.MapPath("/FileUploads/TrainingPersonExcel/") + fileName;
            //ExcelHelper.TableToExcel(dt, path);
            //return File(path, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);

        }

        private void SetColumnStyle(int col, ExcelWorksheet sheet)
        {
            sheet.Column(col).Style.Font.Name = YaHeiUILight;
            sheet.Column(col).AutoFit();
        }
        #endregion

    }
}