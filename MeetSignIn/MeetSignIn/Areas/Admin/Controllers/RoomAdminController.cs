﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Room;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class RoomAdminController : BaseAdminController
    {
        private RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());

        public ActionResult Index(RoomIndexView model)
        {
            var query = roomRepository.Query(model.FactoryID, model.RoomNumber, model.Status);
            var pageResult = query.ToPageResult<Room>(model);
            model.PageResult = Mapper.Map<PageResult<RoomView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new RoomView();
            if (id != 0)
            {
                var query = roomRepository.FindBy(id);
                model = Mapper.Map<RoomView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(RoomView model)
        {
            if (ModelState.IsValid)
            {
                Room data = Mapper.Map<Room>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = roomRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    roomRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            roomRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}