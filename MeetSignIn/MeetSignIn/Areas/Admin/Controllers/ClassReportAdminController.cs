﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.ClassReport;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ClassReportAdminController : BaseAdminController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private ClassReportRepository classReportRepository = new ClassReportRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());

        // GET: Admin/ClassReportAdmin
        public ActionResult Index(ClassReportIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (model.TrainingID == 0)
            {
                if (adminInfo.Type >= (int)AdminType.ClassSupervisor)
                {
                    return RedirectToAction("Participate", "TrainingAdmin", new { PersonID = adminInfo.Account });
                }
                else
                {
                    return RedirectToAction("Index", "TrainingAdmin");
                }
            }
            var query = classReportRepository.Query(model.TrainingID, model.PersonID, model.Name);
            var pageResult = query.ToPageResult<ClassReport>(model);
            model.PageResult = Mapper.Map<PageResult<ClassReportView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0, int trainingId = 0, string personId = "", bool isNew = false, bool FromAudit = false)
        {
            if (trainingId == 0 || string.IsNullOrEmpty(personId))
            {
                return RedirectToAction("Index", "HomeAdmin");
            }
            var model = new ClassReportView();
            var query = classReportRepository.Query(trainingId, personId).OrderByDescending(q => q.ID).FirstOrDefault();
            if (query != null)
            {
                model = Mapper.Map<ClassReportView>(query);
                if (isNew)
                {
                    var newData = Mapper.Map<ClassReportView>(query);
                    var oldAudit = auditRepository.Query(false, query.ID, (int)AuditType.ClassReport, null, "", "", query.TrainingID).FirstOrDefault();
                    if (oldAudit != null)
                    {
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        newData.ID = 0;
                        newData.IsAudit = false;
                        newData.OfficerAudit = 1;
                        newData.OfficerRemark = "";
                        newData.OfficerSignature = "";
                        newData.ManagerAudit = 1;
                        newData.ManagerRemark = "";
                        newData.ManagerSignature = "";
                        newData.ViceAudit = 1;
                        newData.ViceRemark = "";
                        newData.ViceSignature = "";
                        newData.PrincipalAudit = 1;
                        newData.PrincipalRemark = "";
                        newData.PrincipalSignature = "";
                        newData.PresidentAudit = 1;
                        newData.PresidentRemark = "";
                        newData.PresidentSignature = "";
                        newData.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.ClassReport, query.TrainingID, query.PersonID);
                        return View(newData);
                    }
                }
            }
            else
            {
                model = new ClassReportView()
                {
                    PersonID = personId,
                    TrainingID = trainingId,
                    Name = t8personRepository.FindByPersonId(personId).Name
                };
            }
            model.FromAudit = FromAudit;           

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ClassReportView model)
        {
            if (ModelState.IsValid)
            {
                ClassReport data = Mapper.Map<ClassReport>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();
                int adminId = adminInfo.ID;

                #region 日期處理
                data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                data.PrincipalAuditDate = SetAuditDate(data.PrincipalAudit, data.PrincipalAuditDate);
                data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                #endregion

                #region 簽章
                //承辦人
                var officerInfo = adminRepository.GetById(model.TrainingInfo.Creater);
                data.OfficerSignature = officerInfo.Account + ".jpg";
                //主管
                var personInfo = t8personRepository.FindByPersonId(data.PersonID);
                var personDept = personInfo == null ? "0001" : personInfo.DeptID;
                var tempSameDept = t8personRepository.GetPersonQuery("", "", personDept).ToList();
                var deptPersonIdList = tempSameDept.Select(q => q.PersonId).ToList();
                var managerInfo = adminRepository.Query(true, (int)AdminType.Manager).Where(q => deptPersonIdList.Contains(q.Account)).FirstOrDefault();
                data.ManagerSignature = managerInfo == null ? "Fairly.jpg" : managerInfo.Account + ".jpg";
                //副總
                var vice = adminRepository.Query(true, (int)AdminType.Vice).FirstOrDefault();
                data.ViceSignature = vice == null ? "vice.jpg" : vice.Account + ".jpg";
                //校長
                //var principal = adminRepository.Query(true, (int)AdminType.Principal).FirstOrDefault();
                var principal = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                data.PrincipalSignature = principal == null ? "principal.jpg" : principal.Account + ".jpg";
                //總經理
                var president = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                data.PresidentSignature = president == null ? "president.jpg" : president.Account + ".jpg";
                #endregion

                #region 檔案處理
                //bool hasFile = ImageHelper.CheckFileExists(model.ManagerPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.ManagerSignature);
                //    data.ManagerSignature = ImageHelper.SaveFile(model.ManagerPost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.VicePost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.ViceSignature);
                //    data.ViceSignature = ImageHelper.SaveFile(model.VicePost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.OfficerPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.OfficerSignature);
                //    data.OfficerSignature = ImageHelper.SaveFile(model.OfficerPost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.PrincipalPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.PrincipalSignature);
                //    data.PrincipalSignature = ImageHelper.SaveFile(model.PrincipalPost, PhotoFolder);
                //}
                //hasFile = ImageHelper.CheckFileExists(model.PresidentPost);
                //if (hasFile)
                //{
                //    ImageHelper.DeleteFile(PhotoFolder, model.PresidentSignature);
                //    data.PresidentSignature = ImageHelper.SaveFile(model.PresidentPost, PhotoFolder);
                //}
                #endregion

                #region 簽核人員
                if (adminInfo.Type < (int)AdminType.ClassSupervisor && adminInfo.Type > (int)AdminType.Sysadmin)
                {
                    var trainingInfo = trainingRepository.GetById(data.TrainingID);
                    if (trainingInfo.Creater == adminInfo.ID)
                    {
                        switch (adminInfo.Type)
                        {
                            case (int)AdminType.Manager:
                                data.ManagerAudit = data.OfficerAudit;
                                data.ManagerAuditDate = data.OfficerAuditDate;
                                data.ManagerRemark = data.OfficerRemark;
                                data.ManagerSignature = data.OfficerSignature;
                                break;
                            case (int)AdminType.Vice:
                                data.ViceAudit = data.OfficerAudit;
                                data.ViceAuditDate = data.OfficerAuditDate;
                                data.ViceRemark = data.OfficerRemark;
                                data.ViceSignature = data.OfficerSignature;
                                break;
                            //case (int)AdminType.Principal:
                            //    data.PrincipalAudit = data.OfficerAudit;
                            //    data.PrincipalAuditDate = data.OfficerAuditDate;
                            //    data.PrincipalRemark = data.OfficerRemark;
                            //    data.PrincipalSignature = data.OfficerSignature;
                            //    break;
                            case (int)AdminType.President:
                                data.PresidentAudit = data.OfficerAudit;
                                data.PresidentAuditDate = data.OfficerAuditDate;
                                data.PresidentRemark = data.OfficerRemark;
                                data.PresidentSignature = data.OfficerSignature;
                                break;
                        }
                    }
                }
                #endregion

                if (model.ID == 0)
                {
                    model.ID = classReportRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    try
                    {
                        classReportRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                }

                #region 送審
                if (model.IsAudit)
                {
                    var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.ClassReport).FirstOrDefault();
                    var state = (int)AuditState.Processing;
                    if (data.OfficerAudit == (int)FormAudit.Approval)
                    {
                        state = (int)AuditState.OfficerAudit;
                    }
                    if (data.ManagerAudit == (int)FormAudit.Approval)
                    {
                        state = (int)AuditState.ManagerAudit;
                    }
                    if (data.ViceAudit == (int)FormAudit.Approval)
                    {
                        state = (int)AuditState.ViceAudit;
                    }
                    if (data.PrincipalAudit == (int)FormAudit.Approval)
                    {
                        state = (int)AuditState.PrincipalAudit;
                    }
                    if (data.PresidentAudit == (int)FormAudit.Approval)
                    {
                        state = (int)AuditState.PresidentAudit;
                    }
                    if (data.OfficerAudit == (int)FormAudit.Reject || data.ManagerAudit == (int)FormAudit.Reject || data.PrincipalAudit == (int)FormAudit.Reject || data.ViceAudit == (int)FormAudit.Reject || data.PresidentAudit == (int)FormAudit.Reject)
                    {
                        auditInfo.IsInvalid = true;
                    }
                    if (auditInfo == null)
                    {
                        var auditId = auditRepository.Insert(new Audit()
                        {
                            TrainingID = data.TrainingID,
                            PersonID = data.PersonID,
                            TargetID = data.ID,
                            Type = (int)AuditType.ClassReport,
                            State = state,
                            IsInvalid = false,
                            Officer = model.TrainingInfo.Creater,
                            CreateDate = DateTime.Now
                        });
                    }
                    else
                    {
                        auditInfo.State = state;
                        auditRepository.Update(auditInfo);
                    }
                    string reciver = GetAuditRevicer(state, model.TrainingInfo.Creater, (int)AuditType.ClassReport, model.PersonID);
                    if (!string.IsNullOrEmpty(reciver))
                    {
                        //SendAuditNotify(model.TrainingID, model.ID, (int)AuditType.ClassOpinion, reciver);
                    }
                }
                #endregion

                return RedirectToAction("Edit", new { trainingId = model.TrainingID, personId = model.PersonID, FromAudit = model.FromAudit });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            classReportRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        #region 審核階段通知

        #endregion
    }
}