﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.AuditLevel;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class AuditLevelAdminController : BaseAdminController
    {
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());

        public ActionResult Index(AuditLevelIndexView model)
        {
            var existDeptList = t8personRepository.GetDeptQuery();
            //var deptIdList = existDeptList.Select(q => q.DeptId).ToList();
            var query = auditLevelRepository.Query(model.IsOnline, model.PrincipalReview, model.PresidentReview, model.ViceReview, model.ManagerReview, model.CSReview, model.TLReview, model.HRReview, model.DeptName);
            foreach (var item in existDeptList)
            {
                if (auditLevelRepository.GetAll().Where(q => q.DeptId == item.DeptId).FirstOrDefault() == null)
                {
                    auditLevelRepository.Insert(new AuditLevel()
                    {
                        DeptId = item.DeptId,
                        DeptName = item.DeptName,
                        PrincipalReview = "0000",
                        PresidentReview = "0000",
                        ViceReview = "0000",
                        ManagerReview = "0000",
                        CSReview = "0000",
                        TLReview = "0000",
                        HRReview = "0000",
                        IsOnline = true,
                        UpdateDate = DateTime.Now,
                    });
                }
            }

            var pageResult = query.ToPageResult<AuditLevel>(model);
            model.PageResult = Mapper.Map<PageResult<AuditLevelView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {                
                if (item.PrincipalReview != "0000")
                {
                    var principalData = t8personRepository.FindByPersonId(item.PrincipalReview);
                    item.PrincipalReview = principalData == null ? "已離職/更換職位" : principalData.Name;
                }
                else
                {
                    item.PrincipalReview = "---";
                }

                if (item.PresidentReview != "0000")
                {
                    var presidentData = t8personRepository.FindByPersonId(item.PresidentReview);
                    item.PresidentReview = presidentData == null ? "已離職/更換職位" : presidentData.Name;
                }
                else
                {
                    item.PresidentReview = "---";
                }

                if (item.ViceReview != "0000")
                {
                    var viceData = t8personRepository.FindByPersonId(item.ViceReview);
                    item.ViceReview = viceData == null ? "已離職/更換職位" : viceData.Name;
                }
                else
                {
                    item.ViceReview = "---";
                }

                if (item.ManagerReview != "0000")
                {
                    var managerData = t8personRepository.FindByPersonId(item.ManagerReview);
                    item.ManagerReview = managerData == null ? "已離職/更換職位" : managerData.Name;
                }
                else
                {
                    item.ManagerReview = "---";
                }

                if (!string.IsNullOrEmpty(item.CSReview))
                {
                    var selectedCSIds = item.CSReview.Split(',');
                    string reviewStr = "";
                    foreach (var slt in selectedCSIds)
                    {
                        var csData = t8personRepository.FindByPersonId(slt.Trim());
                        if (csData != null)
                        {
                            reviewStr += csData.Name + " ";
                        }
                    }
                    item.CSReview = string.IsNullOrEmpty(reviewStr) ? "---" : reviewStr;
                }
                if (!string.IsNullOrEmpty(item.TLReview))
                {
                    var selectedTLIds = item.TLReview.Split(',');
                    string reviewStr = "";
                    foreach (var slt in selectedTLIds)
                    {
                        var tlData = t8personRepository.FindByPersonId(slt.Trim());
                        if (tlData != null)
                        {
                            reviewStr += tlData.Name + " ";
                        }
                    }
                    item.TLReview = string.IsNullOrEmpty(reviewStr) ? "---" : reviewStr;
                }

                //var csData = t8personRepository.FindByJobID(item.CSReview);
                //item.CSReview = csData == null ? "---" : csData.Name;
                //var tlData = t8personRepository.FindByJobID(item.TLReview);
                //item.TLReview = tlData == null ? "---" : tlData.Name;

                var hrData = t8personRepository.FindByPersonId(item.HRReview);
                item.HRReview = hrData == null ? "---" : hrData.Name;
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            var model = new AuditLevelView();

            var query = auditLevelRepository.FindBy(id);
            model = Mapper.Map<AuditLevelView>(query);
            model.SelectedCSIds = model.CSReview.Split(',');
            model.SelectedTLIds = model.TLReview.Split(',');

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(AuditLevelView model)
        {
            if (ModelState.IsValid)
            {
                AuditLevel data = Mapper.Map<AuditLevel>(model);
                data.CSReview = string.Join(",", model.SelectedCSIds.ToArray());
                data.TLReview = string.Join(",", model.SelectedTLIds.ToArray());
                if (model.ID == 0)
                {
                    model.ID = auditLevelRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    auditLevelRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }
    }
}