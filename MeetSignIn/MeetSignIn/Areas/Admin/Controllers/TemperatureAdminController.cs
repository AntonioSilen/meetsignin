﻿using AutoMapper;
//using IronXL;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Temperature;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class TemperatureAdminController : BaseAdminController
    {
        private TemperatureRepository temperatureRepository;
        private AdminRepository adminRepository;

        public TemperatureAdminController() : this(null, null, null) { }

        public TemperatureAdminController(TemperatureRepository repo, Repositories.T8Repositories.PersonRepository repo2, AdminRepository repo3)
        {
            temperatureRepository = repo ?? new TemperatureRepository(new MeetingSignInDBEntities());
            t8personRepository = repo2 ?? new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
            adminRepository = repo3 ?? new AdminRepository(new MeetingSignInDBEntities());
        }

        // GET: Admin/TemperatureAdmin
        public ActionResult Index(TemperatureIndexView model)
        {
            DateTime? searchDateStr = Convert.ToDateTime(model.SearchDate);
            var searchDate = searchDateStr > Convert.ToDateTime("2000/01/01") ? searchDateStr : null;
            var query = temperatureRepository.Query(model.PersonID, model.IsAbnormal, searchDate);
            var pageResult = query.ToPageResult<Temperature>(model);
            model.PageResult = Mapper.Map<PageResult<TemperatureView>>(pageResult);
            //foreach (var item in model.PageResult.Data)
            //{
            //    var personInfo = t8personRepository.FindByJobID(item.PersonID);
            //    if (personInfo == null)
            //    {
            //        var personAdminInfo = adminRepository.Query(null, 0, item.PersonID).FirstOrDefault();
            //        if (personAdminInfo != null)//驗證人員
            //        {

            //        }
            //    }
            //}

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new TemperatureView();
            if (id != 0)
            {
                var query = temperatureRepository.FindBy(id);
                model = Mapper.Map<TemperatureView>(query);
                model.TemperatureLogList = Mapper.Map<List<TemperatureView>>(temperatureRepository.Query(model.PersonID, null, null));
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(TemperatureView model)
        {
            if (ModelState.IsValid)
            {
                Temperature data = Mapper.Map<Temperature>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = temperatureRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    temperatureRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult LimitHours()
        {
            var model = new LimitHoursView();

            var limitH = GetLimitHours().Split(','); ;
            model.Start = Convert.ToInt32(limitH[0]);
            model.End = Convert.ToInt32(limitH[1]);


            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult LimitHours(LimitHoursView model)
        {
            if (ModelState.IsValid)
            {
                var result = WriteLimitHours(model.Start.ToString(), model.End.ToString());

                if (result)
                {
                    ShowMessage(true, "修改成功");

                    return RedirectToAction("LimitHours");
                }
            }

            return View(model);
        }

        public string GetLimitHours()
        {
            string result = "";
            foreach (string line in System.IO.File.ReadLines(Server.MapPath("/Setting_Data/limithours.txt")))
            {
                result += line + ",";
            }

            return result;
        }

        public bool WriteLimitHours(string start, string end)
        {
            try
            {
                StreamWriter sw = new StreamWriter(Server.MapPath("/Setting_Data/limithours.txt"));
                sw.WriteLine(start);
                sw.WriteLine(end);
                //Close the file
                sw.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool WriteLog(string path, string msg)
        {
            try
            {
                StreamWriter sw = new StreamWriter(path, true);
                sw.WriteLine(DateTime.Now.ToString());
                sw.WriteLine(msg);
                sw.WriteLine("---------------------------------------------------");
                //Close the file
                sw.Close();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public ActionResult ExportExcel(string personID, bool? isAbnormal, DateTime? searchDate)
        {
            try
            {
                var allGroupPersonQuery = t8personRepository.GetPersonQuery();
                //var allGroupPerson = Mapper.Map<List<TemperatureNotInView>>(allGroupPersonQuery);

                DateTime? searchDateStr = Convert.ToDateTime(searchDate);
                var searchDateTemp = searchDateStr > Convert.ToDateTime("2000/01/01") ? searchDateStr : null;
                var query = temperatureRepository.Query(personID, isAbnormal, searchDateTemp).ToList();
                var InList = Mapper.Map<List<TemperatureView>>(query);

                var InPersonIdList = InList.Select(q => q.PersonID).ToList();
                var notInList = allGroupPersonQuery.Where(q => !InPersonIdList.Contains(q.JobID)).ToList();
                var notInPersonIdList = notInList.Select(q => q.JobID).ToList();

                var otherList = adminRepository.Query(true).Where(q => q.Account.StartsWith("9"));
                //var otherInList = otherList.Where(q => !InPersonIdList.Contains(q.Account)).ToList();
                var otherNotInList = otherList.Where(q => !InPersonIdList.Contains(q.Account)).ToList();

                // Set Name and Path
                string persinIdStr = string.IsNullOrEmpty(personID) ? "" : $"【{personID}】";
                string dateStr = searchDateStr > Convert.ToDateTime("2000/01/01") ? $"〔{Convert.ToDateTime(searchDateStr).ToString("yyyy-MM-dd ")}〕" : "全部";
                string fileName = persinIdStr + dateStr + "體溫紀錄";
                string path = Server.MapPath("/FileUploads/TemperatureExcel/") + fileName + ".xlsx";

                try
                {
                    ExcelPackage ep = CreateSheet(Server.MapPath("/Setting_Data/log.txt"), InList, notInList, otherNotInList, fileName);

                    //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                    MemoryStream fileStream = new MemoryStream();
                    ep.SaveAs(fileStream);
                    ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                    fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤
                    return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName + ".xlsx");
                }
                catch (Exception e)
                {
                    return Json(new { status = "error", message = e.Message });
                }
            }
            catch (Exception e)
            {
                Response.Write(e.Message);
                return null;
            }
        }

        private static ExcelPackage CreateSheet(string logpath, List<TemperatureView> InList
            , List<GroupJoinPersonAndDepartment> notInList
            , List<Models.MainAdmin> otherNotInList
            , string fileName)
        {
            Repositories.T8Repositories.DepartmentRepository departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());

            TemperatureAdminController bac = new TemperatureAdminController();

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                                                                        //建立Excel
            ExcelPackage ep = new ExcelPackage();

            //建立第一個Sheet，後方為定義Sheet的名稱
            ExcelWorksheet sheet = ep.Workbook.Worksheets.Add(fileName);

            //欄:直的，因為要從第1欄開始，所以初始為1
            int col = 1;    

            //第1列是標題列 
            //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
            //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
            //取得做法可參考最後的參考連結。

            sheet.Cells[1, col++].Value = "工號";
            sheet.Cells[1, col++].Value = "姓名";
            sheet.Cells[1, col++].Value = "課別";
            sheet.Cells[1, col++].Value = "體溫";
            sheet.Cells[1, col++].Value = "身體異常";
            sheet.Cells[1, col++].Value = "症狀情況";
            sheet.Cells[1, col++].Value = "接觸史";
            sheet.Cells[1, col++].Value = "建立時間";

            //資料從第2列開始
            int row = 2;    //列:橫的
            try
            {
                foreach (var item in InList.OrderBy(q => q.PersonInfo.DeptID))
                {
                    try
                    {
                        string AbnormalDescriptionStr = "";
                        if (!string.IsNullOrEmpty(item.AbnormalDescriptionStr))
                        {
                            var AbnormalDescriptionArr = item.AbnormalDescriptionStr.Split(',');
                            foreach (var ad in AbnormalDescriptionArr)
                            {
                                if (!string.IsNullOrEmpty(ad))
                                {
                                    AbnormalDescriptionStr += item.AbnormalOptions.Where(q => q.Value == ad).FirstOrDefault().Text + ",";
                                }
                            }
                        }

                        string personName = item.PersonInfo != null ? item.PersonInfo.Name + " " + item.PersonInfo.EngName : "";
                        string personDept = item.PersonInfo != null ? item.PersonInfo.DepartmentStr : "";

                        col = 1;                               
                        sheet.Cells[row, col++].Value = item.PersonID;
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = personName;
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = personDept;
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = item.BodyTemperature + " °C"; ;
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = item.IsAbnormal ? "異常" : "正常";
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = AbnormalDescriptionStr;
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = item.ContactHistory;
                        sheet.Column(col).AutoFit();
                        sheet.Cells[row, col++].Value = item.CreateDate.ToString("yyyy-MM-dd");
                        sheet.Column(col).AutoFit();
                        row++;
                    }
                    catch (Exception e)
                    {
                        bac.WriteLog(logpath, "in item : " + item.PersonID + "failed");
                        ExcelPackage epErr = new ExcelPackage();
                        ExcelWorksheet sheetErr = epErr.Workbook.Worksheets.Add("InListrror" + item.PersonID);
                        col = 1; 
                        sheet.Cells[1, col++].Value = "Message";
                        sheet.Cells[1, col++].Value = $"InList : {item.PersonID} | {e.Message}";

                        return epErr;
                    }
                }

            }
            catch (Exception e)
            {
                bac.WriteLog(logpath, "in list failed");
                bac.WriteLog(logpath, e.Message);
                ExcelPackage epErr = new ExcelPackage();
                ExcelWorksheet sheetErr = epErr.Workbook.Worksheets.Add("InList");
                col = 1;
                sheet.Cells[1, col++].Value = "Message";
                sheet.Cells[1, col++].Value = $"InList : Error";

                return epErr;
            }

            ExcelWorksheet sheet2 = ep.Workbook.Worksheets.Add("未提交名單");
            col = 1;
            sheet2.Cells[1, col++].Value = "工號";
            sheet2.Cells[1, col++].Value = "姓名";
            sheet2.Cells[1, col++].Value = "課別";
            sheet2.Cells[1, col++].Value = "電話";
            sheet2.Cells[1, col++].Value = "信箱";

            //資料從第2列開始
            row = 2;    //列:橫的
            try
            {
                foreach (var item in notInList.OrderBy(q => q.DeptID))
                {                    
                    try
                    {
                        col = 1;//每換一列，欄位要從1開始
                                //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                        sheet2.Cells[row, col++].Value = item.JobID;
                        sheet2.Column(col).AutoFit();
                        sheet2.Cells[row, col++].Value = item.Name + item.EngName;
                        sheet2.Column(col).AutoFit();
                        sheet2.Cells[row, col++].Value = item.DepartmentStr;
                        sheet2.Column(col).AutoFit();
                        sheet2.Cells[row, col++].Value = item.Phone;
                        sheet2.Column(col).AutoFit();
                        sheet2.Cells[row, col++].Value = item.Email;
                        sheet2.Column(col).AutoFit();
                        row++;
                    }
                    catch (Exception e)
                    {
                        bac.WriteLog(logpath, "not in item : " + item.JobID + "failed");
                        ExcelPackage epErr = new ExcelPackage();
                        ExcelWorksheet sheetErr = epErr.Workbook.Worksheets.Add("notInList" + item.JobID);
                        col = 1;
                        sheet.Cells[1, col++].Value = "Message";
                        sheet.Cells[1, col++].Value = $"notInList : {item.JobID} | {e.Message}";

                        return epErr;
                    }
                }
                try
                {
                    foreach (var item in otherNotInList.OrderBy(q => q.Department))
                    {
                        try
                        {
                            col = 1;
                            sheet2.Cells[row, col++].Value = item.Account;
                            sheet2.Column(col).AutoFit();
                            sheet2.Cells[row, col++].Value = item.Name;
                            sheet2.Column(col).AutoFit();
                            sheet2.Cells[row, col++].Value = departmentRepository.FindByDeptId(item.Department).DeptName;
                            sheet2.Column(col).AutoFit();
                            sheet2.Cells[row, col++].Value = "";
                            sheet2.Column(col).AutoFit();
                            sheet2.Cells[row, col++].Value = "";
                            sheet2.Column(col).AutoFit();
                            row++;
                        }
                        catch (Exception)
                        {
                            bac.WriteLog(logpath, "other not in item : " + item.Account + "failed");                            
                        }
                    }
                }
                catch (Exception)
                {
                    bac.WriteLog(logpath, "other not in failed");
                }
            }
            catch (Exception e)
            {
                bac.WriteLog(logpath, "not in failed");
                ExcelPackage epErr = new ExcelPackage();
                ExcelWorksheet sheetErr = epErr.Workbook.Worksheets.Add("notInList");
                col = 1;
                sheet.Cells[1, col++].Value = "Message";
                sheet.Cells[1, col++].Value = $"notInList : Error";

                return epErr;
            }

            return ep;

        }


    }
}