﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.ReviewReport;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ReviewReportAdminController : BaseAdminController
    {
        private ReviewReportRepository reviewReportRepository = new ReviewReportRepository(new MeetingSignInDBEntities());

        public ActionResult Index(ReviewReportIndexView model)
        {
            var query = reviewReportRepository.Query(model.Title, model.Year, model.Month);
            var pageResult = query.ToPageResult<ReviewReport>(model);
            model.PageResult = Mapper.Map<PageResult<ReviewReportView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new ReviewReportView();
            if (id != 0)
            {
                var query = reviewReportRepository.FindBy(id);
                model = Mapper.Map<ReviewReportView>(query);
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ReviewReportView model)
        {
            if (ModelState.IsValid)
            {
                ReviewReport data = Mapper.Map<ReviewReport>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                #region 檔案處理
                bool hasReport = ImageHelper.CheckFileExists(model.FileNamePost);
                if (hasReport)
                {//報告
                    ImageHelper.DeleteFile(PhotoFolder, model.FileName);
                    data.FileName = ImageHelper.SaveFile(model.FileNamePost, PhotoFolder, model.FileNamePost.FileName.Split('.')[0]);
                }
                #endregion

                if (model.ID == 0)
                {
                    model.ID = reviewReportRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    reviewReportRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            reviewReportRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult DeleteReport(int id)
        {
            string file = reviewReportRepository.DeleteReport(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }
    }
}