﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Competency;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class CompetencyAdminController : BaseAdminController
    {
        private CompetencyRepository competencyRepository = new CompetencyRepository(new MeetingSignInDBEntities());

        public ActionResult Index(CompetencyIndexView model)
        {
            var query = competencyRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<Competency>(model);
            model.PageResult = Mapper.Map<PageResult<CompetencyView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new CompetencyView();
            if (id != 0)
            {
                var query = competencyRepository.FindBy(id);
                model = Mapper.Map<CompetencyView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(CompetencyView model)
        {
            if (ModelState.IsValid)
            {
                Competency data = Mapper.Map<Competency>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = competencyRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    competencyRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            competencyRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}