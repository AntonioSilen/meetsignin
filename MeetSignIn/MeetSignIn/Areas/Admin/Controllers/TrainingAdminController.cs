﻿using AutoMapper;
using Ical.Net.DataTypes;
using Ical.Net.Serialization;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels;
using MeetSignIn.Areas.Admin.ViewModels.Competency;
using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Areas.Admin.ViewModels.ExternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.InternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Areas.Admin.ViewModels.Vision;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Repositories.TrainingForm;
using MeetSignIn.Utility;
using MeetSignIn.Utility.Helpers;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using static MeetSignIn.Repositories.TrainingRepository;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class TrainingAdminController : BaseAdminController
    {
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private FunctionDocRepository functionDocRepository = new FunctionDocRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingImageRepository trainingImageRepository = new TrainingImageRepository(new MeetingSignInDBEntities());
        private RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        private CompetencyRepository competencyRepository = new CompetencyRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private TrainingAbnormalRepository trainingAbnormalRepository = new TrainingAbnormalRepository(new MeetingSignInDBEntities());
        private Repositories.PersonRepository personRepository = new Repositories.PersonRepository(new MeetingSignInDBEntities());

        private Repositories.DepartmentRepository departmentRepository = new Repositories.DepartmentRepository(new MeetingSignInDBEntities());
        private ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());
        private ExternalCourseRepository externalCourseRepository = new ExternalCourseRepository(new MeetingSignInDBEntities());
        private ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());
        private InternalCourseRepository internalCourseRepository = new InternalCourseRepository(new MeetingSignInDBEntities());
        private InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
        private SeminarPlanRepository seminarPlanRepository = new SeminarPlanRepository(new MeetingSignInDBEntities());
        private PlanHolderRepository planHolderRepository = new PlanHolderRepository(new MeetingSignInDBEntities());
        private ClosingFileRepository closingFileRepository = new ClosingFileRepository(new MeetingSignInDBEntities());
        private ClassOpinionRepository classOpinionRepository = new ClassOpinionRepository(new MeetingSignInDBEntities());

        comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());

        public string ResponseClosingFile = "反應評估";
        public string LearningClosingFile = "學習評估";
        public string BehaviorClosingFile = "行為評估";
        public string OutcomeClosingFile = "成果評估";
        public string EvaluationClosingFile = "評估報告";

        public DateTime seriesStartDate = Convert.ToDateTime("2020/01/01");
        public DateTime seriesEndDate = DateTime.Now.AddYears(2);
        public DateTime publishDate = Convert.ToDateTime("2022/09/01 08:00");

        /// <summary>
        /// 教育訓練列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Index(TrainingIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var query = trainingRepository.Query(model.Title, model.StartDate, model.EndDate, model.DeptID, model.DepartmentStr, model.FactoryID, model.RoomID, 0, null, model.IsClose, model.Category, modelState);
            if (model.IsOfficer)
            {
                query = query.Where(q => q.Creater == adminInfo.ID);
            }
            var pageResult = query.ToPageResult<Training>(model);
            model.PageResult = Mapper.Map<PageResult<TrainingView>>(pageResult);
            var dataList = new List<TrainingView>(model.PageResult.Data);
            foreach (var item in dataList.ToArray())
            {
                item.DeptName = departmentRepository.FindByDeptID(item.DeptID).Title;

                #region 參與部門
                var signInInfo = trainingPersonRepository.GetSignInInfoList(item.ID).Select(q => q.DeptName).Distinct().ToList();
                item.DepartmentListStr = signInInfo.Count() > 0 ? signInInfo.Aggregate((a, b) => a + ", " + b) : "";
                if (!string.IsNullOrEmpty(model.DepartmentStr))
                {
                    var departmentTitle = departmentRepository.FindByDeptID(model.DepartmentStr).Title;
                    if (!item.DepartmentListStr.Contains(departmentTitle))
                    {
                        dataList.Remove(item);
                    }
                }
                #endregion

                #region 審核狀態
                var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", item.ID).Where(q => q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal && !q.IsInvalid).OrderByDescending(q => q.ID).FirstOrDefault();
                if (!CheckPlanExist(item))
                {
                    item.AuditStatus = "尚未填寫";
                    item.IsInvalid = false;
                }
                else if (CheckPlanExist(item) && auditInfo == null)
                {
                    item.AuditStatus = "已填寫未送審";
                    item.IsInvalid = false;
                }
                else
                {
                    item.AuditState = auditInfo == null ? 0 : auditInfo.State;
                    item.AuditStatus = EnumHelper.GetDescription((AuditState)item.AuditState);
                    item.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                }
                #endregion

                #region 承辦人
                var officer = adminRepository.GetById(item.Creater);
                if (officer != null)
                {
                    var personInfo = t8personRepository.FindByPersonId(officer.Account);
                    if (personInfo != null)
                    {
                        if (string.IsNullOrEmpty(officer.Name) || string.IsNullOrEmpty(officer.DeptId))
                        {
                            officer.Name = personInfo.Name;
                            officer.DeptId = personInfo.DeptID;
                            adminRepository.Update(officer);
                        }
                    }
                    else if (personInfo == null && item.Creater != 1)
                    {
                        item.AuditStatus = "核准:人員離職";
                        item.IsInvalid = false;
                    }
                }
                string officerStr = string.IsNullOrEmpty(officer.Name) ? officer.Account : officer.Name;
                item.Organiser = " 【承辦人 : " + officerStr + "】" + item.Organiser;
                #endregion
            }
            model.PageResult.Data = dataList.AsEnumerable();

            return View(model);
        }

        /// <summary>
        /// 已填寫計畫表之教育訓練
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool CheckPlanExist(TrainingView item)
        {
            bool epExist = externalPlanRepository.Query(item.ID, null).Where(q => q.Objecive != "").OrderByDescending(q => q.ID).FirstOrDefault() != null;
            bool ipExist = internalPlanRepository.Query(item.ID, null).Where(q => q.Investigation != "").OrderByDescending(q => q.ID).FirstOrDefault() != null;
            return epExist || ipExist;
        }

        /// <summary>
        /// 教育訓練圖表概覽
        /// </summary>
        /// <returns></returns>
        public ActionResult OverView(OverViewView model)
        {
            Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
            //OverViewView model = new OverViewView();
            model.DeptList = Mapper.Map<List<DeptItem>>(t8departmentRepository.GetDepartmentQuery().OrderBy(q => q.DeptID));
            model.DepartmentList = Mapper.Map<List<DeptItem>>(t8departmentRepository.GetDepartmentQuery().OrderBy(q => q.DeptID));
            model.StartDate = Convert.ToDateTime("2020/01/01");
            model.EndDate = DateTime.Now;

            model.TrainingStatistics = new StatisticsView();

            model.TrainingStatistics.Overall = new OverallStatisticsView();
            var overallTrainingQuery = trainingRepository.Query("", model.StartDate, model.EndDate, "", "", 0, 0, 0, null, null, 0, modelState).Where(q => q.Category != 3);
            var overallTrainingIdList = overallTrainingQuery.Select(q => q.ID).ToList();
            model.TrainingStatistics.Overall.TrainingNum = overallTrainingIdList.Count();//辦訓課程總數
            var overallTrainingPersonQuery = trainingPersonRepository.GetAll().Where(q => overallTrainingIdList.Contains(q.ID));
            model.TrainingStatistics.Overall.VisitorsNum = overallTrainingPersonQuery.Count();//辦訓總時數
            model.TrainingStatistics.Overall.TrainingHours = Convert.ToInt32(overallTrainingQuery.Sum(q => q.Hours));//辦訓總時數
            var overallTraineesIdList = overallTrainingPersonQuery.Select(q => q.JobID).Distinct();
            model.TrainingStatistics.Overall.TraineesNum = overallTraineesIdList.Count();//受訓總人數
            model.TrainingStatistics.Overall.SeniorNum = overallTraineesIdList.Where(q => seniorList.Contains(q)).Count();//高階主管
            model.TrainingStatistics.Overall.JuniorNum = overallTraineesIdList.Where(q => juniorList.Contains(q)).Count();//中低階主管
            var overallStatistics = model.TrainingStatistics.Overall;
            model.TrainingStatistics.Overall.EmployeeNum = overallStatistics.TraineesNum - (overallStatistics.SeniorNum + overallStatistics.JuniorNum);//非管理職員工
            model.TrainingStatistics.Overall.YoungNum = 0;//青年(29歲(含)以下)受訓總人數

            model.TrainingStatistics.Internal = new InternalStatisticsView();
            var internalTrainingQuery = trainingRepository.Query("", model.StartDate, model.EndDate, "", "", 0, 0, 0, null, null, 0, modelState).Where(q => q.Category == 1);
            var internalTrainingIdList = internalTrainingQuery.Select(q => q.ID).ToList();
            model.TrainingStatistics.Internal.TrainingNum = internalTrainingIdList.Count();
            var internalTrainingPersonQuery = trainingPersonRepository.GetAll().Where(q => overallTrainingIdList.Contains(q.ID));
            model.TrainingStatistics.Internal.VisitorsNum = internalTrainingPersonQuery.Count();
            model.TrainingStatistics.Internal.TrainingHours = Convert.ToInt32(internalTrainingQuery.Sum(q => q.Hours));
            var internalTraineesIdList = internalTrainingPersonQuery.Select(q => q.JobID).Distinct();
            model.TrainingStatistics.Internal.TraineesNum = internalTraineesIdList.Count();//受訓總人數
            model.TrainingStatistics.Internal.SeniorNum = internalTraineesIdList.Where(q => seniorList.Contains(q)).Count();//高階主管
            model.TrainingStatistics.Internal.JuniorNum = internalTraineesIdList.Where(q => juniorList.Contains(q)).Count();//中低階主管
            var internalStatistics = model.TrainingStatistics.Internal;
            model.TrainingStatistics.Internal.EmployeeNum = internalStatistics.TraineesNum - (internalStatistics.SeniorNum + internalStatistics.JuniorNum);//非管理職員工
            model.TrainingStatistics.Internal.TotalFee = 0;//內部訓練總花費

            model.TrainingStatistics.External = new ExternalStatisticsView();
            var externalTrainingQuery = trainingRepository.Query("", model.StartDate, model.EndDate, "", "", 0, 0, 0, null, null, 0, modelState).Where(q => q.Category == 2 || q.Category == 4);
            var externalTrainingIdList = externalTrainingQuery.Select(q => q.ID).ToList();
            model.TrainingStatistics.External.TrainingNum = externalTrainingIdList.Count();
            var externalTrainingPersonQuery = trainingPersonRepository.GetAll().Where(q => overallTrainingIdList.Contains(q.ID));
            model.TrainingStatistics.External.VisitorsNum = externalTrainingPersonQuery.Count();
            model.TrainingStatistics.External.TrainingHours = Convert.ToInt32(externalTrainingQuery.Sum(q => q.Hours));
            var externalTraineesIdList = externalTrainingPersonQuery.Select(q => q.JobID).Distinct();
            model.TrainingStatistics.External.TraineesNum = externalTraineesIdList.Count();//受訓總人數
            model.TrainingStatistics.External.SeniorNum = externalTraineesIdList.Where(q => seniorList.Contains(q)).Count();//高階主管
            model.TrainingStatistics.External.JuniorNum = externalTraineesIdList.Where(q => juniorList.Contains(q)).Count();//中低階主管
            var externalStatistics = model.TrainingStatistics.External;
            model.TrainingStatistics.External.EmployeeNum = externalStatistics.TraineesNum - (externalStatistics.SeniorNum + externalStatistics.JuniorNum);//非管理職員工
            var planIdList = externalPlanRepository.GetAll().Where(q => externalTrainingIdList.Contains(q.TrainingID)).GroupBy(q => q.TrainingID).SelectMany(q => q).Select(q => q.ID).ToList();
            var courseSum = Convert.ToInt32(externalCourseRepository.GetAll().Where(q => planIdList.Contains(q.ExternalID)).Sum(q => q.Total));
            model.TrainingStatistics.External.TotalFee = courseSum;//外部訓練總花費

            return View(model);
        }

        /// <summary>
        /// 教育訓練費用概覽
        /// </summary>
        /// <returns></returns>
        public ActionResult FeeOverView()
        {
            Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
            FeeOverViewView model = new FeeOverViewView();
            model.DeptList = Mapper.Map<List<DeptItem>>(t8departmentRepository.GetDepartmentQuery().OrderBy(q => q.DeptID));
            model.DepartmentList = Mapper.Map<List<DeptItem>>(t8departmentRepository.GetDepartmentQuery().OrderBy(q => q.DeptID));
            model.StartDate = Convert.ToDateTime("2020/01/01");
            model.EndDate = DateTime.Now;

            model.ExternalTrainingList = Mapper.Map<List<ExternalTrainingModel>>(externalCourseRepository.Query());
            foreach (var item in model.ExternalTrainingList)
            {
                var trainingInfo = trainingRepository.GetById(item.TrainingID);
                if (trainingInfo != null)
                {
                    var trainingPersonQuery = trainingPersonRepository.Query(true, "", trainingInfo.ID, "", "", 1);
                    item.Title = trainingInfo.Title;
                    item.Organizer = item.Organizer;
                    item.Lecturer = item.Lecturer;
                    item.CourseDate = trainingInfo.StartDate.ToString("yyyy年MM月dd日 HH:mm");
                    item.Registery = item.Registery;
                    item.Travel = item.Travel;
                    item.Others = item.Others;
                    item.Total = item.Total;
                    item.SignedCount = trainingPersonQuery.Count();
                }
            }

            return View(model);
        }

        public ActionResult PDDRO(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            TrainingPDDROView model = new TrainingPDDROView();
            model.ID = id;
            var trainingInfo = Mapper.Map<TrainingView>(trainingRepository.GetById(id));
            trainingInfo.PicList = trainingImageRepository.Query(id).ToList();
            model.TrainingInfo = trainingInfo;
            comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
            model.TrainingInfo.DeptName = comDepartmentRepository.FindByDeptId(trainingInfo.DeptID).DeptName;

            var courseIdList = new List<int>();
            var iCourseList = internalCourseRepository.Query(id, 0, true);
            if (iCourseList.Count() > 0)
            {
                courseIdList = iCourseList.Select(q => q.ID).ToList();
            }
            var eCourseList = externalCourseRepository.Query(id, 0, true);
            if (eCourseList.Count() > 0)
            {
                courseIdList = eCourseList.Select(q => q.ID).ToList();
            }

            model.SeriesTrainingList = Mapper.Map<List<TrainingView>>(trainingRepository
                .Query("", seriesStartDate, seriesEndDate, "", "", 0, 0, 0, id, null, 0)
                .Where(q => q.TrainingCourseID != null && courseIdList.Contains((int)q.TrainingCourseID))
                .OrderByDescending(q => q.StartDate)
                .OrderBy(q => q.StartDate));

            foreach (var series in model.SeriesTrainingList)
            {
                series.TrainingPersonInfo = Mapper.Map<List<ViewModels.TrainingPerson.TrainingPersonView>>(trainingPersonRepository.Query(null, "", series.ID));
                //foreach (var item in series.TrainingPersonInfo)
                //{
                //    var opionInfo = classOpinionRepository.Query(series.ID, item.JobID).OrderByDescending(q => q.ID).FirstOrDefault();
                //    if (opionInfo != null)
                //    {
                //        item.FilledOpinion = true;
                //    }
                //}
            }

            #region 結案文件
            var isFinished = (model.TrainingInfo.AuditState == (int)AuditState.Approval && model.TrainingInfo.AskClose) || model.TrainingInfo.IsInvalid;
            if (model.TrainingInfo.AskClose)
            {
                SetClosingUpload(id, model.TrainingInfo, isFinished, false);
            }

            #endregion

            var auditLevelData = auditLevelRepository.FindByDept(trainingInfo.DeptID);
            var principalInfo = adminRepository.Query(true, 0, "", true).FirstOrDefault();

            string deptPrincipal = "0000";
            if (auditLevelData != null)
            {
                deptPrincipal = auditLevelData.ManagerReview;
                if (string.IsNullOrEmpty(deptPrincipal) || deptPrincipal == "0000")
                {
                    deptPrincipal = auditLevelData.ViceReview;
                }
                if (string.IsNullOrEmpty(deptPrincipal) || deptPrincipal == "0000")
                {
                    deptPrincipal = auditLevelData.PresidentReview;
                }
                if (string.IsNullOrEmpty(deptPrincipal) || deptPrincipal == "0000")
                {
                    deptPrincipal = auditLevelData.PrincipalReview;
                }
                //負責主管
                model.DeptPrincipal = t8personRepository.FindPersonByPersonId(deptPrincipal).PersonName;
            }
            else
            {
                //負責主管
                model.DeptPrincipal = t8personRepository.FindPersonByPersonId(principalInfo.Account).PersonName;
            }
            //菲力校長
            model.FairlyPrincipal = t8personRepository.FindPersonByPersonId(principalInfo.Account).PersonName;

            #region 參與部門
            var signInInfo = trainingPersonRepository.GetSignInInfoList(model.ID).Select(q => q.DeptName).Distinct().ToList();
            model.TrainingInfo.DepartmentListStr = signInInfo.Count() > 0 ? signInInfo.Aggregate((a, b) => a + ", " + b) : "";
            #endregion

            VisionRepository visionRepository = new VisionRepository(new MeetingSignInDBEntities());
            if (trainingInfo.VisionID == 0)
            {
                trainingInfo.VisionID = 4;
                trainingRepository.Update(Mapper.Map<Training>(trainingInfo));
            }
            model.VisionInfo = Mapper.Map<VisionView>(visionRepository.GetById(trainingInfo.VisionID));
            model.TrainingPersonInfo = Mapper.Map<List<ViewModels.TrainingPerson.TrainingPersonView>>(trainingPersonRepository.Query(null, "", id));

            var hrInfo = compersonRepository.GetPersonQuery("", "", "B40").FirstOrDefault();
            model.HRInfo = hrInfo == null ? new MeetSignIn.Models.T8.Join.GroupJoinPersonAndDepartment() : hrInfo;

            #region 審查
            TrainingAbnormalRepository trainingAbnormalRepository = new TrainingAbnormalRepository(new MeetingSignInDBEntities());
            var unuseDate = Convert.ToDateTime("2001/01/01");
            model.TrainingAbnormalInfo = Mapper.Map<List<ViewModels.TrainingAbnormal.TrainingAbnormalView>>(trainingAbnormalRepository.Query(unuseDate, unuseDate, id, true));

            ReviewReportRepository reviewReportRepository = new ReviewReportRepository(new MeetingSignInDBEntities());
            var trainingEndDate = model.TrainingInfo.EndDate;
            model.ReviewReportInfo = Mapper.Map<ViewModels.ReviewReport.ReviewReportView>(reviewReportRepository.Query("", trainingEndDate.Year, trainingEndDate.Month).FirstOrDefault());
            #endregion

            if (trainingInfo != null)
            {
                model.Category = trainingInfo.Category;
                if (trainingInfo.Category == (int)Category.External)
                {
                    model.ExternalPlanInfo = Mapper.Map<ExternalPlanView>(externalPlanRepository.Query(id).OrderByDescending(q => q.ID).FirstOrDefault());
                    if (model.ExternalPlanInfo != null)
                    {
                        model.ExternalPlanInfo.CourseList = Mapper.Map<List<ExternalCourseView>>(externalCourseRepository.Query(id, model.ExternalPlanInfo.ID));
                    }
                    model.ExternalPlanInfo.DocList = functionDocRepository.Query("LT" + id.ToString("D6")).ToList();
                    model.ExternalPlanInfo.HolderList = planHolderRepository.Query(id).ToList();

                    var eaQuery = externalAcceptanceRepository.Query(id).GroupBy(q => q.PersonID).SelectMany(q => q.Take(1)).ToList();
                    model.ExternalAcceptanceInfo = Mapper.Map<List<ExternalAcceptanceView>>(eaQuery);
                    var trainingPlan = externalPlanRepository.Query(id, null).FirstOrDefault();
                    if (trainingPlan != null)
                    {
                        var inspList = trainingPlan.Inspection.Split(',');
                        model.HasReport = inspList.Contains("1");//心得報告【行為評估】
                        model.HasCertificate = inspList.Contains("2");//技能檢定/證書【學習評估】
                        model.HasDelegate = inspList.Contains("3");//內部轉授【行為評估】
                        model.HasActionPlan = inspList.Contains("4");//工作應用行動計畫【行為評估】
                        model.HasPerformance = inspList.Contains("5");//績效評估【成果評估】
                        model.HasOpinion = inspList.Contains("6");//滿意度調查【反應評估】
                    }
                }
                else if (trainingInfo.Category == (int)Category.Internal)
                {
                    model.InternalPlanInfo = Mapper.Map<InternalPlanView>(internalPlanRepository.Query(id).OrderByDescending(q => q.ID).FirstOrDefault());
                    if (model.InternalPlanInfo != null)
                    {
                        model.InternalPlanInfo.CourseList = Mapper.Map<List<InternalCourseView>>(internalCourseRepository.Query(id, model.InternalPlanInfo.ID));
                    }
                    model.InternalPlanInfo.DocList = functionDocRepository.Query("LT" + id.ToString("D6")).ToList();
                    model.InternalPlanInfo.HolderList = planHolderRepository.Query(id).ToList();

                    var iaQuery = internalAcceptanceRepository.Query(id).GroupBy(q => q.PersonID).SelectMany(q => q.Take(1)).ToList();
                    model.InternalAcceptanceInfo = Mapper.Map<List<InternalAcceptanceView>>(iaQuery);
                    var trainingPlan = internalPlanRepository.Query(id, null).FirstOrDefault();
                    if (trainingPlan != null)
                    {
                        model.HasOpinion = trainingPlan.Response;//【反應評估】滿意度調查機制
                        model.HasLearning = trainingPlan.Learning;//【學習評估】考試或報告機制
                        model.HasActionPlan = trainingPlan.Behavior;//【行為評估】課後行動計畫調查機制
                        model.HasPerformance = trainingPlan.Outcome;//【成果評估】工作績效調查機制
                    }
                }

                //菲力已記錄講師文件
                var createrInfo = adminRepository.GetById(model.TrainingInfo.Creater);
                var createrPersonInfo = t8personRepository.GetPersonQuery("", "", "", false, true, true).Where(q => q.PersonId == createrInfo.Account).FirstOrDefault();
                if (createrPersonInfo != null)
                {
                    model.CreaterDocList = functionDocRepository.Query(createrPersonInfo.PersonId).ToList();
                }

                var createrDeptId = createrPersonInfo == null ? "0000" : createrPersonInfo.DeptID;
                model.CompetencyInfo = Mapper.Map<CompetencyView>(competencyRepository.Query(true, createrPersonInfo.DeptName).FirstOrDefault());
            }

            return View(model);
        }

        /// <summary>
        /// 取得異動資料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public JsonResult GetAbnormalInfo(int id)
        {
            var result = trainingAbnormalRepository.GetById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 未結案列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult ClosedStatusIndex(TrainingCloseIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            model.IsClose = false;
            var query = trainingRepository.Query("", model.StartDate, model.EndDate, "", "", 0, 0, 0, 0, model.IsClose, 0, modelState);
            if (model.IsOfficer)
            {
                query = query.Where(q => q.Creater == adminInfo.ID);
            }
            var pageResult = query.ToPageResult<Training>(model);
            model.PageResult = Mapper.Map<PageResult<TrainingView>>(pageResult);
            foreach (var tr in model.PageResult.Data)
            {
                #region 審核狀態
                var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", tr.ID).Where(q => q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal && !q.IsInvalid).OrderByDescending(q => q.ID).FirstOrDefault();
                bool planExist = CheckPlanExist(tr);
                string auditStatus = "";
                if (!planExist)
                {
                    auditStatus = "尚未填寫";
                }
                else if (planExist && auditInfo == null)
                {
                    auditStatus = "已填寫未送審";
                }
                else
                {
                    tr.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                    if (tr.IsInvalid)
                    {
                        auditStatus = "已駁回";
                    }
                    else if (tr.AuditState == (int)MeetSignIn.Models.AuditState.PresidentAudit)
                    {
                        auditStatus = "已核准";
                    }
                    else
                    {
                        tr.AuditState = auditInfo == null ? 0 : auditInfo.State;
                        auditStatus = EnumHelper.GetDescription((AuditState)tr.AuditState);
                    }
                }
                #endregion
                tr.AuditStatus = auditStatus;

                //    string closeStatus = "";
                //    if (tr.IsClose)
                //    {
                //        closeStatus = "已結案";
                //    }
                //    else if (tr.PresidentClose)
                //    {
                //        closeStatus = "待校長結案";
                //    }
                //    else if (tr.AskClose)
                //    {
                //        closeStatus = "待總經理結案";
                //    }
                //    tr.CloseStatus = closeStatus;
            }

            return View(model);
        }

        /// <summary>
        /// 取得計劃表資料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public JsonResult GetAcceptanceInfo(int id, int category)
        {
            if (category == (int)Category.Internal)
            {
                var result = internalAcceptanceRepository.GetById(id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            if (category == (int)Category.External)
            {
                var result = externalAcceptanceRepository.GetById(id);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return null;
        }

        /// <summary>
        /// 複製訓練
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Copy(int id)
        {
            try
            {
                var model = new TrainingView();
                var query = trainingRepository.FindBy(id);
                model = Mapper.Map<TrainingView>(query);
                if (model.StartDate < Convert.ToDateTime("2010/01/01") || model.EndDate < Convert.ToDateTime("2010/01/01"))
                {
                    SetDefaultDate(model);
                }

                #region 建立新會議
                string Add14Str = DateTime.Now.AddDays(14).ToString("yyyy/MM/dd");
                var data = Mapper.Map<Training>(model);
                data.ID = 0;
                data.Title += "---複製";
                data.SignDate = Add14Days(Add14Str, data.SignDate);
                data.StartDate = Add14Days(Add14Str, data.StartDate);
                data.EndDate = Add14Days(Add14Str, data.EndDate);
                data.IsSignable = false;
                data.IsOnline = true;
                data.AskClose = false;
                data.IsClose = false;
                data.CreateDate = DateTime.Now;

                int nId = trainingRepository.Insert(data);
                #endregion

                var notifyList = trainingPersonRepository.Query(null, "", id).Select(q => q.JobID).ToList();
                var employeeInfoList = t8personRepository.GetPersonQuery().Where(q => notifyList.Contains(q.JobID)).ToList();
                //model.Notify = Mapper.Map<List<EmployeeView>>(employeeInfoList);

                #region 新增訓練名單
                //新增名單                   
                foreach (var item in employeeInfoList)
                {
                    string engName = string.IsNullOrEmpty(item.EngName) ? "" : " " + item.EngName;
                    var result = trainingPersonRepository.Insert(new TrainingPerson()
                    {
                        SignCode = Convert.ToString(Guid.NewGuid()),
                        TraningID = nId,
                        JobID = item.JobID,
                        Name = item.Name + engName,
                        IsSign = false,
                        SignStatus = (int)SignStatus.UnSigned,
                        IsSent = false,
                        IsPassed = false,
                    });
                }
                #endregion

                #region 複製計畫表
                switch ((Category)data.Category)
                {
                    case Category.Internal:
                        var inplan = Mapper.Map<InternalPlanView>(internalPlanRepository.Query(id).OrderByDescending(q => q.ID).FirstOrDefault());
                        if (inplan != null)
                        {
                            var newPlan = inplan;
                            newPlan.ID = 0;
                            newPlan.TrainingID = nId;
                            SetAuditHelper.SetDefault(newPlan);
                            var newPlanData = Mapper.Map<InternalPlan>(newPlan);
                            newPlanData.OfficerAuditDate = SetNullDate(newPlanData.OfficerAuditDate);
                            newPlanData.ManagerAuditDate = SetNullDate(newPlanData.ManagerAuditDate);
                            newPlanData.ViceAuditDate = SetNullDate(newPlanData.ViceAuditDate);
                            newPlanData.PresidentAuditDate = SetNullDate(newPlanData.PresidentAuditDate);
                            newPlanData.PrincipalAuditDate = SetNullDate(newPlanData.PrincipalAuditDate);
                            if (newPlanData.OfficerAuditDate == DefaultDate)
                            {
                                newPlanData.OfficerAuditDate = null;
                            }
                            newPlan.ID = internalPlanRepository.Insert(newPlanData);

                            #region 複製課程
                            var incourseList = internalCourseRepository.Query(id, inplan.ID, true);
                            foreach (var item in incourseList.ToList())
                            {
                                internalCourseRepository.Insert(new InternalCourse()
                                {
                                    InternalID = newPlan.ID,
                                    TrainingID = id,
                                    Outline = item.Outline,
                                    Date = item.Date,
                                    Time = item.Time,
                                    Hours = item.Hours,
                                    Progress = item.Progress,
                                    Lecturer = item.Lecturer,
                                    TrainingMethod = item.TrainingMethod,
                                    IsOnline = true,
                                });
                            }
                            #endregion
                        }
                        break;
                    case Category.External:
                        var explan = Mapper.Map<ExternalPlanView>(externalPlanRepository.Query(id).OrderByDescending(q => q.ID).FirstOrDefault());
                        if (explan != null)
                        {
                            var newPlan = explan;
                            newPlan.ID = 0;
                            newPlan.TrainingID = nId;
                            SetAuditHelper.SetDefault(newPlan);
                            var newPlanData = Mapper.Map<ExternalPlan>(newPlan);
                            newPlanData.OfficerAuditDate = SetNullDate(newPlanData.OfficerAuditDate);
                            newPlanData.ManagerAuditDate = SetNullDate(newPlanData.ManagerAuditDate);
                            newPlanData.ViceAuditDate = SetNullDate(newPlanData.ViceAuditDate);
                            newPlanData.PresidentAuditDate = SetNullDate(newPlanData.PresidentAuditDate);
                            newPlanData.PrincipalAuditDate = SetNullDate(newPlanData.PrincipalAuditDate);
                            newPlan.ID = externalPlanRepository.Insert(newPlanData);

                            #region 複製課程
                            var excourseList = externalCourseRepository.Query(id, explan.ID, true);
                            foreach (var item in excourseList.ToList())
                            {
                                externalCourseRepository.Insert(new ExternalCourse()
                                {
                                    ExternalID = newPlan.ID,
                                    TrainingID = id,
                                    Organizer = item.Organizer,
                                    Lecturer = item.Lecturer,
                                    CourseMaterial = item.CourseMaterial,
                                    Address = item.Address,
                                    StartDate = item.StartDate,
                                    EndDate = item.EndDate,
                                    Registery = item.Registery,
                                    Travel = item.Travel,
                                    Others = item.Others,
                                    Total = item.Total,
                                    IsOnline = true,
                                });
                            }
                            #endregion
                        }
                        break;
                        //case Category.Seminar:
                        //    break;
                        //case Category.ExternalLecturer:
                        //    break;
                }
                #endregion

                return RedirectToAction("Edit", new { id = nId });
            }
            catch (Exception ex)
            {

                throw;
            }
            //return RedirectToAction("Edit", new { id = id });
        }

        private DateTime? SetNullDate(DateTime? date)
        {
            if (date == DefaultDate)
            {
                return null;
            }
            return date;
        }

        private static DateTime Add14Days(string Add14Str, DateTime date)
        {
            return Convert.ToDateTime(Add14Str + date.ToString(" HH:mm"));
        }

        /// <summary>
        /// 內訓建立編輯頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id = 0, bool FromAudit = false)
        {
            var model = new TrainingView();
            model.FromAudit = FromAudit;
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (id != 0)
            {
                var query = trainingRepository.FindBy(id);
                model = Mapper.Map<TrainingView>(query);
                model.Department = departmentRepository.FindByDeptID(model.DeptID).ID;
                if (model.StartDate < Convert.ToDateTime("2010/01/01") || model.EndDate < Convert.ToDateTime("2010/01/01"))
                {
                    SetDefaultDate(model);
                }

                var notifyList = trainingPersonRepository.Query(null, "", id).Select(q => q.JobID).ToList();
                var employeeInfo = t8personRepository.GetPersonQuery().Where(q => notifyList.Contains(q.JobID)).ToList();
                model.Notify = Mapper.Map<List<EmployeeView>>(employeeInfo);

                //部門主管判斷檢查
                model.SameDeptManager = false;
                if (adminInfo.Type == (int)AdminType.Manager)
                {
                    var personInfo = t8personRepository.FindByPersonId(adminRepository.GetById(adminInfo.ID).Account);
                    var personDept = personInfo == null ? "" : personInfo.DeptID;
                    var deptIdList = trainingPersonRepository.GetSignInInfoList(model.ID).Select(q => q.DeptID).Distinct().ToList();
                    model.SameDeptManager = deptIdList.Contains(personDept);
                }

                model.PicList = trainingImageRepository.Query(id).ToList();

                #region 團體驗收
                if (model.GroupType == (int)GroupType.Group)
                {
                    var IAQuery = internalAcceptanceRepository.Query(model.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (IAQuery != null)
                    {
                        model.IAInfo = Mapper.Map<InternalAcceptanceView>(IAQuery);
                        var trainingPlan = internalPlanRepository.Query(model.ID, null).FirstOrDefault();
                        if (trainingPlan != null)
                        {
                            model.IAInfo.HasOpinion = trainingPlan.Response;//【反應評估】滿意度調查機制
                            model.IAInfo.HasLearning = trainingPlan.Learning;//【學習評估】考試或報告機制
                            model.IAInfo.HasActionPlan = trainingPlan.Behavior;//【行為評估】課後行動計畫調查機制
                            model.IAInfo.HasPerformance = trainingPlan.Outcome;//【成果評估】工作績效調查機制
                        }
                    }
                }
                #endregion

                var IPQuery = internalPlanRepository.Query(model.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                model.HasExam = false; 
                if (IPQuery != null)
                {
                    model.IPInfo = Mapper.Map<InternalPlanView>(IPQuery);
                    if (IPQuery.Learning)
                    {
                        model.HasExam = true;
                    }
                }

            }
            else
            {
                SetDefaultDate(model);
                model.IsSignable = true;
                model.IsOnline = true;
                model.LateMinutes = 10;
            }

            model.Category = (int)Category.Internal;

            //bool existExternal = externalPlanRepository.Query(model.ID, true).OrderByDescending(q => q.ID).FirstOrDefault() != null;
            //bool existInternal = internalPlanRepository.Query(model.ID, true).OrderByDescending(q => q.ID).FirstOrDefault() != null;
            //model.IsAudit = CheckPlanExist(model) && model.ID != 0;

            var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", model.ID).Where(q => (q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal) && !q.IsInvalid).OrderByDescending(q => q.ID).FirstOrDefault();
            model.AuditState = auditInfo == null ? 0 : auditInfo.State;
            var planExist = CheckPlanExist(model);
            model.IsAudit = planExist && auditInfo != null && model.ID != 0;
            if (!planExist)
            {
                model.AuditStatus = "尚未填寫";
                model.IsInvalid = false;
            }
            else if (planExist && auditInfo == null)
            {
                model.AuditStatus = "已填寫未送審";
                model.IsInvalid = false;
            }
            else
            {
                model.AuditState = auditInfo == null ? 0 : auditInfo.State;
                model.AuditStatus = EnumHelper.GetDescription((AuditState)model.AuditState);
                model.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
            }

            #region 承辦人
            var officer = adminRepository.GetById(model.Creater);
            if (officer != null)
            {
                var personInfo = t8personRepository.FindByPersonId(officer.Account);
                if (personInfo == null && model.Creater != 1)
                {
                    model.AuditStatus = "核准:人員離職";
                    model.IsInvalid = false;
                }
            }
            #endregion

            #region 結案文件
            var isFinished = (model.AuditState == (int)AuditState.Approval && model.AskClose) || model.IsInvalid;
            var isCreater = adminInfo.ID == model.Creater;
            //if (model.AskClose)
            //{
                SetClosingUpload(id, model, isFinished, isCreater);
            //}
            #endregion


            return View(model);
        }

        private void SetClosingUpload(int id, TrainingView model, bool isFinished, bool isCreater)
        {
            #region 反應評估
            model.ResponseASSAFile = GetClosingFile(id, isFinished, isCreater, ResponseClosingFile
                , "課後滿意度調查分析表及建議回饋紀錄", "課後滿意度調查分析表及課後檢討講師、學員、人資、部門主管等利益關係人的建議回饋紀錄。"
                , true, "/Admin/TrainingAdmin/PDDRO/" + id + "?ahr=pddro_outcome", "進頁面後，移動到最下方 \"Outcome 成果\"，點選【意見回饋】按鈕，可查看課堂意見統計"
                , false, ""
                , false);

            var trainingReportList = new List<string>();
            //if (!string.IsNullOrEmpty(model.MainReport))
            //{
            //    trainingReportList.Add($"/FileUploads/TrainingPhoto/{model.MainReport}");
            //    var closingFileData = closingFileRepository.FindFile(id, ResponseClosingFile, "課程結案報告");
            //    if (closingFileData == null)
            //    {//結案報告複製課堂報告
            //        ClosingFile fileData = new ClosingFile();
            //        fileData.TrainingID = id;
            //        fileData.IndexItem = ResponseClosingFile;
            //        fileData.FileNameType = "課程結案報告";

            //        fileData.FileName = model.MainReport;
            //        closingFileRepository.Insert(fileData);
            //    }
            //}
            model.ResponseASSBFile = GetClosingFile(id, isFinished, isCreater, ResponseClosingFile
                , "課程結案報告", "課程結案報告（含問卷調查統計分析、課程規劃紀錄等含對後續課程的開課建議事項）。"
                , false, "", ""
                , false, ""
                , true, trainingReportList);

            model.ResponseASSCFile = GetClosingFile(id, isFinished, isCreater, ResponseClosingFile
                , "課程改善行動方案", "依反應評估進行課程改善的行動方案。"
                , false, "", ""
                , true, "/Content/SampleFile/課程改善行動方案.xlsx");

            model.ResponseASSDFile = GetClosingFile(id, isFinished, isCreater, ResponseClosingFile
                , "其他");
            #endregion

            #region 學習評估
            //model.LearningASSAFile = GetClosingFile(id, isFinished, isCreater, LearningClosingFile
            //, "綜合檢討報告", "評量紀錄與綜合檢討報告、考試成績紀錄與學員升等的相關性佐證、學員心得報告與對課程的改善建議。");

            model.LearningASSBFile = GetClosingFile(id, isFinished, isCreater, LearningClosingFile
                , "訓練心得分享", "如社群討論有關訓練心得分享的資訊等。"
                , true, "/Admin/TrainingPersonAdmin?TraningID=" + id, "進頁面後，點選名單後方【內/外訓驗收】按鈕，可查看該人員心得回饋");

            model.LearningASSCFile = GetClosingFile(id, isFinished, isCreater, LearningClosingFile
                , "講師/承辦人課後回饋", "講師評鑑或講師課後回饋（含對後續課程的開課建議事項）。"
                , false, "", ""
                , true, "/Content/SampleFile/講師.承辦人課後回饋.xlsx");

            model.LearningASSDFile = GetClosingFile(id, isFinished, isCreater, LearningClosingFile
                , "其他");
            #endregion

            #region 行為評估
            model.BehaviorASSAFile = GetClosingFile(id, isFinished, isCreater, BehaviorClosingFile
                , "課後行動計畫表", ""
                , true, "/Admin/TrainingPersonAdmin?TraningID=" + id, "進頁面後，點選名單後方【內/外訓驗收】按鈕，可查看該人員行動計畫");

            model.BehaviorASSBFile = GetClosingFile(id, isFinished, isCreater, BehaviorClosingFile
                , "學習成效表或課後行動計畫實施成效查核統計表", "學員課後行為追蹤評估回饋表、學習成效表、課後行動計畫實施成效查核統計表。"
                , false, "", ""
                , true, "/Content/SampleFile/教育訓練成效追蹤調查表.xlsx");

            //model.BehaviorASSCFile = GetClosingFile(id, isFinished, isCreater, BehaviorClosingFile
            //, "訓練項目相關的績效考核表");

            model.BehaviorASSDFile = GetClosingFile(id, isFinished, isCreater, BehaviorClosingFile
                , "其他", "主管或高階主管認為訓練後具有員工行為、能力顯著改善的成效。 <br> 及其他如知識管理社群討論、神秘客實地稽核（外部稽核）課後機制等。");
            #endregion

            #region 成果評估
            model.OutcomeASSAFile = GetClosingFile(id, isFinished, isCreater, OutcomeClosingFile
                , "訓後成果評估調查機制", "訓練相關的組織績效訓後成果評估調查機制。");

            model.OutcomeASSBFile = GetClosingFile(id, isFinished, isCreater, OutcomeClosingFile
                , "組織 KPI 評估檢討報告");

            model.OutcomeASSCFile = GetClosingFile(id, isFinished, isCreater, OutcomeClosingFile
                , "具體呈現訓練與組織績效提升文件", "教育訓練績效報告中，具體呈現訓練與組織績效提升的相關性，如與訓練相關的特定營收成長、成本降低報告。");

            model.OutcomeASSDFile = GetClosingFile(id, isFinished, isCreater, OutcomeClosingFile
                , "其他", "主管或高階主管認為訓練後具有員工行為、能力顯著改善的成效。 <br> 其他如知識管理社群討論、神秘客實地稽核（外部稽核）課後機制。 <br> 展現經營績效及訓練貢獻連結的機制或方法。");
            #endregion

            #region 評估報告
            model.EvaluationASSAFile = GetClosingFile(id, isFinished, isCreater, EvaluationClosingFile
                , "評估報告", "無");
            #endregion
        }

        private FilesView GetClosingFile(int trainingId, bool isFinished, bool isCreater, string indexItem, string fileNameType, string description = ""
            , bool hasLink = false, string linkText = "", string linkDesc = ""
            , bool hasSample = false, string sampleLink = "", bool canUpload = true, List<string> existFiles = null)
        {
            var data = new FilesView();
            var closingFileQuery = closingFileRepository.FindFile(trainingId, indexItem, fileNameType);
            if (closingFileQuery != null)
            {
                var query = closingFileRepository.FindFile(trainingId, indexItem, fileNameType);
                data = Mapper.Map<FilesView>(query);
            }
            if (hasLink)
            {
                data.HasLink = hasLink;
                data.LinkText = linkText;
                data.LinkDesc = linkDesc;
            }
            if (hasSample)
            {
                data.HasSample = hasSample;
                data.SampleLink = sampleLink;
            }
            data.TrainingID = trainingId;
            data.IsFinished = isFinished;
            data.IsCreater = isCreater;
            data.IndexItem = indexItem;
            data.Description = description;
            data.FileNameType = fileNameType;
            data.CanUpload = canUpload;
            data.ExistFiles = existFiles == null ? new List<string>() : existFiles;
            data.FileList = Mapper.Map<List<ClosingFile>>(closingFileRepository.Query(trainingId, indexItem, fileNameType));
            return data;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(TrainingView model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Training data = Mapper.Map<Training>(model);
                    var adminInfo = AdminInfoHelper.GetAdminInfo();
                    int adminId = adminInfo.ID;
                    if (modelState)
                    {
                        data.IsModel = true;
                    }

                    #region 檔案處理
                    //bool hasPic = ImageHelper.CheckFileExists(model.TraningPic);
                    bool hasFile = ImageHelper.CheckFileExists(model.TraningFile);
                    bool hasVideo = ImageHelper.CheckFileExists(model.TraningVideo);
                    bool hasReport = ImageHelper.CheckFileExists(model.TraningReport);
                    bool hasAcceptance = ImageHelper.CheckFileExists(model.TraningAcceptance);
                    //if (hasPic)
                    //{//照片
                    //    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    //    data.MainPic = ImageHelper.SaveFile(model.TraningPic, PhotoFolder);
                    //}
                    if (model.PicsFiles != null)
                    {//上傳多圖
                        foreach (HttpPostedFileBase pic in model.PicsFiles)
                        {
                            bool hasPic = ImageHelper.CheckFileExists(pic);
                            if (hasPic)
                            {
                                TrainingImage fileData = new TrainingImage();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                fileData.TrainingID = model.ID;
                                fileData.Pics = ImageHelper.SaveFile(pic, PhotoFolder);
                                trainingImageRepository.Insert(fileData);
                            }
                        }
                    }
                    if (hasFile)
                    {//檔案
                        ImageHelper.DeleteFile(PhotoFolder, model.MainFile);
                        data.MainFile = ImageHelper.SaveFile(model.TraningFile, PhotoFolder);
                    }
                    if (hasVideo)
                    {//影片
                        ImageHelper.DeleteFile(PhotoFolder, model.MainVideo);
                        data.MainVideo = ImageHelper.SaveFile(model.TraningVideo, PhotoFolder);
                    }
                    if (hasReport)
                    {//報告
                        ImageHelper.DeleteFile(PhotoFolder, model.MainReport);
                        data.MainReport = ImageHelper.SaveFile(model.TraningReport, PhotoFolder);
                    }
                    if (hasAcceptance)
                    {//驗收
                        ImageHelper.DeleteFile(PhotoFolder, model.MainAcceptance);
                        data.MainAcceptance = ImageHelper.SaveFile(model.TraningAcceptance, PhotoFolder);
                    }

                    #endregion

                    if (model.ID == 0)
                    {
                        data.Creater = adminId;
                        data.MeetingNumber = data.StartDate.ToString("yyyyMmddHHmm");
                        data.AskClose = false;
                        data.IsClose = false;
                        model.ID = trainingRepository.Insert(data);
                        ShowMessage(true, "新增成功");
                    }
                    else
                    {
                        if (adminId != trainingRepository.FindBy(model.ID).Creater && adminInfo.Type > (int)AdminType.Manager)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return View(model);
                        }
                        if (model.PresidentClose && string.IsNullOrEmpty(model.PresidentComment))
                        {
                            model.PresidentClose = false;
                            data.PresidentClose = false;
                        }
                        if (model.IsClose && string.IsNullOrEmpty(model.PrincipalComment))
                        {
                            model.IsClose = false;
                            data.IsClose = false;
                        }
                        trainingRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }

                    System.IO.Directory.CreateDirectory(Server.MapPath("/FileUploads/Qrcode") + "/" + model.ID);

                    #region 新增會議名單
                    //判斷會議存在
                    if (model.ID != 0)
                    {
                        //新名單
                        var employeeInfoList = t8personRepository.GetSelected(model.selectedNotifyList);
                        var newList = employeeInfoList.OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                        var newJoinedId = newList.Count() > 0 ? newList.Aggregate((a, b) => a + ", " + b) : "";
                        //原名單
                        var oldList = trainingPersonRepository.Query(null, "", model.ID).OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                        var oldJoinedId = oldList.Count() > 0 ? oldList.Aggregate((a, b) => a + ", " + b) : "";
                        bool isDiff = (oldJoinedId != newJoinedId && newList.Count() > 0);
                        if (isDiff)
                        {
                            try
                            {
                                //刪除原名單
                                var deletList = trainingPersonRepository.Query(null, "", model.ID).Where(q => !newList.Contains(q.JobID)).Select(q => q.ID);
                                foreach (var item in deletList)
                                {
                                    trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
                                    trainingPersonRepository.Delete(item);
                                }

                                //新增名單                   
                                var failedList = new List<string>();
                                foreach (var item in employeeInfoList.Where(q => !oldList.Contains(q.JobID)))
                                {
                                    string engName = string.IsNullOrEmpty(item.EngName) ? "" : " " + item.EngName;
                                    var result = trainingPersonRepository.Insert(new TrainingPerson()
                                    {
                                        SignCode = Convert.ToString(Guid.NewGuid()),
                                        TraningID = model.ID,
                                        JobID = item.JobID,
                                        Name = item.Name + engName,
                                        IsSign = false,
                                        SignStatus = (int)SignStatus.UnSigned,
                                        IsSent = false,
                                        IsPassed = false,
                                    });
                                    if (result == 0)
                                    {
                                        failedList.Add(item.JobID);
                                    }
                                    var personInfo = personRepository.GetByJobID(item.JobID);
                                }
                            }
                            catch (System.Exception e)
                            {

                                throw;
                            }
                        }
                    }
                    #endregion

                    #region 結案審核狀態                  
                    UpdateCloseAudit(model);
                    #endregion

                    if (model.SeriesParentID == 0)
                    {
                        bool existForm = internalPlanRepository.Query(model.ID).Count() > 0;
                        if (!existForm)
                        {
                            return RedirectToAction("Internal", "TrainingAdmin", new { trainingId = model.ID });
                        }
                    }
                }
                catch (Exception e)
                {
                    ShowMessage(false, e.Message);
                }

                return RedirectToAction("Edit", new { id = model.ID, FromAudit = model.FromAudit });
            }
            else
            {
                ShowMessage(false, ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage);
            }

            return View(model);
        }

        public ActionResult ClosingTraining(TrainingView model)
        {
            try
            {
                var trainingInfo = trainingRepository.GetById(model.ID);
                #region 結案文件
                if (!CheckHasFile(model.ResponseASSAFile)
                    && CheckHasFile(model.ResponseASSBFile)
                    && CheckHasFile(model.ResponseASSCFile)
                    && CheckHasFile(model.ResponseASSDFile)
                    && CheckHasFile(model.LearningASSBFile)
                    && CheckHasFile(model.LearningASSCFile)
                    && CheckHasFile(model.LearningASSDFile)
                    && CheckHasFile(model.BehaviorASSAFile)
                    && CheckHasFile(model.BehaviorASSBFile)
                    && CheckHasFile(model.BehaviorASSDFile)
                    && CheckHasFile(model.OutcomeASSAFile)
                    && CheckHasFile(model.OutcomeASSBFile)
                    && CheckHasFile(model.OutcomeASSCFile)
                    && CheckHasFile(model.OutcomeASSDFile)
                    && CheckHasFile(model.EvaluationASSAFile)
                    && string.IsNullOrEmpty(trainingInfo.MainReport)
                    && closingFileRepository.Query(model.ID).Count() <= 0)
                {
                    ShowMessage(false, "需上傳捷案文件才可執行此動作 !");
                    if (model.Category == (int)Category.External)
                    {
                        return RedirectToAction("ExternalEdit", new { id = model.ID });
                    }
                    else
                    {
                        return RedirectToAction("Edit", new { id = model.ID });
                    }
                }
                //儲存結案文件
                string ClosingFileFolder = Path.Combine(Server.MapPath(FileUploads), "ClosingFile");
                SaveClosingFiles(model.ID, ResponseClosingFile, "課後滿意度調查分析表、建議回饋紀錄", model.ResponseASSAFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, ResponseClosingFile, "課程結案報告", model.ResponseASSBFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, ResponseClosingFile, "課程改善行動方案", model.ResponseASSCFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, ResponseClosingFile, "其他", model.ResponseASSDFile.UploadFiles, ClosingFileFolder);

                //SaveClosingFiles(model.ID, LearningClosingFile, "綜合檢討報告", model.LearningASSAFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, LearningClosingFile, "訓練心得分享", model.LearningASSBFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, LearningClosingFile, "講師/承辦人課後回饋", model.LearningASSCFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, LearningClosingFile, "其他", model.LearningASSDFile.UploadFiles, ClosingFileFolder);

                SaveClosingFiles(model.ID, BehaviorClosingFile, "課後行動計畫表", model.BehaviorASSAFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, BehaviorClosingFile, "學習成效表或課後行動計畫實施成效查核統計表", model.BehaviorASSBFile.UploadFiles, ClosingFileFolder);
                //SaveClosingFiles(model.ID, BehaviorClosingFile, "訓練項目相關的績效考核表", model.BehaviorASSCFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, BehaviorClosingFile, "其他", model.BehaviorASSDFile.UploadFiles, ClosingFileFolder);

                SaveClosingFiles(model.ID, OutcomeClosingFile, "訓後成果評估調查機制", model.OutcomeASSAFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, OutcomeClosingFile, "組織 KPI 評估檢討報告", model.OutcomeASSBFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, OutcomeClosingFile, "具體呈現訓練與組織績效提升文件", model.OutcomeASSCFile.UploadFiles, ClosingFileFolder);
                SaveClosingFiles(model.ID, OutcomeClosingFile, "其他", model.OutcomeASSDFile.UploadFiles, ClosingFileFolder);

                SaveClosingFiles(model.ID, EvaluationClosingFile, "評估報告", model.EvaluationASSAFile.UploadFiles, ClosingFileFolder);
                if (model.SendClose)
                {
                    if (closingFileRepository.Query(model.ID).Count() > 0)
                    {
                        var attendanceQuery = trainingPersonRepository.Query(true, "", model.ID);
                        if (model.Category == (int)Category.External)
                        {
                            ///Inspection : 
                            ///1. 心得報告【行為評估】
                            ///2. 技能檢定/證書【學習評估】
                            ///3. 內部轉授【行為評估】
                            ///4. 工作應用行動計畫【行為評估】
                            ///5. 績效評估【成果評估】
                            ///6. 滿意度調查【反應評估】
                            //檢查驗收資料全部填寫完畢
                            bool acceptAskedClose = true;
                            var externalPlanData = externalPlanRepository.Query(model.ID, true).OrderByDescending(q => q.ID).FirstOrDefault();
                            if (externalPlanData == null)
                            {
                                ShowMessage(false, "無法結案 : 計畫表尚未核准");
                                acceptAskedClose = false;
                            }
                            else
                            {
                                var trainingPersonQuery = trainingPersonRepository.Query(true, "", model.ID);
                                if (model.GroupType == (int)GroupType.Person)
                                {
                                    if (externalPlanData.Inspection.Contains("1") || externalPlanData.Inspection.Contains("2") || externalPlanData.Inspection.Contains("3") || externalPlanData.Inspection.Contains("4") || externalPlanData.Inspection.Contains("5"))
                                    {
                                        var acceptanceQuery = externalAcceptanceRepository.Query(model.ID);
                                        if (acceptanceQuery.Count() < trainingPersonQuery.Count())
                                        {
                                            ShowMessage(false, "無法結案 : 學員驗收資料未補齊");
                                            acceptAskedClose = false;
                                        }
                                    }
                                }
                                else
                                {
                                    string createrPersonId = "0000";
                                    var createrPersonInfo = adminRepository.GetById(model.Creater);
                                    if (createrPersonInfo != null)
                                    {
                                        createrPersonId = createrPersonInfo.Account;
                                    }
                                    if (string.IsNullOrEmpty(model.MainAcceptance) && externalAcceptanceRepository.FindByTrainingIdAndPersonId(model.ID, createrPersonId) == null)
                                    {
                                        ShowMessage(false, "無法結案 : 承辦人驗收資料未補齊");
                                        acceptAskedClose = false;
                                    }
                                }
                                if (externalPlanData.Inspection.Contains("1"))
                                {
                                    var opinionQuery = classOpinionRepository.Query(model.ID);
                                    if (opinionQuery.Count() < trainingPersonQuery.Count())
                                    {
                                        ShowMessage(false, "無法結案 : 學員意見回饋未補齊");
                                        acceptAskedClose = false;
                                    }
                                }

                            }
                            if (acceptAskedClose)
                            {
                                AskCloseTraining(model.ID, "ExternalEdit");
                            }
                        }
                        else if (model.Category == (int)Category.Internal)
                        {
                            ///Response【反應評估】滿意度調查機制
                            ///Learning【學習評估】考試或報告機制
                            ///Behavior【行為評估】課後行動計畫調查機制
                            ///Outcome【成果評估】工作績效調查機制
                            //檢查驗收資料全部填寫完畢
                            bool acceptAskedClose = true;
                            var internalPlanData = internalPlanRepository.Query(model.ID, true).OrderByDescending(q => q.ID).FirstOrDefault();
                            if (internalPlanData == null)
                            {
                                ShowMessage(false, "無法結案 : 計畫表尚未核准");
                                acceptAskedClose = false;
                            }
                            else
                            {
                                var trainingPersonQuery = trainingPersonRepository.Query(true, "", model.ID);
                                if (model.GroupType == (int)GroupType.Person)
                                {
                                    var acceptanceQuery = internalAcceptanceRepository.Query(model.ID);
                                    if (acceptanceQuery.Count() < trainingPersonQuery.Count())
                                    {
                                        ShowMessage(false, "無法結案 : 學員驗收資料未補齊");
                                        acceptAskedClose = false;
                                    }
                                }
                                else
                                {
                                    string createrPersonId = "0000";
                                    var createrPersonInfo = adminRepository.GetById(model.Creater);
                                    if (createrPersonInfo != null)
                                    {
                                        createrPersonId = createrPersonInfo.Account;
                                    }
                                    if (string.IsNullOrEmpty(model.MainAcceptance) && internalAcceptanceRepository.FindByTrainingIdAndPersonId(model.ID, createrPersonId) == null)
                                    {
                                        ShowMessage(false, "無法結案 : 承辦人驗收資料未補齊");
                                        acceptAskedClose = false;
                                    }                                    
                                }
                                if (internalPlanData.Response)
                                {
                                    var opinionQuery = classOpinionRepository.Query(model.ID);
                                    if (opinionQuery.Count() < trainingPersonQuery.Count())
                                    {
                                        ShowMessage(false, "無法結案 : 學員意見回饋未補齊");
                                        acceptAskedClose = false;
                                    }
                                }
                            }

                            if (acceptAskedClose)
                            {
                                AskCloseTraining(model.ID, "Edit");
                            }
                        }
                    }
                }
                #endregion
                ShowMessage(true, "儲存成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "儲存失敗 : " + ex.Message);
            }
            if (model.Category == (int)Category.External)
            {
                return RedirectToAction("ExternalEdit", new { id = model.ID });
            }
            else
            {
                return RedirectToAction("Edit", new { id = model.ID });
            }
        }

        private bool CheckHasFile(FilesView ASSFile)
        {
            if (ASSFile == null && ASSFile.UploadFiles[0] == null)
            {
                return false;
            }

            return true;
        }

        public void SaveClosingFiles(int trainingId, string indexItem, string fileNameType, List<HttpPostedFileBase> UploadFiles, string saveFolder)
        {
            if (UploadFiles != null)
            {
                foreach (HttpPostedFileBase fileItem in UploadFiles)
                {
                    bool hasPic = ImageHelper.CheckFileExists(fileItem);
                    if (hasPic)
                    {
                        ClosingFile fileData = new ClosingFile();
                        fileData.TrainingID = trainingId;
                        fileData.IndexItem = indexItem;
                        fileData.FileNameType = fileNameType;
                        string fileName = fileItem.FileName;
                        var fileNameArr = fileName.Split('.');
                        string saveFileName = "";
                        for (int i = 0; i < fileNameArr.Count() - 1; i++)
                        {
                            saveFileName += fileNameArr[i];
                        }

                        fileData.FileName = ImageHelper.SaveFile(fileItem, saveFolder, trainingId + "-" + saveFileName);
                        closingFileRepository.Insert(fileData);
                    }
                    //else
                    //{
                    //    var closingFileData = closingFileRepository.FindFile(trainingId, indexItem, fileNameType);
                    //    if (closingFileData == null)
                    //    {
                    //        ClosingFile fileData = new ClosingFile();
                    //        fileData.TrainingID = trainingId;
                    //        fileData.IndexItem = indexItem;
                    //        fileData.FileNameType = fileNameType;
                    //        string fileName = fileItem.FileName;
                    //        var fileNameArr = fileName.Split('.');
                    //        string saveFileName = "";
                    //        for (int i = 0; i < fileNameArr.Count() - 1; i++)
                    //        {
                    //            saveFileName += fileNameArr[i];
                    //        }

                    //        fileData.FileName = "";
                    //        closingFileRepository.Insert(fileData);
                    //    }
                    //}
                }
            }
        }

        private void UpdateCloseAudit(TrainingView model)
        {
            var closeAuditInfo = new Audit();
            if (model.Category == (int)Category.External)
            {
                closeAuditInfo = auditRepository.FindByTarget(model.ID, (int)AuditType.CloseExternalTraining, model.ID);
            }
            if (model.Category == (int)Category.Internal)
            {
                closeAuditInfo = auditRepository.FindByTarget(model.ID, (int)AuditType.CloseInternalTraining, model.ID);
            }
            var trainingInfo = trainingRepository.GetById(model.ID);
            if (closeAuditInfo != null)
            {
                var oldStatus = closeAuditInfo.State;
                if (model.IsClose)
                {
                    closeAuditInfo.State = (int)AuditState.Approval;
                }
                else if (model.PresidentClose)
                {
                    closeAuditInfo.State = (int)AuditState.PresidentAudit;
                }
                else if (model.AskClose)
                {
                    closeAuditInfo.State = (int)AuditState.ViceAudit;
                }

                #region 通知結案審核人員、課程承辦人
                if (oldStatus != closeAuditInfo.State)
                {
                    string mailBody = "";
                    string mailTo = "";
                    mailTo = "i1o2i2o3i1@gmail.com";
                    var personInfo = new Models.T8.Join.GroupJoinPersonAndDepartment();
                    switch ((AuditState)closeAuditInfo.State)
                    {
                        case AuditState.ViceAudit:
                            var presidentAdmin = adminRepository.Query(true, 2).FirstOrDefault();
                            if (presidentAdmin != null)
                            {
                                personInfo = t8personRepository.FindByPersonId(presidentAdmin.Account);
                                mailTo = personInfo.Email;
                                mailBody = $"<h3>課程【{trainingInfo.Title}】已提交結案，請前往查看/審核</h3>";
                            }
                            break;
                        case AuditState.PresidentAudit:
                            var principalAdmin = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                            if (principalAdmin != null)
                            {
                                personInfo = t8personRepository.FindByPersonId(principalAdmin.Account);
                                mailTo = personInfo.Email;
                                mailBody = $"<h3>課程【{trainingInfo.Title}】已提交結案，請前往查看/審核</h3>";
                            }
                            break;
                        case AuditState.Approval:
                            var organiserAdmin = adminRepository.GetById(model.Creater);
                            if (organiserAdmin != null)
                            {
                                personInfo = t8personRepository.FindByPersonId(organiserAdmin.Account);
                                mailTo = personInfo.Email;
                                mailBody = $"<h3>課程【{trainingInfo.Title}】結案申請已通過，請前往查看/確認</h3>";
                            }
                            break;
                    }
                    //mailTo = personInfo.EMail;
                    string auditUrl = "";
                    switch ((AuditType)closeAuditInfo.Type)
                    {
                        case AuditType.CloseInternalTraining:
                            auditUrl = $"{Request.Url.Scheme}://{officialHost}/Admin/TrainingAdmin/Edit?id={closeAuditInfo.TargetID}";
                            break;
                        case AuditType.CloseExternalTraining:
                            auditUrl = $"{Request.Url.Scheme}://{officialHost}/Admin/TrainingAdmin/ExternalEdit?id={closeAuditInfo.TargetID}";
                            break;
                    }

                    mailBody += $"<p>前往察看 : <a href=\"{auditUrl}\">{auditUrl}</a></p>";

                    MailHelper.POP3Mail(mailTo, "【" + trainingInfo.Title + "】結案審核進度通知", mailBody);
                }
                #endregion
                auditRepository.Update(closeAuditInfo);
            }
        }

        /// <summary>
        /// 外訓建立編輯頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ExternalEdit(int id = 0, bool FromAudit = false)
        {
            var model = new TrainingView();
            model.FromAudit = FromAudit;
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (id != 0)
            {
                var query = trainingRepository.FindBy(id);
                model = Mapper.Map<TrainingView>(query);
                model.Department = departmentRepository.FindByDeptID(model.DeptID).ID;
                if (model.StartDate < Convert.ToDateTime("2010/01/01") || model.EndDate < Convert.ToDateTime("2010/01/01"))
                {
                    SetDefaultDate(model);
                }

                var notifyList = trainingPersonRepository.Query(null, "", id).Select(q => q.JobID).ToList();
                var employeeInfo = t8personRepository.GetPersonQuery().Where(q => notifyList.Contains(q.JobID)).ToList();
                model.Notify = Mapper.Map<List<EmployeeView>>(employeeInfo);

                model.SameDeptManager = false;
                if (adminInfo.Type == (int)AdminType.Manager)
                {
                    var personInfo = t8personRepository.FindByPersonId(adminRepository.GetById(adminInfo.ID).Account);
                    var personDept = personInfo == null ? "" : personInfo.DeptID;
                    var deptIdList = trainingPersonRepository.GetSignInInfoList(model.ID).Select(q => q.DeptID).Distinct().ToList();
                    model.SameDeptManager = deptIdList.Contains(personDept);
                }

                model.PicList = trainingImageRepository.Query(id).ToList();

                #region 團體驗收
                if (model.GroupType == (int)GroupType.Group)
                {
                    var EAQuery = externalAcceptanceRepository.Query(model.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (EAQuery != null)
                    {
                        model.EAInfo = Mapper.Map<ExternalAcceptanceView>(EAQuery);
                        var trainingPlan = externalPlanRepository.Query(model.ID, null).FirstOrDefault();
                        if (trainingPlan != null)
                        {
                            var inspList = trainingPlan.Inspection.Split(',');
                            model.EAInfo.HasReport = inspList.Contains("1");//心得報告
                            model.EAInfo.HasCertificate = inspList.Contains("2");//技能檢定/證書
                            model.EAInfo.HasDelegate = inspList.Contains("3");//內部轉授
                            model.EAInfo.HasActionPlan = inspList.Contains("4");//工作應用行動計畫
                            model.EAInfo.HasPerformance = inspList.Contains("5");//績效評估
                            model.EAInfo.HasOpinion = inspList.Contains("6");//滿意度調查
                            model.EAInfo.DelegateDate = (DateTime)trainingPlan.DelegateDate;
                        }
                    }
                }
                #endregion
            }
            else
            {
                SetDefaultDate(model);
                model.IsSignable = true;
                model.IsOnline = true;
                model.LateMinutes = 10;
            }

            model.Category = (int)Category.External;

            var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", model.ID).Where(q => (q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal) && !q.IsInvalid).OrderByDescending(q => q.ID).FirstOrDefault();
            model.AuditState = auditInfo == null ? 0 : auditInfo.State;
            var planExist = CheckPlanExist(model);
            model.IsAudit = planExist && auditInfo != null && model.ID != 0;
            if (!planExist)
            {
                model.AuditStatus = "尚未填寫";
                model.IsInvalid = false;
            }
            else if (planExist && auditInfo == null)
            {
                model.AuditStatus = "已填寫未送審";
                model.IsInvalid = false;
            }
            else
            {
                model.AuditState = auditInfo == null ? 0 : auditInfo.State;
                model.AuditStatus = EnumHelper.GetDescription((AuditState)model.AuditState);
                model.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
            }

            #region 承辦人
            var officer = adminRepository.GetById(model.Creater);
            if (officer != null)
            {
                var personInfo = t8personRepository.FindByPersonId(officer.Account);
                if (personInfo == null && model.Creater != 1)
                {
                    model.AuditStatus = "核准:人員離職";
                    model.IsInvalid = false;
                }
            }
            #endregion

            #region 結案文件
            var isFinished = (model.AuditState == (int)AuditState.Approval && model.AskClose) || model.IsInvalid;
            var isCreater = adminInfo.ID == model.Creater;
            //if (model.AskClose)
            //{
                SetClosingUpload(id, model, isFinished, isCreater);
            //}
            #endregion

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult ExternalEdit(TrainingView model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Training data = Mapper.Map<Training>(model);
                    var adminInfo = AdminInfoHelper.GetAdminInfo();
                    int adminId = adminInfo.ID;
                    if (modelState)
                    {
                        data.IsModel = true;
                    }                   

                    #region 檔案處理
                    //bool hasPic = ImageHelper.CheckFileExists(model.TraningPic);
                    //if (hasPic)
                    //{//照片
                    //    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    //    data.MainPic = ImageHelper.SaveFile(model.TraningPic, PhotoFolder);
                    //}

                    //bool hasFile = ;
                    //bool hasVideo = ;
                    //bool hasReport = ;
                    //bool hasAcceptance = ;

                    if (model.PicsFiles != null)
                    {
                        foreach (HttpPostedFileBase pic in model.PicsFiles)
                        {
                            bool hasPic = ImageHelper.CheckFileExists(pic);
                            if (hasPic)
                            {
                                TrainingImage fileData = new TrainingImage();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                fileData.TrainingID = model.ID;
                                fileData.Pics = ImageHelper.SaveFile(pic, PhotoFolder);
                                trainingImageRepository.Insert(fileData);
                            }
                        }
                    }
                    if (ImageHelper.CheckFileExists(model.TraningFile))
                    {//檔案
                        ImageHelper.DeleteFile(PhotoFolder, model.MainFile);
                        data.MainFile = ImageHelper.SaveFile(model.TraningFile, PhotoFolder);
                    }
                    if (ImageHelper.CheckFileExists(model.TraningVideo))
                    {//影片
                        ImageHelper.DeleteFile(PhotoFolder, model.MainVideo);
                        data.MainVideo = ImageHelper.SaveFile(model.TraningVideo, PhotoFolder);
                    }
                    if (ImageHelper.CheckFileExists(model.TraningReport))
                    {//報告
                        ImageHelper.DeleteFile(PhotoFolder, model.MainReport);
                        data.MainReport = ImageHelper.SaveFile(model.TraningReport, PhotoFolder);
                    }
                    if (ImageHelper.CheckFileExists(model.TraningAcceptance))
                    {//驗收
                        ImageHelper.DeleteFile(PhotoFolder, model.MainAcceptance);
                        data.MainAcceptance = ImageHelper.SaveFile(model.TraningAcceptance, PhotoFolder);
                    }
                    #endregion

                    if (model.ID == 0)
                    {
                        data.Creater = adminId;
                        data.MeetingNumber = data.StartDate.ToString("yyyyMmddHHmm");
                        data.AskClose = false;
                        data.IsClose = false;
                        model.ID = trainingRepository.Insert(data);
                        ShowMessage(true, "新增成功");
                    }
                    else
                    {
                        if (adminId != trainingRepository.FindBy(model.ID).Creater && adminInfo.Type > (int)AdminType.Manager)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return View(model);
                        }
                        trainingRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }

                    System.IO.Directory.CreateDirectory(Server.MapPath("/FileUploads/Qrcode") + "/" + model.ID);

                    #region 新增會議名單
                    //判斷會議存在
                    if (model.ID != 0)
                    {
                        //新名單
                        var employeeInfoList = t8personRepository.GetSelected(model.selectedNotifyList);
                        var newList = employeeInfoList.OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                        var newJoinedId = newList.Count() > 0 ? newList.Aggregate((a, b) => a + ", " + b) : "";
                        //原名單
                        var oldList = trainingPersonRepository.Query(null, "", model.ID).OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                        var oldJoinedId = oldList.Count() > 0 ? oldList.Aggregate((a, b) => a + ", " + b) : "";
                        bool isDiff = (oldJoinedId != newJoinedId && newList.Count() > 0);
                        if (isDiff)
                        {
                            try
                            {
                                //刪除原名單
                                var deletList = trainingPersonRepository.Query(null, "", model.ID).Where(q => !newList.Contains(q.JobID)).Select(q => q.ID);
                                foreach (var item in deletList)
                                {
                                    trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
                                    trainingPersonRepository.Delete(item);
                                }

                                //新增名單                   
                                var failedList = new List<string>();
                                foreach (var item in employeeInfoList.Where(q => !oldList.Contains(q.JobID)))
                                {
                                    string engName = string.IsNullOrEmpty(item.EngName) ? "" : " " + item.EngName;
                                    var result = trainingPersonRepository.Insert(new TrainingPerson()
                                    {
                                        SignCode = Convert.ToString(Guid.NewGuid()),
                                        TraningID = model.ID,
                                        JobID = item.JobID,
                                        Name = item.Name + engName,
                                        IsSign = false,
                                        SignStatus = (int)SignStatus.UnSigned,
                                        IsSent = false,
                                        IsPassed = false,
                                    });
                                    if (result == 0)
                                    {
                                        failedList.Add(item.JobID);
                                    }
                                    var personInfo = personRepository.GetByJobID(item.JobID);
                                }
                            }
                            catch (System.Exception e)
                            {

                                throw;
                            }
                        }
                    }
                    #endregion

                    #region 結案審核狀態
                    UpdateCloseAudit(model);
                    #endregion

                    if (model.SeriesParentID == 0)
                    {
                        bool existForm = externalPlanRepository.Query(model.ID).Count() > 0;
                        if (!existForm)
                        {
                            return RedirectToAction("External", "TrainingAdmin", new { trainingId = model.ID });
                        }
                    }
                }
                catch (Exception e)
                {
                    ShowMessage(false, e.Message);
                }

                return RedirectToAction("ExternalEdit", new { id = model.ID, FromAudit = model.FromAudit });
            }
            else
            {
                ShowMessage(false, ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage);
            }

            return View(model);
        }
        private static void SetDefaultDate(TrainingView model)
        {
            model.StartDate = DateTime.Now.AddDays(1);
            model.SignDate = model.StartDate.AddHours(-0.5);
            model.Hours = 2;
            model.EndDate = model.StartDate.AddHours(Convert.ToDouble(model.Hours));
        }
        private static void SetSeminarDefaultDate(SeminarTrainingView model)
        {
            model.StartDate = DateTime.Now.AddDays(1);
            model.SignDate = model.StartDate.AddHours(-0.5);
            model.Hours = 2;
            model.EndDate = model.StartDate.AddHours(Convert.ToDouble(model.Hours));
        }

        /// <summary>
        /// 內訓研討會建立編輯頁
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SeminarEdit(int id = 0, bool FromAudit = false)
        {
            var model = new SeminarTrainingView();
            model.FromAudit = FromAudit;
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (id != 0)
            {
                var query = trainingRepository.FindBy(id);
                model = Mapper.Map<SeminarTrainingView>(query);
                model.Department = departmentRepository.FindByDeptID(model.DeptID).ID;
                if (model.StartDate < Convert.ToDateTime("2010/01/01") || model.EndDate < Convert.ToDateTime("2010/01/01"))
                {
                    SetSeminarDefaultDate(model);
                }

                var notifyList = trainingPersonRepository.Query(null, "", id).Select(q => q.JobID).ToList();
                var employeeInfo = t8personRepository.GetPersonQuery().Where(q => notifyList.Contains(q.JobID)).ToList();
                model.Notify = Mapper.Map<List<EmployeeView>>(employeeInfo);

                model.SameDeptManager = false;
                if (adminInfo.Type == (int)AdminType.Manager)
                {
                    var personInfo = t8personRepository.FindByPersonId(adminRepository.GetById(adminInfo.ID).Account);
                    var personDept = personInfo == null ? "" : personInfo.DeptID;
                    var deptIdList = trainingPersonRepository.GetSignInInfoList(model.ID).Select(q => q.DeptID).Distinct().ToList();
                    model.SameDeptManager = deptIdList.Contains(personDept);
                }

                model.PicList = trainingImageRepository.Query(id).ToList();

                #region 計畫審核資料
                //var planInfo = seminarPlanRepository.Query(model.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                //if (planInfo.OfficerAuditDate != null)
                //{
                //    model.OfficerAuditDate = (DateTime)planInfo.OfficerAuditDate;
                //}
                //if (planInfo.ManagerAuditDate != null)
                //{
                //    model.ManagerAuditDate = (DateTime)planInfo.ManagerAuditDate;
                //}
                //if (planInfo.ViceAuditDate != null)
                //{
                //    model.ViceAuditDate = (DateTime)planInfo.ViceAuditDate;
                //}
                //if (planInfo.PrincipalAuditDate != null)
                //{
                //    model.PrincipalAuditDate = (DateTime)planInfo.PrincipalAuditDate;
                //}
                //if (planInfo.PresidentAuditDate != null)
                //{
                //    model.PresidentAuditDate = (DateTime)planInfo.PresidentAuditDate;
                //}

                //model.OfficerAudit = planInfo.OfficerAudit < AuditApproval ? AuditProcessing : planInfo.OfficerAudit;
                //model.OfficerRemark = planInfo.OfficerRemark;
                //model.OfficerSignature = planInfo.OfficerSignature;
                //model.ManagerAudit = planInfo.ManagerAudit < AuditApproval ? AuditProcessing : planInfo.ManagerAudit;
                //model.ManagerRemark = planInfo.ManagerRemark;
                //model.ManagerSignature = planInfo.ManagerSignature;
                //model.ViceAudit = planInfo.ViceAudit < AuditApproval ? AuditProcessing : planInfo.ViceAudit;
                //model.ViceRemark = planInfo.ViceRemark;
                //model.ViceSignature = planInfo.ViceSignature;
                //model.PresidentAudit = planInfo.PresidentAudit < AuditApproval ? AuditProcessing : planInfo.PresidentAudit;
                //model.PresidentRemark = planInfo.PresidentRemark;
                //model.PresidentSignature = planInfo.PresidentSignature;
                //model.PrincipalAudit = planInfo.PrincipalAudit < AuditApproval ? AuditProcessing : planInfo.PrincipalAudit;
                //model.PrincipalRemark = planInfo.PrincipalRemark;
                //model.PrincipalSignature = planInfo.PrincipalSignature;
                #endregion
            }
            else
            {
                SetSeminarDefaultDate(model);
                model.IsSignable = true;
                model.IsOnline = true;
                model.LateMinutes = 10;
            }

            model.Category = (int)Category.Seminar;

            //var auditInfo = auditRepository.Query(null, 0, (int)AuditType.Seminar, null, "", "", model.ID).OrderByDescending(q => q.ID).FirstOrDefault();
            //model.AuditState = auditInfo == null ? 0 : auditInfo.State;
            //var planExist = seminarPlanRepository.Query(model.ID).Count() > 0;
            //model.IsAudit = planExist && auditInfo != null && model.ID != 0;
            //if (!planExist)
            //{
            //    model.AuditStatus = "尚未填寫";
            //    model.IsInvalid = false;
            //}
            //else if (planExist && auditInfo == null)
            //{
            //    model.AuditStatus = "已填寫未送審";
            //    model.IsInvalid = false;
            //}
            //else
            //{
            //    model.AuditState = auditInfo == null ? 0 : auditInfo.State;
            //    model.AuditStatus = EnumHelper.GetDescription((AuditState)model.AuditState);
            //    model.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
            //}

            #region 承辦人
            //var officer = adminRepository.GetById(model.Creater);
            //if (officer != null)
            //{
            //    var personInfo = t8personRepository.FindByPersonId(officer.Account);
            //    if (personInfo == null && model.Creater != 1)
            //    {
            //        model.AuditStatus = "核准:人員離職";
            //        model.IsInvalid = false;
            //    }
            //}
            #endregion

            if (model.ID != 0)
            {
                SetSeminarViewData(FromAudit, model);
            }

            return View(model);
        }

        /// <summary>
        /// 研討會簽章資料
        /// </summary>
        /// <param name="FromAudit"></param>
        /// <param name="model"></param>
        private void SetSeminarViewData(bool FromAudit, SeminarTrainingView model)
        {
            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var officerPersonInfo = t8personRepository.FindByPersonId(adminRepository.GetById(model.Creater).Account);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                //if (adminRepository.FindByPersonId(officerPersonInfo.JobID).Type == (int)AdminType.Principal)
                if (adminRepository.FindByPersonId(officerPersonInfo.JobID).IsPrincipal)
                {
                    model.ManagerAudit = AuditProcessing;
                    auditLevelData.ViceReview = officerPersonInfo.JobID;
                    model.PresidentAudit = AuditProcessing;
                }
                model.AuditLevelInfo = auditLevelData;

                SetSignature(model, auditLevelData);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SeminarEdit(SeminarTrainingView model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Training data = Mapper.Map<Training>(model);
                    var adminInfo = AdminInfoHelper.GetAdminInfo();
                    int adminId = adminInfo.ID;

                    if (model.IsAudit)
                    {
                        model.OfficerAudit = (int)FormAudit.Approval;
                    }

                    #region 檔案處理
                    //bool hasPic = ImageHelper.CheckFileExists(model.TraningPic);
                    bool hasFile = ImageHelper.CheckFileExists(model.TraningFile);
                    bool hasVideo = ImageHelper.CheckFileExists(model.TraningVideo);
                    bool hasReport = ImageHelper.CheckFileExists(model.TraningReport);
                    bool hasAcceptance = ImageHelper.CheckFileExists(model.TraningAcceptance);
                    //if (hasPic)
                    //{//照片
                    //    ImageHelper.DeleteFile(PhotoFolder, model.MainPic);
                    //    data.MainPic = ImageHelper.SaveFile(model.TraningPic, PhotoFolder);
                    //}
                    if (model.PicsFiles != null)
                    {
                        foreach (HttpPostedFileBase pic in model.PicsFiles)
                        {
                            bool hasPic = ImageHelper.CheckFileExists(pic);
                            if (hasPic)
                            {
                                TrainingImage fileData = new TrainingImage();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                fileData.TrainingID = model.ID;
                                fileData.Pics = ImageHelper.SaveFile(pic, PhotoFolder);
                                trainingImageRepository.Insert(fileData);
                            }
                        }
                    }
                    if (hasFile)
                    {//檔案
                        ImageHelper.DeleteFile(PhotoFolder, model.MainFile);
                        data.MainFile = ImageHelper.SaveFile(model.TraningFile, PhotoFolder);
                    }
                    if (hasVideo)
                    {//影片
                        ImageHelper.DeleteFile(PhotoFolder, model.MainVideo);
                        data.MainVideo = ImageHelper.SaveFile(model.TraningVideo, PhotoFolder);
                    }
                    if (hasReport)
                    {//報告
                        ImageHelper.DeleteFile(PhotoFolder, model.MainReport);
                        data.MainReport = ImageHelper.SaveFile(model.TraningReport, PhotoFolder);
                    }
                    //if (hasAcceptance)
                    //{//驗收
                    //    ImageHelper.DeleteFile(PhotoFolder, model.MainAcceptance);
                    //    data.MainAcceptance = ImageHelper.SaveFile(model.TraningAcceptance, PhotoFolder);
                    //}

                    #endregion

                    if (model.ID == 0)
                    {
                        data.Creater = adminId;
                        data.MeetingNumber = data.StartDate.ToString("yyyyMmddHHmm");
                        data.AskClose = false;
                        data.IsClose = false;
                        model.ID = trainingRepository.Insert(data);
                        ShowMessage(true, "新增成功");
                    }
                    else
                    {
                        if (adminId != trainingRepository.FindBy(model.ID).Creater && adminInfo.Type > (int)AdminType.Manager)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return View(model);
                        }
                        trainingRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }

                    System.IO.Directory.CreateDirectory(Server.MapPath("/FileUploads/Qrcode") + "/" + model.ID);

                    #region 新增會議名單
                    //判斷會議存在
                    if (model.ID != 0)
                    {
                        //新名單
                        var employeeInfoList = t8personRepository.GetSelected(model.selectedNotifyList);
                        var newList = employeeInfoList.OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                        var newJoinedId = newList.Count() > 0 ? newList.Aggregate((a, b) => a + ", " + b) : "";
                        //原名單
                        var oldList = trainingPersonRepository.Query(null, "", model.ID).OrderBy(q => q.JobID).Select(q => q.JobID).ToList();
                        var oldJoinedId = oldList.Count() > 0 ? oldList.Aggregate((a, b) => a + ", " + b) : "";
                        bool isDiff = (oldJoinedId != newJoinedId && newList.Count() > 0);
                        if (isDiff)
                        {
                            try
                            {
                                //刪除原名單
                                var deletList = trainingPersonRepository.Query(null, "", model.ID).Where(q => !newList.Contains(q.JobID)).Select(q => q.ID);
                                foreach (var item in deletList)
                                {
                                    trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
                                    trainingPersonRepository.Delete(item);
                                }

                                //新增名單                   
                                var failedList = new List<string>();
                                foreach (var item in employeeInfoList.Where(q => !oldList.Contains(q.JobID)))
                                {
                                    string engName = string.IsNullOrEmpty(item.EngName) ? "" : " " + item.EngName;
                                    var result = trainingPersonRepository.Insert(new TrainingPerson()
                                    {
                                        SignCode = Convert.ToString(Guid.NewGuid()),
                                        TraningID = model.ID,
                                        JobID = item.JobID,
                                        Name = item.Name + engName,
                                        IsSign = false,
                                        SignStatus = (int)SignStatus.UnSigned,
                                        IsSent = false,
                                        IsPassed = false,
                                    });
                                    if (result == 0)
                                    {
                                        failedList.Add(item.JobID);
                                    }
                                    var personInfo = personRepository.GetByJobID(item.JobID);
                                }
                            }
                            catch (System.Exception e)
                            {

                                throw;
                            }
                        }
                    }
                    #endregion

                    #region 送審
                    if (model.IsAudit)
                    {
                        var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.Seminar).FirstOrDefault();

                        #region 簽核狀態
                        int state = (int)AuditState.OfficerAudit;
                        var planAuditInfo = seminarPlanRepository.Query(model.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                        if (planAuditInfo != null)
                        {
                            state = HandleState(model.CreaterDeptId, planAuditInfo.OfficerAudit, planAuditInfo.ManagerAudit, planAuditInfo.ViceAudit, planAuditInfo.PresidentAudit, planAuditInfo.PrincipalAudit);

                            #region 日期處理
                            planAuditInfo.OfficerAuditDate = SetAuditDate(model.OfficerAudit, model.OfficerAuditDate);
                            planAuditInfo.ManagerAuditDate = SetAuditDate(model.ManagerAudit, model.ManagerAuditDate);
                            planAuditInfo.ViceAuditDate = SetAuditDate(model.ViceAudit, model.ViceAuditDate);
                            planAuditInfo.PresidentAuditDate = SetAuditDate(model.PresidentAudit, model.PresidentAuditDate);
                            planAuditInfo.PrincipalAuditDate = SetAuditDate(model.PrincipalAudit, model.PrincipalAuditDate);
                            #endregion
                            planAuditInfo.OfficerAudit = model.OfficerAudit < AuditApproval ? AuditProcessing : model.OfficerAudit;
                            planAuditInfo.OfficerRemark = model.OfficerRemark;
                            planAuditInfo.OfficerSignature = model.OfficerSignature;
                            planAuditInfo.ManagerAudit = model.ManagerAudit < AuditApproval ? AuditProcessing : model.ManagerAudit;
                            planAuditInfo.ManagerRemark = model.ManagerRemark;
                            planAuditInfo.ManagerSignature = model.ManagerSignature;
                            planAuditInfo.ViceAudit = model.ViceAudit < AuditApproval ? AuditProcessing : model.ViceAudit;
                            planAuditInfo.ViceRemark = model.ViceRemark;
                            planAuditInfo.ViceSignature = model.ViceSignature;
                            planAuditInfo.PresidentAudit = model.PresidentAudit < AuditApproval ? AuditProcessing : model.PresidentAudit;
                            planAuditInfo.PresidentRemark = model.PresidentRemark;
                            planAuditInfo.PresidentSignature = model.PresidentSignature;
                            planAuditInfo.PrincipalAudit = model.PrincipalAudit < AuditApproval ? AuditProcessing : model.PrincipalAudit;
                            planAuditInfo.PrincipalRemark = model.PrincipalRemark;
                            planAuditInfo.PrincipalSignature = model.PrincipalSignature;
                            if (model.PrincipalAudit == Approved)
                            {
                                var trainingInfo = trainingRepository.GetById(planAuditInfo.TrainingID);
                                trainingInfo.IsApproved = true;
                                trainingRepository.Update(trainingInfo);
                            }
                            seminarPlanRepository.Update(planAuditInfo);
                        }
                        else
                        {
                            var planData = new SeminarPlan()
                            {
                                TrainingID = model.ID,
                                People = model.selectedNotifyList.Split(',').Count(),
                                Response = true,
                                CreateDate = DateTime.Now,
                                IsAudit = true,
                                OfficerAudit = AuditApproval,
                                OfficerRemark = model.OfficerRemark,
                                OfficerSignature = model.OfficerSignature,
                                OfficerAuditDate = SetAuditDate(model.OfficerAudit, model.OfficerAuditDate),
                                ManagerAudit = AuditProcessing,
                                ManagerRemark = "",
                                ManagerSignature = "",
                                ManagerAuditDate = null,
                                ViceAudit = AuditProcessing,
                                ViceRemark = "",
                                ViceSignature = "",
                                ViceAuditDate = null,
                                PrincipalAudit = AuditProcessing,
                                PrincipalRemark = "",
                                PrincipalSignature = "",
                                PrincipalAuditDate = null,
                                PresidentAudit = AuditProcessing,
                                PresidentRemark = "",
                                PresidentSignature = "",
                                PresidentAuditDate = null,
                            };
                            seminarPlanRepository.Insert(planData);
                        }
                        #endregion
                        if (auditInfo == null)
                        {
                            var auditId = auditRepository.Insert(new Audit()
                            {
                                TrainingID = model.ID,
                                PersonID = "",
                                TargetID = model.ID,
                                Type = (int)AuditType.Seminar,
                                State = state,
                                IsInvalid = false,
                                Officer = model.Creater,
                                CreateDate = DateTime.Now
                            });
                        }
                        else
                        {
                            auditInfo.State = state;
                            auditRepository.Update(auditInfo);
                        }

                    }
                    #endregion

                }
                catch (Exception e)
                {
                    model.OfficerAudit = (int)FormAudit.Processing;
                    ShowMessage(false, e.Message);
                }

                return RedirectToAction("SeminarEdit", new { id = model.ID });
            }
            else
            {
                model.IsAudit = false;
                //ShowMessage(false, "部分資料缺失/錯誤，儲存失敗。");
                ShowMessage(false, ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage);
            }

            return View(model);
        }

        /// <summary>
        /// 外訓計畫表
        /// </summary>
        /// <param name="trainingId"></param>
        /// <returns></returns>
        public ActionResult External(int trainingId = 0, bool isNew = false, bool FromAudit = false)
        {
            if (trainingId == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }

            var model = new ExternalPlanView();
            var query = externalPlanRepository.Query(trainingId).OrderByDescending(q => q.ID).FirstOrDefault();
            if (query != null)
            {//舊資料
                model = Mapper.Map<ExternalPlanView>(query);
                model.HolderList = planHolderRepository.Query(model.TrainingID).ToList();
                model.DocList = functionDocRepository.Query("LT" + model.TrainingID.ToString("D6")).ToList();
                if (isNew && model.IsAudit)
                {//取消送審/重新編輯
                    var newData = Mapper.Map<ExternalPlanView>(query);
                    var oldAudit = auditRepository.Query(null, query.ID, (int)AuditType.External, null, "", "", query.TrainingID).FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        //重置審核資料
                        newData.AuditInfo = model.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.External, query.TrainingID);
                    }
                    SetAuditHelper.SetDefault(newData);
                    ExternalPlan data = Mapper.Map<ExternalPlan>(newData);

                    newData.ID = externalPlanRepository.Insert(data);
                    newData = Mapper.Map<ExternalPlanView>(externalPlanRepository.Query(trainingId).OrderByDescending(q => q.ID).FirstOrDefault());

                    var existList = externalCourseRepository.Query(model.TrainingID, model.ID).ToList();
                    foreach (var item in existList)
                    {
                        item.ExternalID = newData.ID;
                        externalCourseRepository.Update(item);
                    }

                    var trainingInfo = trainingRepository.GetById(data.TrainingID);
                    trainingInfo.IsApproved = false;
                    trainingRepository.Update(trainingInfo);

                    SetExternalViewData(FromAudit, newData);

                    return View(newData);
                }
            }
            else
            {//新建資料
                externalPlanRepository.Insert(new ExternalPlan()
                {
                    TrainingID = trainingId,
                    Objecive = "",
                    Reason = "",
                    Trained = 1,
                    Trainee = "",
                    RewardType = 1,
                    Reward = "",
                    Inspection = "6",
                    DelegateDate = DateTime.Now,
                    ActionPlan = "",
                    Contract = false,
                    CreateDate = DateTime.Now,
                    ManagerAudit = AuditProcessing,
                    ViceAudit = AuditProcessing,
                    OfficerAudit = AuditProcessing,
                    PrincipalAudit = AuditProcessing,
                    PresidentAudit = AuditProcessing,
                });

                model = Mapper.Map<ExternalPlanView>(externalPlanRepository.Query(trainingId).FirstOrDefault());
            }

            SetExternalViewData(FromAudit, model);

            return View(model);
        }

        /// <summary>
        /// 外訓簽章資料
        /// </summary>
        /// <param name="FromAudit"></param>
        /// <param name="model"></param>
        private void SetExternalViewData(bool FromAudit, ExternalPlanView model)
        {
            model.CourseList = Mapper.Map<List<ExternalCourseView>>(externalCourseRepository.Query(model.TrainingID, model.ID));
            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var officerPersonInfo = t8personRepository.FindByPersonId(adminRepository.GetById(model.TrainingInfo.Creater).Account);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                //if (adminRepository.FindByPersonId(officerPersonInfo.JobID).Type == (int)AdminType.Principal)
                if (adminRepository.FindByPersonId(officerPersonInfo.JobID).IsPrincipal)
                {
                    model.ManagerAudit = AuditProcessing;
                    auditLevelData.ViceReview = officerPersonInfo.JobID;
                    model.PresidentAudit = AuditProcessing;
                }
                model.AuditLevelInfo = auditLevelData;
                SetSignature(model, auditLevelData);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult External(ExternalPlanView model)
        {
            if (model.IsSkip)
            {
                ShowMessage(true, "建立完成");
                return RedirectToAction("ExternalEdit", new { id = model.TrainingID });
            }
            ExternalPlan data = Mapper.Map<ExternalPlan>(model);
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            int adminId = adminInfo.ID;

            if (model.IsAudit)
            {
                data.OfficerAudit = model.OfficerAudit = (int)FormAudit.Approval;
            }

            #region 簽核人員
            var trainingInfo = trainingRepository.GetById(data.TrainingID);
            if (adminInfo.Type < (int)AdminType.ClassSupervisor)
            {
                if (data.IsAudit && string.IsNullOrEmpty(data.OfficerRemark))
                {
                    ModelState.AddModelError("OfficerRemark", "送審需填寫審核備註 !");
                }
                switch (adminInfo.Type)
                {
                    case (int)AdminType.Sysadmin:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.ManagerAudit = data.OfficerAudit;
                            data.ManagerAuditDate = data.OfficerAuditDate;
                            data.ManagerRemark = data.OfficerRemark;
                            data.ManagerSignature = data.OfficerSignature;
                        }
                        if (data.ManagerAudit != AuditProcessing && string.IsNullOrEmpty(data.ManagerRemark))
                        {
                            ModelState.AddModelError("ManagerRemark", "需填寫審核備註 !");
                        }
                        break;
                    case (int)AdminType.Manager:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.ManagerAudit = data.OfficerAudit;
                            data.ManagerAuditDate = data.OfficerAuditDate;
                            data.ManagerRemark = data.OfficerRemark;
                            data.ManagerSignature = data.OfficerSignature;
                        }
                        if (data.ManagerAudit != AuditProcessing && string.IsNullOrEmpty(data.ManagerRemark))
                        {
                            ModelState.AddModelError("ManagerRemark", "需填寫審核備註 !");
                        }
                        break;
                    case (int)AdminType.Vice:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.ViceAudit = data.OfficerAudit;
                            data.ViceAuditDate = data.OfficerAuditDate;
                            data.ViceRemark = data.OfficerRemark;
                            data.ViceSignature = data.OfficerSignature;
                        }
                        if (data.ViceAudit != AuditProcessing && string.IsNullOrEmpty(data.ViceRemark))
                        {
                            ModelState.AddModelError("ViceRemark", "需填寫審核備註 !");
                        }
                        break;
                    case (int)AdminType.Principal:
                        //data.PrincipalAudit = data.OfficerAudit;
                        //data.PrincipalAuditDate = data.OfficerAuditDate;
                        //data.PrincipalRemark = data.OfficerRemark;
                        //data.PrincipalSignature = data.OfficerSignature;
                        //if (data.PrincipalAudit != AuditProcessing && string.IsNullOrEmpty(data.PrincipalRemark))
                        //{
                        //    ModelState.AddModelError("PrincipalRemark", "需填寫審核備註 !");
                        //}
                        break;
                    case (int)AdminType.President:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.PresidentAudit = data.OfficerAudit;
                            data.PresidentAuditDate = data.OfficerAuditDate;
                            data.PresidentRemark = data.OfficerRemark;
                            data.PresidentSignature = data.OfficerSignature;
                        }
                        if (data.PresidentAudit != AuditProcessing && string.IsNullOrEmpty(data.PresidentRemark))
                        {
                            ModelState.AddModelError("PresidentRemark", "需填寫審核備註 !");
                        }
                        break;
                    default:
                        if (data.OfficerAudit != AuditProcessing && string.IsNullOrEmpty(data.OfficerRemark))
                        {
                            ModelState.AddModelError("OfficerRemark", "需填寫審核備註 !");
                        }
                        break;
                }
            }
            if (adminRepository.GetById(adminInfo.ID).IsPrincipal)
            {
                if (data.PrincipalAudit != AuditProcessing && string.IsNullOrEmpty(data.PrincipalRemark))
                {
                    ModelState.AddModelError("PrincipalRemark", "需填寫審核備註 !");
                }
            }
            #endregion
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.HoldersFiles != null)
                    {//上傳多檔
                        foreach (HttpPostedFileBase pic in model.HoldersFiles)
                        {
                            bool hasFile = ImageHelper.CheckFileExists(pic);
                            if (hasFile)
                            {
                                PlanHolder fileData = new PlanHolder();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                fileData.TrainingID = model.TrainingID;
                                fileData.FileName = ImageHelper.SaveFile(pic, PhotoFolder, pic.FileName.Split('.')[0]);
                                fileData.Type = 1;
                                planHolderRepository.Insert(fileData);
                            }
                        }
                    }

                    #region 日期處理
                    data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                    data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                    data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                    data.PrincipalAuditDate = SetAuditDate(data.PrincipalAudit, data.PrincipalAuditDate);
                    data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                    #endregion

                    #region 簽章
                    //承辦人
                    var officerInfo = adminRepository.GetById(model.TrainingInfo.Creater);
                    var officerPersonInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                    if (data.OfficerAudit > AuditProcessing)
                    {
                        if (officerPersonInfo != null)
                        {
                            data.OfficerSignature = officerInfo.Account + ".jpg";
                        }
                        else
                        {
                            data.OfficerSignature = adminSignature;
                        }
                        //int intCK;
                        //if (!int.TryParse(officerInfo.Account, out intCK))
                        //{
                        //    data.OfficerSignature = adminSignature;
                        //}
                        //else
                        //{
                        //    data.OfficerSignature = officerInfo.Account + ".jpg";
                        //}
                    }
                    if (officerPersonInfo != null)
                    {
                        var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                        //主管
                        if (data.ManagerAudit > AuditProcessing && string.IsNullOrEmpty(data.ManagerSignature))
                        {
                            data.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                        }
                        //副總
                        if (data.ViceAudit > AuditProcessing && string.IsNullOrEmpty(data.ViceSignature))
                        {
                            data.ViceSignature = auditLevelData.ViceReview + ".jpg";
                        }
                        //總經理
                        if (data.PresidentAudit > AuditProcessing && string.IsNullOrEmpty(data.PresidentSignature))
                        {
                            data.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                        }
                        //校長
                        if (data.PrincipalAudit > AuditProcessing && string.IsNullOrEmpty(data.PrincipalSignature))
                        {
                            data.PrincipalSignature = auditLevelData.PrincipalReview + ".jpg";
                        }
                    }
                    else
                    {
                        data.ManagerSignature = adminSignature;
                        data.ViceSignature = adminSignature;
                        //校長
                        if (data.PrincipalAudit > AuditProcessing)
                        {
                            //var principal = adminRepository.Query(true, (int)AdminType.Principal).FirstOrDefault();
                            var principal = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                            data.PrincipalSignature = principal == null ? emptySignature : principal.Account + ".jpg";
                        }
                        //總經理
                        if (data.PresidentAudit > AuditProcessing)
                        {
                            var president = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                            data.PresidentSignature = president == null ? emptySignature : president.Account + ".jpg";
                        }
                    }

                    #endregion

                    if (model.DocFiles != null)
                    {
                        foreach (HttpPostedFileBase file in model.DocFiles)
                        {
                            bool hasFile = ImageHelper.CheckFileExists(file);
                            if (hasFile)
                            {
                                FunctionDoc docData = new FunctionDoc();
                                docData.PersonId = "LT" + model.TrainingID.ToString("D6");
                                docData.FileName = ImageHelper.SaveFile(file, PhotoFolder.Replace("TrainingPhoto", "ManagerPhoto"), docData.PersonId + "-" + file.FileName.Split('.')[0]);
                                docData.Type = 2;
                                functionDocRepository.Insert(docData);
                            }
                        }
                    }

                    if (model.ID == 0)
                    {
                        data.CreateDate = DateTime.Now;
                        model.ID = externalPlanRepository.Insert(data);
                        ShowMessage(true, "新增成功");
                    }
                    else
                    {
                        if (adminId != trainingInfo.Creater && adminInfo.Type > (int)AdminType.Manager)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return RedirectToAction("External", new { trainingId = model.TrainingID, FromAudit = model.FromAudit });
                        }
                        if (model.IsAbnormal && data.IsAudit)
                        {
                            data.IsAbnormal = false;
                        }
                        if (model.PrincipalAudit == Approved)
                        {
                            trainingInfo.IsApproved = true;
                            trainingRepository.Update(trainingInfo);
                        }
                        externalPlanRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }

                    #region 關聯課程
                    if (!string.IsNullOrEmpty(model.JsonCourse))
                    {
                        var jsonCourse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ExternalCourse>>(model.JsonCourse);
                        if (model.TrainingID != 0 && model.ID != 0)
                        {
                            var oldCourseQuery = externalCourseRepository.Query(model.TrainingID, model.ID);
                            var newCourseIdList = new List<int>();
                            foreach (var item in jsonCourse)
                            {
                                if (item.ID == 0)
                                {
                                    int newId = externalCourseRepository.Insert(new ExternalCourse()
                                    {
                                        ExternalID = model.ID,
                                        TrainingID = model.TrainingID,
                                        Organizer = item.Organizer,
                                        Lecturer = item.Lecturer,
                                        CourseMaterial = item.CourseMaterial,
                                        Address = item.Address,
                                        StartDate = item.StartDate,
                                        EndDate = item.EndDate,
                                        Registery = item.Registery,
                                        Travel = item.Travel,
                                        Others = item.Others,
                                        Total = item.Total,
                                        IsOnline = true,
                                        Device = "",
                                        DeviceCheck = true
                                    });
                                    newCourseIdList.Add(newId);
                                }
                                else
                                {
                                    var courseInfo = Mapper.Map<ExternalCourse>(item);
                                    courseInfo.IsOnline = true;
                                    externalCourseRepository.Update(courseInfo);
                                    newCourseIdList.Add(item.ID);
                                }

                            }
                            var offLineList = oldCourseQuery.Where(q => !newCourseIdList.Contains(q.ID)).ToList();
                            foreach (var item in offLineList)
                            {
                                var disableData = externalCourseRepository.GetById(item.ID);
                                disableData.IsOnline = false;
                                externalCourseRepository.Update(disableData);
                                //externalCourseRepository.Delete(item.ID);
                            }
                        }
                    }
                    #endregion

                    #region 送審
                    if (model.OfficerAudit == (int)FormAudit.Approval && model.IsAudit)
                    {
                        var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.External).FirstOrDefault();

                        #region 簽核狀態
                        int state = HandleState(model.TrainingInfo.CreaterDeptId, data.OfficerAudit, data.ManagerAudit, data.ViceAudit, data.PresidentAudit, data.PrincipalAudit);
                        #endregion

                        if (data.OfficerAudit == AuditReject || data.ManagerAudit == AuditReject || data.PrincipalAudit == AuditReject || data.ViceAudit == AuditReject || data.PresidentAudit == AuditReject)
                        {
                            auditInfo.IsInvalid = true;

                            var personInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                            //駁回通知承辦人
                            string mailBody = "";
                            mailBody = $"<h3>您所送審的【{trainingInfo.Title}】外訓計畫表已駁回，請前往查看</h3>";

                            string linkUrl = $"{Request.Url.Scheme}://{officialHost}/InternalAcceptanceAdmin/Edit/{model.ID}?tid={trainingInfo.ID}&FromAudit=false";

                            mailBody += $"<p>前往察看 : <a href=\"{linkUrl}\">{linkUrl}</a></p>";

                            //MailHelper.POP3Mail("i1o2i2o3i1@gmail.com", "【" + model.TrainingInfo.Title + "】已被駁回", mailBody);
                            MailHelper.POP3Mail(personInfo.Email, "【" + model.TrainingInfo.Title + "】的計畫表已被駁回", mailBody);
                        }
                        if (auditInfo == null)
                        {
                            var auditId = auditRepository.Insert(new Audit()
                            {
                                TrainingID = data.TrainingID,
                                PersonID = "",
                                TargetID = data.ID,
                                Type = (int)AuditType.External,
                                State = state,
                                IsInvalid = false,
                                Officer = model.TrainingInfo.Creater,
                                CreateDate = DateTime.Now
                            });
                        }
                        else
                        {
                            auditInfo.State = state;
                            auditRepository.Update(auditInfo);
                        }
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    ShowMessage(false, e.Message);
                }

            }
            else
            {
                ShowMessage(false, ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage);
            }

            return RedirectToAction("External", new { trainingId = model.TrainingID, FromAudit = model.FromAudit });
        }

        /// <summary>
        /// 內訓計畫表
        /// </summary>
        /// <param name="trainingId"></param>
        /// <returns></returns>
        public ActionResult Internal(int trainingId = 0, bool isNew = false, bool FromAudit = false)
        {
            if (trainingId == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }

            var model = new InternalPlanView();
            var query = internalPlanRepository.Query(trainingId).OrderByDescending(q => q.ID).FirstOrDefault();
            if (query != null)
            {//舊資料
                model = Mapper.Map<InternalPlanView>(query);
                model.HolderList = planHolderRepository.Query(model.TrainingID).ToList();
                model.DocList = functionDocRepository.Query("LT" + model.TrainingID.ToString("D6")).ToList();
                if (isNew && model.IsAudit)
                {//取消送審/重新編輯
                    var newData = Mapper.Map<InternalPlanView>(query);
                    var oldAudit = auditRepository.Query(null, query.ID, (int)AuditType.Internal, null, "", "", query.TrainingID).FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        //重置審核資料
                        newData.AuditInfo = model.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.External, query.TrainingID);
                    }
                    SetAuditHelper.SetDefault(newData);
                    InternalPlan data = Mapper.Map<InternalPlan>(newData);

                    newData.ID = internalPlanRepository.Insert(data);
                    newData = Mapper.Map<InternalPlanView>(internalPlanRepository.Query(trainingId).OrderByDescending(q => q.ID).FirstOrDefault());

                    var existList = internalCourseRepository.Query(model.TrainingID, model.ID).ToList();
                    foreach (var item in existList)
                    {
                        var courseData = internalCourseRepository.GetById(item.ID);
                        courseData.InternalID = newData.ID;
                        internalCourseRepository.Update(courseData);
                    }

                    var trainingInfo = trainingRepository.GetById(data.TrainingID);
                    trainingInfo.IsApproved = false;
                    trainingRepository.Update(trainingInfo);

                    SetInternalViewData(FromAudit, newData);

                    return View(newData);
                }
            }
            else
            {//新建資料
                int totalPeople = trainingPersonRepository.Query(null, "", trainingId).Count();
                var trainingInfo = trainingRepository.GetById(trainingId);
                internalPlanRepository.Insert(new InternalPlan()
                {
                    TrainingID = trainingId,
                    Investigation = "",
                    Education = "",
                    Experience = "",
                    Objective = "",
                    Address = "",
                    CreateDate = DateTime.Now,
                    ManagerAudit = AuditProcessing,
                    ViceAudit = AuditProcessing,
                    OfficerAudit = AuditProcessing,
                    PrincipalAudit = AuditProcessing,
                    PresidentAudit = AuditProcessing,
                    Response = true,
                    People = totalPeople,
                    Devices = trainingInfo.Device
                });

                model = Mapper.Map<InternalPlanView>(internalPlanRepository.Query(trainingId).FirstOrDefault());
            }

            //model = Mapper.Map<InternalPlanView>(internalPlanRepository.Query(trainingId).FirstOrDefault());
            SetInternalViewData(FromAudit, model);

            return View(model);
        }

        /// <summary>
        /// 內訓簽章資料
        /// </summary>
        /// <param name="FromAudit"></param>
        /// <param name="model"></param>
        private void SetInternalViewData(bool FromAudit, InternalPlanView model)
        {
            model.CourseList = Mapper.Map<List<InternalCourseView>>(internalCourseRepository.Query(model.TrainingID, model.ID));
            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var officerAdminInfo = adminRepository.GetById(model.TrainingInfo.Creater);
            var officerPersonInfo = t8personRepository.FindByPersonId(officerAdminInfo.Account, true);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                //if (adminRepository.FindByPersonId(officerPersonInfo.JobID).Type == (int)AdminType.Principal)
                if (adminRepository.FindByPersonId(officerPersonInfo.JobID).IsPrincipal)
                {
                    model.ManagerAudit = AuditProcessing;
                    auditLevelData.ViceReview = officerPersonInfo.JobID;
                    model.PresidentAudit = AuditProcessing;
                }
                model.AuditLevelInfo = auditLevelData;

                SetSignature(model, auditLevelData);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Internal(InternalPlanView model)
        {
            if (model.IsSkip)
            {
                ShowMessage(true, "建立完成");
                return RedirectToAction("Edit", new { id = model.TrainingID });
            }
            InternalPlan data = Mapper.Map<InternalPlan>(model);
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            int adminId = adminInfo.ID;

            if (model.IsAudit)
            {
                data.OfficerAudit = model.OfficerAudit = (int)FormAudit.Approval;
            }
            #region 簽核人員
            var trainingInfo = trainingRepository.GetById(data.TrainingID);
            if (adminInfo.Type < (int)AdminType.ClassSupervisor)
            {
                if (data.IsAudit && string.IsNullOrEmpty(data.OfficerRemark))
                {
                    ModelState.AddModelError("OfficerRemark", "送審需填寫審核備註 !");
                }
                switch (adminInfo.Type)
                {
                    case (int)AdminType.Sysadmin:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.ManagerAudit = data.OfficerAudit;
                            data.ManagerAuditDate = data.OfficerAuditDate;
                            data.ManagerRemark = data.OfficerRemark;
                            data.ManagerSignature = data.OfficerSignature;
                        }
                        if (data.ManagerAudit != AuditProcessing && string.IsNullOrEmpty(data.ManagerRemark))
                        {
                            ModelState.AddModelError("ManagerRemark", "需填寫審核備註 !");
                        }
                        break;
                    case (int)AdminType.Manager:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.ManagerAudit = data.OfficerAudit;
                            data.ManagerAuditDate = data.OfficerAuditDate;
                            data.ManagerRemark = data.OfficerRemark;
                            data.ManagerSignature = data.OfficerSignature;
                        }
                        if (data.ManagerAudit != AuditProcessing && string.IsNullOrEmpty(data.ManagerRemark))
                        {
                            ModelState.AddModelError("ManagerRemark", "需填寫審核備註 !");
                        }
                        break;
                    case (int)AdminType.Vice:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.ViceAudit = data.OfficerAudit;
                            data.ViceAuditDate = data.OfficerAuditDate;
                            data.ViceRemark = data.OfficerRemark;
                            data.ViceSignature = data.OfficerSignature;
                        }
                        if (data.ViceAudit != AuditProcessing && string.IsNullOrEmpty(data.ViceRemark))
                        {
                            ModelState.AddModelError("ViceRemark", "需填寫審核備註 !");
                        }
                        break;
                    case (int)AdminType.Principal:
                        //data.PrincipalAudit = data.OfficerAudit;
                        //data.PrincipalAuditDate = data.OfficerAuditDate;
                        //data.PrincipalRemark = data.OfficerRemark;
                        //data.PrincipalSignature = data.OfficerSignature;
                        //if (data.PrincipalAudit != AuditProcessing && string.IsNullOrEmpty(data.PrincipalRemark))
                        //{
                        //    ModelState.AddModelError("PrincipalRemark", "需填寫審核備註 !");
                        //}
                        break;
                    case (int)AdminType.President:
                        if (trainingInfo.Creater == adminInfo.ID)
                        {
                            data.PresidentAudit = data.OfficerAudit;
                            data.PresidentAuditDate = data.OfficerAuditDate;
                            data.PresidentRemark = data.OfficerRemark;
                            data.PresidentSignature = data.OfficerSignature;
                        }
                        if (data.PresidentAudit != AuditProcessing && string.IsNullOrEmpty(data.PresidentRemark))
                        {
                            ModelState.AddModelError("PresidentRemark", "需填寫審核備註 !");
                        }
                        break;
                    default:
                        if (data.OfficerAudit != AuditProcessing && string.IsNullOrEmpty(data.OfficerRemark))
                        {
                            ModelState.AddModelError("OfficerRemark", "需填寫審核備註 !");
                        }
                        break;
                }
            }
            if (adminRepository.GetById(adminInfo.ID).IsPrincipal)
            {
                if (data.PrincipalAudit != AuditProcessing && string.IsNullOrEmpty(data.PrincipalRemark))
                {
                    ModelState.AddModelError("PrincipalRemark", "需填寫審核備註 !");
                }
            }
            #endregion
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.HoldersFiles != null)
                    {//上傳多檔
                        foreach (HttpPostedFileBase pic in model.HoldersFiles)
                        {
                            bool hasFile = ImageHelper.CheckFileExists(pic);
                            if (hasFile)
                            {
                                PlanHolder fileData = new PlanHolder();
                                //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                                fileData.TrainingID = model.TrainingID;
                                fileData.FileName = ImageHelper.SaveFile(pic, PhotoFolder);
                                fileData.Type = 2;
                                planHolderRepository.Insert(fileData);
                            }
                        }
                    }
                    if (model.OfficerAudit == 2 && model.IsAudit)
                    {
                        var holderFilesCount = planHolderRepository.Query(model.TrainingID).Count();
                        if (holderFilesCount < 1 || string.IsNullOrEmpty(model.Stakeholders))
                        {
                            ModelState.AddModelError("Stakeholders", "需填寫及上傳利益關係資料才可送審 !");
                            ShowMessage(false, ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage);
                            return RedirectToAction("Internal", new { trainingId = model.TrainingID, FromAudit = model.FromAudit });
                        }
                    }

                    #region 日期處理
                    data.OfficerAuditDate = SetAuditDate(data.OfficerAudit, data.OfficerAuditDate);
                    data.ManagerAuditDate = SetAuditDate(data.ManagerAudit, data.ManagerAuditDate);
                    data.ViceAuditDate = SetAuditDate(data.ViceAudit, data.ViceAuditDate);
                    data.PrincipalAuditDate = SetAuditDate(data.PrincipalAudit, data.PrincipalAuditDate);
                    data.PresidentAuditDate = SetAuditDate(data.PresidentAudit, data.PresidentAuditDate);
                    #endregion

                    #region 簽章
                    //承辦人
                    var officerInfo = adminRepository.GetById(model.TrainingInfo.Creater);
                    var officerPersonInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                    if (data.OfficerAudit > 1)
                    {
                        if (officerPersonInfo != null)
                        {
                            data.OfficerSignature = officerInfo.Account + ".jpg";
                        }
                        else
                        {
                            data.OfficerSignature = adminSignature;
                        }
                        //int intCK;
                        //if (!int.TryParse(officerInfo.Account, out intCK))
                        //{
                        //    data.OfficerSignature = adminSignature;
                        //}
                        //else
                        //{
                        //    data.OfficerSignature = officerInfo.Account + ".jpg";
                        //}
                    }
                    if (officerPersonInfo != null)
                    {
                        var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                        //主管
                        if (data.ManagerAudit > AuditProcessing && string.IsNullOrEmpty(data.ManagerSignature))
                        {
                            data.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                        }
                        //副總
                        if (data.ViceAudit > AuditProcessing && string.IsNullOrEmpty(data.ViceSignature))
                        {
                            data.ViceSignature = auditLevelData.ViceReview + ".jpg";
                        }
                        //總經理
                        if (data.PresidentAudit > AuditProcessing && string.IsNullOrEmpty(data.PresidentSignature))
                        {
                            data.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                        }
                        //校長
                        if (data.PrincipalAudit > AuditProcessing && string.IsNullOrEmpty(data.PrincipalSignature))
                        {
                            data.PrincipalSignature = auditLevelData.PrincipalReview + ".jpg";
                        }
                    }
                    else
                    {
                        data.ManagerSignature = adminSignature;
                        data.ViceSignature = adminSignature;
                        //校長
                        if (data.PrincipalAudit > 1)
                        {
                            //var principal = adminRepository.Query(true, (int)AdminType.Principal).FirstOrDefault();
                            var principal = adminRepository.Query(true, 0, "", true).FirstOrDefault();
                            data.PrincipalSignature = principal == null ? emptySignature : principal.Account + ".jpg";
                        }
                        //總經理
                        if (data.PresidentAudit > 1)
                        {
                            var president = adminRepository.Query(true, (int)AdminType.President).FirstOrDefault();
                            data.PresidentSignature = president == null ? emptySignature : president.Account + ".jpg";
                        }
                    }
                    #endregion

                    if (model.DocFiles != null)
                    {
                        foreach (HttpPostedFileBase file in model.DocFiles)
                        {
                            bool hasFile = ImageHelper.CheckFileExists(file);
                            if (hasFile)
                            {
                                FunctionDoc docData = new FunctionDoc();
                                docData.PersonId = "LT" + model.TrainingID.ToString("D6");
                                docData.FileName = ImageHelper.SaveFile(file, PhotoFolder.Replace("TrainingPhoto", "ManagerPhoto"), docData.PersonId + "-" + file.FileName.Split('.')[0]);
                                docData.Type = 3;
                                functionDocRepository.Insert(docData);
                            }
                        }
                    }

                    if (model.ID == 0)
                    {
                        data.CreateDate = DateTime.Now;
                        model.ID = internalPlanRepository.Insert(data);
                        ShowMessage(true, "新增成功");
                    }
                    else
                    {
                        if (adminId != trainingInfo.Creater && adminInfo.Type > (int)AdminType.Manager)
                        {
                            ShowMessage(false, "儲存失敗，無修改權限");
                            return RedirectToAction("Internal", new { trainingId = model.TrainingID, FromAudit = model.FromAudit });
                        }
                        if (model.IsAbnormal && data.IsAudit)
                        {
                            data.IsAbnormal = false;
                        }
                        if (model.PrincipalAudit == Approved)
                        {
                            trainingInfo.IsApproved = true;
                            trainingRepository.Update(trainingInfo);
                        }
                        internalPlanRepository.Update(data);
                        ShowMessage(true, "修改成功");
                    }

                    #region 關聯課程
                    if (!string.IsNullOrEmpty(model.JsonCourse))
                    {
                        var jsonCourse = Newtonsoft.Json.JsonConvert.DeserializeObject<List<InternalCourse>>(model.JsonCourse);
                        if (model.TrainingID != 0 && model.ID != 0)
                        {
                            var oldCourseQuery = internalCourseRepository.Query(model.TrainingID, model.ID);
                            var newCourseIdList = new List<int>();
                            foreach (var item in jsonCourse)
                            {
                                if (item.ID == 0)
                                {
                                    int newId = internalCourseRepository.Insert(new InternalCourse()
                                    {
                                        InternalID = model.ID,
                                        TrainingID = model.TrainingID,
                                        Outline = item.Outline,
                                        Date = item.Date,
                                        Time = item.Time,
                                        Hours = item.Hours,
                                        Progress = item.Progress,
                                        Lecturer = item.Lecturer,
                                        TrainingMethod = item.TrainingMethod,
                                        IsOnline = true,
                                        Device = "",
                                        DeviceCheck = true
                                    });
                                    newCourseIdList.Add(newId);
                                }
                                else
                                {
                                    var courseInfo = Mapper.Map<InternalCourse>(item);
                                    courseInfo.IsOnline = true;
                                    internalCourseRepository.Update(courseInfo);
                                    newCourseIdList.Add(item.ID);
                                }
                            }
                            var offLineList = oldCourseQuery.Where(q => !newCourseIdList.Contains(q.ID)).ToList();
                            foreach (var item in offLineList)
                            {
                                var disableData = internalCourseRepository.GetById(item.ID);
                                disableData.IsOnline = false;
                                internalCourseRepository.Update(disableData);
                                //internalCourseRepository.Delete(item.ID);
                            }
                        }
                    }
                    #endregion

                    #region 送審
                    if (model.OfficerAudit == 2 && model.IsAudit)
                    {                       
                        var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.Internal).FirstOrDefault();

                        #region 簽核狀態
                        int state = HandleState(model.TrainingInfo.CreaterDeptId, data.OfficerAudit, data.ManagerAudit, data.ViceAudit, data.PresidentAudit, data.PrincipalAudit);
                        #endregion

                        if (data.OfficerAudit == AuditReject || data.ManagerAudit == AuditReject || data.PrincipalAudit == AuditReject || data.ViceAudit == AuditReject || data.PresidentAudit == AuditReject)
                        {
                            auditInfo.IsInvalid = true;

                            var personInfo = t8personRepository.FindByPersonId(officerInfo.Account);
                            //駁回通知承辦人
                            string mailBody = "";
                            mailBody = $"<h3>您所送審的【{trainingInfo.Title}】內訓計畫表已駁回，請前往查看</h3>";

                            string linkUrl = $"{Request.Url.Scheme}://{officialHost}/InternalAcceptanceAdmin/Edit/{model.ID}?tid={trainingInfo.ID}&FromAudit=True";

                            mailBody += $"<p>前往察看 : <a href=\"{linkUrl}\">{linkUrl}</a></p>";

                            //MailHelper.POP3Mail("i1o2i2o3i1@gmail.com", "【" + model.TrainingInfo.Title + "】已被駁回", mailBody);
                            MailHelper.POP3Mail(personInfo.Email, "【" + model.TrainingInfo.Title + "】的計畫表已被駁回", mailBody);
                        }
                        if (auditInfo == null)
                        {
                            var auditId = auditRepository.Insert(new Audit()
                            {
                                TrainingID = data.TrainingID,
                                PersonID = "",
                                TargetID = data.ID,
                                Type = (int)AuditType.Internal,
                                State = state,
                                IsInvalid = false,
                                Officer = model.TrainingInfo.Creater,
                                CreateDate = DateTime.Now
                            });
                        }
                        else
                        {
                            auditInfo.State = state;
                            auditRepository.Update(auditInfo);
                        }
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    model.OfficerAudit = (int)FormAudit.Processing;
                    ShowMessage(false, e.Message);
                }

            }
            else
            {
                ShowMessage(false, ModelState.Values.Where(q => q.Errors.Count() > 0).FirstOrDefault().Errors[0].ErrorMessage);
            }
            return RedirectToAction("Internal", new { trainingId = model.TrainingID, FromAudit = model.FromAudit });
        }

        /// <summary>
        /// 使用者參與的課後表單
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Participate(TrainingIndexView model)
        {
            if (string.IsNullOrEmpty(model.PersonID))
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            var personInfo = t8personRepository.FindByPersonId(model.PersonID);
            model.PersonName = personInfo == null ? "" : personInfo.Name;
            var query = trainingRepository.ParticipateQuery(model.TrainingID, model.Title, model.PersonID);
            var pageResult = query.ToPageResult<Training>(model);
            model.PageResult = Mapper.Map<PageResult<TrainingView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                item.DeptName = departmentRepository.FindByDeptID(item.DeptID).Title;

                #region 參與部門
                var signInInfo = trainingPersonRepository.GetSignInInfoList(item.ID).Select(q => q.DeptName).Distinct().ToList();
                item.DepartmentListStr = signInInfo.Count() > 0 ? signInInfo.Aggregate((a, b) => a + ", " + b) : "";
                #endregion

                item.HasAcceptance = false;
                //驗收審核狀態
                if (item.Category == (int)Category.External)
                {
                    var EAAudit = auditRepository.Query(null, 0, (int)AuditType.ExternalAcceptance, null, "", model.PersonID, item.ID).FirstOrDefault();
                    if (EAAudit != null)
                    {
                        if (EAAudit.IsInvalid)
                        {
                            item.EAStatus = "駁回";
                        }
                        else
                        {
                            item.EAStatus = EnumHelper.GetDescription((AuditState)EAAudit.State);
                        }
                    }
                    else
                    {
                        ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());
                        item.EAStatus = externalAcceptanceRepository.Query(item.ID, model.PersonID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                    }

                    var externalPlan = externalPlanRepository.Query(item.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (externalPlan != null)
                    {
                        item.HasAcceptance = !string.IsNullOrEmpty(externalPlan.Inspection.Replace(",", "").Replace("6", "")) && item.GroupType == (int)GroupType.Person;
                        item.HasOpinion = externalPlan.Inspection.IndexOf("6") >= 0;
                    }
                }
                else
                {
                    var IAAudit = auditRepository.Query(null, 0, (int)AuditType.InternalAcceptance, null, "", model.PersonID, item.ID).FirstOrDefault();
                    if (IAAudit != null)
                    {
                        if (IAAudit.IsInvalid)
                        {
                            item.IAStatus = "駁回";
                        }
                        else
                        {
                            item.IAStatus = EnumHelper.GetDescription((AuditState)IAAudit.State);
                        }
                    }
                    else
                    {
                        InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
                        item.IAStatus = internalAcceptanceRepository.Query(item.ID, model.PersonID).Count() > 0 ? "已填寫未送審" : "尚未填寫";
                    }

                    var internalPlan = internalPlanRepository.Query(item.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (internalPlan != null)
                    {
                        item.HasAcceptance = (/*internalPlan.Learning ||*/ internalPlan.Behavior || internalPlan.Outcome) && item.GroupType == (int)GroupType.Person;
                        item.HasOpinion = internalPlan.Response;
                    }
                }
                if (item.HasOpinion)
                {
                    ClassOpinionRepository classOpinionRepository = new ClassOpinionRepository(new MeetingSignInDBEntities());
                    item.OpinionStatus = classOpinionRepository.Query(item.ID, model.PersonID).Count() > 0 ? "已填寫" : "尚未填寫";
                }
                else
                {
                    item.OpinionStatus = "無需填寫";
                }
                if (item.StartDate < publishDate)
                {
                    item.IAStatus = "無需填寫";
                    item.EAStatus = "無需填寫";
                    item.OpinionStatus = "無需填寫";
                }

                var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", item.ID).Where(q => q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal).OrderByDescending(q => q.ID).FirstOrDefault();
                if (!CheckPlanExist(item))
                {
                    item.AuditStatus = "尚未填寫";
                    item.IsInvalid = false;
                }
                else if (CheckPlanExist(item) && auditInfo == null)
                {
                    item.AuditStatus = "已填寫未送審";
                    item.IsInvalid = false;
                }
                else
                {
                    item.AuditState = auditInfo == null ? 0 : auditInfo.State;
                    item.AuditStatus = EnumHelper.GetDescription((AuditState)item.AuditState);
                    item.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                }

                #region 承辦人
                var officer = adminRepository.GetById(model.Creater);
                if (officer != null)
                {
                    personInfo = t8personRepository.FindByPersonId(officer.Account);
                    if (personInfo == null && item.Creater != 1)
                    {
                        item.AuditStatus = "核准:人員離職";
                        item.IsInvalid = false;
                    }
                }
                #endregion
            }

            return View(model);
        }

        /// <summary>
        /// 結案請求
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult AskCloseTraining(int id, string page = "Edit")
        {
            var query = trainingRepository.GetById(id);
            if (query != null)
            {
                query.AskClose = true;
                trainingRepository.Update(query);

                int state = (int)AuditState.ViceAudit;
                var officerInfo = adminRepository.GetById(query.Creater);
                auditRepository.Insert(new Audit
                {
                    TrainingID = id,
                    PersonID = officerInfo == null ? "" : officerInfo.Account,
                    TargetID = id,
                    Type = page == "Edit" ? (int)AuditType.CloseInternalTraining : (int)AuditType.CloseExternalTraining,
                    State = state,
                    IsInvalid = false,
                    Officer = query.Creater,
                    CreateDate = DateTime.Now,
                });
            }
            ShowMessage(true, "已送出結案請求");

            return RedirectToAction(page, new { id = id });
        }

        #region Delete
        public ActionResult Delete(int id)
        {
            //刪除
            //var imageList = trainingImageRepository.Query(id).ToList();
            //foreach (var item in imageList)
            //{
            //    string file = trainingImageRepository.DeletePic(item.ID);
            //    ImageHelper.DeleteFile(PhotoFolder, file);
            //}
            //trainingRepository.Delete(id);

            //停用
            var data = trainingRepository.GetById(id);
            data.IsOnline = false;
            trainingRepository.Update(data);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public ActionResult DeletePic(int id, int trainingId, string editPage)
        {
            string file = trainingImageRepository.DeletePic(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction(editPage, new { id = trainingId });
        }

        public ActionResult DeleteClosingFile(int id, int trainingId, string editPage)
        {
            var fileData = closingFileRepository.GetById(id);
            string ClosingFileFolder = Path.Combine(Server.MapPath(FileUploads), "ClosingFile");
            ImageHelper.DeleteFile(ClosingFileFolder, fileData.FileName);
            closingFileRepository.Delete(id);

            ShowMessage(true, "刪除成功");

            return RedirectToAction(editPage, new { id = trainingId });

            //return Json(fileData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteVideo(int id)
        {
            string file = trainingRepository.DeleteVideo(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult DeleteReport(int id)
        {
            string file = trainingRepository.DeleteReport(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult DeleteAcceptance(int id)
        {
            string file = trainingRepository.DeleteAcceptance(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult DeleteFile(int id)
        {
            string file = trainingRepository.DeleteFile(id);
            ImageHelper.DeleteFile(PhotoFolder, file);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult DeleteHolderFile(int id)
        {
            var data = planHolderRepository.GetById(id);
            ImageHelper.DeleteFile(PhotoFolder, data.FileName);
            planHolderRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            if (data.Type == 1)
            {
                return RedirectToAction("External", new { trainingId = data.TrainingID });
            }
            else
            {
                return RedirectToAction("Internal", new { trainingId = data.TrainingID });
            }
        }
        #endregion

        #region Ajax
        /// <summary>
        /// 取得費用概覽課程資料
        /// </summary>
        /// <param name="factory"></param>
        /// <returns></returns>
        public JsonResult GetTrainingFeeList(string department, int theme, DateTime StartDate, DateTime EndDate)
        {
            var trainingIdList = trainingRepository.Query("", StartDate, EndDate, "", department, 0, 0, 0, null, null, 0, false, theme).Select(q => q.ID).ToList();
            var externalTrainingList = Mapper.Map<List<ExternalTrainingModel>>(externalCourseRepository.Query().Where(q => q.Total > 0 && trainingIdList.Contains(q.TrainingID)));
            foreach (var item in externalTrainingList)
            {
                var trainingInfo = trainingRepository.GetById(item.TrainingID);
                if (trainingInfo != null)
                {
                    var trainingPersonQuery = trainingPersonRepository.Query(true, "", trainingInfo.ID, "", "", 1);
                    item.Title = trainingInfo.Title;
                    item.Organizer = item.Organizer;
                    item.Lecturer = item.Lecturer;
                    item.CourseDate = trainingInfo.StartDate.ToString("yyyy年MM月dd日 HH:mm");
                    item.Registery = item.Registery;
                    item.Travel = item.Travel;
                    item.Others = item.Others;
                    item.Total = item.Total;
                    item.SignedCount = trainingPersonQuery.Count();
                }
            }
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(externalTrainingList.Where(q => !q.Title.Contains("測試")).ToList(), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得課程出席率資料
        /// </summary>
        /// <param name="factory"></param>
        /// <returns></returns>
        public JsonResult GetExacutiveData(string type, string deptId, string department, DateTime startDate, DateTime endDate)
        {
            //取出要匯出Excel的資料
            //var query = trainingPersonRepository.GetExacutiveList(deptId, department, startDate, endDate);
            var query = trainingRepository.Query("", startDate, endDate, deptId, department).Where(q => q.Category < 3);
            List<TrainingExacutiveModel> trainingExacutiveList = new List<TrainingExacutiveModel>();
            foreach (var item in query)
            {
                TrainingExacutiveModel data = new TrainingExacutiveModel();
                data.ID = item.ID;
                data.Title = item.Title;
                data.StartDate = item.StartDate;
                data.EndDate = item.EndDate;
                var trainingPersonQuery = trainingPersonRepository.Query(null, "", item.ID);
                data.TotalNum = trainingPersonQuery.Count();
                switch (type)
                {
                    case "attend":
                        data.SignedNum = trainingPersonQuery.Where(q => q.SignStatus == 1).Count();
                        data.LeaveNum = trainingPersonQuery.Where(q => q.SignStatus == 2).Count();
                        data.UnSignNum = trainingPersonQuery.Where(q => q.SignStatus == 1).Count();
                        break;
                    case "opinion":
                        data.OpinionFilledNum = classOpinionRepository.Query(item.ID).Count();
                        break;
                    case "acceptance":
                        if (item.Category == (int)Category.External)
                        {
                            var acceptanceQuery = externalAcceptanceRepository.Query(item.ID);
                            data.AccptanceFilledNum = acceptanceQuery.Count();
                        }
                        else if (item.Category == (int)Category.Internal)
                        {
                            var acceptanceQuery = internalAcceptanceRepository.Query(item.ID);
                            data.AccptanceFilledNum = acceptanceQuery.Count();
                        }
                        break;
                    case "quiz":
                        if (item.Category == (int)Category.External)
                        {
                            var acceptanceQuery = externalAcceptanceRepository.Query(item.ID);
                            data.QuizCompleteNum = acceptanceQuery.Count();
                        }
                        else if (item.Category == (int)Category.Internal)
                        {
                            var acceptanceQuery = internalAcceptanceRepository.Query(item.ID);
                            data.QuizCompleteNum = acceptanceQuery.Where(q => q.IsPassed == true).Count();
                        }
                        break;
                }

                trainingExacutiveList.Add(data);
            }
          
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(query, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 取得廠區會議室資料
        /// </summary>
        /// <param name="factory"></param>
        /// <returns></returns>
        public JsonResult GetRooms(int factory)
        {
            var query = roomRepository.Query(factory, "", true);
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(query.ToList(), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得課別員工資料
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        //public JsonResult GetEmployee(string deptId)
        public JsonResult GetEmployee(int department)
        {
            var deptId = departmentRepository.FindBy(department).DeptID;
            var query = t8personRepository.GetPersonQuery("", "", deptId);
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(query.ToList(), JsonRequestBehavior.AllowGet);
        }

        public string CheckRoomStatus(int room, DateTime start, DateTime end, int excmid)
        {
            string usedStr = "該會議室於此時段已被預約囉，請重新選擇時間或更換會議室，謝謝。";
            if (trainingRepository.Query("", start, end, "", "", 0, room, excmid, 0).Where(q => q.FactoryID != 9).Count() > 0)
            {
                return usedStr;
            }
            MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());
            if (meetingRepository.Query("", start, end, 0, 0, room, excmid).Where(q => q.FactoryID != 9).Count() > 0)
            {
                return usedStr;
            }
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return "可選會議室";
        }

        /// <summary>
        /// 取得教育訓練頁統計資料
        /// </summary>
        /// <param name="type"></param>
        /// <param name="trainingId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetStatistics(string deptId, string department, DateTime startDate, DateTime endDate)
        {
            var trainingStatistics = new StatisticsView();

            trainingStatistics.Overall = new OverallStatisticsView();
            var overallTrainingQuery = trainingRepository.Query("", startDate, endDate, "", "", 0, 0, 0, null, null, 0, modelState).Where(q => q.Category != 3);
            var overallTrainingIdList = overallTrainingQuery.Select(q => q.ID).ToList();
            trainingStatistics.Overall.TrainingNum = overallTrainingIdList.Count();//辦訓課程總數
            var overallTrainingPersonQuery = trainingPersonRepository.GetAll().Where(q => overallTrainingIdList.Contains(q.ID));
            trainingStatistics.Overall.VisitorsNum = overallTrainingPersonQuery.Count();//辦訓總時數
            trainingStatistics.Overall.TrainingHours = Convert.ToInt32(overallTrainingQuery.Sum(q => q.Hours));//辦訓總時數
            var overallTraineesIdList = overallTrainingPersonQuery.Select(q => q.JobID).Distinct();
            trainingStatistics.Overall.TraineesNum = overallTraineesIdList.Count();//受訓總人數
            trainingStatistics.Overall.SeniorNum = overallTraineesIdList.Where(q => seniorList.Contains(q)).Count();//高階主管
            trainingStatistics.Overall.JuniorNum = overallTraineesIdList.Where(q => juniorList.Contains(q)).Count();//中低階主管
            var overallStatistics = trainingStatistics.Overall;
            trainingStatistics.Overall.EmployeeNum = overallStatistics.TraineesNum - (overallStatistics.SeniorNum + overallStatistics.JuniorNum);//非管理職員工
            trainingStatistics.Overall.YoungNum = 0;//青年(29歲(含)以下)受訓總人數

            trainingStatistics.Internal = new InternalStatisticsView();
            var internalTrainingQuery = trainingRepository.Query("", startDate, endDate, "", "", 0, 0, 0, null, null, 0, modelState).Where(q => q.Category == 1);
            var internalTrainingIdList = internalTrainingQuery.Select(q => q.ID).ToList();
            trainingStatistics.Internal.TrainingNum = internalTrainingIdList.Count();
            var internalTrainingPersonQuery = trainingPersonRepository.GetAll().Where(q => overallTrainingIdList.Contains(q.ID));
            trainingStatistics.Internal.VisitorsNum = internalTrainingPersonQuery.Count();
            trainingStatistics.Internal.TrainingHours = Convert.ToInt32(internalTrainingQuery.Sum(q => q.Hours));
            var internalTraineesIdList = internalTrainingPersonQuery.Select(q => q.JobID).Distinct();
            trainingStatistics.Internal.TraineesNum = internalTraineesIdList.Count();//受訓總人數
            trainingStatistics.Internal.SeniorNum = internalTraineesIdList.Where(q => seniorList.Contains(q)).Count();//高階主管
            trainingStatistics.Internal.JuniorNum = internalTraineesIdList.Where(q => juniorList.Contains(q)).Count();//中低階主管
            var internalStatistics = trainingStatistics.Internal;
            trainingStatistics.Internal.EmployeeNum = internalStatistics.TraineesNum - (internalStatistics.SeniorNum + internalStatistics.JuniorNum);//非管理職員工
            trainingStatistics.Internal.TotalFee = 0;//內部訓練總花費

            trainingStatistics.External = new ExternalStatisticsView();
            var externalTrainingQuery = trainingRepository.Query("", startDate, endDate, "", "", 0, 0, 0, null, null, 0, modelState).Where(q => q.Category == 2 || q.Category == 4);
            var externalTrainingIdList = externalTrainingQuery.Select(q => q.ID).ToList();
            trainingStatistics.External.TrainingNum = externalTrainingIdList.Count();
            var externalTrainingPersonQuery = trainingPersonRepository.GetAll().Where(q => overallTrainingIdList.Contains(q.ID));
            trainingStatistics.External.VisitorsNum = externalTrainingPersonQuery.Count();
            trainingStatistics.External.TrainingHours = Convert.ToInt32(externalTrainingQuery.Sum(q => q.Hours));
            var externalTraineesIdList = externalTrainingPersonQuery.Select(q => q.JobID).Distinct();
            trainingStatistics.External.TraineesNum = externalTraineesIdList.Count();//受訓總人數
            trainingStatistics.External.SeniorNum = externalTraineesIdList.Where(q => seniorList.Contains(q)).Count();//高階主管
            trainingStatistics.External.JuniorNum = externalTraineesIdList.Where(q => juniorList.Contains(q)).Count();//中低階主管
            var externalStatistics = trainingStatistics.External;
            trainingStatistics.External.EmployeeNum = externalStatistics.TraineesNum - (externalStatistics.SeniorNum + externalStatistics.JuniorNum);//非管理職員工
            var planIdList = externalPlanRepository.GetAll().Where(q => externalTrainingIdList.Contains(q.TrainingID)).GroupBy(q => q.TrainingID).SelectMany(q => q).Select(q => q.ID).ToList();
            var courseSum = Convert.ToInt32(externalCourseRepository.GetAll().Where(q => planIdList.Contains(q.ExternalID)).Sum(q => q.Total));
            trainingStatistics.External.TotalFee = courseSum;//外部訓練總花費
            return Json(trainingStatistics, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得教育訓練頁圖表資料
        /// </summary>
        /// <param name="type"></param>
        /// <param name="trainingId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetTrainingChart(string type, int trainingId)
        {
            var query = trainingPersonRepository.GetNumAndPercentage(trainingId, type).ToList();

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得概覽頁圖表資料
        /// </summary>
        /// <param name="type"></param>
        /// <param name="deptId"></param>
        /// <param name="department"></param>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetChart(string type, string deptId, string department, DateTime StartDate, DateTime EndDate)
        {
            var query = trainingRepository.GetNumAndPercentage(StartDate, EndDate, deptId, department, type, modelState);
            switch (type)
            {
                case "Category":
                    foreach (var data in query)
                    {
                        data.Title = EnumHelper.GetDescription((Category)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Theme":
                    foreach (var data in query)
                    {
                        data.Title = GetThemeTitle(Convert.ToInt32(data.Title));
                    }
                    break;
                case "Fee":
                    query = query.OrderBy(q => q.Title).ToList();
                    foreach (var data in query)
                    {
                        data.Title = Convert.ToDouble(data.Title).ToString("#,##.##");
                    }
                    break;
                case "Hours":
                    query = query.OrderBy(q => q.DoubleTitle).ToList();
                    foreach (var data in query)
                    {
                        data.Title = Convert.ToDouble(data.Title).ToString();
                    }
                    break;
                //case "Exacutive":
                //    query = query.OrderBy(q => q.DoubleTitle).ToList();
                //    foreach (var data in query)
                //    {
                //        if (true)
                //        {
                //            data.Title = Convert.ToDouble(data.Title).ToString();
                //        }                      
                //    }
                //    break;
            }

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPDDROChart(int id, string type)
        {
            var query = trainingRepository.GetPDDRONumAndPercentage(id, type);
            switch (type)
            {
                case "Compatibility":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Architecture":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Schedule":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "CourseMaterials":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Device":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Professional":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Expression":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "TimeControl":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "TeachingMethod":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((SatisfyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Recommended":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((AdmitRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "ActualUsage":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((AdmitRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "CurrentUsage":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((AdmitRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "FutureUsage":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((AdmitRadio)Convert.ToInt32(data.Title));
                    }
                    break;
                case "Difficulty":
                    foreach (var data in query)
                    {
                        data.Title = data.Title + EnumHelper.GetDescription((DifficultyRadio)Convert.ToInt32(data.Title));
                    }
                    break;
            }

            return Json(query.OrderBy(q => q.Title), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddInternalCourse(int trainingId, int internalId, string outline, DateTime date, string time, float hours, string progress, string lecturer, string method)
        {
            var Data = new InternalCourse()
            {
                TrainingID = trainingId,
                InternalID = internalId,
                Outline = outline,
                Date = date,
                Time = time,
                Hours = Convert.ToDecimal(hours),
                Progress = progress,
                Lecturer = lecturer,
                TrainingMethod = method,
                IsOnline = true,
            };
            var id = internalCourseRepository.Insert(Data);
            var data = internalCourseRepository.GetById(id);
            if (data != null)
            {
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        #endregion

        #region 匯出Excel
        /// <summary>
        /// 匯出Excel清單 (非同步)
        /// </summary>
        /// <returns></returns>
        public ActionResult ExportListExcel(DateTime StartDate, DateTime EndDate, string Title = "", int FactoryID = 0, int RoomID = 0, string DeptID = "", string DepartmentStr = "", bool IsClose = false)
        {
            //取出要匯出Excel的資料
            var query = trainingRepository.Query(Title, StartDate, EndDate, DeptID, DepartmentStr, FactoryID, RoomID, 0, 0, IsClose, 0, modelState);
            List<TrainingView> trainingList = Mapper.Map<IEnumerable<TrainingView>>(query).ToList();

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("課程清單");

                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //第1列是標題列 
                //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                //取得做法可參考最後的參考連結。

                int row = 1;    //列:橫的
                //sheet.Cells[row, col++].Value = "編號";
                sheet.Cells[row, col++].Value = "課程名稱";
                sheet.Cells[row, col++].Value = "發起人";
                sheet.Cells[row, col++].Value = "訓練單位";
                sheet.Cells[row, col++].Value = "上課日期";
                sheet.Cells[row, col++].Value = "訓練類別";
                sheet.Cells[row, col++].Value = "研訓類別";
                sheet.Cells[row, col++].Value = "時數(小時)";
                sheet.Cells[row, col++].Value = "費用";
                sheet.Cells[row, col++].Value = "講師";
                sheet.Cells[row, col++].Value = "證書/證明";
                sheet.Cells[row, col++].Value = "14. 講義/照片/影片";
                sheet.Cells[row, col++].Value = "17a. 意見表";
                sheet.Cells[row, col++].Value = "17b. 驗收資料";
                //sheet.Cells[row, col++].Value = "計畫表審核狀態";
                sheet.Cells[row, col++].Value = "驗收層級";
                sheet.Cells[row, col++].Value = "備註";
                sheet.Cells[row, col++].Value = "姓名";
                sheet.Cells[row, col++].Value = "部門";
                sheet.Cells[row, col++].Value = "是否合格";
                row++;

                //迴圈讀取訓練資料
                //資料從第2列開始               
                foreach (var tr in trainingList)
                {
                    string startDateStr = tr.StartDate.ToString("yyyy/MM/dd HH:mm");

                    var trainingPersonList = trainingPersonRepository.GetSignInInfoList(tr.ID);

                    string dept = departmentRepository.FindByDeptID(tr.DeptID).Title;
                    string category = EnumHelper.GetDescription((Category)Convert.ToInt32(tr.Category));
                    string theme = GetThemeTitle(Convert.ToInt32(tr.Theme));

                    string certStr = tr.Cert.ToString();
                    var certInfo = tr.CertOptions.Where(q => q.Value == certStr).FirstOrDefault();

                    var picQuery = trainingImageRepository.Query(tr.ID);
                    string hasFiles = (!string.IsNullOrEmpty(tr.MainReport) || !string.IsNullOrEmpty(tr.MainVideo) || picQuery.Count() > 0) ? "Yes" : "No";

                    //#region 審核狀態
                    //var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", tr.ID).Where(q => q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal && !q.IsInvalid).OrderByDescending(q => q.ID).FirstOrDefault();
                    //bool planExist = CheckPlanExist(tr);
                    //string auditStatus = "";
                    //if (!planExist)
                    //{
                    //    auditStatus = "尚未填寫";
                    //}
                    //else if (planExist && auditInfo == null)
                    //{
                    //    auditStatus = "已填寫未送審";
                    //}
                    //else
                    //{
                    //    tr.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                    //    if (tr.IsInvalid)
                    //    {
                    //        auditStatus = "已駁回";
                    //    }
                    //    else if (tr.AuditState == (int)MeetSignIn.Models.AuditState.PresidentAudit)
                    //    {
                    //        auditStatus = "已核准";
                    //    }
                    //    else
                    //    {
                    //        tr.AuditState = auditInfo == null ? 0 : auditInfo.State;
                    //        auditStatus = EnumHelper.GetDescription((AuditState)tr.AuditState);
                    //    }
                    //}
                    //#endregion

                    string acceptanceLevel = "L0";
                    string hasOpinion = "No";
                    var iplan = internalPlanRepository.Query(tr.ID, null).OrderBy(q => q.ID).FirstOrDefault();
                    var ePlan = new ExternalPlan();
                    if (iplan == null)
                    {
                        ePlan = externalPlanRepository.Query(tr.ID, null).OrderBy(q => q.ID).FirstOrDefault();
                        if (ePlan.Inspection.Contains("6"))
                        {
                            acceptanceLevel = "L1";
                            hasOpinion = "Yes";
                        }
                        if (ePlan.Inspection.Contains("1") || ePlan.Inspection.Contains("2"))
                        {
                            acceptanceLevel = "L2";
                        }
                        if (ePlan.Inspection.Contains("4"))
                        {
                            acceptanceLevel = "L3";
                        }
                        if (ePlan.Inspection.Contains("5"))
                        {
                            acceptanceLevel = "L4";
                        }
                    }
                    else
                    {
                        if (iplan.Response)
                        {
                            acceptanceLevel = "L1";
                            hasOpinion = "Yes";
                        }
                        if (iplan.Learning)
                        {
                            acceptanceLevel = "L2";
                        }
                        if (iplan.Behavior)
                        {
                            acceptanceLevel = "L3";
                        }
                        if (iplan.Outcome)
                        {
                            acceptanceLevel = "L4";
                        }
                    }

                    string remark = "";
                    if (!string.IsNullOrEmpty(tr.Remark))
                    {
                        remark = System.Text.RegularExpressions.Regex.Replace(tr.Remark, "<[^>]+>", "");
                        remark = System.Text.RegularExpressions.Regex.Replace(remark, "&[^;]+;", "");
                    }
                    foreach (var tp in trainingPersonList)
                    {
                        var tpInfo = trainingPersonRepository.FindByJobID(tp.PersonId);

                        col = 1;//每換一列，欄位要從1開始
                        //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                        //sheet.Cells[row, col++].Value = tr.ID;
                        //sheet.Cells[row, col++].Value = tr.ID;
                        sheet.Cells[row, col++].Value = tr.Title;
                        sheet.Cells[row, col++].Value = tr.Organiser;
                        sheet.Cells[row, col++].Value = dept;
                        sheet.Cells[row, col++].Value = startDateStr;
                        sheet.Cells[row, col++].Value = category;
                        sheet.Cells[row, col++].Value = theme;
                        sheet.Cells[row, col++].Value = tr.Hours;
                        sheet.Cells[row, col++].Value = tr.Fee;
                        sheet.Cells[row, col++].Value = tr.Lecturer;
                        sheet.Cells[row, col++].Value = certInfo == null ? "無" : certInfo.Text;
                        sheet.Cells[row, col++].Value = hasFiles;
                        sheet.Cells[row, col++].Value = hasOpinion;
                        sheet.Cells[row, col++].Value = (acceptanceLevel != "L0" && acceptanceLevel != "L1") ? "Yes" : "No";
                        //sheet.Cells[row, col++].Value = auditStatus;
                        sheet.Cells[row, col++].Value = acceptanceLevel;
                        sheet.Cells[row, col++].Value = remark;
                        sheet.Cells[row, col++].Value = tp.Name;
                        sheet.Cells[row, col++].Value = tp.DeptName;
                        sheet.Cells[row, col++].Value = tpInfo.IsPassed ? "通過" : "";
                        row++;
                    }                   
                }
                for (int i = 1; i <= col; i++)
                {
                    sheet.Column(col).AutoFit();
                }

                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "訓練資料.xlsx");
            }
            catch (Exception e)
            {

                throw;
            }
        }

        /// <summary>
        /// 匯出未結案清單
        /// </summary>
        /// <param name="StartDate"></param>
        /// <param name="EndDate"></param>
        /// <returns></returns>
        public ActionResult ExportUnClosedList(DateTime StartDate, DateTime EndDate)
        {
            //取出要匯出Excel的資料
            var query = trainingRepository.Query("", StartDate, EndDate, "", "", 0, 0, 0, 0, false, 0, modelState);
            List<TrainingView> trainingList = Mapper.Map<IEnumerable<TrainingView>>(query).OrderBy(q => q.Organiser).ToList();

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("課程清單");

                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //第1列是標題列 
                //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                //取得做法可參考最後的參考連結。

                int row = 1;    //列:橫的
                //sheet.Cells[row, col++].Value = "編號";
                sheet.Cells[row, col++].Value = "課程";
                sheet.Cells[row, col++].Value = "承辦人";
                sheet.Cells[row, col++].Value = "開課時間";
                sheet.Cells[row, col++].Value = "類別";
                sheet.Cells[row, col++].Value = "講師";
                sheet.Cells[row, col++].Value = "14. 講義/照片/影片";
                sheet.Cells[row, col++].Value = "17a. 意見表";
                sheet.Cells[row, col++].Value = "17b. 驗收資料";
                sheet.Cells[row, col++].Value = "計畫表審核狀態";
                sheet.Cells[row, col++].Value = "結案狀態";

                row++;

                //迴圈讀取訓練資料
                //資料從第2列開始               
                foreach (var tr in trainingList)
                {
                    var trainingPersonList = trainingPersonRepository.GetSignInInfoList(tr.ID);

                    col = 1;//每換一列，欄位要從1開始
                    //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入

                    //sheet.Cells[row, col++].Value = tr.ID;
                    //sheet.Cells[row, col++].Value = tr.ID;
                    sheet.Cells[row, col++].Value = tr.Title;
                    sheet.Cells[row, col++].Value = tr.Organiser;
                    sheet.Cells[row, col++].Value = tr.StartDate.ToString("yyyy/MM/dd HH:mm");
                    sheet.Cells[row, col++].Value = EnumHelper.GetDescription((Category)Convert.ToInt32(tr.Category));
                    sheet.Cells[row, col++].Value = tr.Lecturer;

                    //是否有講義/照片/影片                    
                    sheet.Cells[row, col++].Value = (trainingImageRepository.Query(tr.ID).Count() > 0 || !string.IsNullOrEmpty(tr.MainVideo) || !string.IsNullOrEmpty(tr.MainFile)) ? "Yes" : "No";

                    //是否有意見表
                    sheet.Cells[row, col++].Value = classOpinionRepository.Query(tr.ID).Count() > 0 ? "Yes" : "No";

                    //是否有驗收資料
                    sheet.Cells[row, col++].Value = internalAcceptanceRepository.Query(tr.ID).Count() > 0 || externalAcceptanceRepository.Query(tr.ID).Count() > 0 ? "Yes" : "No";

                    #region 審核狀態
                    var auditInfo = auditRepository.Query(null, 0, 0, null, "", "", tr.ID).Where(q => q.Type == (int)AuditType.External || q.Type == (int)AuditType.Internal && !q.IsInvalid).OrderByDescending(q => q.ID).FirstOrDefault();
                    bool planExist = CheckPlanExist(tr);
                    string auditStatus = "";
                    if (!planExist)
                    {
                        auditStatus = "尚未填寫";
                    }
                    else if (planExist && auditInfo == null)
                    {
                        auditStatus = "已填寫未送審";
                    }
                    else
                    {
                        tr.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
                        if (tr.IsInvalid)
                        {
                            auditStatus = "已駁回";
                        }
                        else if (tr.AuditState == (int)MeetSignIn.Models.AuditState.PresidentAudit)
                        {
                            auditStatus = "已核准";
                        }
                        else
                        {
                            tr.AuditState = auditInfo == null ? 0 : auditInfo.State;
                            auditStatus = EnumHelper.GetDescription((AuditState)tr.AuditState);
                        }
                    }
                    #endregion
                    sheet.Cells[row, col++].Value = auditStatus;

                    string closeStatus = "";
                    if (tr.IsClose)
                    {
                        closeStatus = "已結案";
                    }
                    else if (tr.PresidentClose)
                    {
                        closeStatus = "待校長結案";
                    }
                    else if (tr.AskClose)
                    {
                        closeStatus = "待總經理結案";
                    }
                    sheet.Cells[row, col++].Value = closeStatus;
                    row++;
                }
                for (int i = 1; i <= col; i++)
                {
                    sheet.Column(col).AutoFit();
                }

                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "訓練資料.xlsx");
            }
            catch (Exception e)
            {

                throw;
            }
        }
        public ActionResult ExportFee(DateTime StartDate, DateTime EndDate, string Title = "", int FactoryID = 0, int RoomID = 0, string DeptID = "", string DepartmentStr = "")
        {
            //取出要匯出Excel的資料
            var query = trainingRepository.Query(Title, StartDate, EndDate, DeptID, DepartmentStr, FactoryID, RoomID, 0, 0, null, 0, modelState);
            List<TrainingView> trainingList = Mapper.Map<IEnumerable<TrainingView>>(query).ToList();

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage ep = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("報修清單");

                int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                //第1列是標題列 
                //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                //取得做法可參考最後的參考連結。

                int row = 1;    //列:橫的
                sheet.Cells[row, col++].Value = "課程名稱";
                sheet.Cells[row, col++].Value = "發起人";
                sheet.Cells[row, col++].Value = "訓練單位";
                sheet.Cells[row, col++].Value = "上課日期";
                sheet.Cells[row, col++].Value = "訓練類別";
                sheet.Cells[row, col++].Value = "研訓類別";
                sheet.Cells[row, col++].Value = "時數(小時)";
                sheet.Cells[row, col++].Value = "費用";
                sheet.Cells[row, col++].Value = "講師";
                sheet.Cells[row, col++].Value = "證書/證明";
                sheet.Cells[row, col++].Value = "備註";
                sheet.Cells[row, col++].Value = "姓名";
                sheet.Cells[row, col++].Value = "部門";
                sheet.Cells[row, col++].Value = "是否合格";
                row++;

                //迴圈讀取訓練資料
                //資料從第2列開始               
                foreach (var tr in trainingList)
                {
                    var trainingPersonList = trainingPersonRepository.GetSignInInfoList(tr.ID);

                    foreach (var tp in trainingPersonList)
                    {
                        try
                        {
                            col = 1;//每換一列，欄位要從1開始
                                    //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                                    //sheet.Cells[row, col++].Value = tr.ID;
                            sheet.Cells[row, col++].Value = tr.Title;
                            sheet.Cells[row, col++].Value = tr.Organiser;
                            sheet.Cells[row, col++].Value = departmentRepository.FindByDeptID(tr.DeptID).Title;
                            sheet.Cells[row, col++].Value = tr.StartDate.ToString("yyyy/MM/dd HH:mm");
                            sheet.Cells[row, col++].Value = EnumHelper.GetDescription((Category)Convert.ToInt32(tr.Category));
                            sheet.Cells[row, col++].Value = GetThemeTitle(Convert.ToInt32(tr.Theme));
                            sheet.Cells[row, col++].Value = tr.Hours;
                            sheet.Cells[row, col++].Value = tr.Fee;
                            sheet.Cells[row, col++].Value = tr.Lecturer;
                            string certStr = tr.Cert == 0 ? "2" : tr.Cert.ToString();
                            sheet.Cells[row, col++].Value = tr.CertOptions.Where(q => q.Value == certStr).FirstOrDefault().Text;
                            tr.Remark = tr.Remark == null ? "" : tr.Remark;
                            string remark = HttpUtility.HtmlDecode(Regex.Replace(tr.Remark, "<.*?>", string.Empty));
                            sheet.Cells[row, col++].Value = remark;
                            sheet.Cells[row, col++].Value = tp.Name;
                            sheet.Cells[row, col++].Value = tp.DeptName;
                            var tpInfo = trainingPersonRepository.FindByJobID(tp.PersonId);
                            sheet.Cells[row, col++].Value = tpInfo.IsPassed ? "通過" : "";
                            row++;
                        }
                        catch (Exception ex)
                        {

                            throw;
                        }
                    }
                }
                for (int i = 1; i <= col; i++)
                {
                    sheet.Column(col).AutoFit();
                }

                //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                MemoryStream fileStream = new MemoryStream();
                ep.SaveAs(fileStream);
                ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "訓練資料.xlsx");
            }
            catch (Exception e)
            {

                throw;
            }
        }

        /// <summary>
        /// 匯出課程出席率清單
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public ActionResult ExportTrainingAttendList(DateTime start, DateTime end, string personId = "")
        {
            //取出要匯出Excel的資料
            var personInfo = t8personRepository.FindByPersonId(personId, true);
            if (string.IsNullOrEmpty(personId) || personInfo.PersonId != personId)
            {
                return RedirectToAction("Index", "ManagerAdmin");
            }
            if (personInfo != null)
            {
                var trainingIdList = trainingPersonRepository.Query(null, "", 0, personId).Select(q => q.TraningID).ToList();
                var query = trainingRepository.Query("", start, end, "", "", 0, 0, 0, null, false, 0, modelState).Where(q => trainingIdList.Contains(q.ID));
                List<TrainingView> trainingList = Mapper.Map<IEnumerable<TrainingView>>(query).ToList();

                try
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                                                                                //建立Excel
                    ExcelPackage ep = new ExcelPackage();

                    //建立第一個Sheet，後方為定義Sheet的名稱
                    ExcelWorksheet sheet = ep.Workbook.Worksheets.Add("報修清單");

                    int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                    //第1列是標題列 
                    //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                    //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                    //取得做法可參考最後的參考連結。

                    int row = 1;    //列:橫的
                    sheet.Cells[row, col++].Value = "課程名稱";
                    sheet.Cells[row, col++].Value = "發起人";
                    sheet.Cells[row, col++].Value = "講師";
                    sheet.Cells[row, col++].Value = "上課日期";
                    sheet.Cells[row, col++].Value = "訓練類別";
                    sheet.Cells[row, col++].Value = "研訓類別";
                    sheet.Cells[row, col++].Value = "出席狀態";
                    sheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Row(row).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Wheat); // 設置背景顏色

                    row++;

                    //迴圈讀取訓練資料
                    //資料從第2列開始               
                    int totalCount = trainingList.Count();
                    int attendCount = 0;
                    int missCount = 0;
                    int leaveCount = 0;
                    foreach (var tr in trainingList)
                    {
                        var trainingPersonData = trainingPersonRepository.Query(null, "", tr.ID, personId).FirstOrDefault();

                        col = 1;//每換一列，欄位要從1開始
                        //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                        //sheet.Cells[row, col++].Value = tr.ID;
                        sheet.Cells[row, col++].Value = tr.Title;
                        sheet.Cells[row, col++].Value = tr.Organiser;
                        sheet.Cells[row, col++].Value = tr.Lecturer;
                        sheet.Cells[row, col++].Value = tr.StartDate.ToString("yyyy/MM/dd HH:mm");
                        sheet.Cells[row, col++].Value = EnumHelper.GetDescription((Category)Convert.ToInt32(tr.Category));
                        sheet.Cells[row, col++].Value = GetThemeTitle(Convert.ToInt32(tr.Theme));

                        string statusStr = "未簽到";
                        switch ((SignStatus)trainingPersonData.SignStatus)
                        {
                            case SignStatus.UnSigned:
                                statusStr = "未簽到";
                                missCount++;
                                break;
                            case SignStatus.Signed:
                                statusStr = "已簽到";
                                attendCount++;
                                break;
                            case SignStatus.Leave:
                                statusStr = "請假";
                                leaveCount++;
                                break;
                        }
                        sheet.Cells[row, col++].Value = statusStr;
                        row++;
                    }

                    row++;

                    col = 1;
                    sheet.Cells[row, col].Value = "課程數";
                    sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Cells[row, col++].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);
                    sheet.Cells[row++, col].Value = totalCount + "";
                    if (totalCount > 0)
                    {
                        col = 1;
                        sheet.Cells[row, col].Value = "出席數";
                        sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells[row, col++].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                        sheet.Cells[row++, col].Value = attendCount + $"({(attendCount * 100) / totalCount} %)";

                        col = 1;
                        sheet.Cells[row, col].Value = "缺席數";
                        sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells[row, col++].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightPink);
                        sheet.Cells[row++, col].Value = missCount + $"({(missCount * 100) / totalCount} %)";

                        col = 1;
                        sheet.Cells[row, col].Value = "請假數";
                        sheet.Cells[row, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        sheet.Cells[row, col++].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                        sheet.Cells[row++, col].Value = leaveCount + $"({(leaveCount * 100) / totalCount} %)";
                    }
                    for (int i = 1; i <= col; i++)
                    {
                        sheet.Column(col).AutoFit();
                    }

                    //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                    MemoryStream fileStream = new MemoryStream();
                    ep.SaveAs(fileStream);
                    ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                    fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                    return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", DateTime.Now.ToString("yyyy-MM-dd HH:mm ") + personInfo.PersonName + " 出席紀錄.xlsx");
                }
                catch (Exception e)
                {

                    throw;
                }
            }
            return RedirectToAction("Index", "ManagerAdmin");
        }

        /// <summary>
        /// 匯出課程出席率清單
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public ActionResult ExportExacutiveRate(string deptId, string department, DateTime startDate, DateTime endDate)
        {
            //取出要匯出Excel的資料
            var query = trainingPersonRepository.GetExacutiveRateList(deptId, department, startDate, endDate);
            //啟用的課程
            var onlineTrainingIdList = trainingRepository.GetAll().Where(q => q.IsOnline).Select(q => q.ID).ToList();
            //是啟用中主課程或是系列課程
            var inTrainingIdList = trainingRepository.GetAll().Where(q => (q.SeriesParentID == 0 && q.IsOnline) || onlineTrainingIdList.Contains(q.SeriesParentID)).Select(q => q.ID).ToList();

            //有建立內訓課程
            var haveICourseIdList = internalCourseRepository.GetAll().Where(q => onlineTrainingIdList.Contains(q.TrainingID)).Select(q => q.TrainingID).ToList();
            //有建立外訓課程
            var haveECourseIdList = externalCourseRepository.GetAll().Where(q => onlineTrainingIdList.Contains(q.TrainingID)).Select(q => q.TrainingID).ToList();

            query = query.Where(q => inTrainingIdList.Contains(q.ID) && (haveICourseIdList.Contains(q.ID) || haveECourseIdList.Contains(q.ID)));
            if (query.Count() > 0)
            {
                try
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial; // 關閉新許可模式通知
                    //建立Excel
                    ExcelPackage ep = new ExcelPackage();

                    //建立第一個Sheet，後方為定義Sheet的名稱
                    ExcelWorksheet sheet = ep.Workbook.Worksheets.Add(startDate.ToString("yyyy-MM-dd") + "至" + endDate.ToString("yyyy-MM-dd") + "課程出席率");

                    int col = 1;    //欄:直的，因為要從第1欄開始，所以初始為1

                    //第1列是標題列 
                    //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                    //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                    //取得做法可參考最後的參考連結。

                    int row = 1;    //列:橫的
                    sheet.Cells[row, col++].Value = "課程編號";
                    sheet.Cells[row, col++].Value = "課程名稱";
                    sheet.Cells[row, col++].Value = "講師";
                    sheet.Cells[row, col++].Value = "上課日期";
                    sheet.Cells[row, col++].Value = "學員";
                    sheet.Cells[row, col++].Value = "出席狀態";
                    sheet.Cells[row, col++].Value = "課程規劃滿意度";
                    sheet.Cells[row, col++].Value = "講師滿意度";
                    sheet.Cells[row, col++].Value = "收穫滿意度";
                    sheet.Cells[row, col++].Value = "滿意度總分";
                    sheet.Cells[row, col++].Value = "難易度";
                    sheet.Row(row).Style.Fill.PatternType = ExcelFillStyle.Solid;
                    sheet.Row(row).Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.Wheat); // 設置背景顏色

                    row++;

                    //迴圈讀取訓練資料
                    //資料從第2列開始
                    var tempList = query.OrderBy(q => q.ID).ToList();
                    for (int i = 0; i < tempList.Count(); i++)
                    {
                        var item = tempList[i];
                        col = 1;//每換一列，欄位要從1開始
                                //指定Sheet的欄與列(欄名列號ex.A1,B20，在這邊都是用數字)，將資料寫入
                        sheet.Cells[row, col++].Value = item.ID;
                        sheet.Cells[row, col++].Value = item.Title;
                        sheet.Cells[row, col++].Value = item.Lecturer;
                        sheet.Cells[row, col++].Value = item.StartDate.ToString("yyyy/MM/dd HH:mm");
                        sheet.Cells[row, col++].Value = item.Person;

                        string statusStr = "未簽到";
                        switch ((SignStatus)item.SignStatus)
                        {
                            case SignStatus.UnSigned:
                                statusStr = "未簽到";
                                break;
                            case SignStatus.Signed:
                                statusStr = "已簽到";
                                break;
                            case SignStatus.Leave:
                                statusStr = "請假";
                                break;
                        }
                        sheet.Cells[row, col++].Value = statusStr;

                        sheet.Cells[row, col++].Value = item.CoursePoints;
                        sheet.Cells[row, col++].Value = item.LecturerPoints;
                        sheet.Cells[row, col++].Value = item.RewordPoints;
                        sheet.Cells[row, col++].Value = item.OpinionPoints;
                        sheet.Cells[row, col++].Value = item.Difficulty;
                        row++;
                    }

                    for (int i = 1; i < 12; i++)
                    {
                        sheet.Column(i).AutoFit();
                    }

                    //因為ep.Stream是加密過的串流，故要透過SaveAs將資料寫到MemoryStream，在將MemoryStream使用FileStreamResult回傳到前端
                    MemoryStream fileStream = new MemoryStream();
                    ep.SaveAs(fileStream);
                    ep.Dispose();//如果這邊不下Dispose，建議此ep要用using包起來，但是要記得先將資料寫進MemoryStream在Dispose。
                    fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤

                    return File(fileStream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", startDate.ToString("yyyy-MM-dd") + "至" + endDate.ToString("yyyy-MM-dd") + "課程出席率.xlsx");
                }
                catch (Exception e)
                {

                    throw;
                }
            }
            return RedirectToAction("Overview", "TrainingAdmin", new { deptId = deptId , department = department, StartDate = startDate, EndDate = endDate });
        }
        #endregion

        #region Mail
        public ActionResult SendNotify(int tid)
        {
            #region Gmail 發信
            //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port
            //System.Net.Mail.SmtpClient MySmtp = new System.Net.Mail.SmtpClient("smtp.gmail.com", 587);

            //設定你的帳號密碼
            //MySmtp.Credentials = new System.Net.NetworkCredential("a842695137@gmail.com", "judnmgpwvfdvmsyk");

            //Gmial 的 smtp 使用 SSL
            //MySmtp.EnableSsl = true;
            #endregion

            try
            {
                var trainingInfo = Mapper.Map<TrainingView>(trainingRepository.FindBy(tid));
                if (trainingInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無教育訓練資料");
                }
                var roomInfo = roomRepository.FindBy(trainingInfo.RoomID);
                var notifyList = trainingPersonRepository.Query(null, "", tid);
                var notifyJidList = notifyList.Select(q => q.JobID).ToList();
                var reciverList = Mapper.Map<List<EmployeeView>>(t8personRepository.GetPersonQuery().Where(q => notifyJidList.Contains(q.JobID)).ToList());
                foreach (var item in reciverList)
                {
                    var thisTrainingPerson = notifyList.Where(q => q.JobID == item.JobID).FirstOrDefault();

                    //生成Qrcode圖片
                    string host = $"{Request.Url.Scheme}://{Request.Url.Authority}";

                    string fileName = "";
                    string qrcodeUrl = "";
                    if (roomInfo != null)
                    {
                        //簽到QrCode
                        fileName = $"{trainingInfo.ID}_{item.JobID}.png";
                        qrcodeUrl = $"{host}/Sign/TrainingCheck?rn={roomInfo.RoomNumber}&tid={trainingInfo.ID}";
                        //string qrcodeUrl = thisTrainingPerson.SignCode;
                        string savePath = Server.MapPath("/FileUploads/Qrcode") + $"/{trainingInfo.ID}/{fileName}";
                        QRCodeHelper.GeneratorQrCodeImage(qrcodeUrl, savePath);
                    }

                    //意見表QrCode
                    string opinionfileName = $"{trainingInfo.ID}_opinion_{item.JobID}.png";
                    //string opinionqrcodeUrl = $"{host}/ClassOpinionAdmin/Edit?trainingId={trainingInfo.ID}&personId={item.JobID}";
                    string opinionqrcodeUrl = $"{host}/Admin/ClassOpinionAdmin/ToOpinion?trainingId={trainingInfo.ID}";
                    string opinionsavePath = Server.MapPath("/FileUploads/Qrcode") + $"/{trainingInfo.ID}/{opinionfileName}";
                    QRCodeHelper.GeneratorQrCodeImage(opinionqrcodeUrl, opinionsavePath);

                    //send mail test : http://{officialHost}/Admin/TrainingAdmin/SendNotify?tid=312
                    //opinion link test : https://localhost:44305/Admin/ClassOpinionAdmin/Edit?trainingId=312&personId=2416

                    //寄送Email
                    //string from = "a842695137@gmail.com";
                    //string to = "seelen12@fairlybike.com";
                    string to = item.Email;
                    string subject = "";
                    string mailBody = "";
                    mailBody = $"<h3>【{trainingInfo.Title}】 {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{trainingInfo.RoomStr}</h3>" +
                                                $"<p>發起人 : {trainingInfo.Organiser}</p>" +
                                                $"<p>被通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                    string signUrl = $"{Request.Url.Scheme}://{officialHost}/Sign/TrainingCheck?rn={trainingInfo.RoomNum}&tid={trainingInfo.ID}";
                    if (!string.IsNullOrEmpty(trainingInfo.Lecturer))
                    {
                        mailBody += $"<p>講師 : {trainingInfo.Lecturer}";
                    }
                    mailBody += $"<p>預定開始時間 : {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                        $"<p>預定結束時間 : {trainingInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";
                    if (trainingInfo.FactoryID == 9)
                    {
                        mailBody += $"<p>簽到連結 : <a href=\"{signUrl}\">{signUrl}</a></p>";
                        if (!string.IsNullOrEmpty(trainingInfo.Url))
                        {
                            var urlArr = Regex.Split(trainingInfo.Url, "\r\n");
                            for (int i = 0; i < urlArr.Count(); i++)
                            {
                                mailBody += $"<p>會議連結 第{i}場 : <a href=\"{urlArr[i]}\">{urlArr[i]}</a></p>";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.Note))
                    {
                        mailBody += $"<p>    注意事項 : </p>";
                        var urlArr = Regex.Split(trainingInfo.Note, "\r\n");
                        for (int i = 0; i < urlArr.Count(); i++)
                        {
                            mailBody += $"<p>    {urlArr[i].Replace("  ", "  ")}</p>";
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.NotifyMsg))
                    {
                        mailBody += $"<p>{trainingInfo.NotifyMsg.Replace("\r\n", "<br>")}</p>";
                    }

                    if (roomInfo != null)
                    {
                        mailBody += $"<br><p>掃碼簽到</p>" +
                                                    $"<br><span>* 需待會議〔開放簽到〕後始可簽到</span>" +
                                                    $"<br><img src=\"{host}/FileUploads/Qrcode/{trainingInfo.ID}/{fileName}\" alt=\"QR Code\" />";
                        mailBody += $"<br><a class=\"btn\" href=\"{qrcodeUrl}\">點擊簽到</a>";
                    }

                    mailBody += $"<br><p>意見回饋</p>" +
                                                $"<br><img src=\"{host}/FileUploads/Qrcode/{trainingInfo.ID}/{opinionfileName}\" alt=\"QR Code\" />";
                    mailBody += $"<br><a class=\"btn\" href=\"{opinionqrcodeUrl}\">點擊前往</a>";

                    subject = $"教育訓練通知 - 系統信件";

                    var calendar = new Ical.Net.Calendar();

                    DateTime today = DateTime.Now;
                    calendar.Events.Add(new Ical.Net.CalendarComponents.CalendarEvent
                    {
                        Class = "PUBLIC",
                        Summary = subject,
                        Created = new CalDateTime(today),
                        //Description = $"【{meetingInfo.Title}】會議通知",
                        Description = $"【{trainingInfo.Title}】教育訓練通知",
                        Start = new CalDateTime(trainingInfo.StartDate),
                        End = new CalDateTime(trainingInfo.EndDate),
                        Sequence = 0,
                        Uid = Guid.NewGuid().ToString(),
                        Location = trainingInfo.RoomStr,
                    });

                    var serializer = new CalendarSerializer(new SerializationContext());
                    var serializedCalendar = serializer.SerializeToString(calendar);
                    var bytesCalendar = Encoding.UTF8.GetBytes(serializedCalendar);
                    MemoryStream ms = new MemoryStream(bytesCalendar);

                    MailHelper.POP3CalendarMail(to, subject, mailBody, ms, "教育訓練邀請");
                    //if (System.IO.File.Exists(savePath))
                    //{
                    //    System.IO.File.Delete(savePath);
                    //}

                    //更新寄送狀態                    
                    thisTrainingPerson.IsSent = true;
                    trainingPersonRepository.Update(thisTrainingPerson);
                }

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "TrainingAdmin", new { id = tid });
        }
        public ActionResult SendExternalAuditNotify(int tid)
        {
            try
            {
                var trainingInfo = Mapper.Map<ViewModels.Training.TrainingView>(trainingRepository.FindBy(tid));
                if (trainingInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無教育訓練資料");
                }
                var roomInfo = roomRepository.FindBy(trainingInfo.RoomID);
                var notifyList = trainingPersonRepository.Query(null, "", tid);
                var notifyJidList = notifyList.Select(q => q.JobID).ToList();
                var reciverList = Mapper.Map<List<EmployeeView>>(t8personRepository.GetPersonQuery().Where(q => notifyJidList.Contains(q.JobID)).ToList());
                foreach (var item in reciverList)
                {
                    var thisTrainingPerson = notifyList.Where(q => q.JobID == item.JobID).FirstOrDefault();

                    //寄送Email
                    //string from = "a842695137@gmail.com";
                    //string to = "seelen12@fairlybike.com";
                    string to = item.Email;
                    string subject = "";
                    string mailBody = "";
                    mailBody = $"<h3>【{trainingInfo.Title}】 {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{trainingInfo.RoomStr}</h3>" +
                                                $"<p>發起人 : {trainingInfo.Organiser}</p>" +
                                                $"<p>被通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                    string signUrl = $"{Request.Url.Scheme}://{officialHost}/Sign/TrainingCheck?rn={trainingInfo.RoomNum}&tid={trainingInfo.ID}";
                    if (!string.IsNullOrEmpty(trainingInfo.Lecturer))
                    {
                        mailBody += $"<p>講師 : {trainingInfo.Lecturer}";
                    }
                    mailBody += $"<p>預定開始時間 : {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                        $"<p>預定結束時間 : {trainingInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";
                    if (trainingInfo.FactoryID == 9)
                    {
                        mailBody += $"<p>簽到連結 : <a href=\"{signUrl}\">{signUrl}</a></p>";
                        if (!string.IsNullOrEmpty(trainingInfo.Url))
                        {
                            var urlArr = Regex.Split(trainingInfo.Url, "\r\n");
                            for (int i = 0; i < urlArr.Count(); i++)
                            {
                                mailBody += $"<p>會議連結 第{i}場 : <a href=\"{urlArr[i]}\">{urlArr[i]}</a></p>";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.Note))
                    {
                        mailBody += $"<p>    注意事項 : </p>";
                        var urlArr = Regex.Split(trainingInfo.Note, "\r\n");
                        for (int i = 0; i < urlArr.Count(); i++)
                        {
                            mailBody += $"<p>    {urlArr[i].Replace("  ", "  ")}</p>";
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.NotifyMsg))
                    {
                        mailBody += $"<p>{trainingInfo.NotifyMsg.Replace("\r\n", "<br>")}</p>";
                    }

                    subject = $"教育訓練通知 - 系統信件";

                    MailHelper.POP3Mail(to, subject, mailBody);

                    //更新寄送狀態                    
                    thisTrainingPerson.IsSent = true;
                    trainingPersonRepository.Update(thisTrainingPerson);
                }

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "TrainingAdmin", new { id = tid });
        }
        public ActionResult SendInternalAuditNotify(int tid)
        {
            try
            {
                var trainingInfo = Mapper.Map<ViewModels.Training.TrainingView>(trainingRepository.FindBy(tid));
                if (trainingInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無教育訓練資料");
                }
                var roomInfo = roomRepository.FindBy(trainingInfo.RoomID);
                var notifyList = trainingPersonRepository.Query(null, "", tid);
                var notifyJidList = notifyList.Select(q => q.JobID).ToList();
                var reciverList = Mapper.Map<List<EmployeeView>>(t8personRepository.GetPersonQuery().Where(q => notifyJidList.Contains(q.JobID)).ToList());
                foreach (var item in reciverList)
                {
                    var thisTrainingPerson = notifyList.Where(q => q.JobID == item.JobID).FirstOrDefault();

                    //生成Qrcode圖片
                    string host = $"{Request.Url.Scheme}://{Request.Url.Authority}";

                    //簽到QrCode
                    string fileName = $"{trainingInfo.ID}_{item.JobID}.png";
                    string qrcodeUrl = $"{host}/Sign/TrainingCheck?rn={roomInfo.RoomNumber}&tid={trainingInfo.ID}";
                    //string qrcodeUrl = thisTrainingPerson.SignCode;
                    string savePath = Server.MapPath("/FileUploads/Qrcode") + $"/{trainingInfo.ID}/{fileName}";
                    QRCodeHelper.GeneratorQrCodeImage(qrcodeUrl, savePath);

                    //意見表QrCode
                    string opinionfileName = $"{trainingInfo.ID}_opinion_{item.JobID}.png";
                    //string opinionqrcodeUrl = $"{host}/ClassOpinionAdmin/Edit?trainingId={trainingInfo.ID}&personId={item.JobID}";
                    string opinionqrcodeUrl = $"{host}/Admin/ClassOpinionAdmin/ToOpinion?trainingId={trainingInfo.ID}";
                    string opinionsavePath = Server.MapPath("/FileUploads/Qrcode") + $"/{trainingInfo.ID}/{opinionfileName}";
                    QRCodeHelper.GeneratorQrCodeImage(opinionqrcodeUrl, opinionsavePath);

                    //寄送Email
                    //string from = "a842695137@gmail.com";
                    //string to = "seelen12@fairlybike.com";
                    string to = item.Email;
                    string subject = "";
                    string mailBody = "";
                    mailBody = $"<h3>【{trainingInfo.Title}】 {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{trainingInfo.RoomStr}</h3>" +
                                                $"<p>發起人 : {trainingInfo.Organiser}</p>" +
                                                $"<p>被通知人 : 「{item.DepartmentStr}」 {item.Name + item.EngName}</p>";

                    string signUrl = $"{Request.Url.Scheme}://{officialHost}/Sign/TrainingCheck?rn={trainingInfo.RoomNum}&tid={trainingInfo.ID}";
                    if (!string.IsNullOrEmpty(trainingInfo.Lecturer))
                    {
                        mailBody += $"<p>講師 : {trainingInfo.Lecturer}";
                    }
                    mailBody += $"<p>預定開始時間 : {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")}</p>" +
                                        $"<p>預定結束時間 : {trainingInfo.EndDate.ToString("yyyy/MM/dd HH:mm")}</p>";
                    if (trainingInfo.FactoryID == 9)
                    {
                        mailBody += $"<p>簽到連結 : <a href=\"{signUrl}\">{signUrl}</a></p>";
                        if (!string.IsNullOrEmpty(trainingInfo.Url))
                        {
                            var urlArr = Regex.Split(trainingInfo.Url, "\r\n");
                            for (int i = 0; i < urlArr.Count(); i++)
                            {
                                mailBody += $"<p>會議連結 第{i}場 : <a href=\"{urlArr[i]}\">{urlArr[i]}</a></p>";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.Note))
                    {
                        mailBody += $"<p>    注意事項 : </p>";
                        var urlArr = Regex.Split(trainingInfo.Note, "\r\n");
                        for (int i = 0; i < urlArr.Count(); i++)
                        {
                            mailBody += $"<p>{trainingInfo.NotifyMsg.Replace("\r\n", "<br>")}</p>";
                        }
                    }
                    if (!string.IsNullOrEmpty(trainingInfo.NotifyMsg))
                    {
                        mailBody += $"<br>" + trainingInfo.NotifyMsg;
                    }

                    mailBody += $"<br><p>掃碼簽到</p>" +
                                               $"<br><span>* 需待會議〔開放簽到〕後始可簽到</span>" +
                                               $"<br><img src=\"{host}/FileUploads/Qrcode/{trainingInfo.ID}/{fileName}\" alt=\"QR Code\" />";
                    mailBody += $"<br><a class=\"btn\" href=\"{qrcodeUrl}\">點擊簽到</a>";
                    mailBody += $"<br><p>意見回饋</p>" +
                                                $"<br><img src=\"{host}/FileUploads/Qrcode/{trainingInfo.ID}/{opinionfileName}\" alt=\"QR Code\" />";
                    mailBody += $"<br><a class=\"btn\" href=\"{opinionqrcodeUrl}\">點擊前往</a>";

                    subject = $"教育訓練通知 - 系統信件";

                    MailHelper.POP3Mail(to, subject, mailBody);
                    //if (System.IO.File.Exists(savePath))
                    //{
                    //    System.IO.File.Delete(savePath);
                    //}

                    //更新寄送狀態                    
                    thisTrainingPerson.IsSent = true;
                    trainingPersonRepository.Update(thisTrainingPerson);
                }

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "TrainingAdmin", new { id = tid });
        }

        /// <summary>
        /// 通知填寫意見表
        /// </summary>
        /// <param name="trainingId"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public ActionResult SendOpinionNotify(int trainingId, string personId)
        {
            try
            {
                var trainingInfo = Mapper.Map<ViewModels.Training.TrainingView>(trainingRepository.FindBy(trainingId));
                if (trainingInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無教育訓練資料");
                }
                var personInfo = t8personRepository.FindByPersonId(personId);
                if (personInfo == null)
                {
                    ShowMessage(true, "發送失敗，查無人員資料");
                }


                //寄送Email
                //string from = "a842695137@gmail.com";
                //string to = "seelen12@fairlybike.com";
                string to = personInfo.Email;
                string subject = "";
                string mailBody = "";
                mailBody = $"<h3>【{trainingInfo.Title}】 {trainingInfo.StartDate.ToString("yyyy/MM/dd HH:mm")} 於{trainingInfo.RoomStr}</h3>" +
                                            $"<p>發起人 : {trainingInfo.Organiser}</p>" +
                                            $"<p>被通知人 : 「{personInfo.DeptName}」 {personInfo.Name + personInfo.EngName}</p>";

                subject = $"教育訓練通知 - 系統信件";

                MailHelper.POP3Mail(to, subject, mailBody);

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Edit", "TrainingAdmin", new { id = trainingId });
        }
        /// <summary>
        /// 通知填寫意見表
        /// </summary>
        /// <param name="trainingId"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public ActionResult SendUnClosedNotifiy(DateTime startDate, DateTime endDate)
        {
            try
            {
                var unClosedTrainingQuery = trainingRepository.Query("", startDate, endDate, "", "", 0, 0, 0, 0, false);
                foreach (var creater in unClosedTrainingQuery.Select(q => q.Creater).Distinct())
                {
                    var adminPerson = adminRepository.GetById(creater);
                    if (adminPerson == null)
                    {
                        continue;
                    }
                    var personInfo = t8personRepository.FindByPersonId(adminPerson.Account);
                    if (personInfo == null)
                    {
                        continue;
                    }
                    //寄送Email
                    //string from = "a842695137@gmail.com";
                    //string to = "seelen12@fairlybike.com";
                    string to = personInfo.Email;
                    string subject = "";
                    string mailBody = "";
                    mailBody = $"<h3>【訓練未結案通知】您有訓練課程尚未結案，請於12/31前送審結案</h3>" +
                                                $"<p>申請人 : {personInfo.PersonName}</p>";

                    subject = $"教育訓練通知 - 訓練未結案";

                    MailHelper.POP3Mail(to, subject, mailBody);
                }

                ShowMessage(true, "通知發送成功");
            }
            catch (Exception ex)
            {
                ShowMessage(true, "發送失敗，錯誤訊息 : " + ex.Message);
            }
            return RedirectToAction("Index", "TrainingAdmin");
        }
        #endregion

        public string GetThemeTitle(int themeId)
        {
            ThemeRepository themeRepository = new ThemeRepository(new MeetingSignInDBEntities());
            var themeData = themeRepository.GetById(themeId);
            if (themeData != null)
            {
                return themeData.Title;
            }
            return "";
        }
    }

    public static class SetAuditHelper
    {
        public static void SetSignature<T>(T model, AuditLevel auditLevelData) where T : AuditView
        {
            Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

            int AuditProcessing = (int)FormAudit.Processing;

            //主管
            if (model.ManagerAudit > AuditProcessing)
            {
                model.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.ManagerReview);
                if (personInfo != null)
                {
                    model.ManagerDeptId = personInfo.DeptId;
                    model.ManagerDeptName = personInfo.DepartmentStr;
                    model.ManagerName = personInfo.Name;
                }
            }
            //副總
            if (model.ViceAudit > AuditProcessing)
            {
                model.ViceSignature = auditLevelData.ViceReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.ViceReview);
                if (personInfo != null)
                {
                    model.ViceDeptId = personInfo.DeptId;
                    model.ViceDeptName = personInfo.DepartmentStr;
                    model.ViceName = personInfo.Name;
                }
            }
            //總經理
            if (model.PresidentAudit > AuditProcessing)
            {
                model.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.PresidentReview);
                if (personInfo != null)
                {
                    model.PresidentDeptId = personInfo.DeptId;
                    model.PresidentDeptName = personInfo.DepartmentStr;
                    model.PresidentName = personInfo.Name;
                }
            }
            //校長
            if (model.PrincipalAudit > AuditProcessing)
            {
                model.PrincipalSignature = auditLevelData.PrincipalReview + ".jpg";
                var personInfo = t8personRepository.FindByPersonId(auditLevelData.PrincipalReview);
                if (personInfo != null)
                {
                    model.PrincipalDeptId = personInfo.DeptId;
                    model.PrincipalName = personInfo.DepartmentStr;
                    model.PrincipalName = personInfo.Name;
                }
            }
        }
        public static void SetDefault<T>(T model) where T : AuditView
        {
            int AuditProcessing = (int)FormAudit.Processing;
            DateTime DefaultDate = Convert.ToDateTime("1999/12/31");

            model.IsAudit = false;
            model.OfficerRemark = model.ManagerRemark =
                model.ViceRemark = model.PrincipalRemark = model.PresidentRemark = "";
            model.OfficerSignature = model.ManagerSignature =
                model.ViceSignature = model.PrincipalSignature = model.PresidentSignature = "";
            model.OfficerAuditDate = model.ManagerAuditDate =
                model.ViceAuditDate = model.PrincipalAuditDate = model.PresidentAuditDate = DefaultDate;
            model.OfficerAudit = model.ManagerAudit =
                model.ViceAudit = model.PrincipalAudit = model.PresidentAudit = AuditProcessing;
        }
    }
}