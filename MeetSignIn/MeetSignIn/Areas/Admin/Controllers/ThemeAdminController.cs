﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Theme;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class ThemeAdminController : BaseAdminController
    {
        private ThemeRepository themeRepository = new ThemeRepository(new MeetingSignInDBEntities());

        public ActionResult Index(ThemeIndexView model)
        {
            var query = themeRepository.Query(model.IsOnline);
            var pageResult = query.ToPageResult<Theme>(model);
            model.PageResult = Mapper.Map<PageResult<ThemeView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new ThemeView();
            if (id != 0)
            {
                var query = themeRepository.FindBy(id);
                model = Mapper.Map<ThemeView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(ThemeView model)
        {
            if (ModelState.IsValid)
            {
                Theme data = Mapper.Map<Theme>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = themeRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    themeRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            themeRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}