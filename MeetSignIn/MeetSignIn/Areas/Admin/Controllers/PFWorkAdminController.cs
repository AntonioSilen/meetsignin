﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.PFWork;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class PFWorkAdminController : BaseAdminController
    {
        private PFWorkRepository pFWorkRepository = new PFWorkRepository(new MeetingSignInDBEntities());

        public ActionResult Index(PFWorkIndexView model)
        {
            var query = pFWorkRepository.Query(model.IsOnline, model.ClassID, model.DeptId, model.Title);
            var pageResult = query.ToPageResult<PFWork>(model);
            model.PageResult = Mapper.Map<PageResult<PFWorkView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new PFWorkView();
            if (id != 0)
            {
                var query = pFWorkRepository.FindBy(id);
                model = Mapper.Map<PFWorkView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PFWorkView model)
        {
            if (ModelState.IsValid)
            {
                PFWork data = Mapper.Map<PFWork>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = pFWorkRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    pFWorkRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            pFWorkRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}