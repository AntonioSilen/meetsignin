﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Areas.Admin.ViewModels.QuestionDB;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class QuestionDBAdminController : BaseAdminController
    {
        private QuestionDBRepository questionDBRepository = new QuestionDBRepository(new MeetingSignInDBEntities());
        private QNRQuestionRepository qNRQuestionRepository = new QNRQuestionRepository(new MeetingSignInDBEntities());
        private QNRRepository qNRRepository = new QNRRepository(new MeetingSignInDBEntities());

        public ActionResult Index(QuestionDBIndexView model)
        {
            var query = questionDBRepository.Query(null, model.Question, model.AnswerType);
            var pageResult = query.ToPageResult<QuestionDB>(model);
            model.PageResult = Mapper.Map<PageResult<QuestionDBView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new QuestionDBView();
            if (id != 0)
            {
                var query = questionDBRepository.FindBy(id);
                model = Mapper.Map<QuestionDBView>(query);
                var usedQNRIdList = qNRQuestionRepository.Query(null, 0, query.Question).Select(q => q.QNRID).ToList();
                model.UsedQNRList = Mapper.Map<List<QNRView>>(qNRRepository.Query().Where(q => usedQNRIdList.Contains(q.ID)));
            }
            else
            {
                return RedirectToAction("Index", "QuestionDBAdmin");
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(QuestionDBView model)
        {
            if (ModelState.IsValid)
            {
                QuestionDB data = Mapper.Map<QuestionDB>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = questionDBRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    questionDBRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            questionDBRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}