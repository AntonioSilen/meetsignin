﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.FairlyPerson;
using MeetSignIn.Areas.Admin.ViewModels.KPI;
using MeetSignIn.Areas.Admin.ViewModels.Person;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPI;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPIMonth;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPIState;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class PersonKPIAdminController : BaseAdminController
    {
        // GET: Admin/PersonKPIAdmin
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
        private KPIRelationRepository kPIRelationRepository = new KPIRelationRepository(new MeetingSignInDBEntities());
        private PersonKPIRepository personKPIRepository = new PersonKPIRepository(new MeetingSignInDBEntities());
        private PersonKPIStateRepository personKPIStateRepository = new PersonKPIStateRepository(new MeetingSignInDBEntities());
        private PersonKPIMonthRepository personKPIMonthRepository = new PersonKPIMonthRepository(new MeetingSignInDBEntities());
        private CheckRepository checkRepository = new CheckRepository(new MeetingSignInDBEntities());
        private comPersonRepository comPersonRepository = new comPersonRepository(new T8ERPEntities());
        public ActionResult Index(FairlyPersonIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            //if (adminInfo.Type >= (int)AdminType.Employee || adminInfo.Type == (int)AdminType.Principal)
            if (adminInfo.Type >= (int)AdminType.Employee || adminInfo.IsPrincipal)
            {
                return RedirectToAction("QuarterEdit", new {PersonId = adminInfo.Account });
            }

            AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
            if (adminInfo.Type < (int)AdminType.ClassSupervisor && adminInfo.Type != (int)AdminType.Sysadmin)
            {
                List<string> auditDepts = new List<string>();
                if (adminInfo.Type == (int)AdminType.Manager)
                {//主管審核
                    auditDepts = auditLevelRepository.Query(true, "", "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
                }
                else if (adminInfo.Type == (int)AdminType.Vice)
                {//副總審核
                    auditDepts = auditLevelRepository.Query(true, "", "", adminInfo.Account, "").Select(q => q.DeptId).ToList();
                }
                else if (adminInfo.Type == (int)AdminType.President)
                {//總經理審核
                    auditDepts = auditLevelRepository.Query(true, "", adminInfo.Account, "", "").Select(q => q.DeptId).ToList();
                }
                model.DisplayDeptId = string.Join(",", auditDepts);
                if (auditDepts.Count() > 0 && string.IsNullOrEmpty(model.DeptId))
                {
                    model.DeptId = auditDepts.FirstOrDefault();
                }
            }

            var query = comPersonRepository.GetPersonQuery(model.PersonName, model.PersonId, model.DeptId);
            var pageResult = query.ToPageResult<GroupJoinPersonAndDepartment>(model);
            model.PageResult = Mapper.Map<PageResult<FairlyPersonView>>(pageResult);

            return View(model);
        }

        public ActionResult History(FairlyPersonView model)
        {
            if (string.IsNullOrEmpty(model.PersonId))
            {
                return RedirectToAction("Index");
            }

            var personInfo = comPersonRepository.FindByPersonID(model.PersonId);
            if (personInfo == null)
            {
                model.AuditStatus = "核准:人員離職";
                model.IsInvalid = false;
            }
            else
            {
                model.PersonName = personInfo.PersonName;
            }

            //var query = personKPIRepository.Query(null, null, model.KPIID, model.PersonId, "", 0, true) ;
            model.PersonKPIList = Mapper.Map<List<PersonKPIView>>(personKPIRepository.Query(null, null, model.KPIID, model.PersonId, "", 0, true).OrderByDescending(q => q.CreateDate));
            if (model.PersonKPIList != null && model.PersonKPIList.Count() > 0)
            {
                foreach (var item in model.PersonKPIList)
                {
                    item.FairlyKPITitle = "";
                    var deptKpiData = kPIRepository.GetById(item.KPIID);
                    if (deptKpiData == null)
                    {
                        continue;
                    }
                    var fairlyKpiData = kPIRepository.GetById(deptKpiData.ParentKPIID);
                    if (fairlyKpiData == null)
                    {
                        continue;
                    }
                    item.FairlyKPITitle = fairlyKpiData.Title;
                }
            }

            model.PersonKPIPercentageTasks = new List<PersonKPIPercentageTask>()
            {
                new PersonKPIPercentageTask() {PersonId = personInfo.PersonId, DeptId= personInfo.PersonId, Percentage = 0},
            };

            if (model.KPIID != 0)
            {
                var personKPI = personKPIRepository.GetById(model.KPIID);
                if (personKPI != null)
                {
                    model.KPITitle = personKPI.KPITitle;
                }
            }           

            return View(model);
        }

        public ActionResult QuarterEdit(string PersonId = "")
        {
            if (string.IsNullOrEmpty(PersonId))
            {
                return RedirectToAction("Index");
            }
            FairlyPersonView model = new FairlyPersonView();
            var personInfo = comPersonRepository.FindByPersonID(PersonId);
            if (personInfo == null)
            {
                model.AuditStatus = "核准:人員離職";
                model.IsInvalid = false;
            }

            model = Mapper.Map<FairlyPersonView>(personInfo);
            var relateKPIIdList = kPIRelationRepository.Query(0, personInfo.DeptId).Select(q => q.KPIID).ToList();
            var joinedKPIIdList = personKPIRepository.Query(null, true, 0, model.PersonId, "", 0, false).Select(q => q.KPIID).ToList();
            model.DeptKPIList = Mapper.Map<List<KPIView>>(kPIRepository.Query(true, "", 0, 2, 0, "", true).Where(q => relateKPIIdList.Contains(q.ID)));
            model.JoinableDeptKPIList = Mapper.Map<List<KPIView>>(model.DeptKPIList.Where(q => !joinedKPIIdList.Contains(q.ID)));
            foreach (var item in model.JoinableDeptKPIList)
            {
                if (item.ParentKPIID != 0)
                {
                    item.ParentKPITitle = kPIRepository.GetById(item.ParentKPIID).Title;
                }
                item.Dept = GetKPIDept(item.ID);
            }

            model.PersonKPIList = Mapper.Map<List<PersonKPIView>>(personKPIRepository.Query(null, true, 0, model.PersonId).OrderByDescending(q => q.CreateDate));
            if (model.PersonKPIList != null && model.PersonKPIList.Count() > 0)
            {
                foreach (var item in model.PersonKPIList)
                {
                    item.FairlyKPITitle = "";
                    var deptKpiData = kPIRepository.GetById(item.KPIID);
                    if (deptKpiData == null)
                    {
                        continue;
                    }                   
                    var fairlyKpiData = kPIRepository.GetById(deptKpiData.ParentKPIID);
                    if (fairlyKpiData == null)
                    {
                        continue;
                    }
                    item.FairlyKPITitle = fairlyKpiData.Title;
                }
            }

            model.PersonKPIPercentageTasks = new List<PersonKPIPercentageTask>()
            {
                new PersonKPIPercentageTask() {PersonId = personInfo.PersonId, DeptId= personInfo.PersonId, Percentage = 0},
            };

            //#region 審核資料
            //var auditInfo = auditRepository.Query(null, 0, (int)AuditType.PersonKPI, null, "", "", Convert.ToInt32(model.PersonId)).Where(q => q.Type == (int)AuditType.Seminar).OrderByDescending(q => q.ID).FirstOrDefault();
            //model.AuditState = auditInfo == null ? 0 : auditInfo.State;
            //model.AuditStatus = EnumHelper.GetDescription((AuditState)model.AuditState);
            //model.IsInvalid = auditInfo == null ? false : auditInfo.IsInvalid;
            //#endregion

            return View(model);
        }

        public ActionResult MonthsEdit(string PersonId ="")
        {
            if (string.IsNullOrEmpty(PersonId))
            {
                return RedirectToAction("Index");
            }
            FairlyPersonView model = new FairlyPersonView();
            var personInfo = comPersonRepository.FindByPersonID(PersonId);
            if (personInfo == null)
            {
                model.AuditStatus = "核准:人員離職";
                model.IsInvalid = false;
            }

            model = Mapper.Map<FairlyPersonView>(personInfo);

            var relateKPIIdList = kPIRelationRepository.Query(0, personInfo.DeptId).Select(q => q.KPIID).ToList();
            var joinedKPIIdList = personKPIRepository.Query(null, true, 0, model.PersonId, "", 0, false).Select(q => q.KPIID).ToList();
            model.DeptKPIList = Mapper.Map<List<KPIView>>(kPIRepository.Query(true, "", 0, 2, 0, "", true).Where(q => relateKPIIdList.Contains(q.ID)));
            model.JoinableDeptKPIList = Mapper.Map<List<KPIView>>(model.DeptKPIList.Where(q => !joinedKPIIdList.Contains(q.ID)));
            foreach (var item in model.JoinableDeptKPIList)
            {
                if (item.ParentKPIID != 0)
                {
                    item.ParentKPITitle = kPIRepository.GetById(item.ParentKPIID).Title;
                }
                item.Dept = GetKPIDept(item.ID);
            }

            model.PersonKPIList = Mapper.Map<List<PersonKPIView>>(personKPIRepository.Query(null, true, 0, model.PersonId).OrderByDescending(q => q.KPIID));
            if (model.PersonKPIList != null && model.PersonKPIList.Count() > 0)
            {
                foreach (var item in model.PersonKPIList)
                {
                    item.FairlyKPITitle = "";
                    var deptKpiData = kPIRepository.GetById(item.KPIID);
                    if (deptKpiData == null)
                    {
                        continue;
                    }
                    item.FairlyKPIID = deptKpiData.ParentKPIID;
                    var fairlyKpiData = kPIRepository.GetById(item.FairlyKPIID);
                    if (fairlyKpiData == null)
                    {
                        continue;
                    }
                    item.FairlyKPITitle = fairlyKpiData.Title;
                    item.FairlyKPIDescription = string.IsNullOrEmpty(fairlyKpiData.Description) ? "" : fairlyKpiData.Description;
                }
            }

            model.PersonKPIPercentageTasks = new List<PersonKPIPercentageTask>()
            {
                new PersonKPIPercentageTask() {PersonId = personInfo.PersonId, DeptId= personInfo.PersonId, Percentage = 0},
            };


            return View(model);
        }

        public ActionResult Edit(int kpiId = 0, string personId = "", bool isNew = false, bool FromAudit = false)
        {
            var persomnInfo = t8personRepository.FindByPersonId(personId);
            if (kpiId == 0 || persomnInfo == null)
            {
                return RedirectToAction("Index", "PersonKPIAdmin");
            }

            var model = new PersonKPIView();            
            var query = personKPIRepository.Query(null, null, kpiId).OrderByDescending(q => q.ID).FirstOrDefault();
            if (query != null)
            {//舊資料
                model = Mapper.Map<PersonKPIView>(query);
                var stateDataList = personKPIStateRepository.Query(null, model.ID).OrderBy(q => q.ID).ToList();
                model.StateList = Mapper.Map<List<PersonKPIStateView>>(stateDataList);
                if (isNew && model.IsAudit)
                {//取消送審/重新編輯
                    var newData = Mapper.Map<PersonKPIView>(query);
                    var oldAudit = auditRepository.Query(null, query.ID, (int)AuditType.PersonKPI, null, "", "").FirstOrDefault();
                    if (oldAudit != null)//複製並更新舊的審核紀錄
                    {
                        //撤銷舊資料
                        oldAudit.IsInvalid = true;
                        auditRepository.Update(oldAudit);

                        //重置審核資料
                        newData.AuditInfo = model.AuditInfo = auditRepository.FindByTarget(query.ID, (int)AuditType.PersonKPI);
                    }
                    PersonKPI data = Mapper.Map<PersonKPI>(newData);
                    data.IsAudit = false;
                    data.OfficerAudit = AuditProcessing;
                    data.OfficerRemark = "";
                    data.OfficerSignature = "";
                    data.OfficerAuditDate = null;
                    data.ManagerAudit = AuditProcessing;
                    data.ManagerRemark = "";
                    data.ManagerSignature = "";
                    data.ManagerAuditDate = null;
                    data.ViceAudit = AuditProcessing;
                    data.ViceRemark = "";
                    data.ViceSignature = "";
                    data.ViceAuditDate = null;
                    data.PresidentAudit = AuditProcessing;
                    data.PresidentRemark = "";
                    data.PresidentSignature = "";
                    data.PresidentAuditDate = null;
                    newData.ID = personKPIRepository.Insert(data);
                    newData = Mapper.Map<PersonKPIView>(personKPIRepository.Query(null, null, kpiId).OrderByDescending(q => q.ID).FirstOrDefault());

                    SetPersonKPIViewData(FromAudit, newData);

                    #region 複製進度資料
                    if (newData.ID != 0)
                    {
                        foreach (var item in stateDataList)
                        {
                            var newState = new PersonKPIState()
                            {
                                PersonKPIID = newData.ID,
                                PersonID = item.PersonID,
                                DeptID = item.DeptID,
                                Sort = item.Sort,
                                Title = item.Title,
                                Description = item.Description,
                                TargetNum = item.TargetNum,
                                TargetUnit = item.TargetUnit,
                                Percentage = item.Percentage,
                                IsAssessed = item.IsAssessed,
                                Quarter = item.Quarter,
                                ApprovalState = item.ApprovalState,
                                IsLatest = item.IsLatest,
                                IsFinished = item.IsFinished,
                                FinishDate = item.FinishDate,
                                CheckDate = item.CheckDate,
                                CreateDate = item.CreateDate,
                            };
                            personKPIStateRepository.Insert(newState);
                        }
                    }

                    newData.StateList = Mapper.Map<List<PersonKPIStateView>>(personKPIStateRepository.Query(null, newData.ID).OrderByDescending(q => q.CreateDate));
                    #endregion

                    return View(newData);
                }
            }
            else
            {//新建資料
                personKPIRepository.Insert(new PersonKPI()
                {
                    KPIID = kpiId,
                    KPITitle = query.KPITitle,
                    KPIDescription = query.KPIDescription,
                    PersonId = personId,
                    DeptId = persomnInfo.DeptID,
                    State = 1,
                    ApprovalState = 0,
                    IsLatest = true,
                    IsInvalid = false,
                    CreateDate = DateTime.Now,
                    ManagerAudit = AuditProcessing,
                    ViceAudit = AuditProcessing,
                    OfficerAudit = AuditProcessing,
                    PresidentAudit = AuditProcessing,
                });

                model = Mapper.Map<PersonKPIView>(personKPIRepository.Query(null, null, kpiId).OrderByDescending(q => q.ID).FirstOrDefault());
            }
            model.DeptName = persomnInfo.DeptName;
            model.Name = persomnInfo.Name;
            var kpiData = kPIRepository.GetById(model.KPIID);
            var fairlyKpiData = kPIRepository.GetById(kpiData.ParentKPIID);
            model.FairlyKPITitle = fairlyKpiData == null ? "" : fairlyKpiData.Title;

            SetPersonKPIViewData(FromAudit, model);

            return View(model);
        }

        /// <summary>
        /// 個人目標簽章資料
        /// </summary>
        /// <param name="FromAudit"></param>
        /// <param name="model"></param>
        private void SetPersonKPIViewData(bool FromAudit, PersonKPIView model)
        {
            model.FromAudit = FromAudit;

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var officerPersonInfo = t8personRepository.FindByPersonId(model.PersonId);
            if (officerPersonInfo != null)
            {
                model.OfficerDeptId = officerPersonInfo.DeptID;
                model.OfficerDeptName = officerPersonInfo.DepartmentStr;
                model.OfficerName = officerPersonInfo.Name;

                var auditLevelData = auditLevelRepository.FindByDept(officerPersonInfo.DeptID);
                //if (adminRepository.FindByPersonId(officerPersonInfo.JobID).Type == (int)AdminType.Principal)
                if (adminRepository.FindByPersonId(officerPersonInfo.JobID).IsPrincipal)
                {
                    model.ManagerAudit = AuditProcessing;
                    auditLevelData.ViceReview = officerPersonInfo.JobID;
                    model.PresidentAudit = AuditProcessing;
                }
                model.AuditLevelInfo = auditLevelData;
                //主管
                if (model.ManagerAudit > AuditProcessing)
                {
                    model.ManagerSignature = auditLevelData.ManagerReview + ".jpg";
                    var personInfo = t8personRepository.FindByPersonId(auditLevelData.ManagerReview);
                    model.ManagerDeptId = personInfo.DeptId;
                    model.ManagerDeptName = personInfo.DepartmentStr;
                    model.ManagerName = personInfo.Name;
                }
                //副總
                if (model.ViceAudit > AuditProcessing)
                {
                    model.ViceSignature = auditLevelData.ViceReview + ".jpg";
                    var personInfo = t8personRepository.FindByPersonId(auditLevelData.ViceReview);
                    model.ViceDeptId = personInfo.DeptId;
                    model.ViceDeptName = personInfo.DepartmentStr;
                    model.ViceName = personInfo.Name;
                }
                //總經理
                if (model.PresidentAudit > AuditProcessing)
                {
                    model.PresidentSignature = auditLevelData.PresidentReview + ".jpg";
                    var personInfo = t8personRepository.FindByPersonId(auditLevelData.PresidentReview);
                    model.PresidentDeptId = personInfo.DeptId;
                    model.PresidentDeptName = personInfo.DepartmentStr;
                    model.PresidentName = personInfo.Name;
                }

            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PersonKPIView model)
        {
            var adminInfo = adminRepository.FindByPersonId(model.PersonId);
            if (ModelState.IsValid && adminInfo != null)
            {
                PersonKPI data = Mapper.Map<PersonKPI>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;
                #region 日期處理
                data.OfficerAuditDate = SetAuditDate(model.OfficerAudit, model.OfficerAuditDate);
                data.ManagerAuditDate = SetAuditDate(model.ManagerAudit, model.ManagerAuditDate);
                data.ViceAuditDate = SetAuditDate(model.ViceAudit, model.ViceAuditDate);
                data.PresidentAuditDate = SetAuditDate(model.PresidentAudit, model.PresidentAuditDate);
                #endregion

                if (model.ID == 0)
                {
                    data.CreateDate = DateTime.Now;
                    model.ID = personKPIRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    personKPIRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 送審
                if (model.IsAudit)
                {
                    var auditInfo = auditRepository.Query(null, model.ID, (int)AuditType.PersonKPI).FirstOrDefault();

                    #region 簽核狀態
                    int state = (int)AuditState.OfficerAudit;
                    var planAuditInfo = personKPIRepository.Query(null, null, model.KPIID).OrderByDescending(q => q.ID).FirstOrDefault();
                    if (planAuditInfo != null)
                    {
                        state = HandleState(model.DeptId, planAuditInfo.OfficerAudit, planAuditInfo.ManagerAudit, planAuditInfo.ViceAudit, planAuditInfo.PresidentAudit, 2);
                        planAuditInfo.ApprovalState = state;

                        #region 日期處理
                        model.OfficerAudit = Approved;
                        planAuditInfo.OfficerAuditDate = SetAuditDate(model.OfficerAudit, model.OfficerAuditDate);
                        planAuditInfo.ManagerAuditDate = SetAuditDate(model.ManagerAudit, model.ManagerAuditDate);
                        planAuditInfo.ViceAuditDate = SetAuditDate(model.ViceAudit, model.ViceAuditDate);
                        planAuditInfo.PresidentAuditDate = SetAuditDate(model.PresidentAudit, model.PresidentAuditDate);
                        
                        #endregion
                        planAuditInfo.OfficerAudit = AuditApproval;
                        planAuditInfo.OfficerRemark = model.OfficerRemark;
                        planAuditInfo.OfficerSignature = model.OfficerSignature;
                        planAuditInfo.ManagerAudit = model.ManagerAudit < AuditApproval ? AuditProcessing : model.ManagerAudit;
                        planAuditInfo.ManagerRemark = model.ManagerRemark;
                        planAuditInfo.ManagerSignature = model.ManagerSignature;
                        planAuditInfo.ViceAudit = model.ViceAudit < AuditApproval ? AuditProcessing : model.ViceAudit;
                        planAuditInfo.ViceRemark = model.ViceRemark;
                        planAuditInfo.ViceSignature = model.ViceSignature;
                        planAuditInfo.PresidentAudit = model.PresidentAudit < AuditApproval ? AuditProcessing : model.PresidentAudit;
                        planAuditInfo.PresidentRemark = model.PresidentRemark;
                        planAuditInfo.PresidentSignature = model.PresidentSignature;
                        personKPIRepository.Update(planAuditInfo);
                    }
                    else
                    {
                        var planData = new PersonKPI()
                        {
                            KPIID = model.KPIID,
                            KPITitle = model.KPITitle,
                            KPIDescription = model.KPIDescription,
                            PersonId = model.PersonId,
                            DeptId = model.DeptId,
                            State = 1,
                            ApprovalState = 0,
                            IsLatest = true,
                            IsInvalid = false,
                            CreateDate = DateTime.Now,
                            IsAudit = true,
                            OfficerAudit = AuditApproval,
                            OfficerRemark = model.OfficerRemark,
                            OfficerSignature = model.OfficerSignature,
                            OfficerAuditDate = SetAuditDate(model.OfficerAudit, model.OfficerAuditDate),
                            ManagerAudit = AuditProcessing,
                            ManagerRemark = "",
                            ManagerSignature = "",
                            ManagerAuditDate = null,
                            ViceAudit = AuditProcessing,
                            ViceRemark = "",
                            ViceSignature = "",
                            ViceAuditDate = null,
                            PresidentAudit = AuditProcessing,
                            PresidentRemark = "",
                            PresidentSignature = "",
                            PresidentAuditDate = null,
                        };
                        personKPIRepository.Insert(planData);
                    }
                    #endregion
                    if (auditInfo == null)
                    {
                        var auditId = auditRepository.Insert(new Audit()
                        {
                            TrainingID = model.KPIID,
                            PersonID = model.PersonId,
                            TargetID = model.ID,
                            Type = (int)AuditType.PersonKPI,
                            State = state,
                            IsInvalid = false,
                            Officer = adminInfo.ID,
                            CreateDate = DateTime.Now
                        });
                    }
                    else
                    {
                        auditInfo.State = state;
                        auditRepository.Update(auditInfo);
                    }

                    return RedirectToAction("Edit", new { kpiId = model.KPIID, personId = model.PersonId });
                }
                #endregion
            }

            return RedirectToAction("Edit", new { kpiId = model.KPIID, personId = model.PersonId });
        }

        public ActionResult Delete(int id)
        {
            personKPIRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 恢復作廢資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Restore(int id)
        {
            var newData = personKPIRepository.GetById(id);
            string personId = newData == null ? "" : newData.PersonId;
            try
            {
                //作廢原版本
                var oldData = personKPIRepository.Query(null, true, newData.KPIID, newData.PersonId, "", 0, false).FirstOrDefault();
                if (oldData != null)
                {
                    oldData.IsLatest = false;
                    oldData.IsInvalid = true;
                    personKPIRepository.Update(oldData);
                }
                //恢復舊版本
                newData.IsLatest = true;
                newData.IsInvalid = false;
                personKPIRepository.Update(newData);
                
                ShowMessage(true, "恢復成功");
            }
            catch (Exception e)
            {
                ShowMessage(false, "恢復成功");
            }
            return RedirectToAction("QuarterEdit", "PersonKPIAdmin", new { PersonId = personId });
        }

        /// <summary>
        /// 作廢資料
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Invalid(int id)
        {
            var data = personKPIRepository.GetById(id);
            string personId = data == null ? "" : data.PersonId;
            try
            {                
                personId = data.PersonId;

                data.IsLatest = false;
                data.IsInvalid = true;
                data.ApprovalState = (int)AuditState.Processing;
                data.IsAudit = false;
                personKPIRepository.Update(data);

                var auditInfo = auditRepository.FindByTarget(data.ID, (int)AuditType.PersonKPI, data.KPIID, data.PersonId);
                if (auditInfo != null)
                {
                    auditInfo.IsInvalid = true;
                    auditRepository.Update(auditInfo);
                }
                ShowMessage(true, "作廢成功");
            }
            catch (Exception e)
            {
                ShowMessage(false, "作廢失敗");
            }
            return RedirectToAction("QuarterEdit", "PersonKPIAdmin", new { PersonId = personId });
        }

        #region Person KPI Ajax
        /// <summary>
        /// 加入KPI
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string JoinKPI(int kpiid, string personId, string deptId)
        {
            try
            {
                var kpiInfo = kPIRepository.GetById(kpiid);
                if (kpiInfo != null)
                {
                    PersonKPI data = new PersonKPI();
                    data.KPIID = kpiid;
                    data.KPITitle = kpiInfo.Title;
                    data.KPIDescription = kpiInfo.Description;
                    data.PersonId = personId;
                    data.DeptId = deptId;
                    data.State = 1;
                    data.ApprovalState = 0;
                    data.IsLatest = true;
                    data.IsInvalid = false;
                    data.CreateDate = DateTime.Now;
                    personKPIRepository.Insert(data);

                    #region 建立空白進度 四個季度
                    for (int i = 1; i <= 4; i++)
                    {
                        personKPIStateRepository.Insert(new PersonKPIState()
                        {
                            PersonKPIID = data.ID,
                            PersonID = personId,
                            DeptID = deptId,
                            Sort = 0,
                            Title = "Q" + i.ToString(),
                            IsAssessed = false,
                            IsLatest = true,
                            IsFinished = false,
                            CreateDate = data.CreateDate
                        });
                    }
                    #endregion

                    return "success";
                }
            }
            catch (Exception e)
            {
                
            }
            return "failed";
        }

        /// <summary>
        /// 取得個人目標
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetInfo(int id)
        {
            var result = Mapper.Map<PersonKPIView>(personKPIRepository.GetById(id));
            result.StateList = Mapper.Map<List<PersonKPIStateView>>(personKPIStateRepository.Query(null, id).OrderByDescending(q => q.CreateDate));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 儲存個人目標
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult SavePersonKPI(PersonKPI data)
        {
            try
            {
                //var query = new PersonKPI();
                data.IsLatest = true;
                data.IsInvalid = false;
                if (!Convert.ToBoolean(data.IsAudit))
                {
                    data.OfficerAudit = Approving;
                    data.ManagerAudit = Approving;
                    data.ViceAudit = Approving;
                    data.PresidentAudit = Approving;
                }
                else
                {
                    var auditData = personKPIRepository.GetById(data.ID);
                    data.OfficerAudit = (int)ApprovalState.Approved;
                    data.ManagerAudit = auditData.ManagerAudit == 0 ? Approving : auditData.ManagerAudit;
                    data.ViceAudit = auditData.ViceAudit == 0 ? Approving : auditData.ViceAudit;
                    data.PresidentAudit = auditData.PresidentAudit == 0 ? Approving : auditData.PresidentAudit;
                }
                if (data.ID == 0)
                {
                    data.CreateDate = DateTime.Now;
                    data.ID = personKPIRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    var query = personKPIRepository.GetById(data.ID);
                  
                    data.IsLatest = true;
                    data.CreateDate = query.CreateDate;
                    personKPIRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 送審
                //if (data.ApprovalState == Approving)
                //{
                //    var newCheck = new Check()
                //    {
                //        TargetID = data.ID,
                //        TargetType = (int)TargetType.PersonKPI,
                //        ApprovalState = Approving,
                //        PersonID = data.PersonId,

                //        CreateDate = DateTime.Now,
                //        Creater = AdminInfoHelper.GetAdminInfo().ID,
                //    };
                //    checkRepository.Insert(newCheck);
                //}
                #endregion

                //query = personKPIRepository.GetById(id);
                var result = Mapper.Map<PersonKPIView>(data);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        /// <summary>
        /// 作廢 Person KPI
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult InvalidInfo(int id)
        {
            var data = personKPIRepository.GetById(id);
            data.IsInvalid = true;
            personKPIRepository.Update(data);

            return Json("", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Person KPI State Ajax
        /// <summary>
        /// 取得Person KPI 進度清單
        /// </summary>
        /// <param name="personkpi_id"></param>
        /// <returns></returns>
        public JsonResult GetState(int personkpiId)
        {
            var result = Mapper.Map<List<PersonKPIStateView>>(personKPIStateRepository.Query(null, personkpiId, true));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得Person KPI 進度資料
        /// </summary>
        /// <param name="personkpi_id"></param>
        /// <returns></returns>
        public JsonResult GetStateInfo(string personId, string deptId, int stateId)
        {
            var result = Mapper.Map<PersonKPIStateView>(personKPIStateRepository.GetById(stateId));
            result.PersonID = personId;
            result.DeptId = deptId;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得Person KPI 月目標資料
        /// </summary>
        /// <param name="personkpi_id"></param>
        /// <returns></returns>
        public JsonResult GetMonthKPIInfo(string personId, string deptId, int personkpiId, int month)
        {
            var result = Mapper.Map<PersonKPIMonthView>(personKPIMonthRepository.FindByMonth(personId, personkpiId, month));
            if (result == null)
            {
                result = new PersonKPIMonthView();
            }
            result.PersonID = personId;
            result.DeptId = deptId;
            result.PersonKPIID = personkpiId;
            result.Month = month;

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string SaveStates(string stateDatas)
        {
            try
            {
                if (!string.IsNullOrEmpty(stateDatas))
                {
                    //var json = JObject.Parse(stateDatas);
                    var datas = JsonConvert.DeserializeObject<stateDataModel>(stateDatas);
                    var dataList = datas.datas;

                    #region 更新副指標
                    var personKPI = personKPIRepository.GetById(dataList.FirstOrDefault().PersonKPIID);

                    #region 新增個人目標 Latest version
                    var newPersonKPI = new PersonKPI();
                    newPersonKPI.KPIID = personKPI.KPIID;
                    newPersonKPI.KPITitle = personKPI.KPITitle;
                    newPersonKPI.KPIDescription = personKPI.KPIDescription;
                    newPersonKPI.PersonId = personKPI.PersonId;
                    newPersonKPI.DeptId = personKPI.DeptId;
                    newPersonKPI.State = personKPI.State;
                    newPersonKPI.ApprovalState = Draft;//新版設為草稿
                    newPersonKPI.IsLatest = true;//設為新版
                    newPersonKPI.IsInvalid = false;//啟用
                    newPersonKPI.CreateDate = DateTime.Now;

                    int newId = personKPIRepository.Insert(newPersonKPI);
                    #endregion

                    #region 改為個人目標 Old version
                    personKPI.IsLatest = false;//改為舊版
                                               //personKPI.ApprovalState = Draft;
                    personKPI.IsInvalid = true;//作廢
                    personKPIRepository.Update(personKPI);
                    #endregion

                    #region 清除舊紀錄
                    var logs = personKPIRepository.Query(null, false, personKPI.KPIID, personKPI.PersonId, personKPI.DeptId, 0, true).OrderByDescending(q => q.ID).ToList();
                    if (logs.Count() > 10)
                    {
                        try
                        {
                            foreach (var item in logs.Skip(10))
                            {
                                personKPIRepository.Delete(item.ID);
                                var logStates = personKPIStateRepository.Query(null, item.ID).ToList();
                                if (logStates.Count() > 0)
                                {
                                    foreach (var logState in logStates)
                                    {
                                        personKPIStateRepository.Delete(logState.ID);
                                    }
                                }
                            }
                        }
                        catch (Exception e)
                        {

                            throw;
                        }
                    }
                    #endregion
                    #endregion

                    #region MyRegion
                    if (dataList != null && dataList.Count() > 0)
                    {
                        foreach (var item in dataList)
                        {
                            //newId
                            var newState = new PersonKPIState();
                            newState.PersonKPIID = newId;
                            newState.PersonID = item.PersonID;
                            newState.DeptID = item.DeptID;
                            newState.Sort = item.Sort;
                            newState.Title = item.Title;
                            newState.Description = item.Description;
                            newState.TargetNum = item.TargetNum;
                            newState.TargetUnit = item.TargetUnit;
                            newState.Percentage = item.Percentage;
                            newState.Quarter = item.Quarter;
                            newState.ApprovalState = item.ApprovalState;
                            newState.IsLatest = item.IsLatest;
                            newState.IsFinished = item.IsFinished;
                            newState.FinishDate = item.FinishDate;
                            newState.CheckDate = item.CheckDate;
                            newState.CreateDate = item.CreateDate;
                            personKPIStateRepository.Insert(newState);
                        }
                    }
                    #endregion

                    return "success";
                }
                return "empty";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        /// <summary>
        /// 儲存個人目標進度
        /// </summary>
        /// <param name="id"></param>
        /// <param name="personkpiid"></param>
        /// <param name="personid"></param>
        /// <param name="deptid"></param>
        /// <param name="title"></param>
        /// <param name="description"></param>
        /// <param name="num"></param>
        /// <param name="unit"></param>
        /// <param name="approvalState"></param>
        /// <param name="sort"></param>
        /// <param name="isfinished"></param>
        /// <param name="checkdate"></param>
        /// <returns></returns>
        public JsonResult SaveState(int id, int personkpiid, string personid, string deptid, string title, string description, int num, string unit, int approvalState, int sort, bool isfinished, DateTime checkdate)
        {
            try
            {
                var query = new PersonKPIState();

                #region 更新狀態
                #region 更新副指標
                var personKPI = personKPIRepository.GetById(personkpiid);

                #region 新增個人目標 Latest version
                var newPersonKPI = new PersonKPI();
                newPersonKPI.KPIID = personKPI.KPIID;
                newPersonKPI.KPITitle = personKPI.KPITitle;
                newPersonKPI.KPIDescription = personKPI.KPIDescription;
                newPersonKPI.PersonId = personKPI.PersonId;
                newPersonKPI.DeptId = personKPI.DeptId;
                newPersonKPI.State = personKPI.State;
                newPersonKPI.ApprovalState = Draft;//新版設為草稿
                newPersonKPI.IsLatest = true;//設為新版
                newPersonKPI.IsInvalid = false;//啟用
                newPersonKPI.CreateDate = DateTime.Now;

                int newId = personKPIRepository.Insert(newPersonKPI);
                #endregion

                #region 改為個人目標 Old version
                personKPI.IsLatest = false;//改為舊版
                //personKPI.ApprovalState = Draft;
                personKPI.IsInvalid = true;//作廢
                personKPIRepository.Update(personKPI);
                #endregion

                #region 清除舊紀錄
                var logs = personKPIRepository.Query(null, false, personKPI.KPIID, personKPI.PersonId, personKPI.DeptId, 0, true).OrderByDescending(q => q.ID).ToList();
                if (logs.Count() > 10)
                {
                    try
                    {
                        foreach (var item in logs.Skip(10))
                        {
                            personKPIRepository.Delete(item.ID);
                            var logStates = personKPIStateRepository.Query(null, item.ID).ToList();
                            if (logStates.Count() > 0)
                            {
                                foreach (var logState in logStates)
                                {
                                    personKPIStateRepository.Delete(logState.ID);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {

                        throw;
                    }
                }
                #endregion
                #endregion

                #region 複製個人進度到 Latest version
                var existStateList = personKPIStateRepository.Query(null, personkpiid).ToList();
                foreach (var item in existStateList)
                {
                    var newState = new PersonKPIState();
                    if (id == item.ID)
                    {
                        newState.PersonKPIID = newId;
                        newState.PersonID = personid;
                        newState.DeptID = deptid;
                        newState.Sort = sort;
                        newState.Title = title;
                        newState.Description = description;
                        newState.TargetNum = num;
                        newState.TargetUnit = unit;
                        newState.ApprovalState = approvalState;
                        newState.IsLatest = true;
                        newState.IsFinished = isfinished;
                        newState.FinishDate = item.FinishDate;
                        newState.CheckDate = checkdate;
                        newState.CreateDate = item.CreateDate;
                        id = personKPIStateRepository.Insert(newState);
                    }
                    else
                    {
                        newState.PersonKPIID = newId;
                        newState.PersonID = item.PersonID;
                        newState.DeptID = item.DeptID;
                        newState.Sort = item.Sort;
                        newState.Title = item.Title;
                        newState.Description = item.Description;
                        newState.TargetNum = item.TargetNum;
                        newState.TargetUnit = item.TargetUnit;
                        newState.ApprovalState = item.ApprovalState;
                        newState.IsLatest = item.IsLatest;
                        newState.IsFinished = item.IsFinished;
                        newState.FinishDate = item.FinishDate;
                        newState.CheckDate = item.CheckDate;
                        newState.CreateDate = item.CreateDate;
                        id = personKPIStateRepository.Insert(newState);
                    }
            
                }
                #endregion

                //if (id == 0)
                //{//新增
                //    query = new PersonKPIState()
                //    {
                //        ID = id,
                //        PersonKPIID = newId,
                //        PersonID = personid,
                //        DeptID = deptid,
                //        Sort = sort,
                //        Title = title,
                //        TargetNum = num,
                //        TargetUnit = unit,
                //        ApprovalState = approvalState,
                //        Description = description,
                //        IsFinished = isfinished,
                //        IsLatest = true,
                //        CreateDate = DateTime.Now,
                //        CheckDate = checkdate,
                //    };
                //    id = personKPIStateRepository.Insert(query);
                //    ShowMessage(true, "新增成功");
                //}
                //else
                //{//更新
                //    query = personKPIStateRepository.GetById(id);
                //    query.ID = id;
                //    query.PersonKPIID = newId;
                //    query.PersonID = personid;
                //    query.DeptID = deptid;
                //    query.Sort = sort;
                //    query.Title = title;
                //    query.TargetNum = num;
                //    query.TargetUnit = unit;
                //    query.ApprovalState = approvalState;
                //    query.Description = description;
                //    query.IsFinished = isfinished;
                //    query.IsLatest = true;
                //    query.CheckDate = checkdate;
                //    personKPIStateRepository.Update(query);
                //    ShowMessage(true, "修改成功");
                //}

                var lastFinished = personKPIStateRepository.Query(null, newId).OrderByDescending(q => q.Sort).ThenByDescending(q => q.ID).Where(q => q.IsFinished && q.IsLatest).FirstOrDefault();
                if (lastFinished != null)
                {
                    personKPI = personKPIRepository.GetById(newId);
                    personKPI.State = lastFinished.ID;
                    personKPIRepository.Update(personKPI);
                }
                ShowMessage(true, "儲存成功");

                #region 進度送審(停用)
                if (approvalState == (int)ApprovalState.Approving)
                {
                    var newCheck = new Check()
                    {
                        TargetID = id,
                        TargetType = (int)TargetType.PersonKPIState,
                        ApprovalState = (int)ApprovalState.Approving,
                        CreateDate = DateTime.Now,
                        Creater = AdminInfoHelper.GetAdminInfo().ID,
                    };
                    checkRepository.Insert(newCheck);
                }
                #endregion

                #endregion

                query = personKPIStateRepository.GetById(id);
                var result = Mapper.Map<PersonKPIStateView>(query);

                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }
        }
        #endregion
    }

    public class stateDataModel
    {
        public List<PersonKPIState> datas {get; set;}
    }
}