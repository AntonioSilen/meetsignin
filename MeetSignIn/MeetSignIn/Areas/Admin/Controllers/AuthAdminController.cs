﻿using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Auth;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MeetSignIn.Areas.Admin.Controllers
{
    //[ErrorHandleAdminActionFilter]
    public class AuthAdminController : Controller
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        // GET: Admin/AccountAdmin
        public ActionResult Login(string ReturnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
            {
                Session.RemoveAll();
                FormsAuthentication.SignOut();
                return RedirectToAction("Login");
            }
            if (AdminInfoHelper.GetAdminInfo() != null)
            {
                return RedirectToAction("Index", "HomeAdmin", new { lang = "zh_TW" });
            }
            //#if DEBUG
            //            if (System.Diagnostics.Debugger.IsAttached)
            //            {
            //                var controller = DependencyResolver.Current.GetService<AuthAdminController>();
            //                controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
            //                return controller.Login(new LoginView() { Account = "fairlyadmin", Password = "123456" });
            //            }
            //#endif
            var login = new LoginView();
            login.ReturnUrl = ReturnUrl;
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                var admin = adminRepository.Login(login.Account, login.Password);
                if (admin != null)
                {
                    SetLogin(login, admin);

                    //var ReturnUrl = Request.QueryString["ReturnUrl"];
                    if (!string.IsNullOrEmpty(login.ReturnUrl) && login.ReturnUrl.IndexOf("AuthAdmin") < 0)
                    {
                        if (login.ReturnUrl.IndexOf("ClassOpinionAdmin") > 0)
                        {
                            login.ReturnUrl += "&lang=" + login.Language;
                        }
                        return Redirect(login.ReturnUrl);
                    }
                    else
                    {
                        //var ReturnUrl = FormsAuthentication.GetRedirectUrl(login.Account, false);
                        return RedirectToAction("Index", "HomeAdmin", new { lang = login.Language });
                    }
                }
                else
                {
                    var t8Person = t8personRepository.GetPersonQuery("", login.Account).FirstOrDefault();
                    if (t8Person != null)
                    {
                        if (adminRepository.Query(null, 0, login.Account).FirstOrDefault() == null)
                        {
                            DateTime now = DateTime.Now;
                            var newPerson = new MainAdmin
                            {
                                Account = login.Account,
                                Password = t8Person.Birthday.ToString(),
                                Type = (int)AdminType.Employee,
                                Name = t8Person.Name,
                                DeptId = "",
                                Department = "",
                                Status = true,
                                UpdateDate = now,
                                Updater = 1,
                                CreateDate = now,
                                Creater = 1
                            };
                            newPerson.ID = adminRepository.Insert(newPerson);

                            if (login.Password == newPerson.Password)
                            {
                                SetLogin(login, newPerson);

                                if (!string.IsNullOrEmpty(login.ReturnUrl) && login.ReturnUrl.IndexOf("AuthAdmin") < 0)
                                {
                                    if (login.ReturnUrl.IndexOf("ClassOpinionAdmin") > 0)
                                    {
                                        login.ReturnUrl += "&lang=" + login.Language;
                                    }
                                    return Redirect(login.ReturnUrl);
                                }
                                else
                                {
                                    return RedirectToAction("Index", "HomeAdmin", new { lang = login.Language });
                                }
                            }
                        }
                    }
                }
            }
            return View(login);
        }

        private static void SetLogin(LoginView login, Models.MainAdmin admin)
        {
            Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
            AdminInfo adminInfo = new AdminInfo();
            adminInfo.ID = admin.ID;
            adminInfo.Account = admin.Account;
            adminInfo.Type = admin.Type;
            adminInfo.IsPrincipal = admin.IsPrincipal;
            adminInfo.Name = admin.Name;
            var personInfo = t8personRepository.FindByPersonId(admin.Account);
            adminInfo.DeptId = personInfo == null ? "0000" : personInfo.DeptID;
            adminInfo.DeptIName = personInfo == null ? "菲力工業" : personInfo.DeptName;
            AdminInfoHelper.Login(adminInfo, login.RememberMe);
        }

        [Authorize]
        public ActionResult Logout()
        {
            AdminInfoHelper.Logout();
            return RedirectToAction("Login");
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            //記錄錯誤訊息

            //log.record(filterContext.Exception)

            // 如果登入 Cookie 過期 or 有可能案上一頁又在登入一次，造成表單防偽驗證失敗拋出 Exception，導回 Login 頁面重新登入
            if (filterContext.Exception is HttpAntiForgeryException)
            {
                var queryString = filterContext.HttpContext.Request.QueryString["ReturnUrl"];
                var loginUrl = @Url.Action("Login", "AuthAdmin");
                // 登入後又在一次登入
                if (filterContext.RequestContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    // 取得剛剛 Exception 前的表單 Url
                    var returnUrl = filterContext.HttpContext.Request.Path;
                    if (string.IsNullOrEmpty(queryString))
                        filterContext.Result = new RedirectResult(loginUrl);
                    else
                        filterContext.Result = new RedirectResult($"{queryString}");
                }
                else
                {
                    if (!string.IsNullOrEmpty(queryString))
                    {
                        loginUrl = loginUrl + "?ReturnUrl=" + queryString;
                    }
                    filterContext.Result = new RedirectResult(loginUrl);
                }

                // 表示已經處理此例外事件
                filterContext.ExceptionHandled = true;
            }
            else
            {
                base.OnException(filterContext);
            }
        }
    }
}