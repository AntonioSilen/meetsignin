﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Areas.Admin.ViewModels.PFWorks;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class PFWorksAdminController : BaseAdminController
    {
        private PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
        private PFWorksRepository pFWorksRepository = new PFWorksRepository(new MeetingSignInDBEntities());
        private PFWorksRelationRepository pFWorksRelationRepository = new PFWorksRelationRepository(new MeetingSignInDBEntities());
        private PFWorkRepository pFWorkRepository = new PFWorkRepository(new MeetingSignInDBEntities());
        private PFSeasonRepository pFSeasonRepository = new PFSeasonRepository(new MeetingSignInDBEntities());
        private comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
        public string CLS = EnumHelper.GetDescription(LevelType.CLS);
        public string CATE = EnumHelper.GetDescription(LevelType.CATE);
        public string ITM = EnumHelper.GetDescription(LevelType.ITM);
        public string LVL = EnumHelper.GetDescription(LevelType.LVL);
        public string WORK = EnumHelper.GetDescription(LevelType.WORK);

        public ActionResult Index(PFWorksIndexView model)
        {
            if (string.IsNullOrEmpty(model.DeptId) || model.ClassID == 0)
            {
                return RedirectToAction("Index", "PFCategoryAdmin");
            }

            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;

            //model.TitleList = pFWorksRepository.GetTitles(model.ClassID, model.ItemID, model.DeptId).Distinct().ToList();
            model.WorkList = pFWorkRepository.Query(null, model.ClassID, model.DeptId).ToList();

            return View(model);
        }

        public ActionResult Edit(int id = 0, string DeptId = "", int ClassID = 0)
        {
            var model = new PFWorksIndexView();
            if (id != 0)
            {
                model = Mapper.Map<PFWorksIndexView>(pFWorkRepository.GetById(id));
            }
            else
            {
                if (string.IsNullOrEmpty(DeptId) || ClassID == 0)
                {
                    return RedirectToAction("Index", "PFCategoryAdmin");
                }
                model.DeptId = DeptId;
                 model.ClassID = ClassID;
            }
           
            var deptInfo = comDepartmentRepository.FindByDeptId(DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;
           

            var categoryList = pFCategoryRepository.Query(true, model.DeptId, model.ClassID, CATE);
            model.ClassCatesList = Mapper.Map<List<PFCategoryCateView>>(categoryList);
            model.Points = 0;
            foreach (var cate in model.ClassCatesList)
            {
                var itemList = pFCategoryRepository.Query(true, model.DeptId, cate.ID, ITM, null, 1);
                cate.ItmList = Mapper.Map<List<PFCategoryItmView>>(itemList);
                foreach (var item in cate.ItmList)
                {
                    var lvlList = pFCategoryRepository.Query(true, model.DeptId, item.ID, LVL);
                    item.LvlList = Mapper.Map<List<PFCategoryLvlView>>(lvlList);
                }
            }

            return View(model);
        }

        public ActionResult Edit_BK(string DeptId = "", int ClassID = 0, string Title = "")
        {
            if (string.IsNullOrEmpty(DeptId) || ClassID == 0)
            {
                return RedirectToAction("Index", "PFCategoryAdmin");
            }
            var model = new PFWorksIndexView();
            model.Title = Title;

            model.DeptId = DeptId;
            var deptInfo = comDepartmentRepository.FindByDeptId(DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;
            model.ClassID = ClassID;

            var categoryList = pFCategoryRepository.Query(true, model.DeptId, model.ClassID, CATE);
            model.ClassCatesList = Mapper.Map<List<PFCategoryCateView>>(categoryList);
            model.Points = 0;
            foreach (var cate in model.ClassCatesList)
            {
                var itemList = pFCategoryRepository.Query(true, model.DeptId, cate.ID, ITM, null, 1);
                cate.ItmList = Mapper.Map<List<PFCategoryItmView>>(itemList);
                foreach (var item in cate.ItmList)
                {
                    var lvlList = pFCategoryRepository.Query(true, model.DeptId, item.ID, LVL);
                    item.LvlList = Mapper.Map<List<PFCategoryLvlView>>(lvlList);
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(PFWorksView model)
        {
            var deptArr = model.DeptId.Split('_');
            model.DeptId = deptArr[0];
            model.DeptName = deptArr[1];
            if (ModelState.IsValid)
            {
                PFWorks data = Mapper.Map<PFWorks>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                //if (model.ID == 0)
                //{
                //    model.ID = pFWorksRepository.Insert(data);
                //    ShowMessage(true, "新增成功");
                //}
                //else
                //{
                //    pFWorksRepository.Update(data);
                //    ShowMessage(true, "修改成功");
                //}

                return RedirectToAction("Edit", new { id = model.ID, DeptId = model.DeptId + "_" + model.DeptName, ClassID = model.ClassID });
            }

            return View(model);
        }

        public JsonResult GetWorks(int workId, string title, int itemId, int classId, string deptId)
        {
            var result = Mapper.Map<PFWorksView>(pFWorksRelationRepository.Query(null, classId, workId, itemId, deptId, title).FirstOrDefault());
            if (result == null)
            {
                result = new PFWorksView();
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public string SaveWorks(int id, string title, int classId, string deptId, int points, string jsonData)
        {
            try
            {
                //儲存作業
                PFWork data = new PFWork();
                if (id == 0)
                {
                    data.Title = title;
                    data.ClassID = classId;
                    data.DeptId = deptId;
                    data.IsOnline = true;
                    data.Points = points;
                    id = pFWorkRepository.Insert(data);
                }
                else
                {
                    data = pFWorkRepository.GetById(id);
                    data.Title = title;
                    data.ClassID = classId;
                    data.DeptId = deptId;
                    data.IsOnline = true;
                    data.Points = points;
                    pFWorkRepository.Update(data);
                }

                //儲存作業關聯
                var saveData = Newtonsoft.Json.JsonConvert.DeserializeObject<PFWorksSaveModel>(jsonData);
                foreach (var item in saveData.saveData)
                {
                    var lvlInfo = pFCategoryRepository.GetById(item.LvlID);
                    var rdata = new PFWorksRelation();
                    if (item.ID != 0)
                    {
                        rdata = pFWorksRelationRepository.GetById(item.ID);
                        rdata.WorkID = id;
                        rdata.LvlID = item.LvlID;
                        rdata.IsOnline = true;
                        rdata.Points = lvlInfo == null ? 0 : lvlInfo.Points;
                        pFWorksRelationRepository.Update(rdata);
                    }
                    else
                    {
                        rdata.Title = title;
                        rdata.ClassID = classId;
                        rdata.DeptId = deptId;
                        rdata.WorkID = id;
                        rdata.ItemID = item.ItemID;
                        rdata.LvlID = item.LvlID;
                        rdata.IsOnline = true;
                        rdata.Points = lvlInfo == null ? 0 : lvlInfo.Points;
                        item.ID = pFWorksRelationRepository.Insert(rdata);
                    }
                }
                ShowMessage(true, "儲存成功");
                return id.ToString();
            }
            catch (Exception e)
            {
                ShowMessage(false, "儲存失敗");
                return "failed";
            }
        }

        public string SaveWorks_BK(int id, string title, int classId, string deptId, string jsonData)
        {
            try
            {
                PFWork data = new PFWork();
                if (id == 0)
                {
                    data.Title = title;
                    data.ClassID = classId;
                    data.DeptId = deptId;
                    data.IsOnline = true;
                    id = pFWorkRepository.Insert(data);
                }
                else
                {
                    data = pFWorkRepository.GetById(id);
                    data.Title = title;
                    data.ClassID = classId;
                    data.DeptId = deptId;
                    data.IsOnline = true;
                    pFWorkRepository.Update(data);
                }

                var saveData = Newtonsoft.Json.JsonConvert.DeserializeObject<PFWorksSaveModel>(jsonData);
                foreach (var item in saveData.saveData)
                {
                    var lvlInfo = pFCategoryRepository.GetById(item.LvlID);
                    var rdata = new PFWorksRelation();
                    if (item.ID != 0)
                    {
                        rdata = pFWorksRelationRepository.GetById(item.ID);
                        rdata.LvlID = item.LvlID;
                        rdata.IsOnline = true;
                        rdata.Points = lvlInfo == null ? 0 : lvlInfo.Points;
                        pFWorksRelationRepository.Update(rdata);
                    }
                    else
                    {
                        data.Title = title;
                        data.ClassID = classId;
                        data.DeptId = deptId;
                        rdata.ItemID = item.ItemID;
                        rdata.LvlID = item.LvlID;
                        rdata.IsOnline = true;
                        rdata.Points = lvlInfo == null ? 0 : lvlInfo.Points;
                        item.ID = pFWorksRelationRepository.Insert(rdata);
                    }
                }
                ShowMessage(true, "儲存成功");
                return saveData.saveData.FirstOrDefault().Title;
            }
            catch (Exception e)
            {
                ShowMessage(false, "儲存失敗");
                return "failed";
            }
        }
    }

    public class PFWorksSaveModel
    {
        public List<PFWorks> saveData { get; set; }
    }
}