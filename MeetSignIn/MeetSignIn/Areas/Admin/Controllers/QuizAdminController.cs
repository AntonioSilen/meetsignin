﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.OptionGroup;
using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Areas.Admin.ViewModels.Quiz;
using MeetSignIn.Areas.Admin.ViewModels.QuizResult;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class QuizAdminController : BaseAdminController
    {
        private QNRRepository qNRRepository = new QNRRepository(new MeetingSignInDBEntities());
        private QNRQuestionRepository qNRQuestionRepository = new QNRQuestionRepository(new MeetingSignInDBEntities());
        private QNRResultRepository qNRResultRepository = new QNRResultRepository(new MeetingSignInDBEntities());
        private QuestionDBRepository questionDBRepository = new QuestionDBRepository(new MeetingSignInDBEntities());
        private OptionItemRepository optionItemRepository = new OptionItemRepository(new MeetingSignInDBEntities());

        // GET: Admin/ExamAdmin
        /// <summary>
        /// 試卷管理不需要連到列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult Index(QuizIndexView model)
        {
            var query = qNRRepository.Query(model.IsOnline, model.Title, model.Description, model.Year, model.Months);
            var pageResult = query.ToPageResult<QNR>(model);
            model.PageResult = Mapper.Map<PageResult<QuizView>>(pageResult);

            return View(model);
        }

        /// <summary>
        /// 編輯試卷
        /// </summary>
        /// <param name="tid"></param>
        /// <returns></returns>
        public ActionResult Edit(int tid = 0)
        {
            if (tid == 0)
            {
                return RedirectToAction("Index", "TrainingAdmin");
            }
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var model = new QNRView();
            model.TrainingID = tid;
            var qnrData = qNRRepository.FindByTrainingId(tid);
            if (qnrData != null)
            {
                model = Mapper.Map<QNRView>(qnrData);
                model.QuizQuestionList = Mapper.Map<List<ViewModels.QNR.QuizQuestionView>>(qNRQuestionRepository.Query(null, qnrData.ID));

                model.PersonResultList = new List<QNRPersonResultView>();
                var personIdList = qNRResultRepository.Query(qnrData.ID).Select(q => q.PersonId).Distinct().ToList();
                foreach (var personId in personIdList)
                {
                    var gradeData = new QNRPersonResultView();
                    gradeData.TotalPoints = 0;
                    var personQuizResult = qNRResultRepository.Query(qnrData.ID, 0, personId);
                    if (personQuizResult.Count() > 0)
                    {
                        var firstResult = personQuizResult.FirstOrDefault();
                        var personInfo = t8personRepository.FindByPersonId(personId);

                        gradeData.QNRID = qnrData.ID;
                        gradeData.PersonId = personId;
                        gradeData.PersonName = personInfo.Name;
                        gradeData.DeptId = personInfo.DeptId;
                        gradeData.DeptName = personInfo.DeptName;
                        gradeData.CreateDate = firstResult.CreateDate;

                        gradeData.TotalPoints = personQuizResult.Select(q => q.CorrectPoints).Sum();
                        gradeData.CorrectNum = personQuizResult.Where(q => q.CorrectPoints > 0).Count();
                        
                        model.PersonResultList.Add(gradeData);
                    }
                }
                model.FullPoints = model.QuizQuestionList.Select(q => q.Points).ToList().Sum();
            }
            else
            {
                qNRRepository.Insert(new QNR() {
                    TrainingID = tid,
                    Title = "",
                    Description = "",
                    Depts = "",
                    Year = DateTime.Now.Year,
                    Month = DateTime.Now.Month,
                    IsOnline = true,
                    CreateDate = DateTime.Now,
                    CreatePersonId = adminInfo.Account,
                });;
                model.QuizQuestionList = new List<ViewModels.QNR.QuizQuestionView>();
                model.PersonResultList = new List<QNRPersonResultView>();
            }            

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(QNRView model)
        {
            if (ModelState.IsValid)
            {
                QNR data = Mapper.Map<QNR>(model);
                data.Depts = "";
                var adminInfo = AdminInfoHelper.GetAdminInfo();

                if (model.ID == 0)
                {
                    data.CreatePersonId = adminInfo.Account;
                    model.ID = qNRRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    qNRRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }
            }

            return RedirectToAction("Edit", new { tid = model.TrainingID });
        }

        public ActionResult CopyQuiz(int id)
        {
            try
            {
                var model = new QNRView();
                var query = qNRRepository.FindBy(id);
                model = Mapper.Map<QNRView>(query);

                #region 建立新試卷
                var data = Mapper.Map<QNR>(model);
                data.ID = 0;
                data.Title += "---複製";
                data.Description = query.Description;
                data.Depts = query.Depts;
                data.Year = query.Year;
                data.Month = query.Month;
                data.IsOnline = false;
                data.CreateDate = DateTime.Now;

                int nId = qNRRepository.Insert(data);
                #endregion

                var qnestionList = qNRQuestionRepository.Query(null, query.ID).ToList();

                #region 複製問題
                //新增名單                   
                foreach (var item in qnestionList)
                {
                    var result = qNRQuestionRepository.Insert(new QNRQuestion()
                    {
                        QNRID = nId,
                        Question = item.Question,
                        AnswerType = item.AnswerType,
                        OptionGroup = item.OptionGroup,
                        IsRequired = item.IsRequired,
                        Sort = item.Sort,
                    });
                }
                #endregion

                return RedirectToAction("Edit", new { id = nId });
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult Delete(int id)
        {
            qNRRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        public string SaveQuestion(int id, int qnrId, string question, int sort, int points, string options)
        {
            try
            {
                if (id != 0)
                {
                    var query = qNRQuestionRepository.GetById(id);
                    //query.QNRID = qnrId;
                    query.Question = question;
                    //query.AnswerType = 4;
                    query.Sort = sort;
                    query.Options = options;
                    query.Points = points;
                    //query.OptionGroup = 0;
                    //query.IsRequired = true;
                    qNRQuestionRepository.Update(query);
                }
                else
                {
                    id = qNRQuestionRepository.Insert(new QNRQuestion()
                    {
                        QNRID = qnrId,
                        Question = question,
                        AnswerType = 4,
                        Sort = sort,
                        Options = options,
                        Points = points,
                        OptionGroup = 0,
                        IsRequired = true
                    });
                }

                //加入題庫
                var questionDBQuery = questionDBRepository.Query(null, question, 0);
                if (questionDBQuery.Count() <= 0)
                {
                    var questionData = qNRQuestionRepository.GetById(id);

                    var newqData = new QuestionDB();
                    newqData.Question = questionData.Question;
                    newqData.AnswerType = questionData.AnswerType;
                    newqData.OptionGroup = questionData.OptionGroup;
                    newqData.IsRequired = questionData.IsRequired;
                    newqData.Sort = questionData.Sort;
                    newqData.Points = questionData.Points;
                    newqData.Options = questionData.Options;
                    
                    newqData.UsedQNRIDs = id.ToString();
                    questionDBRepository.Insert(newqData);
                }
                else
                {
                    var questionDBData = questionDBRepository.Query().FirstOrDefault();
                    var qIdList = questionDBData.UsedQNRIDs.Split(',').ToList();
                    if (!qIdList.Contains(id.ToString()))
                    {
                        questionDBData.UsedQNRIDs += "," + id.ToString();
                        questionDBRepository.Update(questionDBData);
                    }                  
                }

                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

        public JsonResult GetQuestions()
        {
            var query = qNRQuestionRepository.Query().Where(q => q.Points > 0 && q.AnswerType == 4);

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuestion(int questionId)
        {
            var query = qNRQuestionRepository.GetById(questionId);

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuestionByString(string question)
        {
            var query = qNRQuestionRepository.Query(null, 0, question).FirstOrDefault();

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQuizResult(string personId, int qnrid)
        {
            var result = Mapper.Map<QNRView>(qNRRepository.GetById(qnrid));
            result.QuizQuestionList = Mapper.Map<List<Areas.Admin.ViewModels.QNR.QuizQuestionView>>(qNRQuestionRepository.Query(null, qnrid));
            result.TotalPoints = 0;
            foreach (var item in result.QuizQuestionList)
            {
                item.OptionsList = new List<SelectListItem>();
                if (item != null && !string.IsNullOrEmpty(item.Options))
                {
                    var optionsArr = item.Options.Split('}');
                    for (int i = 0; i < optionsArr.Count(); i++)
                    {
                        var opt = optionsArr[i];
                        //取得正確答案和此題是否正確
                        if (optionsArr[i].Contains("@@"))
                        {
                            item.Answer = (i + 1).ToString();
                        }
                        if (!string.IsNullOrEmpty(opt))
                        {
                            item.OptionsList.Add(new SelectListItem()
                            {
                                Text = opt.Replace("{", "").Replace("@@", ""),
                                Value = (i + 1).ToString(),
                            });
                        }
                    }
                    var resultData = qNRResultRepository.Query(qnrid, item.ID, personId).FirstOrDefault();
                    item.IsCorrect = item.Answer == resultData.Answer;
                    if (item.IsCorrect)
                    {
                        result.TotalPoints += item.Points;
                    }
                    item.ChooseAnswer = resultData.Answer;
                }
            }
            result.FullPoints = result.QuizQuestionList.Select(q => q.Points).ToList().Sum();
            result.CorrectNum = result.QuizQuestionList.Where(q => q.IsCorrect).Count();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetOptions(int groupId, int qId)
        {
            var query = Mapper.Map<List<OptionItemView>>(optionItemRepository.Query(true, groupId));
            foreach (var item in query)
            {
                var linkqInfo = qNRQuestionRepository.FindBy(qId);
                if (linkqInfo == null)
                {
                    return Json(new List<OptionItemView>(), JsonRequestBehavior.AllowGet);
                }
                item.LinkQNRQID = linkqInfo.ID;
                item.LinkQNRQTitle = linkqInfo.Question;
                var qInfo = qNRQuestionRepository.FindLinkedQuestion(qId, item.ID);
                item.QNRQID = 0;
                item.QNRQTitle = "";
                if (qInfo != null)
                {
                    item.QNRQID = qInfo.ID;
                    item.QNRQTitle = qInfo.Question;
                }
            }

            return Json(query, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClearQuizResult(int qid = 0)
        {
            var qnrData = qNRRepository.GetById(qid);
            var queryList = qNRResultRepository.Query(qid).ToList();
            foreach (var item in queryList)
            {
                qNRResultRepository.Delete(item.ID);
            }

            return RedirectToAction("Edit", "QuizAdmin", new { tid = qnrData.TrainingID });
        }

        public string DeleteQuestion(int Id)
        {
            try
            {
                qNRQuestionRepository.Delete(Id);
                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }
    }
}