﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.KPI;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class FairlyKPIAdminController : BaseAdminController
    {
        // GET: Admin/FairlyKPIAdmin
        private KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
        private KPIRelationRepository kPIRelationRepository = new KPIRelationRepository(new MeetingSignInDBEntities());
        private comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());

        public ActionResult Index(KPIIndexView model)
        {
            var query = kPIRepository.Query(model.IsOnline, model.Title, model.Type, 1);
            var pageResult = query.ToPageResult<KPI>(model);
            model.PageResult = Mapper.Map<PageResult<KPIView>>(pageResult);

            foreach (var item in model.PageResult.Data)
            {
                item.Dept = GetKPIDept(item.ID);
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new KPIView();
            model.CheckDate = DateTime.Now.AddMonths(6);
            model.Level = 1;
            model.ApprovalState = Draft;
            model.Dept = "";
            if (id != 0)
            {
                var query = kPIRepository.GetById(id);
                if (query == null)
                {
                    return RedirectToAction("Index");
                }
                model = Mapper.Map<KPIView>(query);

                model.DeptKPIList = Mapper.Map<List<KPIView>>(kPIRepository.Query(null, "", 0, 2, model.ID));
                foreach (var item in model.DeptKPIList)
                {
                    item.Dept = GetKPIDept(item.ID);
                }
            }
            else
            {

            }

            #region 部門關聯
            if (model.ID != 0)
            {
                model.Dept = GetKPIDept(model.ID);
                //var relationList = kPIRelationRepository.Query(model.ID).Select(q => q.DeptName);
                //model.Dept = String.Join(", ", relationList.ToArray());
            }
            #endregion

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(KPIView model)
        {
            if (ModelState.IsValid)
            {
                KPI data = Mapper.Map<KPI>(model);
                data.ApprovalState = Approved;
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = kPIRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    kPIRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }
                #region 部門關聯
                if (!string.IsNullOrEmpty(model.Dept))
                {
                    var oldData = kPIRelationRepository.Query(model.ID);
                    foreach (var item in oldData.ToList())
                    {
                        kPIRelationRepository.Delete(item.ID);
                    }
                    var deptArr = model.Dept.Split(',');
                    foreach (var item in deptArr)
                    {
                        var deptInfo = comDepartmentRepository.FindByDeptName(item.Trim());
                        kPIRelationRepository.Insert(new KPIRelation()
                        {
                            KPIID = model.ID,
                            KPITitle = model.Title,
                            DeptId = deptInfo.DeptId,
                            DeptName = deptInfo.DeptName,
                        });
                    }
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            kPIRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 執行表單動作
        /// </summary>
        /// <param name="action">表單動作</param>
        public void DoAction(int kpiId, ApprovalAction action)
        {
            var kpi = kPIRepository.GetById(kpiId);
            // 取得目前狀態下執行【傳入動作】的下個狀態
            var newState = _stateMachine.Transition(action);

            // 如果目前狀態不能執行【傳入動作】行為 => 會拋出 Exception
            // 如果目前狀態可以執行【傳入動作】行為 => 會取得新狀態 newState

            // TODO: 使用新狀態去更新該筆資料(DB)的狀態
            //Console.WriteLine($"目前狀態 {_product.State} 執行 {action} 動作後，狀態改變成 {newState} ");
            kpi.ApprovalState = (int)newState;
            kPIRepository.Update(kpi);
        }
    }
}