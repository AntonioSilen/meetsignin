﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Check;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    //[ErrorHandleAdminActionFilter]
    public class CheckAdminController : BaseAdminController
    {
        // GET: Admin/CheckAdmin
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private CheckRepository checkRepository = new CheckRepository(new MeetingSignInDBEntities());
        private CheckImageRepository checkImageRepository = new CheckImageRepository(new MeetingSignInDBEntities());
        private KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
        private KPIRelationRepository kPIRelationRepository = new KPIRelationRepository(new MeetingSignInDBEntities());
        private PersonKPIRepository personKPIRepository = new PersonKPIRepository(new MeetingSignInDBEntities());
        private PersonKPIStateRepository personKPIStateRepository = new PersonKPIStateRepository(new MeetingSignInDBEntities());
        private comPersonRepository comPersonRepository = new comPersonRepository(new T8ERPEntities());

        public ActionResult Index(CheckIndexView model)
        {
            var adminInfo = AdminInfoHelper.GetAdminInfo();
            var query = checkRepository.Query(model.TargetType, 0, (int)ApprovalState.Approving);

            #region 篩選負責部門
            List<string> auditDepts = new List<string>();
            if (adminInfo.Type == (int)AdminType.Manager)
            {//主管審核
                auditDepts = auditLevelRepository.Query(true, "", "", "", adminInfo.Account).Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.Type == (int)AdminType.Vice)
            {//副總審核
                auditDepts = auditLevelRepository.Query(true, "", "", adminInfo.Account, "").Select(q => q.DeptId).ToList();
            }
            else if (adminInfo.Type == (int)AdminType.President)
            {//總經理審核
                auditDepts = auditLevelRepository.Query(true, "", adminInfo.Account, "", "").Select(q => q.DeptId).ToList();
            }

            var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
            var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account)).Select(q => q.ID).ToList();
            if (adminInfo.Type < (int)AdminType.ClassSupervisor)
            {
                query = query.Where(q => sameDeptPersonIdList.Contains(q.PersonID));
            }
            //else
            //{
            //    query = query.Where(q => sameDeptPersonIdList.Contains(q.PersonID));
            //}
            #endregion

            var pageResult = query.OrderBy(q => q.ApprovalState).ThenByDescending(q => q.CreateDate).ToPageResult<Check>(model);
            model.PageResult = Mapper.Map<PageResult<CheckView>>(pageResult);

            var personInfo = new GroupJoinPersonAndDepartment();
            foreach (var item in model.PageResult.Data)
            {
                switch (item.TargetType)
                {
                    case (int)TargetType.KPI:                       
                        item.TargetTitle = kPIRepository.GetById(item.TargetID).Title;
                        break;
                    case (int)TargetType.PersonKPI:
                        var personkpidata = personKPIRepository.GetById(item.TargetID);
                        item.TargetTitle = personkpidata.KPITitle;
                        item.DeptName = comPersonRepository.FindDepartments(personkpidata.DeptId).DeptName;
                        item.PersonName = comPersonRepository.FindByPersonID(personkpidata.PersonId).PersonName;
                        break;
                    case (int)TargetType.PersonKPIState:
                        var stateData = personKPIStateRepository.GetById(item.TargetID);
                        personkpidata = personKPIRepository.GetById(stateData.PersonKPIID);
                        personInfo = comPersonRepository.FindByPersonID(personkpidata.PersonId);
                        item.TargetTitle = stateData.Title;
                        item.ParentKPIID = personkpidata.ID;
                        item.ParentKPITitle = personkpidata.KPITitle;
                        item.PersonName = personInfo.PersonName;
                        item.DeptName = personInfo.DeptName;
                        break;
                }

                if (string.IsNullOrEmpty(item.Auditors))
                {
                    item.AuditorName = "系統管理員";
                }
                else
                {
                    personInfo = comPersonRepository.FindByPersonID(item.Auditors);
                    item.AuditorName = personInfo == null ? "系統管理員" : personInfo.PersonName;
                }
               
                var createrPersonId = adminRepository.GetById(item.Creater);
                item.CreaterName = createrPersonId == null ? "系統管理員" : createrPersonId.Name;
            }

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new CheckView();
            if (id == 0)
            {
                return RedirectToAction("Index");
            }
            var query = checkRepository.GetById(id);
            model = Mapper.Map<CheckView>(query);
            var kpiRelationList = kPIRelationRepository.Query(query.TargetID).Select(q => q.DeptName).ToList();
            model.Dept = string.Join(",", kpiRelationList);
            switch (model.TargetType)
            {
                case (int)TargetType.KPI://部門KPI資訊異動
                    var kpidata = kPIRepository.GetById(model.TargetID);
                    model.TargetTitle = kpidata.Title;

                    //相關指標
                    model.ChildKPIList = Mapper.Map<List<CheckChildModel>>(personKPIRepository.Query(null, true, model.TargetID));
                    break;
                case (int)TargetType.PersonKPI://個人目標資訊異動
                    var personkpidata = personKPIRepository.GetById(model.TargetID);
                    var personInfo = comPersonRepository.FindByPersonID(personkpidata.PersonId);
                    model.TargetTitle = personkpidata.KPITitle;
                    model.PersonName = personInfo.PersonName;
                    model.DeptName = personInfo.DeptName;

                    //相關指標
                    model.ChildKPIList = Mapper.Map<List<CheckChildModel>>(personKPIStateRepository.Query(null, model.TargetID));
                    break;
                case (int)TargetType.PersonKPIState://個人目標進度異動
                    var personkpistatedata = personKPIStateRepository.GetById(model.TargetID);
                    personkpidata = personKPIRepository.GetById(personkpistatedata.PersonKPIID);
                    personInfo = comPersonRepository.FindByPersonID(personkpidata.PersonId);
                    model.TargetTitle = personkpistatedata.Title;
                    model.ParentKPIID = personkpidata.ID;
                    model.ParentKPITitle = personkpidata.KPITitle;
                    model.PersonName = personInfo.PersonName;
                    model.DeptName = personInfo.DeptName;
                    break;
            }

            DateTime defaultDate = Convert.ToDateTime("2000/01/01");
            if (model.AuditDate < defaultDate)
            {
                model.AuditDate = defaultDate;
            }            

            model.AttachList = checkImageRepository.Query(id).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(CheckView model)
        {
            if (ModelState.IsValid)
            {
                Check data = Mapper.Map<Check>(model);
                var adminInfo = AdminInfoHelper.GetAdminInfo();

                if (model.ID == 0)
                {
                    model.ID = checkRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    if (model.ApprovalState == (int)ApprovalState.Approved || model.ApprovalState == (int)ApprovalState.Reject)
                    {
                        data.AuditDate = DateTime.Now;
                        data.Auditors = adminInfo.Account;
                    }
                    checkRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                #region 檔案處理
                if (model.AttachFiles != null)
                {
                    foreach (HttpPostedFileBase pic in model.AttachFiles)
                    {
                        bool hasFile = ImageHelper.CheckFileExists(pic);
                        if (hasFile)
                        {
                            CheckImage repairData = new CheckImage();
                            //ImageHelper.DeleteFile(PhotoFolder, model.FileOne);
                            repairData.CheckID = model.ID;
                            repairData.File = ImageHelper.SaveFile(pic, PhotoFolder);
                            checkImageRepository.Insert(repairData);
                        }
                    }
                }
                #endregion

                #region 資料審核狀態更新
                try
                {
                    if (model.ApprovalState == (int)ApprovalState.Approved || model.ApprovalState == (int)ApprovalState.Reject)
                    {
                        switch ((TargetType)model.TargetType)
                        {
                            case TargetType.KPI:
                                var kpidata = kPIRepository.GetById(model.TargetID);
                                kpidata.ApprovalState = model.ApprovalState;
                                kPIRepository.Update(kpidata);
                                break;
                            case TargetType.PersonKPI:
                                var personkpidata = personKPIRepository.GetById(model.TargetID);
                                personkpidata.ApprovalState = model.ApprovalState;
                                personKPIRepository.Update(personkpidata);
                                break;
                            case TargetType.PersonKPIState:
                                var personkpistatedata = personKPIStateRepository.GetById(model.TargetID);
                                personkpistatedata.ApprovalState = model.ApprovalState;
                                personKPIStateRepository.Update(personkpistatedata);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {

                    throw;
                }
                #endregion

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            checkRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    }
}