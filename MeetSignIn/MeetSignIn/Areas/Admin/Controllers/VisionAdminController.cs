﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.Vision;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.Controllers
{
    [Authorize]
    [ErrorHandleAdminActionFilter]
    public class VisionAdminController : BaseAdminController
    {
        private VisionRepository visionRepository = new VisionRepository(new MeetingSignInDBEntities());

        public ActionResult Index(VisionIndexView model)
        {
            var query = visionRepository.Query(model.IsOnline, model.Year, model.Brief);
            var pageResult = query.ToPageResult<Vision>(model);
            model.PageResult = Mapper.Map<PageResult<VisionView>>(pageResult);

            return View(model);
        }

        public ActionResult Edit(int id = 0)
        {
            var model = new VisionView();
            if (id != 0)
            {
                var query = visionRepository.FindBy(id);
                model = Mapper.Map<VisionView>(query);
            }
            else
            {

            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit(VisionView model)
        {
            if (ModelState.IsValid)
            {
                Vision data = Mapper.Map<Vision>(model);
                int adminId = AdminInfoHelper.GetAdminInfo().ID;

                if (model.ID == 0)
                {
                    model.ID = visionRepository.Insert(data);
                    ShowMessage(true, "新增成功");
                }
                else
                {
                    visionRepository.Update(data);
                    ShowMessage(true, "修改成功");
                }

                return RedirectToAction("Edit", new { id = model.ID });
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {   
            visionRepository.Delete(id);
            ShowMessage(true, "刪除成功");
            return RedirectToAction("Index");
        }
    
    }
}