﻿using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute(
            //    "Admin_default",
            //    "Admin/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);
           // context.MapRoute(
           //    "Admin_Lang_default",
           //    "Admin/{lang}/{controller}/{action}/{id}",
           //    new { action = "Index", id = UrlParameter.Optional, lang = "zh-TW" }
           //);
            context.MapRoute(
               "Admin_default",
               "Admin/{controller}/{action}/{id}",
               new { action = "Index", id = UrlParameter.Optional }
           );
        }
    }
}