﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.Room
{
    public class RoomIndexView : PageQuery
    {
        public RoomIndexView()
        {
            this.Sorting = "RoomNumber";
            this.IsDescending = false;
        }

        public PageResult<RoomView> PageResult { get; set; }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        [Required]
        [Display(Name = "會議室")]
        public string RoomNumber { get; set; }

        [Required]
        [Display(Name = "地點")]
        public string Place { get; set; }

        [Display(Name = "使用狀態")]
        public bool? Status { get; set; }

        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> FloorOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.RoomNumber,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
    }
}