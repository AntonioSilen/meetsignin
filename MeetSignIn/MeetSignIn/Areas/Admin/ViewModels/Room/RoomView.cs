﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Room
{
    public class RoomView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }
        public string FactoryStr
        {
            get
            {
                return EnumHelper.GetDescription((Factory)this.FactoryID);
            }
        }

        [Required]
        [Display(Name = "會議室")]
        public string RoomNumber { get; set; }

        [Required]
        [Display(Name = "地點")]
        public string Place { get; set; }

        [Display(Name = "使用狀態")]
        public bool Status { get; set; }

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

    }
}