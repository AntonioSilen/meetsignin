﻿using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PFAppraise
{
    public class PFAppraiseIndexView : PageQuery
    {
        public PFAppraiseIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<PFAppraiseView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "季度標題")]
        public string SeasonTitle { get; set; }

        [Display(Name = "季度")]
        public int SeasonID { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "部門名稱")]
        public string DeptName { get; set; }

        [Display(Name = "班別名稱")]
        public string ClassTitle { get; set; }

        [Display(Name = "班別")]
        public int ClassID { get; set; }

        [Display(Name = "評比作業標題")]
        public string WorkTitle { get; set; }

        [Display(Name = "評比作業")]
        public int WorksID { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "評比編號")]
        public int AppraiseID { get; set; }

        //public List<SelectListItem> DeptOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
        //        var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Text = v.Key.DeptName,
        //                Value = (v.Key.DeptId).ToString()
        //            });
        //        }

        //        return result;
        //    }
        //}
    }
}