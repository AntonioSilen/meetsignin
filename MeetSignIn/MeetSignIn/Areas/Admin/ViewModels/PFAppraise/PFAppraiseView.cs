﻿using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Areas.Admin.ViewModels.PFWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.PFAppraise
{
    public class PFAppraiseView : PFAuditView
    {
        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string PersonId { get; set; }
        public string PersonName { get; set; }
        public string PersonDeptName { get; set; }

        [Required]
        [Display(Name = "季度標題")]
        public string SeasonTitle { get; set; }

        [Required]
        [Display(Name = "評比季度")]
        public int SeasonID { get; set; }

        [Required]
        [Display(Name = "評比部門")]
        public string DeptId { get; set; }

        [Required]
        [Display(Name = "部門名稱")]
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "班別名稱")]
        public string ClassTitle { get; set; }
        public List<string> ClassTitleList { get; set; }

        [Required]
        [Display(Name = "班別")]
        public int ClassID { get; set; }

        [Required]
        [Display(Name = "評比作業標題")]
        public string WorkTitle { get; set; }

        [Required]
        [Display(Name = "評比作業")]
        public int WorksID { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        public bool IsInvalid { get; set; }
        public int AuditState { get; set; }
        public string AuditStatus { get; set; }

        public int AppraiseID { get; set; }
        public int AppraiseWorksID { get; set; }
        public bool IsFinished { get; set; }
        public int Points { get; set; }

        public List<PFWorkView> WorkList { get; set; }
        //public List<PFCategoryCateView> CateList { get; set; }
    }
}