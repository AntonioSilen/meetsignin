﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.Theme
{
    public class ThemeIndexView : PageQuery
    {
        public ThemeIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<ThemeView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "研訓類別標題")]
        public string Title { get; set; }

        [Display(Name = "負責人")]
        public string ResponsiblePerson { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }
    }
}