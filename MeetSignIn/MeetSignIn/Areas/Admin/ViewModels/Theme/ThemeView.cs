﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Theme
{
    public class ThemeView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "研訓類別標題")]
        public string Title { get; set; }

        [Display(Name = "負責人")]
        public string ResponsiblePerson { get; set; }

        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public List<SelectListItem> ResponsiblePersonOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                Repositories.T8Repositories.comPersonRepository compersonRepository = new Repositories.T8Repositories.comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.PersonId, x.PersonName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.PersonName,
                        Value = (v.Key.PersonId).ToString()
                    });
                }

                return result;
            }
        }
    }
}