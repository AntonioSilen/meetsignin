﻿using MeetSignIn.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.DrawAwardRelation
{
    public class DrawAwardRelationIndexView : PageQuery
    {
        public DrawAwardRelationIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<DrawAwardRelationView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "抽籤活動編號")]
        public int DrawID { get; set; }

        [Display(Name = "抽籤活動編號")]
        public int AwardID { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "註解")]
        public string Description { get; set; }

        [Display(Name = "兌獎日期")]
        public string AwardDate { get; set; }

        [Display(Name = "撤銷狀態")]
        public bool? IsRevoke { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
    }
}