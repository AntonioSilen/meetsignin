﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.TrainingAbnormal
{
    public class TrainingAbnormalView : AuditView
    {
        public bool FromAudit { get; set; }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }

        [Display(Name = "訓練標題")]
        public string Title { get; set; }

        [Display(Name = "訓練類別")]
        public int Category { get; set; }

        [Required]
        [Display(Name = "講師")]
        public string Lecturer { get; set; }

        [Required]
        [Display(Name = "時間/場地")]
        public string Location { get; set; }

        [Required]
        [Display(Name = "課程安排")]
        public string Schedule { get; set; }

        [Required]
        [Display(Name = "設備")]
        public string Device { get; set; }

        [Required]
        [Display(Name = "教材")]
        public string Material { get; set; }

        [Required]
        [Display(Name = "原因判定")]
        public string Reason { get; set; }

        [Required]
        [Display(Name = "責任相關人員")]
        public string Responsibility { get; set; }

        [Required]
        [Display(Name = "處理方式")]
        public string Method { get; set; }

        [Required]
        [Display(Name = "防止再發生措施")]
        public string Measure { get; set; }

        [Required]
        [Display(Name = "追踨事項")]
        public string Track { get; set; }

        [Display(Name = "建單日期")]
        public DateTime CreateDate { get; set; }

    }
}