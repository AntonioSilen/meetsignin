﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using AutoMapper;

namespace MeetSignIn.Areas.Admin.ViewModels.TrainingAbnormal
{
    public class TrainingAbnormalIndexView : PageQuery
    {
        public TrainingAbnormalIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = true;

            this.StartDate = Convert.ToDateTime(DateTime.Now.Year + "/01/01");
            this.EndDate = DateTime.Now.AddMonths(6);
        }

        public PageResult<TrainingAbnormalView> PageResult { get; set; }

        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "訓練編號")]
        public int TrainingID { get; set; }

        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }

        [Display(Name = "訓練類別")]
        public int Category { get; set; }

        [Required]
        [Display(Name = "訓練標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "講師")]
        public string Lecturer { get; set; }

        [Required]
        [Display(Name = "時間/場地")]
        public string Location { get; set; }

        [Required]
        [Display(Name = "課程安排")]
        public string Schedule { get; set; }

        [Required]
        [Display(Name = "設備")]
        public string Device { get; set; }

        [Required]
        [Display(Name = "教材")]
        public string Material { get; set; }

        [Required]
        [Display(Name = "原因判定")]
        public string Reason { get; set; }

        [Required]
        [Display(Name = "責任相關人員")]
        public string Responsibility { get; set; }

        [Required]
        [Display(Name = "處理方式")]
        public string Method { get; set; }

        [Required]
        [Display(Name = "防止再發生措施")]
        public string Measure { get; set; }

        [Required]
        [Display(Name = "追踨事項")]
        public string Track { get; set; }

        [Display(Name = "建單日期")]
        public DateTime CreateDate { get; set; }
    }
}