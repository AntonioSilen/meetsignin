﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.PersonKPIState
{
    public class PersonKPIStateView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "個人目標編號")]
        public int PersonKPIID { get; set; }

        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }        

        [Display(Name = "簡述")]
        public string Description { get; set; }        

        [Display(Name = "目標值")]
        public int TargetNum { get; set; }        

        [Display(Name = "目標單位")]
        public string TargetUnit { get; set; }        

        [Display(Name = "目標單位")]
        public string Percentage { get; set; }        

        [Display(Name = "目標單位")]
        public string IsAssessed { get; set; }        

        [Display(Name = "目標單位")]
        public string Quarter { get; set; }        

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }        

        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }        

        [Display(Name = "完成狀態")]
        public bool IsFinished { get; set; }        

        [Display(Name = "最新資料")]
        public bool IsLatest { get; set; }        

        [Display(Name = "完成時間")]
        public DateTime FinishDate { get; set; }       

        [Display(Name = "檢核時間")]
        public DateTime CheckDate { get; set; }       
    }
}