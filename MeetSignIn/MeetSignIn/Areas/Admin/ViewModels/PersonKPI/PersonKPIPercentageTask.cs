﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.PersonKPI
{
    public class PersonKPIPercentageTask
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "個人目標編號")]
        public int PersonKPIID { get; set; }

        [Display(Name = "個人目標標題")]
        public string PersonKPITitle { get; set; }

        [Display(Name = "進度編號")]
        public int PersonKPIStateID { get; set; }

        [Display(Name = "進度標題")]
        public string PersonKPIStateTitle { get; set; }

        [Display(Name = "季度標題")]
        public string QuarterTitle { get; set; }

        [Display(Name = "百分比")]
        public int Percentage { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "最新版本")]
        public bool IsLatest { get; set; }

        [Display(Name = "作廢版本")]
        public bool IsInvalid { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
    }
}