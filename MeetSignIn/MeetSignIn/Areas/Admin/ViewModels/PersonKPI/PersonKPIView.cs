﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPIMonth;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPIState;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PersonKPI
{
    public class PersonKPIView : GeneralAuditView
    {
        public bool FromAudit { get; set; }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "主要指標")]
        public string FairlyKPITitle { get; set; }
        public int FairlyKPIID { get; set; }
        public string FairlyKPIDescription { get; set; }

        [Display(Name = "部門指標編號")]
        public int KPIID { get; set; }

        [Display(Name = "部門指標")]
        public string KPITitle { get; set; }

        [Display(Name = "指標簡述")]
        public string KPIDescription { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "部門")]
        public string ParentDeptId { get; set; }

        [Display(Name = "課別")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "進度狀態")]
        public int State { get; set; }
        public string StateStr { get; set; }

        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }

        [Display(Name = "最新版本")]
        public bool IsLatest { get; set; }

        [Display(Name = "作廢版本")]
        public bool IsInvalid { get; set; }

        [Display(Name = "審核時間")]
        public DateTime? AuditDate { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        public List<PersonKPIStateView> StateList { get; set; }

        public DeptKPIPercentage DivisionInfo { get; set; }

        public List<SelectListItem> FairlyKPIOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
                var values = kPIRepository.Query(true, "", 0, 2, 0, this.DeptId);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }

        public List<Models.PersonKPIState> StateOptions
        {
            get
            {
                PersonKPIStateRepository personKPIStateRepository = new PersonKPIStateRepository(new MeetingSignInDBEntities());
                var result = personKPIStateRepository.Query(null, this.ID).OrderBy(q => q.Sort).ToList();
                return result;
            }
        }

        public List<PersonKPIMonthView> MonthOptions
        {
            get
            {
                PersonKPIMonthRepository personKPIMonthRepository = new PersonKPIMonthRepository(new MeetingSignInDBEntities());
                var result = Mapper.Map<List<PersonKPIMonthView>>(personKPIMonthRepository.Query(null, this.ID, this.PersonId, 0, (int)InfoState.Initial).OrderBy(q => q.Month).ToList());
                if (result.Count() == 0)
                {
                    #region 建立空白進度 12個月分
                    DateTime Now = DateTime.Now;
                    for (int i = 1; i <= 12; i++)
                    {
                        personKPIMonthRepository.Insert(new Models.PersonKPIMonth()
                        {
                            PersonKPIID = this.ID,
                            PersonID = this.PersonId,
                            DeptID = this.DeptId,
                            Month = i,
                            ToDo = "",
                            Expect = "",
                            Complete = "",
                            Percentage = 0,
                            Type = (int)InfoState.Initial,

                            IsFinished = false,
                            Deadline = new DateTime(Now.Year, i, 1).AddMonths(1).AddDays(-1),
                            CreateDate = this.CreateDate
                        });
                    }
                    #endregion
                    result = Mapper.Map<List<PersonKPIMonthView>>(personKPIMonthRepository.Query(null, this.ID, this.PersonId).OrderBy(q => q.Month).ToList());
                }
                return result;
            }
        }

        public List<SelectListItem> QuarterOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var now = DateTime.Now;
                int thisYear = now.Year;
                int endYear = now.AddYears(1).Year;
                for (int i = thisYear; i <= endYear; i++)
                {
                    for (int j = 1; j <= 4; j++)
                    {
                        string quarter = i.ToString() + "年" + (j * 2 + j - 2).ToString() + "月~" + (j * 2 + j).ToString() + "月";

                        result.Add(new SelectListItem()
                        {
                            Text = quarter,
                            Value = quarter
                        });
                    }
                }

                return result;
            }
        }
    }
}