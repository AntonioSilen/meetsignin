﻿using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PersonKPI
{
    public class PersonKPIIndexView : PageQuery
    {
        public PersonKPIIndexView()
        {
            this.Sorting = "DeptId";
            this.IsDescending = false;
        }

        public PageResult<PersonKPIView> PageResult { get; set; }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "主指標")]
        public string FairlyKPITitle { get; set; }

        [Display(Name = "指標編號")]
        public int KPIID { get; set; }

        [Display(Name = "指標")]
        public string KPITitle { get; set; }

        [Display(Name = "指標簡述")]
        public string KPIDescription { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "姓名")]
        public string PersonName { get; set; }

        [Display(Name = "部門")]
        public string ParentDeptId { get; set; }

        [Display(Name = "部門編號")]
        public string DeptId { get; set; }

        [Display(Name = "部門")]
        public string DeptName { get; set; }

        [Display(Name = "進度狀態")]
        public int State { get; set; }
        public string StateStr { get; set; }

        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }

        [Display(Name = "最新版本")]
        public bool? IsLatest { get; set; }

        [Display(Name = "作廢版本")]
        public bool? IsInvalid { get; set; }

        [Display(Name = "審核時間")]
        public DateTime CheckDate { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "個人KPI列表")]
        public List<PersonKPIView> PersonKPIList { get; set; }

        public List<SelectListItem> FairlyKPIOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
                var values = kPIRepository.Query(true, "", 0, 2, 0, this.DeptId);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ApprovalStateOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ApprovalState>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> QuarterOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var now = DateTime.Now;
                int thisYear = now.Year;
                int endYear = now.AddYears(1).Year;
                for (int i = thisYear; i <= endYear; i++)
                {
                    for (int j = 1; j <= 4; j++)
                    {
                        string quarter = i.ToString() + "年" + (j * 2 + j - 2).ToString() + "月~" + (j * 2 + j).ToString() + "月";

                        result.Add(new SelectListItem()
                        {
                            Text = quarter,
                            Value = quarter
                        });
                    }
                }

                return result;
            }
        }
    }
}