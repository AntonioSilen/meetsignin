﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Temperature
{
    public class LimitHoursView
    {
        [Required]
        [Display(Name = "開始")]
        public int Start { get; set; }

        [Required]
        [Display(Name = "結束")]
        public int End { get; set; }

        public List<SelectListItem> HourOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                for (int i = 0; i <= 24; i++)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = i.ToString(),
                        Text = $"{i.ToString("00")}:00"
                    });
                }
                return result;
            }
        }
    }
}