﻿using MeetSignIn.Models;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Temperature
{
    public class TemperatureView
    {
        Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());

        public int ID { get; set; }

        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "生日")]
        public string Birthday { get; set; }

        [Display(Name = "體溫")]
        public decimal BodyTemperature { get; set; }

        [Display(Name = "異常狀況")]
        public int AbnormalDescription { get; set; }

        [Display(Name = "症狀情況")]
        public string AbnormalDescriptionStr { get; set; }

        [Display(Name = "身體異常")]
        public bool IsAbnormal { get; set; }

        [Display(Name = "接觸史")]
        public string ContactHistory { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        public List<TemperatureView> TemperatureLogList { get; set; }

        public GroupJoinPersonAndDepartment PersonInfo
        {
            get
            {
                var personInfo = t8personRepository.FindByPersonId(this.PersonID);
                if (personInfo != null)
                {
                    return personInfo;
                }
                else 
                { 
                    var personAdminInfo = adminRepository.Query(null, 0, this.PersonID).FirstOrDefault();
                    if (personAdminInfo != null)//驗證人員
                    {
                        if (personAdminInfo.Department == "0")
                        {
                            personAdminInfo.Department = "0001";
                        }
                        Repositories.T8Repositories.DepartmentRepository departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                        return new GroupJoinPersonAndDepartment() {
                            DeptID = personAdminInfo.Department,
                            DepartmentStr = departmentRepository.FindByDeptId(personAdminInfo.Department).DeptName,
                            JobID = this.PersonID,
                            Name = personAdminInfo.Name,
                        };
                    }
                    return new GroupJoinPersonAndDepartment()
                    {
                        DeptID = "0001",
                        DepartmentStr = "菲力工業股份有限公司",
                        JobID = this.PersonID,
                        Name = this.PersonID,
                    }; ;
                }
            }
        }

        public List<SelectListItem> TemperatureOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                for (int i = 20; i <= 50; i++)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = i.ToString() + " °C",
                        Value = i.ToString()
                    });
                }

                return result;
            }
        }

        public List<SelectListItem> AbnormalOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Abnormal>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }

                return result;
            }
        }

    }
    public class TemperatureNotInView
    {
        public string PersonID { get; set; }
        public string PersonName { get; set; }
        public string Phone { get; set; }
        public string EMail { get; set; }
    }
}