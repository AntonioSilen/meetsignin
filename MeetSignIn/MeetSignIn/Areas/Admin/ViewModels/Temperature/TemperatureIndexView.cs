﻿using MeetSignIn.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Temperature
{
    public class TemperatureIndexView : PageQuery
    {
        public TemperatureIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
            //this.SearchDate = DateTime.Now.ToShortDateString();
        }

        [Required]
        public PageResult<TemperatureView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "課別")]
        public string DepartmentStr { get; set; }

        [Display(Name = "體溫")]
        public decimal BodyTemperature { get; set; }

        [Display(Name = "異常狀況")]
        public int AbnormalDescription { get; set; }

        [Display(Name = "症狀情況")]
        public string AbnormalDescriptionStr { get; set; }

        [Display(Name = "身體異常")]
        public bool? IsAbnormal { get; set; }

        [Display(Name = "接觸史")]
        public string ContactHistory { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "搜尋日期")]
        public string SearchDate { get; set; }
    }
}