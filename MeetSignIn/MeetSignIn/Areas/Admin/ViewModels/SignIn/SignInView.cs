﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.SignIn
{
    public class SignInView
    {
        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        [Required]
        public int ID { get; set; }

        public string SignCode { get; set; }

        [Required]
        [Display(Name = "會議編號")]
        public int MeetingID { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "是否簽到")]
        public bool IsSign { get; set; }

        [Required]
        [Display(Name = "簽到狀態")]
        public int SignStatus { get; set; }

        [Display(Name = "通知狀態")]
        public bool IsSent { get; set; }

        [Display(Name = "簽到時間")]
        public DateTime SignTime { get; set; }

        public string SignTimeStr
        {
            get
            {
                return this.IsSign ? this.SignTime.ToString("yyyy/MM/dd HH:mm") : "";
            }
        }

        public string DeptID
        {
            get
            {
                try
                {
                    return personRepository.FindByPersonId(this.JobID).DeptID;
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }
        public string DeptName
        {
            get
            {
                try
                {
                    return personRepository.FindByPersonId(this.JobID).DeptName;
                }
                catch (Exception)
                {
                    return "菲力工業";
                }
            }
        }
    }

    public class ExportSignInView
    {

    }
}