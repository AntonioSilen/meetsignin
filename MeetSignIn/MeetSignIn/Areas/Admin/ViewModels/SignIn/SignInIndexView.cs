﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.SignIn
{
    public class SignInIndexView : PageQuery
    {
        public SignInIndexView()
        {
            this.Sorting = "SignTime";
            this.IsDescending = false;
        }

        public PageResult<SignInView> PageResult { get; set; }

        public string SignCode { get; set; }

        [Required]
        [Display(Name = "會議編號")]
        public int MeetingID { get; set; }

        public string MeetingTitle { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "是否簽到")]
        public bool? IsSign { get; set; }

        [Display(Name = "簽到狀態")]
        public int SignStatus { get; set; }

        [Display(Name = "通知狀態")]
        public bool? IsSent { get; set; }

        [Display(Name = "簽到時間")]
        public DateTime SignTime { get; set; }

    }
}