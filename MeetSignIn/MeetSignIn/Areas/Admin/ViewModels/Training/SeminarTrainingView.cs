﻿using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class SeminarTrainingView : AuditView
    {
        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        ThemeRepository themeRepository = new ThemeRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public SeminarTrainingView()
        {
            this.IsOnline = true;
        }

        public bool FromAudit { get; set; }

        public bool IsAudit { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "課程編號")]
        public string MeetingNumber { get; set; }

        [Required]
        [Display(Name = "願景/策略")]
        public int VisionID { get; set; }

        [Required]
        [Display(Name = "課程名稱")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "發起人")]
        public string Organiser { get; set; }

        [Required]
        [Display(Name = "訓練單位")]
        public string DeptID { get; set; }
        public string DeptName { get; set; }
        //{ 
        //    get
        //    {
        //        return departmentRepository.FindByDeptID(this.DeptID).Title;
        //    }
        //}

        [Display(Name = "選擇名單課別")]
        public int Department { get; set; }

        [Display(Name = "參與課別")]
        public string DepartmentListStr { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        [Required]
        [Display(Name = "會議室編號")]
        public int RoomID { get; set; }
        public string RoomStr
        {
            get
            {
                return this.RoomID != 0 ? roomRepository.GetRoomString(this.RoomID) : "";
            }
        }

        public string RoomNum
        {
            get
            {
                return this.RoomID == 0 ? "" : roomRepository.FindBy(this.RoomID).RoomNumber;
            }
        }

        [Required]
        [Display(Name = "訓練類別")]
        public int Category { get; set; }

        [Required]
        [Display(Name = "研訓類別")]
        public int Theme { get; set; }
        public string ThemeTitle { get; set; }
        //{
        //    get
        //    {
        //        return themeRepository.GetById(Convert.ToInt32(this.Theme)).Title;
        //    }
        //}

        [Required]
        [Display(Name = "團體類別")]
        public int GroupType { get; set; }

        [Required]
        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Required]
        [Display(Name = "證書/證明")]
        public int Cert { get; set; }

        [Display(Name = "講師")]
        public string Lecturer { get; set; }

        [Required]
        [Display(Name = "簽到時間")]
        public DateTime SignDate { get; set; }

        [Required]
        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "時數")]
        public decimal Hours { get; set; }

        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "線上會議連結")]
        public string Url { get; set; }

        [Display(Name = "注意事項")]
        public string Note { get; set; }

        [Display(Name = "備註")]
        public string Remark { get; set; }

        [Display(Name = "會議通知內容")]
        public string NotifyMsg { get; set; }

        [Display(Name = "上課照片")]
        public string MainPic { get; set; }
        public List<TrainingImage> PicList { get; set; }
        public List<HttpPostedFileBase> PicsFiles { get; set; }
        public HttpPostedFileBase TraningPic { get; set; }

        [Display(Name = "課堂講義")]
        public string MainFile { get; set; }
        public HttpPostedFileBase TraningFile { get; set; }

        [Display(Name = "課堂影片")]
        public string MainVideo { get; set; }
        public HttpPostedFileBase TraningVideo { get; set; }

        [Display(Name = "課堂報告")]
        public string MainReport { get; set; }
        public HttpPostedFileBase TraningReport { get; set; }

        [Display(Name = "課堂驗收")]
        public string MainAcceptance { get; set; }
        public HttpPostedFileBase TraningAcceptance { get; set; }

        [Required]
        [Display(Name = "可簽到狀態")]
        public bool IsSignable { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "可遲到分鐘")]
        public int LateMinutes { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public string CreaterDeptId
        {
            get
            {
                AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
                string account = adminRepository.GetById(this.Creater).Account;
                var t8Info = t8personRepository.FindByPersonId(account);
                return t8Info == null ? "0001" : t8Info.DeptID;
            }
        }

        public AdminInfo AdminInfo
        {
            get
            {
                return AdminInfoHelper.GetAdminInfo(); ;
            }
        }

        [Display(Name = "已選通知名單")]
        public List<EmployeeView> Notify { get; set; }

        [Display(Name = "選擇課別後勾選名單")]
        [Required(ErrorMessage = "請至少選擇一個通知名單")]
        public string selectedNotifyList { get; set; }

        #region 審核狀態
        public string ActionPlanStatus { get; set; }
        public string OpinionStatus { get; set; }
        public string ReportStatus { get; set; }
        public string EAStatus { get; set; }
        public string IAStatus { get; set; }
        public int AuditState { get; set; }
        public string AuditStatus { get; set; }
        public bool IsInvalid { get; set; }
        public bool HasAcceptance { get; set; }
        public bool HasOpinion { get; set; }
        #endregion

        public bool SameDeptManager { get; set; }

        public List<SelectListItem> VisionOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                VisionRepository visionRepository = new VisionRepository(new MeetingSignInDBEntities());
                var values = visionRepository.Query(true, Convert.ToInt32(DateTime.Now.Year));
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = "【" + v.Year + "】 " + v.Brief
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> RoomOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.GetAll();
                foreach (var v in values)
                {

                    result.Add(new SelectListItem()
                    {
                        Text = v.Place + " (" + v.RoomNumber + ")",
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> LateMinutesOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = EnumHelper.GetValues<LateMinutes>();
                for (int i = 0; i <= 60; i += 5)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = i.ToString(),
                        Text = i.ToString() + "分鐘"
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DeptIDOptions
        {
            get
            {

                List<SelectListItem> result = new List<SelectListItem>();
                var values = departmentRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.DeptID
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = t8personRepository.GetDeptQuery();
                var values = departmentRepository.Query(true).OrderBy(q => q.DeptID);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> CategoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Category>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> GroupTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<GroupType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ThemeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = themeRepository.Query(true);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> CertOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Certification>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}