﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class InternalCourseIndexView
    {
        public List<InternalCourseView> CourseList { get; set; }
        public string CourseListJson { get; set; }

        public string JsonCourse { get; set; }

        public TrainingView TrainingInfo { get; set; }

        public TrainingView UploadFilesData { get; set; }
        public int SeriesTrainingID { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Required]
        [Display(Name = "內訓編號")]
        public int InternalID { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "內訓編號")]
        public string Outline { get; set; }

        [Required]
        [Display(Name = "授課日期")]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "授課時間")]
        public string Time { get; set; }

        [Required]
        [Display(Name = "時數")]
        public decimal Hours { get; set; }

        [Required]
        [Display(Name = "課程進度/內容")]
        public string Progress { get; set; }

        [Required]
        [Display(Name = "授課講師")]
        public string Lecturer { get; set; }

        [Required]
        [Display(Name = "訓練方式")]
        public string TrainingMethod { get; set; }
    }
}