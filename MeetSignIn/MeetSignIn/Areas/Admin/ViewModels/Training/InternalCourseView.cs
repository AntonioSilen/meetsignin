﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class InternalCourseView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Required]
        [Display(Name = "內訓編號")]
        public int InternalID { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "內訓編號")]
        public string Outline { get; set; }

        [Required]
        [Display(Name = "授課日期")]
        public DateTime Date { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "授課時間")]
        public string Time { get; set; }

        [Required]
        [Display(Name = "時數")]
        public decimal Hours { get; set; }

        [Required]
        [Display(Name = "課程進度/內容")]
        public string Progress { get; set; }

        [Required]
        [Display(Name = "授課講師")]
        public string Lecturer { get; set; }

        [Display(Name = "設備")]
        public string Device { get; set; }

        [Required]
        [Display(Name = "設備檢查")]
        public bool DeviceCheck { get; set; }

        [Required]
        [Display(Name = "訓練方式")]
        public string TrainingMethod { get; set; }

        [Display(Name = "簽到連結")]
        public string SingInUrl { get; set; }

        [Display(Name = "意見連結")]
        public string OpinionUrl { get; set; }

        [Display(Name = "系列課程編號")]
        public int SeriesTrainingID { get; set; }

        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Display(Name = "會議室已登記")]
        public bool Registed { get; set; }
    }
}