﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class ExternalPlanView : AuditView
    {
        public ExternalPlanView()
        {
            this.IsSkip = false;
        }

        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }
        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }


        [Required]
        [Display(Name = "訓練目的")]
        public string Objecive { get; set; }

        public List<ExternalCourseView> CourseList { get; set; }

        [Display(Name = "遴選原因")]
        public string Reason { get; set; }

        [Required]
        [Display(Name = "受訓人單位(是否派訓)")]
        public int Trained { get; set; }

        [StringLength(50, ErrorMessage = "最多輸入50個字")]
        [Display(Name = "受訓人")]
        public string Trainee { get; set; }

        [Required]
        [Display(Name = "受訓人收穫")]
        public int RewardType { get; set; }        

        [Display(Name = "其他收穫")]
        public string Reward { get; set; }        
    
        [Required]
        [Display(Name = "訓練後續驗收")]
        public string Inspection { get; set; }        

        [Display(Name = "轉授日期")]
        public DateTime DelegateDate { get; set; }        

        [Display(Name = "工作應用行動計畫")]
        public string ActionPlan { get; set; }        

        [Required]
        [Display(Name = "服務契約")]
        public bool Contract { get; set; }

        [Required]
        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "利益關係文件")]
        public string StakeholderDoc { get; set; }
        public List<PlanHolder> HolderList { get; set; }
        public List<HttpPostedFileBase> HoldersFiles { get; set; }
        public HttpPostedFileBase TraningHolder { get; set; }

        [Display(Name = "利益關係人")]
        public string Stakeholders { get; set; }

        [Display(Name = "講師職能文件")]
        public string FunctionDocs { get; set; }
        public List<FunctionDoc> DocList { get; set; }
        public List<HttpPostedFileBase> DocFiles { get; set; }
        public HttpPostedFileBase FunctionDoc { get; set; }

        [Required]
        [Display(Name = "異動狀態")]
        public bool IsAbnormal { get; set; }

        public bool IsSkip { get; set; }

        public string JsonCourse { get; set; }

        public List<SelectListItem> StakeholdersOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<StackHolders>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}