﻿using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Areas.Admin.ViewModels.ExternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.InternalAcceptance;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class TrainingView
    {
        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        ThemeRepository themeRepository = new ThemeRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public bool IsTrue => true;

        public TrainingView()
        {
            this.IsOnline = true;
            this.SeriesParentID = 0;
            this.GroupType = 2;
        }

        public bool FromAudit { get; set; }
        public bool IsAudit { get; set; }
        public bool IsAudited { get; set; }
        public bool SendClose { get; set; }
        public bool HasExam { get; set; }

        public string Address { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "課程編號")]
        public string MeetingNumber { get; set; }

        [Required]
        [Display(Name = "願景/策略")]
        public int VisionID { get; set; }

        [Required]
        [Display(Name = "課程名稱")]
        public string Title { get; set; }
        
        [Required]
        [Display(Name = "發起人")]
        public string Organiser { get; set; }        

        [Required]
        [Display(Name = "訓練單位")]
        public string DeptID { get; set; }
        public string DeptName { get; set; }
        //{ 
        //    get
        //    {
        //        return departmentRepository.FindByDeptID(this.DeptID).Title;
        //    }
        //}

        [Display(Name = "選擇名單課別")]
        public int Department { get; set; }

        [Display(Name = "參與課別")]
        public string DepartmentListStr { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }        

        [Required]
        [Display(Name = "會議室編號")]
        public int RoomID { get; set; }
        public string RoomStr
        {
            get
            {
                return this.RoomID != 0 ? roomRepository.GetRoomString(this.RoomID) : "";
            }
        }

        public string RoomNum
        {
            get
            {
                return this.RoomID == 0 ? "" : roomRepository.FindBy(this.RoomID).RoomNumber;
            }
        }

        [Required]
        [Display(Name = "系列父層編號")]
        public int SeriesParentID { get; set; }

        [Display(Name = "系列課程編號")]
        public int TrainingCourseID { get; set; }

        [Required]
        [Display(Name = "訓練類別")]
        public int Category { get; set; }

        [Required]
        [Display(Name = "研訓類別")]
        public int Theme { get; set; }        
        public string ThemeTitle { get; set; }
        //{
        //    get
        //    {
        //        return themeRepository.GetById(Convert.ToInt32(this.Theme)).Title;
        //    }
        //}

        [Required]
        [Display(Name = "團體類別(含課後回饋方式)")]
        public int GroupType { get; set; }

        [Required]
        [Display(Name = "費用")]
        public int Fee { get; set; }        

        [Required]
        [Display(Name = "證書/證明")]
        public int Cert { get; set; }        

        [Display(Name = "講師")]
        public string Lecturer { get; set; }        

        [Required]
        [Display(Name = "簽到時間")]
        public DateTime SignDate { get; set; }        

        [Required]
        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }        

        [Required]
        [Display(Name = "時數")]
        public decimal Hours { get; set; }        

        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "線上會議連結")]
        public string Url { get; set; }

        [Display(Name = "注意事項")]
        public string Note { get; set; }

        [Display(Name = "備註")]
        public string Remark { get; set; }

        [Display(Name = "會議通知內容")]
        public string NotifyMsg { get; set; }

        [Display(Name = "上課照片")]
        public string MainPic { get; set; }
        public List<TrainingImage> PicList { get; set; }
        public List<HttpPostedFileBase> PicsFiles { get; set; }
        public HttpPostedFileBase TraningPic { get; set; }

        [Display(Name = "課堂講義")]
        public string MainFile { get; set; }
        public HttpPostedFileBase TraningFile { get; set; }

        [Display(Name = "課堂影片")]
        public string MainVideo { get; set; }
        public HttpPostedFileBase TraningVideo { get; set; }

        [Display(Name = "課堂報告")]
        public string MainReport { get; set; }
        public HttpPostedFileBase TraningReport { get; set; }

        [Display(Name = "驗收檔案")]
        public string MainAcceptance { get; set; }
        public HttpPostedFileBase TraningAcceptance { get; set; }

        [Display(Name = "設備")]
        public string Device { get; set; }

        [Required]
        [Display(Name = "已檢查")]
        [System.ComponentModel.DataAnnotations.Compare(nameof(IsTrue), ErrorMessage = "需確認並勾選設備檢查才可繼續")]
        public bool DeviceCheck { get; set; }

        [Required]
        [Display(Name = "可簽到狀態")]
        public bool IsSignable { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "可遲到分鐘")]
        public int LateMinutes { get; set; }

        [Required]
        [Display(Name = "已核准")]
        public bool IsApproved { get; set; }

        [Display(Name = "總經理評語")]
        public string PresidentComment { get; set; }

        [Required]
        [Display(Name = "總經理結案")]
        public bool PresidentClose { get; set; }

        [Display(Name = "校長評語")]
        public string PrincipalComment { get; set; }

        [Required]
        [Display(Name = "結案送審")]
        public bool AskClose { get; set; }

        [Required]
        [Display(Name = "結案")]
        public bool IsClose { get; set; }

        [Required]
        [Display(Name = "精選課程")]
        public bool IsModel { get; set; }
        public bool ModelID { get; set; }

        [Required]
        [Display(Name = "異動狀態")]
        public bool IsAbnormal { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }        

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }        

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public string CreaterDeptId {
            get
            {
                AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
                string account = adminRepository.GetById(this.Creater).Account;
                var t8Info = t8personRepository.FindByPersonId(account);
                return t8Info == null ? "0001" : t8Info.DeptID;
            }
        }

        public AdminInfo AdminInfo
        {
            get
            {
                return AdminInfoHelper.GetAdminInfo(); ;
            }
        }

        public List<TrainingPerson.TrainingPersonView> TrainingPersonInfo { get; set; }

        [Display(Name = "已選通知名單")]
        public List<EmployeeView> Notify { get; set; }

        [Display(Name = "選擇課別後勾選名單")]
        [Required(ErrorMessage = "請至少選擇一個通知名單")]
        public string selectedNotifyList { get; set; }

        #region 審核狀態
        public string ActionPlanStatus { get; set; }
        public string OpinionStatus { get; set; }
        public string ReportStatus { get; set; }
        public string EAStatus { get; set; }
        public ExternalAcceptanceView EAInfo { get; set; }
        public ExternalPlanView EPInfo { get; set; }
        public string IAStatus { get; set; }
        public InternalAcceptanceView IAInfo { get; set; }
        public InternalPlanView IPInfo { get; set; }
        public int AuditState { get; set; }
        public string AuditStatus { get; set; }
        public bool IsInvalid { get; set; }
        public bool HasAcceptance { get; set; }
        public bool HasOpinion { get; set; }
        public string CloseStatus { get; set; }
        #endregion

        public bool SameDeptManager { get; set; }

        public List<SelectListItem> VisionOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                VisionRepository visionRepository = new VisionRepository(new MeetingSignInDBEntities());
                var values = visionRepository.Query(true, Convert.ToInt32(DateTime.Now.Year));
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = "【" + v.Year + "】 " + v.Brief
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> RoomOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.GetAll();
                foreach (var v in values)
                {

                    result.Add(new SelectListItem()
                    {
                        Text = v.Place + " (" + v.RoomNumber + ")",
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> LateMinutesOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = EnumHelper.GetValues<LateMinutes>();
                for (int i = 0; i <= 60; i += 5)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = i.ToString(),
                        Text = i.ToString() + "分鐘"
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DeptIDOptions
        {
            get
            {

                List<SelectListItem> result = new List<SelectListItem>();
                var values = departmentRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.DeptID
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = t8personRepository.GetDeptQuery();
                var values = departmentRepository.Query(true).OrderBy(q => q.DeptID);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> CategoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Category>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> GroupTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<GroupType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ThemeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = themeRepository.Query(true);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> CertOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Certification>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }

        #region 結案送審文件
        [Display(Name = "滿意度調查分析表 / 建議回饋紀錄")]
        public string ResponseASSA { get; set; }
        public FilesView ResponseASSAFile { get; set; }

        [Display(Name = "課程結案報告")]
        public string ResponseASSB { get; set; }
        public FilesView ResponseASSBFile { get; set; }

        [Display(Name = "建議回饋紀錄")]
        public string ResponseASSC { get; set; }
        public FilesView ResponseASSCFile { get; set; }

        [Display(Name = "其他")]
        public string ResponseASSD { get; set; }
        public FilesView ResponseASSDFile { get; set; }

        [Display(Name = "評量紀錄 / 綜合檢討報告")]
        public string LearningASSA { get; set; }
        public FilesView LearningASSAFile { get; set; }

        [Display(Name = "心得分享 / 講師評鑑 / 講師課後回饋")]
        public string LearningASSB { get; set; }
        public FilesView LearningASSBFile { get; set; }

        [Display(Name = "回饋會議紀錄")]
        public string LearningASSC { get; set; }
        public FilesView LearningASSCFile { get; set; }

        [Display(Name = "其他")]
        public string LearningASSD { get; set; }
        public FilesView LearningASSDFile { get; set; }

        [Display(Name = "課後行動計畫表")]
        public string BehaviorASSA { get; set; }
        public FilesView BehaviorASSAFile { get; set; }

        [Display(Name = "課後追蹤評估回饋表 / 學習成效表 / 課後行動計畫實施成效查核統計表")]
        public string BehaviorASSB { get; set; }
        public FilesView BehaviorASSBFile { get; set; }

        [Display(Name = "績效考核表")]
        public string BehaviorASSC { get; set; }
        public FilesView BehaviorASSCFile { get; set; }

        [Display(Name = "其他")]
        public string BehaviorASSD { get; set; }
        public FilesView BehaviorASSDFile { get; set; }

        [Display(Name = "訓後成果評估")]
        public string OutcomeASSA { get; set; }
        public FilesView OutcomeASSAFile { get; set; }

        [Display(Name = "KPI 評估檢討報告")]
        public string OutcomeASSB { get; set; }
        public FilesView OutcomeASSBFile { get; set; }

        [Display(Name = "具體績效提升的相關報告")]
        public string OutcomeASSC { get; set; }
        public FilesView OutcomeASSCFile { get; set; }

        [Display(Name = "其他")]
        public string OutcomeASSD { get; set; }
        public FilesView OutcomeASSDFile { get; set; }

        [Display(Name = "其他")]
        public string EvaluationASSA { get; set; }
        public FilesView EvaluationASSAFile { get; set; }
        #endregion

    }

    public class FilesView
    {
        public int TrainingID { get; set; }
        public bool IsFinished { get; set; }
        public bool IsCreater { get; set; }
        public string MainFile { get; set; }
        public string IndexItem { get; set; }
        public string Description { get; set; }
        public string FileNameType { get; set; }
        public bool HasLink { get; set; }
        public string LinkText { get; set; }
        public string LinkDesc { get; set; }
        public bool HasSample { get; set; }
        public string SampleLink { get; set; }
        public bool CanUpload { get; set; }
        public List<string> ExistFiles { get; set; }
        public List<ClosingFile> FileList { get; set; }
        public List<HttpPostedFileBase> UploadFiles { get; set; }
    }
}