﻿using AutoMapper;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class InternalPlanView : AuditView
    {
        public InternalPlanView()
        {
            this.IsSkip = false;
        }

        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }

        [Required]
        [Display(Name = "訓練人數")]
        public int People { get; set; }

        [Required]
        [Display(Name = "訓練週數")]
        public int Weeks { get; set; }

        [Required]
        [Display(Name = "訓練需求調查")]
        public string Investigation { get; set; }

        [Required]

        [StringLength(100, ErrorMessage = "最多輸入100個字")]
        [Display(Name = "師資學歷")]
        public string Education { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "師資經歷")]
        public string Experience { get; set; }

        [StringLength(100, ErrorMessage = "最多輸入100個字")]
        [Display(Name = "師資專業領域")]
        public string Professional { get; set; }

        [Display(Name = "學員資格")]
        public string Eligible { get; set; }

        [Required]
        [Display(Name = "訓練目標")]
        public string Objective { get; set; }

        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "場地條件")]
        public string Conditions { get; set; }

        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "其他器材設備")]
        public string Devices { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "授課地點")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "L1【反應評估】滿意度調查機制")]
        public bool Response { get; set; }

        [Required]
        [Display(Name = "L2【學習評估】考試or心得報告機制")]
        public bool Learning { get; set; }

        [Required]
        [Display(Name = "L3【行為評估】課後行動計畫調查機制or實際運用於工作展現")]
        public bool Behavior { get; set; }

        [Required]
        [Display(Name = "L4【成果評估】工作績效調查機制")]
        public bool Outcome { get; set; }

        public List<InternalCourseView> CourseList { get; set; }

        [Required]
        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "利益關係文件")]
        public string StakeholderDoc { get; set; }
        public List<PlanHolder> HolderList { get; set; }
        public List<HttpPostedFileBase> HoldersFiles { get; set; }
        public HttpPostedFileBase TraningHolder { get; set; }

        [Display(Name = "利益關係人")]
        public string Stakeholders { get; set; }

        [Display(Name = "講師職能文件")]
        public string FunctionDocs { get; set; }
        public List<FunctionDoc> DocList { get; set; }
        public List<HttpPostedFileBase> DocFiles { get; set; }
        public HttpPostedFileBase FunctionDoc { get; set; }

        [Required]
        [Display(Name = "異動狀態")]
        public bool IsAbnormal { get; set; }

        public bool IsSkip { get; set; }

        public string JsonCourse { get; set; }

        public List<SelectListItem> StakeholdersOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<StackHolders>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}