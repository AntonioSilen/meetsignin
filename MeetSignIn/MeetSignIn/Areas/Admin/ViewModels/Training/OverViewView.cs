﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class OverViewView
    {
        ThemeRepository themeRepository = new ThemeRepository(new MeetingSignInDBEntities());

        [Display(Name = "訓練單位")]
        public string DeptID { get; set; }

        [Display(Name = "時數")]
        public decimal Hours { get; set; }

        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Display(Name = "訓練資料")]
        public List<TrainingView> TrainingList { get; set; }

        [Display(Name = "")]
        public List<DeptItem> DeptList { get; set; }
        public List<DeptItem> DepartmentList { get; set; }
        public DateTime DateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public List<SelectListItem> ThemeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = themeRepository.Query(true);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ID.ToString(),
                        Text = v.Title
                    });
                }
                return result;
            }
        }

        public StatisticsView TrainingStatistics { get; set;}
    }
    public class DeptItem
    {
        public string DeptID { get; set; }
        public string ParentDeptID { get; set; }
        public string Title { get; set; }
    }
    public class StatisticsView
    {
        public OverallStatisticsView Overall { get; set; }
        public InternalStatisticsView Internal { get; set; }
        public ExternalStatisticsView External { get; set; }
    }
    public class OverallStatisticsView
    {
        public int TrainingNum { get; set; }
        public int VisitorsNum { get; set; }
        public int TrainingHours { get; set; }
        public int TraineesNum { get; set; }
        public int SeniorNum { get; set; }
        public int JuniorNum { get; set; }
        public int EmployeeNum { get; set; }
        public int YoungNum { get; set; }
    }
    public class InternalStatisticsView
    {
        public int TrainingNum { get; set; }
        public int VisitorsNum { get; set; }
        public int TrainingHours { get; set; }
        public int TraineesNum { get; set; }
        public int SeniorNum { get; set; }
        public int JuniorNum { get; set; }
        public int EmployeeNum { get; set; }
        public int TotalFee { get; set; }
    }
    public class ExternalStatisticsView
    {
        public int TrainingNum { get; set; }
        public int VisitorsNum { get; set; }
        public int TrainingHours { get; set; }
        public int TraineesNum { get; set; }
        public int SeniorNum { get; set; }
        public int JuniorNum { get; set; }
        public int EmployeeNum { get; set; }
        public int TotalFee { get; set; }
    }
}