﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class ExternalCourseIndexView
    {
        public List<ExternalCourseView> CourseList { get; set; }

        public string JsonCourse { get; set; }

        public TrainingView TrainingInfo { get; set; }

        public TrainingView UploadFilesData { get; set; }
        public int SeriesTrainingID { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Required]
        [Display(Name = "外訓編號")]
        public int ExternalID { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "主辦單位")]
        public string Organizer { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "最多輸入50個字")]
        [Display(Name = "講師姓名")]
        public string Lecturer { get; set; }

        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "教材")]
        public string CourseMaterial { get; set; }

        [Required]
        [StringLength(200, ErrorMessage = "最多輸入200個字")]
        [Display(Name = "地點")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "開始日期")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "結束日期")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "報名費")]
        public int Registery { get; set; }

        [Display(Name = "差旅")]
        public int Travel { get; set; }

        [Display(Name = "其他")]
        public int Others { get; set; }

        [Required]
        [Display(Name = "合計")]
        public int Total { get; set; }
    }
}