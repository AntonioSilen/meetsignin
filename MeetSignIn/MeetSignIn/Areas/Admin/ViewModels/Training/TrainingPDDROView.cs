﻿using MeetSignIn.Areas.Admin.ViewModels.Competency;
using MeetSignIn.Areas.Admin.ViewModels.ExternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.InternalAcceptance;
using MeetSignIn.Areas.Admin.ViewModels.ReviewReport;
using MeetSignIn.Areas.Admin.ViewModels.Vision;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class TrainingPDDROView : PageQuery
    {
        public TrainingPDDROView()
        {

        }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "訓練類別")]
        public int Category { get; set; }

        public string DeptPrincipal { get; set; }
        public string FairlyPrincipal { get; set; }

        public CompetencyView CompetencyInfo { get; set; }

        public TrainingView TrainingInfo { get; set; }
        public VisionView VisionInfo { get; set; }
        public List<TrainingPerson.TrainingPersonView> TrainingPersonInfo { get; set; }
        public GroupJoinPersonAndDepartment HRInfo { get; set; }

        public List<TrainingView> SeriesTrainingList { get; set; }

        #region 審查
        public List<TrainingAbnormal.TrainingAbnormalView> TrainingAbnormalInfo { get; set; }
        public ReviewReportView ReviewReportInfo { get; set; }

        #endregion
        public ExternalPlanView ExternalPlanInfo { get; set; }
        public List<ExternalAcceptanceView> ExternalAcceptanceInfo { get; set; }
        public InternalPlanView InternalPlanInfo { get; set; }
        public List<InternalAcceptanceView> InternalAcceptanceInfo { get; set; }

        [Display(Name = "承辦人職能文件")]
        public List<FunctionDoc> CreaterDocList { get; set; }

        #region 驗收方式
        public bool HasOpinion { get; set; }
        public bool HasPerformance { get; set; }
        public bool HasActionPlan { get; set; }
        public bool HasLearning { get; set; }
        public bool HasReport { get; set; }
        public bool HasCertificate { get; set; }
        public bool HasDelegate { get; set; }
        #endregion
    }

}