﻿using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class TrainingIndexView : PageQuery
    {
        public TrainingIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;

            this.StartDate = DateTime.Now.AddYears(-1);
            //this.StartDate = Convert.ToDateTime( DateTime.Now.Year + "/01/01");
            this.EndDate = DateTime.Now.AddMonths(3);

            var adminInfo = AdminInfoHelper.GetAdminInfo();
            if (adminInfo != null && adminInfo.Type > (int)AdminType.Manager)
            {
                this.IsOfficer = true;
            }
        }

        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());

        public PageResult<TrainingView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int TrainingID { get; set; }
        
        [Display(Name = "編號")]
        public string PersonID { get; set; }
        public string PersonName { get; set; }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "課程編號")]
        public string MeetingNumber { get; set; }

        //[Required]
        [Display(Name = "課程名稱")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "發起人")]
        public string Organiser { get; set; }

        [Required]
        [Display(Name = "訓練單位")]
        public string DeptID { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "受訓單位")] 
        public string DepartmentStr { get; set; }

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        [Required]
        [Display(Name = "會議室編號")]
        public int RoomID { get; set; }

        [Required]
        [Display(Name = "訓練類別")]
        public int Category { get; set; }

        [Required]
        [Display(Name = "研訓類別")]
        public int Theme { get; set; }

        [Required]
        [Display(Name = "費用")]
        public int Fee { get; set; }

        [Required]
        [Display(Name = "證書/證明")]
        public int Cert { get; set; }

        [Display(Name = "講師")]
        public string Lecturer { get; set; }

        [Required]
        [Display(Name = "簽到時間")]
        public DateTime SignDate { get; set; }

        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "時數")]
        public int Hours { get; set; }

        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "線上會議連結")]
        public string Url { get; set; }

        [Display(Name = "備註")]
        public string Remark { get; set; }

        [Display(Name = "上課照片")]
        public string MainPic { get; set; }

        [Display(Name = "課堂講義")]
        public string MainFile { get; set; }

        [Display(Name = "課堂影片")]
        public string MainVideo { get; set; }

        [Required]
        [Display(Name = "可簽到狀態")]
        public bool IsSignable { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "是否結案")]
        public bool? IsClose { get; set; }

        [Required]
        [Display(Name = "可遲到分鐘")]
        public int LateMinutes { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public string Creater { get; set; }

        [Display(Name = "顯示(本帳號)承辦的訓練")]
        public bool IsOfficer { get; set; }

        [Display(Name = "顯示當日課程")]
        public bool ShowToday { get; set; }

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> RoomOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.RoomNumber,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DeptIDOptions
        {
            get
            {
                //須建立自動更新T8資料(Insert新部門) ?????
                //var 


                List<SelectListItem> result = new List<SelectListItem>();
                var values = departmentRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.DeptID
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> CategoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Category>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}