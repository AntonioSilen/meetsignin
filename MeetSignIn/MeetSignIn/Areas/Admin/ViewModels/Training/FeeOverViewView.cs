﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Training
{
    public class FeeOverViewView : OverViewView
    {
        [Display(Name = "課程")]
        public string TrainingTItle { get; set; }    

        [Display(Name = "承辦人")]
        public string Organizer { get; set; }

        [Display(Name = "上課時間")]
        public string CourseDate { get; set; }

        [Display(Name = "講師")]
        public string Lecturer { get; set; }

        [Display(Name = "報名費")]
        public string Registery { get; set; }

        [Display(Name = "差旅費")]
        public string Travel { get; set; }

        [Display(Name = "其他費用")]
        public string Others { get; set; }

        [Display(Name = "總額")]
        public string Total { get; set; }

        public List<ExternalTrainingModel> ExternalTrainingList { get; set; }
    }

    public class ExternalTrainingModel
    {
        public int TrainingID { get; set; }
        public string Title { get; set; }
        public string Organizer { get; set; }
        public string Lecturer { get; set; }
        public string CourseDate { get; set; }
        public int Registery { get; set; }
        public int Travel { get; set; }
        public int Others { get; set; }
        public int Total { get; set; }
        public int SignedCount { get; set; }
    }
}