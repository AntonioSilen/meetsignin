﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Check
{
    public class CheckView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "審核對象編號")]
        public int TargetID { get; set; }

        [Display(Name = "待審KPI標題/個人目標")]
        public string TargetTitle { get; set; }

        [Display(Name = "部門KPI編號")]
        public int ParentKPIID { get; set; }

        [Display(Name = "上層指標")]
        public string ParentKPITitle { get; set; }

        [Display(Name = "涵蓋部門")]
        public string Dept { get; set; }

        [Display(Name = "審核對象部門")]
        public string DeptName { get; set; }

        [Display(Name = "審核人員姓名")]
        public string PersonName { get; set; }

        [Display(Name = "審核對象類型")]
        public int TargetType { get; set; }

        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }

        [Display(Name = "備註")]
        public string Remark { get; set; }

        [Display(Name = "附件")]
        public string AttachFile { get; set; }
        public List<CheckImage> AttachList { get; set; }
        public List<HttpPostedFileBase> AttachFiles { get; set; }

        [Display(Name = "審核時間")]
        public DateTime AuditDate { get; set; }

        [Display(Name = "審核人員")]
        public string Auditors { get; set; }
        public string AuditorName { get; set; }

        [Display(Name = "送審時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "審核人員")]
        public int Creater { get; set; }
        public string CreaterName { get; set; }

        public List<CheckChildModel> ChildKPIList { get; set; }

        public List<SelectListItem> TargetTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<TargetType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }
    }
}