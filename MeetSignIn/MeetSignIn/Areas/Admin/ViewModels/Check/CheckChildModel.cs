﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Check
{
    public class CheckChildModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int ApprovalState { get; set; }
        public DateTime FinishDate { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsOnline { get; set; }
    }
}