﻿using MeetSignIn.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.DrawPerson
{
    public class DrawPersonIndexView : PageQuery
    {
        public DrawPersonIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<DrawPersonView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "抽籤活動編號")]
        public int DrawID { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "姓名")]
        public string PersonName { get; set; }

        [Display(Name = "抽籤狀態")]
        public int DrawStatus { get; set; }

        [Display(Name = "可抽籤")]
        public bool? IsOnline { get; set; }

        //[Display(Name = "中獎項目")]
        //public string Win { get; set; }
    }
}