﻿using MeetSignIn.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Draw
{
    public class DrawIndexView : PageQuery
    {
        public DrawIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<DrawView> PageResult { get; set; }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "類別")]
        public int Type { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        public List<SelectListItem> DrawTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "1",
                    Text = "全廠"
                });
                result.Add(new SelectListItem()
                {
                    Value = "2",
                    Text = "匯入"
                });

                return result;
            }
        }
    }
}