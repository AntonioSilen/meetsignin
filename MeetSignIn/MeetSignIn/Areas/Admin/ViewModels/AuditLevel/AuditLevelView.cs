﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.AuditLevel
{
    public class AuditLevelView
    {
        public AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        public Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public AuditLevelView()
        {
            this.PrincipalReview = "0000";
            this.PresidentReview = "0000";
            this.ViceReview = "0000";
            this.ManagerReview = "0000";
            this.CSReview = "0000";
            this.TLReview = "0000";
            this.HRReview = "0000";
        }

        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "部門編號")]
        public string DeptId { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "校長審核")]
        public string PrincipalReview { get; set; }

        [Required]
        [Display(Name = "總經理審核")]
        public string PresidentReview { get; set; }

        [Required]
        [Display(Name = "副總經理審核")]
        public string ViceReview { get; set; }

        [Required]
        [Display(Name = "主管審核")]
        public string ManagerReview { get; set; }

        [Required]
        [Display(Name = "課級主管審核")]
        public string CSReview { get; set; }

        [Required]
        [Display(Name = "班組長審核")]
        public string TLReview { get; set; }

        [Required]
        [Display(Name = "人資審核")]
        public string HRReview { get; set; }
      
        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        public List<SelectListItem> PrincipalOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                //var values = adminRepository.Query(true, (int)AdminType.Principal);
                var values = adminRepository.Query(true, 0, "", true);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> PresidentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.President);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ViceOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.Vice);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> ManagerOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.Manager);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result;
            }
        }

        public string[] SelectedCSIds { get; set; }
        public IEnumerable<SelectListItem> CSItems
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.ClassSupervisor);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result.OrderBy(q => q.Text).AsEnumerable();
            }
        }
        public List<SelectListItem> CSOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.ClassSupervisor);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result.OrderBy(q => q.Text).ToList();
            }
        }


        public string[] SelectedTLIds { get; set; }
        public IEnumerable<SelectListItem> TLItems
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.TeamLeader);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result.OrderBy(q => q.Text).AsEnumerable();
            }
        }
        public List<SelectListItem> TLOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.TeamLeader);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result.OrderBy(q => q.Text).ToList();
            }
        }
        public List<SelectListItem> HROptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Value = "0000",
                    Text = "無須審核"
                });
                var values = adminRepository.Query(true, (int)AdminType.HR);
                foreach (var v in values)
                {
                    var personInfo = t8personRepository.FindByPersonId(v.Account);
                    string deptName = personInfo == null ? "菲力工業" : personInfo.DeptName;
                    result.Add(new SelectListItem()
                    {
                        Value = v.Account,
                        Text = deptName + " - " + v.Name
                    });
                }
                return result;
            }
        }
    }
}