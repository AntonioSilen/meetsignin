﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels
{
    public class BasicSettingView
    {
        public List<SelectListItem> YearOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                int thisYear = DateTime.Now.Year;
                for (int i = 2010; i <= thisYear + 5; i++)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = i.ToString() + "年",
                        Value = i.ToString()
                    });
                }

                return result;
            }
        }

        public List<SelectListItem> MonthOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                for (int i = 1; i <= 12; i++)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = i.ToString() + "月",
                        Value = i.ToString()
                    });
                }

                return result;
            }
        }
        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                Repositories.T8Repositories.comPersonRepository compersonRepository = new Repositories.T8Repositories.comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }
    }
}