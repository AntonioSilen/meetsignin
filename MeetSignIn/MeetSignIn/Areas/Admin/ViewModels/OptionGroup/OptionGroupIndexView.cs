﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.OptionGroup
{
    public class OptionGroupIndexView : PageQuery
    {
        public OptionGroupIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<OptionGroupView> PageResult { get; set; }

        public int ID { get; set; }
        [Display(Name = "選項名稱")]
        public string Title { get; set; }

    }
}