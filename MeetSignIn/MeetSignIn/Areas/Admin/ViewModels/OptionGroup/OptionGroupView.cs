﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.OptionGroup
{
    public class OptionGroupView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "選項名稱")]
        public string Title { get; set; }

        public List<OptionItemView> ItemList { get; set; }
    }

    public class OptionItemView
    {        
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "選項組編號")]
        public string OptionGroupID { get; set; }

        [Required]
        [Display(Name = "選項")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }
        
        [Required]
        [Display(Name = "排序")]
        public string Sort { get; set; }


        [Display(Name = "此問題編號")]
        public int QNRQID { get; set; }
        [Display(Name = "此問題")]
        public string QNRQTitle { get; set; }

        [Display(Name = "觸發問題編號")]
        public int LinkQNRQID { get; set; }
        [Display(Name = "觸發問題")]
        public string LinkQNRQTitle { get; set; }
    }
}