﻿using MeetSignIn.Areas.Admin.ViewModels.PersonFile;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Areas.Admin.ViewModels.TrainingPerson;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Person
{
    public class PersonView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Required]
        [Display(Name = "訓練種類/職能")]
        public string Competency { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        public PersonInfo PersonInfo { get; set; }
        public List<TrainingPersonListModel> TrainingList { get; set; }
        public int TrainingCount { get; set; }

        [Display(Name = "人員資料")]
        public List<PersonFileView> PersonFileList { get; set; }
        public List<HttpPostedFileBase> PersonFiles { get; set; }
    }

    public class PersonInfo
    {
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "性別")]
        public string Gender { get; set; }

        [Display(Name = "部門")]
        public string Department { get; set; }

        [Display(Name = "職位")]
        public string JobTitle { get; set; }

        [Display(Name = "學歷")]
        public string Educate { get; set; }

        [Display(Name = "到職日")]
        public int InductionDate { get; set; }
        public string InductionDateStr { get; set; }

        [Display(Name = "離職")]
        public string Quit { get; set; }
    }

    public class TrainingPersonListModel
    {
        public TrainingView TrainingInfo { get; set; }
        public TrainingPersonView TrainingPersonInfo { get; set; }
        public int AuditState { get; set; }
        public string AuditStatus { get; set; }

        public string EAStatus { get; set; }
        public string IAStatus { get; set; }
    }
}