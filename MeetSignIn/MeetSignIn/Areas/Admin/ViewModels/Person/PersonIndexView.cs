﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.Repositories.T8Repositories;

namespace MeetSignIn.Areas.Admin.ViewModels.Person
{
    public class PersonIndexView : PageQuery
    {
        public PersonIndexView()
        {
            this.Sorting = "JobID";
            this.IsDescending = false;
        }

        public PageResult<PersonView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Display(Name = "課別")]
        public string DeptID { get; set; }

        [Display(Name = "訓練種類/職能")]
        public string Competency { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "部門")]
        public string Department { get; set; }

        [Display(Name = "職位")]
        public string JobTitle { get; set; }

        [Display(Name = "訓練次數")]
        public int TrainingCount { get; set; }

        [Display(Name = "更新日期")]
        public DateTime UpdateDate { get; set; }

        //public List<SelectListItem> DeptOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
        //        var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Text = v.Key.DeptName,
        //                Value = (v.Key.DeptName).ToString()
        //            });
        //        }

        //        return result;
        //    }
        //}
    }
}