﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels
{
    public class GeneralAuditView
    {
        [Display(Name = "送審")]
        public bool IsAudit { get; set; }

        #region 主管
        public string ManagerDeptId { get; set; }
        public string ManagerDeptName { get; set; }
        public string ManagerName { get; set; }

        [Display(Name = "主管")]
        public int ManagerAudit { get; set; }

        [Display(Name = "簽章")]
        public string ManagerSignature { get; set; }
        public HttpPostedFileBase ManagerPost { get; set; }

        [Display(Name = "主管備註")]
        public string ManagerRemark { get; set; }

        [Display(Name = "主管核日期")]
        public DateTime ManagerAuditDate { get; set; }
        #endregion

        #region 副總
        public string ViceDeptId { get; set; }
        public string ViceDeptName { get; set; }
        public string ViceName { get; set; }

        [Display(Name = "副總")]
        public int ViceAudit { get; set; }

        [Display(Name = "簽章")]
        public string ViceSignature { get; set; }
        public HttpPostedFileBase VicePost { get; set; }

        [Display(Name = "副總備註")]
        public string ViceRemark { get; set; }

        [Display(Name = "副總簽核日期")]
        public DateTime ViceAuditDate { get; set; }
        #endregion

        #region 承辦人
        public string OfficerDeptId { get; set; }
        public string OfficerDeptName { get; set; }
        public string OfficerName { get; set; }

        [Display(Name = "承辦人")]
        public int OfficerAudit { get; set; }

        [Display(Name = "簽章")]
        public string OfficerSignature { get; set; }
        public HttpPostedFileBase OfficerPost { get; set; }

        [Display(Name = "承辦人備註")]
        public string OfficerRemark { get; set; }

        [Display(Name = "承辦人簽核日期")]
        public DateTime OfficerAuditDate { get; set; }
        #endregion

        #region 總經理
        public string PresidentDeptId { get; set; }
        public string PresidentDeptName { get; set; }
        public string PresidentName { get; set; }

        [Display(Name = "總經理")]
        public int PresidentAudit { get; set; }

        [Display(Name = "簽章")]
        public string PresidentSignature { get; set; }
        public HttpPostedFileBase PresidentPost { get; set; }

        [Display(Name = "總經理備註")]
        public string PresidentRemark { get; set; }

        [Display(Name = "總經理簽核日期")]
        public DateTime PresidentAuditDate { get; set; }
        #endregion

        public Models.Audit AuditInfo { get; set; }

        public Models.AuditLevel AuditLevelInfo { get; set; }
    }
}