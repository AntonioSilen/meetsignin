﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.ClassOpinion
{
    public class ClassOpinionIndexView : PageQuery
    {
        public ClassOpinionIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<ClassOpinionView> PageResult { get; set; }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                return Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
            }
        }

        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "內容與主題契合度")]
        public int Compatibility { get; set; }

        [Display(Name = "內容與主題契合度---建議事項")]
        public string CompatibilityOpinion { get; set; }

        [Display(Name = "課程架構")]
        public int Architecture { get; set; }

        [Display(Name = "課程架構---建議事項")]
        public string ArchitectureOpinion { get; set; }

        [Display(Name = "課程時間安排")]
        public int Schedule { get; set; }

        [Display(Name = "課程時間安排---建議事項")]
        public string ScheduleOpinion { get; set; }

        [Display(Name = "教材設備")]
        public int CourseMaterials { get; set; }

        [Display(Name = "教材設備---建議事項")]
        public string CourseMaterialsOpinion { get; set; }

        [Display(Name = "教室整體設施")]
        public int Device { get; set; }

        [Display(Name = "教室整體設施---建議事項")]
        public string DeviceOpinion { get; set; }

        [Display(Name = "對主題之專業程度")]
        public int Professional { get; set; }

        [Display(Name = "對主題之專業程度---建議事項")]
        public string ProfessionalOpinion { get; set; }

        [Display(Name = "表達技巧")]
        public int Expression { get; set; }

        [Display(Name = "表達技巧---建議事項")]
        public string ExpressionOpinion { get; set; }

        [Display(Name = "課程時間掌握能力")]
        public int TimeControl { get; set; }

        [Display(Name = "課程時間掌握能力---建議事項")]
        public string TimeControlOpinion { get; set; }

        [Display(Name = "教學方式")]
        public int TeachingMethod { get; set; }

        [Display(Name = "教學方式---建議事項")]
        public string TeachingMethodOpinion { get; set; }

        [Display(Name = "您是否會像其他同仁推薦該講師之課程")]
        public int Recommended { get; set; }

        [Display(Name = "您是否會像其他同仁推薦該講師之課程---建議事項")]
        public string RecommendedOpinion { get; set; }

        [Display(Name = "具實際應用價值")]
        public int ActualUsage { get; set; }

        [Display(Name = "具實際應用價值---建議事項")]
        public string ActualUsageOpinion { get; set; }

        [Display(Name = "應用於目前工作上的程度")]
        public int CurrentUsage { get; set; }

        [Display(Name = "應用於目前工作上的程度---建議事項")]
        public string CurrentUsageOpinion { get; set; }

        [Display(Name = "應用於未來工作上的程度")]
        public int FutureUsage { get; set; }

        [Display(Name = "應用於未來工作上的程度---建議事項")]
        public string FutureUsageOpinion { get; set; }

        [Display(Name = "對您而言，本課程內容的難易程度")]
        public int Difficulty { get; set; }

        [Display(Name = "期望後續安排哪方面課程")]
        public string Expect { get; set; }

        [Display(Name = "其他建議")]
        public string Advice { get; set; }

        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "初審")]
        public bool? FirstAudit { get; set; }

        [Display(Name = "初審備註")]
        public string FirstRemark { get; set; }

        [Display(Name = "複審")]
        public bool? SecondAudit { get; set; }

        [Display(Name = "複審備註")]
        public string SecondRemark { get; set; }

        [Display(Name = "承辦單位承辦人")]
        public bool? OfficerAudit { get; set; }

        [Display(Name = "承辦人備註")]
        public string OfficerRemark { get; set; }

        [Display(Name = "承辦人簽核日期")]
        public DateTime OfficerAuditDate { get; set; }

        [Display(Name = "總經理")]
        public bool? PresidentAudit { get; set; }

        [Display(Name = "總經理備註")]
        public string PresidentRemark { get; set; }

        [Display(Name = "總經理簽核日期")]
        public DateTime PresidentAuditDate { get; set; }
    }
}