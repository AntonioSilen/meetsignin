﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.ClassOpinion
{
    public class ClassOpinionView : AuditView
    {
        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var query = trainingRepository.GetById(this.TrainingID);
                var info = Mapper.Map<TrainingView>(query);
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }

        [Required]
        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "內容與主題契合度")]
        public int Compatibility { get; set; }

        [Display(Name = "內容與主題契合度---建議事項")]
        public string CompatibilityOpinion { get; set; }

        [Required]
        [Display(Name = "課程架構")]
        public int Architecture { get; set; }

        [Display(Name = "課程架構---建議事項")]
        public string ArchitectureOpinion { get; set; }

        [Required]
        [Display(Name = "課程時間安排")]
        public int Schedule { get; set; }

        [Display(Name = "課程時間安排---建議事項")]
        public string ScheduleOpinion { get; set; }

        [Required]
        [Display(Name = "教材設備")]
        public int CourseMaterials { get; set; }

        [Display(Name = "教材設備---建議事項")]
        public string CourseMaterialsOpinion { get; set; }

        [Required]
        [Display(Name = "教室整體設施")]
        public int Device { get; set; }

        [Display(Name = "教室整體設施---建議事項")]
        public string DeviceOpinion { get; set; }

        [Required]
        [Display(Name = "對主題之專業程度")]
        public int Professional { get; set; }

        [Display(Name = "對主題之專業程度---建議事項")]
        public string ProfessionalOpinion { get; set; }

        [Required]
        [Display(Name = "表達技巧")]
        public int Expression { get; set; }

        [Display(Name = "表達技巧---建議事項")]
        public string ExpressionOpinion { get; set; }

        [Required]
        [Display(Name = "課程時間掌握能力")]
        public int TimeControl { get; set; }

        [Display(Name = "課程時間掌握能力---建議事項")]
        public string TimeControlOpinion { get; set; }

        [Required]
        [Display(Name = "教學方式")]
        public int TeachingMethod { get; set; }

        [Display(Name = "教學方式---建議事項")]
        public string TeachingMethodOpinion { get; set; }

        [Required]
        [Display(Name = "您是否會像其他同仁推薦該講師之課程")]
        public int Recommended { get; set; }

        [Display(Name = "您是否會像其他同仁推薦該講師之課程---建議事項")]
        public string RecommendedOpinion { get; set; }

        [Required]
        [Display(Name = "具實際應用價值")]
        public int ActualUsage { get; set; }

        [Display(Name = "具實際應用價值---建議事項")]
        public string ActualUsageOpinion { get; set; }

        [Required]
        [Display(Name = "應用於目前工作上的程度")]
        public int CurrentUsage { get; set; }

        [Display(Name = "應用於目前工作上的程度---建議事項")]
        public string CurrentUsageOpinion { get; set; }

        [Required]
        [Display(Name = "應用於未來工作上的程度")]
        public int FutureUsage { get; set; }

        [Display(Name = "應用於未來工作上的程度---建議事項")]
        public string FutureUsageOpinion { get; set; }

        [Required]
        [Display(Name = "對您而言，本課程內容的難易程度")]
        public int Difficulty { get; set; }

        [Required]
        [Display(Name = "期望後續安排哪方面課程")]
        public string Expect { get; set; }

        [Required]
        [Display(Name = "其他建議")]
        public string Advice { get; set; }

        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }    
    }
}