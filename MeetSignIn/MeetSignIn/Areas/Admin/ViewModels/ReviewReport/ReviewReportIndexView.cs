﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.ReviewReport
{
    public class ReviewReportIndexView : PageQuery
    {
        public ReviewReportIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<ReviewReportView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "檔名")]
        public string FileName { get; set; }

        [Display(Name = "年度")]
        public int Year { get; set; }

        [Display(Name = "月份")]
        public int Month { get; set; }
    }
}