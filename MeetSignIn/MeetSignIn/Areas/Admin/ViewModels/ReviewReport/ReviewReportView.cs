﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.ReviewReport
{
    public class ReviewReportView : BasicSettingView
    {
        public ReviewReportView()
        {
            this.Year = DateTime.Now.Year;
        }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "檔名")]
        public string FileName { get; set; }
        public HttpPostedFileBase FileNamePost { get; set; }

        [Required]
        [Display(Name = "年度")]
        public int Year { get; set; }

        [Required]
        [Display(Name = "月份")]
        public int Month { get; set; }
    }
}