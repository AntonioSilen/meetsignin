﻿using MeetSignIn.Models;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.KPI
{
    public class KPIView : BaseView
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "類型")]
        public int Type { get; set; }
        public string TypeStr { get; set; }

        [Display(Name = "層級")]
        public int Level { get; set; }
        public string LevelStr { get; set; }

        [Display(Name = "公司KPI")]
        public int ParentKPIID { get; set; }
        public string ParentKPITitle { get; set; }

        [Display(Name = "涵蓋部門")]
        public string Dept { get; set; }

        [Display(Name = "描述")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }

        [Required]
        [Display(Name = "預計檢核時間")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckDate { get; set; }
        public string CheckDateStr { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        public List<KPIView> DeptKPIList { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<KPIType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> LevelOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<KPILevel>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        //public List<SelectListItem> DeptOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
        //        var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Text = v.Key.DeptName,
        //                Value = (v.Key.DeptId).ToString()
        //            });
        //        }

        //        return result;
        //    }
        //}
    }
}