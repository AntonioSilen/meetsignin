﻿using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.KPI
{
    public class KPIIndexView : PageQuery
    {
        public KPIIndexView()
        {
            this.Sorting = "Type";
            this.IsDescending = false;
        }

        public PageResult<KPIView> PageResult { get; set; }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "層級")]
        public int Level { get; set; }

        [Display(Name = "公司KPI")]
        public int ParentKPIID { get; set; }
        public string ParentKPITitle { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "涵蓋部門")]
        public string Dept { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }

        [Display(Name = "審核狀態")]
        public int? ApprovalState { get; set; }

        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "預計檢核時間")]
        public DateTime CheckDate { get; set; }

        public List<SelectListItem> TypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<KPIType>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> FairlyKPIOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                KPIRepository kPIRepository = new KPIRepository(new MeetingSignInDBEntities());
                var values = kPIRepository.Query(true, "", 0, 1);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> LevelOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<KPILevel>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        public List<SelectListItem> ApprovalStateOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<ApprovalState>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = EnumHelper.GetDescription(v),
                        Value = ((int)v).ToString()
                    });
                }
                return result;
            }
        }

        //public List<SelectListItem> DeptOptions
        //{
        //    get
        //    {
        //        List<SelectListItem> result = new List<SelectListItem>();
        //        comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
        //        var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
        //        foreach (var v in values)
        //        {
        //            result.Add(new SelectListItem()
        //            {
        //                Text = v.Key.DeptName,
        //                Value = (v.Key.DeptId).ToString()
        //            });
        //        }

        //        return result;
        //    }
        //}
    }
}