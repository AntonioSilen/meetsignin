﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels
{
    public class AuditView
    {
        public AuditView()
        {
            int AuditApproval = (int)FormAudit.Approval;
            this.IsAudited = this.ManagerAudit >= AuditApproval || this.ViceAudit >= AuditApproval || this.PresidentAudit >= AuditApproval || this.PrincipalAudit >= AuditApproval;
        }
        [Display(Name = "送審")]
        public bool IsAudit { get; set; }
        public bool IsAudited { get; set; }

        //承辦/編寫人
        public string OfficerDeptId { get; set; }
        public string OfficerDeptName { get; set; }
        public string OfficerName { get; set; }

        [Display(Name = "承辦人")]
        public int OfficerAudit { get; set; }

        [Display(Name = "簽章")]
        public string OfficerSignature { get; set; }
        public HttpPostedFileBase OfficerPost { get; set; }

        [Display(Name = "承辦人備註/審核結果建議")]
        public string OfficerRemark { get; set; }

        [Display(Name = "承辦人簽核日期")]
        public DateTime OfficerAuditDate { get; set; }

        //主管
        public string ManagerDeptId { get; set; }
        public string ManagerDeptName { get; set; }
        public string ManagerName { get; set; }

        [Display(Name = "主管")]
        public int ManagerAudit { get; set; }

        [Display(Name = "簽章")]
        public string ManagerSignature { get; set; }
        public HttpPostedFileBase ManagerPost { get; set; }

        [Display(Name = "主管備註/審核結果建議")]
        public string ManagerRemark { get; set; }

        [Display(Name = "主管核日期")]
        public DateTime ManagerAuditDate { get; set; }

        //副總
        public string ViceDeptId { get; set; }
        public string ViceDeptName { get; set; }
        public string ViceName { get; set; }

        [Display(Name = "副總")]
        public int ViceAudit { get; set; }

        [Display(Name = "簽章")]
        public string ViceSignature { get; set; }
        public HttpPostedFileBase VicePost { get; set; }

        [Display(Name = "副總備註/審核結果建議")]
        public string ViceRemark { get; set; }

        [Display(Name = "副總簽核日期")]
        public DateTime ViceAuditDate { get; set; }

        //總經理
        public string PresidentDeptId { get; set; }
        public string PresidentDeptName { get; set; }
        public string PresidentName { get; set; }

        [Display(Name = "總經理")]
        public int PresidentAudit { get; set; }

        [Display(Name = "簽章")]
        public string PresidentSignature { get; set; }
        public HttpPostedFileBase PresidentPost { get; set; }

        [Display(Name = "總經理備註/審核結果建議")]
        public string PresidentRemark { get; set; }

        [Display(Name = "總經理簽核日期")]
        public DateTime PresidentAuditDate { get; set; }

        //人資
        public string HRDeptId { get; set; }
        public string HRDeptName { get; set; }
        public string HRName { get; set; }

        [Display(Name = "承辦人")]
        public int HRAudit { get; set; }

        [Display(Name = "簽章")]
        public string HRSignature { get; set; }
        public HttpPostedFileBase HRPost { get; set; }

        [Display(Name = "承辦人備註")]
        public string HRRemark { get; set; }

        [Display(Name = "承辦人簽核日期")]
        public DateTime HRAuditDate { get; set; }


        //校長
        public string PrincipalDeptId { get; set; }
        public string PrincipalDeptName { get; set; }
        public string PrincipalName { get; set; }

        [Display(Name = "校長")]
        public int PrincipalAudit { get; set; }

        [Display(Name = "簽章")]
        public string PrincipalSignature { get; set; }
        public HttpPostedFileBase PrincipalPost { get; set; }

        [Display(Name = "校長備註/審核結果建議")]
        public string PrincipalRemark { get; set; }

        [Display(Name = "校長簽核日期")]
        public DateTime PrincipalAuditDate { get; set; }

        public Models.Audit AuditInfo { get; set; }

        public Models.AuditLevel AuditLevelInfo { get; set; }
    }
}