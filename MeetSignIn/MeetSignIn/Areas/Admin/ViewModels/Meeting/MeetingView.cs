﻿using MeetSignIn.Areas.Admin.ViewModels.Employee;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Meeting
{
    public class MeetingView
    {
        [Required]
        public int ID { get; set; }

        [Display(Name = "會議編號")]
        public string MeetingNumber { get; set; }

        [Required]
        [Display(Name = "會議主題")]
        [StringLength(50, ErrorMessage = "請填寫主題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "發起人")]
        [StringLength(50, ErrorMessage = "請填寫發起人")]
        public string Organiser { get; set; }

        [Required]
        [Display(Name = "課別")]
        public int Department { get; set; }

        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        SignInRepository signInRepository = new SignInRepository(new MeetingSignInDBEntities());
        [Display(Name = "參與課別")]
        public string DepartmentListStr { get; set; }
        //{
        //    get
        //    {
        //        var signInInfo = signInRepository.GetSignInInfoList(this.ID).Select(q => q.DepartmentTitle).Distinct().ToList();
        //        return signInInfo.Aggregate((a, b) => a + ", " + b);
        //    }
        //}

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

      

        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        public string RoomStr
        {
            get
            {
                //var roomInfo = roomRepository.FindBy(this.RoomID);
                //if (roomInfo != null)
                //{
                //    var factory = EnumHelper.GetDescription((Factory)roomInfo.FactoryID);
                //    return "(" + roomInfo.RoomNumber + ")" + factory + " " + roomInfo.Place;
                //}
                //return "";
                return this.RoomID != 0 ? roomRepository.GetRoomString(this.RoomID) : "";
            } 
        }

        public string RoomNum
        {
            get
            {
                return this.RoomID == 0 ? "" : roomRepository.FindBy(this.RoomID).RoomNumber;
            }
        }

        [Required]
        [Display(Name = "會議室")]
        public int RoomID { get; set; }

        [Required]
        [Display(Name = "開放簽到時間")]
        public DateTime SignDate { get; set; }

        [Required]
        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        //[Required]
        public string MeetingTimeStr { get; set; }

        [Required]
        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "線上會議連結")]
        public string Url { get; set; }

        [Display(Name = "注意事項")]
        public string Note { get; set; }

        [Display(Name = "會議記錄")]
        public string Record { get; set; }

        [Display(Name = "檔案一")]
        public string FileOne { get; set; }

        [Display(Name = "檔案二")]
        public string FileTwo { get; set; }

        [Display(Name = "會議通知內容")]
        public string NotifyMsg { get; set; }

        [Display(Name = "需出勤紀錄")]
        public bool IsAttendance { get; set; }

        [Display(Name = "開放簽到")]
        public bool IsSignable { get; set; }

        [Display(Name = "可遲到分鐘")]
        [Range(0, int.MaxValue, ErrorMessage = "請輸入數字")]
        public int LateMinutes { get; set; }

        [Required]
        public DateTime UpdateDate { get; set; }
        [Required]
        public int Updater { get; set; }
        [Required]
        public DateTime CreateDate { get; set; }
        [Required]
        public int Creater { get; set; }

        public AdminInfo AdminInfo
        {
            get
            {
                return MeetSignIn.Utility.Helpers.AdminInfoHelper.GetAdminInfo(); ;
            }
        }

        [Display(Name = "已選通知名單")]
        public List<EmployeeView> Notify { get; set; }

        [Display(Name = "選擇課別後勾選名單")]
        [Required(ErrorMessage = "請至少選擇一個通知名單")]
        public string selectedNotifyList { get; set; }

        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> RoomOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.GetAll();
                foreach (var v in values)
                {
                   
                    result.Add(new SelectListItem()
                    {
                        Text = v.Place + " (" + v.RoomNumber + ")",
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = t8personRepository.GetDeptQuery();
                var values = departmentRepository.Query(true).OrderBy(q => q.DeptID);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> LateMinutesOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = EnumHelper.GetValues<LateMinutes>();
                for (int i = 0; i <= 60; i+=5)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = i.ToString(),
                        Text = i.ToString() + "分鐘"
                    });
                }
                return result;
            }
        }

    }
}