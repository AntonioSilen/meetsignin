﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.Meeting
{
    public class MeetingIndexView : PageQuery
    {
        public MeetingIndexView()
        {
            this.Sorting = "StartDate";
            this.IsDescending = true;

            this.StartDate = DateTime.Now.AddMonths(-6);
            this.EndDate = DateTime.Now.AddMonths(12);
        }

        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());

        public PageResult<MeetingView> PageResult { get; set; }

        [Required]
        [Display(Name = "主題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "發起人")]
        public string Organiser { get; set; }

        [Required]
        [Display(Name = "課別")]
        public int Department { get; set; }       

        [Display(Name = "參與課別")]
        public string DepartmentListStr { get; set; }
     

        [Required]
        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        [Required]
        [Display(Name = "會議室")]
        public int RoomID { get; set; }

        [Required]
        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "需出勤紀錄")]
        public bool? IsAttendance { get; set; }

        [Display(Name = "開放簽到")]
        public bool? IsSignable { get; set; }
     
        public List<SelectListItem> FactoryOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Factory>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> RoomOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.GetAll();
                foreach (var v in values)
                {                  
                    result.Add(new SelectListItem()
                    {
                        Text = v.RoomNumber,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                //須建立自動更新T8資料(Insert新部門) ?????
                List<SelectListItem> result = new List<SelectListItem>();
                var values = departmentRepository.GetAll();
                foreach (var v in values)
                {                  
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }
      
    }
}