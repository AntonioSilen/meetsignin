﻿using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PFCategory
{
    public class PFCategoryIndexView : PageQuery
    {
        public PFCategoryIndexView()
        {
            this.Sorting = "Sort";
            this.IsDescending = false;
        }

        public PageResult<PFCategoryView> PageResult { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "使用班別")]
        public int ParentID { get; set; }

        [Display(Name = "分類層級")]
        public string LevelType { get; set; }

        [Display(Name = "職責點數")]
        public int Points { get; set; }

        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "必填")]
        public bool IsRequired { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comDepartmentRepository comdepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
                var lvList = new List<int>();
                lvList.Add(3);
                lvList.Add(4);
                var values = comdepartmentRepository.Query(lvList);

                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.DeptName,
                        Value = (v.DeptId).ToString()
                    });
                }
                
                return result;
            }
        }
        public List<SelectListItem> LevelTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comDepartmentRepository comdepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
                var lvList = new List<int>();
                var values = comdepartmentRepository.Query(lvList);

                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.DeptName,
                        Value = (v.DeptId).ToString()
                    });
                }
                
                return result;
            }
        }
    }
}