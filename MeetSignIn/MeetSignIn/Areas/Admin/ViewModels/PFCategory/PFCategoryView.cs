﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.PFCategory
{
    public class PFCategoryView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }
        [Display(Name = "泰文標題")]
        public string ThaiTitle { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "使用班別")]
        public int ParentID { get; set; }

        [Required]
        [Display(Name = "分類層級")]
        public string LevelType { get; set; }

        [Required]
        [Display(Name = "職責點數")]
        public int Points { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "必填")]
        public bool IsRequired { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public List<PFCategoryView> ClassList { get; set; }
    }
}