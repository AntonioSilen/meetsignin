﻿using MeetSignIn.Areas.Admin.ViewModels.PFSeason;
using MeetSignIn.Models;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PFCategory
{
    public class PFCategoryDeptView
    {
        public PFSeasonView SeasonInfo { get; set; }
     
        [Required]
        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        public List<PFCategoryView> ClassList { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comDepartmentRepository comdepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
                var lvList = new List<int>();
                var values = comdepartmentRepository.Query(lvList);

                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.DeptName,
                        Value = (v.DeptId).ToString()
                    });
                }

                return result;
            }
        }
        public List<SelectListItem> LevelTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comDepartmentRepository comdepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
                var lvList = new List<int>();
                var values = comdepartmentRepository.Query(lvList);

                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.DeptName,
                        Value = (v.DeptId).ToString()
                    });
                }

                return result;
            }
        }
    }
}