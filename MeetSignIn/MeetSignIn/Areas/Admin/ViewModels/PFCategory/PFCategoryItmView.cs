﻿using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PFCategory
{
    public class PFCategoryItmView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "泰文標題")]
        public string ThaiTitle { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "使用班別")]
        public int ParentID { get; set; }

        [Required]
        [Display(Name = "分類層級")]
        public string LevelType { get; set; }

        [Required]
        [Display(Name = "職責點數")]
        public int Points { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Required]
        [Display(Name = "應用類別")]
        public int Type { get; set; }

        [Required]
        [Display(Name = "必填")]
        public bool IsRequired { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public int LvlID { get; set; }
        public string LvlTitle { get; set; }
        public string ThaiLvlTitle { get; set; }
        public int LvlPoints { get; set; }

        public List<PFCategoryLvlView> LvlList { get; set; }
        public List<SelectListItem> LvlOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new Models.MeetingSignInDBEntities());
                var values = pFCategoryRepository.Query(true, "", this.ID, EnumHelper.GetDescription(Models.LevelType.LVL));
                string lang = LanguageHelper.GetLang();
                foreach (var v in values)
                {
                    if (lang == "zh-tw")
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = v.Title,
                            Value = (v.ID).ToString()
                        });
                    }
                    else
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = v.ThaiTitle,
                            Value = (v.ID).ToString()
                        });
                    }
                }

                return result;
            }
        }
    }
}