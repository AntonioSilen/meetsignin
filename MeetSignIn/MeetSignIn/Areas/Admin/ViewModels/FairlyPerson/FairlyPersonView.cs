﻿using MeetSignIn.Areas.Admin.ViewModels.KPI;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPI;
using MeetSignIn.Areas.Admin.ViewModels.PersonKPIState;
using MeetSignIn.Models;
using MeetSignIn.Models.T8.Join;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.FairlyPerson
{
    public class FairlyPersonView : GeneralAuditView
    {
        public bool FromAudit { get; set; }

        [Display(Name = "部門")]
        public string ParentDeptId { get; set; }

        [Display(Name = "主部門")]
        public string MainParentDeptId { get; set; }

        [Display(Name = "等級")]
        public int Lv { get; set; }

        [Display(Name = "課別")]
        public string DeptId { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "姓名")]
        public string PersonName { get; set; }

        [Display(Name = "主指標")]
        public string FairlyKPITitle { get; set; }

        [Display(Name = "副指標編號")]
        public int KPIID { get; set; }

        [Display(Name = "副指標")]
        public string KPITitle { get; set; }

        [Display(Name = "進度狀態")]
        public int State { get; set; }

        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }

        //[Display(Name = "啟用狀態")]
        //public bool IsOnline { get; set; }

        [Display(Name = "最新資料")]
        public bool IsLatest { get; set; }

        [Display(Name = "紀錄更新時間")]
        public DateTime CreateDate { get; set; }

        public GroupJoinPersonAndDepartment PersonInfo { get; set; }

        [Display(Name = "部門KPI列表")]
        public List<KPIView> DeptKPIList { get; set; }

        [Display(Name = "部門KPI列表 (尚未參與)")]
        public List<KPIView> JoinableDeptKPIList { get; set; }

        [Display(Name = "個人KPI列表")]
        public List<PersonKPIView> PersonKPIList { get; set; }

        [Display(Name = "季度個人目標占比")]
        public List<PersonKPIPercentageTask> PersonKPIPercentageTasks { get; set; }

        public int AuditState { get; set; }
        public string AuditStatus { get; set; }
        public bool IsInvalid { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }

        public List<SelectListItem> QuarterOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var now = DateTime.Now;
                int thisYear = now.Year;
                int endYear = now.AddYears(1).Year;
                for (int i = thisYear; i <= endYear; i++)
                {
                    for (int j = 1; j <= 4; j++)
                    {
                        string quarter = i.ToString() + "年" + (j * 2 + j - 2).ToString() +  "月~" + (j * 2 + j).ToString() + "月";
                         
                        result.Add(new SelectListItem()
                        {
                            Text =  quarter,
                            Value = quarter
                        });
                    }                   
                }

                return result;
            }
        }
    }
}