﻿using MeetSignIn.Areas.Admin.ViewModels.PersonKPI;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.FairlyPerson
{
    public class FairlyPersonIndexView : PageQuery
    {
        public FairlyPersonIndexView()
        {
            this.Sorting = "DeptId";
            this.IsDescending = false;
        }

        public PageResult<FairlyPersonView> PageResult { get; set; }

        [Display(Name = "部門")]
        public string ParentDeptId { get; set; }

        [Display(Name = "課別")]
        public string DeptId { get; set; }
        public string DisplayDeptId { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "姓名")]
        public string PersonName { get; set; }

        public List<SelectListItem> AllDeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comDepartmentRepository comdepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
                var lvList = new List<int>();
                var values = comdepartmentRepository.Query(lvList);
                if (values == null || values.Count() == 0)
                {
                    foreach (var v in GetDepts())
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = v.DeptName,
                            Value = (v.DeptId).ToString()
                        });
                    }
                }
                else
                {
                    foreach (var v in values)
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = v.DeptName,
                            Value = (v.DeptId).ToString()
                        });
                    }
                }
                return result;
            }
        }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }

        public List<DeptModel> GetDepts()
        {
            var depts = new List<DeptModel>();
            depts.Add(new DeptModel() { DeptId = "0001", DeptName = "菲力工業股份有限公司" });
            depts.Add(new DeptModel() { DeptId = "A10", DeptName = "總經理室" });
            depts.Add(new DeptModel() { DeptId = "B00", DeptName = "管理部" });
            depts.Add(new DeptModel() { DeptId = "B01", DeptName = "工安部" });
            depts.Add(new DeptModel() { DeptId = "C00", DeptName = "營業部" });
            depts.Add(new DeptModel() { DeptId = "D00", DeptName = "品技部" });
            depts.Add(new DeptModel() { DeptId = "E00", DeptName = "資材部" });
            depts.Add(new DeptModel() { DeptId = "F00", DeptName = "生產部1" });
            depts.Add(new DeptModel() { DeptId = "G00", DeptName = "生產部2" });
            depts.Add(new DeptModel() { DeptId = "H00", DeptName = "外派" });

            return depts;
        }

        public class DeptModel
        {
            public string DeptId { get; set; }
            public string DeptName { get; set; }
        }
    }
}