﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.Competency
{
    public class CompetencyIndexView : PageQuery
    {
        public CompetencyIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<CompetencyView> PageResult { get; set; }

        public int ID { get; set; }
        [Display(Name = "職能別標題")]
        public string Title { get; set; }

        [Display(Name = "關聯部門")]
        public string Depts { get; set; }

        [Display(Name = "負責人工號")]
        public string ResponsiblePersonId { get; set; }

        [Display(Name = "負責人姓名")]
        public string ResponsibleName { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }
    }
}