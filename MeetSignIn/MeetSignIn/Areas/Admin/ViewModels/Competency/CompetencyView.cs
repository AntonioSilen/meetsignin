﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Competency
{
    public class CompetencyView : BasicSettingView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "職能別標題")]
        public string Title { get; set; }

        [Display(Name = "關聯部門")]
        public string Depts { get; set; }

        [Display(Name = "負責人工號")]
        public string ResponsiblePersonId { get; set; }

        [Display(Name = "負責人姓名")]
        public string ResponsibleName { get; set; }
        public string ResponsibleNameStr { get; set; }

        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        
    }
}