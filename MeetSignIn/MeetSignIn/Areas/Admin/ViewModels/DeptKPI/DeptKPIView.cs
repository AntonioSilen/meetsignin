﻿using MeetSignIn.Areas.Admin.ViewModels.PersonKPI;
using MeetSignIn.Models;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.DeptKPI
{
    public class DeptKPIView
    {
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "副指標標題")]
        public string Title { get; set; }

        [Display(Name = "公司KPI")]
        public int ParentKPIID { get; set; }
        [Display(Name = "主指標標題")]
        public string ParentKPITitle { get; set; }

        [Display(Name = "涵蓋部門")]
        public string Dept { get; set; }

        [Display(Name = "描述")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Required]
        [Display(Name = "審核狀態")]
        public int ApprovalState { get; set; }

        [Required]
        [Display(Name = "預計檢核時間")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CheckDate { get; set; }
        public string CheckDateStr { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }

        [Display(Name = "參與人員清單")]
        public List<PersonKPIView> PersonKPIList { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }
    }
}