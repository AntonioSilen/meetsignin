﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.QuizResult
{
    public class QuizResultIndexView : PageQuery
    {
        public QuizResultIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<QuizResultView> PageResult { get; set; }

        public int ID { get; set; }

        [Required]
        [Display(Name = "試卷編號")]
        public int QuizID { get; set; }

        [Required]
        [Display(Name = "問題編號")]
        public int QuizQuestionID { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "問題類型")]
        public int AnswerType { get; set; }

        [Display(Name = "回答")]
        public int Answer { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }
    }
}