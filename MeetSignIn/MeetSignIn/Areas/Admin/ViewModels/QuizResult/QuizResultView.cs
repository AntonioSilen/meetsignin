﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.QuizResult
{
    public class QuizResultView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "試卷編號")]
        public int QuizID { get; set; }

        [Required]
        [Display(Name = "問題編號")]
        public int QuizQuestionID { get; set; }
        public string Question { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string PersonId { get; set; }
        public string PersonName { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "問題類型")]
        public int AnswerType { get; set; }

        [Display(Name = "回答")]
        public string Answer { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        public bool IsCorrect { get; set; }
    }
}