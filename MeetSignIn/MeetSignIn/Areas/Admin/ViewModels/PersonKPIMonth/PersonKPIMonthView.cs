﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.PersonKPIMonth
{
    public class PersonKPIMonthView
    {
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "個人目標編號")]
        public int PersonKPIID { get; set; }

        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Display(Name = "月份")]
        public int Month { get; set; }

        [Display(Name = "應做事情")]
        public string ToDo { get; set; }

        [Display(Name = "預期目標")]
        public string Expect { get; set; }

        [Display(Name = "完成事項")]
        public string Complete { get; set; }

        [Display(Name = "目標單位")]
        public string Percentage { get; set; }

        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "完成狀態")]
        public bool IsFinished { get; set; }

        [Display(Name = "完成時間")]
        public DateTime FinishDate { get; set; }

        [Display(Name = "期限")]
        public DateTime Deadline { get; set; }
        public string DeadlineStr {
            get { 
                return this.Deadline.ToString("yyyy-MM-dd");
            }
        }

        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }
    }
}