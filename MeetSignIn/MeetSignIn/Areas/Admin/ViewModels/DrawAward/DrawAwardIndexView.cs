﻿using MeetSignIn.Models.Others;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.DrawAward
{
    public class DrawAwardIndexView : PageQuery
    {
        public DrawAwardIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<DrawAwardView> PageResult { get; set; }

        public int ID { get; set; }

        [Display(Name = "抽籤活動編號")]
        public int DrawID { get; set; }

        [Display(Name = "獎項標題")]
        public string Title { get; set; }

        [Display(Name = "占比")]
        public int Rate { get; set; }

        [Display(Name = "數量")]
        public int Number { get; set; }

        [Display(Name = "可重複")]
        public bool? IsRepeatable { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }
    }
}