﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.DrawAwardRelation;
using MeetSignIn.Areas.Admin.ViewModels.DrawPerson;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.DrawAward
{
    public class DrawAwardView
    {
        //public DrawAwardView()
        //{
        //    DrawAwardRelationRepository drawAwardRelationRepository = new DrawAwardRelationRepository(new MeetingSignInDBEntities());
        //    DrawPersonRepository drawPersonRepository = new DrawPersonRepository(new MeetingSignInDBEntities());
        //    var awardRelationList = drawAwardRelationRepository.Query(this.DrawID, this.ID).Select(q => q.PersonId).ToList();
        //    this.WinList = Mapper.Map<List<DrawPersonView>>(drawPersonRepository.Query(this.DrawID).Where(q => awardRelationList.Contains(q.PersonId)));
        //    this.IsDraw = this.WinList.Count() > 0;
        //}

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "抽籤活動編號")]
        public int DrawID { get; set; }

        [Required]
        [Display(Name = "獎項標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "占比")]
        public int Rate { get; set; }

        [Required]
        [Display(Name = "數量")]
        public int Number { get; set; }

        [Required]
        [Display(Name = "可重複")]
        public bool IsRepeatable { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public bool IsDraw { get; set; }

        public List<DrawAwardRelationView> WinList { get; set; }
    }
}