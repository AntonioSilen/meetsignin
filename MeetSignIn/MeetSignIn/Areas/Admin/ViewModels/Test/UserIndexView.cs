﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Test
{
    public class UserIndexView
    {
        public UserView User { get; set; }
        public List<UserTask> UserTasks { get; set; }
    }
}