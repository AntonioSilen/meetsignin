﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.Test
{
    // 使用者
    public class UserView
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
    }

    // 使用者待做事項
    public class UserTask
    {
        public string UserTaskId { get; set; }
        public string TaskName { get; set; }
        public DateTime CompletedDate { get; set; }

        // FK
        public string UserId { get; set; }

        // Navigation
        public virtual UserView User { get; set; }
    }
}