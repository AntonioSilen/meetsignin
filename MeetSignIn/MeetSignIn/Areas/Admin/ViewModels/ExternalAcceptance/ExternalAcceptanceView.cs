﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.ExternalAcceptance
{
    public class ExternalAcceptanceView : AuditView
    {
        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }

        [Required]
        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "團體驗收")]
        public bool IsGroup { get; set; }

        [Display(Name = "心得")]
        public string Report { get; set; }
        public string ReportFile { get; set; }
        public HttpPostedFileBase ReportPost { get; set; }

        [Display(Name = "證書")]
        public string Certificate { get; set; }
        public string CertificateFile { get; set; }
        public HttpPostedFileBase CertificatePost { get; set; }

        [Display(Name = "轉授日期")]
        public DateTime DelegateDate { get; set; }

        [Display(Name = "轉授說明")]
        public string DelegateDesc { get; set; }

        [Display(Name = "行動計畫")]
        public string ActionPlan { get; set; }
        public string ActionPlanFile { get; set; }
        public HttpPostedFileBase ActionPlanPost { get; set; }

        [Display(Name = "績效")]
        public string Performance { get; set; }

        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }
        #region 驗收方式
        public bool HasOpinion { get; set; }
        public bool HasPerformance { get; set; }
        public bool HasActionPlan { get; set; }

        public bool HasReport { get; set; }
        public bool HasCertificate { get; set; }
        public bool HasDelegate { get; set; }
        #endregion

        public string Address { get; set; }
        public string Lecturer { get; set; }
    }
}