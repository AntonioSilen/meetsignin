﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.QuestionDB
{
    public class QuestionDBIndexView : PageQuery
    {
        public QuestionDBIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<QuestionDBView> PageResult { get; set; }

        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "問題")]
        public string Question { get; set; }

        [Required]
        [Display(Name = "問題類型")]
        public int AnswerType { get; set; }

        [Required]
        [Display(Name = "選擇清單")]
        public int OptionGroup { get; set; }

        [Required]
        [Display(Name = "必填")]
        public bool IsRequired { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "分數")]
        public int Points { get; set; }

        [Display(Name = "選項")]
        public string Options { get; set; }

        [Display(Name = "使用過的題目編號")]
        public string UsedQNRIDs { get; set; }

        public List<SelectListItem> AnswerTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                result.Add(new SelectListItem()
                {
                    Text = "文字",
                    Value = "1"
                });
                result.Add(new SelectListItem()
                {
                    Text = "多行文字",
                    Value = "2"
                });
                result.Add(new SelectListItem()
                {
                    Text = "日期",
                    Value = "3"
                });
                result.Add(new SelectListItem()
                {
                    Text = "單選",
                    Value = "4"
                });
                result.Add(new SelectListItem()
                {
                    Text = "複選",
                    Value = "5"
                });
                return result;
            }
        }
    }
}