﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Models.Others;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.ClassReport
{
    public class ClassReportIndexView : PageQuery
    {
        public ClassReportIndexView()
        {
            this.Sorting = "CreateDate";
            this.IsDescending = true;
        }

        public PageResult<ClassReportView> PageResult { get; set; }

        [Display(Name = "ID")]
        public int ID { get; set; }

        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                return Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
            }
        }

        [Required]
        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "學習心得")]
        public string Report { get; set; }

        [Display(Name = "如何應用到時機工作上")]
        public string Usage { get; set; }

        [Display(Name = "對未來課程之需求與規畫建議")]
        public string RequireAdvice { get; set; }

        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "初審")]
        public bool? FirstAudit { get; set; }

        [Display(Name = "初審備註")]
        public string FirstRemark { get; set; }

        [Display(Name = "複審")]
        public bool? SecondAudit { get; set; }

        [Display(Name = "複審備註")]
        public string SecondRemark { get; set; }

        [Display(Name = "承辦單位承辦人")]
        public bool? OfficerAudit { get; set; }

        [Display(Name = "承辦人備註")]
        public string OfficerRemark { get; set; }

        [Display(Name = "承辦人簽核日期")]
        public DateTime OfficerAuditDate { get; set; }

        [Display(Name = "總經理")]
        public bool? PresidentAudit { get; set; }

        [Display(Name = "總經理備註")]
        public string PresidentRemark { get; set; }

        [Display(Name = "總經理簽核日期")]
        public DateTime PresidentAuditDate { get; set; }
    }
}