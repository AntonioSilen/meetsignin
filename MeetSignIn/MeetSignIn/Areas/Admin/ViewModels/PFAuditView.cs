﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels
{
    public class PFAuditView
    {
        [Display(Name = "評比作業編號")]
        public int AppraiseWorksID { get; set; }
        [Display(Name = "填寫完成")]
        public bool IsFinished { get; set; }
        [Display(Name = "送審")]
        public bool IsAudit { get; set; }

        public string ManagerDeptId { get; set; }
        public string ManagerDeptName { get; set; }
        public string ManagerName { get; set; }

        [Display(Name = "主管")]
        public int ManagerAudit { get; set; }

        [Display(Name = "主管簽章")]
        public string ManagerSignature { get; set; }
        public HttpPostedFileBase ManagerPost { get; set; }

        [Display(Name = "主管備註")]
        public string ManagerRemark { get; set; }

        [Display(Name = "主管簽核日期")]
        public DateTime ManagerAuditDate { get; set; }

        public string CSDeptId { get; set; }
        public string CSDeptName { get; set; }
        public string CSName { get; set; }

        [Display(Name = "課級主管")]
        public int CSAudit { get; set; }

        [Display(Name = "課級主管簽章")]
        public string CSSignature { get; set; }
        public HttpPostedFileBase CSPost { get; set; }

        [Display(Name = "課級主管備註")]
        public string CSRemark { get; set; }

        [Display(Name = "課級主管核日期")]
        public DateTime CSAuditDate { get; set; }

        public string TLDeptId { get; set; }
        public string TLDeptName { get; set; }
        public string TLName { get; set; }

        [Display(Name = "班組長")]
        public int TLAudit { get; set; }

        [Display(Name = "班組長簽章")]
        public string TLSignature { get; set; }
        public HttpPostedFileBase TLPost { get; set; }

        [Display(Name = "班組長備註")]
        public string TLRemark { get; set; }

        [Display(Name = "班組長簽核日期")]
        public DateTime TLAuditDate { get; set; }

        public Models.Audit AuditInfo { get; set; }

        public Models.AuditLevel AuditLevelInfo { get; set; }
    }
}