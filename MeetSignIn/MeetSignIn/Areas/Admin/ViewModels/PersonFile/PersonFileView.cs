﻿using System.ComponentModel.DataAnnotations;

namespace MeetSignIn.Areas.Admin.ViewModels.PersonFile
{
    public class PersonFileView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "人員編號")]
        public int PersonID { get; set; }

        [Required]
        [Display(Name = "檔案名稱")]
        public string FileName { get; set; }

        [Required]
        [Display(Name = "訓練種類/職能")]
        public string MainFile { get; set; }
    }
}