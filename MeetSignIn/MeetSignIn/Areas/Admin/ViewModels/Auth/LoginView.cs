﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Auth
{
    public class LoginView
    {
        [Required]
        [Display(Name = "Account / Employee Number")]
        public string Account { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "記住我")]
        public bool RememberMe { get; set; }

        [Display(Name = "恢復網址")]
        public string ReturnUrl { get; set; }

        [Display(Name = "Language")]
        public string Language { get; set; }

        public List<SelectListItem> LanguageOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Language>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = v.ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }
                return result;
            }
        }
    }
}