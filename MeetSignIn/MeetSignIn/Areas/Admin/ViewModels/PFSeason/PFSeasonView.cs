﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PFSeason
{
    public class PFSeasonView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }
        [Display(Name = "泰文標題")]
        public string ThaiTitle { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "班別")]
        public int ClassID { get; set; }
        public string ClassName
        {
            get
            {
                PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
                var data = pFCategoryRepository.GetById(ClassID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Required]
        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }
    }
}