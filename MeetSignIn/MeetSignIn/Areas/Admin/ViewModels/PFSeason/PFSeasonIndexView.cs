﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.Repositories.T8Repositories;

namespace MeetSignIn.Areas.Admin.ViewModels.PFSeason
{
    public class PFSeasonIndexView : PageQuery
    {
        public PFSeasonIndexView()
        {
            this.Sorting = "StartDate";
            this.IsDescending = true;
        }

        public PageResult<PFSeasonView> PageResult { get; set; }

        [Required]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "班別")]
        public int ClassID { get; set; }

        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "使用狀態")]
        public bool? IsOnline { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comPersonRepository compersonRepository = new comPersonRepository(new T8ERPEntities());
                var values = compersonRepository.GetPersonQuery().GroupBy(x => new { x.DeptId, x.DeptName }).ToList();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Key.DeptName,
                        Value = (v.Key.DeptId).ToString()
                    });
                }

                return result;
            }
        }
    }
}