﻿using MeetSignIn.Models;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Manager
{
    public class ManagerView
    {
        public ManagerView()
        {
            this.Type = (int)AdminType.Employee;
        }
        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "帳號")]
        public string Account { get; set; }

        [Required]
        [Display(Name = "密碼")]
        public string Password { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "課別編號")]
        public string DeptId { get; set; }

        [Display(Name = "課別")]
        public string Department { get; set; }

        DepartmentRepository departmentRepository = new DepartmentRepository(new T8ERPEntities());

        [Required]
        [Display(Name = "類型")]
        public int Type { get; set; }

        [Display(Name = "職能文件")]
        public string FunctionDocs { get; set; }
        public List<FunctionDoc> DocList { get; set; }
        public List<HttpPostedFileBase> DocFiles { get; set; }
        public HttpPostedFileBase FunctionDoc { get; set; }

        [Required]
        [Display(Name = "菲力校長")]
        public bool IsPrincipal { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Required]
        [Display(Name = "更新時間")]
        public DateTime UpdateDate { get; set; }

        [Required]
        [Display(Name = "更新者")]
        public int Updater { get; set; }

        [Required]
        [Display(Name = "建立時間")]
        public DateTime CreateDate { get; set; }

        [Required]
        [Display(Name = "建立者")]
        public int Creater { get; set; }


        public List<SelectListItem> AdminTypeOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<AdminType>();
                foreach (var v in values)
                {
                    //if (v != AdminType.Principal)
                    //{
                    //    result.Add(new SelectListItem()
                    //    {
                    //        Text = EnumHelper.GetDescription(v),
                    //        Value = ((int)v).ToString()
                    //    });
                    //}
                    if (v != AdminType.Sysadmin)
                    {
                        result.Add(new SelectListItem()
                        {
                            Text = EnumHelper.GetDescription(v),
                            Value = ((int)v).ToString()
                        });
                    }
                 
                }
                return result.OrderByDescending(q => q.Value).ToList();
            }
        }

        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = departmentRepository.Query().OrderBy(q => q.DeptId);
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.DeptName,
                        Value = v.DeptId
                    });
                }
                return result;
            }
        }
    }
}