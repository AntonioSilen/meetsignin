﻿using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.PFWorks
{
    public class PFWorksView
    {
        [Required]
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "標題")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Required]
        [Display(Name = "班別")]
        public int ClassID { get; set; }
        public string ClassName
        {
            get
            {
                PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
                var data = pFCategoryRepository.GetById(ClassID);
                return data == null ? "" : data.Title;
            }
        }

        [Required]
        [Display(Name = "評比項目")]
        public int ItemID { get; set; }

        [Required]
        [Display(Name = "選擇層級")]
        public int LvlID { get; set; }

        [Required]
        [Display(Name = "職責點數")]
        public int Points { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public List<PFCategoryLvlView> LvlList { get; set;}

    }
}