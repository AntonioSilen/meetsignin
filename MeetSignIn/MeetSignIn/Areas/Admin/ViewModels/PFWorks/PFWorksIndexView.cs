﻿using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.Areas.Admin.ViewModels.PFWorks
{
    public class PFWorksIndexView : PageQuery
    {
        public PFWorksIndexView()
        {
            this.Sorting = "Title";
            this.IsDescending = false;
        }

        public PageResult<PFWorksView> PageResult { get; set; }
        public List<PFWorksTitleModel> TitleList { get; set; }
        public List<Models.PFWork> WorkList { get; set; }

        [Display(Name = "作業項目編號")]
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "班別")]
        public int ClassID { get; set; }
        public string ClassName
        {
            get
            {
                PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
                var data = pFCategoryRepository.GetById(ClassID);
                return data == null ? "" : data.Title;
            }
        }

        [Display(Name = "評比項目")]
        public int ItemID { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }

        [Display(Name = "職責點數(加總)")]
        public int Points { get; set; }

        public List<PFCategoryCateView> ClassCatesList { get; set; }
    }
}