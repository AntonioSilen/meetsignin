﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.ClassActionPlan
{
    public class ClassActionPlanView : AuditView
    {
        public bool FromAudit { get; set; }

        [Required]
        [Display(Name = "ID")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TrainingID { get; set; }

        [Display(Name = "教育訓練")]
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = Mapper.Map<TrainingView>(trainingRepository.GetById(this.TrainingID));
                info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                return info;
            }
        }

        [Required]
        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "行動作法")]
        public string Method { get; set; }

        [Required]
        [Display(Name = "原因")]
        public string Reason { get; set; }

        [Required]
        [Display(Name = "期限(日程計畫)")]
        public string Expire { get; set; }

        [Required]
        [Display(Name = "採用方法")]
        public string Ways { get; set; }

        [Display(Name = "填寫時間")]
        public DateTime CreateDate { get; set; }
    }
}