﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Vision
{
    public class VisionView : BasicSettingView
    {
        public VisionView()
        {
            this.Year = Convert.ToInt32(DateTime.Now.Year);
        }

        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "年度")]
        public int Year { get; set; }

        [Required]
        [Display(Name = "簡述")]
        public string Brief { get; set; }

        [Required]
        [Display(Name = "說明")]
        public string Description { get; set; }

        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        public List<SelectListItem> YearOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var thisYear = DateTime.Now.Year;
                for (int i = 2010; i <= thisYear + 1; i++)
                {
                    string iYear = i.ToString();
                    result.Add(new SelectListItem()
                    {
                        Value = iYear,
                        Text = iYear + "年"
                    });
                }            
                return result;
            }
        }

    }
}