﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.Vision
{
    public class VisionIndexView : PageQuery
    {
        public VisionIndexView()
        {
            this.Sorting = "Year";
            this.IsDescending = true;
        }

        public PageResult<VisionView> PageResult { get; set; }

        [Required]
        public int ID { get; set; }

        [Display(Name = "年度")]
        public int Year { get; set; }

        [Display(Name = "簡述")]
        public string Brief { get; set; }

        [Display(Name = "說明")]
        public string Description { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }

        public List<SelectListItem> YearOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var thisYear = Convert.ToInt32(DateTime.Now.Year);
                for (int i = 2018; i <= thisYear + 1; i++)
                {
                    string iYear = i.ToString();
                    result.Add(new SelectListItem()
                    {
                        Value = iYear,
                        Text = iYear + "年"
                    });
                }
                return result;
            }
        }
    }
}