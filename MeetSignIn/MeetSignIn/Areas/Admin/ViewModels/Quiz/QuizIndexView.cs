﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;

namespace MeetSignIn.Areas.Admin.ViewModels.Quiz
{
    public class QuizIndexView : PageQuery
    {
        public QuizIndexView()
        {
            this.Sorting = "ID";
            this.IsDescending = false;
        }

        public PageResult<QuizView> PageResult { get; set; }

        public int ID { get; set; }
        [Required]
        [Display(Name = "試卷標題")]
        public string Title { get; set; }

        [Display(Name = "說明")]
        public string Description { get; set; }

        [Display(Name = "填寫部門")]
        public string Depts { get; set; }

        [Display(Name = "年度")]
        public int Year { get; set; }

        [Display(Name = "月份")]
        public int Months { get; set; }

        [Display(Name = "啟用狀態")]
        public bool? IsOnline { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立人工號")]
        public string CreatePersonId { get; set; }
    }
}