﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.QuizResult;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Quiz
{
    public class QuizView : BasicSettingView
    {
        public QuizView()
        {
            this.Year = DateTime.Now.Year;
        }

        [Display(Name = "填寫狀態")]
        public bool IsFilled { get; set; }

        [Required]
        public int ID { get; set; }

        public int TrainingID { get; set; }
        public TrainingView TrainingInfo
        {
            get
            {
                TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
                Repositories.T8Repositories.DepartmentRepository t8departmentRepository = new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
                var info = new TrainingView();
                var query = trainingRepository.GetById(this.TrainingID);
                if (query != null)
                {
                    info = Mapper.Map<TrainingView>(query);
                    info.DeptName = t8departmentRepository.FindByDeptId(info.DeptID).DeptName;
                }
                return info;
            }
        }

        [Required]
        [Display(Name = "題目")]
        public string Title { get; set; }

        [Display(Name = "說明")]
        public string Description { get; set; }

        [Display(Name = "填寫部門")]
        public string Depts { get; set; }

        [Display(Name = "年度")]
        public int Year { get; set; }

        [Display(Name = "月份")]
        public int Month { get; set; }

        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "建立人工號")]
        public string CreatePersonId { get; set; }

        public List<QuizQuestionView> QuestionList { get; set; }
        public List<QuizPersonResultView> PersonResultList { get; set; }
    }

    public class QuizQuestionView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Required]
        [Display(Name = "試卷編號")]
        public int QuizID { get; set; }

        [Display(Name = "觸發類型")]
        public int TriggerType { get; set; }

        [Display(Name = "觸發答案")]
        public string TriggerAnswer { get; set; }

        [Display(Name = "連結問題")]
        public int LinkQuizQID { get; set; }

        [Required]
        [Display(Name = "問題")]
        public string Question { get; set; }

        [Required]
        [Display(Name = "問題類型")]
        public int AnswerType { get; set; }

        [Required]
        [Display(Name = "選擇清單")]
        public int OptionGroup { get; set; }
        public List<OptionItem> Options { get; set; }

        [Required]
        [Display(Name = "必填")]
        public bool IsRequired { get; set; }

        [Required]
        [Display(Name = "排序")]
        public int Sort { get; set; }

        [Display(Name = "排序")]
        public int Points { get; set; }
    }

    public class QuizPersonResultView
    {
        //public int ID { get; set; }

        [Display(Name = "試卷編號")]
        public int QuizID { get; set; }

        [Display(Name = "人員")]
        public string PersonId { get; set; }
        public string PersonName { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "填寫日期")]
        public DateTime CreateDate { get; set; }

        public List<QuizResultView> ResultList { get; set; }

    }
}