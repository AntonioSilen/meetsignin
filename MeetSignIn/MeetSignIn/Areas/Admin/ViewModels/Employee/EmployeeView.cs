﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Admin.ViewModels.Employee
{
    public class EmployeeView
    {
        [Required]
        public int ID { get; set; }

        [Required]
        [Display(Name = "課別")]
        public int DepartmentID { get; set; }
        public string DeptID { get; set; }

        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        public string DepartmentStr
        {
            get
            {
                return departmentRepository.FindByDeptID(this.DeptID).Title;
            }
        }

        public List<SelectListItem> DepartmentOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = departmentRepository.GetAll();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.Title,
                        Value = v.ID.ToString()
                    });
                }
                return result;
            }
        }

        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "英文名")]
        public string EngName { get; set; }
        

        [Required]
        [Display(Name = "信箱")]
        public string Email { get; set; }
        

        [Display(Name = "分機")]
        public string Ext { get; set; }
        

        [Required]
        [Display(Name = "照片")]
        public string MainPic { get; set; }

        [Required]
        [Display(Name = "啟用狀態")]
        public bool Status { get; set; }

        [Display(Name = "入職日期")]
        public int InductionDate { get; set; }

        [Display(Name = "年資(年)")]
        public int WorkAge { get; set; }

        [Display(Name = "年資(月)")]
        public int WorkMonths { get; set; }

        [Display(Name = "生日")]
        public int Birthday { get; set; }

        [Display(Name = "年齡")]
        public int Age { get; set; }

        [Display(Name = "MSN")]
        public string MSNNo { get; set; }

    }
}