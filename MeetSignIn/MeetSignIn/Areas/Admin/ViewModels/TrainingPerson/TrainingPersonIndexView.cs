﻿using MeetSignIn.Models.Others;
using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.Areas.Admin.ViewModels.Training;

namespace MeetSignIn.Areas.Admin.ViewModels.TrainingPerson
{
    public class TrainingPersonIndexView : PageQuery
    {
        //public TrainingPersonIndexView()
        //{
        //    this.Sorting = "OpinionStatus";
        //    this.IsDescending = false;
        //    this.PageSize = 100;
        //}

        public PageResult<TrainingPersonView> PageResult { get; set; }
        public List<TrainingPersonView> TrainingPersonList { get; set; }

        public string SignCode { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TraningID { get; set; }

        public string TraningTitle { get; set; }
        public TrainingView TrainingInfo { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "是否簽到")]
        public bool? IsSign { get; set; }

        [Display(Name = "簽到狀態")]
        public int SignStatus { get; set; }

        [Display(Name = "通知狀態")]
        public bool? IsSent { get; set; }

        [Display(Name = "是否合格")]
        public bool? IsPassed { get; set; }

        [Display(Name = "簽到時間")]
        public DateTime SignTime { get; set; }

        //訓練判斷資訊
        public bool HasLearning { get; set; }
        public int Category { get; set; }
        public int Creater { get; set; }
        public int GroupType { get; set; }

        [Display(Name = "課後驗收")]
        public string OpinionStatus { get; set; }
    }
}