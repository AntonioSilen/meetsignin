﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Health.ViewModels;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Health.Controllers
{
    [ErrorHandleActionFilter]
    public class TemperatureController : BaseController
    {
        private TemperatureRepository temperatureRepository;
        private Repositories.T8Repositories.PersonRepository t8personRepository;
        private AdminRepository adminRepository;

        public TemperatureController() : this(null, null, null) { }

        public TemperatureController(TemperatureRepository repo, Repositories.T8Repositories.PersonRepository repo2, AdminRepository repo3)
        {
            temperatureRepository = repo ?? new TemperatureRepository(new MeetingSignInDBEntities());
            t8personRepository = repo2 ?? new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
            adminRepository = repo3 ?? new AdminRepository(new MeetingSignInDBEntities());
        }

        public ActionResult Login()
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo != null)
            {
                var controller = DependencyResolver.Current.GetService<TemperatureController>();
                controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
                var loginInfo = adminRepository.GetById(memberInfo.ID);
                if (loginInfo != null)
                {
                    return controller.Login(new LoginView() { Account = loginInfo.Account, Password = loginInfo.Password });
                }
            }

            var login = new LoginView();
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                var admin = adminRepository.Login(login.Account, login.Password);
                if (admin != null)
                {
                    MemberInfo memberInfo = new MemberInfo();
                    memberInfo.ID = admin.ID;
                    memberInfo.Account = admin.Account;
                    //memberInfo.Name = admin.Name;
                    MemberInfoHelper.Login(memberInfo, true);

                    return RedirectToAction("Index", "Home");
                }
            }
            return View(login);
        }

        public ActionResult Logout()
        {
            MemberInfoHelper.Logout();
            return RedirectToAction("Login");
        }

        // GET: Health/Temperature
        public ActionResult Index(string jid = "", string bid = "")
        {
            TemperatureView model = new TemperatureView();
            model.BodyTemperature = 35;
            model.PersonID = jid;           
            if (!string.IsNullOrEmpty(jid))
            {
                //驗證人員
                var personInfo = t8personRepository.FindByPersonId(jid);
                if (personInfo != null)
                {
                    DateTime today = DateTime.Now.Date;

                    //取得當日紀錄
                    var temperInfo = temperatureRepository.Query(jid, null, null).Where(q => q.CreateDate == today).FirstOrDefault();
                    if (temperInfo != null)
                    {
                        model = Mapper.Map<TemperatureView>(temperInfo);
                        if (bid != "")
                        {
                            model.Birthday = bid;
                        }
                    }
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(TemperatureView model)
        {
            string resultStr = "";
            if (ModelState.IsValid)
            {
                var limithoursArr = GetLimitHours().Split(',');
                int startHour = Convert.ToInt32(limithoursArr[0]);
                int endHour = Convert.ToInt32(limithoursArr[1]);

                var todayStr = DateTime.Now.ToString("yyyy/MM/dd");
                var now = DateTime.Now;     
                DateTime start = Convert.ToDateTime($"{todayStr} {startHour}:00");
                DateTime end = start;
                
                if (endHour == 24)
                {
                    end = Convert.ToDateTime($"{todayStr} 23:59:59");
                }
                else
                {
                    end = Convert.ToDateTime($"{todayStr} {endHour}:00");
                }

                //驗證時間
                if (now <start)
                {
                    resultStr = "今日尚未開放填寫";
                    ShowMessage(false, resultStr);
                }
                else if (now > end)
                {
                    resultStr = "已超過今日填寫時間";
                    ShowMessage(false, resultStr);
                }
                else
                {
                    model.PersonID = model.PersonID.PadLeft(4, '0');
                    DateTime today = DateTime.Now.Date;
                    var personInfo = t8personRepository.FindByPersonId(model.PersonID);
                    if (personInfo != null)//驗證人員
                    {
                        if (model.Birthday == personInfo.Birthday.ToString())
                        {
                            //建立紀錄
                            resultStr = CreateTemperLog(model, todayStr, now, today);
                        }
                        else
                        {
                            resultStr = "生日輸入錯誤";
                            ShowMessage(false, resultStr);
                        }
                    }
                    else
                    {
                        var personAdminInfo = adminRepository.Query(null, 0, model.PersonID).FirstOrDefault();
                        if (personAdminInfo != null)//驗證人員
                        {
                            if (model.Birthday == personAdminInfo.Password.ToString())
                            {
                                //建立紀錄
                                resultStr = CreateTemperLog(model, todayStr, now, today);
                            }
                            else
                            {
                                resultStr = "生日輸入錯誤";
                                ShowMessage(false, resultStr);
                            }
                        }
                        else
                        {
                            resultStr = "工號輸入錯誤";
                            ShowMessage(false, resultStr);
                        }
                    }
                }
            }

            ViewBag.Result = resultStr;
            return View(model);
        }

        /// <summary>
        /// 建立紀錄
        /// </summary>
        /// <param name="model"></param>
        /// <param name="todayStr"></param>
        /// <param name="now"></param>
        /// <param name="today"></param>
        /// <returns></returns>
        private string CreateTemperLog(TemperatureView model, string todayStr, DateTime now, DateTime today)
        {
            string resultStr;
            //取得當日紀錄
            var temperInfo = temperatureRepository.Query(model.PersonID, null, null).Where(q => q.CreateDate == today).FirstOrDefault();

            var data = Mapper.Map<Temperature>(model);
            data.CreateDate = now;
            if (temperInfo != null && todayStr != temperInfo.CreateDate.ToString("yyyy/MM/dd"))
            {
                data.ID = 0;
            }
            if (model.ID == 0 && temperInfo == null)
            {
                temperatureRepository.Insert(data);
            }
            else if (temperInfo != null)
            {
                data.ID = temperInfo.ID;
                temperatureRepository.Update(data);
            }
            else
            {
                temperatureRepository.Update(data);
            }
            resultStr = "提交成功";
            ShowMessage(true, resultStr);
            return resultStr;
        }

        public string GetLimitHours()
        {
            string result = "";
            foreach (string line in System.IO.File.ReadLines(Server.MapPath("/Setting_Data/limithours.txt")))
            {
                result += line + ",";
            }

            return result;
        }
    }
}