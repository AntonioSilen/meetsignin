﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Areas.Health.ViewModels
{
    public class TemperatureView
    {
        public int ID { get; set; }

        [Display(Name = "工號")]
        public string PersonID { get; set; }

        [Display(Name = "生日")]
        public string Birthday { get; set; }

        [Display(Name = "體溫")]
        public decimal BodyTemperature { get; set; }

        [Display(Name = "異常狀況")]
        public int AbnormalDescription { get; set; }

        [Display(Name = "症狀情況")]
        public string AbnormalDescriptionStr { get; set; }

        [Display(Name = "身體異常")]
        public bool IsAbnormal { get; set; }

        [Display(Name = "接觸史")]
        public string ContactHistory { get; set; }

        [Display(Name = "建立時間")]
        public string CreateDate { get; set; }
                
        public List<SelectListItem> TemperatureOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                for (double i = 25; i <= 45; i=i+0.5)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = i.ToString() + " °C",
                        Value = i.ToString()
                    });
                }
          
                return result;
            }
        }
                
        public List<SelectListItem> AbnormalOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = EnumHelper.GetValues<Abnormal>();
                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Value = ((int)v).ToString(),
                        Text = EnumHelper.GetDescription(v)
                    });
                }

                return result;
            }
        }
    }
}