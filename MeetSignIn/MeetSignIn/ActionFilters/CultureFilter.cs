﻿using MeetSignIn.Models;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.ActionFilters
{
    public class CultureFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string culture = "";
            string lang = "en-US";
            if (filterContext.HttpContext.Request.QueryString["lang"] != null)
            {//網址參數 /Admin/HomeAdmin?lang={en-US}
                lang = filterContext.HttpContext.Request.QueryString["lang"];
            }
            else if (filterContext.RouteData.Values["lang"] != null)
            {//路徑參數 /Admin/{en-US}/HomeAdmin/.....
                lang = filterContext.RouteData.Values["lang"].ToString();
            }
            //lang = filterContext.RouteData.Values["lang"] != null ? filterContext.RouteData.Values["lang"].ToString() : "en-US";
            //bool isWeb = true;

            //判斷前/後台網址
            //foreach (var item in filterContext.RouteData.Values)
            //{
            //    var value = item.Value.ToString();
            //    if (value.Contains("Admin") || value.Contains("Manager"))
            //    {
            //        isWeb = false;
            //    }
            //}

            //後台語系
            Language langCulture = LanguageHelper.GetLanguage(lang);
            culture = langCulture.ToString().Replace("_", "-");
            

            //設定多語系
            CultureInfo ci = new CultureInfo(culture);
            ci.DateTimeFormat.ShortDatePattern = "yyyy/MM/dd";
            ci.DateTimeFormat.LongTimePattern = "HH:mm:ss";
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(ci.Name);
            base.OnActionExecuting(filterContext);
        }
    }
}