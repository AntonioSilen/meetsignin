﻿using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.T8.Join
{
    public class TrainPersonJoinPersonAndDepartment
    {
        //TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        public int ID { get; set; }
        public int TrainingID { get; set; }
        //public string ParentDeptID { get; set; }
        public string DeptID { get; set; }
        public string DeptName { get; set; }
        //PersonId
        public string PersonId { get; set; }
        //public bool IsSign { get; set; }
        public int SignStatus { get; set; }
        public DateTime SignDate { get; set; }
        public bool IsPassed { get; set; }
        //public bool IsPassed 
        //{ 
        //    get
        //    {
        //        return trainingPersonRepository.FindByJobID(this.PersonId).IsPassed;
        //    }
        //}
        //PersonName
        public string Name { get; set; }
        public string EngName { get; set; }
        //EMail
        public string Email { get; set; }
        //public int InductionDate { get; set; }
        //public int Birthday { get; set; }
    }
}