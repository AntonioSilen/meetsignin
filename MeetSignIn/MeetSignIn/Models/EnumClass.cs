﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models
{
    /// <summary>
    /// 語系
    /// </summary>
    public enum Language
    {
        /// <summary>
        /// 繁體中文
        /// </summary>
        [Description("繁體中文")]
        zh_TW = 1,

        /// <summary>
        /// 印尼文
        /// </summary>
        [Description("印尼文")]
        id_ID = 2,

        /// <summary>
        /// 泰文
        /// </summary>
        [Description("泰文")]
        th_TH = 3,

        /// <summary>
        /// 英文
        /// </summary>
        [Description("英文")]
        en_US = 4,
    }

    /// <summary>
    /// 1.系統管理員  2.使用者
    /// </summary>
    public enum AdminType //管理員類型
    {
        /// <summary>
        /// 系統管理員
        /// </summary>
        [Description("系統管理員")]
        Sysadmin = 1,

        /// <summary>
        /// 總經理
        /// </summary>
        [Description("總經理")]
        President = 2,

        /// <summary>
        /// 副總經理
        /// </summary>
        [Description("副總經理")]
        Vice = 3,

        /// <summary>
        /// 校長
        /// </summary>
        [Description("校長")]
        Principal = 4,

        /// <summary>
        /// 主管
        /// </summary>
        [Description("主管")]
        Manager = 5,

        /// <summary>
        /// 人資
        /// </summary>
        [Description("人資")]
        HR = 6,

        /// <summary>
        /// 課級主管
        /// </summary>
        [Description("課級主管")]
        ClassSupervisor = 7,

        /// <summary>
        /// 班組長
        /// </summary>
        [Description("班組長")]
        TeamLeader = 8,

        /// <summary>
        /// 一般員工
        /// </summary>
        [Description("一般員工")]
        Employee = 9,
    }

    /// <summary>
    /// 廠區
    /// </summary>
    public enum Factory
    {
        /// <summary>
        /// 一廠
        /// </summary>
        [Description("一廠")]
        Bar = 1,

        /// <summary>
        /// 二廠
        /// </summary>
        [Description("二廠")]
        Straight = 2,

        /// <summary>
        /// 三廠
        /// </summary>
        [Description("三廠")]
        Square = 3,

        /// <summary>
        /// 全廠
        /// </summary>
        [Description("全廠")]
        Fairlybike = 9,
    }

    /// <summary>
    /// 裝置
    /// </summary>
    public enum DeviceType
    {
        /// <summary>
        /// 電腦版
        /// </summary>
        [Description("電腦版")]
        PC = 1,

        /// <summary>
        /// 手機版
        /// </summary>
        [Description("手機版")]
        Mobile = 2,
    }

    /// <summary>
    /// 簽到結果
    /// </summary>
    public enum SignInResult
    {
        /// <summary>
        /// 失敗
        /// </summary>
        [Description("簽到失敗，請確認選擇會議室是否取消或更換地址，謝謝。")]
        Failed = 1,

        /// <summary>
        /// 未開放簽到
        /// </summary>
        [Description("尚未開放簽到，請於會議開放簽到後再行簽到，謝謝。")]
        NotSignable = 2,

        /// <summary>
        /// 未在出席名單
        /// </summary>
        [Description("您並未在出席名單中，請確認填寫會議室及工號是否正確，謝謝。")]
        NotInList = 3,

        /// <summary>
        /// 成功
        /// </summary>
        [Description("簽到成功，感謝您的參與。")]
        Successed = 4,

        /// <summary>
        /// 重複
        /// </summary>
        [Description("您已簽到，感謝您的參與。")]
        Repeat = 5,

        /// <summary>
        /// 無申請紀錄
        /// </summary>
        [Description("會議室無申請紀錄，請確認填寫會議室是否正確，謝謝。")]
        NotUsed = 6,

        /// <summary>
        /// 超過時間
        /// </summary>
        [Description("由於您已超過簽到時限，故無法簽到，如有疑問請聯繫會議發起人，感謝您的配合。")]
        Late = 7,

        /// <summary>
        /// 無效Qrcode
        /// </summary>
        [Description("無效登入Qrcode，請使用通知信中Qrcode掃碼，謝謝。")]
        NotFound = 8,
    }

    /// <summary>
    /// 訓練類別
    /// </summary>
    public enum Category
    {
        /// <summary>
        /// 內訓
        /// </summary>
        [Description("內訓")]
        Internal = 1,

        /// <summary>
        /// 外訓
        /// </summary>
        [Description("外訓")]
        External = 2,

        /// <summary>
        /// 研討會
        /// </summary>
        [Description("研討會")]
        Seminar = 3,

        /// <summary>
        /// 外聘講師
        /// </summary>
        [Description("外聘講師")]
        ExternalLecturer = 4,
    }

    /// <summary>
    /// 團體類別
    /// </summary>
    public enum GroupType
    {
        /// <summary>
        /// 團體
        /// </summary>
        [Description("團體(承辦人統一回饋)")]
        Group = 1,

        /// <summary>
        /// 個人
        /// </summary>
        [Description("個人(參與人員自行回饋)")]
        Person = 2,
    }

    /// <summary>
    /// 證書/證明
    /// </summary>
    public enum Certification
    {
        /// <summary>
        /// 有
        /// </summary>
        [Description("有")]
        Yes = 1,

        /// <summary>
        /// 無
        /// </summary>
        [Description("無")]
        No = 2,

        /// <summary>
        /// 上課證明
        /// </summary>
        [Description("上課證明")]
        ClassCert = 3,
    }

    /// <summary>
    /// 異常狀況
    /// </summary>
    public enum Abnormal
    {
        /// <summary>
        /// 乾咳 / ไอแห้ง
        /// </summary>
        [Description("乾咳 / ไอแห้ง")]
        One = 1,

        /// <summary>
        /// 喉嚨痛 / เจ็บคอ
        /// </summary>
        [Description("喉嚨痛 / เจ็บคอ")]
        Two = 2,

        /// <summary>
        /// 發燒 / ไข้
        /// </summary>
        [Description("發燒 / ไข้")]
        Three = 3,
        /// <summary>
        /// 本人或密切接觸者有確診者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดของฉันได้รับการยืนยันเคสแล้ว
        /// </summary>
        [Description("本人或密切接觸者有確診者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดของฉันได้รับการยืนยันเคสแล้ว")]
        Four = 4,

        /// <summary>
        /// 本人或密切接觸者有疑似病例者 / ผู้ที่มีเคสต้องสงสัยในตัวเองหรือคนใกล้ชิด
        /// </summary>
        [Description("本人或密切接觸者有疑似病例者 / ผู้ที่มีเคสต้องสงสัยในตัวเองหรือคนใกล้ชิด")]
        Five = 5,

        /// <summary>
        /// 本人或密切接觸者有遭居家檢疫者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดอยู่ภายใต้การกักกันที่บ้าน
        /// </summary>
        [Description("本人或密切接觸者有遭居家檢疫者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดอยู่ภายใต้การกักกันที่บ้าน")]
        Six = 6,

        /// <summary>
        /// 本人與確診案例同時段出現於活動地點，或有收到來自中央疫情指揮中心的疫情警示相關訊息者 / บุคคลที่ปรากฏตัว ณ สถานที่เกิดเหตุพร้อมกับผู้ป่วยที่ได้รับการยืนยัน หรือผู้ที่ได้รับข้อความเตือนโรคระบาดจากศูนย์บัญชาการแพร่ระบาดกลาง
        /// </summary>
        [Description("本人與確診案例同時段出現於活動地點，或有收到來自中央疫情指揮中心的疫情警示相關訊息者 / บุคคลที่ปรากฏตัว ณ สถานที่เกิดเหตุพร้อมกับผู้ป่วยที่ได้รับการยืนยัน หรือผู้ที่ได้รับข้อความเตือนโรคระบาดจากศูนย์บัญชาการแพร่ระบาดกลาง")]
        Seven = 7,

        /// <summary>
        /// 其他 / อื่นๆ
        /// </summary>
        [Description("其他 / อื่นๆ")]
        Eight = 8,

        /// <summary>
        /// 鼻塞 流鼻水 / คัดจมูก น้ำมูกไหล
        /// </summary>
        [Description("鼻塞 流鼻水 / คัดจมูก น้ำมูกไหล")]
        Nine = 9,

        /// <summary>
        /// 腹瀉 / ท้องเสีย
        /// </summary>
        [Description("腹瀉 / ท้องเสีย")]
        Ten = 10,
    }

    /// <summary>
    /// 審核狀態
    /// </summary>
    public enum PFAuditState
    {
        /// <summary>
        /// 等待班組長簽核中
        /// </summary>
        [Description("等待班組長簽核中")]
        Processing = 0,

        /// <summary>
        /// 待課級主管簽核中
        /// </summary>
        [Description("待課級主管簽核中")]
        TLAudit = 1,

        /// <summary>
        /// 待主管簽核中
        /// </summary>
        [Description("待主管簽核中")]
        CSAudit = 2,

        /// <summary>
        /// 待主管簽核中
        /// </summary>
        [Description("簽核完成")]
        ManagerAudit = 3,

        ///// <summary>
        ///// 簽核完成
        ///// </summary>
        //[Description("簽核完成")]
        //PrincipalAudit = 4,

        /// <summary>
        /// 駁回
        /// </summary>
        [Description("駁回")]
        Reject = 6,

        /// <summary>
        /// 核准
        /// </summary>
        [Description("核准")]
        Approval = 7,
    }

    /// <summary>
    /// 審核狀態
    /// </summary>
    public enum AuditState
    {
        /// <summary>
        /// 等待承辦人簽核中
        /// </summary>
        [Description("等待承辦人簽核中")]
        Processing = 0,

        /// <summary>
        /// 待主管簽核中
        /// </summary>
        [Description("待主管簽核中")]
        OfficerAudit = 1,

        /// <summary>
        /// 待副總經理簽核中
        /// </summary>
        [Description("待副總經理簽核中")]
        ManagerAudit = 2,

        /// <summary>
        /// 待總經理簽核中
        /// </summary>
        [Description("待總經理簽核中")]
        ViceAudit = 3,

        /// <summary>
        /// 待校長簽核中
        /// </summary>
        [Description("待校長簽核中")]
        PresidentAudit = 5,

        /// <summary>
        /// 簽核完成
        /// </summary>
        [Description("簽核完成")]
        PrincipalAudit = 4,

        /// <summary>
        /// 駁回
        /// </summary>
        [Description("駁回")]
        Reject = 6,

        /// <summary>
        /// 核准
        /// </summary>
        [Description("核准")]
        Approval = 7,

        /// <summary>
        /// 待人資簽核中
        /// </summary>
        [Description("待人資簽核中")]
        HRAudit = 8,
    }

    /// <summary>
    /// 出席狀態
    /// </summary>
    public enum SignStatus
    {
        /// <summary>
        /// 狀態:未簽到
        /// </summary>
        [Description("未簽到")]
        UnSigned = 0,

        /// <summary>
        /// 狀態:已簽到
        /// </summary>
        [Description("已簽到")]
        Signed = 1,

        /// <summary>
        /// 狀態:請假
        /// </summary>
        [Description("請假")]
        Leave = 2,
    }

    /// <summary>
    /// 表單狀態
    /// </summary>
    public enum ApprovalState
    {
        /// <summary>
        /// 狀態:草稿
        /// </summary>
        [Description("草稿")]
        Draft = 0,

        /// <summary>
        /// 狀態:審核中
        /// </summary>
        [Description("審核中")]
        Approving = 1,

        /// <summary>
        /// 狀態:審核通過
        /// </summary>
        [Description("審核通過")]
        Approved = 2,

        /// <summary>
        /// 狀態:審核駁回
        /// </summary>
        [Description("審核駁回")]
        Reject = 3,
    }

    /// <summary>
    /// KPI層級
    /// </summary>
    public enum KPIType
    {
        /// <summary>
        /// 倉儲
        /// </summary>
        [Description("倉儲")]
        Warehouse = 1,

        /// <summary>
        /// 營銷
        /// </summary>
        [Description("營銷")]
        Marketing = 2,

        /// <summary>
        /// 公司制度
        /// </summary>
        [Description("公司制度")]
        System = 3,

        /// <summary>
        /// 人事
        /// </summary>
        [Description("人事")]
        HR = 4,

        /// <summary>
        /// 環境
        /// </summary>
        [Description("環境")]
        Environment = 5,
    }

    /// <summary>
    /// KPI層級
    /// </summary>
    public enum KPILevel
    {
        /// <summary>
        /// 菲力指標
        /// </summary>
        [Description("菲力指標")]
        FairlyKPI = 1,

        /// <summary>
        /// 部門指標
        /// </summary>
        [Description("部門指標")]
        DeptKPI = 2,
    }

    /// <summary>
    /// 審核對象類型
    /// </summary>
    public enum TargetType
    {
        /// <summary>
        /// KPI
        /// </summary>
        [Description("部門KPI")]
        KPI = 1,

        /// <summary>
        /// PersonKPI
        /// </summary>
        [Description("個人KPI")]
        PersonKPI = 2,

        /// <summary>
        /// PersonKPIState
        /// </summary>
        [Description("個人進度")]
        PersonKPIState = 3,
    }

    /// <summary>
    /// 表單狀態
    /// </summary>
    public enum ApprovalAction
    {
        /// <summary>
        /// 動作:送審
        /// </summary>
        [Description("動作:送審")]
        Submit = 1,

        /// <summary>
        /// 動作:核准
        /// </summary>
        [Description("動作:核准")]
        Approve = 2,

        /// <summary>
        /// 動作:退回
        /// </summary>
        [Description("動作:退回")]
        Reject = 3,

        /// <summary>
        /// 動作:編輯
        /// </summary>
        [Description("動作:編輯")]
        Edit = 4,
    }

    /// <summary>
    /// 審核類型
    /// </summary>
    public enum AuditType
    {
        /// <summary>
        /// 外訓
        /// </summary>
        [Description("外訓")]
        External = 1,

        /// <summary>
        /// 內訓
        /// </summary>
        [Description("內訓")]
        Internal = 2,

        /// <summary>
        /// 行動方案
        /// </summary>
        [Description("行動方案")]
        ClassActionPlan = 3,

        /// <summary>
        /// 課後意見
        /// </summary>
        [Description("課後意見")]
        ClassOpinion = 4,

        /// <summary>
        /// 心得報告
        /// </summary>
        [Description("心得報告")]
        ClassReport = 5,

        /// <summary>
        /// 外訓驗收
        /// </summary>
        [Description("外訓驗收")]
        ExternalAcceptance = 6,

        /// <summary>
        /// 內訓驗收
        /// </summary>
        [Description("內訓驗收")]
        InternalAcceptance = 7,

        /// <summary>
        /// 內訓研討會
        /// </summary>
        [Description("內訓研討會")]
        Seminar = 8,

        /// <summary>
        /// 個人目標
        /// </summary>
        [Description("個人目標")]
        PersonKPI = 9,

        /// <summary>
        /// 績效評比
        /// </summary>
        [Description("績效評比")]
        PF = 10,

        /// <summary>
        /// 外訓結案
        /// </summary>
        [Description("外訓結案")]
        CloseExternalTraining = 11,

        /// <summary>
        /// 內訓結案
        /// </summary>
        [Description("內訓結案")]
        CloseInternalTraining = 12,

        /// <summary>
        /// 課程異動
        /// </summary>
        [Description("課程異動")]
        TrainingAbnormal = 13,
    }

    /// <summary>
    /// 表單簽核
    /// </summary>
    public enum FormAudit
    {
        /// <summary>
        /// 待審
        /// </summary>
        [Description("待審")]
        Processing = 1,

        /// <summary>
        /// 核准
        /// </summary>
        [Description("核准")]
        Approval = 2,

        /// <summary>
        /// 駁回
        /// </summary>
        [Description("駁回")]
        Reject = 3,
    }

    /// <summary>
    /// 同意程度
    /// </summary>
    public enum AdmitRadio
    {
        /// <summary>
        /// 非常不同意
        /// </summary>
        [Description("非常不同意")]
        L1 = 1,

        /// <summary>
        /// 不同意
        /// </summary>
        [Description("不同意")]
        L2 = 2,

        /// <summary>
        /// 不太同意
        /// </summary>
        [Description("不太同意")]
        L3 = 3,

        /// <summary>
        /// 普通
        /// </summary>
        [Description("普通")]
        L4 = 4,

        /// <summary>
        /// 部分同意
        /// </summary>
        [Description("部分同意")]
        L5 = 5,

        /// <summary>
        /// 同意
        /// </summary>
        [Description("同意")]
        L6 = 6,

        /// <summary>
        /// 完全同意
        /// </summary>
        [Description("完全同意")]
        L7 = 7,
    }

    /// <summary>
    /// 滿意程度
    /// </summary>
    public enum SatisfyRadio
    {
        /// <summary>
        /// 非常不滿意
        /// </summary>
        [Description("非常不滿意")]
        L1 = 1,

        /// <summary>
        /// 不滿意
        /// </summary>
        [Description("不滿意")]
        L2 = 2,

        /// <summary>
        /// 稍微不滿意
        /// </summary>
        [Description("稍微不滿意")]
        L3 = 3,

        /// <summary>
        /// 普通
        /// </summary>
        [Description("普通")]
        L4 = 4,

        /// <summary>
        /// 還行
        /// </summary>
        [Description("還行")]
        L5 = 5,

        /// <summary>
        /// 滿意
        /// </summary>
        [Description("滿意")]
        L6 = 6,

        /// <summary>
        /// 非常滿意
        /// </summary>
        [Description("非常滿意")]
        L7 = 7,
    }

    /// <summary>
    /// 難易程度
    /// </summary>
    public enum DifficultyRadio
    {
        /// <summary>
        /// 有點難
        /// </summary>
        [Description("有點難")]
        Hard = 1,

        /// <summary>
        /// 適中
        /// </summary>
        [Description("適中")]
        OK = 2,

        /// <summary>
        /// 有點簡單
        /// </summary>
        [Description("有點簡單")]
        Easy = 3,
    }

    /// <summary>
    /// 資料狀態
    /// </summary>
    public enum InfoState
    {
        /// <summary>
        /// 初期設定
        /// </summary>
        [Description("初期設定")]
        Initial = 1,

        /// <summary>
        /// 最終檢核
        /// </summary>
        [Description("最終檢核")]
        Final = 2,
    }

    /// <summary>
    /// 分類層級
    /// 班別>>分類>>評比項目>>層級 && 作業
    /// </summary>
    public enum LevelType
    {
        /// <summary>
        /// 班別
        /// </summary>
        [Description("班別")]
        CLS = 1,

        /// <summary>
        /// 分類
        /// </summary>
        [Description("分類")]
        CATE = 2,

        /// <summary>
        /// 評比項目
        /// </summary>
        [Description("評比項目")]
        ITM = 3,

        /// <summary>
        /// 層級
        /// </summary>
        [Description("層級")]
        LVL = 4,

        /// <summary>
        /// 作業
        /// </summary>
        [Description("作業")]
        WORK = 5,
    }

    /// <summary>
    /// 績效評比分類
    /// </summary>
    public enum PF_Category
    {
        /// <summary>
        /// S技能
        /// </summary>
        [Description("S技能")]
        S = 1,

        /// <summary>
        /// E訓練與經驗
        /// </summary>
        [Description("E訓練與經驗")]
        E = 2,

        /// <summary>
        /// K知識
        /// </summary>
        [Description("K知識")]
        K = 3,

        /// <summary>
        /// 其他特殊工作加分項目
        /// </summary>
        [Description("其他特殊工作加分項目")]
        OTHER = 4,

        /// <summary>
        /// P特質/A態度
        /// </summary>
        [Description("P特質/A態度")]
        PA = 5,
    }

    /// <summary>
    /// 抽獎狀態
    /// </summary>
    public enum DrawStatus
    {
        /// <summary>
        /// 未中獎
        /// </summary>
        [Description("未中獎")]
        Zero = 1,

        /// <summary>
        /// 中獎
        /// </summary>
        [Description("中獎")]
        One = 2,

        /// <summary>
        /// 多次中獎
        /// </summary>
        [Description("多次中獎")]
        MoreThanOne = 3,
    }

    /// <summary>
    /// 利益關係人選項
    /// </summary>
    public enum StackHolders
    {
        /// <summary>
        /// 學員
        /// </summary>
        [Description("學員")]
        Students = 1,

        /// <summary>
        /// 主管
        /// </summary>
        [Description("主管")]
        Manager = 2,

        /// <summary>
        /// 公司
        /// </summary>
        [Description("公司")]
        Company = 3,

        /// <summary>
        /// 客戶
        /// </summary>
        [Description("客戶")]
        Client = 4,

        /// <summary>
        /// 供應商
        /// </summary>
        [Description("供應商")]
        Supplier = 5,

        /// <summary>
        /// 講師
        /// </summary>
        [Description("講師")]
        Lecturer = 6,
    }

    /// <summary>
    /// 問題類型
    /// </summary>
    public enum AnswerType
    {
        /// <summary>
        /// 文字
        /// </summary>
        [Description("文字")]
        TextBox = 1,

        /// <summary>
        /// 多行文字
        /// </summary>
        [Description("多行文字")]
        TextArea = 2,

        /// <summary>
        /// 日期
        /// </summary>
        [Description("日期")]
        Date = 3,

        /// <summary>
        /// 單選
        /// </summary>
        [Description("單選")]
        SingleSelect = 4,

        /// <summary>
        /// 複選
        /// </summary>
        [Description("複選")]
        MultipleSelect = 5,

        /// <summary>
        /// 狀態切換
        /// </summary>
        [Description("狀態切換")]
        Switch = 6,

        /// <summary>
        /// 檔案上傳
        /// </summary>
        [Description("檔案上傳")]
        Upload = 7,

        /// <summary>
        /// 編輯器
        /// </summary>
        [Description("編輯器")]
        CKEditor = 8,

        /// <summary>
        /// BK
        /// </summary>
        [Description("BK")]
        BK = 9,
    }
}