﻿using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.Join
{
    public class MeetingJoinSingInAndEmployeeAndDepartment
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Organiser { get; set; }

        public int FactoryID { get; set; }

        public int RoomID { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsSignable { get; set; }

        public int LateMinutes { get; set; }

        public string SignCode { get; set; }
        public string JobID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentTitle { get; set; }


        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        public string RoomStr
        {
            get
            {
                return roomRepository.GetRoomString(this.RoomID);
            }
        }
       
    }
}