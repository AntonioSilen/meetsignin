﻿using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.Join
{
    public class TrainingStatisticsJoinModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Lecturer { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Person { get; set; }

        public string PersonId { get; set; }

        public int SignStatus { get; set; }

        public int CoursePoints { get; set; }

        public int LecturerPoints { get; set; }

        public int RewordPoints { get; set; }

        public int OpinionPoints { get; set; }

        public string Difficulty { get; set; }

        public string DeptID { get; set; }

    }
}