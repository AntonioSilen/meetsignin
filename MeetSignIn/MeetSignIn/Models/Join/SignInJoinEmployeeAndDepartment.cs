﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.Join
{
    public class SignInJoinEmployeeAndDepartment
    {
        public int ID { get; set; }
        public string SignCode { get; set; }
        public string JobID { get; set; }
        public string DepartmentTitle { get; set; }
        public string Name { get; set; }
        public string EngName { get; set; }
        public string Email { get; set; }
        public string Ext { get; set; }
        public bool IsForeign { get; set; }
        //public bool IsSign { get; set; }
        public int SignStatus { get; set; }
        public bool IsSent { get; set; }
        public DateTime? SignTime { get; set; }
        public int MeetingID { get; set; }
        public int DepartmentID { get; set; }
        public string DeptID { get; set; }
    }
}