﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.Chart
{
    public class NumAndPercentageModel
    {
        public string Title { get; set; }
        public double DoubleTitle
        {
            get
            {
                try
                {
                    return Convert.ToDouble(this.Title);
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }
        public int Num { get; set; }

        public int AllCount { get; set; }
        public int CateCount { get; set; }

        public string Percentage
        {
            get
            {
                return (100.0 * this.CateCount / this.AllCount).ToString("0.00");
            }
        }
    }
}