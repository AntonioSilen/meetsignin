﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.Others
{
    public class AdminInfo
    {
        public int ID { get; set; }

        public string Account { get; set; }

        public int Type { get; set; }

        public bool IsPrincipal { get; set; }

        public string Name { get; set; }

        public string DeptId { get; set; }
        public string DeptIName { get; set; }
    }
}