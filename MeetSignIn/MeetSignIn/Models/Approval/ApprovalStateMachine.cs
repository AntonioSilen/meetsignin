﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models.Approval
{
    /// <summary>
    /// 表單 State Machine
    /// </summary>
    public class ApprovalStateMachine
    {
        /// <summary>
        /// 目前狀態
        /// </summary>
        private ApprovalState _currentState;

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="state">初始狀態</param>
        public ApprovalStateMachine(ApprovalState state)
        {
            _currentState = state;
        }

        /// <summary>
        /// 定義動作與狀態互動關係
        /// </summary>
        /// <param name="action">動作</param>
        /// <returns>新狀態</returns>
        private ApprovalState Manipulate(ApprovalAction action)
        {
            switch (action)
            {
                case ApprovalAction.Submit:
                    if (_currentState == ApprovalState.Draft)
                    {
                        return ApprovalState.Approving;
                    }
                    break;
                case ApprovalAction.Approve:
                    if (_currentState == ApprovalState.Approving)
                    {
                        return ApprovalState.Approved;
                    }
                    break;
                case ApprovalAction.Reject:
                    if (_currentState == ApprovalState.Approving)
                    {
                        return ApprovalState.Reject;
                    }
                    break;
                case ApprovalAction.Edit:
                    if (_currentState == ApprovalState.Approved)
                    {
                        return ApprovalState.Draft;
                    }
                    else if (_currentState == ApprovalState.Reject)
                    {
                        return ApprovalState.Draft;
                    }
                    break;
            }
            throw new Exception($"目前狀態 {_currentState} 不允許執行 {action} 動作！");
        }

        /// <summary>
        /// 執行動作來改變狀態
        /// </summary>
        /// <param name="action">動作</param>
        /// <returns>新狀態</returns>
        public ApprovalState Transition(ApprovalAction action)
        {
            // 執行動作取得新狀態
            var newState = Manipulate(action);

            // 更新目前狀態
            _currentState = newState;

            return newState;
        }

        /// <summary>
        /// 提供允許執行動作清單
        /// </summary>
        /// <returns>允許執行動作清單</returns>
        public List<ApprovalAction> AvailableActions()
        {
            var availableActions = new List<ApprovalAction>();

            foreach (ApprovalAction action in Enum.GetValues(typeof(ApprovalAction)))
            {
                try
                {
                    // 模擬執行動作
                    Manipulate(action);

                    // 有定義此狀態可以執行的動作時就加入清單
                    availableActions.Add(action);
                }
                catch (Exception e)
                {
                    // 非法操作就不列入
                }
            }

            return availableActions;
        }
    }
}