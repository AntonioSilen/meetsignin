﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Models
{
    public class TrainingExacutiveModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set;}
        public DateTime EndDate { get; set;}

        //總人數
        public int TotalNum { get; set; }

        //簽到人數
        public int SignedNum { get; set; }
        //請假人數
        public int LeaveNum { get; set; }
        //未簽到人數
        public int UnSignNum { get; set; }
        //意見填寫人數
        public int OpinionFilledNum { get; set; }
        //回饋填寫人數
        public int AccptanceFilledNum { get; set; }
        //完成考試人數
        public int QuizCompleteNum { get; set; }
    }
}