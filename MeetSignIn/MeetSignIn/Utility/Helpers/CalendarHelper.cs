﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Security;
using System.Web;

namespace MeetSignIn.Utility.Helpers
{
    public class CalendarHelper
    {
        static bool RedirectionCallback(string url)
        {
            return url.ToLower().StartsWith("https://");
        }

        public static void UseExchangeService(string userEmailAddress, SecureString userPassword)
        {
            try
            {
                ExchangeService service = new ExchangeService();

                #region Authentication

                // Set specific credentials.
                service.Credentials = new NetworkCredential(userEmailAddress, userPassword);
                #endregion

                #region Endpoint management

                // Look up the user's EWS endpoint by using Autodiscover.
                //service.AutodiscoverUrl(userEmailAddress, RedirectionCallback);
                service.AutodiscoverUrl(userEmailAddress);

                //Console.WriteLine("EWS Endpoint: {0}", service.Url);
                #endregion

                #region Working with groups of items

                //// Create two new contacts in the user's default
                //// Contacts folder.
                //List<Contact> contactsToAdd = new List<Contact>();

                //Contact newContact1 = new Contact(service);
                //newContact1.GivenName = "Rosetta";
                //newContact1.Surname = "Simpson";
                //newContact1.PhoneNumbers[PhoneNumberKey.MobilePhone] = "425-555-1234";
                //newContact1.EmailAddresses[EmailAddressKey.EmailAddress1] = "rosetta@alpineskihouse.com";

                //contactsToAdd.Add(newContact1);

                //Contact newContact2 = new Contact(service);
                //newContact2.GivenName = "Barney";
                //newContact2.Surname = "Carmack";
                //newContact2.PhoneNumbers[PhoneNumberKey.MobilePhone] = "425-555-5678";
                //newContact2.EmailAddresses[EmailAddressKey.EmailAddress1] = "barney@contoso.com";

                //contactsToAdd.Add(newContact2);

                //ServiceResponseCollection<ServiceResponse> createItemsResponse =
                //         service.CreateItems(contactsToAdd, WellKnownFolderName.Contacts, null, null);

                //if (createItemsResponse.OverallResult != ServiceResult.Success)
                //{
                //    Console.WriteLine("CreateItems returned a non-success response!");
                //    for (int i = 0; i < createItemsResponse.Count; i++)
                //    {
                //        Console.WriteLine("{0}: {1} - {2}", i + 1,
                //            createItemsResponse[i].ErrorCode, createItemsResponse[i].ErrorMessage);
                //    }
                //}
                //else
                //{
                //    Console.WriteLine("CreateItems successfully created 2 contacts in default Contacts folder.");
                //}
                #endregion

                #region Working with delegates

                //// Add a user as a delegate with Reviewer rights
                //// to the user's Calendar folder.
                //Mailbox mailbox = new Mailbox(userEmailAddress);

                //DelegateUser newDelegate = new DelegateUser("ian@fourthcoffee.com");
                //newDelegate.Permissions.CalendarFolderPermissionLevel = DelegateFolderPermissionLevel.Reviewer;

                //List<DelegateUser> delegatesToAdd = new List<DelegateUser>();
                //delegatesToAdd.Add(newDelegate);

                //Collection<DelegateUserResponse> addDelegateResponse = service.AddDelegates(mailbox, null, delegatesToAdd);

                //for (int i = 0; i < addDelegateResponse.Count; i++)
                //{
                //    if (addDelegateResponse[i].Result != ServiceResult.Success)
                //    {
                //        Console.WriteLine("Unable to add {0} as a delegate.",
                //            addDelegateResponse[i].DelegateUser.UserId.PrimarySmtpAddress);
                //        Console.WriteLine("    {0}: {1}", addDelegateResponse[i].ErrorCode,
                //            addDelegateResponse[i].ErrorMessage);
                //    }
                //    else
                //    {
                //        Console.WriteLine("Added {0} as a delegate.",
                //            addDelegateResponse[i].DelegateUser.UserId.PrimarySmtpAddress);
                //    }
                //}

                #endregion

                #region Create Appointment
                Appointment appointment = new Appointment(service);
                // Set the properties on the appointment object to create the appointment.
                appointment.Subject = "Tennis lesson";
                appointment.Body = "Focus on backhand this week.";
                appointment.Start = DateTime.Now.AddDays(2);
                appointment.End = appointment.Start.AddHours(1);
                appointment.Location = "Tennis club";
                appointment.ReminderDueBy = DateTime.Now;
                // Save the appointment to your calendar.
                appointment.Save(SendInvitationsMode.SendToNone);
                // Verify that the appointment was created by using the appointment's item ID.
                Item item = Item.Bind(service, appointment.Id, new PropertySet(ItemSchema.Subject));
                Console.WriteLine("\nAppointment created: " + item.Subject + "\n");
                #endregion
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}