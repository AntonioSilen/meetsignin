﻿using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace MeetSignIn.Utility.Helpers
{
    public class LanguageHelper
    { /// <summary>
      /// 語系預設值
      /// </summary>
        private static Language defaultLanguage = Language.zh_TW;

        public static Language GetLanguage(string language)
        {
            language = language.Replace("-", "_");
            try
            {
                var langCulture = (Language)Enum.Parse(typeof(Language), language, false);
                return langCulture;
            }
            catch (Exception ex)
            {
                //預設值
                return defaultLanguage;
            }
        }

        /// <summary>
        /// 存放資料的Key
        /// </summary>
        private const string FAIRLY_USER_LANG = "FairlyUserLang";

        /// <summary>
        /// 儲存語系
        /// </summary>
        /// <param name="member"></param>
        /// <param name="isRemeber"></param>
        /// <returns></returns>
        public static void SetLang(string lang)
        {
            SessionHelper.Del(FAIRLY_USER_LANG);
            CookieHelper.Del(FAIRLY_USER_LANG);

            DateTime expires = DateTime.Now.AddDays(1);
            CookieHelper.SetCookie(FAIRLY_USER_LANG, lang, expires);
            SessionHelper.SetSession(FAIRLY_USER_LANG, lang);
        }


        /// <summary>
        /// 取得語系
        /// </summary>
        /// <returns></returns>
        public static string GetLang()
        {
            string data = SessionHelper.Get(FAIRLY_USER_LANG);
            if (!string.IsNullOrEmpty(data))
            {
                return data.ToString();
            }

            data = CookieHelper.Get(FAIRLY_USER_LANG);
            if (!string.IsNullOrEmpty(data))
            {
                return data.ToString();
            }
            return null;
        }
    }
}