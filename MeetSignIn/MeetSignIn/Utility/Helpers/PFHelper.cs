﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.Utility.Helpers
{
    public class PFHelper
    {
        public PFSeason GetActivatePFSeason(string deptId)
        {
            PFSeasonRepository pFSeasonRepository = new PFSeasonRepository(new MeetingSignInDBEntities());
            return pFSeasonRepository.Query(true, 0, deptId, "", true).FirstOrDefault();
        }
    }
}