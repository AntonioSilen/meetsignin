﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.ViewModels.Employee;
using MeetSignIn.ViewModels.Meeting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Controllers
{
    [ErrorHandleActionFilter]
    public class MeetingController : BaseController
    {
        private MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());
        private SignInRepository signInRepository = new SignInRepository(new MeetingSignInDBEntities());
        //private EmployeeRepository employeeRepository = new EmployeeRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        // GET: Meeting 
        public ActionResult Index()
        {
            MeetingIndexView model = new MeetingIndexView();
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(MeetingIndexView model)
        {
            return RedirectToAction("List", new { jid = model.JobID });
        }

        public ActionResult List(string jid = "")
        {
            MeetingListView model = new MeetingListView();
            var employeeInfo = personRepository.FindByPersonId(jid);
            if (string.IsNullOrEmpty(jid) || employeeInfo == null)
            {
                ShowMessage(false, "查無此工號");
                return RedirectToAction("Index", "Meeting");
            }
            DateTime today = DateTime.Today;
            DateTime filterStart = Convert.ToDateTime(today.ToString("yyyy/MM/dd"));
            DateTime filterEnd = today.AddYears(5);
            var signInList = signInRepository.Query(null, "", 0, jid);
            var signInMIdList = signInList.Select(q => q.MeetingID).ToList();
            var meetList = meetingRepository.Query("", filterStart, filterEnd).Where(q => signInMIdList.Contains(q.ID));
            model.MeetingList = Mapper.Map<List<MeetingView>>(meetList);

            foreach (var item in model.MeetingList)
            {
                var signInfo = signInList.Where(q => q.MeetingID == item.ID && q.JobID == jid).FirstOrDefault();
                item.SignTime = signInfo.SignTime;
                item.IsSigned = item.SignTime != null;
                DateTime lastSignTime = item.StartDate.AddMinutes(item.LateMinutes);
                item.SignableStatus = GetSignableStatus(item.SignDate, lastSignTime, item.IsSignable);
            }

            model.EmployeeInfo =  Mapper.Map<EmployeeView>(personRepository.GetPersonQuery("", jid).FirstOrDefault());

            return View(model);
        }
    }
}