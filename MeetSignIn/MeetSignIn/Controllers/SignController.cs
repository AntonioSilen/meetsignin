﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
//using BarcodeLib.BarcodeReader;
using System.IO;
using ZXing;
using System.Drawing;
using MeetSignIn.ViewModels.Sign;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.ViewModels.Meeting;
using MeetSignIn.Models.Others;
using MeetSignIn.Areas.Admin.ViewModels.Quiz;
using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Areas.Admin.ViewModels.Training;
using MeetSignIn.Repositories.TrainingForm;
using Newtonsoft.Json;
using MeetSignIn.ViewModels.QNR;

namespace MeetSignIn.Controllers
{
    [ErrorHandleActionFilter]
    public class SignController : BaseController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());
        private SignInRepository signInRepository = new SignInRepository(new MeetingSignInDBEntities());
        private RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());
        private ClassOpinionRepository classOpinionRepository = new ClassOpinionRepository(new MeetingSignInDBEntities());

        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        private QNRRepository qNRRepository = new QNRRepository(new MeetingSignInDBEntities());
        private QNRQuestionRepository qNRQuestionRepository = new QNRQuestionRepository(new MeetingSignInDBEntities());
        private QNRResultRepository qNRResultRepository = new QNRResultRepository(new MeetingSignInDBEntities());
        private OptionItemRepository optionItemRepository = new OptionItemRepository(new MeetingSignInDBEntities());

        public Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public int SignedStatus = (int)SignStatus.Signed;
        public int UnSignedStatus = (int)SignStatus.UnSigned;
        public int LeaveStatus = (int)SignStatus.Leave;

        public ActionResult Index(string rn)
        {
            SignInView model = new SignInView();

            model.RoomNumber = rn;
            model.MeetingInfo = Mapper.Map<MeetingView>(meetingRepository.GetMeetByNum(rn));
            var meetingInfo = model.MeetingInfo;
            if (meetingInfo != null)
            {
                DateTime lastSignTime = meetingInfo.StartDate.AddMinutes(meetingInfo.LateMinutes);
                meetingInfo.SignableStatus = GetSignableStatus(meetingInfo.SignDate, lastSignTime, meetingInfo.IsSignable);
            }
            else
            {
                ShowMessage(false, "無會議資料");
                return View(model);
            }

            var url = Request.Url;
            model.QRCodeUrl = model.MeetingInfo == null ? "" : url.Scheme + "://" + url.Authority + "/Sign/Check?rn=" + rn + "&mid=" + model.MeetingInfo.ID;
            //model.QRCodeUrl = "https://cwww.spgateway.com/main/login_center/single_login";
            if (!string.IsNullOrEmpty(model.QRCodeUrl))
            {
                return Redirect(model.QRCodeUrl);
            }

            return View(model);
        }

        public ActionResult Scanner()
        {


            return View();
        }

        public ActionResult Read()
        {
            ShowMessage(false, "查無此工號");
            return View();
        }
  
        public ActionResult Check(string rn, int mid)
        {
            var meetingInfo = meetingRepository.GetMeetByNum(rn);
            if (meetingInfo == null)
            {
                return RedirectToAction("Result", new { jid = "", mid = mid, rn = 0, result = rn + EnumHelper.GetDescription(SignInResult.NotUsed) });
            }
            DateTime lastSignTime = meetingInfo.StartDate.AddMinutes(meetingInfo.LateMinutes);
            int signableStatus = GetSignableStatus(meetingInfo.SignDate, lastSignTime, meetingInfo.IsSignable);
            if (signableStatus == 2)
            {
                return RedirectToAction("Result", new { jid = "", mid = mid, rn = rn, result = EnumHelper.GetDescription(SignInResult.NotSignable) });
            }
            if (signableStatus == 3)
            {
                return RedirectToAction("Result", new { jid = "", mid = mid, rn = rn, result = EnumHelper.GetDescription(SignInResult.Late), mi = meetingInfo.ID });
            }

            CheckView model = new CheckView();
            model.Title = meetingInfo.Title;
            model.Organiser = meetingInfo.Organiser;
            model.RoomNumber = rn;
            model.MeetingID = mid;

            return View(model);
        }

        [HttpPost]
        public ActionResult Check(CheckView model)
        {
            try
            {               
                string result = SignInValidate(model.MeetingID, model.JobID);
                return RedirectToAction("Result", new { jid = model.JobID, mid = model.MeetingID, rn = model.RoomNumber, result = result });
            }
            catch (Exception e)
            {
                return View(model);
            }
        }
        public ActionResult TrainingCheck(string rn = "", int tid = 0)
        {
            if (string .IsNullOrEmpty(rn) || tid == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            var currentTraining = trainingRepository.GetTrainingByNum(rn, tid);
            if (currentTraining == null)
            {//無申請紀錄
                return RedirectToAction("TrainingResult", new { jid = "", tid = tid, rn = 0, result = rn + EnumHelper.GetDescription(SignInResult.NotUsed) });
            }

            var triningInfo = trainingRepository.GetById(currentTraining.ID);
            DateTime lastSignTime = triningInfo.StartDate.AddMinutes(triningInfo.LateMinutes);
            int signableStatus = GetSignableStatus(triningInfo.SignDate, lastSignTime, triningInfo.IsSignable);
            if (signableStatus == 2)
            {//未開放簽到

                return RedirectToAction("TrainingResult", new { jid = "", tid = tid, rn = rn, result = EnumHelper.GetDescription(SignInResult.NotSignable) });
            }
            if (signableStatus == 3)
            {//超過時間

                return RedirectToAction("TrainingResult", new { jid = "", tid = tid, rn = rn, result = EnumHelper.GetDescription(SignInResult.Late), mi = triningInfo.ID });
            }

            TrainingCheckView model = new TrainingCheckView();
            model.Title = triningInfo.Title;
            model.Organiser = triningInfo.Organiser;
            model.RoomNumber = rn;
            model.TrainingID = tid;

            return View(model);
        }

        [HttpPost]
        public ActionResult TrainingCheck(TrainingCheckView model)
        {
            try
            {               
                string result = SignInValidate(model.TrainingID, model.JobID, true);
                return RedirectToAction("TrainingResult", new { jid = model.JobID, tid = model.TrainingID, rn = model.RoomNumber, result = result });
            }
            catch (Exception e)
            {
                return View(model);
            }
        }

        public ActionResult MailSignIn(int mid, string jid, string rn)
        {
            string result = SignInValidate(mid, jid);
            return RedirectToAction("Result", new { jid = jid, mid = mid, rn = rn, result = result });
        }

        public string SignInValidate(int mid, string jid, bool isTraining = false)
        {
            
            string result = EnumHelper.GetDescription(SignInResult.Failed);
            if (!isTraining)
            {
                var meetingInfo = Mapper.Map<Areas.Admin.ViewModels.Meeting.MeetingView>(meetingRepository.FindBy(mid));
                var signInData = signInRepository.Query(null, "", mid, jid).FirstOrDefault();
                DateTime lastSignTime = meetingInfo.StartDate.AddMinutes(meetingInfo.LateMinutes);
                int signableStatus = GetSignableStatus(meetingInfo.SignDate, lastSignTime, meetingInfo.IsSignable);
                switch (signableStatus)
                {
                    case 2:
                        return EnumHelper.GetDescription(SignInResult.NotSignable);
                    case 3:
                        return EnumHelper.GetDescription(SignInResult.Late);
                }
                if (signInData == null)
                {
                    return EnumHelper.GetDescription(SignInResult.NotInList);
                }
                if (signInData != null && !signInData.IsSign)
                {
                    signInData.IsSign = true;
                    signInData.SignStatus = (int)SignStatus.Signed;
                    signInData.SignTime = DateTime.Now;
                    signInRepository.Update(signInData);
                    return EnumHelper.GetDescription(SignInResult.Successed);
                }
                else if (signInData != null && signInData.IsSign)
                {
                    return EnumHelper.GetDescription(SignInResult.Repeat);
                }
            }
            else
            {
                var trainingInfo = Mapper.Map<Areas.Admin.ViewModels.Training.TrainingView>(trainingRepository.FindBy(mid));
                var trainPersonData = trainingPersonRepository.Query(null, "", mid, jid).FirstOrDefault();
                DateTime lastSignTime = trainingInfo.StartDate.AddMinutes(trainingInfo.LateMinutes);
                int signableStatus = GetSignableStatus(trainingInfo.SignDate, lastSignTime, trainingInfo.IsSignable);
                switch (signableStatus)
                {
                    case 2:
                        return EnumHelper.GetDescription(SignInResult.NotSignable);
                    case 3:
                        return EnumHelper.GetDescription(SignInResult.Late);
                }
                if (trainPersonData == null)
                {
                    return EnumHelper.GetDescription(SignInResult.NotInList);
                }
                if (trainPersonData != null && !trainPersonData.IsSign)
                {
                    trainPersonData.IsSign = true;
                    trainPersonData.SignStatus = (int)SignStatus.Signed;
                    trainPersonData.SignTime = DateTime.Now;
                    trainingPersonRepository.Update(trainPersonData);
                    return EnumHelper.GetDescription(SignInResult.Successed);
                }
                else if (trainPersonData != null && trainPersonData.IsSign)
                {
                    return EnumHelper.GetDescription(SignInResult.Repeat);
                }
            }
            return result;
        }

        public ActionResult Result(string jid, int mid, string rn, string result)
        {
            ResultView model = new ResultView();
            if (rn == "0" || string.IsNullOrEmpty(jid))
            {
                ViewBag.Type = 1;
            }
            else
            {
                ViewBag.Type = 0;
            }
            ViewBag.RoomNumber = rn;
            ViewBag.ResultStr = result;
            ViewBag.MeetingID = mid;
            ViewBag.MeetingTitle = meetingRepository.FindBy(mid).Title;
            if (!string.IsNullOrEmpty(jid))
            {
                var signInInfo = signInRepository.Query(null, "", mid, jid.ToString()).FirstOrDefault();
                if (signInInfo != null)
                {
                    var employeeInfo = personRepository.FindByPersonId(jid);
                    ViewBag.JobID = signInInfo.JobID;
                    ViewBag.Name = employeeInfo.Name + " " + employeeInfo.EngName;
                    ViewBag.SignTime = signInInfo.SignTime;
                }
            }

            return View();
        }
        public ActionResult TrainingResult(string jid, int tid = 0, string rn = "", string result = "")
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth", new { returnUrl = $"/Sign/TrainingResult?jid={jid}&tid={tid}&rn={rn}&result={result}" });
            }
            if (tid == 0)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            ResultView model = new ResultView();
            if (rn == "0" || string.IsNullOrEmpty(jid))
            {
                ViewBag.Type = 1;
            }
            else
            {
                ViewBag.Type = 0;
            }
            ViewBag.RoomNumber = rn;
            ViewBag.ResultStr = result;
            ViewBag.MeetingID = tid;
            ViewBag.TrainingTitle = trainingRepository.FindBy(tid).Title;
            if (!string.IsNullOrEmpty(jid))
            {
                var signInInfo = trainingPersonRepository.Query(null, "", tid, jid.ToString()).FirstOrDefault();
                if (signInInfo != null)
                {
                    var employeeInfo = personRepository.FindByPersonId(jid);
                    ViewBag.JobID = signInInfo.JobID;
                    ViewBag.Name = employeeInfo.Name + " " + employeeInfo.EngName;
                    ViewBag.SignTime = signInInfo.SignTime;
                }
            }

            return View();
        }

        public ActionResult RoomIndex()
        {
            SignInView model = new SignInView();
            return View(model);
        }

        [HttpPost]
        public ActionResult RoomIndex(SignInView model)
        {
            model.MeetingInfo = Mapper.Map<MeetingView>(meetingRepository.GetMeetByNum(model.RoomNumber));
            var meetingInfo = model.MeetingInfo;
            if (meetingInfo != null)
            {
                return RedirectToAction("Log", "Sign", new { mid = meetingInfo.ID });
            }
            return View(model);
        }
        public ActionResult Log(int mid = 0)
        {
            var meetingInfo = meetingRepository.FindBy(mid);
            if (meetingInfo == null)
            {
                return RedirectToAction("RoomIndex", "Sign");
            }

            Areas.Admin.ViewModels.SignIn.SignInIndexView model = new Areas.Admin.ViewModels.SignIn.SignInIndexView();
            model.IsDescending = true;
            model.MeetingID = mid;
            model.MeetingTitle = meetingInfo.Title;
            var query = signInRepository.Query(null, "", meetingInfo.ID, "");
            var pageResult = query.ToPageResult<SignIn>(model);
            model.PageResult = Mapper.Map<PageResult<Areas.Admin.ViewModels.SignIn.SignInView>>(pageResult);

            return View(model);
        }
        public ActionResult TrainingLog(int mid = 0)
        {
            var trainingInfo = trainingRepository.FindBy(mid);
            if (trainingInfo == null)
            {
                return RedirectToAction("RoomIndex", "Sign");
            }

            Areas.Admin.ViewModels.TrainingPerson.TrainingPersonIndexView model = new Areas.Admin.ViewModels.TrainingPerson.TrainingPersonIndexView();
            model.IsDescending = true;
            model.TraningID = mid;
            model.TraningTitle = trainingInfo.Title;
            var query = trainingPersonRepository.Query(null, "", trainingInfo.ID, "");
            var pageResult = query.ToPageResult<TrainingPerson>(model);
            model.PageResult = Mapper.Map<PageResult<Areas.Admin.ViewModels.TrainingPerson.TrainingPersonView>>(pageResult);

            return View(model);
        }

        public ActionResult MeetingSign(Areas.Admin.ViewModels.Meeting.MeetingIndexView model)
        {
            model.PageSize = 20;
            var query = meetingRepository.Query(model.Title, DateTime.Now.AddYears(-10), DateTime.Now.AddYears(1));
            var pageResult = query.ToPageResult<Meeting>(model);
            model.PageResult = Mapper.Map<PageResult<Areas.Admin.ViewModels.Meeting.MeetingView>>(pageResult);

            return View(model);
        }

        public ActionResult MeetingSignResult(int id = 0)
        {
            if (id == 0)
            {
                return RedirectToAction("MeetingSign", "Sign");
            }
            MeetingSignView model = new MeetingSignView();
            var data = Mapper.Map<List<Areas.Admin.ViewModels.SignIn.SignInView>>(signInRepository.Query(null, "", id));
            model.SignedList = data.Where(q => q.SignStatus == SignedStatus).OrderBy(q => q.DeptID).ToList();
            model.UnSignedList = data.Where(q => q.SignStatus == UnSignedStatus).OrderBy(q => q.DeptID).ToList();
            model.OtherList = data.Where(q => q.SignStatus == LeaveStatus).OrderBy(q => q.DeptID).ToList();

            return View(model);
        }

        public ActionResult TrainingSign(Areas.Admin.ViewModels.Training.TrainingIndexView model)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth", new { returnUrl = "/Sign/TrainingSign/" });
            }
            model.PageSize = 20;
            DateTime start = DateTime.Now.AddYears(-10);
            DateTime end = DateTime.Now.AddYears(1);
            if (model.ShowToday)
            {
                start = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd ") + "00:00");
                end = Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd ") + "23:59");
            }
            var query = trainingRepository.Query(model.Title, start, end, "", "", 0, 0, 0, null).Where(q => q.IsOnline);
            var participateTrainingIdList = trainingPersonRepository.Query(null, "", 0, memberInfo.Account).Select(q => q.TraningID).ToList();
            query = query.Where(q => participateTrainingIdList.Contains(q.ID)).AsQueryable();
            var pageResult = query.ToPageResult<Training>(model);
            model.PageResult = Mapper.Map<PageResult<Areas.Admin.ViewModels.Training.TrainingView>>(pageResult);
            foreach (var item in model.PageResult.Data)
            {
                var IPQuery = internalPlanRepository.Query(item.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                item.HasExam = false;
                if (IPQuery != null)
                {
                    item.IPInfo = Mapper.Map<InternalPlanView>(IPQuery);
                    var examData = qNRRepository.FindByTrainingId(item.ID);
                    if (IPQuery.Learning && examData != null)
                    {
                        item.HasExam = true;
                    }
                }

            }

            return View(model);
        }

        public ActionResult TrainingSignResult(int id = 0)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth", new { returnUrl = $"/Sign/TrainingSignResult/{id}" });
            }
            if (id == 0)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            TrainingSignView model = new TrainingSignView();
            model.TrainingID = id;
            var trainingInfo = trainingRepository.FindBy(id);
            model.CreaterPersonId = trainingInfo == null ? "0000" : adminRepository.GetById(trainingInfo.Creater).Account;

            var IPQuery = internalPlanRepository.Query(trainingInfo.ID).OrderByDescending(q => q.ID).FirstOrDefault();
            model.HasExam = false;
            model.HasOpinion = false;
            if (IPQuery != null)
            {                
                var examData = qNRRepository.FindByTrainingId(trainingInfo.ID);
                if (IPQuery.Learning && examData != null)
                {
                    model.HasExam = true;
                }
                if (IPQuery.Response)
                {
                    model.HasOpinion = true;
                }
            }

            var data = Mapper.Map<List<Areas.Admin.ViewModels.TrainingPerson.TrainingPersonView>>(trainingPersonRepository.Query(null, "", id));
            model.SignedList = data.Where(q => q.SignStatus == SignedStatus).OrderBy(q => q.DeptID).ToList();
            foreach (var item in model.SignedList)
            {
                item.OpinionStatus = classOpinionRepository.Query(id, item.JobID).Count() > 0 ? "已填寫" : "尚未填寫";
            }
            model.UnSignedList = data.Where(q => q.SignStatus == UnSignedStatus).OrderBy(q => q.DeptID).ToList();
            foreach (var item in model.UnSignedList)
            {
                item.OpinionStatus = classOpinionRepository.Query(id, item.JobID).Count() > 0 ? "已填寫" : "尚未填寫";
            }
            model.OtherList = data.Where(q => q.SignStatus == LeaveStatus).OrderBy(q => q.DeptID).ToList();

            model.PersonResultList = new List<QNRPersonResultView>();
            var qnrData = qNRRepository.FindByTrainingId(id);
            var quizQuestionList = qNRQuestionRepository.Query(null, qnrData.ID);
            foreach (var item in quizQuestionList)
            {
                model.FullPoints += Convert.ToInt32(item.Points);
            }
            model.QuestionNum = quizQuestionList.Count();

            var personIdList = qNRResultRepository.Query(qnrData.ID).Select(q => q.PersonId).Distinct().ToList();
            foreach (var personId in personIdList)
            {
                var gradeData = new QNRPersonResultView();
                gradeData.TotalPoints = 0;
                var personQuizResult = qNRResultRepository.Query(qnrData.ID, 0, personId);
                if (personQuizResult.Count() > 0)
                {
                    var firstResult = personQuizResult.FirstOrDefault();
                    var personInfo = t8personRepository.FindByPersonId(personId);

                    gradeData.QNRID = qnrData.ID;
                    gradeData.PersonId = personId;
                    gradeData.PersonName = personInfo.Name;
                    gradeData.DeptId = personInfo.DeptId;
                    gradeData.DeptName = personInfo.DeptName;
                    gradeData.CreateDate = firstResult.CreateDate;

                    gradeData.TotalPoints = personQuizResult.Select(q => q.CorrectPoints).Sum();
                    gradeData.CorrectNum = personQuizResult.Where(q => q.CorrectPoints > 0).Count();

                    model.PersonResultList.Add(gradeData);
                }
            }

            return View(model);
        }

        public ActionResult QuizStart(int id = 0)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth", new { returnUrl = $"/Sign/QuizStart/{id}"});
            }
            if (id == 0)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            QNRView model = new QNRView();
            model = Mapper.Map<QNRView>(qNRRepository.FindByTrainingId(id));
            if (model == null)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            model.ExamineeList = trainingPersonRepository.Query(null, "", id).Select(q => q.JobID).ToList();
            //var memberInfo = MemberInfoHelper.GetMemberInfo();
            //var resultExist = qNRResultRepository.Query(model.ID, 0, memberInfo.Account).Count() > 0;

            //if (resultExist)
            //{
            //    return RedirectToAction("QuizResult", "Sign", new { id = id });
            //}

            //return RedirectToAction("Quiz", new { id = id});
            return View(model);
        }

        /// <summary>
        /// 填寫試卷
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Quiz(int id = 0)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth", new { returnUrl = $"/Sign/Quiz/{id}"});
            }
            if (id == 0)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            //var trainingInfo = trainingRepository.GetById(id);
            QNRView model = new QNRView();
            model = Mapper.Map<QNRView>(qNRRepository.FindByTrainingId(id));
            if (model == null)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            var resultExist = qNRResultRepository.Query(model.ID, 0, memberInfo.Account).Count() > 0;

            if (resultExist)
            {
                return RedirectToAction("QuizResult", "Sign", new { id = id });
            }
            model.ExamineeList = trainingPersonRepository.Query(null, "", id).Select(q => q.JobID).ToList();
            model.QuizQuestionList = Mapper.Map<List<Areas.Admin.ViewModels.QNR.QuizQuestionView>>(qNRQuestionRepository.Query(null, model.ID));
            foreach (var item in model.QuizQuestionList)
            {
                item.OptionsList = new List<SelectListItem>();
                if (item != null && !string.IsNullOrEmpty(item.Options))
                {
                    var optionsArr = item.Options.Split('}');
                    for (int i = 0; i < optionsArr.Count(); i++)
                    {
                        var opt = optionsArr[i];
                        if (!string.IsNullOrEmpty(opt))
                        {
                            item.OptionsList.Add(new SelectListItem()
                            {
                                Text = opt.Replace("{", "").Replace("@@", ""),
                                Value = (i + 1).ToString(),
                            });
                        }
                    }
                }
            }

            return View(model);
        }

        /// <summary>
        /// 試卷結果
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult QuizResult(int id = 0, string personId = "")
        {
            if (id == 0)
            {
                return RedirectToAction("TrainingSign", "Sign");
            }
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            QNRView model = new QNRView();
            model = Mapper.Map<QNRView>(qNRRepository.FindByTrainingId(id));
            model.QuizQuestionList = Mapper.Map<List<Areas.Admin.ViewModels.QNR.QuizQuestionView>>(qNRQuestionRepository.Query(null, model.ID));
            model.TotalPoints = 0;
            foreach (var item in model.QuizQuestionList)
            {
                item.Answer = "0";
                if (item != null && !string.IsNullOrEmpty(item.Options))
                {
                    item.OptionsList = new List<SelectListItem>();
                    var optionsArr = item.Options.Split('}');
                    for (int i = 0; i < optionsArr.Length; i++)
                    {
                        var opt = optionsArr[i];
                        //取得正確答案和此題是否正確
                        if (optionsArr[i].Contains("@@"))
                        {
                            item.Answer = (i + 1).ToString();
                        }

                        if (!string.IsNullOrEmpty(opt))
                        {
                            item.OptionsList.Add(new SelectListItem()
                            {
                                Text = opt.Replace("{", "").Replace("@@", ""),
                                Value = (i + 1).ToString(),
                            });
                        }
                        //item.OptionsResultList.Add(resultData);
                    }
                    string account = memberInfo.Account;
                    if (!string.IsNullOrEmpty(personId))
                    {
                        account = personId;
                    }
                    var resultData = qNRResultRepository.Query(model.ID, item.ID, account).FirstOrDefault();
                    item.IsCorrect = item.Answer == resultData.Answer;
                    if (item.IsCorrect)
                    {
                        model.TotalPoints += item.Points;
                    }
                    item.ChooseAnswer = resultData.Answer;
                }
            }
            model.FullPoints = model.QuizQuestionList.Select(q => q.Points).ToList().Sum();
            model.CorrectNum = model.QuizQuestionList.Where(q => q.IsCorrect).Count();

            return View(model);
        }

        public JsonResult GetSignLog(int mid)
        {
            var query = signInRepository.Query(true, "", mid).OrderByDescending(q => q.SignTime);
            List<Areas.Admin.ViewModels.SignIn.SignInView> data = Mapper.Map<List<Areas.Admin.ViewModels.SignIn.SignInView>>(query);
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(data.ToList());
        }

        public JsonResult GetQRCodeUrl(string rn)
        {
            SignInView model = new SignInView();

            //var latestMeeting = meetingRepository.Query("", today, Convert.ToDateTime(today.ToShortDateString() + " 23:59:59"), 0, 0, 0, 0).FirstOrDefault();
            model.RoomNumber = rn;
            model.MeetingInfo = Mapper.Map<MeetingView>(meetingRepository.GetMeetByNum(rn));
            var url = Request.Url;
            model.QRCodeUrl = model.MeetingInfo == null ? "" : url.Scheme + "://" + url.Authority + "/Sign/Check?rn=" + rn + "&mid=" + model.MeetingInfo.ID;
            //result格式未處理，須為 {"XXX":[{"label":"A", "value":1},{"label":"B","value":2},{"label":"C","value":3}]} 

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult QrcodeSignIn(string signCode)
        {
            ResultView model = new ResultView();
            if (string.IsNullOrEmpty(signCode) || signCode.IndexOf("MailSignIn") > 0)
            {
                model.ResultStr = "驗證碼錯誤。";
            }
            var signInInfo = signInRepository.FindBySignCode(signCode);           
            if (signInInfo != null)
            {
                try
                {
                    string a = "https://localhost:44305/Sign/QrcodeSignIn?signCode=c39ce3a1-0e4d-411e-a02f-29d8c85c9fb5";

                    var signInfo = signInRepository.GetSignInInfoList(signInInfo.MeetingID, "", signCode).FirstOrDefault();

                    var meetingInfo = meetingRepository.FindByMeetingID(signInInfo.MeetingID);
                    model.ResultStr = SignInValidate(signInInfo.MeetingID, signInInfo.JobID);
                    model.RoomNumber = meetingInfo.RoomNumber;
                    model.RoomStr = meetingInfo.RoomStr;
                    model.MeetingID = signInInfo.MeetingID;
                    model.MeetingTitle = meetingInfo.Title;
                    model.JobID = signInInfo.JobID;
                    model.Name = signInfo.Name + " " + signInfo.EngName;
                    model.SignTime = Convert.ToDateTime(signInInfo.SignTime).ToString("yyyy/MM/dd HH:mm");
                    model.Depqrtment = signInfo.DepartmentTitle;
                }
                catch (Exception e)
                {

                    //throw;
                }
            }
            else
            {
                model.ResultStr = EnumHelper.GetDescription(SignInResult.NotFound);
            }

            //var resultData = signInRepository.FindBySignCode(signCode);

            //string signInTime = resultData.SignTime == null ? "" : resultData.SignTime.ToString();
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public string SaveQuizResults(string jsonData)
        {
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    var datas = JsonConvert.DeserializeObject<QNRResultListModel>(jsonData);
                    var resultList = datas.datas;
                    if (resultList.Count() > 0)
                    {
                        var firstData = resultList.FirstOrDefault();
                        if (qNRResultRepository.Query(firstData.QNRID, firstData.QNRQuestionID, firstData.PersonId).Count() <= 0)
                        {
                            foreach (var item in resultList)
                            {
                                item.DeptId = "0001";
                                item.CreateDate = DateTime.Now;
                                item.Version = 1;
                                item.AnswerType = 4;

                                //計算得分
                                var question = qNRQuestionRepository.GetById(item.QNRQuestionID);
                                item.CorrectPoints = 0;
                                if (question != null && !string.IsNullOrEmpty(question.Options))
                                {
                                    int index = -1;
                                    var correctAnswer = question.Options.Split('}');
                                    for (int i = 0; i < correctAnswer.Length; i++)
                                    {
                                        if (correctAnswer[i].Contains("@@"))
                                        {
                                            index = i;
                                            break;
                                        }
                                    }
                                    var correctAnswerValue = (index + 1).ToString();
                                    if (item.Answer == correctAnswerValue)
                                    {
                                        item.CorrectPoints = Convert.ToInt32(question.Points);
                                    }
                                }

                                qNRResultRepository.Insert(item);
                            }
                        }
                    }
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public StringBuilder ValidateCode()
        {
            Random random = new Random();
            // 英數
            string letters = "0123456789";
            string letter;
            StringBuilder sb = new StringBuilder();

            // 加入隨機10個數字
            for (int word = 0; word < 6; word++)
            {
                letter = letters.Substring(random.Next(0, letters.Length - 1), 1);
                sb.Append(letter);
            }
            return sb;
        }
    }
}