﻿using MeetSignIn.ActionFilters;
using MeetSignIn.Models;
using MeetSignIn.Models.Others;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.ViewModels.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Controllers
{
    [ErrorHandleActionFilter]
    public class AuthController : Controller
    {
        public AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private comPersonRepository comPersonRepository = new comPersonRepository(new T8ERPEntities());
        // GET: Auth
        public ActionResult Login(string returnUrl = "")
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo != null)
            {
                var controller = DependencyResolver.Current.GetService<AuthController>();
                controller.ControllerContext = new ControllerContext(this.Request.RequestContext, controller);
                var loginInfo = adminRepository.GetById(memberInfo.ID);
                //if (loginInfo != null && loginInfo.Type <= (int)MeetAdminType.Manager)
                if (loginInfo != null && loginInfo.Type <= (int)AdminType.ClassSupervisor)
                {
                    return controller.Login(new LoginView() { Account = loginInfo.Account, Password = loginInfo.Password });
                }
            }

            var login = new LoginView();
            login.ReturnUrl = returnUrl;
            return View(login);
        }

        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginView login)
        {
            if (ModelState.IsValid)
            {
                login.RememberMe = true;
                var admin = adminRepository.Login(login.Account, login.Password);
                //if (admin != null && admin.Type <= (int)MeetAdminType.Manager)
                if (admin != null && admin.Type <= (int)AdminType.Employee)
                {
                    var personInfo = comPersonRepository.FindByPersonID(admin.Account);
                    if (personInfo == null)
                    {
                        return View(login);
                    }
                    MemberInfo memberInfo = new MemberInfo();
                    memberInfo.ID = admin.ID;
                    memberInfo.Account = admin.Account;
                    memberInfo.Type = admin.Type;
                    memberInfo.Name = personInfo.PersonName;
                    memberInfo.DeptId = personInfo.DeptId;
                    MemberInfoHelper.Login(memberInfo, true);

                    //return RedirectToAction("PFIndex", "PF");
                    if (!string.IsNullOrEmpty(login.ReturnUrl))
                    {
                        return Redirect(login.ReturnUrl);
                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(login);
        }

        public ActionResult Logout(string returnUrl = "")
        {
            MemberInfoHelper.Logout();
            return RedirectToAction("Login", new { returnUrl = returnUrl });
        }
    }
}