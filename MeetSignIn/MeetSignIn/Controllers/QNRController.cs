﻿using AutoMapper;
using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.ViewModels.QNR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Controllers
{
    public class QNRController : BaseController
    {
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private QNRRepository qNRRepository = new QNRRepository(new MeetingSignInDBEntities());
        private QNRQuestionRepository qNRQuestionRepository = new QNRQuestionRepository(new MeetingSignInDBEntities());
        private QNRResultRepository qNRResultRepository = new QNRResultRepository(new MeetingSignInDBEntities());
        private OptionGroupRepository optionGroupRepository = new OptionGroupRepository(new MeetingSignInDBEntities());
        private OptionItemRepository optionItemRepository = new OptionItemRepository(new MeetingSignInDBEntities());

        // GET: QNR
        public ActionResult Index()
        {
            var model = new ViewModels.QNR.QNRIndexView();
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            model.QNRList = Mapper.Map<List<QNRView>>(qNRRepository.Query(true));

            return View(model);
        }

        public ActionResult Form(int qid = 0)
        {
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth");
            }
            if (qid == 0)
            {
                return RedirectToAction("Index");
            }
            var model = new QNRView();
            var query = qNRRepository.FindBy(qid);
            model = Mapper.Map<QNRView>(query);
            model.QuestionList = Mapper.Map<List<QNRQuestionView>>(qNRQuestionRepository.Query(null, qid));
            foreach (var item in model.QuestionList)
            {
                //var linkQuestionInfo = qNRQuestionRepository.FindBy(item.ID, );
                if (item.OptionGroup != 0)
                {
                    item.OptionList = optionItemRepository.Query(true, item.OptionGroup).ToList();
                }
            }

            return View(model);
        }

        //[HttpPost]
        //public ActionResult Form(QNRView model)
        //{


        //    return View(model);
        //}

        public string SaveQNRResults(string jsonData)
        {
            try
            {
                if (!string.IsNullOrEmpty(jsonData))
                {
                    var datas = JsonConvert.DeserializeObject<QNRResultListModel>(jsonData);
                    var resultList = datas.datas;
                    if (resultList.Count() > 0)
                    {
                        var firstData = resultList.FirstOrDefault();
                        var newVersion = qNRResultRepository.Query(firstData.QNRID, firstData.QNRQuestionID, firstData.PersonId).Count() + 1;
                        foreach (var item in resultList)
                        {
                            item.DeptId = "0001";
                            item.CreateDate = DateTime.Now;
                            item.Version = newVersion;

                            qNRResultRepository.Insert(item);
                        }
                    }
                }

                return "success";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}