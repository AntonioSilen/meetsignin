﻿using AutoMapper;
using MeetSignIn.ActionFilters;
using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Repositories.T8Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.ViewModels.PF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Controllers
{
    //[ErrorHandleActionFilter]
    public class PFController : BaseController
    {
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private PFSeasonRepository pFSeasonRepository = new PFSeasonRepository(new MeetingSignInDBEntities());
        private PFCategoryRepository pFCategoryRepository = new PFCategoryRepository(new MeetingSignInDBEntities());
        private PFWorksRepository pFWorksRepository = new PFWorksRepository(new MeetingSignInDBEntities());
        private PFWorkRepository pFWorkRepository = new PFWorkRepository(new MeetingSignInDBEntities());
        private PFAppraiseRepository pFAppraiseRepository = new PFAppraiseRepository(new MeetingSignInDBEntities());
        private PFAppraiseWorksRepository pFAppraiseWorksRepository = new PFAppraiseWorksRepository(new MeetingSignInDBEntities());
        private PFWorksRelationRepository pFWorksRelationRepository = new PFWorksRelationRepository(new MeetingSignInDBEntities());
        private PFAppraiseRelationRepository pFAppraiseRelationRepository = new PFAppraiseRelationRepository(new MeetingSignInDBEntities());
        private comDepartmentRepository comDepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
        private comPersonRepository comPersonRepository = new comPersonRepository(new T8ERPEntities());

        public static PFHelper pFHelper = new PFHelper();

        public int PF = (int)AuditType.PF;

        public string CLS = EnumHelper.GetDescription(LevelType.CLS);
        public string CATE = EnumHelper.GetDescription(LevelType.CATE);
        public string ITM = EnumHelper.GetDescription(LevelType.ITM);
        public string LVL = EnumHelper.GetDescription(LevelType.LVL);
        public string WORK = EnumHelper.GetDescription(LevelType.WORK);

        public ActionResult SendAudit(int aid, int awid)
        {
            var appraiseData = pFAppraiseRepository.GetById(aid);
            var appraiseWorksData = pFAppraiseWorksRepository.GetById(awid);
            try
            {
                var seasonInfo = pFHelper.GetActivatePFSeason(appraiseData.DeptId);
                appraiseWorksData.IsAudit = true;
                pFAppraiseWorksRepository.Update(appraiseWorksData);
                appraiseData = pFAppraiseRepository.GetById(aid);
                var relationData = pFAppraiseRelationRepository.Query(aid).OrderByDescending(q => q.ID).FirstOrDefault();
                var auditData = auditRepository.FindByTarget(appraiseData.ID, PF, seasonInfo.ID, appraiseData.PersonId);
                if (auditData == null)
                {//未送審
                    auditRepository.Insert(new Audit()
                    {
                        TrainingID = seasonInfo.ID,//Season ID
                        PersonID = appraiseData.PersonId,//Appraise ID
                        TargetID = appraiseData.ID,
                        Type = PF,
                        State = (int)PFAuditState.Processing,
                        IsInvalid = false,
                        Officer = adminRepository.FindByPersonId(appraiseData.PersonId).ID,
                        CreateDate = DateTime.Now
                    });
                }

                ShowMessage(true, "填寫資料已送出。");
                //返回表單總攬
                return RedirectToAction("PFFormOverview", new { DeptId = appraiseData.DeptId, ClassID = appraiseWorksData.ClassID, WorkID = appraiseWorksData.WorksID });
            }
            catch (Exception e)
            {//送審失敗
                ShowMessage(false, e.Message);
                var relationData = pFAppraiseWorksRepository.Query(aid).OrderByDescending(q => q.ID).FirstOrDefault();
                return RedirectToAction("PFFormOverview", new { DeptId = appraiseData.DeptId, ClassID = appraiseWorksData.ClassID, WorkID = relationData.WorksID });
            }
        }

        // GET: PF
        public ActionResult PFIndex()
        {
            var personInfo = MemberInfoHelper.GetMemberInfo();
            if (personInfo == null)
            {
                ShowMessage(false, "查無人員資料 / ไม่พบพนักงาน");
                return RedirectToAction("Logout", "Auth");
            }
            ClassView model = new ClassView();
            return View(model);
        }

        [HttpPost]
        public ActionResult PFIndex(ClassView model)
        {
            if (ModelState.IsValid)
            {
                var memberInfo = MemberInfoHelper.GetMemberInfo();
                if (memberInfo == null)
                {
                    return RedirectToAction("Logout", "Auth");
                }
                var personDept = comPersonRepository.FindByPersonID(memberInfo.Account);

                LanguageHelper.SetLang(model.Language);

                //暫時
                //return RedirectToAction("PFWorks", new { DeptId  = "G20"});
                //正式
                if (personDept.Lv >= 4)
                {
                    return RedirectToAction("PFWorks", new { DeptId = personDept.LvFourDeptID });
                }
                else
                {
                    return RedirectToAction("PFWorks", new { DeptId = personDept.DeptId });
                }

                //return RedirectToAction("PFWorks", new { DeptId  = model.DeptId, ClassID = model.ClassID, ItemID = 0});
            }

            return View(model);
        }

        public ActionResult PFWorks(WorksView model)
        {
            if (string.IsNullOrEmpty(model.DeptId)/* || model.ClassID == 0*/)
            {
                return RedirectToAction("PFIndex");
            }
            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;

            var seasonInfo = pFHelper.GetActivatePFSeason(model.DeptId);
            if (seasonInfo == null)
            {
                ShowMessage(false, "查無評比季度活動 / ไม่มีกิจกรรมการประเมินรายไตรมาส");
                return RedirectToAction("PFIndex");
            }
            model.SeasonTitle = seasonInfo.Title;
            model.ThaiSeasonTitle = seasonInfo.ThaiTitle;

            var personInfo = MemberInfoHelper.GetMemberInfo();
            if (personInfo == null)
            {
                ShowMessage(false, "查無人員資料 / ไม่พบพนักงาน");
                return RedirectToAction("Logout", "Auth");
            }

            model.ClassList = Mapper.Map<List<PFCategoryClsView>>(pFCategoryRepository.Query(true, model.DeptId, 0, CLS));
            var appraiseData = pFAppraiseRepository.GetFirstData(personInfo.Account, seasonInfo.ID, model.DeptId);
            List<int> editedWorkIdList = new List<int>();
            if (appraiseData != null)
            {
                editedWorkIdList = pFAppraiseWorksRepository.Query(appraiseData.ID).Select(q => q.WorksID).ToList();
            }
           
            foreach (var cls in model.ClassList)
            {
                cls.WorkList = Mapper.Map<List<WorksView>>(pFWorkRepository.Query(null, cls.ID, model.DeptId).ToList());
                //已編輯
                var editedList = cls.WorkList.Where(q => editedWorkIdList.Contains(q.ID)).ToList();
                foreach (var work in editedList)
                {
                    work.IsEdited = true;
                }

                if (editedList.Count() > 0)
                {
                    cls.IsEdited = true;
                }              
            }

            ViewBag.Language = LanguageHelper.GetLang();

            return View(model);
        }

        public ActionResult PFFormOverview(FormOverview model)
        {
            if (string.IsNullOrEmpty(model.DeptId) || model.ClassID == 0 || model.WorkID == 0)
            {
                return RedirectToAction("PFIndex");
            }
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth");
            }
            model.PersonId = memberInfo.Account;
            var personInfo = comPersonRepository.FindByPersonID(model.PersonId);
            model.PersonName = personInfo.PersonName;

            var seasonInfo = pFHelper.GetActivatePFSeason(model.DeptId);
            if (seasonInfo == null)
            {
                ShowMessage(false, "查無評比季度活動 / ไม่มีกิจกรรมการประเมินรายไตรมาส");
                return RedirectToAction("PFIndex");
            }
            model.SeasonTitle = seasonInfo.Title;
            model.ThaiSeasonTitle = seasonInfo.ThaiTitle;

            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;

            var workData = pFWorkRepository.GetById(model.WorkID);
            if (workData == null)
            {
                return RedirectToAction("PFWorks", new { DeptId = model.DeptId, ClassID = model.ClassID, ItemID = 0 });
            }
            model.WorkTitle = workData.Title;
            model.ThaiWorkTitle = workData.ThaiTitle;

            var appraiseData = pFAppraiseRepository.GetFirstData(model.PersonId/*, 0*/, seasonInfo.ID, model.DeptId);
            if (appraiseData == null)
            {
                var clsData = pFCategoryRepository.GetById(model.ClassID);
                appraiseData = new PFAppraise()
                {
                    PersonId = model.PersonId,
                    DeptId = model.DeptId,
                    SeasonTitle = seasonInfo == null ? "" : seasonInfo.Title,
                    SeasonID = seasonInfo == null ? 0 : seasonInfo.ID,
                    DeptName = deptInfo == null ? "" : deptInfo.DeptName,
                    CreateDate = DateTime.Now,
                };
                appraiseData.ID = pFAppraiseRepository.Insert(appraiseData);
            }
            model.AppraiseID = appraiseData.ID;          

            var appraiseWorkData = pFAppraiseWorksRepository.GetFirstData(appraiseData.ID, model.WorkID, model.PersonId, model.ClassID, model.DeptId);
            if (appraiseWorkData == null)
            {
                var clsData = pFCategoryRepository.GetById(model.ClassID);               
                appraiseWorkData = new PFAppraiseWorks()
                {
                    AppraiseID = appraiseData.ID,
                    PersonId = model.PersonId,
                    DeptId = deptInfo.DeptId,
                    SeasonTitle = seasonInfo == null ? "" : seasonInfo.Title,
                    SeasonID = seasonInfo == null ? 0 : seasonInfo.ID,
                    DeptName = deptInfo == null ? "" : deptInfo.DeptName,
                    ClassTitle = clsData == null ? "" : clsData.Title,
                    ClassID = clsData == null ? 0 : clsData.ID,
                    WorksID = workData == null ? 0 : workData.ID,
                    WorkTitle = workData == null ? "" : workData.Title,
                    TLAudit = AuditProcessing,
                    CSAudit = AuditProcessing,
                    ManagerAudit = AuditProcessing,
                    IsFinished = false,
                    IsAudit = false,
                    CreateDate = DateTime.Now,
                };
                appraiseWorkData.ID = pFAppraiseWorksRepository.Insert(appraiseWorkData);
            }

            model.IsFinished = appraiseWorkData.IsFinished;

            if (!appraiseWorkData.IsFinished)
            {
                var allItmCounts = pFCategoryRepository.GetItmsQuery(model.DeptId, model.ClassID, 0, 2).Count();
                var allappraiseRelationCounts = pFAppraiseRelationRepository.Query(appraiseData.ID, appraiseWorkData.ID).Count();
                if (allItmCounts == allappraiseRelationCounts)
                {
                    appraiseWorkData.IsFinished = true;
                    pFAppraiseWorksRepository.Update(appraiseWorkData);
                    model.IsFinished = true;
                }
            }

            model.AppraiseWorksID = appraiseWorkData.ID;           
            model.IsAudit = appraiseWorkData.IsAudit;

            model.CateList = Mapper.Map<List<PFCategoryCateView>>(pFCategoryRepository.Query(true, model.DeptId, model.ClassID, CATE));
            foreach (var cate in model.CateList)
            {
                cate.ItmList = Mapper.Map<List<PFCategoryItmView>>(pFCategoryRepository.Query(true, model.DeptId, cate.ID, ITM, null , 2));
                foreach (var item in cate.ItmList)
                {
                    var relationInfo = pFAppraiseRelationRepository.Query(appraiseData.ID, appraiseWorkData.ID, item.ID, 0, model.WorkID).FirstOrDefault();
                    if (relationInfo != null)
                    {
                        var lvlInfo = pFCategoryRepository.GetById(relationInfo.LvlID);
                        item.LvlID = lvlInfo == null ? 0 : lvlInfo.ID;
                        item.LvlTitle = lvlInfo == null ? "" : lvlInfo.Title;
                        item.ThaiLvlTitle = lvlInfo == null ? "" : lvlInfo.ThaiTitle;
                    }

                }
            }

            ViewBag.Language = LanguageHelper.GetLang();

            return View(model);
        }

        public ActionResult PFForm(string DeptId = "", int ClassID = 0, int ItmID = 0, int WorkID = 0)
        {
            //PF/PFForm?DeptId=G20&ClassID=2&ItmID=228&WorkID=1
            FormView model = new FormView();
            var memberInfo = MemberInfoHelper.GetMemberInfo();
            if (memberInfo == null)
            {
                return RedirectToAction("Logout", "Auth");
            }
            //取得啟用中季度
            var activateSeason = pFHelper.GetActivatePFSeason(DeptId);
            if (ClassID == 0 || string.IsNullOrEmpty(DeptId) || activateSeason == null)
            {
                return RedirectToAction("PFIndex", "PF");
            }
            model.DeptId = DeptId;            
            model.ClassID = ClassID;
            model.ItemID = ItmID;
            //取得指定班別所有填寫選項
            var allItmList = pFCategoryRepository.GetItmsQuery(DeptId, ClassID, 0, 2).ToArray();
            if (ItmID == 0)
            {//第一個問題
                var thisItm = allItmList.FirstOrDefault();
                model = GetFormViewData(model, thisItm);//當前(第一筆)資料
                if (allItmList.Count() > 1)
                {
                    model.NextModel = GetFormViewData(model.NextModel, thisItm);//下一筆資料
                }              
            }
            else
            {
                var thisItm = allItmList.Where(q => q.ID == ItmID).FirstOrDefault();
                model = Mapper.Map<FormView>(thisItm);
                model = GetFormViewData(model, thisItm);//當前資料
                var thisIndex = Array.IndexOf(allItmList, thisItm);
                if (thisIndex > 0)
                {
                    var lastModel = allItmList[thisIndex - 1];
                    model.LastModel = GetFormViewData(model.LastModel, lastModel);//上一筆資料
                }
             
                if (thisIndex < (allItmList.Count() - 1))
                {
                    model.NextModel = GetFormViewData(model.NextModel, allItmList[thisIndex + 1]);//下一筆資料
                }
            }

            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            model.DeptName = deptInfo == null ? "" : deptInfo.DeptName;

            model.PersonId = memberInfo.Account;
            var personInfo = comPersonRepository.FindByPersonID(model.PersonId);
            model.PersonName = personInfo.PersonName;

            model.SeasonID = activateSeason.ID;

            var itmInfo = pFCategoryRepository.GetById(model.ItemID);
            model.ItmTitle = itmInfo == null ? "" : itmInfo.Title;
            model.ThaiItmTitle = itmInfo == null ? "" : itmInfo.ThaiTitle;

            model.WorkID = WorkID;
            var workData = pFWorkRepository.GetById(model.WorkID);
            model.WorkTitle = workData == null ? "" : workData.Title;
            model.ThaiWorkTitle = workData == null ? "" : workData.ThaiTitle;

            var seasonInfo = pFHelper.GetActivatePFSeason(model.DeptId);
            if (seasonInfo == null)
            {
                ShowMessage(false, "查無評比季度活動 / ไม่มีกิจกรรมการประเมินรายไตรมาส");
                return RedirectToAction("PFIndex");
            }
            var appraiseData = pFAppraiseRepository.GetFirstData(model.PersonId/*, model.ClassID*/, seasonInfo.ID);
            if (appraiseData == null)
            {
                var clsData = pFCategoryRepository.GetById(model.ClassID);
                appraiseData = new PFAppraise()
                {
                    PersonId = model.PersonId,
                    DeptId = model.DeptId,
                    SeasonTitle = seasonInfo == null ? "" : seasonInfo.Title,
                    SeasonID = seasonInfo == null ? 0 : seasonInfo.ID,
                    DeptName = deptInfo == null ? "" : deptInfo.DeptName,
                    TLAudit = AuditProcessing,
                    CSAudit = AuditProcessing,
                    ManagerAudit = AuditProcessing,
                    CreateDate = DateTime.Now,
                };
                appraiseData.ID = pFAppraiseRepository.Insert(appraiseData);
            }
            model.AppraiseID = appraiseData.ID;
            var appraiseRelationInfo = pFAppraiseRelationRepository.GetFirstData(appraiseData.ID, model.ItemID, 0, model.WorkID);
            if (appraiseRelationInfo != null)
            {
                model.LvlID = appraiseRelationInfo.LvlID;
            }

            model.LvlList = pFCategoryRepository.Query(true, model.DeptId, model.ItemID, LVL).ToList();

            ViewBag.Language = LanguageHelper.GetLang();

            return View(model);
        }

        private static FormView GetFormViewData(FormView thisModel, PFItmsModel thisData)
        {
            thisModel = Mapper.Map<FormView>(thisData);
            thisModel.ItemID = thisData.ID;
            thisModel.ClassID = thisData.ClassID;
            return thisModel;
        }

        [HttpPost]
        public ActionResult PFForm(FormView model)
        {
            var activateSeason = pFHelper.GetActivatePFSeason(model.DeptId);
            #region 取得資訊
            var seasonInfo = pFHelper.GetActivatePFSeason(model.DeptId);
            if (seasonInfo == null)
            {
                ShowMessage(false, "查無評比季度活動 / ไม่มีกิจกรรมการประเมินรายไตรมาส");
                return RedirectToAction("PFIndex");
            }
            var deptInfo = comDepartmentRepository.FindByDeptId(model.DeptId);
            var clsData = pFCategoryRepository.GetById(model.ClassID);
            var cateData = pFCategoryRepository.GetById(model.CateID);
            var itmData = pFCategoryRepository.GetById(model.ItemID);
            var lvlData = pFCategoryRepository.GetById(model.LvlID);
            var workData = pFWorkRepository.GetById(model.WorkID);
            if (workData == null)
            {
                model.LvlList = pFCategoryRepository.Query(true, model.DeptId, model.ItemID, LVL).ToList();
                return View(model);
            }
            #endregion
            //取得評比資料
            var appraiseData = pFAppraiseRepository.GetFirstData(model.PersonId/*, model.ClassID*/, seasonInfo.ID);
            if (appraiseData == null)
            {
                appraiseData = new PFAppraise()
                {
                    PersonId = model.PersonId,
                    DeptId = deptInfo.DeptId,
                    SeasonTitle = seasonInfo == null ? "" : seasonInfo.Title,
                    SeasonID = seasonInfo == null ? 0 : seasonInfo.ID,
                    DeptName = deptInfo == null ? "" : deptInfo.DeptName,
                    TLAudit = AuditProcessing,
                    CSAudit = AuditProcessing,
                    ManagerAudit = AuditProcessing,
                    CreateDate = DateTime.Now,
                };
                appraiseData.ID = pFAppraiseRepository.Insert(appraiseData);
            }

            //取得評比作業
            PFAppraiseWorks appraiseWorkData = pFAppraiseWorksRepository.GetFirstData(appraiseData.ID, model.WorkID, appraiseData.PersonId, model.ClassID, appraiseData.DeptId);
            if (appraiseWorkData == null)
            {
                appraiseWorkData = new PFAppraiseWorks()
                {
                    AppraiseID = appraiseData.ID,
                    PersonId = model.PersonId,
                    DeptId = deptInfo.DeptId,
                    SeasonTitle = seasonInfo == null ? "" : seasonInfo.Title,
                    SeasonID = seasonInfo == null ? 0 : seasonInfo.ID,
                    DeptName = deptInfo == null ? "" : deptInfo.DeptName,
                    ClassTitle = clsData == null ? "" : clsData.Title,
                    ClassID = clsData == null ? 0 : clsData.ID,
                    WorksID = workData == null ? 0 : workData.ID,
                    WorkTitle = workData == null ? "" : workData.Title,
                    TLAudit = AuditProcessing,
                    CSAudit = AuditProcessing,
                    ManagerAudit = AuditProcessing,
                    IsFinished = false,
                    IsAudit = false,
                    CreateDate = DateTime.Now,
                };
                appraiseWorkData.ID = pFAppraiseWorksRepository.Insert(appraiseWorkData);
            }

            //更新評比分數
            PFAppraiseRelation data = pFAppraiseRelationRepository.GetFirstData(appraiseData.ID, model.ItemID, 0, model.WorkID);

            int id = 0;
            if (data == null)
            {
                data = new PFAppraiseRelation();
                data.AppraiseID = appraiseData == null ? 0 : appraiseData.ID;
                data.AppraiseWorksID = appraiseWorkData == null ? 0 : appraiseWorkData.ID;
                data.CateID = cateData == null ? 0 : cateData.ID;
                data.CateTitle = cateData == null ? "" : cateData.Title;
                data.ItmID = itmData == null ? 0 : itmData.ID;
                data.ItmTitle = itmData == null ? "" : itmData.Title;
                data.LvlID = lvlData == null ? 0 : lvlData.ID;
                data.LvlTitle = lvlData == null ? "" : lvlData.Title;
                data.WorkTitle = workData == null ? "" : workData.Title;
                data.WorksID = workData == null ? 0 : workData.ID;
                data.Points = lvlData == null ? 0 : lvlData.Points;
                data.CreateDate = DateTime.Now;

                id = pFAppraiseRelationRepository.Insert(data);
            }
            else
            {
                data.ItmID = model.ItemID;
                data.LvlID = model.LvlID;
                data.WorkTitle = workData == null ? "" : workData.Title;
                data.WorksID = workData == null ? 0 : workData.ID;
                data.Points = lvlData == null ? 0 : lvlData.Points;
                data.CreateDate = DateTime.Now;

                pFAppraiseRelationRepository.Update(data);
                id = data.ID;
            }

            #region 取得下一個評比項目
            var nextItemId = 0;
            var allItmList = pFCategoryRepository.GetItmsQuery(model.DeptId, model.ClassID, 0, 2).ToArray();
            var thisIndex = Array.IndexOf(allItmList, allItmList.Where(q => q.ID == model.ItemID).FirstOrDefault());
            if (thisIndex + 1 >= allItmList.Count())
            {//填寫完成
                appraiseData = pFAppraiseRepository.GetById(appraiseData.ID);
                var appraiseWorksData = pFAppraiseWorksRepository.GetById(appraiseWorkData.ID);
                appraiseWorksData.IsFinished = true;
                pFAppraiseWorksRepository.Update(appraiseWorksData);
                return RedirectToAction("PFFormOverview", "PF", new { DeptId = model.DeptId, ClassID = model.ClassID, WorkID = model.WorkID });
            }
            nextItemId = allItmList.Skip(thisIndex+1).FirstOrDefault().ID;

            //var thisItem = pFCategoryRepository.GetById(model.ItemID);
            ////此班別績效表單項目
            //nextItemId = GetNextItm(model, nextItemId, thisItem);
            //if (model.isLast == true)
            //{
            //    //此班別績效表單分類
            //    var cateArr = pFCategoryRepository.Query(true, model.DeptId, model.ClassID, CATE).OrderBy(q => q.Sort).ToArray();
            //    var cateInfo = pFCategoryRepository.GetById(model.CateID);
            //    var cateIndex = Array.IndexOf(cateArr, cateInfo) + 1;
            //    if (cateArr.Count() > cateIndex)
            //    {
            //        model.CateID = cateArr.Skip(cateIndex).FirstOrDefault().ID;
            //        model.isLast = false;
            //        nextItemId = GetNextItm(model, nextItemId, thisItem);
            //    }
            //}
            #endregion

            return RedirectToAction("PFForm", new { DeptId = model.DeptId, ClassID = model.ClassID, ItmID = nextItemId, WorkID = model.WorkID });

            //return RedirectToAction("PFForm", new { DeptId = model.DeptId, ClassID = model.ClassID, CateID = model.CateID, ItemID = nextItemId, WorkTitle = model.WorkTitle, isFirst = false, isLast = model.isLast, lastAid = id });
        }

        private int GetNextItm(FormView model, int nextItemId, PFCategory thisItem)
        {
            var itmArr = pFCategoryRepository.Query(true, model.DeptId, model.CateID, ITM).OrderBy(q => q.Sort).ToArray();
            if (itmArr.Count() > 0)
            {
                int thisIndexCount = Array.IndexOf(itmArr, thisItem) + 1;
                if (itmArr.Count() > thisIndexCount)
                {
                    nextItemId = itmArr.Skip(thisIndexCount).FirstOrDefault().ID;
                }
                else if (itmArr.Count() == thisIndexCount)
                {
                    model.isLast = true;
                }
            }

            return nextItemId;
        }

        public JsonResult GetClass(string deptId)
        {
            var query = pFCategoryRepository.Query(null, deptId, 0, "班別");
            return Json(query, JsonRequestBehavior.AllowGet);
        }
    }
}