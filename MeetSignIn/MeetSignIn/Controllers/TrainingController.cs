﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.Controllers
{
    public class TrainingController : Controller
    {
        private TrainingRepository trainingRepository;
        private Repositories.T8Repositories.DepartmentRepository t8departmentRepository;
        private Repositories.T8Repositories.PersonRepository t8personRepository;
        private TrainingPersonRepository trainingPersonRepository;

        public TrainingController() : this(null, null, null, null) { }

        public TrainingController(TrainingRepository repo, Repositories.T8Repositories.DepartmentRepository repo2, Repositories.T8Repositories.PersonRepository repo3, TrainingPersonRepository repo4)
        {
            trainingRepository = repo ?? new TrainingRepository(new MeetingSignInDBEntities());
            t8departmentRepository = repo2 ?? new Repositories.T8Repositories.DepartmentRepository(new T8ERPEntities());
            t8personRepository = repo3 ?? new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
            trainingPersonRepository = repo4 ?? new TrainingPersonRepository(new MeetingSignInDBEntities());
        }
        // GET: Training
        public ActionResult Index()
        {
            UploadList();
            return View();
        }

        //[HttpPost]
        public JsonResult UploadList()
        {
            //if (Request.Files.Count > 0)
            //{
            try
            {
                //HttpFileCollectionBase files = Request.Files;
                //for (int i = 0; i < files.Count; i++)
                //{
                //    // 另存檔案
                //    HttpPostedFileBase file = files[i];
                //    string fname;
                string fname = "C:/訓練紀錄1.xlsx";
                //file.SaveAs(fname);

                using (FileStream fs = new FileStream(fname, FileMode.Open, FileAccess.Read))
                {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    //載入Excel檔案
                    using (ExcelPackage ep = new ExcelPackage(fs))
                    {
                        ExcelWorksheet sheet = ep.Workbook.Worksheets[0];//取得Sheet1
                        //List<UserListRowData> RowData = new List<UserListRowData>();

                        bool isLastRow = false;
                        int RowId = 2;   // 因為有標題列，所以從第2列開始讀起
                        string lastTitle = "";
                        int lastId = 0;
                        do  // 讀取資料，直到讀到空白列為止
                        {
                            string cellTitle = sheet.Cells[RowId, 1].Text;
                            string cellDate = sheet.Cells[RowId, 3].Text;
                            var thisdateStart = Convert.ToDateTime(cellDate.ToString() + "00:00:01");
                            var thisdateEnd = Convert.ToDateTime(cellDate.ToString() + "23:23:59");
                            if (string.IsNullOrEmpty(cellTitle))
                            {
                                isLastRow = true;
                            }
                            else
                            {
                                if (trainingRepository.GetAll().Where(q => q.Title == cellTitle 
                                && q.StartDate >= thisdateStart 
                                && q.EndDate <= thisdateEnd).Count() > 0)
                                {
                                    continue;
                                }
                                DateTime now = DateTime.Now;
                                if (lastTitle != cellTitle)
                                {
                                    //新教育訓練
                                    Training tInfo = new Training();
                                    tInfo.StartDate = Convert.ToDateTime(cellDate + " 10:00:00");//上課日期
                                    tInfo.MeetingNumber = tInfo.StartDate.ToString("yyyyMmddHHmm");
                                    tInfo.Title = cellTitle;//課程名稱
                                    tInfo.Organiser = "人資部";
                                    tInfo.DeptID = GetDeptId(sheet.Cells[RowId, 8].Text);//部門
                                    tInfo.FactoryID = 1;
                                    Random myObject = new Random();
                                    tInfo.RoomID = myObject.Next(2, 6);
                                    tInfo.Category = GetCategory(sheet.Cells[RowId, 9].Text);//研訓類別
                                    tInfo.Theme = GetTheme(sheet.Cells[RowId, 5].Text);//訓練類別
                                    tInfo.Fee = string.IsNullOrEmpty(sheet.Cells[RowId, 12].Text) ? 0 : Convert.ToInt32(sheet.Cells[RowId, 12].Text);//費用
                                    tInfo.Cert = GetCert(sheet.Cells[RowId, 14].Text);//證書/證明
                                    tInfo.Lecturer = sheet.Cells[RowId, 13].Text;//講師
                                    tInfo.SignDate = tInfo.StartDate;
                                    tInfo.Hours = !string.IsNullOrEmpty(sheet.Cells[RowId, 7].Text) ? Convert.ToDecimal(sheet.Cells[RowId, 7].Text) : 2;//時數
                                    tInfo.EndDate = tInfo.SignDate.AddHours(Convert.ToDouble(tInfo.Hours));
                                    tInfo.Remark = sheet.Cells[RowId, 11].Text;//備註
                                    tInfo.IsSignable = true;
                                    tInfo.LateMinutes = 0;

                                    lastId = trainingRepository.Insert(tInfo);
                                    lastTitle = cellTitle;
                                }

                                string name = sheet.Cells[RowId, 4].Text;
                                var person = t8personRepository.GetPersonQuery(name).FirstOrDefault();
                                if (person == null)
                                {
                                    RowId++;
                                    continue;
                                }
                                var existTrainingPerson = trainingPersonRepository.Query(null, "", lastId, person.JobID);
                                if (existTrainingPerson.Count() <= 0)
                                {
                                    //新增訓練人員
                                    TrainingPerson tpInfo = new TrainingPerson();
                                    tpInfo.SignCode = Convert.ToString(Guid.NewGuid());
                                    tpInfo.TraningID = lastId;
                                    tpInfo.Name = name;//姓名
                                  
                                    tpInfo.JobID = person.JobID;
                                    tpInfo.IsSign = true;
                                    tpInfo.SignStatus = (int)SignStatus.Signed;
                                    tpInfo.IsSent = true;
                                    tpInfo.IsPassed = !string.IsNullOrEmpty(sheet.Cells[RowId, 10].Text);
                                    tpInfo.SignTime = now;
                                    trainingPersonRepository.Insert(tpInfo);
                                }

                                RowId++;
                            }
                        } while (!isLastRow);
                    }
                }
                //}
                return Json("File Uploaded Successfully!");
            }
            catch (Exception ex)
            {
                return Json("Error occurred. Error details: " + ex.Message);
            }
            //}
            //else
            //{
            //    return Json("No files selected.");
            //}
        }

        public string GetDeptId(string deptName)
        {
            var result = t8departmentRepository.Query(deptName);
            return result.Count() > 0 ? result.FirstOrDefault().DeptId : "0001";
        }
        public int GetCategory(string category)
        {
            var result = 1;
            switch (category)
            {
                case "內訓":
                    return 1;
                case "外訓":
                    return 2;
                case "研討會":
                    return 3;
                case "外聘講師":
                    return 4;
            }
            return result;
        }
        public int GetTheme(string theme)
        {
            var result = 7;
            switch (theme)
            {
                case "新人訓練":
                    return 1;
                case "工安衛訓練":
                    return 2;
                case "研發訓練":
                    return 3;
                case "品質訓練":
                    return 4;
                case "生產技能訓練":
                    return 5;
                case "營業訓練":
                    return 6;
                case "管理訓練":
                    return 7;
                case "人資訓練":
                    return 8;
                case "專業技能訓練":
                    return 9;
                case "ISO訓練":
                    return 10;
            }
            return result;
        }
        public int GetCert(string cert)
        {
            var result = 2;
            switch (cert)
            {
                case "有":
                    return 1;
                case "無":
                    return 2;
                case "上課證明":
                    return 3;
            }
            return result;
        }
    }
}