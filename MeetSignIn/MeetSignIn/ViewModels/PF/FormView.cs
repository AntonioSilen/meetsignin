﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.PF
{
    public class FormView
    {
        public FormView()
        {
            this.isFirst = true;
            this.isLast = false;
        }

        public bool isFirst { get; set; }
        public bool isLast { get; set; }

        [Display(Name = "評比表單編號")]
        public int AppraiseID { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "班別")]
        public int ClassID { get; set; }

        [Display(Name = "分類")]
        public int CateID { get; set; }

        [Display(Name = "評比項目")]
        public int ItemID { get; set; }

        [Display(Name = "層級")]
        public int LvlID { get; set; }

        [Display(Name = "作業項目")]
        public int WorkID { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }
        public string PersonName { get; set; }

        [Display(Name = "季度")]
        public int SeasonID { get; set; }
        public string SeasonTitle { get; set; }

        [Display(Name = "分類")]
        public string CateTitle { get; set; }

        [Display(Name = "評比項目")]
        public string ItmTitle { get; set; }
        public string ThaiItmTitle { get; set; }

        [Display(Name = "作業項目")]
        public string WorkTitle { get; set; }
        public string ThaiWorkTitle { get; set; }

        public List<PFCategory> ClsList { get; set; }
        public List<PFCategory> LvlList { get; set; }

        public FormView LastModel { get; set; }
        public FormView NextModel { get; set; }
    }
}