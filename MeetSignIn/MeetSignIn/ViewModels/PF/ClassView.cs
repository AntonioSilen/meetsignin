﻿using MeetSignIn.Models;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.ViewModels.PF
{
    public class ClassView
    {
        public string DeptId { get; set; }

        public string ClassID { get; set; }

        [Required]
        public string Language { get; set; }

        public List<SelectListItem> DeptOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                comDepartmentRepository comdepartmentRepository = new comDepartmentRepository(new T8ERPEntities());
                var lvList = new List<int>();
                lvList.Add(3);
                lvList.Add(4);
                var values = comdepartmentRepository.Query(lvList);

                foreach (var v in values)
                {
                    result.Add(new SelectListItem()
                    {
                        Text = v.DeptName,
                        Value = (v.DeptId).ToString()
                    });
                }

                return result;
            }
        }       

        public List<SelectListItem> ClassOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                //var values = comdepartmentRepository.Query(lvList);

                //foreach (var v in values)
                //{
                //    result.Add(new SelectListItem()
                //    {
                //        Text = v.DeptName,
                //        Value = (v.DeptId).ToString()
                //    });
                //}

                return result;
            }
        }

        public List<SelectListItem> LanguageOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();

                result.Add(new SelectListItem()
                {
                    Text = "繁體中文 / Traditional Chinese",
                    Value = "zh-tw"
                });

                result.Add(new SelectListItem()
                {//泰語
                    Text = "ภาษาไทย / Thai",
                    Value = "th"
                });

                return result;
            }
        }
    }
}