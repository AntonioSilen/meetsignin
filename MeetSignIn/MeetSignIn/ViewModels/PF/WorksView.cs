﻿using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.PF
{
    public class WorksView
    {
        public WorksView()
        {
            this.IsEdited = false;
        }
        public int ID { get; set; }

        [Display(Name = "標題")]
        public string Title { get; set; }
        public string ThaiTitle { get; set; }

        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "季度")]
        public string SeasonTitle { get; set; }
        public string ThaiSeasonTitle { get; set; }

        [Display(Name = "班別")]
        public int ClassID { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }

        //班別清單
        public List<PFCategoryClsView> ClassList { get; set; }
        
        ////作業清單
        //public List<PFWork> WorksList { get; set; }

        public bool IsEdited { get; set; }
    }
}