﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.ViewModels.PF
{
    public class SignInView
    {
        [Required]
        public string PersonId { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string DeptId { get; set; }

        //public List<SelectListItem> DeptOptions
        //{

        //}
    }
}