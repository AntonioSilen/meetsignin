﻿using MeetSignIn.Areas.Admin.ViewModels.PFCategory;
using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.PF
{
    public class FormOverview
    {
        [Display(Name = "部門")]
        public string DeptId { get; set; }
        public string DeptName { get; set; }

        [Display(Name = "季度")]
        public string SeasonTitle { get; set; }
        public string ThaiSeasonTitle { get; set; }

        [Display(Name = "班別")]
        public int ClassID { get; set; }

        [Display(Name = "評比作業")]
        public int WorkID { get; set; }
        public string WorkTitle { get; set; }
        public string ThaiWorkTitle { get; set; }

        [Display(Name = "工號")]
        public string PersonId { get; set; }
        public string PersonName { get; set; }

        public int AppraiseID { get; set; }
        public int AppraiseWorksID { get; set; }
        public bool IsFinished { get; set; }
        public bool IsAudit { get; set; }

        public List<PFCategoryCateView> CateList { get; set; }
    }
}