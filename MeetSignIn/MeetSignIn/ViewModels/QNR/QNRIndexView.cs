﻿using MeetSignIn.Areas.Admin.ViewModels.QNR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.QNR
{
    public class QNRIndexView
    {
        [Display(Name = "填寫狀態")]
        public bool IsFilled { get; set; }

        public int ID { get; set; }

        [Display(Name = "問卷標題")]
        public string Title { get; set; }

        [Display(Name = "說明")]
        public string Description { get; set; }

        [Display(Name = "填寫部門")]
        public string Depts { get; set; }

        [Display(Name = "年度")]
        public int Year { get; set; }

        [Display(Name = "月份")]
        public int Month { get; set; }

        [Display(Name = "啟用狀態")]
        public bool IsOnline { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }

        public List<QNRView> QNRList { get; set; }
    }
}