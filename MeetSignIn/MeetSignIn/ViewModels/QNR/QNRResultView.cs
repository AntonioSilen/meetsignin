﻿using MeetSignIn.Areas.Admin.ViewModels;
using MeetSignIn.Models;
using MeetSignIn.Repositories.T8Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.ViewModels.QNR
{
    public class QNRResultView : BasicSettingView
    {
        [Display(Name = "編號")]
        public int ID { get; set; }

        [Display(Name = "問卷編號")]
        public int QNRID { get; set; }
        [Display(Name = "問卷標題")]
        public string QNRStr { get; set; }

        [Display(Name = "問題編號")]
        public int QNRQuestionID { get; set; }
        [Display(Name = "問題")]
        public string QNRQuestionStr { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string PersonId { get; set; }

        [Required]
        [Display(Name = "部門")]
        public string DeptId { get; set; }

        [Required]
        [Display(Name = "問題類型")]
        public string AnswerType { get; set; }

        [Display(Name = "回答")]
        public int Answer { get; set; }

        [Display(Name = "建立日期")]
        public DateTime CreateDate { get; set; }
    }
}