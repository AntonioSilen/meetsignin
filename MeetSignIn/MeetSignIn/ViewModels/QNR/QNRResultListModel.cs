﻿using MeetSignIn.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.QNR
{
    public class QNRResultListModel
    {
        public List<QNRResult> datas { get; set; }
    }
}