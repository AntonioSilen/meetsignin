﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Sign
{
    public class MeetingSignView
    {
        public int MeetingID { get; set; }
        public List<Areas.Admin.ViewModels.SignIn.SignInView> SignedList { get; set; }
        public List<Areas.Admin.ViewModels.SignIn.SignInView> UnSignedList { get; set; }
        public List<Areas.Admin.ViewModels.SignIn.SignInView> OtherList { get; set; }
    }
}