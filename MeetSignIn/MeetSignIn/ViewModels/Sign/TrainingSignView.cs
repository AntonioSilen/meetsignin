﻿using MeetSignIn.Areas.Admin.ViewModels.QNR;
using MeetSignIn.Areas.Admin.ViewModels.TrainingPerson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Sign
{
    public class TrainingSignView
    {
        public int TrainingID { get; set; }
        public List<TrainingPersonView> SignedList { get; set; }
        public List<TrainingPersonView> UnSignedList { get; set; }
        public List<TrainingPersonView> OtherList { get; set; }

        public List<QNRPersonResultView> PersonResultList { get; set; }

        public int FullPoints { get; set; }
        public int QuestionNum { get; set; }

        public string CreaterPersonId { get; set; }

        public bool HasExam { get; set; }
        public bool HasOpinion { get; set; }
    }
}