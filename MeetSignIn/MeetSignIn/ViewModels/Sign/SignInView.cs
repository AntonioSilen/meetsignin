﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using MeetSignIn.ViewModels.Meeting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeetSignIn.ViewModels
{
    public class SignInView
    {
        public string RoomNumber { get; set; }
        public string QRCodeUrl { get; set; }

        public MeetingView MeetingInfo { get; set; }

        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        public List<SelectListItem> RoomOptions
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                var values = roomRepository.Query(0, "", true);
                foreach (var v in values)
                {
                    var factory = EnumHelper.GetDescription((Factory)v.FactoryID);                     
                    result.Add(new SelectListItem()
                    {
                        Text = "(" + v.RoomNumber + ")" + factory + " " + v.Place,
                        Value = v.RoomNumber.ToString()
                    });
                }
                return result;
            }
        }
    }
}