﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Sign
{
    public class ResultView
    {
        public string RoomNumber { get; set; }
        public string RoomStr { get; set; }
        public int MeetingID { get; set; }
        public string MeetingTitle { get; set; }
        public string JobID { get; set; }
        public string Name { get; set; }
        public string SignTime { get; set; }
        public string Depqrtment { get; set; }
        public string ResultStr { get; set; }
    }
}