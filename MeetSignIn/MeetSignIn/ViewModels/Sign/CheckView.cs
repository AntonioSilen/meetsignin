﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels
{
    public class CheckView
    {
        public string SignCode { get; set; }
        public string Title { get; set; }
        public string Organiser { get; set; }
        public int MeetingID { get; set; }
        public int MeetingNumber { get; set; }
        public int RoomID { get; set; }
        public string RoomNumber { get; set; }

        [Required]
        public string JobID { get; set; }
    }
    public class TrainingCheckView
    {
        public string SignCode { get; set; }
        public string Title { get; set; }
        public string Organiser { get; set; }
        public int TrainingID { get; set; }
        public int MeetingNumber { get; set; }
        public int RoomID { get; set; }
        public string RoomNumber { get; set; }

        [Required]
        public string JobID { get; set; }
    }
}