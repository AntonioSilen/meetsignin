﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using MeetSignIn.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Meeting
{
    public class MeetingView
    {
        public int ID { get; set; }

        [Display(Name = "課別")]
        public string MeetingNumber { get; set; }

        [Display(Name = "會議主題")]
        public string Title { get; set; }

        [Display(Name = "發起人")]
        public string Organiser { get; set; }

        [Display(Name = "課別")]
        public int Department { get; set; }

        [Display(Name = "廠區")]
        public int FactoryID { get; set; }

        RoomRepository roomRepository = new RoomRepository(new MeetingSignInDBEntities());
        public string RoomStr
        {
            get
            {
                //var roomInfo = roomRepository.FindBy(this.RoomID);
                //if (roomInfo != null)
                //{
                //    var factory = EnumHelper.GetDescription((Factory)roomInfo.FactoryID);
                //    return "(" + roomInfo.RoomNumber + ")" + factory + " " + roomInfo.Place;
                //}
                //return "";
                return roomRepository.GetRoomString(this.RoomID);
            }
        }
        [Display(Name = "會議室")]
        public int RoomID { get; set; }

        [Display(Name = "開放簽到時間")]
        public DateTime SignDate { get; set; }

        [Display(Name = "開始時間")]
        public DateTime StartDate { get; set; }

        [Display(Name = "結束時間")]
        public DateTime EndDate { get; set; }

        [Display(Name = "會議記錄")]
        public string Record { get; set; }

        [Display(Name = "檔案一")]
        public string FileOne { get; set; }

        [Display(Name = "檔案二")]
        public string FileTwo { get; set; }

        [Display(Name = "開放簽到")]
        public bool IsSignable { get; set; }

        [Display(Name = "可遲到分鐘")]
        public int LateMinutes { get; set; }

        public int SignableStatus { get; set; }
        public bool IsSigned { get; set; }
        public DateTime? SignTime { get; set; }
    }
}