﻿using MeetSignIn.ViewModels.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Meeting
{
    public class MeetingListView
    {
        public EmployeeView EmployeeInfo { get; set; }
        public List<MeetingView> MeetingList { get; set; }
    }
}