﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Meeting
{
    public class MeetingIndexView
    {
        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        public string Result { get; set; }
    }
}