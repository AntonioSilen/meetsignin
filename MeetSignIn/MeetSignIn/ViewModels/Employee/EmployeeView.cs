﻿using MeetSignIn.Models;
using MeetSignIn.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignIn.ViewModels.Employee
{
    public class EmployeeView
    {
        [Display(Name = "課別")]
        public int DepartmentID { get; set; }
        public string DeptID { get; set; }

        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
        public string DepartmentStr
        {
            get
            {
                return departmentRepository.FindByDeptID(this.DeptID).Title;
            }
        }

        [Display(Name = "工號")]
        public string JobID { get; set; }

        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Display(Name = "英文名")]
        public string EngName { get; set; }

        [Display(Name = "信箱")]
        public string Email { get; set; }

        [Display(Name = "分機")]
        public string Ext { get; set; }

        [Display(Name = "照片")]
        public string MainPic { get; set; }
    }
}