﻿using MeetSignInNotify.Models;
using MeetSignInNotify.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories
{
    public class AuditRepository : GenericRepository<Audit>
    {
        public AuditRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Audit> Query(bool? isInvalid = null, int targetId = 0, int type = 0, int? state = null, string trainingTitle = "", string personId = "", int trainingId = 0, int officer = 0, string deptId = "", bool auditIndexPage = false)
        {
            var query = GetAll();
            if (isInvalid.HasValue)
            {
                query = query.Where(q => q.IsInvalid == isInvalid);
            }
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (state.HasValue)
            {//審核狀態
                if (officer != 0)
                {//兼承辦人
                    query = query.Where(q => q.Officer == officer || q.State == state);                
                }
                else
                {
                    query = query.Where(q => q.State == state);
                }
            }
            else
            {//未審
                if (officer != 0)
                {//兼承辦人
                    query = query.Where(q => q.Officer == officer);
                }
            }
            if (!string.IsNullOrEmpty(trainingTitle))
            {
                var trainingIdList = db.Training.Where(q => q.Title.Contains(trainingTitle)).Select(q => q.ID).ToList();
                query = query.Where(q => trainingIdList.Contains(q.TrainingID));
            }
            if (trainingId != 0)
            {
                var trainingIdList = db.Training.Where(q => q.ID == trainingId).Select(q => q.ID).ToList();
                query = query.Where(q => trainingIdList.Contains(q.TrainingID));
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }

            if (auditIndexPage)
            {
                var tempList = query.ToList();
                foreach (var item in query)
                {
                    if (item.Type == (int)AuditType.CloseExternalTraining || item.Type == (int)AuditType.CloseInternalTraining)
                    {
                        var trainingData = db.Training.Where(q => q.ID == item.TrainingID).FirstOrDefault();
                        if (trainingData.IsApproved == false || trainingData.IsApproved.HasValue == false)
                        {
                            tempList.Remove(item);
                        }
                    }
                }
                return tempList.AsQueryable();
            }

            return query;
        }

        public Audit FindBy(int id)
        {
            return db.Audit.Find(id);
        }

        public Audit FindByTarget(int targetId = 0, int type = 0, int trainingId = 0, string personId = "")
        {
            var query = GetAll();
            if (targetId != 0)
            {
                query = query.Where(q => q.TargetID == targetId);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (trainingId != 0)
            {
                query = query.Where(q => q.TrainingID == trainingId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            return query.FirstOrDefault();
        }
    }
}