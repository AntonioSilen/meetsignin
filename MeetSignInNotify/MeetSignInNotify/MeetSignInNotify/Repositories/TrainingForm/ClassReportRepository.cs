﻿using MeetSignInNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories.TrainingForm
{
    public class ClassReportRepository : GenericRepository<ClassReport>
    {
        public ClassReportRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ClassReport> Query(int traingingId, string personId = "", string name = "")
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name == name);
            }

            return query;
        }

        public ClassReport FindBy(int id)
        {
            return db.ClassReport.Find(id);
        }
    }
}