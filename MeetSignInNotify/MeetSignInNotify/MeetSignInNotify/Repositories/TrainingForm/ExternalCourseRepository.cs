﻿using MeetSignInNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories.TrainingForm
{
    public class ExternalCourseRepository : GenericRepository<ExternalCourse>
    {
        public ExternalCourseRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ExternalCourse> Query(int traingingId = 0, int externalId = 0)
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }         
            if (externalId != 0)
            {
                query = query.Where(q => q.ExternalID == externalId);
            }         

            return query;
        }

        public ExternalCourse FindBy(int id)
        {
            return db.ExternalCourse.Find(id);
        }
    }
}