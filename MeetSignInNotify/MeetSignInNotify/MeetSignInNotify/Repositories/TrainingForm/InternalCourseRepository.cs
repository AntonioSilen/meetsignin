﻿using MeetSignInNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories.TrainingForm
{
    public class InternalCourseRepository : GenericRepository<InternalCourse>
    {
        public InternalCourseRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<InternalCourse> Query(int traingingId = 0, int internalId = 0)
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }         
            if (traingingId != 0)
            {
                query = query.Where(q => q.InternalID == internalId);
            }         

            return query;
        }

        public InternalCourse FindBy(int id)
        {
            return db.InternalCourse.Find(id);
        }
    }
}