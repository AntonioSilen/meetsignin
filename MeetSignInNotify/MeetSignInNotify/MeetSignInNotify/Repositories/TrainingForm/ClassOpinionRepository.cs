﻿using MeetSignInNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories.TrainingForm
{
    public class ClassOpinionRepository : GenericRepository<ClassOpinion>
    {
        public ClassOpinionRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<ClassOpinion> Query(int traingingId, string personId = "", string name = "")
        {
            var query = GetAll();
            if (traingingId != 0)
            {
                query = query.Where(q => q.TrainingID == traingingId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonID == personId);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name == name);
            }

            return query;
        }

        public ClassOpinion FindBy(int id)
        {
            return db.ClassOpinion.Find(id);
        }
    }
}