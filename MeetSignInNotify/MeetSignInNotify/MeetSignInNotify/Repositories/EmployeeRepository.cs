﻿using MeetSignInNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories
{
    public class EmployeeRepository : GenericRepository<Employee>
    {
        public EmployeeRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<Employee> Query(string name, string jobID = "", int department = 0)
        {
            var query = GetAll();
            if (department != 0)
            {
                query = query.Where(q => q.DepartmentID == department);
            }
            if (!string.IsNullOrEmpty(jobID))
            {
                query = query.Where(q => q.JobID == jobID);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name.Contains(name) || q.EngName.Contains(name));
            }
            return query;
        }

        public Employee FindBy(int id)
        {
            return db.Employee.Find(id);
        }

        public Employee FindByJobID(string jid)
        {
            return db.Employee.Where(q => q.JobID == jid).FirstOrDefault();
        }

        public IQueryable<Employee> GetSelected(string selected)
        {
            var selectedList = selected.Split(',');
            return db.Employee.Where(q => selectedList.Contains(q.JobID));
        }
    }
}