﻿using MeetSignInNotify.Models;
using MeetSignInNotify.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories
{
    public class TrainingRepository : GenericRepository<Training>
    {
        public TrainingRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());

        public IQueryable<Training> ParticipateQuery(int trainingId = 0, string personId = "")
        {
            var query = GetAll();
            var IdList = trainingPersonRepository.Query(null, "", trainingId, personId).Select(q => q.TraningID).ToList();
            query = query.Where(q => IdList.Contains(q.ID));

            return query;
        }

        public IQueryable<Training> Query(string searchValue, DateTime start, DateTime end
            , string DeptID = "", string DepartmentStr = "", int factory = 0, int room = 0, int exceptMid = 0, int parentId = 0, bool? IsClose = null, int category = 0)
        {
            var query = GetAll().Where(q => q.SeriesParentID == parentId);
            if (!string.IsNullOrEmpty(searchValue))
            {
                query = query.Where(q => q.Title.Contains(searchValue) || q.Organiser.Contains(searchValue));
            }
            if (factory != 0)
            {
                query = query.Where(q => q.FactoryID == factory);
            }
            if (room != 0)
            {
                query = query.Where(q => q.RoomID == room);
            }
            if (start > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.StartDate >= start);
            }
            if (end > Convert.ToDateTime("2010/01/01"))
            {
                query = query.Where(q => q.EndDate <= end);
            }
            if (!string.IsNullOrEmpty(DeptID) && DeptID != "0001")
            {
                query = query.Where(q => q.DeptID == DeptID);
            }
            if (exceptMid != 0)
            {
                query = query.Where(q => q.ID != exceptMid);
            }
            if (IsClose.HasValue)
            {
                query = query.Where(q => q.IsClose == IsClose && q.SeriesParentID == 0);
            }
            if (category != 0)
            {
                query = query.Where(q => q.Category == category);
            }

            return query;
        }

        public Training FindBy(int id)
        {
            return db.Training.Find(id);
        }

        public List<NumAndPercentageModel> GetNumAndPercentage(DateTime start, DateTime end, string deptId = "", string department = "", string type = "")
        {
            List<NumAndPercentageModel> result = new List<NumAndPercentageModel>();
            var query = GetAll();
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptID == deptId);
            }
            if (!string.IsNullOrEmpty(department))
            {
                //var trainingIdList = trainingPersonRepository.GetSignInInfoList(0, "", department).Select(q => q.TrainingID).Distinct().ToList();
                var trainingIdList = trainingPersonRepository.Query(true, "", 0, "", department).Select(q => q.TraningID).Distinct().ToList();
                if (trainingIdList.Count() > 0)
                {
                    query = query.Where(q => trainingIdList.Contains(q.ID));
                }
            }
            if (start >= Convert.ToDateTime("2000/01/01") && end >= Convert.ToDateTime("2000/01/01"))
            {
                query = query.Where(q => q.StartDate >= start && q.EndDate <= end);
            }
            List<NumAndPercentageModel> data = new List<NumAndPercentageModel>();
            switch (type)
            {
                case "Category":
                    return query.GroupBy(d => d.Category)
                          .SelectMany(grp => grp
                          .Select(row => new NumAndPercentageModel()
                          {
                              Title = row.Category.ToString(),
                              Num = grp.Count(),
                              CateCount = grp.Count(),
                              AllCount = query.Count()
                          })
                      ).Distinct().ToList();
                case "Theme":
                    return query.GroupBy(d => d.Theme)
                        .SelectMany(grp => grp
                        .Select(row => new NumAndPercentageModel()
                        {
                            Title = row.Theme.ToString(),
                            Num = grp.Count(),
                            CateCount = grp.Count(),
                            AllCount = query.Count()
                        })
                    ).Distinct().ToList();
                case "Fee":
                    return query.GroupBy(d => d.Fee)
                       .SelectMany(grp => grp
                       .Select(row => new NumAndPercentageModel()
                       {
                           Title = row.Fee.ToString(),
                           Num = grp.Count(),
                           CateCount = grp.Count(),
                           AllCount = query.Count()
                       })
                   ).Distinct().ToList();
                case "Hours":
                    return query.GroupBy(d => d.Hours)
                        .SelectMany(grp => grp
                        .Select(row => new NumAndPercentageModel()
                        {
                            Title = row.Hours.ToString(),
                            Num = grp.Count(),
                            CateCount = grp.Count(),
                            AllCount = query.Count()
                        })
                    ).Distinct().ToList();
            }
            return data;
        }

        public Training GetTrainingByNum(string roomNum)
        {
            try
            {
                DateTime now = DateTime.Now;
                DateTime last = Convert.ToDateTime(now.ToShortDateString() + " 23:59:59");
                var roomInfo = db.Room.Where(q => q.RoomNumber == roomNum).FirstOrDefault();
                //var result = GetAll().Where(q => q.RoomID == roomInfo.ID && q.EndDate > now && q.EndDate <= last).OrderBy(q => q.StartDate).FirstOrDefault();
                var result = GetMeetingFilterList().Where(q => q.RoomID == roomInfo.ID && q.SignDate <= now && q.LastSignDate >= now).OrderBy(q => q.StartDate).FirstOrDefault();

                return GetById(result.ID);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string DeletePic(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainPic;
            data.MainPic = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteFile(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainFile;
            data.MainFile = "";

            db.SaveChanges();
            return fileName;
        }
        public string DeleteVideo(int id)
        {
            Training data = FindBy(id);
            string fileName = data.MainVideo;
            data.MainVideo = "";

            db.SaveChanges();
            return fileName;
        }

        public List<TrainingFilterModel> GetMeetingFilterList()
        {
            var query = from t in db.Training
                        where t.IsSignable == true
                        select new TrainingFilterModel()
                        {
                            ID = t.ID,
                            RoomID = t.RoomID,
                            SignDate = t.SignDate,
                            StartDate = t.StartDate,
                            LateMinutes = t.LateMinutes,
                            LastSignDate = t.StartDate,
                            EndDate = t.EndDate,
                        };
            var resultList = query.ToList();
            foreach (var item in resultList)
            {
                item.LastSignDate = item.StartDate.AddMinutes(item.LateMinutes);
            }

            return resultList;
        }

        public class TrainingFilterModel
        {
            public int ID { get; set; }
            public int RoomID { get; set; }
            public DateTime SignDate { get; set; }
            public DateTime StartDate { get; set; }
            public int LateMinutes { get; set; }
            public DateTime LastSignDate { get; set; }
            public DateTime EndDate { get; set; }
        }

        public class NumAndPercentageModel
        {
            public string Title { get; set; }
            public double DoubleTitle
            {
                get
                {
                    try
                    {
                        return Convert.ToDouble(this.Title);
                    }
                    catch (Exception)
                    {
                        return 0;
                    }
                }
            }
            public int Num { get; set; }

            public int AllCount { get; set; }
            public int CateCount { get; set; }

            public string Percentage
            {
                get
                {
                    return (100.0 * this.CateCount / this.AllCount).ToString("0.00");
                }
            }
        }
    }
}