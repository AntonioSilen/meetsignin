﻿using AutoMapper;
using MeetSignInNotify.Models;
using MeetSignInNotify.Models.Join;
using MeetSignInNotify.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Repositories
{
    public class TrainingPersonRepository : GenericRepository<TrainingPerson>
    {
        public TrainingPersonRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();

        public IQueryable<TrainPersonJoinPersonAndDepartment> GetSignInInfoList(int tid = 0, string jid = "", string DeptID = "")
        {
            try
            {
                var query = db.TrainingPerson.AsQueryable();
                if (tid != 0)
                {
                    query = query.Where(p => p.TraningID == tid);
                }
                if (!string.IsNullOrEmpty(jid))
                {
                    //【JobID】等於T8資料庫的【PersonId】
                    query = query.Where(q => q.JobID == jid);
                }
                var notifyViewList = Mapper.Map<List<Areas.Admin.ViewModels.TrainingPerson.TrainingPersonView>>(query).AsQueryable();
                var resultList = new List<TrainPersonJoinPersonAndDepartment>();
                if (!string.IsNullOrEmpty(DeptID))
                {
                    notifyViewList = notifyViewList.Where(q => q.DeptID == DeptID);
                }
                foreach (var item in notifyViewList)
                {
                    var joinQuery = from p in t8db.comPerson
                            join g in t8db.comGroupPerson
                            on p.PersonId equals g.PersonId
                            join d in t8db.comDepartment
                            on p.DeptId equals d.DeptId
                            where g.PersonId == item.JobID
                            select new TrainPersonJoinPersonAndDepartment()
                            {
                                ID = item.ID,
                                DeptID = d.DeptId,
                                ParentDeptID = d.ParentDeptId,
                                PersonId = g.PersonId,
                                TrainingID = item.TraningID,
                                DeptName = d.DeptName,
                                Name = g.PersonName,
                                EngName = g.EngName,
                                Email = g.EMail,
                                InductionDate = p.InductionDate,
                                Birthday = g.Birthday
                            };
                    resultList.Add(joinQuery.FirstOrDefault());
                }
             
                return resultList.AsQueryable();
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public IQueryable<TrainingPerson> Query(bool? status, string name = "", int tid = 0, string jid = "", string DeptID = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsSign == status);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name.Contains(name));
            }
            if (tid != 0)
            {
                query = query.Where(q => q.TraningID == tid);
            }
            if (!string.IsNullOrEmpty(jid))
            {
                query = query.Where(q => q.JobID == jid);
                var ck = query.ToList();
            }
            if (!string.IsNullOrEmpty(DeptID))
            {
                var t8PersonIdList = t8db.comPerson.Where(q => q.DeptId == DeptID).Select(q => q.PersonId).ToList();
                query = query.Where(q => t8PersonIdList.Contains(q.JobID));
            }
            return query;
        }

        public TrainingPerson FindBySignCode(string signCode)
        {
            return db.TrainingPerson.Where(q => q.SignCode == signCode).FirstOrDefault();
        }

        public TrainingPerson FindBy(int id)
        {
            return db.TrainingPerson.Find(id);
        }

        public TrainingPerson FindByJobID(string jid)
        {
            return db.TrainingPerson.Where(q => q.JobID == jid).FirstOrDefault();
        }

        public bool CleanOldList(int tid)
        {
            try
            {
                var queryList = GetAll().Where(q => q.TraningID == tid).ToList();
                if (queryList.Count() > 0)
                {
                    foreach (var data in queryList)
                    {
                        db.TrainingPerson.Remove(data);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}