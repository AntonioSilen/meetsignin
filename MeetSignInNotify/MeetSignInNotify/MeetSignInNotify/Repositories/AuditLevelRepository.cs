﻿using MeetSignInNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetSignInNotify.Repositories
{
    public class AuditLevelRepository : GenericRepository<AuditLevel>
    {
        public AuditLevelRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<AuditLevel> Query(bool? status = null, string principal = "", string president = "", string vice = "", string manager = "")
        {
            var query = GetAll();
            if (!string.IsNullOrEmpty(principal))
            {
                query = query.Where(q => q.PrincipalReview == principal);
            }
            if (!string.IsNullOrEmpty(president))
            {
                query = query.Where(q => q.PresidentReview == president);
            }
            if (!string.IsNullOrEmpty(vice))
            {
                query = query.Where(q => q.ViceReview == vice);
            }
            if (!string.IsNullOrEmpty(manager))
            {
                query = query.Where(q => q.ManagerReview == manager);
            }
            if (status.HasValue)
            {
                query = query.Where(q => q.IsOnline == status);
            }
            return query;
        }

        public AuditLevel FindBy(int id)
        {
            return db.AuditLevel.Find(id);
        }

        public AuditLevel FindByDept(string deptId)
        {
            return db.AuditLevel.Where(q => q.DeptId == deptId).FirstOrDefault();
        }
    }
}
