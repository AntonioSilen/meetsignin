﻿using MeetSignInNotify.Models;
using MeetSignInNotify.Models.Others;
using MeetSignInNotify.Repositories;
using MeetSignInNotify.Repositories.T8Repositories;
using MeetSignInNotify.Repositories.TrainingForm;
using MeetSignInNotify.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeetSignInNotify
{
    public partial class Form1 : Form
    {
        private delegate void DelShowMessage(string sMessage);

        //public bool Active = true;
        public string notifyUrl = "https://notify-api.line.me/api/notify";
        string ClientID = "63Zqpardt29Uzt5EiU5rpS";
        string ClientSecret = "LLZaWh3paSVgjfe104Su7CGWzlxnu5Ik6GSspsuiZOg";
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private AuditRepository auditRepository = new AuditRepository(new MeetingSignInDBEntities());
        private AuditLevelRepository auditLevelRepository = new AuditLevelRepository(new MeetingSignInDBEntities());
        private TrainingRepository trainingRepository = new TrainingRepository(new MeetingSignInDBEntities());
        private TrainingPersonRepository trainingPersonRepository = new TrainingPersonRepository(new MeetingSignInDBEntities());       
        private ClassOpinionRepository classOpinionRepository = new ClassOpinionRepository(new MeetingSignInDBEntities());       
        private ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());       
        private InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());       
        private ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());       
        private InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());       
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        public int NotifyID = Convert.ToInt32(DateTime.Now.Date.ToString("yyyyMMdd"));
        public DateTime startNotifyDate = Convert.ToDateTime(("2022/09/01 08:00"));

        public int AuditProcessing = (int)FormAudit.Processing;

        //測試模式
        public bool testMode = false;
        public string testMail = "seelen12@fairlybike.com";

        private void AddMessage(string sMessage)
        {
            if (this.InvokeRequired) // 若非同執行緒
            {
                DelShowMessage del = new DelShowMessage(AddMessage); //利用委派執行
                this.Invoke(del, sMessage);
            }
            else // 同執行緒
            {
                this.textBox1.Text += sMessage + Environment.NewLine;
            }
        }

        public Form1()
        {
            InitializeComponent();
            TrayMenuContext();
        }

        private void TrayMenuContext()
        {
            this.notifyIcon1.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            this.notifyIcon1.ContextMenuStrip.Items.Add("開啟", null, this.button1_Click);
            this.notifyIcon1.ContextMenuStrip.Items.Add("停止", null, this.button2_Click);
            this.notifyIcon1.ContextMenuStrip.Items.Add("關閉", null, this.button3_Click);
        }

        /// <summary>
        /// App 發送訊息
        /// </summary>
        public async Task StartSendNotify()
        {           
            try
            {
                await Task.Delay(3000);
                Console.WriteLine("test...");
                DateTime nowTime = DateTime.Now;

                #region 待審通知
                string link = "http://220.228.58.43:18151/Admin/AuditAdmin?State=";
                string acceptanceLink = "http://220.228.58.43:18151/Admin/TrainingAdmin/Participate";
                string msg = "<br>";
                msg += " <br> !! 教育訓練簽核通知 !!  <br>";

                List<string> auditDepts = new List<string>();

                bool canClose = true;
                var tempList = new List<Audit>();

                //主管未審資料
                AddMessage("\r\n 待主管簽核");
                var unAuditQuery = auditRepository.Query(false).Where(q => q.State == (int)AuditState.OfficerAudit);
                tempList = CheckApproved(ref unAuditQuery);
                try
                {
                    if (unAuditQuery.Count() > 0)
                    {
                        //待審訓練
                        var trainingIdList = unAuditQuery.Select(q => q.TrainingID).ToList();
                        //承辦人
                        var trainingCreaterList = trainingRepository.GetAll().Where(q => q.IsOnline && trainingIdList.Contains(q.ID) && q.StartDate > startNotifyDate).Select(q => q.Creater).ToList();
                        //主管
                        var reciverQuery = adminRepository.Query(true, (int)AdminType.Manager).ToList();
                        foreach (var item in reciverQuery)
                        {
                            var personInfo = t8personRepository.FindByJobID(item.Account);
                            if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                            {
                                //該主管審核部門
                                var auditDeptList = auditLevelRepository.Query(true, "", "", "", personInfo.PersonId).ToList();
                                auditDepts = auditDeptList.Select(q => q.DeptId).ToList();
                                //該部門人員
                                var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                                //該人員於系統內編號
                                var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && trainingCreaterList.Contains(q.ID)).Select(q => q.ID).ToList();
                                var query = unAuditQuery.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminIdList.Contains(q.Officer));
                                if (adminIdList.Count() > 0 && query.Count() > 0)
                                {
                                    string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "1 >> <br>";
                                    //發送信件
                                    if (testMode)
                                    {
                                        MailHelper.POP3Mail(testMail, "教育訓練通知 - 【待主管簽核】", auditMsg);
                                    }
                                    else
                                    {
                                        MailHelper.POP3Mail(personInfo.Email, "教育訓練通知 - 【待主管簽核】", auditMsg);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    canClose = false;
                    // Get stack trace for the exception with source file information
                    var st = new StackTrace(e, true);
                    // Get the top stack frame
                    var frame = st.GetFrame(0);
                    // Get the line number from the stack frame
                    var line = frame.GetFileLineNumber();
                    AddMessage("\r\n訊息 : " + line + " " + e.Message);
                }

                //副總經理未審資料
                AddMessage("\r\n待副總經理簽核");
                unAuditQuery = auditRepository.Query(false).Where(q => q.State == (int)AuditState.ManagerAudit);
                tempList = CheckApproved(ref unAuditQuery);
                try
                {
                    if (unAuditQuery.Count() > 0)
                    {
                        //待審訓練
                        var trainingIdList = unAuditQuery.Select(q => q.TrainingID).ToList();
                        //承辦人
                        var trainingCreaterList = trainingRepository.GetAll().Where(q => q.IsOnline && trainingIdList.Contains(q.ID) && q.StartDate > startNotifyDate).Select(q => q.Creater).ToList();

                        var reciverQuery = adminRepository.Query(true, (int)AdminType.Vice).ToList();
                        foreach (var item in reciverQuery)
                        {
                            var personInfo = t8personRepository.FindByJobID(item.Account);
                            if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                            {
                                //副總審核部門
                                auditDepts = auditLevelRepository.Query(true, "", "", personInfo.PersonId, "").Select(q => q.DeptId).ToList();
                                //該部門人員
                                var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                                //該人員於系統內編號
                                var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && trainingCreaterList.Contains(q.ID)).Select(q => q.ID).ToList();
                                var query = unAuditQuery.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminIdList.Contains(q.Officer));
                                if (adminIdList.Count() > 0 && query.Count() > 0)
                                {
                                    string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "2 >> <br>";
                                    //發送信件
                                    if (testMode)
                                    {
                                        MailHelper.POP3Mail(testMail, "教育訓練通知 - 【待副總經理簽核】", auditMsg);
                                    }
                                    else
                                    {
                                        MailHelper.POP3Mail(personInfo.Email, "教育訓練通知 - 【待副總經理簽核】", auditMsg);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    canClose = false;
                    // Get stack trace for the exception with source file information
                    var st = new StackTrace(e, true);
                    // Get the top stack frame
                    var frame = st.GetFrame(0);
                    // Get the line number from the stack frame
                    var line = frame.GetFileLineNumber();
                    AddMessage("\r\n訊息 : " + line + " " + e.Message);
                }

                //總經理未審資料
                AddMessage("\r\n待總經理簽核");
                unAuditQuery = auditRepository.Query(false, 0, 0, null, "", "", 0, 0, "", true).Where(q => q.State == (int)AuditState.ViceAudit);
                tempList = CheckApproved(ref unAuditQuery);
                try
                {
                    if (unAuditQuery.Count() > 0)
                    {
                        //待審訓練
                        var trainingIdList = unAuditQuery.Select(q => q.TrainingID).ToList();
                        //承辦人
                        var trainingCreaterList = trainingRepository.GetAll().Where(q => q.IsOnline && trainingIdList.Contains(q.ID) && q.StartDate > startNotifyDate).Select(q => q.Creater).ToList();

                        var reciverQuery = adminRepository.Query(true, (int)AdminType.President).ToList();
                        foreach (var item in reciverQuery)
                        {
                            var personInfo = t8personRepository.FindByJobID(item.Account);
                            if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                            {
                                //總經理審核部門
                                auditDepts = auditLevelRepository.Query(true, "", personInfo.PersonId, "", "").Select(q => q.DeptId).ToList();
                                //該部門人員
                                var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                                //該人員於系統內編號
                                var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && trainingCreaterList.Contains(q.ID)).Select(q => q.ID).ToList();
                                var query = unAuditQuery.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminIdList.Contains(q.Officer) || q.Officer == 1);
                                if (adminIdList.Count() > 0 && query.Count() > 0)
                                {
                                    string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "3 >> <br>";
                                    //發送信件
                                    if (testMode)
                                    {
                                        MailHelper.POP3Mail(testMail, "教育訓練通知 - 【待總經理簽核】", auditMsg);
                                    }
                                    else
                                    {
                                        MailHelper.POP3Mail(personInfo.Email, "教育訓練通知 - 【待總經理簽核】", auditMsg);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    canClose = false;
                    // Get stack trace for the exception with source file information
                    var st = new StackTrace(e, true);
                    // Get the top stack frame
                    var frame = st.GetFrame(0);
                    // Get the line number from the stack frame
                    var line = frame.GetFileLineNumber();
                    AddMessage("\r\n訊息 : " + line + " " + e.Message);
                }

                //校長未審資料
                AddMessage("\r\n待校長簽核");
                unAuditQuery = auditRepository.Query(false, 0, 0, null, "", "", 0, 0, "", true).Where(q => q.State == (int)AuditState.PresidentAudit);
                tempList = CheckApproved(ref unAuditQuery);
                try
                {
                    if (unAuditQuery.Count() > 0)
                    {
                        //待審訓練
                        var trainingIdList = unAuditQuery.Select(q => q.TrainingID).ToList();
                        //承辦人
                        var trainingCreaterList = trainingRepository.GetAll().Where(q => q.IsOnline && trainingIdList.Contains(q.ID) && q.StartDate > startNotifyDate).Select(q => q.Creater).ToList();

                        var reciverQuery = adminRepository.Query(true, 0, "", true).ToList();
                        foreach (var item in reciverQuery)
                        {
                            var personInfo = t8personRepository.FindByJobID(item.Account);
                            if (personInfo != null && !string.IsNullOrEmpty(personInfo.Email))
                            {
                                //校長審核部門
                                auditDepts = auditLevelRepository.Query(true, personInfo.PersonId, "", "", "").Select(q => q.DeptId).ToList();
                                //該部門人員
                                var sameDeptPersonIdList = t8personRepository.GetPersonQuery("", "").Where(q => auditDepts.Contains(q.DeptID)).Select(q => q.JobID).ToList();
                                //該人員於系統內編號
                                var adminIdList = adminRepository.GetAll().Where(q => sameDeptPersonIdList.Contains(q.Account) && trainingCreaterList.Contains(q.ID)).Select(q => q.ID).ToList();
                                var query = unAuditQuery.Where(q => sameDeptPersonIdList.Contains(q.PersonID) || adminIdList.Contains(q.Officer) || q.Officer == 1);
                                if (adminIdList.Count() > 0 && query.Count() > 0)
                                {
                                    string auditMsg = msg + " <br> 有部分資料需要審核，請點擊連結前往察看 <br> << " + link + "5 >> <br>";
                                    //發送信件
                                    if (testMode)
                                    {
                                        MailHelper.POP3Mail(testMail, "教育訓練通知 - 【待校長簽核】", auditMsg);
                                    }
                                    else
                                    {
                                        MailHelper.POP3Mail(personInfo.Email, "教育訓練通知 - 【待校長簽核】", auditMsg);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    canClose = false;
                    // Get stack trace for the exception with source file information
                    var st = new StackTrace(e, true);
                    // Get the top stack frame
                    var frame = st.GetFrame(0);
                    // Get the line number from the stack frame
                    var line = frame.GetFileLineNumber();
                    AddMessage("\r\n訊息 : " + line + " " + e.Message);
                }
                #endregion

                InformSupervisor ISModel = new InformSupervisor();
                ISModel.SupervisorList = new List<SupervisorItem>();

                #region 待填寫驗收/意見通知               
                string acceptanceMsg = " <br> !! 教育訓練課後表單通知 !!  <br>";
                var nearestTrainingList = trainingRepository.Query("", DateTime.Now.AddDays(-7), DateTime.Now).Where(q => q.StartDate > startNotifyDate).ToList();
                List<string> sentPersonIdList = new List<string>();
                try
                {
                    foreach (var item in nearestTrainingList)
                    {
                        bool hasAcceptance = false;
                        bool hasOpinion = false;
                        bool seriesExpired = false;
                        if (item.Category == (int)Category.External)
                        {
                            var externalPlan = externalPlanRepository.Query(item.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                            if (externalPlan != null)
                            {
                                hasAcceptance = !string.IsNullOrEmpty(externalPlan.Inspection.Replace(",", "").Replace("6", "")) && item.GroupType == (int)GroupType.Person;
                                hasOpinion = externalPlan.Inspection.IndexOf("6") >= 0;
                            }
                            DateTime searchSeriesEndDate = DateTime.Now.AddYears(1);
                            var seriesTrainingData = trainingRepository.Query("", item.EndDate, searchSeriesEndDate, "", "", 0, 0, 0, item.ID).OrderByDescending(q => q.EndDate).FirstOrDefault();
                            if (seriesTrainingData != null && seriesTrainingData.EndDate < DateTime.Now.AddDays(-7))
                            {
                                seriesExpired = true;
                            }
                            var trainingPersonList = trainingPersonRepository.Query(null, "", item.ID).Where(q => q.SignStatus == 1).ToList();
                            foreach (var tp in trainingPersonList)
                            {
                                var acceptanceData = externalAcceptanceRepository.FindByTrainingIdAndPersonId(tp.TraningID, tp.JobID);
                                var opinionData = classOpinionRepository.Query(tp.TraningID, tp.JobID).FirstOrDefault();
                                if (((acceptanceData == null && hasAcceptance) || (opinionData == null && hasOpinion)) && seriesExpired)
                                {
                                    if (!sentPersonIdList.Contains(tp.JobID))
                                    {
                                        sentPersonIdList.Add(tp.JobID);
                                        SendAcceptanceMail(acceptanceLink, acceptanceMsg + " <br> " + item.Title + " <br> ", tp);
                                    }

                                    ComposeSupervisorList(ISModel, tp, item);
                                }
                            }
                        }
                        else
                        {
                            var trainingInfo = item;
                            if (item.SeriesParentID != 0)
                            {//取得系列主課程
                                trainingInfo = trainingRepository.GetById(item.SeriesParentID);
                            }
                            if (trainingInfo.GroupType == (int)GroupType.Group)
                            {//跳過團體
                                continue;
                            }
                            var internalPlan = internalPlanRepository.Query(trainingInfo.ID).OrderByDescending(q => q.ID).FirstOrDefault();
                            if (internalPlan != null)
                            {
                                hasAcceptance = (/* internalPlan.Learning || */internalPlan.Behavior || internalPlan.Outcome) && trainingInfo.GroupType == (int)GroupType.Person;
                                hasOpinion = internalPlan.Response;
                            }
                            DateTime searchSeriesEndDate = DateTime.Now.AddYears(1);
                            var seriesTrainingData = trainingRepository.Query("", item.EndDate, searchSeriesEndDate, "", "", 0, 0, 0, item.ID).OrderByDescending(q => q.EndDate).FirstOrDefault();
                            if (seriesTrainingData != null && seriesTrainingData.EndDate < DateTime.Now.AddDays(-7))
                            {
                                seriesExpired = true;
                            }
                            var trainingPersonList = trainingPersonRepository.Query(null, "", trainingInfo.ID).Where(q => q.SignStatus == 1).ToList();
                            foreach (var tp in trainingPersonList)
                            {
                                var acceptanceData = internalAcceptanceRepository.FindByTrainingIdAndPersonId(tp.TraningID, tp.JobID);
                                var opinionData = classOpinionRepository.Query(tp.TraningID, tp.JobID).FirstOrDefault();
                                if (((acceptanceData == null && hasAcceptance) || (opinionData == null && hasOpinion)) && seriesExpired)
                                {
                                    if (!sentPersonIdList.Contains(tp.JobID))
                                    {
                                        sentPersonIdList.Add(tp.JobID);
                                        if (!testMode)
                                        {
                                            SendAcceptanceMail(acceptanceLink, acceptanceMsg + " <br> " + trainingInfo.Title + " <br> ", tp);
                                        }
                                    }
                                    
                                    ComposeSupervisorList(ISModel, tp, trainingInfo);
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    canClose = false;
                    // Get stack trace for the exception with source file information
                    var st = new StackTrace(e, true);
                    // Get the top stack frame
                    var frame = st.GetFrame(0);
                    // Get the line number from the stack frame
                    var line = frame.GetFileLineNumber();
                    AddMessage("\r\n訊息 : " + line + " " + e.Message);
                }
                #endregion

                #region 回饋未填寫(通知主管)
                List<string> sentManagerMailList = new List<string>();
                try
                {
                    #region 產生信件內容
                    foreach (var superVisorItem in ISModel.SupervisorList)
                    {
                        string acceptanceNotifyMsg = " <br> !! 負責部門人員未填寫教育訓練意見表、驗收/回饋通知 !!  <br>";
                        acceptanceNotifyMsg += $@"<p>{superVisorItem.DeptName} - {superVisorItem.PersonId} 【{superVisorItem.Name}】</p>
                                                        <p>未填寫名單 : </p>
                                                        <table style=""font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%;"">
                                                            <thead>
                                                                <tr>
                                                                    <td>部門</td>
                                                                    <td>工號</td>
                                                                    <td>姓名</td>
                                                                    <td>課程</td>
                                                                    <td>查看</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tbody>";
                                                               
                        foreach (var personItem in superVisorItem.PersonList)
                        {
                            acceptanceNotifyMsg += $@"<tr>
                                                                <td style=""border: 1px solid #ddd; padding: 8px;"">{personItem.DeptName} </td>
                                                                <td style=""border: 1px solid #ddd; padding: 8px;"">{personItem.PersonId} </td>
                                                                <td style=""border: 1px solid #ddd; padding: 8px;"">{personItem.Name} </td>
                                                                <td style=""border: 1px solid #ddd; padding: 8px;"">
                                                                    <table>";
                            foreach (var training in personItem.TrainingSimpList)
                            {
                                acceptanceNotifyMsg += $@"<tr>
                                                                    <td>{training.ID}</td>
                                                                    <td>{training.Title}</td>
                                                                    <td>{training.StartDate.ToString("yyyy-MM-dd")}</td>
                                                                </tr>";
                            }
                            acceptanceNotifyMsg += $@"      </tbody>
                                                                    </table> 
                                                                </td>
                                                                <td style=""border: 1px solid #ddd; padding: 8px;""><a href=""http://220.228.58.43:18151/Admin/TrainingAdmin/Participate?PersonID={personItem.PersonId}"">前往</td>
                                                            </tr>";
                        }
                        acceptanceNotifyMsg += @"</tbody>
                                                    </table>";

                        if (testMode)
                        {
                            MailHelper.POP3Mail(testMail, "教育訓練通知 - 【部門人員資料未填寫】", acceptanceNotifyMsg);
                        }
                        else
                        {
                            MailHelper.POP3Mail(superVisorItem.Email, "教育訓練通知 - 【部門人員資料未填寫】", acceptanceNotifyMsg);
                        }
                        
                    }
                    #endregion
                }
                catch (Exception e)
                {
                    canClose = false;
                    // Get stack trace for the exception with source file information
                    var st = new StackTrace(e, true);
                    // Get the top stack frame
                    var frame = st.GetFrame(0);
                    // Get the line number from the stack frame
                    var line = frame.GetFileLineNumber();
                    AddMessage("\r\n訊息 : " + line + " " + e.Message);
                }
                #endregion

                AddMessage("\r\n 發送完成");
                if (canClose)
                {
                    //執行完關閉
                    await Task.Delay(3000);
                    System.Environment.Exit(System.Environment.ExitCode);
                    this.Dispose();
                    this.Close();
                }
            }
            catch (Exception e)
            {
                AddMessage("\r\n" + e.Message);
                throw;
            }
        }

        private void ComposeSupervisorList(InformSupervisor ISModel, TrainingPerson tp, Training trainingInfo)
        {
            var personInfo = t8personRepository.FindByJobID(tp.JobID);
            var principalInfo = adminRepository.Query(true, 0, "", true).FirstOrDefault();
            var auditLevelData = auditLevelRepository.FindByDept(personInfo.DeptId);

            string deptPrincipal = "0000";
            if (auditLevelData != null)
            {
                deptPrincipal = auditLevelData.ManagerReview;
                if (string.IsNullOrEmpty(deptPrincipal) || deptPrincipal == "0000")
                {
                    deptPrincipal = auditLevelData.ViceReview;
                }
                if (string.IsNullOrEmpty(deptPrincipal) || deptPrincipal == "0000")
                {
                    deptPrincipal = auditLevelData.PresidentReview;
                }
                if (string.IsNullOrEmpty(deptPrincipal) || deptPrincipal == "0000")
                {
                    deptPrincipal = auditLevelData.PrincipalReview;
                }
            }
            else
            {
                //負責主管
                deptPrincipal = t8personRepository.FindByJobID(principalInfo.Account).PersonName;
            }

            var supervisorInfo = t8personRepository.FindByJobID(deptPrincipal);
            //加入主管通知
            if (supervisorInfo != null)
            {
                if (ISModel.SupervisorList.Where(q => q.PersonId == supervisorInfo.PersonId).Count() <= 0)
                {
                    ISModel.SupervisorList.Add(new SupervisorItem()
                    {
                        PersonId = supervisorInfo.PersonId,
                        Name = supervisorInfo.Name,
                        DeptId = supervisorInfo.DeptId,
                        DeptName = supervisorInfo.DeptName,
                        Email = supervisorInfo.Email,
                        PersonList = new List<PersonItem>(),
                    });
                }
                var thisSupervisor = ISModel.SupervisorList.Where(q => q.PersonId == supervisorInfo.PersonId).FirstOrDefault();
                if (personInfo != null)
                {
                    if (thisSupervisor.PersonList.Where(q => q.PersonId == personInfo.PersonId).Count() <= 0)
                    {
                        var newPerson = new PersonItem();
                        newPerson.PersonId = personInfo.PersonId;
                        newPerson.Name = personInfo.Name;
                        newPerson.DeptId = personInfo.DeptId;
                        newPerson.DeptName = personInfo.DepartmentStr;
                        newPerson.TrainingSimpList = new List<TrainingItem>();
                        newPerson.TrainingSimpList.Add(new TrainingItem()
                        {
                            ID = trainingInfo.ID,
                            Title = trainingInfo.Title,
                            StartDate = trainingInfo.StartDate,
                        });
                        thisSupervisor.PersonList.Add(newPerson);                            
                    }
                    else
                    {
                        var personData = thisSupervisor.PersonList.Where(q => q.PersonId == personInfo.PersonId).FirstOrDefault();
                        personData.TrainingSimpList.Add(new TrainingItem() {
                            ID = trainingInfo.ID,
                            Title = trainingInfo.Title,
                            StartDate = trainingInfo.StartDate,
                        });
                    }
                }
            }
        }

        private List<Audit> CheckApproved(ref IQueryable<Audit> unAuditQuery)
        {
            List<Audit> tempList = unAuditQuery.ToList();
            foreach (var item in tempList.ToList())
            {
                switch ((AuditType)item.Type)
                {
                    case AuditType.External:
                        ExternalPlanRepository externalPlanRepository = new ExternalPlanRepository(new MeetingSignInDBEntities());
                        var epData = externalPlanRepository.GetById(item.TargetID);
                        if (epData == null || epData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.Internal:
                        InternalPlanRepository internalPlanRepository = new InternalPlanRepository(new MeetingSignInDBEntities());
                        var ipData = internalPlanRepository.GetById(item.TargetID);
                        if (ipData == null || ipData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.ExternalAcceptance:
                        ExternalAcceptanceRepository externalAcceptanceRepository = new ExternalAcceptanceRepository(new MeetingSignInDBEntities());
                        var eaData = externalAcceptanceRepository.GetById(item.TargetID);
                        if (eaData == null || eaData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.InternalAcceptance:
                        InternalAcceptanceRepository internalAcceptanceRepository = new InternalAcceptanceRepository(new MeetingSignInDBEntities());
                        var iaData = internalAcceptanceRepository.GetById(item.TargetID);
                        if (iaData == null || iaData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.Seminar:
                        SeminarPlanRepository seminarPlanRepository = new SeminarPlanRepository(new MeetingSignInDBEntities());
                        var spData = seminarPlanRepository.GetById(item.TargetID);
                        if (spData == null || spData.PrincipalAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.PersonKPI:
                        PersonKPIRepository personKPIRepository = new PersonKPIRepository(new MeetingSignInDBEntities());
                        var pkData = personKPIRepository.GetById(item.TargetID);
                        if (pkData == null || pkData.PresidentAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                    case AuditType.PF:
                        PFAppraiseRepository pFAppraiseRepository = new PFAppraiseRepository(new MeetingSignInDBEntities());
                        var paData = pFAppraiseRepository.GetById(item.TargetID);
                        if (paData == null || paData.ManagerAudit > AuditProcessing)
                        {
                            tempList.Remove(item);
                        }
                        break;
                }
            }
            unAuditQuery = tempList.AsQueryable();
            return tempList;
        }

        private bool SendAcceptanceMail(string acceptanceLink, string acceptanceMsg, TrainingPerson tp)
        {
            try
            {
                var personInfo = t8personRepository.FindByJobID(tp.JobID);
                if (personInfo != null)
                {
                    acceptanceMsg = acceptanceMsg + "<br> (" + personInfo.PersonId + ")" + personInfo.Name + " 您好" + "<br> 近期偵測到您有課後驗收表單/意見表尚未填寫，請點擊連結前往察看填寫 <br> << " + acceptanceLink + "?PersonID=" + tp.JobID + " >> <br>";
                    //發送信件
                    if (testMode)
                    {
                        MailHelper.POP3Mail(testMail, "教育訓練通知 - 【課後表單通知】", acceptanceMsg);
                    }
                    else
                    {
                        MailHelper.POP3Mail(personInfo.Email, "教育訓練 - 【課後表單通知】", acceptanceMsg);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button3.Visible = false;
            //Active = true;
            textBox1.Text = "開始發送訊息 。。。";
            Task.Run(StartSendNotify);
        }

        private void button1_Click(object sender, EventArgs e)
        {//開啟
            //Active = true;
            textBox1.Text += "\r\n開始發送訊息 。。。";
            Task.Run(StartSendNotify);
        }

        private void button2_Click(object sender, EventArgs e)
        {//停止
            //Active = false;
            textBox1.Text += "\r\n已停止執行";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);
            this.Dispose();
            this.Close();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ShowInTaskbar == false)
                notifyIcon1.Visible = true;

            this.ShowInTaskbar = true;
            this.Show();
            this.Activate();
            this.WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);
            this.Dispose();
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                notifyIcon1.Visible = true;
                this.Hide();
                this.ShowInTaskbar = false;
            }
        }
    }
}
