﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetSignInNotify.Models.Others
{
    public class InformSupervisor
    {
        public List<SupervisorItem> SupervisorList { get; set; }
    }

    public class SupervisorItem
    {
        public string PersonId { get; set; }
        public string Name { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public string Email { get; set; }
        public List<PersonItem> PersonList { get; set; }
    }

    public class PersonItem
    {
        public string PersonId { get; set; }
        public string Name { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public List<TrainingItem> TrainingSimpList { get; set; }
        //public int TrainingID { get; set; }
        //public string TrainingTitle { get; set; }
        //public bool PlanFilled { get; set; }
        //public bool OpinionFilled { get; set; }
        //public bool AcceptanceFilled { get; set; }
    }

    public class TrainingItem
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
    }
}
