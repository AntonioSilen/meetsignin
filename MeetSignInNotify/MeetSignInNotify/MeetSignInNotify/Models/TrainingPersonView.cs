﻿using MeetSignInNotify.Models;
using MeetSignInNotify.Repositories;
using MeetSignInNotify.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Areas.Admin.ViewModels.TrainingPerson
{
    public class TrainingPersonView
    {
        private Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        [Required]
        public int ID { get; set; }

        public string SignCode { get; set; }

        [Required]
        [Display(Name = "教育訓練編號")]
        public int TraningID { get; set; }

        [Display(Name = "教育訓練")]
        public string TrainingTitle { get; set; }

        [Required]
        [Display(Name = "工號")]
        public string JobID { get; set; }

        public string DeptID
        {
            get
            {
                try
                {
                    return personRepository.FindByJobID(this.JobID).DeptID;
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }

        [Required]
        [Display(Name = "姓名")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "是否簽到")]
        public bool IsSign { get; set; }

        [Required]
        [Display(Name = "簽到狀態")]
        public int SignStatus { get; set; }

        [Display(Name = "通知狀態")]
        public bool IsSent { get; set; }

        [Display(Name = "是否合格")]
        public bool IsPassed { get; set; }

        [Display(Name = "簽到時間")]
        public DateTime SignTime { get; set; }

        public string SignTimeStr
        {
            get
            {
                return this.IsSign ? this.SignTime.ToString("yyyy/MM/dd HH:mm") : "";
            }
        }

        public int ActionPlanState { get; set; }
        public int OpinionState { get; set; }
        public int ReportState { get; set; }
        public int EAState { get; set; }
        public int IAState { get; set; }

        public string ActionPlanStatus { get; set; }
        public string OpinionStatus { get; set; }
        public string ReportStatus { get; set; }
        public string EAStatus { get; set; }
        public string IAStatus { get; set; }

        //訓練判斷資訊
        public int Score { get; set; }
    }
}