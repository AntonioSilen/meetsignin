﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInNotify.Models.T8.Join
{
    public class ExistDepartment
    {
        public string DeptID { get; set; }
        public string ParentDeptID { get; set; }
        public string Title { get; set; }
    }
}