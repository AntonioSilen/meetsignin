//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace MeetSignInNotify.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClassOpinion
    {
        public int ID { get; set; }
        public int TrainingID { get; set; }
        public string PersonID { get; set; }
        public string Name { get; set; }
        public int Compatibility { get; set; }
        public string CompatibilityOpinion { get; set; }
        public int Architecture { get; set; }
        public string ArchitectureOpinion { get; set; }
        public int Schedule { get; set; }
        public string ScheduleOpinion { get; set; }
        public int CourseMaterials { get; set; }
        public string CourseMaterialsOpinion { get; set; }
        public int Device { get; set; }
        public string DeviceOpinion { get; set; }
        public int Professional { get; set; }
        public string ProfessionalOpinion { get; set; }
        public int Expression { get; set; }
        public string ExpressionOpinion { get; set; }
        public int TimeControl { get; set; }
        public string TimeControlOpinion { get; set; }
        public int TeachingMethod { get; set; }
        public string TeachingMethodOpinion { get; set; }
        public int Recommended { get; set; }
        public string RecommendedOpinion { get; set; }
        public int ActualUsage { get; set; }
        public string ActualUsageOpinion { get; set; }
        public int CurrentUsage { get; set; }
        public string CurrentUsageOpinion { get; set; }
        public int FutureUsage { get; set; }
        public string FutureUsageOpinion { get; set; }
        public int Difficulty { get; set; }
        public string Expect { get; set; }
        public string Advice { get; set; }
        public System.DateTime CreateDate { get; set; }
        public bool IsAudit { get; set; }
        public int ManagerAudit { get; set; }
        public string ManagerSignature { get; set; }
        public string ManagerRemark { get; set; }
        public Nullable<System.DateTime> ManagerAuditDate { get; set; }
        public int ViceAudit { get; set; }
        public string ViceSignature { get; set; }
        public string ViceRemark { get; set; }
        public Nullable<System.DateTime> ViceAuditDate { get; set; }
        public int OfficerAudit { get; set; }
        public string OfficerSignature { get; set; }
        public string OfficerRemark { get; set; }
        public Nullable<System.DateTime> OfficerAuditDate { get; set; }
        public int PrincipalAudit { get; set; }
        public string PrincipalSignature { get; set; }
        public string PrincipalRemark { get; set; }
        public Nullable<System.DateTime> PrincipalAuditDate { get; set; }
        public int PresidentAudit { get; set; }
        public string PresidentSignature { get; set; }
        public string PresidentRemark { get; set; }
        public Nullable<System.DateTime> PresidentAuditDate { get; set; }
    }
}
