﻿using MeetSignInAttendanceNotify.Models;
using MeetSignInAttendanceNotify.Repositories;
using MeetSignInAttendanceNotify.Utility.Helpers;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeetSignInAttendanceNotify
{   
    public partial class Form1 : Form
    {
        //public bool Active = true;
        public string notifyUrl = "https://notify-api.line.me/api/notify";
        string ClientID = "63Zqpardt29Uzt5EiU5rpS";
        string ClientSecret = "LLZaWh3paSVgjfe104Su7CGWzlxnu5Ik6GSspsuiZOg";
        private AdminRepository adminRepository = new AdminRepository(new MeetingSignInDBEntities());
        private SignInRepository signInRepository = new SignInRepository(new MeetingSignInDBEntities());
        private MeetingRepository meetingRepository = new MeetingRepository(new MeetingSignInDBEntities());
        private Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
        public int NotifyID = Convert.ToInt32(DateTime.Now.Date.ToString("yyyyMMdd"));
        public DateTime startNotifyDate = Convert.ToDateTime(("2022/08/31"));

        //測試模式
        public bool testMode = false;
        public string testMail = "seelen12@fairlybike.com";

        public Form1()
        {
            InitializeComponent();
            TrayMenuContext();
        }


        private void TrayMenuContext()
        {
            this.notifyIcon1.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            this.notifyIcon1.ContextMenuStrip.Items.Add("開啟", null, this.button1_Click);
            this.notifyIcon1.ContextMenuStrip.Items.Add("停止", null, this.button2_Click);
            this.notifyIcon1.ContextMenuStrip.Items.Add("關閉", null, this.button3_Click);
        }

        /// <summary>
        /// App 發送訊息
        /// </summary>
        public async Task StartSendNotify()
        {
            try
            {
                await Task.Delay(2000);
                Console.WriteLine("test...");
                DateTime nowTime = DateTime.Now;

                //string link = "http://220.228.58.43:18151/Admin/AuditAdmin?State=";

                #region 出勤通知

                #region 建立Excel
                DateTime now = DateTime.Now;
                DateTime StartDate = now.AddMonths(-1);
                DateTime EndDate = now;
                var CurrentDirectory = Directory.GetCurrentDirectory();
                string fileName = $"({StartDate.ToString("MM_dd")}-{EndDate.ToString("MM_dd")})出勤紀錄";
                //string fullPath = CurrentDirectory.Replace(@"\bin\Debug", @"\ExportFiles\" + fileName + ".xlsx");
                string fullPath = @"C:\inetpub\wwwroot\MeetSignIn\ExportFiles\" + fileName + ".xlsx";

                textBox1.Text += "\r\n 建立Excel";
                var createrEmailList = ExportListExcel(StartDate, EndDate, fullPath);
                #endregion

                if (createrEmailList.Count() > 0 && File.Exists(fullPath))
                {
                    #region 發送通知
                    textBox1.Text += "\r\n 發送通知";
                    string acceptanceMsg = " <br> " + fileName + " <br>";

                    var result = true;
                    foreach (var email in createrEmailList)
                    {
                        result = SendAcceptanceMail("", acceptanceMsg, email, fullPath, fileName);
                    }                    
                    #endregion

                    if (result)
                    {
                        //執行完關閉
                        await Task.Delay(2000);
                        Environment.Exit(Environment.ExitCode);
                        this.Dispose();
                        this.Close();
                    }
                    else
                    {
                        textBox1.Text += "\r\n" + "寄信失敗 !";
                    }
                }
                else
                {
                    textBox1.Text += "\r\n" + "無資料/資料處裡錯誤 !";
                }
                #endregion
            }
            catch (Exception e)
            {
                textBox1.Text += "\r\n" + e.Message;
                throw;
            }
        }

        private bool SendAcceptanceMail(string link, string acceptanceMsg, string email, string attachmentPath = "", string fileName = "")
        {
            try
            {
                //acceptanceMsg = acceptanceMsg + "<br> 近期偵測到您有課後驗收表單/意見表尚未填寫，請點擊連結前往察看填寫 <br>";
                //發送信件
                if (testMode)
                {
                    MailHelper.POP3Mail(testMail, "會議管理 - 例行會議出勤紀錄", acceptanceMsg, attachmentPath);
                }
                else
                {
                    MailHelper.POP3Mail(email, "會議管理 - 例行會議出勤紀錄", acceptanceMsg, attachmentPath);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #region 匯出Excel
        /// <summary>
        /// 匯出Excel清單 (非同步)
        /// </summary>
        /// <returns></returns>
        public List<string> ExportListExcel(DateTime StartDate, DateTime EndDate, string fullPath)
        {
            List<string> emailList = new List<string>();
            emailList.Add("seelen12@fairlybike.com");
            //取出要匯出Excel的資料(勾選需出勤紀錄的會議)
            var nearestMeetingList = meetingRepository.Query("", StartDate, EndDate, 0, 0, 0, 0, true).Where(q => q.StartDate > startNotifyDate).Where(q => q.IsAttendance == true).OrderBy(q => q.StartDate);

            if (nearestMeetingList.Count() > 0)
            {
                textBox1.Text += "\r\n 匯出資料存在.....";
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial; // 關閉新許可模式通知
                //建立Excel
                ExcelPackage excelPackage = new ExcelPackage();

                //建立第一個Sheet，後方為定義Sheet的名稱
                ExcelWorksheet sheet = excelPackage.Workbook.Worksheets.Add("出勤紀錄");

                MemoryStream fileStream = new MemoryStream();
                try
                {

                    int col = 1;    //欄:直的，從第1欄開始

                    //第1列是標題列 
                    //標題列部分，是取得DataAnnotations中的DisplayName，這樣比較一致，
                    //這也可避免後期有修改欄位名稱需求，但匯出excel標題忘了改的問題發生。
                    //取得做法可參考最後的參考連結。

                    int row = 2;    //列:橫的 【課程】
                    //建立所有人員名單                   
                    var allMeetingIdList = nearestMeetingList.Select(q => q.ID).ToList();
                    var allSignPersonIdList = signInRepository.GetAll()
                        .Where(q => allMeetingIdList.Contains(q.MeetingID))
                        .Select(x => new SignInPersonModel
                         {
                             PersonId = x.JobID,
                             Name = x.Name,
                         })
                        .Distinct().ToList();
                    //var checkList = allSignPersonIdList.ToList();
                    textBox1.Text += "\r\n 繪製人員欄位";
                    foreach (var person in allSignPersonIdList.OrderBy(q => q.DeptName))
                    {
                        person.RowNum = row;
                        sheet.Cells[row, col].Value = $"【{person.DeptName}】({person.PersonId}) {person.Name}";
                        sheet.Column(col).AutoFit();
                        row++;
                    }

                    //迴圈讀取訓練資料
                    //資料從第2列開始
                    col = 2;

                    if (nearestMeetingList.Where(q =>q.Organiser == "董光華" || q.Organiser == "陳勇至").Count() > 0)
                    {
                        emailList.Add("steven@fairlybike.com");
                    }

                    textBox1.Text += "\r\n 讀取會議清單";
                    foreach (var mt in nearestMeetingList)
                    {

                        textBox1.Text += "\r\n 會議 : " + mt.Title;
                        row = 1;                    
                        sheet.Cells[row, col].Value = $"{mt.Title} ({mt.StartDate.ToString("yyyy/MM/dd HH:mm")})";
                        sheet.Column(col).AutoFit();

                        var creater = adminRepository.GetById(mt.Creater);
                        var createrInfo = t8personRepository.FindByJobID(creater.Account);
                        if (createrInfo != null && !string.IsNullOrEmpty(createrInfo.Email) && !emailList.Contains(createrInfo.Email))
                        {
                            emailList.Add(createrInfo.Email);
                        }                       

                        var signPersonList = signInRepository.GetSignInInfoList(mt.ID);

                        //textBox1.Text += "\r\n 人員名單";
                        foreach (var sp in signPersonList)
                        {
                            //textBox1.Text += "\r\n 工號 : " + sp.JobID;
                            int rowNum = allSignPersonIdList.Where(q => q.PersonId == sp.JobID).FirstOrDefault().RowNum;

                            var signInResult = "缺席/未簽到";
                            switch (sp.SignStatus)
                            {
                                case 0:
                                    signInResult = "缺席/未簽到";
                                    break;
                                case 1:
                                    signInResult = $"出席({Convert.ToDateTime(sp.SignTime).ToString("yyyy/MM/dd HH:mm")})";
                                    break;
                                case 2:
                                    signInResult = "請假";
                                    break;
                            }
                            sheet.Cells[rowNum, col].Value = signInResult;
                            sheet.Column(col).AutoFit();
                        }
                        col++;
                    }                   

                    var existingFile = new FileInfo(fullPath);
                    if (existingFile.Exists)
                    {
                        existingFile.Delete();
                    }
                    excelPackage.SaveAs(existingFile);
                    excelPackage.Dispose();
                    fileStream.Dispose();
                    //fileStream.Position = 0;//不重新將位置設為0，excel開啟後會出現錯誤
                }
                catch (Exception e)
                {
                    textBox1.Text += "\r\n" + e.Message;
                    excelPackage.Dispose();
                    fileStream.Dispose();
                }
            }
           
            return emailList;
        }

        public static void SaveMemoryStream(MemoryStream ms, string FileName)
        {
            FileStream outStream = File.OpenWrite(FileName);
            ms.WriteTo(outStream);
            outStream.Flush();
            outStream.Close();
        }
        #endregion


        private void Form1_Load(object sender, EventArgs e)
        {
            button3.Visible = false;
            //Active = true;
            textBox1.Text = "開始發送訊息 。。。";
            Task.Run(StartSendNotify);
        }

        private void button1_Click(object sender, EventArgs e)
        {//開啟
            //Active = true;
            textBox1.Text += "\r\n開始發送訊息 。。。";
            Task.Run(StartSendNotify);
        }

        private void button2_Click(object sender, EventArgs e)
        {//停止
            //Active = false;
            textBox1.Text += "\r\n已停止執行";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);
            this.Dispose();
            this.Close();
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ShowInTaskbar == false)
                notifyIcon1.Visible = true;

            this.ShowInTaskbar = true;
            this.Show();
            this.Activate();
            this.WindowState = FormWindowState.Normal;
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Environment.Exit(System.Environment.ExitCode);
            this.Dispose();
            this.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                notifyIcon1.Visible = true;
                this.Hide();
                this.ShowInTaskbar = false;
            }
        }

        private class SignInPersonModel
        {
            public int RowNum { get; set; }
            public string PersonId { get; set; }
            public string Name { get; set; }
            public string DeptName 
            { 
                get {
                    Repositories.T8Repositories.PersonRepository t8personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());
                    var person = t8personRepository.FindByJobID(this.PersonId);
                    return person == null ? "菲力工業" : person.DeptName;  
                } 
            }
        }
    }
}
