﻿using MeetSignInAttendanceNotify.Models;
using MeetSignInAttendanceNotify.Models.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Repositories
{
    public class SignInRepository : GenericRepository<SignIn>
    {
        public SignInRepository(MeetingSignInDBEntities context) : base(context) { }
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        private T8ERPEntities t8db = new T8ERPEntities();
        Repositories.T8Repositories.PersonRepository personRepository = new Repositories.T8Repositories.PersonRepository(new T8ERPEntities());

        public IQueryable<SignInJoinEmployeeAndDepartment> GetSignInInfoList(int mid, string jid = "", string signCode = "")
        {

            var notifyList = db.SignIn.Where(p => p.MeetingID == mid);
            if (!string.IsNullOrEmpty(jid))
            {
                notifyList = notifyList.Where(q => q.JobID == jid);
            }

            var query = from s in notifyList
                        //join e in db.Employee
                        //on s.JobID equals e.JobID
                        //join d in db.Department
                        //on e.DepartmentID equals d.ID
                        select new SignInJoinEmployeeAndDepartment()
                        {
                            ID = s.ID,
                            SignCode = s.SignCode,
                            JobID = s.JobID,
                            MeetingID = s.MeetingID,
                            //DepartmentID = d.ID,
                            //DepartmentTitle = d.Title,
                            //Name = e.Name,
                            //EngName = e.EngName,
                            //Email = e.Email,
                            //Ext = e.Ext,
                            //IsForeign = d.IsForeign,

                            //IsSign = s.IsSign,
                            SignStatus = s.SignStatus,
                            IsSent = s.IsSent,
                            SignTime = s.SignTime,
                        };
            var temp = query.ToList();
            foreach (var item in temp)
            {
                var personInfo = personRepository.FindByJobID(item.JobID);
                if (personInfo != null)
                {
                    item.DeptID = personInfo.DeptID;
                    //item.DepartmentID = db.Department.Where(q => q.DeptID == personInfo.DeptID).FirstOrDefault().ID;
                    item.DepartmentID = 0;
                    item.DepartmentTitle = personInfo.DepartmentStr;
                    item.Name = personInfo.Name;
                    item.EngName = personInfo.EngName;
                    item.Email = personInfo.Email;
                    item.Ext = "";
                    item.IsForeign = false;
                }
                else
                {

                }
              
            }
            if (!string.IsNullOrEmpty(signCode))
            {
                query = query.Where(q => q.SignCode == signCode);
            }
            return temp.AsQueryable();
        }

        public IQueryable<SignIn> Query(bool? status, string name = "", int mid = 0, string jid = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(q => q.IsSign == status);
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name.Contains(name));
            }
            if (mid != 0)
            {
                query = query.Where(q => q.MeetingID == mid);
            }
            if (!string.IsNullOrEmpty(jid))
            {
                query = query.Where(q => q.JobID == jid);
            }
            return query;
        }

        public SignIn FindBySignCode(string signCode)
        {
            return db.SignIn.Where(q => q.SignCode == signCode).FirstOrDefault();
        }

        public SignIn FindBy(int id)
        {
            return db.SignIn.Find(id);
        }

        public bool CleanOldList(int mid)
        {
            try
            {
                var queryList = GetAll().Where(q => q.MeetingID == mid).ToList();
                if (queryList.Count() > 0)
                {
                    foreach (var data in queryList)
                    {
                        db.SignIn.Remove(data);
                    }
                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}