﻿using MeetSignInAttendanceNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Repositories
{
    public class AdminRepository : GenericRepository<MainAdmin>
    {
        //private MeetSignInDBEntity db;

        //public AdminRepository() : this(null) { }

        //public AdminRepository(MeetSignInDBEntity context)
        //{
        //    db = context ?? new MeetSignInDBEntity();
        //}


        public AdminRepository(MeetingSignInDBEntities context) : base(context) { }

        public IQueryable<MainAdmin> Query(bool? status, int type = 0, string account = "")
        {
            var query = GetAll();
            if (status.HasValue)
            {
                query = query.Where(p => p.Status == status);
            }
            if (type != 0)
            {
                query = query.Where(q => q.Type == type);
            }
            if (!string.IsNullOrEmpty(account))
            {
                query = query.Where(q => q.Account.Contains(account));
            }
            return query;
        }

        public MainAdmin Login(string account, string password)
        {
            MainAdmin admin = GetAll().Where(p => p.Account == account && p.Password == password && p.Status).FirstOrDefault();
            return admin;
        }
    }
}