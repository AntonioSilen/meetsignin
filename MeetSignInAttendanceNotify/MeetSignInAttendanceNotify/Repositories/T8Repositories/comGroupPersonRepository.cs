﻿using MeetSignInAttendanceNotify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Repositories.T8Repositories
{
    public class comGroupPersonRepository : GenericRepository<comGroupPerson>
    {
        public comGroupPersonRepository(T8ERPEntities context) : base(context) { }
        private T8ERPEntities t8db = new T8ERPEntities();
        //private InspectionEntities db = new InspectionEntities();
        public IQueryable<comGroupPerson> Query()
        {
            var query = GetAll();

            return query;
        }

        public comGroupPerson FindByPersonID(string personId)
        {
            return t8db.comGroupPerson.Find(personId);
        }
    }
}