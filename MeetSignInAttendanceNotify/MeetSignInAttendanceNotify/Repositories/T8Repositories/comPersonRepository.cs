﻿using MeetSignInAttendanceNotify.Models;
using MeetSignInAttendanceNotify.Models.T8.Join;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Repositories.T8Repositories
{
    public class comPersonRepository : GenericRepository<comPerson>
    {
        public comPersonRepository(T8ERPEntities context) : base(context) { }
        private T8ERPEntities t8db = new T8ERPEntities();
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();

        public IQueryable<GroupJoinPersonAndDepartment> GetPersonQuery(string name = "", string personId = "", string deptId = "")
        {
            var query = from g in t8db.comGroupPerson
                        join p in t8db.comPerson
                        on g.PersonId equals p.PersonId
                        join d in t8db.comDepartment
                        on p.DeptId equals d.DeptId
                        where p.ServiceStatus != "40"
                        //&& !d.DeptId.Contains("Z")
                        //&& !d.DeptId.Contains("H")
                        select new GroupJoinPersonAndDepartment()
                        {
                            PersonId = g.PersonId,
                            PersonName = g.PersonName,
                            EngName = g.EngName,

                            Lv = d.Lv,
                            DeptId = d.DeptId,
                            ParentDeptId = d.ParentDeptId,
                            //ParentDeptId = d.ParentDeptId == "0001" ? "A00" : d.ParentDeptId,
                            //MainParentDeptId = d.DeptId.Substring(0, 1) + "00",

                            DeptName = d.DeptName,
                            InductionDate = p.InductionDate,
                            WorkAge = p.WorkAge,
                            WorkMonths = 0,
                            Birthday = g.Birthday,
                            Age = 0,
                            EMail = g.EMail,
                            MSNNo = g.MSNNo
                        };

            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptId == deptId || q.ParentDeptId == deptId);
            }
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(q => q.PersonId.Contains(personId));
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.PersonName.Contains(name) || q.EngName.Contains(name));
            }

            return query;
        }

        public IQueryable<comDepartment> GetDepartments()
        {
            var query = t8db.comDepartment;

            return query;
        }
        public comDepartment FindDepartments(string deptId)
        {
            var query = t8db.comDepartment.Find(deptId);

            return query;
        }

        public IQueryable<comPerson> Query()
        {
            var query = GetAll();

            return query;
        }

        public comPerson FindBy(int id)
        {
            return t8db.comPerson.Find(id);
        }

        public GroupJoinPersonAndDepartment FindByPersonID(string personId)
        {
            var temp = GetPersonQuery().ToList();
            var result =  temp.Where(q => q.PersonId == personId).FirstOrDefault();
            result.ParentDeptId = result.ParentDeptId == "0001" ? "A00" : result.ParentDeptId;
            //部門層級太低，上提一層
            if (result.ParentDeptId.IndexOf("00") < 0)
            {
                result.ParentDeptId = result.ParentDeptId.Substring(0, 1) + "00";
            }
            return result;
        }

        //public IQueryable<GroupJoinPersonAndDepartment> GetSelected(string selected)
        //{
        //    var selectedList = selected.Split(',');
        //    return GetPersonQuery().Where(q => selectedList.Contains(q.PersonId));
        //}

        public int Age(DateTime bday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - bday.Year;
            if (now < bday.AddYears(age))
                age--;
            return age;
        }
    }
}