﻿using MeetSignInAttendanceNotify.Models;
using MeetSignInAttendanceNotify.Models.T8.Join;
using MeetSignInAttendanceNotify.Utility.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Repositories.T8Repositories
{
    public class PersonRepository : GenericRepository<comPerson>
    {
        public PersonRepository(T8ERPEntities context) : base(context) { }
        private T8ERPEntities t8db = new T8ERPEntities();
        private MeetingSignInDBEntities db = new MeetingSignInDBEntities();
        public List<string> cadreList = new string[] { "0001", "1012", "0663", "0025", "1279", "1077", "1074", "0163", "1830", "1853", "1362", "2055", "0172", "0367", "0029", "0030", "0033", "0010", "0011", "0014", "0017", "0045", "0689", "1277", "2165", "2230", "1677", "1997", "2329", "2409", "1819" }.ToList();

        public IQueryable<GroupJoinPersonAndDepartment> GetPersonQuery(string name = "", string jobID = "", string deptId = "", bool isCadre = false)
        {
            var query = from g in t8db.comGroupPerson
                        join p in t8db.comPerson
                        on g.PersonId equals p.PersonId
                        join d in t8db.comDepartment
                        on p.DeptId equals d.DeptId
                        where p.ServiceStatus != "40"
                        select new GroupJoinPersonAndDepartment()
                        {
                            JobID = g.PersonId,
                            PersonId = g.PersonId,
                            Name = g.PersonName,
                            EngName = g.EngName,
                            DeptID = d.DeptId,
                            DeptId = d.DeptId,
                            DepartmentStr = d.DeptName,
                            DeptName = d.DeptName,
                            ParentDeptID = d.ParentDeptId,
                            InductionDate = p.InductionDate,
                            WorkAge = p.WorkAge,
                            WorkMonths = 0,
                            Birthday = g.Birthday,
                            Age = 0,
                            Email = g.EMail,
                            MSNNo = g.MSNNo,
                            Phone = g.Phone,
                            Gender = g.Sex == 0 ? "男" : "女",
                            EyeState = g.EyeState,
                            PositionId = p.PositionId
                        };

            //var quertList = query.ToArray();
            //foreach (var item in quertList)
            //{
            //    var departmentInfo = db.Department.Where(q => q.Title == item.DepartmentStr).FirstOrDefault();
            //    if (departmentInfo == null)
            //    {
            //        DepartmentRepository departmentRepository = new DepartmentRepository(new MeetingSignInDBEntities());
            //        departmentRepository.Insert(new Department()
            //        {
            //            Title = item.DepartmentStr,
            //            Status = 1,
            //            IsForeign = false,
            //        });
            //        departmentInfo = db.Department.Where(q => q.Title == item.DepartmentStr).FirstOrDefault();
            //    }

            //    item.DepartmentID = departmentInfo.ID;

            //    item.WorkAge = Convert.ToInt32(Math.Round(Convert.ToDouble(item.WorkAge / 12)));
            //    item.WorkMonths = item.WorkAge % 12;
            //    item.Age = Age(DateTime.ParseExact(item.Birthday.ToString(), "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces));
            //}

            if (isCadre)
            {
                query = query.Where(q => cadreList.Contains(q.JobID));
                return query;
            }
            if (!string.IsNullOrEmpty(deptId))
            {
                query = query.Where(q => q.DeptID == deptId);
            }
            if (!string.IsNullOrEmpty(jobID))
            {
                query = query.Where(q => q.JobID.Contains(jobID));
            }
            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(q => q.Name.Contains(name) || q.EngName.Contains(name));
            }

            return query;
        }

        public IQueryable<comPerson> Query()
        {
            var query = GetAll();
        
            return query;
        }

        public IQueryable<comGroupPerson> GroupPersonQuery()
        {
            var query = t8db.comGroupPerson;
        
            return query;
        }

        public comPerson FindBy(int id)
        {
            return t8db.comPerson.Find(id);
        }

        public GroupJoinPersonAndDepartment FindByJobID(string jid)
        {
            var temp = GetPersonQuery().ToList();
            return temp.Where(q => q.JobID == jid).FirstOrDefault();
        }

        public IQueryable<GroupJoinPersonAndDepartment> GetSelected(string selected)
        {
            var selectedList = selected.Split(',');
            return GetPersonQuery().Where(q => selectedList.Contains(q.JobID));
        }

        public int Age(DateTime bday)
        {
            DateTime now = DateTime.Today;
            int age = now.Year - bday.Year;
            if (now < bday.AddYears(age))
                age--;
            return age;
        }
    }
}