//------------------------------------------------------------------------------
// <auto-generated>
//     這個程式碼是由範本產生。
//
//     對這個檔案進行手動變更可能導致您的應用程式產生未預期的行為。
//     如果重新產生程式碼，將會覆寫對這個檔案的手動變更。
// </auto-generated>
//------------------------------------------------------------------------------

namespace MeetSignInAttendanceNotify.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Meeting
    {
        public int ID { get; set; }
        public string MeetingNumber { get; set; }
        public string Title { get; set; }
        public string Organiser { get; set; }
        public int Department { get; set; }
        public int FactoryID { get; set; }
        public int RoomID { get; set; }
        public System.DateTime SignDate { get; set; }
        public System.DateTime StartDate { get; set; }
        public System.DateTime EndDate { get; set; }
        public string Url { get; set; }
        public string Note { get; set; }
        public string Record { get; set; }
        public string FileOne { get; set; }
        public string FileTwo { get; set; }
        public Nullable<bool> IsAttendance { get; set; }
        public bool IsSignable { get; set; }
        public int LateMinutes { get; set; }
        public System.DateTime UpdateDate { get; set; }
        public int Updater { get; set; }
        public System.DateTime CreateDate { get; set; }
        public int Creater { get; set; }
    }
}
