﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Models
{
    /// <summary>
    /// 1.系統管理員  2.使用者
    /// </summary>
    public enum AdminType //管理員類型
    {
        /// <summary>
        /// 系統管理員
        /// </summary>
        [Description("系統管理員")]
        Sysadmin = 1,

        /// <summary>
        /// 總經理
        /// </summary>
        [Description("總經理")]
        President = 2,

        /// <summary>
        /// 副總經理
        /// </summary>
        [Description("副總經理")]
        Vice = 3,

        /// <summary>
        /// 校長
        /// </summary>
        [Description("校長")]
        Principal = 4,

        /// <summary>
        /// 主管
        /// </summary>
        [Description("主管")]
        Manager = 5,

        ///// <summary>
        ///// 承辦人
        ///// </summary>
        //[Description("承辦人")]
        //Officer = 6,

        /// <summary>
        /// 一般員工
        /// </summary>
        [Description("一般員工")]
        Employee = 7,
    }

    /// <summary>
    /// 廠區
    /// </summary>
    public enum Factory
    {
        /// <summary>
        /// 一廠
        /// </summary>
        [Description("一廠")]
        Bar = 1,

        /// <summary>
        /// 二廠
        /// </summary>
        [Description("二廠")]
        Straight = 2,

        /// <summary>
        /// 三廠
        /// </summary>
        [Description("三廠")]
        Square = 3,

        /// <summary>
        /// 全廠
        /// </summary>
        [Description("全廠")]
        Fairlybike = 9,
    }

    /// <summary>
    /// 裝置
    /// </summary>
    public enum DeviceType
    {
        /// <summary>
        /// 電腦版
        /// </summary>
        [Description("電腦版")]
        PC = 1,

        /// <summary>
        /// 手機版
        /// </summary>
        [Description("手機版")]
        Mobile = 2,
    }

    /// <summary>
    /// 簽到結果
    /// </summary>
    public enum SignInResult
    {
        /// <summary>
        /// 失敗
        /// </summary>
        [Description("簽到失敗，請確認選擇會議室是否取消或更換地址，謝謝。")]
        Failed = 1,

        /// <summary>
        /// 未開放簽到
        /// </summary>
        [Description("尚未開放簽到，請於會議開放簽到後再行簽到，謝謝。")]
        NotSignable = 2,

        /// <summary>
        /// 未在出席名單
        /// </summary>
        [Description("您並未在出席名單中，請確認填寫會議室及工號是否正確，謝謝。")]
        NotInList = 3,

        /// <summary>
        /// 成功
        /// </summary>
        [Description("簽到成功，感謝您的參與。")]
        Successed = 4,

        /// <summary>
        /// 重複
        /// </summary>
        [Description("您已簽到，感謝您的參與。")]
        Repeat = 5,

        /// <summary>
        /// 重複
        /// </summary>
        [Description("會議室無申請紀錄，請確認填寫會議室是否正確，謝謝。")]
        NotUsed = 6,

        /// <summary>
        /// 超過時間
        /// </summary>
        [Description("由於您已超過簽到時限，故無法簽到，如有疑問請聯繫會議發起人，感謝您的配合。")]
        Late = 7,

        /// <summary>
        /// 超過時間
        /// </summary>
        [Description("無效登入Qrcode，請使用通知信中Qrcode掃碼，謝謝。")]
        NotFound = 8,
    }

    /// <summary>
    /// 訓練類別
    /// </summary>
    public enum Category
    {
        /// <summary>
        /// 內訓
        /// </summary>
        [Description("內訓")]
        Internal = 1,

        /// <summary>
        /// 外訓
        /// </summary>
        [Description("外訓")]
        External = 2,

        /// <summary>
        /// 研討會
        /// </summary>
        [Description("研討會")]
        Seminar = 3,

        /// <summary>
        /// 外聘講師
        /// </summary>
        [Description("外聘講師")]
        ExternalLecturer = 4,
    }

    /// <summary>
    /// 證書/證明
    /// </summary>
    public enum Certification
    {
        /// <summary>
        /// 有
        /// </summary>
        [Description("有")]
        Yes = 1,

        /// <summary>
        /// 無
        /// </summary>
        [Description("無")]
        No = 2,

        /// <summary>
        /// 上課證明
        /// </summary>
        [Description("上課證明")]
        ClassCert = 3,
    }

    /// <summary>
    /// 異常狀況
    /// </summary>
    public enum Abnormal
    {
        /// <summary>
        /// 乾咳 / ไอแห้ง
        /// </summary>
        [Description("乾咳 / ไอแห้ง")]
        One = 1,

        /// <summary>
        /// 喉嚨痛 / เจ็บคอ
        /// </summary>
        [Description("喉嚨痛 / เจ็บคอ")]
        Two = 2,

        /// <summary>
        /// 發燒 / ไข้
        /// </summary>
        [Description("發燒 / ไข้")]
        Three = 3,
        /// <summary>
        /// 本人或密切接觸者有確診者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดของฉันได้รับการยืนยันเคสแล้ว
        /// </summary>
        [Description("本人或密切接觸者有確診者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดของฉันได้รับการยืนยันเคสแล้ว")]
        Four = 4,

        /// <summary>
        /// 本人或密切接觸者有疑似病例者 / ผู้ที่มีเคสต้องสงสัยในตัวเองหรือคนใกล้ชิด
        /// </summary>
        [Description("本人或密切接觸者有疑似病例者 / ผู้ที่มีเคสต้องสงสัยในตัวเองหรือคนใกล้ชิด")]
        Five = 5,

        /// <summary>
        /// 本人或密切接觸者有遭居家檢疫者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดอยู่ภายใต้การกักกันที่บ้าน
        /// </summary>
        [Description("本人或密切接觸者有遭居家檢疫者 / ฉันหรือผู้ติดต่อที่ใกล้ชิดอยู่ภายใต้การกักกันที่บ้าน")]
        Six = 6,

        /// <summary>
        /// 本人與確診案例同時段出現於活動地點，或有收到來自中央疫情指揮中心的疫情警示相關訊息者 / บุคคลที่ปรากฏตัว ณ สถานที่เกิดเหตุพร้อมกับผู้ป่วยที่ได้รับการยืนยัน หรือผู้ที่ได้รับข้อความเตือนโรคระบาดจากศูนย์บัญชาการแพร่ระบาดกลาง
        /// </summary>
        [Description("本人與確診案例同時段出現於活動地點，或有收到來自中央疫情指揮中心的疫情警示相關訊息者 / บุคคลที่ปรากฏตัว ณ สถานที่เกิดเหตุพร้อมกับผู้ป่วยที่ได้รับการยืนยัน หรือผู้ที่ได้รับข้อความเตือนโรคระบาดจากศูนย์บัญชาการแพร่ระบาดกลาง")]
        Seven = 7,

        /// <summary>
        /// 其他 / อื่นๆ
        /// </summary>
        [Description("其他 / อื่นๆ")]
        Eight = 8,

        /// <summary>
        /// 鼻塞 流鼻水 / คัดจมูก น้ำมูกไหล
        /// </summary>
        [Description("鼻塞 流鼻水 / คัดจมูก น้ำมูกไหล")]
        Nine = 9,

        /// <summary>
        /// 腹瀉 / ท้องเสีย
        /// </summary>
        [Description("腹瀉 / ท้องเสีย")]
        Ten = 10,
    }

    /// <summary>
    /// 審核狀態
    /// </summary>
    public enum AuditState
    {
        /// <summary>
        /// 等待承辦人簽核中
        /// </summary>
        [Description("等待承辦人簽核中")]
        Processing = 0,

        /// <summary>
        /// 待主管簽核中
        /// </summary>
        [Description("待主管簽核中")]
        OfficerAudit = 1,

        /// <summary>
        /// 待副總經理簽核中
        /// </summary>
        [Description("待副總經理簽核中")]
        ManagerAudit = 2,

        /// <summary>
        /// 待校長簽核中
        /// </summary>
        [Description("待校長簽核中")]
        ViceAudit = 3,

        /// <summary>
        /// 待總經理簽核中
        /// </summary>
        [Description("待總經理簽核中")]
        PrincipalAudit = 4,

        /// <summary>
        /// 簽核通過
        /// </summary>
        [Description("簽核通過")]
        PresidentAudit = 5,
    }


    /// <summary>
    /// 表單狀態
    /// </summary>
    public enum ApprovalState
    {
        /// <summary>
        /// 狀態:草稿
        /// </summary>
        [Description("草稿")]
        Draft = 0,

        /// <summary>
        /// 狀態:審核中
        /// </summary>
        [Description("審核中")]
        Approving = 1,

        /// <summary>
        /// 狀態:審核通過
        /// </summary>
        [Description("審核通過")]
        Approved = 2,

        /// <summary>
        /// 狀態:審核駁回
        /// </summary>
        [Description("審核駁回")]
        Reject = 3,
    }

    /// <summary>
    /// KPI層級
    /// </summary>
    public enum KPIType
    {
        /// <summary>
        /// 倉儲
        /// </summary>
        [Description("倉儲")]
        Warehouse = 1,

        /// <summary>
        /// 營銷
        /// </summary>
        [Description("營銷")]
        Marketing = 2,

        /// <summary>
        /// 公司制度
        /// </summary>
        [Description("公司制度")]
        System = 3,

        /// <summary>
        /// 人事
        /// </summary>
        [Description("人事")]
        HR = 4,

        /// <summary>
        /// 環境
        /// </summary>
        [Description("環境")]
        Environment = 5,
    }

    /// <summary>
    /// KPI層級
    /// </summary>
    public enum KPILevel
    {
        /// <summary>
        /// 菲力指標
        /// </summary>
        [Description("菲力指標")]
        FairlyKPI = 1,

        /// <summary>
        /// 部門指標
        /// </summary>
        [Description("部門指標")]
        DeptKPI = 2,
    }

    /// <summary>
    /// 審核對象類型
    /// </summary>
    public enum TargetType
    {
        /// <summary>
        /// KPI
        /// </summary>
        [Description("部門KPI")]
        KPI = 1,

        /// <summary>
        /// PersonKPI
        /// </summary>
        [Description("個人KPI")]
        PersonKPI = 2,

        /// <summary>
        /// PersonKPIState
        /// </summary>
        [Description("個人進度")]
        PersonKPIState = 3,
    }

    /// <summary>
    /// 表單狀態
    /// </summary>
    public enum ApprovalAction
    {
        /// <summary>
        /// 動作:送審
        /// </summary>
        [Description("動作:送審")]
        Submit = 1,

        /// <summary>
        /// 動作:核准
        /// </summary>
        [Description("動作:核准")]
        Approve = 2,

        /// <summary>
        /// 動作:退回
        /// </summary>
        [Description("動作:退回")]
        Reject = 3,

        /// <summary>
        /// 動作:編輯
        /// </summary>
        [Description("動作:編輯")]
        Edit = 4,
    }

    /// <summary>
    /// 審核類型
    /// </summary>
    public enum AuditType
    {
        /// <summary>
        /// 外訓計畫表
        /// </summary>
        [Description("外訓計畫表")]
        External = 1,

        /// <summary>
        /// 內訓計畫表
        /// </summary>
        [Description("內訓計畫表")]
        Internal = 2,

        /// <summary>
        /// 行動方案
        /// </summary>
        [Description("行動方案")]
        ClassActionPlan = 3,

        /// <summary>
        /// 課後意見
        /// </summary>
        [Description("課後意見")]
        ClassOpinion = 4,

        /// <summary>
        /// 心得報告
        /// </summary>
        [Description("心得報告")]
        ClassReport = 5,

        /// <summary>
        /// 外訓驗收
        /// </summary>
        [Description("外訓驗收")]
        ExternalAcceptance = 6,

        /// <summary>
        /// 內訓驗收
        /// </summary>
        [Description("內訓驗收")]
        InternalAcceptance = 7,
    }

    /// <summary>
    /// 表單簽核
    /// </summary>
    public enum FormAudit
    {
        /// <summary>
        /// 待審
        /// </summary>
        [Description("待審")]
        Processing = 1,

        /// <summary>
        /// 核准
        /// </summary>
        [Description("核准")]
        Approval = 2,

        /// <summary>
        /// 駁回
        /// </summary>
        [Description("駁回")]
        Reject = 3,
    }

    /// <summary>
    /// 同意程度
    /// </summary>
    public enum AdmitRadio
    {
        /// <summary>
        /// 非常不同意
        /// </summary>
        [Description("非常不同意")]
        L1 = 1,

        /// <summary>
        /// 不同意
        /// </summary>
        [Description("不同意")]
        L2 = 2,

        /// <summary>
        /// 不太同意
        /// </summary>
        [Description("不太同意")]
        L3 = 3,

        /// <summary>
        /// 普通
        /// </summary>
        [Description("普通")]
        L4 = 4,

        /// <summary>
        /// 部分同意
        /// </summary>
        [Description("部分同意")]
        L5 = 5,

        /// <summary>
        /// 同意
        /// </summary>
        [Description("同意")]
        L6 = 6,

        /// <summary>
        /// 完全同意
        /// </summary>
        [Description("完全同意")]
        L7 = 7,
    }

    /// <summary>
    /// 滿意程度
    /// </summary>
    public enum SatisfyRadio
    {
        /// <summary>
        /// 非常不滿意
        /// </summary>
        [Description("非常不滿意")]
        L1 = 1,

        /// <summary>
        /// 不滿意
        /// </summary>
        [Description("不滿意")]
        L2 = 2,

        /// <summary>
        /// 稍微不滿意
        /// </summary>
        [Description("稍微不滿意")]
        L3 = 3,

        /// <summary>
        /// 普通
        /// </summary>
        [Description("普通")]
        L4 = 4,

        /// <summary>
        /// 還行
        /// </summary>
        [Description("還行")]
        L5 = 5,

        /// <summary>
        /// 滿意
        /// </summary>
        [Description("滿意")]
        L6 = 6,

        /// <summary>
        /// 非常滿意
        /// </summary>
        [Description("非常滿意")]
        L7 = 7,
    }

    /// <summary>
    /// 難易程度
    /// </summary>
    public enum DifficultyRadio
    {
        /// <summary>
        /// 有點難
        /// </summary>
        [Description("有點難")]
        Hard = 1,

        /// <summary>
        /// 適中
        /// </summary>
        [Description("適中")]
        OK = 2,

        /// <summary>
        /// 有點簡單
        /// </summary>
        [Description("有點簡單")]
        Easy = 3,
    }
}