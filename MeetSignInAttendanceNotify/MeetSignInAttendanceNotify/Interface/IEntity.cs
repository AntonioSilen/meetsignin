﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MeetSignInAttendanceNotify.Interface
{
    public interface IEntity
    {
        int ID { get; set; }
    }
}